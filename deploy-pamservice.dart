//#!/usr/bin/env dart
import 'dart:async';
import 'dart:io';
import 'dart:convert';

import 'lib/version.dart' as version;

main() async {
  print(Directory.current);

  // first check if git is clean
  // git diff --quiet --exit-code --cached
  if (await start('git', ['diff', '--quiet', '--exit-code'],
          exitOnError: false) !=
      0) {
    print('git is dirty ... commit first.');
    exit(1);
  } else {
    print('git is clean ...');
  }

  var versionFile = File('lib/version.dart');

  var ver = version.VERSION;
  ver['patch']++;
  ver['date'] = DateTime.now().toUtc().toIso8601String();
  ver['tag'] = '${ver['major']}.${ver['minor']}.${ver['patch']}';

  versionFile.writeAsString('Map VERSION=${json.encode(ver)};');

  await start('git', ['add', '-A']);
  await start('git', ['commit', '-m', 'build ${ver['tag']}']);
  await start('git', ['tag', '-a', ver['tag'] as String, '-m', 'build ${ver['tag']}']);
}

Future<int> start(String executable, List<String> arguments,
    {bool exitOnError: true}) async {
  print('$executable ${arguments.join(' ')}');
  var process = await Process.start(executable, arguments);

  stdout.addStream(process.stdout);
  stderr.addStream(process.stderr);

  var exitCode = await process.exitCode;
  if (exitCode != 0) {
    print('$executable failed $exitCode');
    if (exitOnError) exit(exitCode);
  }

  return exitCode;
}
