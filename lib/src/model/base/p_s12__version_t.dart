part of ps_model_base;

class PS12_Version_tBase extends base.Base {
  static const List<String> PROPERTY_NAMES = const [
    Vocabulary.tICCBInfo_OP,
    Vocabulary.tSYCBInfo_OP,
    Vocabulary.tWSCBInfo_OP
  ];
  BoardInfo_t _tICCBInfo;
  BoardInfo_t _tSYCBInfo;
  BoardInfo_t _tWSCBInfo;

  PS12_Version_tBase() : super() {
    this.noEventDo(() {
      this.tICCBInfo = new BoardInfo_t();
      this.tSYCBInfo = new BoardInfo_t();
      this.tWSCBInfo = new BoardInfo_t();
    });
  }

  PS12_Version_tBase.json(Map m) : super.json(m) {
    this.noEventDo(() {
      this.subKind = m[Vocabulary.SUBKIND] != null
          ? m[Vocabulary.SUBKIND] as String
          : (m[Vocabulary.KIND] != Vocabulary.PS12_Version_t_CLASS
              ? m[Vocabulary.KIND]
              : null) as String;
      if (m[Vocabulary.tICCBInfo_OP] == null)
        this.tICCBInfo = new BoardInfo_t();
      else
        this.tICCBInfo =
            BoardInfo_tBase.fromJson(m[Vocabulary.tICCBInfo_OP] as Map);
      if (m[Vocabulary.tSYCBInfo_OP] == null)
        this.tSYCBInfo = new BoardInfo_t();
      else
        this.tSYCBInfo =
            BoardInfo_tBase.fromJson(m[Vocabulary.tSYCBInfo_OP] as Map);
      if (m[Vocabulary.tWSCBInfo_OP] == null)
        this.tWSCBInfo = new BoardInfo_t();
      else
        this.tWSCBInfo =
            BoardInfo_tBase.fromJson(m[Vocabulary.tWSCBInfo_OP] as Map);
    });
  }

  static PS12_Version_t createFromJson(Map m) => PS12_Version_tBase.fromJson(m);
  static PS12_Version_t fromJson(Map m) {
    final kind = m[Vocabulary.KIND] as String;
    switch (kind) {
      case Vocabulary.PS12_Version_t_CLASS:
        return new PS12_Version_t.json(m);
      default:
        throw new ArgumentError.value(
            kind, "bad kind for class PS12_Version_t in fromJson constructor");
    }
  }

  String get kind => Vocabulary.PS12_Version_t_CLASS;
  BoardInfo_t get tICCBInfo => _tICCBInfo;

  set tICCBInfo(BoardInfo_t o) {
    if (o == _tICCBInfo) return;
    assert(o == null || o is BoardInfo_t);
    if (_tICCBInfo != null) _tICCBInfo.parent = null;
    if (o != null) o.parent = this;
    var old = _tICCBInfo;
    _tICCBInfo = o;
    if (hasListener)
      this.sendChangeEvent(new base.PropertyChangedEvent(
          this, Vocabulary.tICCBInfo_OP, old, _tICCBInfo));
  }

  BoardInfo_t get tSYCBInfo => _tSYCBInfo;

  set tSYCBInfo(BoardInfo_t o) {
    if (o == _tSYCBInfo) return;
    assert(o == null || o is BoardInfo_t);
    if (_tSYCBInfo != null) _tSYCBInfo.parent = null;
    if (o != null) o.parent = this;
    var old = _tSYCBInfo;
    _tSYCBInfo = o;
    if (hasListener)
      this.sendChangeEvent(new base.PropertyChangedEvent(
          this, Vocabulary.tSYCBInfo_OP, old, _tSYCBInfo));
  }

  BoardInfo_t get tWSCBInfo => _tWSCBInfo;

  set tWSCBInfo(BoardInfo_t o) {
    if (o == _tWSCBInfo) return;
    assert(o == null || o is BoardInfo_t);
    if (_tWSCBInfo != null) _tWSCBInfo.parent = null;
    if (o != null) o.parent = this;
    var old = _tWSCBInfo;
    _tWSCBInfo = o;
    if (hasListener)
      this.sendChangeEvent(new base.PropertyChangedEvent(
          this, Vocabulary.tWSCBInfo_OP, old, _tWSCBInfo));
  }

  dynamic get(String name) {
    switch (name) {
      case Vocabulary.tICCBInfo_OP:
        return this.tICCBInfo;
      case Vocabulary.tSYCBInfo_OP:
        return this.tSYCBInfo;
      case Vocabulary.tWSCBInfo_OP:
        return this.tWSCBInfo;
      default:
        return super.get(name);
    }
  }

  set(String name, dynamic value) {
    switch (name) {
      case Vocabulary.tICCBInfo_OP:
        this.tICCBInfo = value as BoardInfo_t;
        return;
      case Vocabulary.tSYCBInfo_OP:
        this.tSYCBInfo = value as BoardInfo_t;
        return;
      case Vocabulary.tWSCBInfo_OP:
        this.tWSCBInfo = value as BoardInfo_t;
        return;
      default:
        super.set(name, value);
    }
  }

  List<String> getPropertyNames() =>
      new List.from(super.getPropertyNames(), growable: true)
        ..addAll(PROPERTY_NAMES);

  PS12_Version_t copy() => new PS12_Version_t.json(this.toJson());
  Map toJson() {
    var m = super.toJson() as Map;
    m[Vocabulary.KIND] = Vocabulary.PS12_Version_t_CLASS;
    if (this.subKind != null && this.subKind != Vocabulary.PS12_Version_t_CLASS)
      m[Vocabulary.SUBKIND] = this.subKind;
    else
      m.remove(Vocabulary.SUBKIND);
    m[Vocabulary.tICCBInfo_OP] = tICCBInfo == null ? null : tICCBInfo.toJson();
    m[Vocabulary.tSYCBInfo_OP] = tSYCBInfo == null ? null : tSYCBInfo.toJson();
    m[Vocabulary.tWSCBInfo_OP] = tWSCBInfo == null ? null : tWSCBInfo.toJson();
    return m;
  }
}
