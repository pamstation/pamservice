part of ps_model_base;

class Focus_LogicalPos_tBase extends base.Base {
  static const List<String> PROPERTY_NAMES = const [
    Vocabulary.rPosForFilter1_DP,
    Vocabulary.rPosForFilter2_DP,
    Vocabulary.rPosForFilter3_DP,
    Vocabulary.rOffsetForDisp1_DP,
    Vocabulary.rOffsetForDisp2_DP,
    Vocabulary.rOffsetForDisp3_DP
  ];
  double _rPosForFilter1;
  double _rPosForFilter2;
  double _rPosForFilter3;
  double _rOffsetForDisp1;
  double _rOffsetForDisp2;
  double _rOffsetForDisp3;

  Focus_LogicalPos_tBase() : super() {
    this.noEventDo(() {
      this.rPosForFilter1 = 0.0;
      this.rPosForFilter2 = 0.0;
      this.rPosForFilter3 = 0.0;
      this.rOffsetForDisp1 = 0.0;
      this.rOffsetForDisp2 = 0.0;
      this.rOffsetForDisp3 = 0.0;
    });
  }

  Focus_LogicalPos_tBase.json(Map m) : super.json(m) {
    this.noEventDo(() {
      this.subKind = m[Vocabulary.SUBKIND] != null
          ? m[Vocabulary.SUBKIND] as String
          : (m[Vocabulary.KIND] != Vocabulary.Focus_LogicalPos_t_CLASS
              ? m[Vocabulary.KIND]
              : null) as String;
      this.rPosForFilter1 = (m[Vocabulary.rPosForFilter1_DP] as num).toDouble();
      this.rPosForFilter2 = (m[Vocabulary.rPosForFilter2_DP] as num).toDouble();
      this.rPosForFilter3 = (m[Vocabulary.rPosForFilter3_DP] as num).toDouble();
      this.rOffsetForDisp1 =
          (m[Vocabulary.rOffsetForDisp1_DP] as num).toDouble();
      this.rOffsetForDisp2 =
          (m[Vocabulary.rOffsetForDisp2_DP] as num).toDouble();
      this.rOffsetForDisp3 =
          (m[Vocabulary.rOffsetForDisp3_DP] as num).toDouble();
    });
  }

  static Focus_LogicalPos_t createFromJson(Map m) =>
      Focus_LogicalPos_tBase.fromJson(m);
  static Focus_LogicalPos_t fromJson(Map m) {
    final kind = m[Vocabulary.KIND] as String;
    switch (kind) {
      case Vocabulary.Focus_LogicalPos_t_CLASS:
        return new Focus_LogicalPos_t.json(m);
      default:
        throw new ArgumentError.value(kind,
            "bad kind for class Focus_LogicalPos_t in fromJson constructor");
    }
  }

  String get kind => Vocabulary.Focus_LogicalPos_t_CLASS;
  double get rPosForFilter1 => _rPosForFilter1;

  set rPosForFilter1(double o) {
    if (o == _rPosForFilter1) return;
    assert(o == null || o is double);
    var old = _rPosForFilter1;
    _rPosForFilter1 = o;
    if (hasListener)
      this.sendChangeEvent(new base.PropertyChangedEvent(
          this, Vocabulary.rPosForFilter1_DP, old, _rPosForFilter1));
  }

  double get rPosForFilter2 => _rPosForFilter2;

  set rPosForFilter2(double o) {
    if (o == _rPosForFilter2) return;
    assert(o == null || o is double);
    var old = _rPosForFilter2;
    _rPosForFilter2 = o;
    if (hasListener)
      this.sendChangeEvent(new base.PropertyChangedEvent(
          this, Vocabulary.rPosForFilter2_DP, old, _rPosForFilter2));
  }

  double get rPosForFilter3 => _rPosForFilter3;

  set rPosForFilter3(double o) {
    if (o == _rPosForFilter3) return;
    assert(o == null || o is double);
    var old = _rPosForFilter3;
    _rPosForFilter3 = o;
    if (hasListener)
      this.sendChangeEvent(new base.PropertyChangedEvent(
          this, Vocabulary.rPosForFilter3_DP, old, _rPosForFilter3));
  }

  double get rOffsetForDisp1 => _rOffsetForDisp1;

  set rOffsetForDisp1(double o) {
    if (o == _rOffsetForDisp1) return;
    assert(o == null || o is double);
    var old = _rOffsetForDisp1;
    _rOffsetForDisp1 = o;
    if (hasListener)
      this.sendChangeEvent(new base.PropertyChangedEvent(
          this, Vocabulary.rOffsetForDisp1_DP, old, _rOffsetForDisp1));
  }

  double get rOffsetForDisp2 => _rOffsetForDisp2;

  set rOffsetForDisp2(double o) {
    if (o == _rOffsetForDisp2) return;
    assert(o == null || o is double);
    var old = _rOffsetForDisp2;
    _rOffsetForDisp2 = o;
    if (hasListener)
      this.sendChangeEvent(new base.PropertyChangedEvent(
          this, Vocabulary.rOffsetForDisp2_DP, old, _rOffsetForDisp2));
  }

  double get rOffsetForDisp3 => _rOffsetForDisp3;

  set rOffsetForDisp3(double o) {
    if (o == _rOffsetForDisp3) return;
    assert(o == null || o is double);
    var old = _rOffsetForDisp3;
    _rOffsetForDisp3 = o;
    if (hasListener)
      this.sendChangeEvent(new base.PropertyChangedEvent(
          this, Vocabulary.rOffsetForDisp3_DP, old, _rOffsetForDisp3));
  }

  dynamic get(String name) {
    switch (name) {
      case Vocabulary.rPosForFilter1_DP:
        return this.rPosForFilter1;
      case Vocabulary.rPosForFilter2_DP:
        return this.rPosForFilter2;
      case Vocabulary.rPosForFilter3_DP:
        return this.rPosForFilter3;
      case Vocabulary.rOffsetForDisp1_DP:
        return this.rOffsetForDisp1;
      case Vocabulary.rOffsetForDisp2_DP:
        return this.rOffsetForDisp2;
      case Vocabulary.rOffsetForDisp3_DP:
        return this.rOffsetForDisp3;
      default:
        return super.get(name);
    }
  }

  set(String name, dynamic value) {
    switch (name) {
      case Vocabulary.rPosForFilter1_DP:
        this.rPosForFilter1 = value as double;
        return;
      case Vocabulary.rPosForFilter2_DP:
        this.rPosForFilter2 = value as double;
        return;
      case Vocabulary.rPosForFilter3_DP:
        this.rPosForFilter3 = value as double;
        return;
      case Vocabulary.rOffsetForDisp1_DP:
        this.rOffsetForDisp1 = value as double;
        return;
      case Vocabulary.rOffsetForDisp2_DP:
        this.rOffsetForDisp2 = value as double;
        return;
      case Vocabulary.rOffsetForDisp3_DP:
        this.rOffsetForDisp3 = value as double;
        return;
      default:
        super.set(name, value);
    }
  }

  List<String> getPropertyNames() =>
      new List.from(super.getPropertyNames(), growable: true)
        ..addAll(PROPERTY_NAMES);

  Focus_LogicalPos_t copy() => new Focus_LogicalPos_t.json(this.toJson());
  Map toJson() {
    var m = super.toJson() as Map;
    m[Vocabulary.KIND] = Vocabulary.Focus_LogicalPos_t_CLASS;
    if (this.subKind != null &&
        this.subKind != Vocabulary.Focus_LogicalPos_t_CLASS)
      m[Vocabulary.SUBKIND] = this.subKind;
    else
      m.remove(Vocabulary.SUBKIND);
    m[Vocabulary.rPosForFilter1_DP] = rPosForFilter1;
    m[Vocabulary.rPosForFilter2_DP] = rPosForFilter2;
    m[Vocabulary.rPosForFilter3_DP] = rPosForFilter3;
    m[Vocabulary.rOffsetForDisp1_DP] = rOffsetForDisp1;
    m[Vocabulary.rOffsetForDisp2_DP] = rOffsetForDisp2;
    m[Vocabulary.rOffsetForDisp3_DP] = rOffsetForDisp3;
    return m;
  }
}
