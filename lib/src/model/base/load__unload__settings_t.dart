part of ps_model_base;

class Load_Unload_Settings_tBase extends base.Base {
  static const List<String> PROPERTY_NAMES = const [
    Vocabulary.rDefaultOverPressure_DP,
    Vocabulary.rDefaultUnderPressure_DP,
    Vocabulary.rInitWagonTemperature_DP,
    Vocabulary.rSafeUnloadTemperature_DP
  ];
  double _rDefaultOverPressure;
  double _rDefaultUnderPressure;
  double _rInitWagonTemperature;
  double _rSafeUnloadTemperature;

  Load_Unload_Settings_tBase() : super() {
    this.noEventDo(() {
      this.rDefaultOverPressure = 0.0;
      this.rDefaultUnderPressure = 0.0;
      this.rInitWagonTemperature = 0.0;
      this.rSafeUnloadTemperature = 0.0;
    });
  }

  Load_Unload_Settings_tBase.json(Map m) : super.json(m) {
    this.noEventDo(() {
      this.subKind = m[Vocabulary.SUBKIND] != null
          ? m[Vocabulary.SUBKIND] as String
          : (m[Vocabulary.KIND] != Vocabulary.Load_Unload_Settings_t_CLASS
              ? m[Vocabulary.KIND]
              : null) as String;
      this.rDefaultOverPressure =
          (m[Vocabulary.rDefaultOverPressure_DP] as num).toDouble();
      this.rDefaultUnderPressure =
          (m[Vocabulary.rDefaultUnderPressure_DP] as num).toDouble();
      this.rInitWagonTemperature =
          (m[Vocabulary.rInitWagonTemperature_DP] as num).toDouble();
      this.rSafeUnloadTemperature =
          (m[Vocabulary.rSafeUnloadTemperature_DP] as num).toDouble();
    });
  }

  static Load_Unload_Settings_t createFromJson(Map m) =>
      Load_Unload_Settings_tBase.fromJson(m);
  static Load_Unload_Settings_t fromJson(Map m) {
    final kind = m[Vocabulary.KIND] as String;
    switch (kind) {
      case Vocabulary.Load_Unload_Settings_t_CLASS:
        return new Load_Unload_Settings_t.json(m);
      default:
        throw new ArgumentError.value(kind,
            "bad kind for class Load_Unload_Settings_t in fromJson constructor");
    }
  }

  String get kind => Vocabulary.Load_Unload_Settings_t_CLASS;
  double get rDefaultOverPressure => _rDefaultOverPressure;

  set rDefaultOverPressure(double o) {
    if (o == _rDefaultOverPressure) return;
    assert(o == null || o is double);
    var old = _rDefaultOverPressure;
    _rDefaultOverPressure = o;
    if (hasListener)
      this.sendChangeEvent(new base.PropertyChangedEvent(this,
          Vocabulary.rDefaultOverPressure_DP, old, _rDefaultOverPressure));
  }

  double get rDefaultUnderPressure => _rDefaultUnderPressure;

  set rDefaultUnderPressure(double o) {
    if (o == _rDefaultUnderPressure) return;
    assert(o == null || o is double);
    var old = _rDefaultUnderPressure;
    _rDefaultUnderPressure = o;
    if (hasListener)
      this.sendChangeEvent(new base.PropertyChangedEvent(this,
          Vocabulary.rDefaultUnderPressure_DP, old, _rDefaultUnderPressure));
  }

  double get rInitWagonTemperature => _rInitWagonTemperature;

  set rInitWagonTemperature(double o) {
    if (o == _rInitWagonTemperature) return;
    assert(o == null || o is double);
    var old = _rInitWagonTemperature;
    _rInitWagonTemperature = o;
    if (hasListener)
      this.sendChangeEvent(new base.PropertyChangedEvent(this,
          Vocabulary.rInitWagonTemperature_DP, old, _rInitWagonTemperature));
  }

  double get rSafeUnloadTemperature => _rSafeUnloadTemperature;

  set rSafeUnloadTemperature(double o) {
    if (o == _rSafeUnloadTemperature) return;
    assert(o == null || o is double);
    var old = _rSafeUnloadTemperature;
    _rSafeUnloadTemperature = o;
    if (hasListener)
      this.sendChangeEvent(new base.PropertyChangedEvent(this,
          Vocabulary.rSafeUnloadTemperature_DP, old, _rSafeUnloadTemperature));
  }

  dynamic get(String name) {
    switch (name) {
      case Vocabulary.rDefaultOverPressure_DP:
        return this.rDefaultOverPressure;
      case Vocabulary.rDefaultUnderPressure_DP:
        return this.rDefaultUnderPressure;
      case Vocabulary.rInitWagonTemperature_DP:
        return this.rInitWagonTemperature;
      case Vocabulary.rSafeUnloadTemperature_DP:
        return this.rSafeUnloadTemperature;
      default:
        return super.get(name);
    }
  }

  set(String name, dynamic value) {
    switch (name) {
      case Vocabulary.rDefaultOverPressure_DP:
        this.rDefaultOverPressure = value as double;
        return;
      case Vocabulary.rDefaultUnderPressure_DP:
        this.rDefaultUnderPressure = value as double;
        return;
      case Vocabulary.rInitWagonTemperature_DP:
        this.rInitWagonTemperature = value as double;
        return;
      case Vocabulary.rSafeUnloadTemperature_DP:
        this.rSafeUnloadTemperature = value as double;
        return;
      default:
        super.set(name, value);
    }
  }

  List<String> getPropertyNames() =>
      new List.from(super.getPropertyNames(), growable: true)
        ..addAll(PROPERTY_NAMES);

  Load_Unload_Settings_t copy() =>
      new Load_Unload_Settings_t.json(this.toJson());
  Map toJson() {
    var m = super.toJson() as Map;
    m[Vocabulary.KIND] = Vocabulary.Load_Unload_Settings_t_CLASS;
    if (this.subKind != null &&
        this.subKind != Vocabulary.Load_Unload_Settings_t_CLASS)
      m[Vocabulary.SUBKIND] = this.subKind;
    else
      m.remove(Vocabulary.SUBKIND);
    m[Vocabulary.rDefaultOverPressure_DP] = rDefaultOverPressure;
    m[Vocabulary.rDefaultUnderPressure_DP] = rDefaultUnderPressure;
    m[Vocabulary.rInitWagonTemperature_DP] = rInitWagonTemperature;
    m[Vocabulary.rSafeUnloadTemperature_DP] = rSafeUnloadTemperature;
    return m;
  }
}
