part of ps_model_base;

class LinuxVersionBase extends base.Base {
  static const List<String> PROPERTY_NAMES = const [
    Vocabulary.sysname_DP,
    Vocabulary.nodename_DP,
    Vocabulary.release_DP,
    Vocabulary.version_DP,
    Vocabulary.machine_DP
  ];
  String _sysname;
  String _nodename;
  String _release;
  String _version;
  String _machine;

  LinuxVersionBase() : super() {
    this.noEventDo(() {
      this.sysname = "";
      this.nodename = "";
      this.release = "";
      this.version = "";
      this.machine = "";
    });
  }

  LinuxVersionBase.json(Map m) : super.json(m) {
    this.noEventDo(() {
      this.subKind = m[Vocabulary.SUBKIND] != null
          ? m[Vocabulary.SUBKIND] as String
          : (m[Vocabulary.KIND] != Vocabulary.LinuxVersion_CLASS
              ? m[Vocabulary.KIND]
              : null) as String;
      this.sysname = m[Vocabulary.sysname_DP] as String;
      this.nodename = m[Vocabulary.nodename_DP] as String;
      this.release = m[Vocabulary.release_DP] as String;
      this.version = m[Vocabulary.version_DP] as String;
      this.machine = m[Vocabulary.machine_DP] as String;
    });
  }

  static LinuxVersion createFromJson(Map m) => LinuxVersionBase.fromJson(m);
  static LinuxVersion fromJson(Map m) {
    final kind = m[Vocabulary.KIND] as String;
    switch (kind) {
      case Vocabulary.LinuxVersion_CLASS:
        return new LinuxVersion.json(m);
      default:
        throw new ArgumentError.value(
            kind, "bad kind for class LinuxVersion in fromJson constructor");
    }
  }

  String get kind => Vocabulary.LinuxVersion_CLASS;
  String get sysname => _sysname;

  set sysname(String o) {
    if (o == _sysname) return;
    assert(o == null || o is String);
    var old = _sysname;
    _sysname = o;
    if (hasListener)
      this.sendChangeEvent(new base.PropertyChangedEvent(
          this, Vocabulary.sysname_DP, old, _sysname));
  }

  String get nodename => _nodename;

  set nodename(String o) {
    if (o == _nodename) return;
    assert(o == null || o is String);
    var old = _nodename;
    _nodename = o;
    if (hasListener)
      this.sendChangeEvent(new base.PropertyChangedEvent(
          this, Vocabulary.nodename_DP, old, _nodename));
  }

  String get release => _release;

  set release(String o) {
    if (o == _release) return;
    assert(o == null || o is String);
    var old = _release;
    _release = o;
    if (hasListener)
      this.sendChangeEvent(new base.PropertyChangedEvent(
          this, Vocabulary.release_DP, old, _release));
  }

  String get version => _version;

  set version(String o) {
    if (o == _version) return;
    assert(o == null || o is String);
    var old = _version;
    _version = o;
    if (hasListener)
      this.sendChangeEvent(new base.PropertyChangedEvent(
          this, Vocabulary.version_DP, old, _version));
  }

  String get machine => _machine;

  set machine(String o) {
    if (o == _machine) return;
    assert(o == null || o is String);
    var old = _machine;
    _machine = o;
    if (hasListener)
      this.sendChangeEvent(new base.PropertyChangedEvent(
          this, Vocabulary.machine_DP, old, _machine));
  }

  dynamic get(String name) {
    switch (name) {
      case Vocabulary.sysname_DP:
        return this.sysname;
      case Vocabulary.nodename_DP:
        return this.nodename;
      case Vocabulary.release_DP:
        return this.release;
      case Vocabulary.version_DP:
        return this.version;
      case Vocabulary.machine_DP:
        return this.machine;
      default:
        return super.get(name);
    }
  }

  set(String name, dynamic value) {
    switch (name) {
      case Vocabulary.sysname_DP:
        this.sysname = value as String;
        return;
      case Vocabulary.nodename_DP:
        this.nodename = value as String;
        return;
      case Vocabulary.release_DP:
        this.release = value as String;
        return;
      case Vocabulary.version_DP:
        this.version = value as String;
        return;
      case Vocabulary.machine_DP:
        this.machine = value as String;
        return;
      default:
        super.set(name, value);
    }
  }

  List<String> getPropertyNames() =>
      new List.from(super.getPropertyNames(), growable: true)
        ..addAll(PROPERTY_NAMES);

  LinuxVersion copy() => new LinuxVersion.json(this.toJson());
  Map toJson() {
    var m = super.toJson() as Map;
    m[Vocabulary.KIND] = Vocabulary.LinuxVersion_CLASS;
    if (this.subKind != null && this.subKind != Vocabulary.LinuxVersion_CLASS)
      m[Vocabulary.SUBKIND] = this.subKind;
    else
      m.remove(Vocabulary.SUBKIND);
    m[Vocabulary.sysname_DP] = sysname;
    m[Vocabulary.nodename_DP] = nodename;
    m[Vocabulary.release_DP] = release;
    m[Vocabulary.version_DP] = version;
    m[Vocabulary.machine_DP] = machine;
    return m;
  }
}
