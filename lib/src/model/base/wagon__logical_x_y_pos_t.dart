part of ps_model_base;

class Wagon_LogicalXYPos_tBase extends base.Base {
  static const List<String> PROPERTY_NAMES = const [
    Vocabulary.xyLoadPosition_OP,
    Vocabulary.xyIncubationPosition_OP,
    Vocabulary.xyFrontPanelPosition_OP,
    Vocabulary.xyReadPosition_OP,
    Vocabulary.xyAspiratePosition_OP,
    Vocabulary.xyDispensePosition_OP,
    Vocabulary.xyOffset_Well_OP,
    Vocabulary.xyOffset_Disposable_OP,
    Vocabulary.xyOffset_FocusGlass_OP,
    Vocabulary.xyOffset_DispenseHead1_OP,
    Vocabulary.xyOffset_DispenseHead2_OP,
    Vocabulary.xyOffset_DispenseHead3_OP,
    Vocabulary.xyOffset_DispenseHead4_OP,
    Vocabulary.xyOffset_Prime1_OP,
    Vocabulary.xyOffset_Prime2_OP
  ];
  XYPos _xyLoadPosition;
  XYPos _xyIncubationPosition;
  XYPos _xyFrontPanelPosition;
  XYPos _xyReadPosition;
  XYPos _xyAspiratePosition;
  XYPos _xyDispensePosition;
  XYPos _xyOffset_Well;
  XYPos _xyOffset_Disposable;
  XYPos _xyOffset_FocusGlass;
  XYPos _xyOffset_DispenseHead1;
  XYPos _xyOffset_DispenseHead2;
  XYPos _xyOffset_DispenseHead3;
  XYPos _xyOffset_DispenseHead4;
  XYPos _xyOffset_Prime1;
  XYPos _xyOffset_Prime2;

  Wagon_LogicalXYPos_tBase() : super() {
    this.noEventDo(() {
      this.xyLoadPosition = new XYPos();
      this.xyIncubationPosition = new XYPos();
      this.xyFrontPanelPosition = new XYPos();
      this.xyReadPosition = new XYPos();
      this.xyAspiratePosition = new XYPos();
      this.xyDispensePosition = new XYPos();
      this.xyOffset_Well = new XYPos();
      this.xyOffset_Disposable = new XYPos();
      this.xyOffset_FocusGlass = new XYPos();
      this.xyOffset_DispenseHead1 = new XYPos();
      this.xyOffset_DispenseHead2 = new XYPos();
      this.xyOffset_DispenseHead3 = new XYPos();
      this.xyOffset_DispenseHead4 = new XYPos();
      this.xyOffset_Prime1 = new XYPos();
      this.xyOffset_Prime2 = new XYPos();
    });
  }

  Wagon_LogicalXYPos_tBase.json(Map m) : super.json(m) {
    this.noEventDo(() {
      this.subKind = m[Vocabulary.SUBKIND] != null
          ? m[Vocabulary.SUBKIND] as String
          : (m[Vocabulary.KIND] != Vocabulary.Wagon_LogicalXYPos_t_CLASS
              ? m[Vocabulary.KIND]
              : null) as String;
      if (m[Vocabulary.xyLoadPosition_OP] == null)
        this.xyLoadPosition = new XYPos();
      else
        this.xyLoadPosition =
            XYPosBase.fromJson(m[Vocabulary.xyLoadPosition_OP] as Map);
      if (m[Vocabulary.xyIncubationPosition_OP] == null)
        this.xyIncubationPosition = new XYPos();
      else
        this.xyIncubationPosition =
            XYPosBase.fromJson(m[Vocabulary.xyIncubationPosition_OP] as Map);
      if (m[Vocabulary.xyFrontPanelPosition_OP] == null)
        this.xyFrontPanelPosition = new XYPos();
      else
        this.xyFrontPanelPosition =
            XYPosBase.fromJson(m[Vocabulary.xyFrontPanelPosition_OP] as Map);
      if (m[Vocabulary.xyReadPosition_OP] == null)
        this.xyReadPosition = new XYPos();
      else
        this.xyReadPosition =
            XYPosBase.fromJson(m[Vocabulary.xyReadPosition_OP] as Map);
      if (m[Vocabulary.xyAspiratePosition_OP] == null)
        this.xyAspiratePosition = new XYPos();
      else
        this.xyAspiratePosition =
            XYPosBase.fromJson(m[Vocabulary.xyAspiratePosition_OP] as Map);
      if (m[Vocabulary.xyDispensePosition_OP] == null)
        this.xyDispensePosition = new XYPos();
      else
        this.xyDispensePosition =
            XYPosBase.fromJson(m[Vocabulary.xyDispensePosition_OP] as Map);
      if (m[Vocabulary.xyOffset_Well_OP] == null)
        this.xyOffset_Well = new XYPos();
      else
        this.xyOffset_Well =
            XYPosBase.fromJson(m[Vocabulary.xyOffset_Well_OP] as Map);
      if (m[Vocabulary.xyOffset_Disposable_OP] == null)
        this.xyOffset_Disposable = new XYPos();
      else
        this.xyOffset_Disposable =
            XYPosBase.fromJson(m[Vocabulary.xyOffset_Disposable_OP] as Map);
      if (m[Vocabulary.xyOffset_FocusGlass_OP] == null)
        this.xyOffset_FocusGlass = new XYPos();
      else
        this.xyOffset_FocusGlass =
            XYPosBase.fromJson(m[Vocabulary.xyOffset_FocusGlass_OP] as Map);
      if (m[Vocabulary.xyOffset_DispenseHead1_OP] == null)
        this.xyOffset_DispenseHead1 = new XYPos();
      else
        this.xyOffset_DispenseHead1 =
            XYPosBase.fromJson(m[Vocabulary.xyOffset_DispenseHead1_OP] as Map);
      if (m[Vocabulary.xyOffset_DispenseHead2_OP] == null)
        this.xyOffset_DispenseHead2 = new XYPos();
      else
        this.xyOffset_DispenseHead2 =
            XYPosBase.fromJson(m[Vocabulary.xyOffset_DispenseHead2_OP] as Map);
      if (m[Vocabulary.xyOffset_DispenseHead3_OP] == null)
        this.xyOffset_DispenseHead3 = new XYPos();
      else
        this.xyOffset_DispenseHead3 =
            XYPosBase.fromJson(m[Vocabulary.xyOffset_DispenseHead3_OP] as Map);
      if (m[Vocabulary.xyOffset_DispenseHead4_OP] == null)
        this.xyOffset_DispenseHead4 = new XYPos();
      else
        this.xyOffset_DispenseHead4 =
            XYPosBase.fromJson(m[Vocabulary.xyOffset_DispenseHead4_OP] as Map);
      if (m[Vocabulary.xyOffset_Prime1_OP] == null)
        this.xyOffset_Prime1 = new XYPos();
      else
        this.xyOffset_Prime1 =
            XYPosBase.fromJson(m[Vocabulary.xyOffset_Prime1_OP] as Map);
      if (m[Vocabulary.xyOffset_Prime2_OP] == null)
        this.xyOffset_Prime2 = new XYPos();
      else
        this.xyOffset_Prime2 =
            XYPosBase.fromJson(m[Vocabulary.xyOffset_Prime2_OP] as Map);
    });
  }

  static Wagon_LogicalXYPos_t createFromJson(Map m) =>
      Wagon_LogicalXYPos_tBase.fromJson(m);
  static Wagon_LogicalXYPos_t fromJson(Map m) {
    final kind = m[Vocabulary.KIND] as String;
    switch (kind) {
      case Vocabulary.Wagon_LogicalXYPos_t_CLASS:
        return new Wagon_LogicalXYPos_t.json(m);
      default:
        throw new ArgumentError.value(kind,
            "bad kind for class Wagon_LogicalXYPos_t in fromJson constructor");
    }
  }

  String get kind => Vocabulary.Wagon_LogicalXYPos_t_CLASS;
  XYPos get xyLoadPosition => _xyLoadPosition;

  set xyLoadPosition(XYPos o) {
    if (o == _xyLoadPosition) return;
    assert(o == null || o is XYPos);
    if (_xyLoadPosition != null) _xyLoadPosition.parent = null;
    if (o != null) o.parent = this;
    var old = _xyLoadPosition;
    _xyLoadPosition = o;
    if (hasListener)
      this.sendChangeEvent(new base.PropertyChangedEvent(
          this, Vocabulary.xyLoadPosition_OP, old, _xyLoadPosition));
  }

  XYPos get xyIncubationPosition => _xyIncubationPosition;

  set xyIncubationPosition(XYPos o) {
    if (o == _xyIncubationPosition) return;
    assert(o == null || o is XYPos);
    if (_xyIncubationPosition != null) _xyIncubationPosition.parent = null;
    if (o != null) o.parent = this;
    var old = _xyIncubationPosition;
    _xyIncubationPosition = o;
    if (hasListener)
      this.sendChangeEvent(new base.PropertyChangedEvent(this,
          Vocabulary.xyIncubationPosition_OP, old, _xyIncubationPosition));
  }

  XYPos get xyFrontPanelPosition => _xyFrontPanelPosition;

  set xyFrontPanelPosition(XYPos o) {
    if (o == _xyFrontPanelPosition) return;
    assert(o == null || o is XYPos);
    if (_xyFrontPanelPosition != null) _xyFrontPanelPosition.parent = null;
    if (o != null) o.parent = this;
    var old = _xyFrontPanelPosition;
    _xyFrontPanelPosition = o;
    if (hasListener)
      this.sendChangeEvent(new base.PropertyChangedEvent(this,
          Vocabulary.xyFrontPanelPosition_OP, old, _xyFrontPanelPosition));
  }

  XYPos get xyReadPosition => _xyReadPosition;

  set xyReadPosition(XYPos o) {
    if (o == _xyReadPosition) return;
    assert(o == null || o is XYPos);
    if (_xyReadPosition != null) _xyReadPosition.parent = null;
    if (o != null) o.parent = this;
    var old = _xyReadPosition;
    _xyReadPosition = o;
    if (hasListener)
      this.sendChangeEvent(new base.PropertyChangedEvent(
          this, Vocabulary.xyReadPosition_OP, old, _xyReadPosition));
  }

  XYPos get xyAspiratePosition => _xyAspiratePosition;

  set xyAspiratePosition(XYPos o) {
    if (o == _xyAspiratePosition) return;
    assert(o == null || o is XYPos);
    if (_xyAspiratePosition != null) _xyAspiratePosition.parent = null;
    if (o != null) o.parent = this;
    var old = _xyAspiratePosition;
    _xyAspiratePosition = o;
    if (hasListener)
      this.sendChangeEvent(new base.PropertyChangedEvent(
          this, Vocabulary.xyAspiratePosition_OP, old, _xyAspiratePosition));
  }

  XYPos get xyDispensePosition => _xyDispensePosition;

  set xyDispensePosition(XYPos o) {
    if (o == _xyDispensePosition) return;
    assert(o == null || o is XYPos);
    if (_xyDispensePosition != null) _xyDispensePosition.parent = null;
    if (o != null) o.parent = this;
    var old = _xyDispensePosition;
    _xyDispensePosition = o;
    if (hasListener)
      this.sendChangeEvent(new base.PropertyChangedEvent(
          this, Vocabulary.xyDispensePosition_OP, old, _xyDispensePosition));
  }

  XYPos get xyOffset_Well => _xyOffset_Well;

  set xyOffset_Well(XYPos o) {
    if (o == _xyOffset_Well) return;
    assert(o == null || o is XYPos);
    if (_xyOffset_Well != null) _xyOffset_Well.parent = null;
    if (o != null) o.parent = this;
    var old = _xyOffset_Well;
    _xyOffset_Well = o;
    if (hasListener)
      this.sendChangeEvent(new base.PropertyChangedEvent(
          this, Vocabulary.xyOffset_Well_OP, old, _xyOffset_Well));
  }

  XYPos get xyOffset_Disposable => _xyOffset_Disposable;

  set xyOffset_Disposable(XYPos o) {
    if (o == _xyOffset_Disposable) return;
    assert(o == null || o is XYPos);
    if (_xyOffset_Disposable != null) _xyOffset_Disposable.parent = null;
    if (o != null) o.parent = this;
    var old = _xyOffset_Disposable;
    _xyOffset_Disposable = o;
    if (hasListener)
      this.sendChangeEvent(new base.PropertyChangedEvent(
          this, Vocabulary.xyOffset_Disposable_OP, old, _xyOffset_Disposable));
  }

  XYPos get xyOffset_FocusGlass => _xyOffset_FocusGlass;

  set xyOffset_FocusGlass(XYPos o) {
    if (o == _xyOffset_FocusGlass) return;
    assert(o == null || o is XYPos);
    if (_xyOffset_FocusGlass != null) _xyOffset_FocusGlass.parent = null;
    if (o != null) o.parent = this;
    var old = _xyOffset_FocusGlass;
    _xyOffset_FocusGlass = o;
    if (hasListener)
      this.sendChangeEvent(new base.PropertyChangedEvent(
          this, Vocabulary.xyOffset_FocusGlass_OP, old, _xyOffset_FocusGlass));
  }

  XYPos get xyOffset_DispenseHead1 => _xyOffset_DispenseHead1;

  set xyOffset_DispenseHead1(XYPos o) {
    if (o == _xyOffset_DispenseHead1) return;
    assert(o == null || o is XYPos);
    if (_xyOffset_DispenseHead1 != null) _xyOffset_DispenseHead1.parent = null;
    if (o != null) o.parent = this;
    var old = _xyOffset_DispenseHead1;
    _xyOffset_DispenseHead1 = o;
    if (hasListener)
      this.sendChangeEvent(new base.PropertyChangedEvent(this,
          Vocabulary.xyOffset_DispenseHead1_OP, old, _xyOffset_DispenseHead1));
  }

  XYPos get xyOffset_DispenseHead2 => _xyOffset_DispenseHead2;

  set xyOffset_DispenseHead2(XYPos o) {
    if (o == _xyOffset_DispenseHead2) return;
    assert(o == null || o is XYPos);
    if (_xyOffset_DispenseHead2 != null) _xyOffset_DispenseHead2.parent = null;
    if (o != null) o.parent = this;
    var old = _xyOffset_DispenseHead2;
    _xyOffset_DispenseHead2 = o;
    if (hasListener)
      this.sendChangeEvent(new base.PropertyChangedEvent(this,
          Vocabulary.xyOffset_DispenseHead2_OP, old, _xyOffset_DispenseHead2));
  }

  XYPos get xyOffset_DispenseHead3 => _xyOffset_DispenseHead3;

  set xyOffset_DispenseHead3(XYPos o) {
    if (o == _xyOffset_DispenseHead3) return;
    assert(o == null || o is XYPos);
    if (_xyOffset_DispenseHead3 != null) _xyOffset_DispenseHead3.parent = null;
    if (o != null) o.parent = this;
    var old = _xyOffset_DispenseHead3;
    _xyOffset_DispenseHead3 = o;
    if (hasListener)
      this.sendChangeEvent(new base.PropertyChangedEvent(this,
          Vocabulary.xyOffset_DispenseHead3_OP, old, _xyOffset_DispenseHead3));
  }

  XYPos get xyOffset_DispenseHead4 => _xyOffset_DispenseHead4;

  set xyOffset_DispenseHead4(XYPos o) {
    if (o == _xyOffset_DispenseHead4) return;
    assert(o == null || o is XYPos);
    if (_xyOffset_DispenseHead4 != null) _xyOffset_DispenseHead4.parent = null;
    if (o != null) o.parent = this;
    var old = _xyOffset_DispenseHead4;
    _xyOffset_DispenseHead4 = o;
    if (hasListener)
      this.sendChangeEvent(new base.PropertyChangedEvent(this,
          Vocabulary.xyOffset_DispenseHead4_OP, old, _xyOffset_DispenseHead4));
  }

  XYPos get xyOffset_Prime1 => _xyOffset_Prime1;

  set xyOffset_Prime1(XYPos o) {
    if (o == _xyOffset_Prime1) return;
    assert(o == null || o is XYPos);
    if (_xyOffset_Prime1 != null) _xyOffset_Prime1.parent = null;
    if (o != null) o.parent = this;
    var old = _xyOffset_Prime1;
    _xyOffset_Prime1 = o;
    if (hasListener)
      this.sendChangeEvent(new base.PropertyChangedEvent(
          this, Vocabulary.xyOffset_Prime1_OP, old, _xyOffset_Prime1));
  }

  XYPos get xyOffset_Prime2 => _xyOffset_Prime2;

  set xyOffset_Prime2(XYPos o) {
    if (o == _xyOffset_Prime2) return;
    assert(o == null || o is XYPos);
    if (_xyOffset_Prime2 != null) _xyOffset_Prime2.parent = null;
    if (o != null) o.parent = this;
    var old = _xyOffset_Prime2;
    _xyOffset_Prime2 = o;
    if (hasListener)
      this.sendChangeEvent(new base.PropertyChangedEvent(
          this, Vocabulary.xyOffset_Prime2_OP, old, _xyOffset_Prime2));
  }

  dynamic get(String name) {
    switch (name) {
      case Vocabulary.xyLoadPosition_OP:
        return this.xyLoadPosition;
      case Vocabulary.xyIncubationPosition_OP:
        return this.xyIncubationPosition;
      case Vocabulary.xyFrontPanelPosition_OP:
        return this.xyFrontPanelPosition;
      case Vocabulary.xyReadPosition_OP:
        return this.xyReadPosition;
      case Vocabulary.xyAspiratePosition_OP:
        return this.xyAspiratePosition;
      case Vocabulary.xyDispensePosition_OP:
        return this.xyDispensePosition;
      case Vocabulary.xyOffset_Well_OP:
        return this.xyOffset_Well;
      case Vocabulary.xyOffset_Disposable_OP:
        return this.xyOffset_Disposable;
      case Vocabulary.xyOffset_FocusGlass_OP:
        return this.xyOffset_FocusGlass;
      case Vocabulary.xyOffset_DispenseHead1_OP:
        return this.xyOffset_DispenseHead1;
      case Vocabulary.xyOffset_DispenseHead2_OP:
        return this.xyOffset_DispenseHead2;
      case Vocabulary.xyOffset_DispenseHead3_OP:
        return this.xyOffset_DispenseHead3;
      case Vocabulary.xyOffset_DispenseHead4_OP:
        return this.xyOffset_DispenseHead4;
      case Vocabulary.xyOffset_Prime1_OP:
        return this.xyOffset_Prime1;
      case Vocabulary.xyOffset_Prime2_OP:
        return this.xyOffset_Prime2;
      default:
        return super.get(name);
    }
  }

  set(String name, dynamic value) {
    switch (name) {
      case Vocabulary.xyLoadPosition_OP:
        this.xyLoadPosition = value as XYPos;
        return;
      case Vocabulary.xyIncubationPosition_OP:
        this.xyIncubationPosition = value as XYPos;
        return;
      case Vocabulary.xyFrontPanelPosition_OP:
        this.xyFrontPanelPosition = value as XYPos;
        return;
      case Vocabulary.xyReadPosition_OP:
        this.xyReadPosition = value as XYPos;
        return;
      case Vocabulary.xyAspiratePosition_OP:
        this.xyAspiratePosition = value as XYPos;
        return;
      case Vocabulary.xyDispensePosition_OP:
        this.xyDispensePosition = value as XYPos;
        return;
      case Vocabulary.xyOffset_Well_OP:
        this.xyOffset_Well = value as XYPos;
        return;
      case Vocabulary.xyOffset_Disposable_OP:
        this.xyOffset_Disposable = value as XYPos;
        return;
      case Vocabulary.xyOffset_FocusGlass_OP:
        this.xyOffset_FocusGlass = value as XYPos;
        return;
      case Vocabulary.xyOffset_DispenseHead1_OP:
        this.xyOffset_DispenseHead1 = value as XYPos;
        return;
      case Vocabulary.xyOffset_DispenseHead2_OP:
        this.xyOffset_DispenseHead2 = value as XYPos;
        return;
      case Vocabulary.xyOffset_DispenseHead3_OP:
        this.xyOffset_DispenseHead3 = value as XYPos;
        return;
      case Vocabulary.xyOffset_DispenseHead4_OP:
        this.xyOffset_DispenseHead4 = value as XYPos;
        return;
      case Vocabulary.xyOffset_Prime1_OP:
        this.xyOffset_Prime1 = value as XYPos;
        return;
      case Vocabulary.xyOffset_Prime2_OP:
        this.xyOffset_Prime2 = value as XYPos;
        return;
      default:
        super.set(name, value);
    }
  }

  List<String> getPropertyNames() =>
      new List.from(super.getPropertyNames(), growable: true)
        ..addAll(PROPERTY_NAMES);

  Wagon_LogicalXYPos_t copy() => new Wagon_LogicalXYPos_t.json(this.toJson());
  Map toJson() {
    var m = super.toJson() as Map;
    m[Vocabulary.KIND] = Vocabulary.Wagon_LogicalXYPos_t_CLASS;
    if (this.subKind != null &&
        this.subKind != Vocabulary.Wagon_LogicalXYPos_t_CLASS)
      m[Vocabulary.SUBKIND] = this.subKind;
    else
      m.remove(Vocabulary.SUBKIND);
    m[Vocabulary.xyLoadPosition_OP] =
        xyLoadPosition == null ? null : xyLoadPosition.toJson();
    m[Vocabulary.xyIncubationPosition_OP] =
        xyIncubationPosition == null ? null : xyIncubationPosition.toJson();
    m[Vocabulary.xyFrontPanelPosition_OP] =
        xyFrontPanelPosition == null ? null : xyFrontPanelPosition.toJson();
    m[Vocabulary.xyReadPosition_OP] =
        xyReadPosition == null ? null : xyReadPosition.toJson();
    m[Vocabulary.xyAspiratePosition_OP] =
        xyAspiratePosition == null ? null : xyAspiratePosition.toJson();
    m[Vocabulary.xyDispensePosition_OP] =
        xyDispensePosition == null ? null : xyDispensePosition.toJson();
    m[Vocabulary.xyOffset_Well_OP] =
        xyOffset_Well == null ? null : xyOffset_Well.toJson();
    m[Vocabulary.xyOffset_Disposable_OP] =
        xyOffset_Disposable == null ? null : xyOffset_Disposable.toJson();
    m[Vocabulary.xyOffset_FocusGlass_OP] =
        xyOffset_FocusGlass == null ? null : xyOffset_FocusGlass.toJson();
    m[Vocabulary.xyOffset_DispenseHead1_OP] =
        xyOffset_DispenseHead1 == null ? null : xyOffset_DispenseHead1.toJson();
    m[Vocabulary.xyOffset_DispenseHead2_OP] =
        xyOffset_DispenseHead2 == null ? null : xyOffset_DispenseHead2.toJson();
    m[Vocabulary.xyOffset_DispenseHead3_OP] =
        xyOffset_DispenseHead3 == null ? null : xyOffset_DispenseHead3.toJson();
    m[Vocabulary.xyOffset_DispenseHead4_OP] =
        xyOffset_DispenseHead4 == null ? null : xyOffset_DispenseHead4.toJson();
    m[Vocabulary.xyOffset_Prime1_OP] =
        xyOffset_Prime1 == null ? null : xyOffset_Prime1.toJson();
    m[Vocabulary.xyOffset_Prime2_OP] =
        xyOffset_Prime2 == null ? null : xyOffset_Prime2.toJson();
    return m;
  }
}
