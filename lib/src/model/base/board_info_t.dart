part of ps_model_base;

class BoardInfo_tBase extends base.Base {
  static const List<String> PROPERTY_NAMES = const [
    Vocabulary.iPS12_nr_DP,
    Vocabulary.iBrd_nr_high_DP,
    Vocabulary.iBrd_nr_medium_DP,
    Vocabulary.iBrd_nr_low_DP,
    Vocabulary.iFW_version_DP,
    Vocabulary.iESW_version_DP,
    Vocabulary.rFlashDate_DP
  ];
  int _iPS12_nr;
  int _iBrd_nr_high;
  int _iBrd_nr_medium;
  int _iBrd_nr_low;
  int _iFW_version;
  int _iESW_version;
  double _rFlashDate;

  BoardInfo_tBase() : super() {
    this.noEventDo(() {
      this.iPS12_nr = 0;
      this.iBrd_nr_high = 0;
      this.iBrd_nr_medium = 0;
      this.iBrd_nr_low = 0;
      this.iFW_version = 0;
      this.iESW_version = 0;
      this.rFlashDate = 0.0;
    });
  }

  BoardInfo_tBase.json(Map m) : super.json(m) {
    this.noEventDo(() {
      this.subKind = m[Vocabulary.SUBKIND] != null
          ? m[Vocabulary.SUBKIND] as String
          : (m[Vocabulary.KIND] != Vocabulary.BoardInfo_t_CLASS
              ? m[Vocabulary.KIND]
              : null) as String;
      this.iPS12_nr = m[Vocabulary.iPS12_nr_DP] as int;
      this.iBrd_nr_high = m[Vocabulary.iBrd_nr_high_DP] as int;
      this.iBrd_nr_medium = m[Vocabulary.iBrd_nr_medium_DP] as int;
      this.iBrd_nr_low = m[Vocabulary.iBrd_nr_low_DP] as int;
      this.iFW_version = m[Vocabulary.iFW_version_DP] as int;
      this.iESW_version = m[Vocabulary.iESW_version_DP] as int;
      this.rFlashDate = (m[Vocabulary.rFlashDate_DP] as num).toDouble();
    });
  }

  static BoardInfo_t createFromJson(Map m) => BoardInfo_tBase.fromJson(m);
  static BoardInfo_t fromJson(Map m) {
    final kind = m[Vocabulary.KIND] as String;
    switch (kind) {
      case Vocabulary.BoardInfo_t_CLASS:
        return new BoardInfo_t.json(m);
      default:
        throw new ArgumentError.value(
            kind, "bad kind for class BoardInfo_t in fromJson constructor");
    }
  }

  String get kind => Vocabulary.BoardInfo_t_CLASS;
  int get iPS12_nr => _iPS12_nr;

  set iPS12_nr(int o) {
    if (o == _iPS12_nr) return;
    assert(o == null || o is int);
    var old = _iPS12_nr;
    _iPS12_nr = o;
    if (hasListener)
      this.sendChangeEvent(new base.PropertyChangedEvent(
          this, Vocabulary.iPS12_nr_DP, old, _iPS12_nr));
  }

  int get iBrd_nr_high => _iBrd_nr_high;

  set iBrd_nr_high(int o) {
    if (o == _iBrd_nr_high) return;
    assert(o == null || o is int);
    var old = _iBrd_nr_high;
    _iBrd_nr_high = o;
    if (hasListener)
      this.sendChangeEvent(new base.PropertyChangedEvent(
          this, Vocabulary.iBrd_nr_high_DP, old, _iBrd_nr_high));
  }

  int get iBrd_nr_medium => _iBrd_nr_medium;

  set iBrd_nr_medium(int o) {
    if (o == _iBrd_nr_medium) return;
    assert(o == null || o is int);
    var old = _iBrd_nr_medium;
    _iBrd_nr_medium = o;
    if (hasListener)
      this.sendChangeEvent(new base.PropertyChangedEvent(
          this, Vocabulary.iBrd_nr_medium_DP, old, _iBrd_nr_medium));
  }

  int get iBrd_nr_low => _iBrd_nr_low;

  set iBrd_nr_low(int o) {
    if (o == _iBrd_nr_low) return;
    assert(o == null || o is int);
    var old = _iBrd_nr_low;
    _iBrd_nr_low = o;
    if (hasListener)
      this.sendChangeEvent(new base.PropertyChangedEvent(
          this, Vocabulary.iBrd_nr_low_DP, old, _iBrd_nr_low));
  }

  int get iFW_version => _iFW_version;

  set iFW_version(int o) {
    if (o == _iFW_version) return;
    assert(o == null || o is int);
    var old = _iFW_version;
    _iFW_version = o;
    if (hasListener)
      this.sendChangeEvent(new base.PropertyChangedEvent(
          this, Vocabulary.iFW_version_DP, old, _iFW_version));
  }

  int get iESW_version => _iESW_version;

  set iESW_version(int o) {
    if (o == _iESW_version) return;
    assert(o == null || o is int);
    var old = _iESW_version;
    _iESW_version = o;
    if (hasListener)
      this.sendChangeEvent(new base.PropertyChangedEvent(
          this, Vocabulary.iESW_version_DP, old, _iESW_version));
  }

  double get rFlashDate => _rFlashDate;

  set rFlashDate(double o) {
    if (o == _rFlashDate) return;
    assert(o == null || o is double);
    var old = _rFlashDate;
    _rFlashDate = o;
    if (hasListener)
      this.sendChangeEvent(new base.PropertyChangedEvent(
          this, Vocabulary.rFlashDate_DP, old, _rFlashDate));
  }

  dynamic get(String name) {
    switch (name) {
      case Vocabulary.iPS12_nr_DP:
        return this.iPS12_nr;
      case Vocabulary.iBrd_nr_high_DP:
        return this.iBrd_nr_high;
      case Vocabulary.iBrd_nr_medium_DP:
        return this.iBrd_nr_medium;
      case Vocabulary.iBrd_nr_low_DP:
        return this.iBrd_nr_low;
      case Vocabulary.iFW_version_DP:
        return this.iFW_version;
      case Vocabulary.iESW_version_DP:
        return this.iESW_version;
      case Vocabulary.rFlashDate_DP:
        return this.rFlashDate;
      default:
        return super.get(name);
    }
  }

  set(String name, dynamic value) {
    switch (name) {
      case Vocabulary.iPS12_nr_DP:
        this.iPS12_nr = value as int;
        return;
      case Vocabulary.iBrd_nr_high_DP:
        this.iBrd_nr_high = value as int;
        return;
      case Vocabulary.iBrd_nr_medium_DP:
        this.iBrd_nr_medium = value as int;
        return;
      case Vocabulary.iBrd_nr_low_DP:
        this.iBrd_nr_low = value as int;
        return;
      case Vocabulary.iFW_version_DP:
        this.iFW_version = value as int;
        return;
      case Vocabulary.iESW_version_DP:
        this.iESW_version = value as int;
        return;
      case Vocabulary.rFlashDate_DP:
        this.rFlashDate = value as double;
        return;
      default:
        super.set(name, value);
    }
  }

  List<String> getPropertyNames() =>
      new List.from(super.getPropertyNames(), growable: true)
        ..addAll(PROPERTY_NAMES);

  BoardInfo_t copy() => new BoardInfo_t.json(this.toJson());
  Map toJson() {
    var m = super.toJson() as Map;
    m[Vocabulary.KIND] = Vocabulary.BoardInfo_t_CLASS;
    if (this.subKind != null && this.subKind != Vocabulary.BoardInfo_t_CLASS)
      m[Vocabulary.SUBKIND] = this.subKind;
    else
      m.remove(Vocabulary.SUBKIND);
    m[Vocabulary.iPS12_nr_DP] = iPS12_nr;
    m[Vocabulary.iBrd_nr_high_DP] = iBrd_nr_high;
    m[Vocabulary.iBrd_nr_medium_DP] = iBrd_nr_medium;
    m[Vocabulary.iBrd_nr_low_DP] = iBrd_nr_low;
    m[Vocabulary.iFW_version_DP] = iFW_version;
    m[Vocabulary.iESW_version_DP] = iESW_version;
    m[Vocabulary.rFlashDate_DP] = rFlashDate;
    return m;
  }
}
