part of ps_model_base;

class BrokenWellBitSetBase extends base.Base {
  static const List<String> PROPERTY_NAMES = const [Vocabulary.value_DP];
  double _value;

  BrokenWellBitSetBase() : super() {
    this.noEventDo(() {
      this.value = 0.0;
    });
  }

  BrokenWellBitSetBase.json(Map m) : super.json(m) {
    this.noEventDo(() {
      this.subKind = m[Vocabulary.SUBKIND] != null
          ? m[Vocabulary.SUBKIND] as String
          : (m[Vocabulary.KIND] != Vocabulary.BrokenWellBitSet_CLASS
              ? m[Vocabulary.KIND]
              : null) as String;
      this.value = (m[Vocabulary.value_DP] as num).toDouble();
    });
  }

  static BrokenWellBitSet createFromJson(Map m) =>
      BrokenWellBitSetBase.fromJson(m);
  static BrokenWellBitSet fromJson(Map m) {
    final kind = m[Vocabulary.KIND] as String;
    switch (kind) {
      case Vocabulary.BrokenWellBitSet_CLASS:
        return new BrokenWellBitSet.json(m);
      default:
        throw new ArgumentError.value(kind,
            "bad kind for class BrokenWellBitSet in fromJson constructor");
    }
  }

  String get kind => Vocabulary.BrokenWellBitSet_CLASS;
  double get value => _value;

  set value(double o) {
    if (o == _value) return;
    assert(o == null || o is double);
    var old = _value;
    _value = o;
    if (hasListener)
      this.sendChangeEvent(new base.PropertyChangedEvent(
          this, Vocabulary.value_DP, old, _value));
  }

  dynamic get(String name) {
    switch (name) {
      case Vocabulary.value_DP:
        return this.value;
      default:
        return super.get(name);
    }
  }

  set(String name, dynamic value) {
    switch (name) {
      case Vocabulary.value_DP:
        this.value = value as double;
        return;
      default:
        super.set(name, value);
    }
  }

  List<String> getPropertyNames() =>
      new List.from(super.getPropertyNames(), growable: true)
        ..addAll(PROPERTY_NAMES);

  BrokenWellBitSet copy() => new BrokenWellBitSet.json(this.toJson());
  Map toJson() {
    var m = super.toJson() as Map;
    m[Vocabulary.KIND] = Vocabulary.BrokenWellBitSet_CLASS;
    if (this.subKind != null &&
        this.subKind != Vocabulary.BrokenWellBitSet_CLASS)
      m[Vocabulary.SUBKIND] = this.subKind;
    else
      m.remove(Vocabulary.SUBKIND);
    m[Vocabulary.value_DP] = value;
    return m;
  }
}
