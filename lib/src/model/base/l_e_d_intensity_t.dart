part of ps_model_base;

class LEDIntensity_tBase extends base.Base {
  static const List<String> PROPERTY_NAMES = const [
    Vocabulary.uIntensity_LED1_DP,
    Vocabulary.uIntensity_LED2_DP,
    Vocabulary.uIntensity_LED3_DP
  ];
  int _uIntensity_LED1;
  int _uIntensity_LED2;
  int _uIntensity_LED3;

  LEDIntensity_tBase() : super() {
    this.noEventDo(() {
      this.uIntensity_LED1 = 0;
      this.uIntensity_LED2 = 0;
      this.uIntensity_LED3 = 0;
    });
  }

  LEDIntensity_tBase.json(Map m) : super.json(m) {
    this.noEventDo(() {
      this.subKind = m[Vocabulary.SUBKIND] != null
          ? m[Vocabulary.SUBKIND] as String
          : (m[Vocabulary.KIND] != Vocabulary.LEDIntensity_t_CLASS
              ? m[Vocabulary.KIND]
              : null) as String;
      this.uIntensity_LED1 = m[Vocabulary.uIntensity_LED1_DP] as int;
      this.uIntensity_LED2 = m[Vocabulary.uIntensity_LED2_DP] as int;
      this.uIntensity_LED3 = m[Vocabulary.uIntensity_LED3_DP] as int;
    });
  }

  static LEDIntensity_t createFromJson(Map m) => LEDIntensity_tBase.fromJson(m);
  static LEDIntensity_t fromJson(Map m) {
    final kind = m[Vocabulary.KIND] as String;
    switch (kind) {
      case Vocabulary.LEDIntensity_t_CLASS:
        return new LEDIntensity_t.json(m);
      default:
        throw new ArgumentError.value(
            kind, "bad kind for class LEDIntensity_t in fromJson constructor");
    }
  }

  String get kind => Vocabulary.LEDIntensity_t_CLASS;
  int get uIntensity_LED1 => _uIntensity_LED1;

  set uIntensity_LED1(int o) {
    if (o == _uIntensity_LED1) return;
    assert(o == null || o is int);
    var old = _uIntensity_LED1;
    _uIntensity_LED1 = o;
    if (hasListener)
      this.sendChangeEvent(new base.PropertyChangedEvent(
          this, Vocabulary.uIntensity_LED1_DP, old, _uIntensity_LED1));
  }

  int get uIntensity_LED2 => _uIntensity_LED2;

  set uIntensity_LED2(int o) {
    if (o == _uIntensity_LED2) return;
    assert(o == null || o is int);
    var old = _uIntensity_LED2;
    _uIntensity_LED2 = o;
    if (hasListener)
      this.sendChangeEvent(new base.PropertyChangedEvent(
          this, Vocabulary.uIntensity_LED2_DP, old, _uIntensity_LED2));
  }

  int get uIntensity_LED3 => _uIntensity_LED3;

  set uIntensity_LED3(int o) {
    if (o == _uIntensity_LED3) return;
    assert(o == null || o is int);
    var old = _uIntensity_LED3;
    _uIntensity_LED3 = o;
    if (hasListener)
      this.sendChangeEvent(new base.PropertyChangedEvent(
          this, Vocabulary.uIntensity_LED3_DP, old, _uIntensity_LED3));
  }

  dynamic get(String name) {
    switch (name) {
      case Vocabulary.uIntensity_LED1_DP:
        return this.uIntensity_LED1;
      case Vocabulary.uIntensity_LED2_DP:
        return this.uIntensity_LED2;
      case Vocabulary.uIntensity_LED3_DP:
        return this.uIntensity_LED3;
      default:
        return super.get(name);
    }
  }

  set(String name, dynamic value) {
    switch (name) {
      case Vocabulary.uIntensity_LED1_DP:
        this.uIntensity_LED1 = value as int;
        return;
      case Vocabulary.uIntensity_LED2_DP:
        this.uIntensity_LED2 = value as int;
        return;
      case Vocabulary.uIntensity_LED3_DP:
        this.uIntensity_LED3 = value as int;
        return;
      default:
        super.set(name, value);
    }
  }

  List<String> getPropertyNames() =>
      new List.from(super.getPropertyNames(), growable: true)
        ..addAll(PROPERTY_NAMES);

  LEDIntensity_t copy() => new LEDIntensity_t.json(this.toJson());
  Map toJson() {
    var m = super.toJson() as Map;
    m[Vocabulary.KIND] = Vocabulary.LEDIntensity_t_CLASS;
    if (this.subKind != null && this.subKind != Vocabulary.LEDIntensity_t_CLASS)
      m[Vocabulary.SUBKIND] = this.subKind;
    else
      m.remove(Vocabulary.SUBKIND);
    m[Vocabulary.uIntensity_LED1_DP] = uIntensity_LED1;
    m[Vocabulary.uIntensity_LED2_DP] = uIntensity_LED2;
    m[Vocabulary.uIntensity_LED3_DP] = uIntensity_LED3;
    return m;
  }
}
