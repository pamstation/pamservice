part of ps_model_base;

class Filter_LogicalPos_tBase extends base.Base {
  static const List<String> PROPERTY_NAMES = const [
    Vocabulary.rPosFilter1_DP,
    Vocabulary.rPosFilter2_DP,
    Vocabulary.rPosFilter3_DP
  ];
  double _rPosFilter1;
  double _rPosFilter2;
  double _rPosFilter3;

  Filter_LogicalPos_tBase() : super() {
    this.noEventDo(() {
      this.rPosFilter1 = 0.0;
      this.rPosFilter2 = 0.0;
      this.rPosFilter3 = 0.0;
    });
  }

  Filter_LogicalPos_tBase.json(Map m) : super.json(m) {
    this.noEventDo(() {
      this.subKind = m[Vocabulary.SUBKIND] != null
          ? m[Vocabulary.SUBKIND] as String
          : (m[Vocabulary.KIND] != Vocabulary.Filter_LogicalPos_t_CLASS
              ? m[Vocabulary.KIND]
              : null) as String;
      this.rPosFilter1 = (m[Vocabulary.rPosFilter1_DP] as num).toDouble();
      this.rPosFilter2 = (m[Vocabulary.rPosFilter2_DP] as num).toDouble();
      this.rPosFilter3 = (m[Vocabulary.rPosFilter3_DP] as num).toDouble();
    });
  }

  static Filter_LogicalPos_t createFromJson(Map m) =>
      Filter_LogicalPos_tBase.fromJson(m);
  static Filter_LogicalPos_t fromJson(Map m) {
    final kind = m[Vocabulary.KIND] as String;
    switch (kind) {
      case Vocabulary.Filter_LogicalPos_t_CLASS:
        return new Filter_LogicalPos_t.json(m);
      default:
        throw new ArgumentError.value(kind,
            "bad kind for class Filter_LogicalPos_t in fromJson constructor");
    }
  }

  String get kind => Vocabulary.Filter_LogicalPos_t_CLASS;
  double get rPosFilter1 => _rPosFilter1;

  set rPosFilter1(double o) {
    if (o == _rPosFilter1) return;
    assert(o == null || o is double);
    var old = _rPosFilter1;
    _rPosFilter1 = o;
    if (hasListener)
      this.sendChangeEvent(new base.PropertyChangedEvent(
          this, Vocabulary.rPosFilter1_DP, old, _rPosFilter1));
  }

  double get rPosFilter2 => _rPosFilter2;

  set rPosFilter2(double o) {
    if (o == _rPosFilter2) return;
    assert(o == null || o is double);
    var old = _rPosFilter2;
    _rPosFilter2 = o;
    if (hasListener)
      this.sendChangeEvent(new base.PropertyChangedEvent(
          this, Vocabulary.rPosFilter2_DP, old, _rPosFilter2));
  }

  double get rPosFilter3 => _rPosFilter3;

  set rPosFilter3(double o) {
    if (o == _rPosFilter3) return;
    assert(o == null || o is double);
    var old = _rPosFilter3;
    _rPosFilter3 = o;
    if (hasListener)
      this.sendChangeEvent(new base.PropertyChangedEvent(
          this, Vocabulary.rPosFilter3_DP, old, _rPosFilter3));
  }

  dynamic get(String name) {
    switch (name) {
      case Vocabulary.rPosFilter1_DP:
        return this.rPosFilter1;
      case Vocabulary.rPosFilter2_DP:
        return this.rPosFilter2;
      case Vocabulary.rPosFilter3_DP:
        return this.rPosFilter3;
      default:
        return super.get(name);
    }
  }

  set(String name, dynamic value) {
    switch (name) {
      case Vocabulary.rPosFilter1_DP:
        this.rPosFilter1 = value as double;
        return;
      case Vocabulary.rPosFilter2_DP:
        this.rPosFilter2 = value as double;
        return;
      case Vocabulary.rPosFilter3_DP:
        this.rPosFilter3 = value as double;
        return;
      default:
        super.set(name, value);
    }
  }

  List<String> getPropertyNames() =>
      new List.from(super.getPropertyNames(), growable: true)
        ..addAll(PROPERTY_NAMES);

  Filter_LogicalPos_t copy() => new Filter_LogicalPos_t.json(this.toJson());
  Map toJson() {
    var m = super.toJson() as Map;
    m[Vocabulary.KIND] = Vocabulary.Filter_LogicalPos_t_CLASS;
    if (this.subKind != null &&
        this.subKind != Vocabulary.Filter_LogicalPos_t_CLASS)
      m[Vocabulary.SUBKIND] = this.subKind;
    else
      m.remove(Vocabulary.SUBKIND);
    m[Vocabulary.rPosFilter1_DP] = rPosFilter1;
    m[Vocabulary.rPosFilter2_DP] = rPosFilter2;
    m[Vocabulary.rPosFilter3_DP] = rPosFilter3;
    return m;
  }
}
