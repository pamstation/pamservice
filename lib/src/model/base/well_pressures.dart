part of ps_model_base;

class WellPressuresBase extends base.Base {
  static const List<String> PROPERTY_NAMES = const [
    Vocabulary.wells_DP,
    Vocabulary.pressures_DP,
    Vocabulary.aspiratePumpStatus_DP,
    Vocabulary.aspiratePressure_DP
  ];
  base.ListChangedBase<int> _wells;
  base.ListChangedBase<double> _pressures;
  bool _aspiratePumpStatus;
  double _aspiratePressure;

  WellPressuresBase() : super() {
    this.noEventDo(() {
      this._wells = new base.ListChangedBase<int>();
      this._wells.parent = this;
      this._pressures = new base.ListChangedBase<double>();
      this._pressures.parent = this;
      this.aspiratePumpStatus = true;
      this.aspiratePressure = 0.0;
    });
  }

  WellPressuresBase.json(Map m) : super.json(m) {
    this.noEventDo(() {
      this.subKind = m[Vocabulary.SUBKIND] != null
          ? m[Vocabulary.SUBKIND] as String
          : (m[Vocabulary.KIND] != Vocabulary.WellPressures_CLASS
              ? m[Vocabulary.KIND]
              : null) as String;
      this._wells = new base.ListChangedBase<int>(
          (m[Vocabulary.wells_DP] as List).cast());
      this._wells.parent = this;
      this._pressures = new base.ListChangedBase<double>(
          (m[Vocabulary.pressures_DP] as List).cast());
      this._pressures.parent = this;
      this.aspiratePumpStatus = m[Vocabulary.aspiratePumpStatus_DP] as bool;
      this.aspiratePressure =
          (m[Vocabulary.aspiratePressure_DP] as num).toDouble();
    });
  }

  static WellPressures createFromJson(Map m) => WellPressuresBase.fromJson(m);
  static WellPressures fromJson(Map m) {
    final kind = m[Vocabulary.KIND] as String;
    switch (kind) {
      case Vocabulary.WellPressures_CLASS:
        return new WellPressures.json(m);
      default:
        throw new ArgumentError.value(
            kind, "bad kind for class WellPressures in fromJson constructor");
    }
  }

  String get kind => Vocabulary.WellPressures_CLASS;
  base.ListChangedBase<int> get wells => _wells;

  base.ListChangedBase<double> get pressures => _pressures;

  bool get aspiratePumpStatus => _aspiratePumpStatus;

  set aspiratePumpStatus(bool o) {
    if (o == _aspiratePumpStatus) return;
    assert(o == null || o is bool);
    var old = _aspiratePumpStatus;
    _aspiratePumpStatus = o;
    if (hasListener)
      this.sendChangeEvent(new base.PropertyChangedEvent(
          this, Vocabulary.aspiratePumpStatus_DP, old, _aspiratePumpStatus));
  }

  double get aspiratePressure => _aspiratePressure;

  set aspiratePressure(double o) {
    if (o == _aspiratePressure) return;
    assert(o == null || o is double);
    var old = _aspiratePressure;
    _aspiratePressure = o;
    if (hasListener)
      this.sendChangeEvent(new base.PropertyChangedEvent(
          this, Vocabulary.aspiratePressure_DP, old, _aspiratePressure));
  }

  dynamic get(String name) {
    switch (name) {
      case Vocabulary.wells_DP:
        return this.wells;
      case Vocabulary.pressures_DP:
        return this.pressures;
      case Vocabulary.aspiratePumpStatus_DP:
        return this.aspiratePumpStatus;
      case Vocabulary.aspiratePressure_DP:
        return this.aspiratePressure;
      default:
        return super.get(name);
    }
  }

  set(String name, dynamic value) {
    switch (name) {
      case Vocabulary.wells_DP:
        _wells
          ..clear()
          ..addAll(value as Iterable<int>);
        return;
      case Vocabulary.pressures_DP:
        _pressures
          ..clear()
          ..addAll(value as Iterable<double>);
        return;
      case Vocabulary.aspiratePumpStatus_DP:
        this.aspiratePumpStatus = value as bool;
        return;
      case Vocabulary.aspiratePressure_DP:
        this.aspiratePressure = value as double;
        return;
      default:
        super.set(name, value);
    }
  }

  List<String> getPropertyNames() =>
      new List.from(super.getPropertyNames(), growable: true)
        ..addAll(PROPERTY_NAMES);

  WellPressures copy() => new WellPressures.json(this.toJson());
  Map toJson() {
    var m = super.toJson() as Map;
    m[Vocabulary.KIND] = Vocabulary.WellPressures_CLASS;
    if (this.subKind != null && this.subKind != Vocabulary.WellPressures_CLASS)
      m[Vocabulary.SUBKIND] = this.subKind;
    else
      m.remove(Vocabulary.SUBKIND);
    m[Vocabulary.wells_DP] = wells;
    m[Vocabulary.pressures_DP] = pressures;
    m[Vocabulary.aspiratePumpStatus_DP] = aspiratePumpStatus;
    m[Vocabulary.aspiratePressure_DP] = aspiratePressure;
    return m;
  }
}
