part of ps_model_base;

class DispenseHeadHeater_Settings_tBase extends base.Base {
  static const List<String> PROPERTY_NAMES = const [
    Vocabulary.rGain_DP,
    Vocabulary.rGain2_DP,
    Vocabulary.rOffset_DP
  ];
  double _rGain;
  double _rGain2;
  double _rOffset;

  DispenseHeadHeater_Settings_tBase() : super() {
    this.noEventDo(() {
      this.rGain = 0.0;
      this.rGain2 = 0.0;
      this.rOffset = 0.0;
    });
  }

  DispenseHeadHeater_Settings_tBase.json(Map m) : super.json(m) {
    this.noEventDo(() {
      this.subKind = m[Vocabulary.SUBKIND] != null
          ? m[Vocabulary.SUBKIND] as String
          : (m[Vocabulary.KIND] !=
                  Vocabulary.DispenseHeadHeater_Settings_t_CLASS
              ? m[Vocabulary.KIND]
              : null) as String;
      this.rGain = (m[Vocabulary.rGain_DP] as num).toDouble();
      this.rGain2 = (m[Vocabulary.rGain2_DP] as num).toDouble();
      this.rOffset = (m[Vocabulary.rOffset_DP] as num).toDouble();
    });
  }

  static DispenseHeadHeater_Settings_t createFromJson(Map m) =>
      DispenseHeadHeater_Settings_tBase.fromJson(m);
  static DispenseHeadHeater_Settings_t fromJson(Map m) {
    final kind = m[Vocabulary.KIND] as String;
    switch (kind) {
      case Vocabulary.DispenseHeadHeater_Settings_t_CLASS:
        return new DispenseHeadHeater_Settings_t.json(m);
      default:
        throw new ArgumentError.value(kind,
            "bad kind for class DispenseHeadHeater_Settings_t in fromJson constructor");
    }
  }

  String get kind => Vocabulary.DispenseHeadHeater_Settings_t_CLASS;
  double get rGain => _rGain;

  set rGain(double o) {
    if (o == _rGain) return;
    assert(o == null || o is double);
    var old = _rGain;
    _rGain = o;
    if (hasListener)
      this.sendChangeEvent(new base.PropertyChangedEvent(
          this, Vocabulary.rGain_DP, old, _rGain));
  }

  double get rGain2 => _rGain2;

  set rGain2(double o) {
    if (o == _rGain2) return;
    assert(o == null || o is double);
    var old = _rGain2;
    _rGain2 = o;
    if (hasListener)
      this.sendChangeEvent(new base.PropertyChangedEvent(
          this, Vocabulary.rGain2_DP, old, _rGain2));
  }

  double get rOffset => _rOffset;

  set rOffset(double o) {
    if (o == _rOffset) return;
    assert(o == null || o is double);
    var old = _rOffset;
    _rOffset = o;
    if (hasListener)
      this.sendChangeEvent(new base.PropertyChangedEvent(
          this, Vocabulary.rOffset_DP, old, _rOffset));
  }

  dynamic get(String name) {
    switch (name) {
      case Vocabulary.rGain_DP:
        return this.rGain;
      case Vocabulary.rGain2_DP:
        return this.rGain2;
      case Vocabulary.rOffset_DP:
        return this.rOffset;
      default:
        return super.get(name);
    }
  }

  set(String name, dynamic value) {
    switch (name) {
      case Vocabulary.rGain_DP:
        this.rGain = value as double;
        return;
      case Vocabulary.rGain2_DP:
        this.rGain2 = value as double;
        return;
      case Vocabulary.rOffset_DP:
        this.rOffset = value as double;
        return;
      default:
        super.set(name, value);
    }
  }

  List<String> getPropertyNames() =>
      new List.from(super.getPropertyNames(), growable: true)
        ..addAll(PROPERTY_NAMES);

  DispenseHeadHeater_Settings_t copy() =>
      new DispenseHeadHeater_Settings_t.json(this.toJson());
  Map toJson() {
    var m = super.toJson() as Map;
    m[Vocabulary.KIND] = Vocabulary.DispenseHeadHeater_Settings_t_CLASS;
    if (this.subKind != null &&
        this.subKind != Vocabulary.DispenseHeadHeater_Settings_t_CLASS)
      m[Vocabulary.SUBKIND] = this.subKind;
    else
      m.remove(Vocabulary.SUBKIND);
    m[Vocabulary.rGain_DP] = rGain;
    m[Vocabulary.rGain2_DP] = rGain2;
    m[Vocabulary.rOffset_DP] = rOffset;
    return m;
  }
}
