part of ps_model_base;

class VersionBase extends base.Base {
  static const List<String> PROPERTY_NAMES = const [
    Vocabulary.name_DP,
    Vocabulary.major_DP,
    Vocabulary.minor_DP,
    Vocabulary.patch_DP,
    Vocabulary.tag_DP,
    Vocabulary.date_DP,
    Vocabulary.commit_DP
  ];
  String _name;
  int _major;
  int _minor;
  int _patch;
  String _tag;
  String _date;
  String _commit;

  VersionBase() : super() {
    this.noEventDo(() {
      this.name = "";
      this.major = 0;
      this.minor = 0;
      this.patch = 0;
      this.tag = "";
      this.date = "";
      this.commit = "";
    });
  }

  VersionBase.json(Map m) : super.json(m) {
    this.noEventDo(() {
      this.subKind = m[Vocabulary.SUBKIND] != null
          ? m[Vocabulary.SUBKIND] as String
          : (m[Vocabulary.KIND] != Vocabulary.Version_CLASS
              ? m[Vocabulary.KIND]
              : null) as String;
      this.name = m[Vocabulary.name_DP] as String;
      this.major = m[Vocabulary.major_DP] as int;
      this.minor = m[Vocabulary.minor_DP] as int;
      this.patch = m[Vocabulary.patch_DP] as int;
      this.tag = m[Vocabulary.tag_DP] as String;
      this.date = m[Vocabulary.date_DP] as String;
      this.commit = m[Vocabulary.commit_DP] as String;
    });
  }

  static Version createFromJson(Map m) => VersionBase.fromJson(m);
  static Version fromJson(Map m) {
    final kind = m[Vocabulary.KIND] as String;
    switch (kind) {
      case Vocabulary.Version_CLASS:
        return new Version.json(m);
      default:
        throw new ArgumentError.value(
            kind, "bad kind for class Version in fromJson constructor");
    }
  }

  String get kind => Vocabulary.Version_CLASS;
  String get name => _name;

  set name(String o) {
    if (o == _name) return;
    assert(o == null || o is String);
    var old = _name;
    _name = o;
    if (hasListener)
      this.sendChangeEvent(
          new base.PropertyChangedEvent(this, Vocabulary.name_DP, old, _name));
  }

  int get major => _major;

  set major(int o) {
    if (o == _major) return;
    assert(o == null || o is int);
    var old = _major;
    _major = o;
    if (hasListener)
      this.sendChangeEvent(new base.PropertyChangedEvent(
          this, Vocabulary.major_DP, old, _major));
  }

  int get minor => _minor;

  set minor(int o) {
    if (o == _minor) return;
    assert(o == null || o is int);
    var old = _minor;
    _minor = o;
    if (hasListener)
      this.sendChangeEvent(new base.PropertyChangedEvent(
          this, Vocabulary.minor_DP, old, _minor));
  }

  int get patch => _patch;

  set patch(int o) {
    if (o == _patch) return;
    assert(o == null || o is int);
    var old = _patch;
    _patch = o;
    if (hasListener)
      this.sendChangeEvent(new base.PropertyChangedEvent(
          this, Vocabulary.patch_DP, old, _patch));
  }

  String get tag => _tag;

  set tag(String o) {
    if (o == _tag) return;
    assert(o == null || o is String);
    var old = _tag;
    _tag = o;
    if (hasListener)
      this.sendChangeEvent(
          new base.PropertyChangedEvent(this, Vocabulary.tag_DP, old, _tag));
  }

  String get date => _date;

  set date(String o) {
    if (o == _date) return;
    assert(o == null || o is String);
    var old = _date;
    _date = o;
    if (hasListener)
      this.sendChangeEvent(
          new base.PropertyChangedEvent(this, Vocabulary.date_DP, old, _date));
  }

  String get commit => _commit;

  set commit(String o) {
    if (o == _commit) return;
    assert(o == null || o is String);
    var old = _commit;
    _commit = o;
    if (hasListener)
      this.sendChangeEvent(new base.PropertyChangedEvent(
          this, Vocabulary.commit_DP, old, _commit));
  }

  dynamic get(String name) {
    switch (name) {
      case Vocabulary.name_DP:
        return this.name;
      case Vocabulary.major_DP:
        return this.major;
      case Vocabulary.minor_DP:
        return this.minor;
      case Vocabulary.patch_DP:
        return this.patch;
      case Vocabulary.tag_DP:
        return this.tag;
      case Vocabulary.date_DP:
        return this.date;
      case Vocabulary.commit_DP:
        return this.commit;
      default:
        return super.get(name);
    }
  }

  set(String name, dynamic value) {
    switch (name) {
      case Vocabulary.name_DP:
        this.name = value as String;
        return;
      case Vocabulary.major_DP:
        this.major = value as int;
        return;
      case Vocabulary.minor_DP:
        this.minor = value as int;
        return;
      case Vocabulary.patch_DP:
        this.patch = value as int;
        return;
      case Vocabulary.tag_DP:
        this.tag = value as String;
        return;
      case Vocabulary.date_DP:
        this.date = value as String;
        return;
      case Vocabulary.commit_DP:
        this.commit = value as String;
        return;
      default:
        super.set(name, value);
    }
  }

  List<String> getPropertyNames() =>
      new List.from(super.getPropertyNames(), growable: true)
        ..addAll(PROPERTY_NAMES);

  Version copy() => new Version.json(this.toJson());
  Map toJson() {
    var m = super.toJson() as Map;
    m[Vocabulary.KIND] = Vocabulary.Version_CLASS;
    if (this.subKind != null && this.subKind != Vocabulary.Version_CLASS)
      m[Vocabulary.SUBKIND] = this.subKind;
    else
      m.remove(Vocabulary.SUBKIND);
    m[Vocabulary.name_DP] = name;
    m[Vocabulary.major_DP] = major;
    m[Vocabulary.minor_DP] = minor;
    m[Vocabulary.patch_DP] = patch;
    m[Vocabulary.tag_DP] = tag;
    m[Vocabulary.date_DP] = date;
    m[Vocabulary.commit_DP] = commit;
    return m;
  }
}
