part of ps_model_base;

class ControllableBase extends base.Base {
  static const List<String> PROPERTY_NAMES = const [
    Vocabulary.name_DP,
    Vocabulary.enabled_DP,
    Vocabulary.error_status_DP,
    Vocabulary.error_number_DP,
    Vocabulary.target_value_DP,
    Vocabulary.actual_value_DP,
    Vocabulary.min_value_DP,
    Vocabulary.max_value_DP
  ];
  String _name;
  bool _enabled;
  bool _error_status;
  int _error_number;
  double _target_value;
  double _actual_value;
  double _min_value;
  double _max_value;

  ControllableBase() : super() {
    this.noEventDo(() {
      this.name = "";
      this.enabled = true;
      this.error_status = true;
      this.error_number = 0;
      this.target_value = 0.0;
      this.actual_value = 0.0;
      this.min_value = 0.0;
      this.max_value = 0.0;
    });
  }

  ControllableBase.json(Map m) : super.json(m) {
    this.noEventDo(() {
      this.subKind = m[Vocabulary.SUBKIND] != null
          ? m[Vocabulary.SUBKIND] as String
          : (m[Vocabulary.KIND] != Vocabulary.Controllable_CLASS
              ? m[Vocabulary.KIND]
              : null) as String;
      this.name = m[Vocabulary.name_DP] as String;
      this.enabled = m[Vocabulary.enabled_DP] as bool;
      this.error_status = m[Vocabulary.error_status_DP] as bool;
      this.error_number = m[Vocabulary.error_number_DP] as int;
      this.target_value = (m[Vocabulary.target_value_DP] as num).toDouble();
      this.actual_value = (m[Vocabulary.actual_value_DP] as num).toDouble();
      this.min_value = (m[Vocabulary.min_value_DP] as num).toDouble();
      this.max_value = (m[Vocabulary.max_value_DP] as num).toDouble();
    });
  }

  static Controllable createFromJson(Map m) => ControllableBase.fromJson(m);
  static Controllable fromJson(Map m) {
    final kind = m[Vocabulary.KIND] as String;
    switch (kind) {
      case Vocabulary.Controllable_CLASS:
        return new Controllable.json(m);
      default:
        throw new ArgumentError.value(
            kind, "bad kind for class Controllable in fromJson constructor");
    }
  }

  String get kind => Vocabulary.Controllable_CLASS;
  String get name => _name;

  set name(String o) {
    if (o == _name) return;
    assert(o == null || o is String);
    var old = _name;
    _name = o;
    if (hasListener)
      this.sendChangeEvent(
          new base.PropertyChangedEvent(this, Vocabulary.name_DP, old, _name));
  }

  bool get enabled => _enabled;

  set enabled(bool o) {
    if (o == _enabled) return;
    assert(o == null || o is bool);
    var old = _enabled;
    _enabled = o;
    if (hasListener)
      this.sendChangeEvent(new base.PropertyChangedEvent(
          this, Vocabulary.enabled_DP, old, _enabled));
  }

  bool get error_status => _error_status;

  set error_status(bool o) {
    if (o == _error_status) return;
    assert(o == null || o is bool);
    var old = _error_status;
    _error_status = o;
    if (hasListener)
      this.sendChangeEvent(new base.PropertyChangedEvent(
          this, Vocabulary.error_status_DP, old, _error_status));
  }

  int get error_number => _error_number;

  set error_number(int o) {
    if (o == _error_number) return;
    assert(o == null || o is int);
    var old = _error_number;
    _error_number = o;
    if (hasListener)
      this.sendChangeEvent(new base.PropertyChangedEvent(
          this, Vocabulary.error_number_DP, old, _error_number));
  }

  double get target_value => _target_value;

  set target_value(double o) {
    if (o == _target_value) return;
    assert(o == null || o is double);
    var old = _target_value;
    _target_value = o;
    if (hasListener)
      this.sendChangeEvent(new base.PropertyChangedEvent(
          this, Vocabulary.target_value_DP, old, _target_value));
  }

  double get actual_value => _actual_value;

  set actual_value(double o) {
    if (o == _actual_value) return;
    assert(o == null || o is double);
    var old = _actual_value;
    _actual_value = o;
    if (hasListener)
      this.sendChangeEvent(new base.PropertyChangedEvent(
          this, Vocabulary.actual_value_DP, old, _actual_value));
  }

  double get min_value => _min_value;

  set min_value(double o) {
    if (o == _min_value) return;
    assert(o == null || o is double);
    var old = _min_value;
    _min_value = o;
    if (hasListener)
      this.sendChangeEvent(new base.PropertyChangedEvent(
          this, Vocabulary.min_value_DP, old, _min_value));
  }

  double get max_value => _max_value;

  set max_value(double o) {
    if (o == _max_value) return;
    assert(o == null || o is double);
    var old = _max_value;
    _max_value = o;
    if (hasListener)
      this.sendChangeEvent(new base.PropertyChangedEvent(
          this, Vocabulary.max_value_DP, old, _max_value));
  }

  dynamic get(String name) {
    switch (name) {
      case Vocabulary.name_DP:
        return this.name;
      case Vocabulary.enabled_DP:
        return this.enabled;
      case Vocabulary.error_status_DP:
        return this.error_status;
      case Vocabulary.error_number_DP:
        return this.error_number;
      case Vocabulary.target_value_DP:
        return this.target_value;
      case Vocabulary.actual_value_DP:
        return this.actual_value;
      case Vocabulary.min_value_DP:
        return this.min_value;
      case Vocabulary.max_value_DP:
        return this.max_value;
      default:
        return super.get(name);
    }
  }

  set(String name, dynamic value) {
    switch (name) {
      case Vocabulary.name_DP:
        this.name = value as String;
        return;
      case Vocabulary.enabled_DP:
        this.enabled = value as bool;
        return;
      case Vocabulary.error_status_DP:
        this.error_status = value as bool;
        return;
      case Vocabulary.error_number_DP:
        this.error_number = value as int;
        return;
      case Vocabulary.target_value_DP:
        this.target_value = value as double;
        return;
      case Vocabulary.actual_value_DP:
        this.actual_value = value as double;
        return;
      case Vocabulary.min_value_DP:
        this.min_value = value as double;
        return;
      case Vocabulary.max_value_DP:
        this.max_value = value as double;
        return;
      default:
        super.set(name, value);
    }
  }

  List<String> getPropertyNames() =>
      new List.from(super.getPropertyNames(), growable: true)
        ..addAll(PROPERTY_NAMES);

  Controllable copy() => new Controllable.json(this.toJson());
  Map toJson() {
    var m = super.toJson() as Map;
    m[Vocabulary.KIND] = Vocabulary.Controllable_CLASS;
    if (this.subKind != null && this.subKind != Vocabulary.Controllable_CLASS)
      m[Vocabulary.SUBKIND] = this.subKind;
    else
      m.remove(Vocabulary.SUBKIND);
    m[Vocabulary.name_DP] = name;
    m[Vocabulary.enabled_DP] = enabled;
    m[Vocabulary.error_status_DP] = error_status;
    m[Vocabulary.error_number_DP] = error_number;
    m[Vocabulary.target_value_DP] = target_value;
    m[Vocabulary.actual_value_DP] = actual_value;
    m[Vocabulary.min_value_DP] = min_value;
    m[Vocabulary.max_value_DP] = max_value;
    return m;
  }
}
