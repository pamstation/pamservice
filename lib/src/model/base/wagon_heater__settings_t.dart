part of ps_model_base;

class WagonHeater_Settings_tBase extends base.Base {
  static const List<String> PROPERTY_NAMES = const [
    Vocabulary.rBottomGain_DP,
    Vocabulary.rCoverGain_DP,
    Vocabulary.rBottomGain2_DP,
    Vocabulary.rCoverGain2_DP,
    Vocabulary.rOffset_DP,
    Vocabulary.rCoverBottomOffset_DP,
    Vocabulary.rMinCoverTemp_DP
  ];
  double _rBottomGain;
  double _rCoverGain;
  double _rBottomGain2;
  double _rCoverGain2;
  double _rOffset;
  double _rCoverBottomOffset;
  double _rMinCoverTemp;

  WagonHeater_Settings_tBase() : super() {
    this.noEventDo(() {
      this.rBottomGain = 0.0;
      this.rCoverGain = 0.0;
      this.rBottomGain2 = 0.0;
      this.rCoverGain2 = 0.0;
      this.rOffset = 0.0;
      this.rCoverBottomOffset = 0.0;
      this.rMinCoverTemp = 0.0;
    });
  }

  WagonHeater_Settings_tBase.json(Map m) : super.json(m) {
    this.noEventDo(() {
      this.subKind = m[Vocabulary.SUBKIND] != null
          ? m[Vocabulary.SUBKIND] as String
          : (m[Vocabulary.KIND] != Vocabulary.WagonHeater_Settings_t_CLASS
              ? m[Vocabulary.KIND]
              : null) as String;
      this.rBottomGain = (m[Vocabulary.rBottomGain_DP] as num).toDouble();
      this.rCoverGain = (m[Vocabulary.rCoverGain_DP] as num).toDouble();
      this.rBottomGain2 = (m[Vocabulary.rBottomGain2_DP] as num).toDouble();
      this.rCoverGain2 = (m[Vocabulary.rCoverGain2_DP] as num).toDouble();
      this.rOffset = (m[Vocabulary.rOffset_DP] as num).toDouble();
      this.rCoverBottomOffset =
          (m[Vocabulary.rCoverBottomOffset_DP] as num).toDouble();
      this.rMinCoverTemp = (m[Vocabulary.rMinCoverTemp_DP] as num).toDouble();
    });
  }

  static WagonHeater_Settings_t createFromJson(Map m) =>
      WagonHeater_Settings_tBase.fromJson(m);
  static WagonHeater_Settings_t fromJson(Map m) {
    final kind = m[Vocabulary.KIND] as String;
    switch (kind) {
      case Vocabulary.WagonHeater_Settings_t_CLASS:
        return new WagonHeater_Settings_t.json(m);
      default:
        throw new ArgumentError.value(kind,
            "bad kind for class WagonHeater_Settings_t in fromJson constructor");
    }
  }

  String get kind => Vocabulary.WagonHeater_Settings_t_CLASS;
  double get rBottomGain => _rBottomGain;

  set rBottomGain(double o) {
    if (o == _rBottomGain) return;
    assert(o == null || o is double);
    var old = _rBottomGain;
    _rBottomGain = o;
    if (hasListener)
      this.sendChangeEvent(new base.PropertyChangedEvent(
          this, Vocabulary.rBottomGain_DP, old, _rBottomGain));
  }

  double get rCoverGain => _rCoverGain;

  set rCoverGain(double o) {
    if (o == _rCoverGain) return;
    assert(o == null || o is double);
    var old = _rCoverGain;
    _rCoverGain = o;
    if (hasListener)
      this.sendChangeEvent(new base.PropertyChangedEvent(
          this, Vocabulary.rCoverGain_DP, old, _rCoverGain));
  }

  double get rBottomGain2 => _rBottomGain2;

  set rBottomGain2(double o) {
    if (o == _rBottomGain2) return;
    assert(o == null || o is double);
    var old = _rBottomGain2;
    _rBottomGain2 = o;
    if (hasListener)
      this.sendChangeEvent(new base.PropertyChangedEvent(
          this, Vocabulary.rBottomGain2_DP, old, _rBottomGain2));
  }

  double get rCoverGain2 => _rCoverGain2;

  set rCoverGain2(double o) {
    if (o == _rCoverGain2) return;
    assert(o == null || o is double);
    var old = _rCoverGain2;
    _rCoverGain2 = o;
    if (hasListener)
      this.sendChangeEvent(new base.PropertyChangedEvent(
          this, Vocabulary.rCoverGain2_DP, old, _rCoverGain2));
  }

  double get rOffset => _rOffset;

  set rOffset(double o) {
    if (o == _rOffset) return;
    assert(o == null || o is double);
    var old = _rOffset;
    _rOffset = o;
    if (hasListener)
      this.sendChangeEvent(new base.PropertyChangedEvent(
          this, Vocabulary.rOffset_DP, old, _rOffset));
  }

  double get rCoverBottomOffset => _rCoverBottomOffset;

  set rCoverBottomOffset(double o) {
    if (o == _rCoverBottomOffset) return;
    assert(o == null || o is double);
    var old = _rCoverBottomOffset;
    _rCoverBottomOffset = o;
    if (hasListener)
      this.sendChangeEvent(new base.PropertyChangedEvent(
          this, Vocabulary.rCoverBottomOffset_DP, old, _rCoverBottomOffset));
  }

  double get rMinCoverTemp => _rMinCoverTemp;

  set rMinCoverTemp(double o) {
    if (o == _rMinCoverTemp) return;
    assert(o == null || o is double);
    var old = _rMinCoverTemp;
    _rMinCoverTemp = o;
    if (hasListener)
      this.sendChangeEvent(new base.PropertyChangedEvent(
          this, Vocabulary.rMinCoverTemp_DP, old, _rMinCoverTemp));
  }

  dynamic get(String name) {
    switch (name) {
      case Vocabulary.rBottomGain_DP:
        return this.rBottomGain;
      case Vocabulary.rCoverGain_DP:
        return this.rCoverGain;
      case Vocabulary.rBottomGain2_DP:
        return this.rBottomGain2;
      case Vocabulary.rCoverGain2_DP:
        return this.rCoverGain2;
      case Vocabulary.rOffset_DP:
        return this.rOffset;
      case Vocabulary.rCoverBottomOffset_DP:
        return this.rCoverBottomOffset;
      case Vocabulary.rMinCoverTemp_DP:
        return this.rMinCoverTemp;
      default:
        return super.get(name);
    }
  }

  set(String name, dynamic value) {
    switch (name) {
      case Vocabulary.rBottomGain_DP:
        this.rBottomGain = value as double;
        return;
      case Vocabulary.rCoverGain_DP:
        this.rCoverGain = value as double;
        return;
      case Vocabulary.rBottomGain2_DP:
        this.rBottomGain2 = value as double;
        return;
      case Vocabulary.rCoverGain2_DP:
        this.rCoverGain2 = value as double;
        return;
      case Vocabulary.rOffset_DP:
        this.rOffset = value as double;
        return;
      case Vocabulary.rCoverBottomOffset_DP:
        this.rCoverBottomOffset = value as double;
        return;
      case Vocabulary.rMinCoverTemp_DP:
        this.rMinCoverTemp = value as double;
        return;
      default:
        super.set(name, value);
    }
  }

  List<String> getPropertyNames() =>
      new List.from(super.getPropertyNames(), growable: true)
        ..addAll(PROPERTY_NAMES);

  WagonHeater_Settings_t copy() =>
      new WagonHeater_Settings_t.json(this.toJson());
  Map toJson() {
    var m = super.toJson() as Map;
    m[Vocabulary.KIND] = Vocabulary.WagonHeater_Settings_t_CLASS;
    if (this.subKind != null &&
        this.subKind != Vocabulary.WagonHeater_Settings_t_CLASS)
      m[Vocabulary.SUBKIND] = this.subKind;
    else
      m.remove(Vocabulary.SUBKIND);
    m[Vocabulary.rBottomGain_DP] = rBottomGain;
    m[Vocabulary.rCoverGain_DP] = rCoverGain;
    m[Vocabulary.rBottomGain2_DP] = rBottomGain2;
    m[Vocabulary.rCoverGain2_DP] = rCoverGain2;
    m[Vocabulary.rOffset_DP] = rOffset;
    m[Vocabulary.rCoverBottomOffset_DP] = rCoverBottomOffset;
    m[Vocabulary.rMinCoverTemp_DP] = rMinCoverTemp;
    return m;
  }
}
