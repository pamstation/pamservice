part of ps_model_base;

class Sensor_Calibration_Data_tBase extends base.Base {
  static const List<String> PROPERTY_NAMES = const [
    Vocabulary.eSensorObj_DP,
    Vocabulary.rSysOffset_DP,
    Vocabulary.rSysGain_DP,
    Vocabulary.rSysGain2_DP,
    Vocabulary.rSensOffset_DP,
    Vocabulary.rSensGain_DP,
    Vocabulary.rSensGain2_DP,
    Vocabulary.rADConversion_DP,
    Vocabulary.rTsample_DP
  ];
  int _eSensorObj;
  double _rSysOffset;
  double _rSysGain;
  double _rSysGain2;
  double _rSensOffset;
  double _rSensGain;
  double _rSensGain2;
  double _rADConversion;
  double _rTsample;

  Sensor_Calibration_Data_tBase() : super() {
    this.noEventDo(() {
      this.eSensorObj = 0;
      this.rSysOffset = 0.0;
      this.rSysGain = 0.0;
      this.rSysGain2 = 0.0;
      this.rSensOffset = 0.0;
      this.rSensGain = 0.0;
      this.rSensGain2 = 0.0;
      this.rADConversion = 0.0;
      this.rTsample = 0.0;
    });
  }

  Sensor_Calibration_Data_tBase.json(Map m) : super.json(m) {
    this.noEventDo(() {
      this.subKind = m[Vocabulary.SUBKIND] != null
          ? m[Vocabulary.SUBKIND] as String
          : (m[Vocabulary.KIND] != Vocabulary.Sensor_Calibration_Data_t_CLASS
              ? m[Vocabulary.KIND]
              : null) as String;
      this.eSensorObj = m[Vocabulary.eSensorObj_DP] as int;
      this.rSysOffset = (m[Vocabulary.rSysOffset_DP] as num).toDouble();
      this.rSysGain = (m[Vocabulary.rSysGain_DP] as num).toDouble();
      this.rSysGain2 = (m[Vocabulary.rSysGain2_DP] as num).toDouble();
      this.rSensOffset = (m[Vocabulary.rSensOffset_DP] as num).toDouble();
      this.rSensGain = (m[Vocabulary.rSensGain_DP] as num).toDouble();
      this.rSensGain2 = (m[Vocabulary.rSensGain2_DP] as num).toDouble();
      this.rADConversion = (m[Vocabulary.rADConversion_DP] as num).toDouble();
      this.rTsample = (m[Vocabulary.rTsample_DP] as num).toDouble();
    });
  }

  static Sensor_Calibration_Data_t createFromJson(Map m) =>
      Sensor_Calibration_Data_tBase.fromJson(m);
  static Sensor_Calibration_Data_t fromJson(Map m) {
    final kind = m[Vocabulary.KIND] as String;
    switch (kind) {
      case Vocabulary.Sensor_Calibration_Data_t_CLASS:
        return new Sensor_Calibration_Data_t.json(m);
      default:
        throw new ArgumentError.value(kind,
            "bad kind for class Sensor_Calibration_Data_t in fromJson constructor");
    }
  }

  String get kind => Vocabulary.Sensor_Calibration_Data_t_CLASS;
  int get eSensorObj => _eSensorObj;

  set eSensorObj(int o) {
    if (o == _eSensorObj) return;
    assert(o == null || o is int);
    var old = _eSensorObj;
    _eSensorObj = o;
    if (hasListener)
      this.sendChangeEvent(new base.PropertyChangedEvent(
          this, Vocabulary.eSensorObj_DP, old, _eSensorObj));
  }

  double get rSysOffset => _rSysOffset;

  set rSysOffset(double o) {
    if (o == _rSysOffset) return;
    assert(o == null || o is double);
    var old = _rSysOffset;
    _rSysOffset = o;
    if (hasListener)
      this.sendChangeEvent(new base.PropertyChangedEvent(
          this, Vocabulary.rSysOffset_DP, old, _rSysOffset));
  }

  double get rSysGain => _rSysGain;

  set rSysGain(double o) {
    if (o == _rSysGain) return;
    assert(o == null || o is double);
    var old = _rSysGain;
    _rSysGain = o;
    if (hasListener)
      this.sendChangeEvent(new base.PropertyChangedEvent(
          this, Vocabulary.rSysGain_DP, old, _rSysGain));
  }

  double get rSysGain2 => _rSysGain2;

  set rSysGain2(double o) {
    if (o == _rSysGain2) return;
    assert(o == null || o is double);
    var old = _rSysGain2;
    _rSysGain2 = o;
    if (hasListener)
      this.sendChangeEvent(new base.PropertyChangedEvent(
          this, Vocabulary.rSysGain2_DP, old, _rSysGain2));
  }

  double get rSensOffset => _rSensOffset;

  set rSensOffset(double o) {
    if (o == _rSensOffset) return;
    assert(o == null || o is double);
    var old = _rSensOffset;
    _rSensOffset = o;
    if (hasListener)
      this.sendChangeEvent(new base.PropertyChangedEvent(
          this, Vocabulary.rSensOffset_DP, old, _rSensOffset));
  }

  double get rSensGain => _rSensGain;

  set rSensGain(double o) {
    if (o == _rSensGain) return;
    assert(o == null || o is double);
    var old = _rSensGain;
    _rSensGain = o;
    if (hasListener)
      this.sendChangeEvent(new base.PropertyChangedEvent(
          this, Vocabulary.rSensGain_DP, old, _rSensGain));
  }

  double get rSensGain2 => _rSensGain2;

  set rSensGain2(double o) {
    if (o == _rSensGain2) return;
    assert(o == null || o is double);
    var old = _rSensGain2;
    _rSensGain2 = o;
    if (hasListener)
      this.sendChangeEvent(new base.PropertyChangedEvent(
          this, Vocabulary.rSensGain2_DP, old, _rSensGain2));
  }

  double get rADConversion => _rADConversion;

  set rADConversion(double o) {
    if (o == _rADConversion) return;
    assert(o == null || o is double);
    var old = _rADConversion;
    _rADConversion = o;
    if (hasListener)
      this.sendChangeEvent(new base.PropertyChangedEvent(
          this, Vocabulary.rADConversion_DP, old, _rADConversion));
  }

  double get rTsample => _rTsample;

  set rTsample(double o) {
    if (o == _rTsample) return;
    assert(o == null || o is double);
    var old = _rTsample;
    _rTsample = o;
    if (hasListener)
      this.sendChangeEvent(new base.PropertyChangedEvent(
          this, Vocabulary.rTsample_DP, old, _rTsample));
  }

  dynamic get(String name) {
    switch (name) {
      case Vocabulary.eSensorObj_DP:
        return this.eSensorObj;
      case Vocabulary.rSysOffset_DP:
        return this.rSysOffset;
      case Vocabulary.rSysGain_DP:
        return this.rSysGain;
      case Vocabulary.rSysGain2_DP:
        return this.rSysGain2;
      case Vocabulary.rSensOffset_DP:
        return this.rSensOffset;
      case Vocabulary.rSensGain_DP:
        return this.rSensGain;
      case Vocabulary.rSensGain2_DP:
        return this.rSensGain2;
      case Vocabulary.rADConversion_DP:
        return this.rADConversion;
      case Vocabulary.rTsample_DP:
        return this.rTsample;
      default:
        return super.get(name);
    }
  }

  set(String name, dynamic value) {
    switch (name) {
      case Vocabulary.eSensorObj_DP:
        this.eSensorObj = value as int;
        return;
      case Vocabulary.rSysOffset_DP:
        this.rSysOffset = value as double;
        return;
      case Vocabulary.rSysGain_DP:
        this.rSysGain = value as double;
        return;
      case Vocabulary.rSysGain2_DP:
        this.rSysGain2 = value as double;
        return;
      case Vocabulary.rSensOffset_DP:
        this.rSensOffset = value as double;
        return;
      case Vocabulary.rSensGain_DP:
        this.rSensGain = value as double;
        return;
      case Vocabulary.rSensGain2_DP:
        this.rSensGain2 = value as double;
        return;
      case Vocabulary.rADConversion_DP:
        this.rADConversion = value as double;
        return;
      case Vocabulary.rTsample_DP:
        this.rTsample = value as double;
        return;
      default:
        super.set(name, value);
    }
  }

  List<String> getPropertyNames() =>
      new List.from(super.getPropertyNames(), growable: true)
        ..addAll(PROPERTY_NAMES);

  Sensor_Calibration_Data_t copy() =>
      new Sensor_Calibration_Data_t.json(this.toJson());
  Map toJson() {
    var m = super.toJson() as Map;
    m[Vocabulary.KIND] = Vocabulary.Sensor_Calibration_Data_t_CLASS;
    if (this.subKind != null &&
        this.subKind != Vocabulary.Sensor_Calibration_Data_t_CLASS)
      m[Vocabulary.SUBKIND] = this.subKind;
    else
      m.remove(Vocabulary.SUBKIND);
    m[Vocabulary.eSensorObj_DP] = eSensorObj;
    m[Vocabulary.rSysOffset_DP] = rSysOffset;
    m[Vocabulary.rSysGain_DP] = rSysGain;
    m[Vocabulary.rSysGain2_DP] = rSysGain2;
    m[Vocabulary.rSensOffset_DP] = rSensOffset;
    m[Vocabulary.rSensGain_DP] = rSensGain;
    m[Vocabulary.rSensGain2_DP] = rSensGain2;
    m[Vocabulary.rADConversion_DP] = rADConversion;
    m[Vocabulary.rTsample_DP] = rTsample;
    return m;
  }
}
