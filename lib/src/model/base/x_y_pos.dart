part of ps_model_base;

class XYPosBase extends base.Base {
  static const List<String> PROPERTY_NAMES = const [
    Vocabulary.x_DP,
    Vocabulary.y_DP
  ];
  double _x;
  double _y;

  XYPosBase() : super() {
    this.noEventDo(() {
      this.x = 0.0;
      this.y = 0.0;
    });
  }

  XYPosBase.json(Map m) : super.json(m) {
    this.noEventDo(() {
      this.subKind = m[Vocabulary.SUBKIND] != null
          ? m[Vocabulary.SUBKIND] as String
          : (m[Vocabulary.KIND] != Vocabulary.XYPos_CLASS
              ? m[Vocabulary.KIND]
              : null) as String;
      this.x = (m[Vocabulary.x_DP] as num).toDouble();
      this.y = (m[Vocabulary.y_DP] as num).toDouble();
    });
  }

  static XYPos createFromJson(Map m) => XYPosBase.fromJson(m);
  static XYPos fromJson(Map m) {
    final kind = m[Vocabulary.KIND] as String;
    switch (kind) {
      case Vocabulary.XYPos_CLASS:
        return new XYPos.json(m);
      default:
        throw new ArgumentError.value(
            kind, "bad kind for class XYPos in fromJson constructor");
    }
  }

  String get kind => Vocabulary.XYPos_CLASS;
  double get x => _x;

  set x(double o) {
    if (o == _x) return;
    assert(o == null || o is double);
    var old = _x;
    _x = o;
    if (hasListener)
      this.sendChangeEvent(
          new base.PropertyChangedEvent(this, Vocabulary.x_DP, old, _x));
  }

  double get y => _y;

  set y(double o) {
    if (o == _y) return;
    assert(o == null || o is double);
    var old = _y;
    _y = o;
    if (hasListener)
      this.sendChangeEvent(
          new base.PropertyChangedEvent(this, Vocabulary.y_DP, old, _y));
  }

  dynamic get(String name) {
    switch (name) {
      case Vocabulary.x_DP:
        return this.x;
      case Vocabulary.y_DP:
        return this.y;
      default:
        return super.get(name);
    }
  }

  set(String name, dynamic value) {
    switch (name) {
      case Vocabulary.x_DP:
        this.x = value as double;
        return;
      case Vocabulary.y_DP:
        this.y = value as double;
        return;
      default:
        super.set(name, value);
    }
  }

  List<String> getPropertyNames() =>
      new List.from(super.getPropertyNames(), growable: true)
        ..addAll(PROPERTY_NAMES);

  XYPos copy() => new XYPos.json(this.toJson());
  Map toJson() {
    var m = super.toJson() as Map;
    m[Vocabulary.KIND] = Vocabulary.XYPos_CLASS;
    if (this.subKind != null && this.subKind != Vocabulary.XYPos_CLASS)
      m[Vocabulary.SUBKIND] = this.subKind;
    else
      m.remove(Vocabulary.SUBKIND);
    m[Vocabulary.x_DP] = x;
    m[Vocabulary.y_DP] = y;
    return m;
  }
}
