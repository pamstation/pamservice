part of ps_model_base;

class TemperaturesBase extends base.Base {
  static const List<String> PROPERTY_NAMES = const [
    Vocabulary.coverLeft_DP,
    Vocabulary.coverMiddle_DP,
    Vocabulary.coverRight_DP,
    Vocabulary.bottomLeft_DP,
    Vocabulary.bottomMiddle_DP,
    Vocabulary.bottomRight_DP
  ];
  double _coverLeft;
  double _coverMiddle;
  double _coverRight;
  double _bottomLeft;
  double _bottomMiddle;
  double _bottomRight;

  TemperaturesBase() : super() {
    this.noEventDo(() {
      this.coverLeft = 0.0;
      this.coverMiddle = 0.0;
      this.coverRight = 0.0;
      this.bottomLeft = 0.0;
      this.bottomMiddle = 0.0;
      this.bottomRight = 0.0;
    });
  }

  TemperaturesBase.json(Map m) : super.json(m) {
    this.noEventDo(() {
      this.subKind = m[Vocabulary.SUBKIND] != null
          ? m[Vocabulary.SUBKIND] as String
          : (m[Vocabulary.KIND] != Vocabulary.Temperatures_CLASS
              ? m[Vocabulary.KIND]
              : null) as String;
      this.coverLeft = (m[Vocabulary.coverLeft_DP] as num).toDouble();
      this.coverMiddle = (m[Vocabulary.coverMiddle_DP] as num).toDouble();
      this.coverRight = (m[Vocabulary.coverRight_DP] as num).toDouble();
      this.bottomLeft = (m[Vocabulary.bottomLeft_DP] as num).toDouble();
      this.bottomMiddle = (m[Vocabulary.bottomMiddle_DP] as num).toDouble();
      this.bottomRight = (m[Vocabulary.bottomRight_DP] as num).toDouble();
    });
  }

  static Temperatures createFromJson(Map m) => TemperaturesBase.fromJson(m);
  static Temperatures fromJson(Map m) {
    final kind = m[Vocabulary.KIND] as String;
    switch (kind) {
      case Vocabulary.Temperatures_CLASS:
        return new Temperatures.json(m);
      default:
        throw new ArgumentError.value(
            kind, "bad kind for class Temperatures in fromJson constructor");
    }
  }

  String get kind => Vocabulary.Temperatures_CLASS;
  double get coverLeft => _coverLeft;

  set coverLeft(double o) {
    if (o == _coverLeft) return;
    assert(o == null || o is double);
    var old = _coverLeft;
    _coverLeft = o;
    if (hasListener)
      this.sendChangeEvent(new base.PropertyChangedEvent(
          this, Vocabulary.coverLeft_DP, old, _coverLeft));
  }

  double get coverMiddle => _coverMiddle;

  set coverMiddle(double o) {
    if (o == _coverMiddle) return;
    assert(o == null || o is double);
    var old = _coverMiddle;
    _coverMiddle = o;
    if (hasListener)
      this.sendChangeEvent(new base.PropertyChangedEvent(
          this, Vocabulary.coverMiddle_DP, old, _coverMiddle));
  }

  double get coverRight => _coverRight;

  set coverRight(double o) {
    if (o == _coverRight) return;
    assert(o == null || o is double);
    var old = _coverRight;
    _coverRight = o;
    if (hasListener)
      this.sendChangeEvent(new base.PropertyChangedEvent(
          this, Vocabulary.coverRight_DP, old, _coverRight));
  }

  double get bottomLeft => _bottomLeft;

  set bottomLeft(double o) {
    if (o == _bottomLeft) return;
    assert(o == null || o is double);
    var old = _bottomLeft;
    _bottomLeft = o;
    if (hasListener)
      this.sendChangeEvent(new base.PropertyChangedEvent(
          this, Vocabulary.bottomLeft_DP, old, _bottomLeft));
  }

  double get bottomMiddle => _bottomMiddle;

  set bottomMiddle(double o) {
    if (o == _bottomMiddle) return;
    assert(o == null || o is double);
    var old = _bottomMiddle;
    _bottomMiddle = o;
    if (hasListener)
      this.sendChangeEvent(new base.PropertyChangedEvent(
          this, Vocabulary.bottomMiddle_DP, old, _bottomMiddle));
  }

  double get bottomRight => _bottomRight;

  set bottomRight(double o) {
    if (o == _bottomRight) return;
    assert(o == null || o is double);
    var old = _bottomRight;
    _bottomRight = o;
    if (hasListener)
      this.sendChangeEvent(new base.PropertyChangedEvent(
          this, Vocabulary.bottomRight_DP, old, _bottomRight));
  }

  dynamic get(String name) {
    switch (name) {
      case Vocabulary.coverLeft_DP:
        return this.coverLeft;
      case Vocabulary.coverMiddle_DP:
        return this.coverMiddle;
      case Vocabulary.coverRight_DP:
        return this.coverRight;
      case Vocabulary.bottomLeft_DP:
        return this.bottomLeft;
      case Vocabulary.bottomMiddle_DP:
        return this.bottomMiddle;
      case Vocabulary.bottomRight_DP:
        return this.bottomRight;
      default:
        return super.get(name);
    }
  }

  set(String name, dynamic value) {
    switch (name) {
      case Vocabulary.coverLeft_DP:
        this.coverLeft = value as double;
        return;
      case Vocabulary.coverMiddle_DP:
        this.coverMiddle = value as double;
        return;
      case Vocabulary.coverRight_DP:
        this.coverRight = value as double;
        return;
      case Vocabulary.bottomLeft_DP:
        this.bottomLeft = value as double;
        return;
      case Vocabulary.bottomMiddle_DP:
        this.bottomMiddle = value as double;
        return;
      case Vocabulary.bottomRight_DP:
        this.bottomRight = value as double;
        return;
      default:
        super.set(name, value);
    }
  }

  List<String> getPropertyNames() =>
      new List.from(super.getPropertyNames(), growable: true)
        ..addAll(PROPERTY_NAMES);

  Temperatures copy() => new Temperatures.json(this.toJson());
  Map toJson() {
    var m = super.toJson() as Map;
    m[Vocabulary.KIND] = Vocabulary.Temperatures_CLASS;
    if (this.subKind != null && this.subKind != Vocabulary.Temperatures_CLASS)
      m[Vocabulary.SUBKIND] = this.subKind;
    else
      m.remove(Vocabulary.SUBKIND);
    m[Vocabulary.coverLeft_DP] = coverLeft;
    m[Vocabulary.coverMiddle_DP] = coverMiddle;
    m[Vocabulary.coverRight_DP] = coverRight;
    m[Vocabulary.bottomLeft_DP] = bottomLeft;
    m[Vocabulary.bottomMiddle_DP] = bottomMiddle;
    m[Vocabulary.bottomRight_DP] = bottomRight;
    return m;
  }
}
