part of ps_model_base;

class AspirationSettings_tBase extends base.Base {
  static const List<String> PROPERTY_NAMES = const [
    Vocabulary.rPressureUnderlimit_DP,
    Vocabulary.rPressureUpperLimit_DP,
    Vocabulary.uAspirateTubeCleanDelay_DP,
    Vocabulary.uAspPressBuildupDelay_DP
  ];
  double _rPressureUnderlimit;
  double _rPressureUpperLimit;
  int _uAspirateTubeCleanDelay;
  int _uAspPressBuildupDelay;

  AspirationSettings_tBase() : super() {
    this.noEventDo(() {
      this.rPressureUnderlimit = 0.0;
      this.rPressureUpperLimit = 0.0;
      this.uAspirateTubeCleanDelay = 0;
      this.uAspPressBuildupDelay = 0;
    });
  }

  AspirationSettings_tBase.json(Map m) : super.json(m) {
    this.noEventDo(() {
      this.subKind = m[Vocabulary.SUBKIND] != null
          ? m[Vocabulary.SUBKIND] as String
          : (m[Vocabulary.KIND] != Vocabulary.AspirationSettings_t_CLASS
              ? m[Vocabulary.KIND]
              : null) as String;
      this.rPressureUnderlimit =
          (m[Vocabulary.rPressureUnderlimit_DP] as num).toDouble();
      this.rPressureUpperLimit =
          (m[Vocabulary.rPressureUpperLimit_DP] as num).toDouble();
      this.uAspirateTubeCleanDelay =
          m[Vocabulary.uAspirateTubeCleanDelay_DP] as int;
      this.uAspPressBuildupDelay =
          m[Vocabulary.uAspPressBuildupDelay_DP] as int;
    });
  }

  static AspirationSettings_t createFromJson(Map m) =>
      AspirationSettings_tBase.fromJson(m);
  static AspirationSettings_t fromJson(Map m) {
    final kind = m[Vocabulary.KIND] as String;
    switch (kind) {
      case Vocabulary.AspirationSettings_t_CLASS:
        return new AspirationSettings_t.json(m);
      default:
        throw new ArgumentError.value(kind,
            "bad kind for class AspirationSettings_t in fromJson constructor");
    }
  }

  String get kind => Vocabulary.AspirationSettings_t_CLASS;
  double get rPressureUnderlimit => _rPressureUnderlimit;

  set rPressureUnderlimit(double o) {
    if (o == _rPressureUnderlimit) return;
    assert(o == null || o is double);
    var old = _rPressureUnderlimit;
    _rPressureUnderlimit = o;
    if (hasListener)
      this.sendChangeEvent(new base.PropertyChangedEvent(
          this, Vocabulary.rPressureUnderlimit_DP, old, _rPressureUnderlimit));
  }

  double get rPressureUpperLimit => _rPressureUpperLimit;

  set rPressureUpperLimit(double o) {
    if (o == _rPressureUpperLimit) return;
    assert(o == null || o is double);
    var old = _rPressureUpperLimit;
    _rPressureUpperLimit = o;
    if (hasListener)
      this.sendChangeEvent(new base.PropertyChangedEvent(
          this, Vocabulary.rPressureUpperLimit_DP, old, _rPressureUpperLimit));
  }

  int get uAspirateTubeCleanDelay => _uAspirateTubeCleanDelay;

  set uAspirateTubeCleanDelay(int o) {
    if (o == _uAspirateTubeCleanDelay) return;
    assert(o == null || o is int);
    var old = _uAspirateTubeCleanDelay;
    _uAspirateTubeCleanDelay = o;
    if (hasListener)
      this.sendChangeEvent(new base.PropertyChangedEvent(
          this,
          Vocabulary.uAspirateTubeCleanDelay_DP,
          old,
          _uAspirateTubeCleanDelay));
  }

  int get uAspPressBuildupDelay => _uAspPressBuildupDelay;

  set uAspPressBuildupDelay(int o) {
    if (o == _uAspPressBuildupDelay) return;
    assert(o == null || o is int);
    var old = _uAspPressBuildupDelay;
    _uAspPressBuildupDelay = o;
    if (hasListener)
      this.sendChangeEvent(new base.PropertyChangedEvent(this,
          Vocabulary.uAspPressBuildupDelay_DP, old, _uAspPressBuildupDelay));
  }

  dynamic get(String name) {
    switch (name) {
      case Vocabulary.rPressureUnderlimit_DP:
        return this.rPressureUnderlimit;
      case Vocabulary.rPressureUpperLimit_DP:
        return this.rPressureUpperLimit;
      case Vocabulary.uAspirateTubeCleanDelay_DP:
        return this.uAspirateTubeCleanDelay;
      case Vocabulary.uAspPressBuildupDelay_DP:
        return this.uAspPressBuildupDelay;
      default:
        return super.get(name);
    }
  }

  set(String name, dynamic value) {
    switch (name) {
      case Vocabulary.rPressureUnderlimit_DP:
        this.rPressureUnderlimit = value as double;
        return;
      case Vocabulary.rPressureUpperLimit_DP:
        this.rPressureUpperLimit = value as double;
        return;
      case Vocabulary.uAspirateTubeCleanDelay_DP:
        this.uAspirateTubeCleanDelay = value as int;
        return;
      case Vocabulary.uAspPressBuildupDelay_DP:
        this.uAspPressBuildupDelay = value as int;
        return;
      default:
        super.set(name, value);
    }
  }

  List<String> getPropertyNames() =>
      new List.from(super.getPropertyNames(), growable: true)
        ..addAll(PROPERTY_NAMES);

  AspirationSettings_t copy() => new AspirationSettings_t.json(this.toJson());
  Map toJson() {
    var m = super.toJson() as Map;
    m[Vocabulary.KIND] = Vocabulary.AspirationSettings_t_CLASS;
    if (this.subKind != null &&
        this.subKind != Vocabulary.AspirationSettings_t_CLASS)
      m[Vocabulary.SUBKIND] = this.subKind;
    else
      m.remove(Vocabulary.SUBKIND);
    m[Vocabulary.rPressureUnderlimit_DP] = rPressureUnderlimit;
    m[Vocabulary.rPressureUpperLimit_DP] = rPressureUpperLimit;
    m[Vocabulary.uAspirateTubeCleanDelay_DP] = uAspirateTubeCleanDelay;
    m[Vocabulary.uAspPressBuildupDelay_DP] = uAspPressBuildupDelay;
    return m;
  }
}
