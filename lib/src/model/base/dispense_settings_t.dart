part of ps_model_base;

class DispenseSettings_tBase extends base.Base {
  static const List<String> PROPERTY_NAMES = const [
    Vocabulary.uHeatableHeadBitSet_DP,
    Vocabulary.rPrimeAmount_DP,
    Vocabulary.rRetractAmount_DP,
    Vocabulary.uRetractDelay_DP
  ];
  int _uHeatableHeadBitSet;
  double _rPrimeAmount;
  double _rRetractAmount;
  int _uRetractDelay;

  DispenseSettings_tBase() : super() {
    this.noEventDo(() {
      this.uHeatableHeadBitSet = 0;
      this.rPrimeAmount = 0.0;
      this.rRetractAmount = 0.0;
      this.uRetractDelay = 0;
    });
  }

  DispenseSettings_tBase.json(Map m) : super.json(m) {
    this.noEventDo(() {
      this.subKind = m[Vocabulary.SUBKIND] != null
          ? m[Vocabulary.SUBKIND] as String
          : (m[Vocabulary.KIND] != Vocabulary.DispenseSettings_t_CLASS
              ? m[Vocabulary.KIND]
              : null) as String;
      this.uHeatableHeadBitSet = m[Vocabulary.uHeatableHeadBitSet_DP] as int;
      this.rPrimeAmount = (m[Vocabulary.rPrimeAmount_DP] as num).toDouble();
      this.rRetractAmount = (m[Vocabulary.rRetractAmount_DP] as num).toDouble();
      this.uRetractDelay = m[Vocabulary.uRetractDelay_DP] as int;
    });
  }

  static DispenseSettings_t createFromJson(Map m) =>
      DispenseSettings_tBase.fromJson(m);
  static DispenseSettings_t fromJson(Map m) {
    final kind = m[Vocabulary.KIND] as String;
    switch (kind) {
      case Vocabulary.DispenseSettings_t_CLASS:
        return new DispenseSettings_t.json(m);
      default:
        throw new ArgumentError.value(kind,
            "bad kind for class DispenseSettings_t in fromJson constructor");
    }
  }

  String get kind => Vocabulary.DispenseSettings_t_CLASS;
  int get uHeatableHeadBitSet => _uHeatableHeadBitSet;

  set uHeatableHeadBitSet(int o) {
    if (o == _uHeatableHeadBitSet) return;
    assert(o == null || o is int);
    var old = _uHeatableHeadBitSet;
    _uHeatableHeadBitSet = o;
    if (hasListener)
      this.sendChangeEvent(new base.PropertyChangedEvent(
          this, Vocabulary.uHeatableHeadBitSet_DP, old, _uHeatableHeadBitSet));
  }

  double get rPrimeAmount => _rPrimeAmount;

  set rPrimeAmount(double o) {
    if (o == _rPrimeAmount) return;
    assert(o == null || o is double);
    var old = _rPrimeAmount;
    _rPrimeAmount = o;
    if (hasListener)
      this.sendChangeEvent(new base.PropertyChangedEvent(
          this, Vocabulary.rPrimeAmount_DP, old, _rPrimeAmount));
  }

  double get rRetractAmount => _rRetractAmount;

  set rRetractAmount(double o) {
    if (o == _rRetractAmount) return;
    assert(o == null || o is double);
    var old = _rRetractAmount;
    _rRetractAmount = o;
    if (hasListener)
      this.sendChangeEvent(new base.PropertyChangedEvent(
          this, Vocabulary.rRetractAmount_DP, old, _rRetractAmount));
  }

  int get uRetractDelay => _uRetractDelay;

  set uRetractDelay(int o) {
    if (o == _uRetractDelay) return;
    assert(o == null || o is int);
    var old = _uRetractDelay;
    _uRetractDelay = o;
    if (hasListener)
      this.sendChangeEvent(new base.PropertyChangedEvent(
          this, Vocabulary.uRetractDelay_DP, old, _uRetractDelay));
  }

  dynamic get(String name) {
    switch (name) {
      case Vocabulary.uHeatableHeadBitSet_DP:
        return this.uHeatableHeadBitSet;
      case Vocabulary.rPrimeAmount_DP:
        return this.rPrimeAmount;
      case Vocabulary.rRetractAmount_DP:
        return this.rRetractAmount;
      case Vocabulary.uRetractDelay_DP:
        return this.uRetractDelay;
      default:
        return super.get(name);
    }
  }

  set(String name, dynamic value) {
    switch (name) {
      case Vocabulary.uHeatableHeadBitSet_DP:
        this.uHeatableHeadBitSet = value as int;
        return;
      case Vocabulary.rPrimeAmount_DP:
        this.rPrimeAmount = value as double;
        return;
      case Vocabulary.rRetractAmount_DP:
        this.rRetractAmount = value as double;
        return;
      case Vocabulary.uRetractDelay_DP:
        this.uRetractDelay = value as int;
        return;
      default:
        super.set(name, value);
    }
  }

  List<String> getPropertyNames() =>
      new List.from(super.getPropertyNames(), growable: true)
        ..addAll(PROPERTY_NAMES);

  DispenseSettings_t copy() => new DispenseSettings_t.json(this.toJson());
  Map toJson() {
    var m = super.toJson() as Map;
    m[Vocabulary.KIND] = Vocabulary.DispenseSettings_t_CLASS;
    if (this.subKind != null &&
        this.subKind != Vocabulary.DispenseSettings_t_CLASS)
      m[Vocabulary.SUBKIND] = this.subKind;
    else
      m.remove(Vocabulary.SUBKIND);
    m[Vocabulary.uHeatableHeadBitSet_DP] = uHeatableHeadBitSet;
    m[Vocabulary.rPrimeAmount_DP] = rPrimeAmount;
    m[Vocabulary.rRetractAmount_DP] = rRetractAmount;
    m[Vocabulary.uRetractDelay_DP] = uRetractDelay;
    return m;
  }
}
