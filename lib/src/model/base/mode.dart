part of ps_model_base;

class ModeBase extends base.Base {
  static const List<String> PROPERTY_NAMES = const [
    Vocabulary.isStub_DP,
    Vocabulary.isStubBoard1_DP,
    Vocabulary.isStubBoard2_DP,
    Vocabulary.isStubBoard3_DP
  ];
  bool _isStub;
  bool _isStubBoard1;
  bool _isStubBoard2;
  bool _isStubBoard3;

  ModeBase() : super() {
    this.noEventDo(() {
      this.isStub = true;
      this.isStubBoard1 = true;
      this.isStubBoard2 = true;
      this.isStubBoard3 = true;
    });
  }

  ModeBase.json(Map m) : super.json(m) {
    this.noEventDo(() {
      this.subKind = m[Vocabulary.SUBKIND] != null
          ? m[Vocabulary.SUBKIND] as String
          : (m[Vocabulary.KIND] != Vocabulary.Mode_CLASS
              ? m[Vocabulary.KIND]
              : null) as String;
      this.isStub = m[Vocabulary.isStub_DP] as bool;
      this.isStubBoard1 = m[Vocabulary.isStubBoard1_DP] as bool;
      this.isStubBoard2 = m[Vocabulary.isStubBoard2_DP] as bool;
      this.isStubBoard3 = m[Vocabulary.isStubBoard3_DP] as bool;
    });
  }

  static Mode createFromJson(Map m) => ModeBase.fromJson(m);
  static Mode fromJson(Map m) {
    final kind = m[Vocabulary.KIND] as String;
    switch (kind) {
      case Vocabulary.Mode_CLASS:
        return new Mode.json(m);
      default:
        throw new ArgumentError.value(
            kind, "bad kind for class Mode in fromJson constructor");
    }
  }

  String get kind => Vocabulary.Mode_CLASS;
  bool get isStub => _isStub;

  set isStub(bool o) {
    if (o == _isStub) return;
    assert(o == null || o is bool);
    var old = _isStub;
    _isStub = o;
    if (hasListener)
      this.sendChangeEvent(new base.PropertyChangedEvent(
          this, Vocabulary.isStub_DP, old, _isStub));
  }

  bool get isStubBoard1 => _isStubBoard1;

  set isStubBoard1(bool o) {
    if (o == _isStubBoard1) return;
    assert(o == null || o is bool);
    var old = _isStubBoard1;
    _isStubBoard1 = o;
    if (hasListener)
      this.sendChangeEvent(new base.PropertyChangedEvent(
          this, Vocabulary.isStubBoard1_DP, old, _isStubBoard1));
  }

  bool get isStubBoard2 => _isStubBoard2;

  set isStubBoard2(bool o) {
    if (o == _isStubBoard2) return;
    assert(o == null || o is bool);
    var old = _isStubBoard2;
    _isStubBoard2 = o;
    if (hasListener)
      this.sendChangeEvent(new base.PropertyChangedEvent(
          this, Vocabulary.isStubBoard2_DP, old, _isStubBoard2));
  }

  bool get isStubBoard3 => _isStubBoard3;

  set isStubBoard3(bool o) {
    if (o == _isStubBoard3) return;
    assert(o == null || o is bool);
    var old = _isStubBoard3;
    _isStubBoard3 = o;
    if (hasListener)
      this.sendChangeEvent(new base.PropertyChangedEvent(
          this, Vocabulary.isStubBoard3_DP, old, _isStubBoard3));
  }

  dynamic get(String name) {
    switch (name) {
      case Vocabulary.isStub_DP:
        return this.isStub;
      case Vocabulary.isStubBoard1_DP:
        return this.isStubBoard1;
      case Vocabulary.isStubBoard2_DP:
        return this.isStubBoard2;
      case Vocabulary.isStubBoard3_DP:
        return this.isStubBoard3;
      default:
        return super.get(name);
    }
  }

  set(String name, dynamic value) {
    switch (name) {
      case Vocabulary.isStub_DP:
        this.isStub = value as bool;
        return;
      case Vocabulary.isStubBoard1_DP:
        this.isStubBoard1 = value as bool;
        return;
      case Vocabulary.isStubBoard2_DP:
        this.isStubBoard2 = value as bool;
        return;
      case Vocabulary.isStubBoard3_DP:
        this.isStubBoard3 = value as bool;
        return;
      default:
        super.set(name, value);
    }
  }

  List<String> getPropertyNames() =>
      new List.from(super.getPropertyNames(), growable: true)
        ..addAll(PROPERTY_NAMES);

  Mode copy() => new Mode.json(this.toJson());
  Map toJson() {
    var m = super.toJson() as Map;
    m[Vocabulary.KIND] = Vocabulary.Mode_CLASS;
    if (this.subKind != null && this.subKind != Vocabulary.Mode_CLASS)
      m[Vocabulary.SUBKIND] = this.subKind;
    else
      m.remove(Vocabulary.SUBKIND);
    m[Vocabulary.isStub_DP] = isStub;
    m[Vocabulary.isStubBoard1_DP] = isStubBoard1;
    m[Vocabulary.isStubBoard2_DP] = isStubBoard2;
    m[Vocabulary.isStubBoard3_DP] = isStubBoard3;
    return m;
  }
}
