part of ps_model_base;

class SettingsBase extends base.Base {
  static const List<String> PROPERTY_NAMES = const [
    Vocabulary.unitName_DP,
    Vocabulary.wagon_logicalXYPos_t_OP,
    Vocabulary.filter_logicalPos_t_OP,
    Vocabulary.focus_logicalPos_t_OP,
    Vocabulary.focus_logicalWellPos_t_OP,
    Vocabulary.ledIntensity_t_OP,
    Vocabulary.load_unload_settings_t_OP,
    Vocabulary.pumpSettings_t_OP,
    Vocabulary.aspirationSettings_t_OP,
    Vocabulary.dispenseSettings_t_OP,
    Vocabulary.sensorCalibrationData_t_OP,
    Vocabulary.controllableSettings_OP
  ];
  String _unitName;
  Wagon_LogicalXYPos_t _wagon_logicalXYPos_t;
  Filter_LogicalPos_t _filter_logicalPos_t;
  Focus_LogicalPos_t _focus_logicalPos_t;
  Focus_LogicalWellPos_t _focus_logicalWellPos_t;
  LEDIntensity_t _ledIntensity_t;
  Load_Unload_Settings_t _load_unload_settings_t;
  PumpSettings_t _pumpSettings_t;
  AspirationSettings_t _aspirationSettings_t;
  DispenseSettings_t _dispenseSettings_t;
  base.ListChanged<Sensor_Calibration_Data_t> _sensorCalibrationData_t;
  base.ListChanged<ControllableSettings> _controllableSettings;

  SettingsBase() : super() {
    this.noEventDo(() {
      this.unitName = "";
      this.wagon_logicalXYPos_t = new Wagon_LogicalXYPos_t();
      this.filter_logicalPos_t = new Filter_LogicalPos_t();
      this.focus_logicalPos_t = new Focus_LogicalPos_t();
      this.focus_logicalWellPos_t = new Focus_LogicalWellPos_t();
      this.ledIntensity_t = new LEDIntensity_t();
      this.load_unload_settings_t = new Load_Unload_Settings_t();
      this.pumpSettings_t = new PumpSettings_t();
      this.aspirationSettings_t = new AspirationSettings_t();
      this.dispenseSettings_t = new DispenseSettings_t();
      this._sensorCalibrationData_t =
          new base.ListChanged<Sensor_Calibration_Data_t>();
      this._sensorCalibrationData_t.parent = this;
      this._controllableSettings = new base.ListChanged<ControllableSettings>();
      this._controllableSettings.parent = this;
    });
  }

  SettingsBase.json(Map m) : super.json(m) {
    this.noEventDo(() {
      this.subKind = m[Vocabulary.SUBKIND] != null
          ? m[Vocabulary.SUBKIND] as String
          : (m[Vocabulary.KIND] != Vocabulary.Settings_CLASS
              ? m[Vocabulary.KIND]
              : null) as String;
      this.unitName = m[Vocabulary.unitName_DP] as String;
      if (m[Vocabulary.wagon_logicalXYPos_t_OP] == null)
        this.wagon_logicalXYPos_t = new Wagon_LogicalXYPos_t();
      else
        this.wagon_logicalXYPos_t = Wagon_LogicalXYPos_tBase.fromJson(
            m[Vocabulary.wagon_logicalXYPos_t_OP] as Map);
      if (m[Vocabulary.filter_logicalPos_t_OP] == null)
        this.filter_logicalPos_t = new Filter_LogicalPos_t();
      else
        this.filter_logicalPos_t = Filter_LogicalPos_tBase.fromJson(
            m[Vocabulary.filter_logicalPos_t_OP] as Map);
      if (m[Vocabulary.focus_logicalPos_t_OP] == null)
        this.focus_logicalPos_t = new Focus_LogicalPos_t();
      else
        this.focus_logicalPos_t = Focus_LogicalPos_tBase.fromJson(
            m[Vocabulary.focus_logicalPos_t_OP] as Map);
      if (m[Vocabulary.focus_logicalWellPos_t_OP] == null)
        this.focus_logicalWellPos_t = new Focus_LogicalWellPos_t();
      else
        this.focus_logicalWellPos_t = Focus_LogicalWellPos_tBase.fromJson(
            m[Vocabulary.focus_logicalWellPos_t_OP] as Map);
      if (m[Vocabulary.ledIntensity_t_OP] == null)
        this.ledIntensity_t = new LEDIntensity_t();
      else
        this.ledIntensity_t =
            LEDIntensity_tBase.fromJson(m[Vocabulary.ledIntensity_t_OP] as Map);
      if (m[Vocabulary.load_unload_settings_t_OP] == null)
        this.load_unload_settings_t = new Load_Unload_Settings_t();
      else
        this.load_unload_settings_t = Load_Unload_Settings_tBase.fromJson(
            m[Vocabulary.load_unload_settings_t_OP] as Map);
      if (m[Vocabulary.pumpSettings_t_OP] == null)
        this.pumpSettings_t = new PumpSettings_t();
      else
        this.pumpSettings_t =
            PumpSettings_tBase.fromJson(m[Vocabulary.pumpSettings_t_OP] as Map);
      if (m[Vocabulary.aspirationSettings_t_OP] == null)
        this.aspirationSettings_t = new AspirationSettings_t();
      else
        this.aspirationSettings_t = AspirationSettings_tBase.fromJson(
            m[Vocabulary.aspirationSettings_t_OP] as Map);
      if (m[Vocabulary.dispenseSettings_t_OP] == null)
        this.dispenseSettings_t = new DispenseSettings_t();
      else
        this.dispenseSettings_t = DispenseSettings_tBase.fromJson(
            m[Vocabulary.dispenseSettings_t_OP] as Map);
      if (m[Vocabulary.sensorCalibrationData_t_OP] == null)
        this._sensorCalibrationData_t =
            new base.ListChanged<Sensor_Calibration_Data_t>();
      else
        this._sensorCalibrationData_t =
            new base.ListChanged<Sensor_Calibration_Data_t>(
                (m[Vocabulary.sensorCalibrationData_t_OP] as List)
                    .map((each) => Sensor_Calibration_Data_tBase.createFromJson(
                        each as Map))
                    .toList());
      this._sensorCalibrationData_t.parent = this;
      if (m[Vocabulary.controllableSettings_OP] == null)
        this._controllableSettings =
            new base.ListChanged<ControllableSettings>();
      else
        this._controllableSettings = new base.ListChanged<ControllableSettings>(
            (m[Vocabulary.controllableSettings_OP] as List)
                .map((each) =>
                    ControllableSettingsBase.createFromJson(each as Map))
                .toList());
      this._controllableSettings.parent = this;
    });
  }

  static Settings createFromJson(Map m) => SettingsBase.fromJson(m);
  static Settings fromJson(Map m) {
    final kind = m[Vocabulary.KIND] as String;
    switch (kind) {
      case Vocabulary.Settings_CLASS:
        return new Settings.json(m);
      default:
        throw new ArgumentError.value(
            kind, "bad kind for class Settings in fromJson constructor");
    }
  }

  String get kind => Vocabulary.Settings_CLASS;
  String get unitName => _unitName;

  set unitName(String o) {
    if (o == _unitName) return;
    assert(o == null || o is String);
    var old = _unitName;
    _unitName = o;
    if (hasListener)
      this.sendChangeEvent(new base.PropertyChangedEvent(
          this, Vocabulary.unitName_DP, old, _unitName));
  }

  Wagon_LogicalXYPos_t get wagon_logicalXYPos_t => _wagon_logicalXYPos_t;

  set wagon_logicalXYPos_t(Wagon_LogicalXYPos_t o) {
    if (o == _wagon_logicalXYPos_t) return;
    assert(o == null || o is Wagon_LogicalXYPos_t);
    if (_wagon_logicalXYPos_t != null) _wagon_logicalXYPos_t.parent = null;
    if (o != null) o.parent = this;
    var old = _wagon_logicalXYPos_t;
    _wagon_logicalXYPos_t = o;
    if (hasListener)
      this.sendChangeEvent(new base.PropertyChangedEvent(this,
          Vocabulary.wagon_logicalXYPos_t_OP, old, _wagon_logicalXYPos_t));
  }

  Filter_LogicalPos_t get filter_logicalPos_t => _filter_logicalPos_t;

  set filter_logicalPos_t(Filter_LogicalPos_t o) {
    if (o == _filter_logicalPos_t) return;
    assert(o == null || o is Filter_LogicalPos_t);
    if (_filter_logicalPos_t != null) _filter_logicalPos_t.parent = null;
    if (o != null) o.parent = this;
    var old = _filter_logicalPos_t;
    _filter_logicalPos_t = o;
    if (hasListener)
      this.sendChangeEvent(new base.PropertyChangedEvent(
          this, Vocabulary.filter_logicalPos_t_OP, old, _filter_logicalPos_t));
  }

  Focus_LogicalPos_t get focus_logicalPos_t => _focus_logicalPos_t;

  set focus_logicalPos_t(Focus_LogicalPos_t o) {
    if (o == _focus_logicalPos_t) return;
    assert(o == null || o is Focus_LogicalPos_t);
    if (_focus_logicalPos_t != null) _focus_logicalPos_t.parent = null;
    if (o != null) o.parent = this;
    var old = _focus_logicalPos_t;
    _focus_logicalPos_t = o;
    if (hasListener)
      this.sendChangeEvent(new base.PropertyChangedEvent(
          this, Vocabulary.focus_logicalPos_t_OP, old, _focus_logicalPos_t));
  }

  Focus_LogicalWellPos_t get focus_logicalWellPos_t => _focus_logicalWellPos_t;

  set focus_logicalWellPos_t(Focus_LogicalWellPos_t o) {
    if (o == _focus_logicalWellPos_t) return;
    assert(o == null || o is Focus_LogicalWellPos_t);
    if (_focus_logicalWellPos_t != null) _focus_logicalWellPos_t.parent = null;
    if (o != null) o.parent = this;
    var old = _focus_logicalWellPos_t;
    _focus_logicalWellPos_t = o;
    if (hasListener)
      this.sendChangeEvent(new base.PropertyChangedEvent(this,
          Vocabulary.focus_logicalWellPos_t_OP, old, _focus_logicalWellPos_t));
  }

  LEDIntensity_t get ledIntensity_t => _ledIntensity_t;

  set ledIntensity_t(LEDIntensity_t o) {
    if (o == _ledIntensity_t) return;
    assert(o == null || o is LEDIntensity_t);
    if (_ledIntensity_t != null) _ledIntensity_t.parent = null;
    if (o != null) o.parent = this;
    var old = _ledIntensity_t;
    _ledIntensity_t = o;
    if (hasListener)
      this.sendChangeEvent(new base.PropertyChangedEvent(
          this, Vocabulary.ledIntensity_t_OP, old, _ledIntensity_t));
  }

  Load_Unload_Settings_t get load_unload_settings_t => _load_unload_settings_t;

  set load_unload_settings_t(Load_Unload_Settings_t o) {
    if (o == _load_unload_settings_t) return;
    assert(o == null || o is Load_Unload_Settings_t);
    if (_load_unload_settings_t != null) _load_unload_settings_t.parent = null;
    if (o != null) o.parent = this;
    var old = _load_unload_settings_t;
    _load_unload_settings_t = o;
    if (hasListener)
      this.sendChangeEvent(new base.PropertyChangedEvent(this,
          Vocabulary.load_unload_settings_t_OP, old, _load_unload_settings_t));
  }

  PumpSettings_t get pumpSettings_t => _pumpSettings_t;

  set pumpSettings_t(PumpSettings_t o) {
    if (o == _pumpSettings_t) return;
    assert(o == null || o is PumpSettings_t);
    if (_pumpSettings_t != null) _pumpSettings_t.parent = null;
    if (o != null) o.parent = this;
    var old = _pumpSettings_t;
    _pumpSettings_t = o;
    if (hasListener)
      this.sendChangeEvent(new base.PropertyChangedEvent(
          this, Vocabulary.pumpSettings_t_OP, old, _pumpSettings_t));
  }

  AspirationSettings_t get aspirationSettings_t => _aspirationSettings_t;

  set aspirationSettings_t(AspirationSettings_t o) {
    if (o == _aspirationSettings_t) return;
    assert(o == null || o is AspirationSettings_t);
    if (_aspirationSettings_t != null) _aspirationSettings_t.parent = null;
    if (o != null) o.parent = this;
    var old = _aspirationSettings_t;
    _aspirationSettings_t = o;
    if (hasListener)
      this.sendChangeEvent(new base.PropertyChangedEvent(this,
          Vocabulary.aspirationSettings_t_OP, old, _aspirationSettings_t));
  }

  DispenseSettings_t get dispenseSettings_t => _dispenseSettings_t;

  set dispenseSettings_t(DispenseSettings_t o) {
    if (o == _dispenseSettings_t) return;
    assert(o == null || o is DispenseSettings_t);
    if (_dispenseSettings_t != null) _dispenseSettings_t.parent = null;
    if (o != null) o.parent = this;
    var old = _dispenseSettings_t;
    _dispenseSettings_t = o;
    if (hasListener)
      this.sendChangeEvent(new base.PropertyChangedEvent(
          this, Vocabulary.dispenseSettings_t_OP, old, _dispenseSettings_t));
  }

  base.ListChanged<Sensor_Calibration_Data_t> get sensorCalibrationData_t =>
      _sensorCalibrationData_t;

  base.ListChanged<ControllableSettings> get controllableSettings =>
      _controllableSettings;

  dynamic get(String name) {
    switch (name) {
      case Vocabulary.unitName_DP:
        return this.unitName;
      case Vocabulary.wagon_logicalXYPos_t_OP:
        return this.wagon_logicalXYPos_t;
      case Vocabulary.filter_logicalPos_t_OP:
        return this.filter_logicalPos_t;
      case Vocabulary.focus_logicalPos_t_OP:
        return this.focus_logicalPos_t;
      case Vocabulary.focus_logicalWellPos_t_OP:
        return this.focus_logicalWellPos_t;
      case Vocabulary.ledIntensity_t_OP:
        return this.ledIntensity_t;
      case Vocabulary.load_unload_settings_t_OP:
        return this.load_unload_settings_t;
      case Vocabulary.pumpSettings_t_OP:
        return this.pumpSettings_t;
      case Vocabulary.aspirationSettings_t_OP:
        return this.aspirationSettings_t;
      case Vocabulary.dispenseSettings_t_OP:
        return this.dispenseSettings_t;
      case Vocabulary.sensorCalibrationData_t_OP:
        return this.sensorCalibrationData_t;
      case Vocabulary.controllableSettings_OP:
        return this.controllableSettings;
      default:
        return super.get(name);
    }
  }

  set(String name, dynamic value) {
    switch (name) {
      case Vocabulary.unitName_DP:
        this.unitName = value as String;
        return;
      case Vocabulary.wagon_logicalXYPos_t_OP:
        this.wagon_logicalXYPos_t = value as Wagon_LogicalXYPos_t;
        return;
      case Vocabulary.filter_logicalPos_t_OP:
        this.filter_logicalPos_t = value as Filter_LogicalPos_t;
        return;
      case Vocabulary.focus_logicalPos_t_OP:
        this.focus_logicalPos_t = value as Focus_LogicalPos_t;
        return;
      case Vocabulary.focus_logicalWellPos_t_OP:
        this.focus_logicalWellPos_t = value as Focus_LogicalWellPos_t;
        return;
      case Vocabulary.ledIntensity_t_OP:
        this.ledIntensity_t = value as LEDIntensity_t;
        return;
      case Vocabulary.load_unload_settings_t_OP:
        this.load_unload_settings_t = value as Load_Unload_Settings_t;
        return;
      case Vocabulary.pumpSettings_t_OP:
        this.pumpSettings_t = value as PumpSettings_t;
        return;
      case Vocabulary.aspirationSettings_t_OP:
        this.aspirationSettings_t = value as AspirationSettings_t;
        return;
      case Vocabulary.dispenseSettings_t_OP:
        this.dispenseSettings_t = value as DispenseSettings_t;
        return;
      case Vocabulary.sensorCalibrationData_t_OP:
        _sensorCalibrationData_t
          ..clear()
          ..addAll(value as Iterable<Sensor_Calibration_Data_t>);
        return;
      case Vocabulary.controllableSettings_OP:
        _controllableSettings
          ..clear()
          ..addAll(value as Iterable<ControllableSettings>);
        return;
      default:
        super.set(name, value);
    }
  }

  List<String> getPropertyNames() =>
      new List.from(super.getPropertyNames(), growable: true)
        ..addAll(PROPERTY_NAMES);

  Settings copy() => new Settings.json(this.toJson());
  Map toJson() {
    var m = super.toJson() as Map;
    m[Vocabulary.KIND] = Vocabulary.Settings_CLASS;
    if (this.subKind != null && this.subKind != Vocabulary.Settings_CLASS)
      m[Vocabulary.SUBKIND] = this.subKind;
    else
      m.remove(Vocabulary.SUBKIND);
    m[Vocabulary.unitName_DP] = unitName;
    m[Vocabulary.wagon_logicalXYPos_t_OP] =
        wagon_logicalXYPos_t == null ? null : wagon_logicalXYPos_t.toJson();
    m[Vocabulary.filter_logicalPos_t_OP] =
        filter_logicalPos_t == null ? null : filter_logicalPos_t.toJson();
    m[Vocabulary.focus_logicalPos_t_OP] =
        focus_logicalPos_t == null ? null : focus_logicalPos_t.toJson();
    m[Vocabulary.focus_logicalWellPos_t_OP] =
        focus_logicalWellPos_t == null ? null : focus_logicalWellPos_t.toJson();
    m[Vocabulary.ledIntensity_t_OP] =
        ledIntensity_t == null ? null : ledIntensity_t.toJson();
    m[Vocabulary.load_unload_settings_t_OP] =
        load_unload_settings_t == null ? null : load_unload_settings_t.toJson();
    m[Vocabulary.pumpSettings_t_OP] =
        pumpSettings_t == null ? null : pumpSettings_t.toJson();
    m[Vocabulary.aspirationSettings_t_OP] =
        aspirationSettings_t == null ? null : aspirationSettings_t.toJson();
    m[Vocabulary.dispenseSettings_t_OP] =
        dispenseSettings_t == null ? null : dispenseSettings_t.toJson();
    m[Vocabulary.sensorCalibrationData_t_OP] = sensorCalibrationData_t.toJson();
    m[Vocabulary.controllableSettings_OP] = controllableSettings.toJson();
    return m;
  }
}
