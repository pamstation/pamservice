part of ps_model_base;

class ControllableSettingsBase extends base.Base {
  static const List<String> PROPERTY_NAMES = const [
    Vocabulary.eCtrlObj_DP,
    Vocabulary.eSensObj_DP,
    Vocabulary.eCurrSensObj_DP,
    Vocabulary.uWaitReadyTimeout_DP,
    Vocabulary.iDefaultPwm_DP,
    Vocabulary.rMaxProperty_DP,
    Vocabulary.rMinProperty_DP,
    Vocabulary.rTrackingError_DP,
    Vocabulary.rTolerance_DP,
    Vocabulary.rMaxCurrent_DP,
    Vocabulary.rMaxVelocity_DP,
    Vocabulary.rMinVelocity_DP,
    Vocabulary.rMaxAcceleration_DP,
    Vocabulary.rMinAcceleration_DP,
    Vocabulary.rDefaultVelocity_DP,
    Vocabulary.rDefaultAcceleration_DP,
    Vocabulary.uSettleTime_DP,
    Vocabulary.sd_KPp_DP,
    Vocabulary.sd_KIp_DP,
    Vocabulary.sd_KDp_DP,
    Vocabulary.sd_Integ_Lim_max_DP,
    Vocabulary.sd_Integ_Lim_min_DP,
    Vocabulary.KPi_DP,
    Vocabulary.KIi_DP,
    Vocabulary.DefaultVel_DP,
    Vocabulary.DefaultAcc_DP,
    Vocabulary.sf_KPp_DP,
    Vocabulary.sf_KIp_DP,
    Vocabulary.sf_KDp_DP,
    Vocabulary.va_scale_DP,
    Vocabulary.T_Settletime_DP,
    Vocabulary.Allow_Homing_DP,
    Vocabulary.Homing_tick_count_DP,
    Vocabulary.Homing_Speed_PWM_DP,
    Vocabulary.Homing_Seeking_PWM_DP,
    Vocabulary.max_output_DP,
    Vocabulary.min_output_DP,
    Vocabulary.max_setpoint_DP,
    Vocabulary.min_setpoint_DP,
    Vocabulary.max_tracking_error_DP,
    Vocabulary.max_tolerance_error_DP,
    Vocabulary.max_dc_i_DP,
    Vocabulary.max_dc_pwm_DP,
    Vocabulary.du_max_vel_DP,
    Vocabulary.du_min_vel_DP,
    Vocabulary.du_max_acc_DP,
    Vocabulary.du_min_acc_DP
  ];
  int _eCtrlObj;
  int _eSensObj;
  int _eCurrSensObj;
  int _uWaitReadyTimeout;
  int _iDefaultPwm;
  double _rMaxProperty;
  double _rMinProperty;
  double _rTrackingError;
  double _rTolerance;
  double _rMaxCurrent;
  double _rMaxVelocity;
  double _rMinVelocity;
  double _rMaxAcceleration;
  double _rMinAcceleration;
  double _rDefaultVelocity;
  double _rDefaultAcceleration;
  int _uSettleTime;
  int _sd_KPp;
  int _sd_KIp;
  int _sd_KDp;
  int _sd_Integ_Lim_max;
  int _sd_Integ_Lim_min;
  int _KPi;
  int _KIi;
  double _DefaultVel;
  double _DefaultAcc;
  int _sf_KPp;
  int _sf_KIp;
  int _sf_KDp;
  int _va_scale;
  int _T_Settletime;
  int _Allow_Homing;
  int _Homing_tick_count;
  int _Homing_Speed_PWM;
  int _Homing_Seeking_PWM;
  int _max_output;
  int _min_output;
  int _max_setpoint;
  int _min_setpoint;
  int _max_tracking_error;
  int _max_tolerance_error;
  int _max_dc_i;
  int _max_dc_pwm;
  double _du_max_vel;
  double _du_min_vel;
  double _du_max_acc;
  double _du_min_acc;

  ControllableSettingsBase() : super() {
    this.noEventDo(() {
      this.eCtrlObj = 0;
      this.eSensObj = 0;
      this.eCurrSensObj = 0;
      this.uWaitReadyTimeout = 0;
      this.iDefaultPwm = 0;
      this.rMaxProperty = 0.0;
      this.rMinProperty = 0.0;
      this.rTrackingError = 0.0;
      this.rTolerance = 0.0;
      this.rMaxCurrent = 0.0;
      this.rMaxVelocity = 0.0;
      this.rMinVelocity = 0.0;
      this.rMaxAcceleration = 0.0;
      this.rMinAcceleration = 0.0;
      this.rDefaultVelocity = 0.0;
      this.rDefaultAcceleration = 0.0;
      this.uSettleTime = 0;
      this.sd_KPp = 0;
      this.sd_KIp = 0;
      this.sd_KDp = 0;
      this.sd_Integ_Lim_max = 0;
      this.sd_Integ_Lim_min = 0;
      this.KPi = 0;
      this.KIi = 0;
      this.DefaultVel = 0.0;
      this.DefaultAcc = 0.0;
      this.sf_KPp = 0;
      this.sf_KIp = 0;
      this.sf_KDp = 0;
      this.va_scale = 0;
      this.T_Settletime = 0;
      this.Allow_Homing = 0;
      this.Homing_tick_count = 0;
      this.Homing_Speed_PWM = 0;
      this.Homing_Seeking_PWM = 0;
      this.max_output = 0;
      this.min_output = 0;
      this.max_setpoint = 0;
      this.min_setpoint = 0;
      this.max_tracking_error = 0;
      this.max_tolerance_error = 0;
      this.max_dc_i = 0;
      this.max_dc_pwm = 0;
      this.du_max_vel = 0.0;
      this.du_min_vel = 0.0;
      this.du_max_acc = 0.0;
      this.du_min_acc = 0.0;
    });
  }

  ControllableSettingsBase.json(Map m) : super.json(m) {
    this.noEventDo(() {
      this.subKind = m[Vocabulary.SUBKIND] != null
          ? m[Vocabulary.SUBKIND] as String
          : (m[Vocabulary.KIND] != Vocabulary.ControllableSettings_CLASS
              ? m[Vocabulary.KIND]
              : null) as String;
      this.eCtrlObj = m[Vocabulary.eCtrlObj_DP] as int;
      this.eSensObj = m[Vocabulary.eSensObj_DP] as int;
      this.eCurrSensObj = m[Vocabulary.eCurrSensObj_DP] as int;
      this.uWaitReadyTimeout = m[Vocabulary.uWaitReadyTimeout_DP] as int;
      this.iDefaultPwm = m[Vocabulary.iDefaultPwm_DP] as int;
      this.rMaxProperty = (m[Vocabulary.rMaxProperty_DP] as num).toDouble();
      this.rMinProperty = (m[Vocabulary.rMinProperty_DP] as num).toDouble();
      this.rTrackingError = (m[Vocabulary.rTrackingError_DP] as num).toDouble();
      this.rTolerance = (m[Vocabulary.rTolerance_DP] as num).toDouble();
      this.rMaxCurrent = (m[Vocabulary.rMaxCurrent_DP] as num).toDouble();
      this.rMaxVelocity = (m[Vocabulary.rMaxVelocity_DP] as num).toDouble();
      this.rMinVelocity = (m[Vocabulary.rMinVelocity_DP] as num).toDouble();
      this.rMaxAcceleration =
          (m[Vocabulary.rMaxAcceleration_DP] as num).toDouble();
      this.rMinAcceleration =
          (m[Vocabulary.rMinAcceleration_DP] as num).toDouble();
      this.rDefaultVelocity =
          (m[Vocabulary.rDefaultVelocity_DP] as num).toDouble();
      this.rDefaultAcceleration =
          (m[Vocabulary.rDefaultAcceleration_DP] as num).toDouble();
      this.uSettleTime = m[Vocabulary.uSettleTime_DP] as int;
      this.sd_KPp = m[Vocabulary.sd_KPp_DP] as int;
      this.sd_KIp = m[Vocabulary.sd_KIp_DP] as int;
      this.sd_KDp = m[Vocabulary.sd_KDp_DP] as int;
      this.sd_Integ_Lim_max = m[Vocabulary.sd_Integ_Lim_max_DP] as int;
      this.sd_Integ_Lim_min = m[Vocabulary.sd_Integ_Lim_min_DP] as int;
      this.KPi = m[Vocabulary.KPi_DP] as int;
      this.KIi = m[Vocabulary.KIi_DP] as int;
      this.DefaultVel = (m[Vocabulary.DefaultVel_DP] as num).toDouble();
      this.DefaultAcc = (m[Vocabulary.DefaultAcc_DP] as num).toDouble();
      this.sf_KPp = m[Vocabulary.sf_KPp_DP] as int;
      this.sf_KIp = m[Vocabulary.sf_KIp_DP] as int;
      this.sf_KDp = m[Vocabulary.sf_KDp_DP] as int;
      this.va_scale = m[Vocabulary.va_scale_DP] as int;
      this.T_Settletime = m[Vocabulary.T_Settletime_DP] as int;
      this.Allow_Homing = m[Vocabulary.Allow_Homing_DP] as int;
      this.Homing_tick_count = m[Vocabulary.Homing_tick_count_DP] as int;
      this.Homing_Speed_PWM = m[Vocabulary.Homing_Speed_PWM_DP] as int;
      this.Homing_Seeking_PWM = m[Vocabulary.Homing_Seeking_PWM_DP] as int;
      this.max_output = m[Vocabulary.max_output_DP] as int;
      this.min_output = m[Vocabulary.min_output_DP] as int;
      this.max_setpoint = m[Vocabulary.max_setpoint_DP] as int;
      this.min_setpoint = m[Vocabulary.min_setpoint_DP] as int;
      this.max_tracking_error = m[Vocabulary.max_tracking_error_DP] as int;
      this.max_tolerance_error = m[Vocabulary.max_tolerance_error_DP] as int;
      this.max_dc_i = m[Vocabulary.max_dc_i_DP] as int;
      this.max_dc_pwm = m[Vocabulary.max_dc_pwm_DP] as int;
      this.du_max_vel = (m[Vocabulary.du_max_vel_DP] as num).toDouble();
      this.du_min_vel = (m[Vocabulary.du_min_vel_DP] as num).toDouble();
      this.du_max_acc = (m[Vocabulary.du_max_acc_DP] as num).toDouble();
      this.du_min_acc = (m[Vocabulary.du_min_acc_DP] as num).toDouble();
    });
  }

  static ControllableSettings createFromJson(Map m) =>
      ControllableSettingsBase.fromJson(m);
  static ControllableSettings fromJson(Map m) {
    final kind = m[Vocabulary.KIND] as String;
    switch (kind) {
      case Vocabulary.ControllableSettings_CLASS:
        return new ControllableSettings.json(m);
      default:
        throw new ArgumentError.value(kind,
            "bad kind for class ControllableSettings in fromJson constructor");
    }
  }

  String get kind => Vocabulary.ControllableSettings_CLASS;
  int get eCtrlObj => _eCtrlObj;

  set eCtrlObj(int o) {
    if (o == _eCtrlObj) return;
    assert(o == null || o is int);
    var old = _eCtrlObj;
    _eCtrlObj = o;
    if (hasListener)
      this.sendChangeEvent(new base.PropertyChangedEvent(
          this, Vocabulary.eCtrlObj_DP, old, _eCtrlObj));
  }

  int get eSensObj => _eSensObj;

  set eSensObj(int o) {
    if (o == _eSensObj) return;
    assert(o == null || o is int);
    var old = _eSensObj;
    _eSensObj = o;
    if (hasListener)
      this.sendChangeEvent(new base.PropertyChangedEvent(
          this, Vocabulary.eSensObj_DP, old, _eSensObj));
  }

  int get eCurrSensObj => _eCurrSensObj;

  set eCurrSensObj(int o) {
    if (o == _eCurrSensObj) return;
    assert(o == null || o is int);
    var old = _eCurrSensObj;
    _eCurrSensObj = o;
    if (hasListener)
      this.sendChangeEvent(new base.PropertyChangedEvent(
          this, Vocabulary.eCurrSensObj_DP, old, _eCurrSensObj));
  }

  int get uWaitReadyTimeout => _uWaitReadyTimeout;

  set uWaitReadyTimeout(int o) {
    if (o == _uWaitReadyTimeout) return;
    assert(o == null || o is int);
    var old = _uWaitReadyTimeout;
    _uWaitReadyTimeout = o;
    if (hasListener)
      this.sendChangeEvent(new base.PropertyChangedEvent(
          this, Vocabulary.uWaitReadyTimeout_DP, old, _uWaitReadyTimeout));
  }

  int get iDefaultPwm => _iDefaultPwm;

  set iDefaultPwm(int o) {
    if (o == _iDefaultPwm) return;
    assert(o == null || o is int);
    var old = _iDefaultPwm;
    _iDefaultPwm = o;
    if (hasListener)
      this.sendChangeEvent(new base.PropertyChangedEvent(
          this, Vocabulary.iDefaultPwm_DP, old, _iDefaultPwm));
  }

  double get rMaxProperty => _rMaxProperty;

  set rMaxProperty(double o) {
    if (o == _rMaxProperty) return;
    assert(o == null || o is double);
    var old = _rMaxProperty;
    _rMaxProperty = o;
    if (hasListener)
      this.sendChangeEvent(new base.PropertyChangedEvent(
          this, Vocabulary.rMaxProperty_DP, old, _rMaxProperty));
  }

  double get rMinProperty => _rMinProperty;

  set rMinProperty(double o) {
    if (o == _rMinProperty) return;
    assert(o == null || o is double);
    var old = _rMinProperty;
    _rMinProperty = o;
    if (hasListener)
      this.sendChangeEvent(new base.PropertyChangedEvent(
          this, Vocabulary.rMinProperty_DP, old, _rMinProperty));
  }

  double get rTrackingError => _rTrackingError;

  set rTrackingError(double o) {
    if (o == _rTrackingError) return;
    assert(o == null || o is double);
    var old = _rTrackingError;
    _rTrackingError = o;
    if (hasListener)
      this.sendChangeEvent(new base.PropertyChangedEvent(
          this, Vocabulary.rTrackingError_DP, old, _rTrackingError));
  }

  double get rTolerance => _rTolerance;

  set rTolerance(double o) {
    if (o == _rTolerance) return;
    assert(o == null || o is double);
    var old = _rTolerance;
    _rTolerance = o;
    if (hasListener)
      this.sendChangeEvent(new base.PropertyChangedEvent(
          this, Vocabulary.rTolerance_DP, old, _rTolerance));
  }

  double get rMaxCurrent => _rMaxCurrent;

  set rMaxCurrent(double o) {
    if (o == _rMaxCurrent) return;
    assert(o == null || o is double);
    var old = _rMaxCurrent;
    _rMaxCurrent = o;
    if (hasListener)
      this.sendChangeEvent(new base.PropertyChangedEvent(
          this, Vocabulary.rMaxCurrent_DP, old, _rMaxCurrent));
  }

  double get rMaxVelocity => _rMaxVelocity;

  set rMaxVelocity(double o) {
    if (o == _rMaxVelocity) return;
    assert(o == null || o is double);
    var old = _rMaxVelocity;
    _rMaxVelocity = o;
    if (hasListener)
      this.sendChangeEvent(new base.PropertyChangedEvent(
          this, Vocabulary.rMaxVelocity_DP, old, _rMaxVelocity));
  }

  double get rMinVelocity => _rMinVelocity;

  set rMinVelocity(double o) {
    if (o == _rMinVelocity) return;
    assert(o == null || o is double);
    var old = _rMinVelocity;
    _rMinVelocity = o;
    if (hasListener)
      this.sendChangeEvent(new base.PropertyChangedEvent(
          this, Vocabulary.rMinVelocity_DP, old, _rMinVelocity));
  }

  double get rMaxAcceleration => _rMaxAcceleration;

  set rMaxAcceleration(double o) {
    if (o == _rMaxAcceleration) return;
    assert(o == null || o is double);
    var old = _rMaxAcceleration;
    _rMaxAcceleration = o;
    if (hasListener)
      this.sendChangeEvent(new base.PropertyChangedEvent(
          this, Vocabulary.rMaxAcceleration_DP, old, _rMaxAcceleration));
  }

  double get rMinAcceleration => _rMinAcceleration;

  set rMinAcceleration(double o) {
    if (o == _rMinAcceleration) return;
    assert(o == null || o is double);
    var old = _rMinAcceleration;
    _rMinAcceleration = o;
    if (hasListener)
      this.sendChangeEvent(new base.PropertyChangedEvent(
          this, Vocabulary.rMinAcceleration_DP, old, _rMinAcceleration));
  }

  double get rDefaultVelocity => _rDefaultVelocity;

  set rDefaultVelocity(double o) {
    if (o == _rDefaultVelocity) return;
    assert(o == null || o is double);
    var old = _rDefaultVelocity;
    _rDefaultVelocity = o;
    if (hasListener)
      this.sendChangeEvent(new base.PropertyChangedEvent(
          this, Vocabulary.rDefaultVelocity_DP, old, _rDefaultVelocity));
  }

  double get rDefaultAcceleration => _rDefaultAcceleration;

  set rDefaultAcceleration(double o) {
    if (o == _rDefaultAcceleration) return;
    assert(o == null || o is double);
    var old = _rDefaultAcceleration;
    _rDefaultAcceleration = o;
    if (hasListener)
      this.sendChangeEvent(new base.PropertyChangedEvent(this,
          Vocabulary.rDefaultAcceleration_DP, old, _rDefaultAcceleration));
  }

  int get uSettleTime => _uSettleTime;

  set uSettleTime(int o) {
    if (o == _uSettleTime) return;
    assert(o == null || o is int);
    var old = _uSettleTime;
    _uSettleTime = o;
    if (hasListener)
      this.sendChangeEvent(new base.PropertyChangedEvent(
          this, Vocabulary.uSettleTime_DP, old, _uSettleTime));
  }

  int get sd_KPp => _sd_KPp;

  set sd_KPp(int o) {
    if (o == _sd_KPp) return;
    assert(o == null || o is int);
    var old = _sd_KPp;
    _sd_KPp = o;
    if (hasListener)
      this.sendChangeEvent(new base.PropertyChangedEvent(
          this, Vocabulary.sd_KPp_DP, old, _sd_KPp));
  }

  int get sd_KIp => _sd_KIp;

  set sd_KIp(int o) {
    if (o == _sd_KIp) return;
    assert(o == null || o is int);
    var old = _sd_KIp;
    _sd_KIp = o;
    if (hasListener)
      this.sendChangeEvent(new base.PropertyChangedEvent(
          this, Vocabulary.sd_KIp_DP, old, _sd_KIp));
  }

  int get sd_KDp => _sd_KDp;

  set sd_KDp(int o) {
    if (o == _sd_KDp) return;
    assert(o == null || o is int);
    var old = _sd_KDp;
    _sd_KDp = o;
    if (hasListener)
      this.sendChangeEvent(new base.PropertyChangedEvent(
          this, Vocabulary.sd_KDp_DP, old, _sd_KDp));
  }

  int get sd_Integ_Lim_max => _sd_Integ_Lim_max;

  set sd_Integ_Lim_max(int o) {
    if (o == _sd_Integ_Lim_max) return;
    assert(o == null || o is int);
    var old = _sd_Integ_Lim_max;
    _sd_Integ_Lim_max = o;
    if (hasListener)
      this.sendChangeEvent(new base.PropertyChangedEvent(
          this, Vocabulary.sd_Integ_Lim_max_DP, old, _sd_Integ_Lim_max));
  }

  int get sd_Integ_Lim_min => _sd_Integ_Lim_min;

  set sd_Integ_Lim_min(int o) {
    if (o == _sd_Integ_Lim_min) return;
    assert(o == null || o is int);
    var old = _sd_Integ_Lim_min;
    _sd_Integ_Lim_min = o;
    if (hasListener)
      this.sendChangeEvent(new base.PropertyChangedEvent(
          this, Vocabulary.sd_Integ_Lim_min_DP, old, _sd_Integ_Lim_min));
  }

  int get KPi => _KPi;

  set KPi(int o) {
    if (o == _KPi) return;
    assert(o == null || o is int);
    var old = _KPi;
    _KPi = o;
    if (hasListener)
      this.sendChangeEvent(
          new base.PropertyChangedEvent(this, Vocabulary.KPi_DP, old, _KPi));
  }

  int get KIi => _KIi;

  set KIi(int o) {
    if (o == _KIi) return;
    assert(o == null || o is int);
    var old = _KIi;
    _KIi = o;
    if (hasListener)
      this.sendChangeEvent(
          new base.PropertyChangedEvent(this, Vocabulary.KIi_DP, old, _KIi));
  }

  double get DefaultVel => _DefaultVel;

  set DefaultVel(double o) {
    if (o == _DefaultVel) return;
    assert(o == null || o is double);
    var old = _DefaultVel;
    _DefaultVel = o;
    if (hasListener)
      this.sendChangeEvent(new base.PropertyChangedEvent(
          this, Vocabulary.DefaultVel_DP, old, _DefaultVel));
  }

  double get DefaultAcc => _DefaultAcc;

  set DefaultAcc(double o) {
    if (o == _DefaultAcc) return;
    assert(o == null || o is double);
    var old = _DefaultAcc;
    _DefaultAcc = o;
    if (hasListener)
      this.sendChangeEvent(new base.PropertyChangedEvent(
          this, Vocabulary.DefaultAcc_DP, old, _DefaultAcc));
  }

  int get sf_KPp => _sf_KPp;

  set sf_KPp(int o) {
    if (o == _sf_KPp) return;
    assert(o == null || o is int);
    var old = _sf_KPp;
    _sf_KPp = o;
    if (hasListener)
      this.sendChangeEvent(new base.PropertyChangedEvent(
          this, Vocabulary.sf_KPp_DP, old, _sf_KPp));
  }

  int get sf_KIp => _sf_KIp;

  set sf_KIp(int o) {
    if (o == _sf_KIp) return;
    assert(o == null || o is int);
    var old = _sf_KIp;
    _sf_KIp = o;
    if (hasListener)
      this.sendChangeEvent(new base.PropertyChangedEvent(
          this, Vocabulary.sf_KIp_DP, old, _sf_KIp));
  }

  int get sf_KDp => _sf_KDp;

  set sf_KDp(int o) {
    if (o == _sf_KDp) return;
    assert(o == null || o is int);
    var old = _sf_KDp;
    _sf_KDp = o;
    if (hasListener)
      this.sendChangeEvent(new base.PropertyChangedEvent(
          this, Vocabulary.sf_KDp_DP, old, _sf_KDp));
  }

  int get va_scale => _va_scale;

  set va_scale(int o) {
    if (o == _va_scale) return;
    assert(o == null || o is int);
    var old = _va_scale;
    _va_scale = o;
    if (hasListener)
      this.sendChangeEvent(new base.PropertyChangedEvent(
          this, Vocabulary.va_scale_DP, old, _va_scale));
  }

  int get T_Settletime => _T_Settletime;

  set T_Settletime(int o) {
    if (o == _T_Settletime) return;
    assert(o == null || o is int);
    var old = _T_Settletime;
    _T_Settletime = o;
    if (hasListener)
      this.sendChangeEvent(new base.PropertyChangedEvent(
          this, Vocabulary.T_Settletime_DP, old, _T_Settletime));
  }

  int get Allow_Homing => _Allow_Homing;

  set Allow_Homing(int o) {
    if (o == _Allow_Homing) return;
    assert(o == null || o is int);
    var old = _Allow_Homing;
    _Allow_Homing = o;
    if (hasListener)
      this.sendChangeEvent(new base.PropertyChangedEvent(
          this, Vocabulary.Allow_Homing_DP, old, _Allow_Homing));
  }

  int get Homing_tick_count => _Homing_tick_count;

  set Homing_tick_count(int o) {
    if (o == _Homing_tick_count) return;
    assert(o == null || o is int);
    var old = _Homing_tick_count;
    _Homing_tick_count = o;
    if (hasListener)
      this.sendChangeEvent(new base.PropertyChangedEvent(
          this, Vocabulary.Homing_tick_count_DP, old, _Homing_tick_count));
  }

  int get Homing_Speed_PWM => _Homing_Speed_PWM;

  set Homing_Speed_PWM(int o) {
    if (o == _Homing_Speed_PWM) return;
    assert(o == null || o is int);
    var old = _Homing_Speed_PWM;
    _Homing_Speed_PWM = o;
    if (hasListener)
      this.sendChangeEvent(new base.PropertyChangedEvent(
          this, Vocabulary.Homing_Speed_PWM_DP, old, _Homing_Speed_PWM));
  }

  int get Homing_Seeking_PWM => _Homing_Seeking_PWM;

  set Homing_Seeking_PWM(int o) {
    if (o == _Homing_Seeking_PWM) return;
    assert(o == null || o is int);
    var old = _Homing_Seeking_PWM;
    _Homing_Seeking_PWM = o;
    if (hasListener)
      this.sendChangeEvent(new base.PropertyChangedEvent(
          this, Vocabulary.Homing_Seeking_PWM_DP, old, _Homing_Seeking_PWM));
  }

  int get max_output => _max_output;

  set max_output(int o) {
    if (o == _max_output) return;
    assert(o == null || o is int);
    var old = _max_output;
    _max_output = o;
    if (hasListener)
      this.sendChangeEvent(new base.PropertyChangedEvent(
          this, Vocabulary.max_output_DP, old, _max_output));
  }

  int get min_output => _min_output;

  set min_output(int o) {
    if (o == _min_output) return;
    assert(o == null || o is int);
    var old = _min_output;
    _min_output = o;
    if (hasListener)
      this.sendChangeEvent(new base.PropertyChangedEvent(
          this, Vocabulary.min_output_DP, old, _min_output));
  }

  int get max_setpoint => _max_setpoint;

  set max_setpoint(int o) {
    if (o == _max_setpoint) return;
    assert(o == null || o is int);
    var old = _max_setpoint;
    _max_setpoint = o;
    if (hasListener)
      this.sendChangeEvent(new base.PropertyChangedEvent(
          this, Vocabulary.max_setpoint_DP, old, _max_setpoint));
  }

  int get min_setpoint => _min_setpoint;

  set min_setpoint(int o) {
    if (o == _min_setpoint) return;
    assert(o == null || o is int);
    var old = _min_setpoint;
    _min_setpoint = o;
    if (hasListener)
      this.sendChangeEvent(new base.PropertyChangedEvent(
          this, Vocabulary.min_setpoint_DP, old, _min_setpoint));
  }

  int get max_tracking_error => _max_tracking_error;

  set max_tracking_error(int o) {
    if (o == _max_tracking_error) return;
    assert(o == null || o is int);
    var old = _max_tracking_error;
    _max_tracking_error = o;
    if (hasListener)
      this.sendChangeEvent(new base.PropertyChangedEvent(
          this, Vocabulary.max_tracking_error_DP, old, _max_tracking_error));
  }

  int get max_tolerance_error => _max_tolerance_error;

  set max_tolerance_error(int o) {
    if (o == _max_tolerance_error) return;
    assert(o == null || o is int);
    var old = _max_tolerance_error;
    _max_tolerance_error = o;
    if (hasListener)
      this.sendChangeEvent(new base.PropertyChangedEvent(
          this, Vocabulary.max_tolerance_error_DP, old, _max_tolerance_error));
  }

  int get max_dc_i => _max_dc_i;

  set max_dc_i(int o) {
    if (o == _max_dc_i) return;
    assert(o == null || o is int);
    var old = _max_dc_i;
    _max_dc_i = o;
    if (hasListener)
      this.sendChangeEvent(new base.PropertyChangedEvent(
          this, Vocabulary.max_dc_i_DP, old, _max_dc_i));
  }

  int get max_dc_pwm => _max_dc_pwm;

  set max_dc_pwm(int o) {
    if (o == _max_dc_pwm) return;
    assert(o == null || o is int);
    var old = _max_dc_pwm;
    _max_dc_pwm = o;
    if (hasListener)
      this.sendChangeEvent(new base.PropertyChangedEvent(
          this, Vocabulary.max_dc_pwm_DP, old, _max_dc_pwm));
  }

  double get du_max_vel => _du_max_vel;

  set du_max_vel(double o) {
    if (o == _du_max_vel) return;
    assert(o == null || o is double);
    var old = _du_max_vel;
    _du_max_vel = o;
    if (hasListener)
      this.sendChangeEvent(new base.PropertyChangedEvent(
          this, Vocabulary.du_max_vel_DP, old, _du_max_vel));
  }

  double get du_min_vel => _du_min_vel;

  set du_min_vel(double o) {
    if (o == _du_min_vel) return;
    assert(o == null || o is double);
    var old = _du_min_vel;
    _du_min_vel = o;
    if (hasListener)
      this.sendChangeEvent(new base.PropertyChangedEvent(
          this, Vocabulary.du_min_vel_DP, old, _du_min_vel));
  }

  double get du_max_acc => _du_max_acc;

  set du_max_acc(double o) {
    if (o == _du_max_acc) return;
    assert(o == null || o is double);
    var old = _du_max_acc;
    _du_max_acc = o;
    if (hasListener)
      this.sendChangeEvent(new base.PropertyChangedEvent(
          this, Vocabulary.du_max_acc_DP, old, _du_max_acc));
  }

  double get du_min_acc => _du_min_acc;

  set du_min_acc(double o) {
    if (o == _du_min_acc) return;
    assert(o == null || o is double);
    var old = _du_min_acc;
    _du_min_acc = o;
    if (hasListener)
      this.sendChangeEvent(new base.PropertyChangedEvent(
          this, Vocabulary.du_min_acc_DP, old, _du_min_acc));
  }

  dynamic get(String name) {
    switch (name) {
      case Vocabulary.eCtrlObj_DP:
        return this.eCtrlObj;
      case Vocabulary.eSensObj_DP:
        return this.eSensObj;
      case Vocabulary.eCurrSensObj_DP:
        return this.eCurrSensObj;
      case Vocabulary.uWaitReadyTimeout_DP:
        return this.uWaitReadyTimeout;
      case Vocabulary.iDefaultPwm_DP:
        return this.iDefaultPwm;
      case Vocabulary.rMaxProperty_DP:
        return this.rMaxProperty;
      case Vocabulary.rMinProperty_DP:
        return this.rMinProperty;
      case Vocabulary.rTrackingError_DP:
        return this.rTrackingError;
      case Vocabulary.rTolerance_DP:
        return this.rTolerance;
      case Vocabulary.rMaxCurrent_DP:
        return this.rMaxCurrent;
      case Vocabulary.rMaxVelocity_DP:
        return this.rMaxVelocity;
      case Vocabulary.rMinVelocity_DP:
        return this.rMinVelocity;
      case Vocabulary.rMaxAcceleration_DP:
        return this.rMaxAcceleration;
      case Vocabulary.rMinAcceleration_DP:
        return this.rMinAcceleration;
      case Vocabulary.rDefaultVelocity_DP:
        return this.rDefaultVelocity;
      case Vocabulary.rDefaultAcceleration_DP:
        return this.rDefaultAcceleration;
      case Vocabulary.uSettleTime_DP:
        return this.uSettleTime;
      case Vocabulary.sd_KPp_DP:
        return this.sd_KPp;
      case Vocabulary.sd_KIp_DP:
        return this.sd_KIp;
      case Vocabulary.sd_KDp_DP:
        return this.sd_KDp;
      case Vocabulary.sd_Integ_Lim_max_DP:
        return this.sd_Integ_Lim_max;
      case Vocabulary.sd_Integ_Lim_min_DP:
        return this.sd_Integ_Lim_min;
      case Vocabulary.KPi_DP:
        return this.KPi;
      case Vocabulary.KIi_DP:
        return this.KIi;
      case Vocabulary.DefaultVel_DP:
        return this.DefaultVel;
      case Vocabulary.DefaultAcc_DP:
        return this.DefaultAcc;
      case Vocabulary.sf_KPp_DP:
        return this.sf_KPp;
      case Vocabulary.sf_KIp_DP:
        return this.sf_KIp;
      case Vocabulary.sf_KDp_DP:
        return this.sf_KDp;
      case Vocabulary.va_scale_DP:
        return this.va_scale;
      case Vocabulary.T_Settletime_DP:
        return this.T_Settletime;
      case Vocabulary.Allow_Homing_DP:
        return this.Allow_Homing;
      case Vocabulary.Homing_tick_count_DP:
        return this.Homing_tick_count;
      case Vocabulary.Homing_Speed_PWM_DP:
        return this.Homing_Speed_PWM;
      case Vocabulary.Homing_Seeking_PWM_DP:
        return this.Homing_Seeking_PWM;
      case Vocabulary.max_output_DP:
        return this.max_output;
      case Vocabulary.min_output_DP:
        return this.min_output;
      case Vocabulary.max_setpoint_DP:
        return this.max_setpoint;
      case Vocabulary.min_setpoint_DP:
        return this.min_setpoint;
      case Vocabulary.max_tracking_error_DP:
        return this.max_tracking_error;
      case Vocabulary.max_tolerance_error_DP:
        return this.max_tolerance_error;
      case Vocabulary.max_dc_i_DP:
        return this.max_dc_i;
      case Vocabulary.max_dc_pwm_DP:
        return this.max_dc_pwm;
      case Vocabulary.du_max_vel_DP:
        return this.du_max_vel;
      case Vocabulary.du_min_vel_DP:
        return this.du_min_vel;
      case Vocabulary.du_max_acc_DP:
        return this.du_max_acc;
      case Vocabulary.du_min_acc_DP:
        return this.du_min_acc;
      default:
        return super.get(name);
    }
  }

  set(String name, dynamic value) {
    switch (name) {
      case Vocabulary.eCtrlObj_DP:
        this.eCtrlObj = value as int;
        return;
      case Vocabulary.eSensObj_DP:
        this.eSensObj = value as int;
        return;
      case Vocabulary.eCurrSensObj_DP:
        this.eCurrSensObj = value as int;
        return;
      case Vocabulary.uWaitReadyTimeout_DP:
        this.uWaitReadyTimeout = value as int;
        return;
      case Vocabulary.iDefaultPwm_DP:
        this.iDefaultPwm = value as int;
        return;
      case Vocabulary.rMaxProperty_DP:
        this.rMaxProperty = value as double;
        return;
      case Vocabulary.rMinProperty_DP:
        this.rMinProperty = value as double;
        return;
      case Vocabulary.rTrackingError_DP:
        this.rTrackingError = value as double;
        return;
      case Vocabulary.rTolerance_DP:
        this.rTolerance = value as double;
        return;
      case Vocabulary.rMaxCurrent_DP:
        this.rMaxCurrent = value as double;
        return;
      case Vocabulary.rMaxVelocity_DP:
        this.rMaxVelocity = value as double;
        return;
      case Vocabulary.rMinVelocity_DP:
        this.rMinVelocity = value as double;
        return;
      case Vocabulary.rMaxAcceleration_DP:
        this.rMaxAcceleration = value as double;
        return;
      case Vocabulary.rMinAcceleration_DP:
        this.rMinAcceleration = value as double;
        return;
      case Vocabulary.rDefaultVelocity_DP:
        this.rDefaultVelocity = value as double;
        return;
      case Vocabulary.rDefaultAcceleration_DP:
        this.rDefaultAcceleration = value as double;
        return;
      case Vocabulary.uSettleTime_DP:
        this.uSettleTime = value as int;
        return;
      case Vocabulary.sd_KPp_DP:
        this.sd_KPp = value as int;
        return;
      case Vocabulary.sd_KIp_DP:
        this.sd_KIp = value as int;
        return;
      case Vocabulary.sd_KDp_DP:
        this.sd_KDp = value as int;
        return;
      case Vocabulary.sd_Integ_Lim_max_DP:
        this.sd_Integ_Lim_max = value as int;
        return;
      case Vocabulary.sd_Integ_Lim_min_DP:
        this.sd_Integ_Lim_min = value as int;
        return;
      case Vocabulary.KPi_DP:
        this.KPi = value as int;
        return;
      case Vocabulary.KIi_DP:
        this.KIi = value as int;
        return;
      case Vocabulary.DefaultVel_DP:
        this.DefaultVel = value as double;
        return;
      case Vocabulary.DefaultAcc_DP:
        this.DefaultAcc = value as double;
        return;
      case Vocabulary.sf_KPp_DP:
        this.sf_KPp = value as int;
        return;
      case Vocabulary.sf_KIp_DP:
        this.sf_KIp = value as int;
        return;
      case Vocabulary.sf_KDp_DP:
        this.sf_KDp = value as int;
        return;
      case Vocabulary.va_scale_DP:
        this.va_scale = value as int;
        return;
      case Vocabulary.T_Settletime_DP:
        this.T_Settletime = value as int;
        return;
      case Vocabulary.Allow_Homing_DP:
        this.Allow_Homing = value as int;
        return;
      case Vocabulary.Homing_tick_count_DP:
        this.Homing_tick_count = value as int;
        return;
      case Vocabulary.Homing_Speed_PWM_DP:
        this.Homing_Speed_PWM = value as int;
        return;
      case Vocabulary.Homing_Seeking_PWM_DP:
        this.Homing_Seeking_PWM = value as int;
        return;
      case Vocabulary.max_output_DP:
        this.max_output = value as int;
        return;
      case Vocabulary.min_output_DP:
        this.min_output = value as int;
        return;
      case Vocabulary.max_setpoint_DP:
        this.max_setpoint = value as int;
        return;
      case Vocabulary.min_setpoint_DP:
        this.min_setpoint = value as int;
        return;
      case Vocabulary.max_tracking_error_DP:
        this.max_tracking_error = value as int;
        return;
      case Vocabulary.max_tolerance_error_DP:
        this.max_tolerance_error = value as int;
        return;
      case Vocabulary.max_dc_i_DP:
        this.max_dc_i = value as int;
        return;
      case Vocabulary.max_dc_pwm_DP:
        this.max_dc_pwm = value as int;
        return;
      case Vocabulary.du_max_vel_DP:
        this.du_max_vel = value as double;
        return;
      case Vocabulary.du_min_vel_DP:
        this.du_min_vel = value as double;
        return;
      case Vocabulary.du_max_acc_DP:
        this.du_max_acc = value as double;
        return;
      case Vocabulary.du_min_acc_DP:
        this.du_min_acc = value as double;
        return;
      default:
        super.set(name, value);
    }
  }

  List<String> getPropertyNames() =>
      new List.from(super.getPropertyNames(), growable: true)
        ..addAll(PROPERTY_NAMES);

  ControllableSettings copy() => new ControllableSettings.json(this.toJson());
  Map toJson() {
    var m = super.toJson() as Map;
    m[Vocabulary.KIND] = Vocabulary.ControllableSettings_CLASS;
    if (this.subKind != null &&
        this.subKind != Vocabulary.ControllableSettings_CLASS)
      m[Vocabulary.SUBKIND] = this.subKind;
    else
      m.remove(Vocabulary.SUBKIND);
    m[Vocabulary.eCtrlObj_DP] = eCtrlObj;
    m[Vocabulary.eSensObj_DP] = eSensObj;
    m[Vocabulary.eCurrSensObj_DP] = eCurrSensObj;
    m[Vocabulary.uWaitReadyTimeout_DP] = uWaitReadyTimeout;
    m[Vocabulary.iDefaultPwm_DP] = iDefaultPwm;
    m[Vocabulary.rMaxProperty_DP] = rMaxProperty;
    m[Vocabulary.rMinProperty_DP] = rMinProperty;
    m[Vocabulary.rTrackingError_DP] = rTrackingError;
    m[Vocabulary.rTolerance_DP] = rTolerance;
    m[Vocabulary.rMaxCurrent_DP] = rMaxCurrent;
    m[Vocabulary.rMaxVelocity_DP] = rMaxVelocity;
    m[Vocabulary.rMinVelocity_DP] = rMinVelocity;
    m[Vocabulary.rMaxAcceleration_DP] = rMaxAcceleration;
    m[Vocabulary.rMinAcceleration_DP] = rMinAcceleration;
    m[Vocabulary.rDefaultVelocity_DP] = rDefaultVelocity;
    m[Vocabulary.rDefaultAcceleration_DP] = rDefaultAcceleration;
    m[Vocabulary.uSettleTime_DP] = uSettleTime;
    m[Vocabulary.sd_KPp_DP] = sd_KPp;
    m[Vocabulary.sd_KIp_DP] = sd_KIp;
    m[Vocabulary.sd_KDp_DP] = sd_KDp;
    m[Vocabulary.sd_Integ_Lim_max_DP] = sd_Integ_Lim_max;
    m[Vocabulary.sd_Integ_Lim_min_DP] = sd_Integ_Lim_min;
    m[Vocabulary.KPi_DP] = KPi;
    m[Vocabulary.KIi_DP] = KIi;
    m[Vocabulary.DefaultVel_DP] = DefaultVel;
    m[Vocabulary.DefaultAcc_DP] = DefaultAcc;
    m[Vocabulary.sf_KPp_DP] = sf_KPp;
    m[Vocabulary.sf_KIp_DP] = sf_KIp;
    m[Vocabulary.sf_KDp_DP] = sf_KDp;
    m[Vocabulary.va_scale_DP] = va_scale;
    m[Vocabulary.T_Settletime_DP] = T_Settletime;
    m[Vocabulary.Allow_Homing_DP] = Allow_Homing;
    m[Vocabulary.Homing_tick_count_DP] = Homing_tick_count;
    m[Vocabulary.Homing_Speed_PWM_DP] = Homing_Speed_PWM;
    m[Vocabulary.Homing_Seeking_PWM_DP] = Homing_Seeking_PWM;
    m[Vocabulary.max_output_DP] = max_output;
    m[Vocabulary.min_output_DP] = min_output;
    m[Vocabulary.max_setpoint_DP] = max_setpoint;
    m[Vocabulary.min_setpoint_DP] = min_setpoint;
    m[Vocabulary.max_tracking_error_DP] = max_tracking_error;
    m[Vocabulary.max_tolerance_error_DP] = max_tolerance_error;
    m[Vocabulary.max_dc_i_DP] = max_dc_i;
    m[Vocabulary.max_dc_pwm_DP] = max_dc_pwm;
    m[Vocabulary.du_max_vel_DP] = du_max_vel;
    m[Vocabulary.du_min_vel_DP] = du_min_vel;
    m[Vocabulary.du_max_acc_DP] = du_max_acc;
    m[Vocabulary.du_min_acc_DP] = du_min_acc;
    return m;
  }
}
