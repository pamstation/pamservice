part of ps_model_base;

class Focus_LogicalWellPos_tBase extends base.Base {
  static const List<String> PROPERTY_NAMES = const [
    Vocabulary.rPosForFilter1_DP,
    Vocabulary.rPosForFilter2_DP,
    Vocabulary.rPosForFilter3_DP,
    Vocabulary.rOffsetForWell1_DP,
    Vocabulary.rOffsetForWell2_DP,
    Vocabulary.rOffsetForWell3_DP,
    Vocabulary.rOffsetForWell4_DP,
    Vocabulary.rOffsetForWell5_DP,
    Vocabulary.rOffsetForWell6_DP,
    Vocabulary.rOffsetForWell7_DP,
    Vocabulary.rOffsetForWell8_DP,
    Vocabulary.rOffsetForWell9_DP,
    Vocabulary.rOffsetForWell10_DP,
    Vocabulary.rOffsetForWell11_DP,
    Vocabulary.rOffsetForWell12_DP
  ];
  double _rPosForFilter1;
  double _rPosForFilter2;
  double _rPosForFilter3;
  double _rOffsetForWell1;
  double _rOffsetForWell2;
  double _rOffsetForWell3;
  double _rOffsetForWell4;
  double _rOffsetForWell5;
  double _rOffsetForWell6;
  double _rOffsetForWell7;
  double _rOffsetForWell8;
  double _rOffsetForWell9;
  double _rOffsetForWell10;
  double _rOffsetForWell11;
  double _rOffsetForWell12;

  Focus_LogicalWellPos_tBase() : super() {
    this.noEventDo(() {
      this.rPosForFilter1 = 0.0;
      this.rPosForFilter2 = 0.0;
      this.rPosForFilter3 = 0.0;
      this.rOffsetForWell1 = 0.0;
      this.rOffsetForWell2 = 0.0;
      this.rOffsetForWell3 = 0.0;
      this.rOffsetForWell4 = 0.0;
      this.rOffsetForWell5 = 0.0;
      this.rOffsetForWell6 = 0.0;
      this.rOffsetForWell7 = 0.0;
      this.rOffsetForWell8 = 0.0;
      this.rOffsetForWell9 = 0.0;
      this.rOffsetForWell10 = 0.0;
      this.rOffsetForWell11 = 0.0;
      this.rOffsetForWell12 = 0.0;
    });
  }

  Focus_LogicalWellPos_tBase.json(Map m) : super.json(m) {
    this.noEventDo(() {
      this.subKind = m[Vocabulary.SUBKIND] != null
          ? m[Vocabulary.SUBKIND] as String
          : (m[Vocabulary.KIND] != Vocabulary.Focus_LogicalWellPos_t_CLASS
              ? m[Vocabulary.KIND]
              : null) as String;
      this.rPosForFilter1 = (m[Vocabulary.rPosForFilter1_DP] as num).toDouble();
      this.rPosForFilter2 = (m[Vocabulary.rPosForFilter2_DP] as num).toDouble();
      this.rPosForFilter3 = (m[Vocabulary.rPosForFilter3_DP] as num).toDouble();
      this.rOffsetForWell1 =
          (m[Vocabulary.rOffsetForWell1_DP] as num).toDouble();
      this.rOffsetForWell2 =
          (m[Vocabulary.rOffsetForWell2_DP] as num).toDouble();
      this.rOffsetForWell3 =
          (m[Vocabulary.rOffsetForWell3_DP] as num).toDouble();
      this.rOffsetForWell4 =
          (m[Vocabulary.rOffsetForWell4_DP] as num).toDouble();
      this.rOffsetForWell5 =
          (m[Vocabulary.rOffsetForWell5_DP] as num).toDouble();
      this.rOffsetForWell6 =
          (m[Vocabulary.rOffsetForWell6_DP] as num).toDouble();
      this.rOffsetForWell7 =
          (m[Vocabulary.rOffsetForWell7_DP] as num).toDouble();
      this.rOffsetForWell8 =
          (m[Vocabulary.rOffsetForWell8_DP] as num).toDouble();
      this.rOffsetForWell9 =
          (m[Vocabulary.rOffsetForWell9_DP] as num).toDouble();
      this.rOffsetForWell10 =
          (m[Vocabulary.rOffsetForWell10_DP] as num).toDouble();
      this.rOffsetForWell11 =
          (m[Vocabulary.rOffsetForWell11_DP] as num).toDouble();
      this.rOffsetForWell12 =
          (m[Vocabulary.rOffsetForWell12_DP] as num).toDouble();
    });
  }

  static Focus_LogicalWellPos_t createFromJson(Map m) =>
      Focus_LogicalWellPos_tBase.fromJson(m);
  static Focus_LogicalWellPos_t fromJson(Map m) {
    final kind = m[Vocabulary.KIND] as String;
    switch (kind) {
      case Vocabulary.Focus_LogicalWellPos_t_CLASS:
        return new Focus_LogicalWellPos_t.json(m);
      default:
        throw new ArgumentError.value(kind,
            "bad kind for class Focus_LogicalWellPos_t in fromJson constructor");
    }
  }

  String get kind => Vocabulary.Focus_LogicalWellPos_t_CLASS;
  double get rPosForFilter1 => _rPosForFilter1;

  set rPosForFilter1(double o) {
    if (o == _rPosForFilter1) return;
    assert(o == null || o is double);
    var old = _rPosForFilter1;
    _rPosForFilter1 = o;
    if (hasListener)
      this.sendChangeEvent(new base.PropertyChangedEvent(
          this, Vocabulary.rPosForFilter1_DP, old, _rPosForFilter1));
  }

  double get rPosForFilter2 => _rPosForFilter2;

  set rPosForFilter2(double o) {
    if (o == _rPosForFilter2) return;
    assert(o == null || o is double);
    var old = _rPosForFilter2;
    _rPosForFilter2 = o;
    if (hasListener)
      this.sendChangeEvent(new base.PropertyChangedEvent(
          this, Vocabulary.rPosForFilter2_DP, old, _rPosForFilter2));
  }

  double get rPosForFilter3 => _rPosForFilter3;

  set rPosForFilter3(double o) {
    if (o == _rPosForFilter3) return;
    assert(o == null || o is double);
    var old = _rPosForFilter3;
    _rPosForFilter3 = o;
    if (hasListener)
      this.sendChangeEvent(new base.PropertyChangedEvent(
          this, Vocabulary.rPosForFilter3_DP, old, _rPosForFilter3));
  }

  double get rOffsetForWell1 => _rOffsetForWell1;

  set rOffsetForWell1(double o) {
    if (o == _rOffsetForWell1) return;
    assert(o == null || o is double);
    var old = _rOffsetForWell1;
    _rOffsetForWell1 = o;
    if (hasListener)
      this.sendChangeEvent(new base.PropertyChangedEvent(
          this, Vocabulary.rOffsetForWell1_DP, old, _rOffsetForWell1));
  }

  double get rOffsetForWell2 => _rOffsetForWell2;

  set rOffsetForWell2(double o) {
    if (o == _rOffsetForWell2) return;
    assert(o == null || o is double);
    var old = _rOffsetForWell2;
    _rOffsetForWell2 = o;
    if (hasListener)
      this.sendChangeEvent(new base.PropertyChangedEvent(
          this, Vocabulary.rOffsetForWell2_DP, old, _rOffsetForWell2));
  }

  double get rOffsetForWell3 => _rOffsetForWell3;

  set rOffsetForWell3(double o) {
    if (o == _rOffsetForWell3) return;
    assert(o == null || o is double);
    var old = _rOffsetForWell3;
    _rOffsetForWell3 = o;
    if (hasListener)
      this.sendChangeEvent(new base.PropertyChangedEvent(
          this, Vocabulary.rOffsetForWell3_DP, old, _rOffsetForWell3));
  }

  double get rOffsetForWell4 => _rOffsetForWell4;

  set rOffsetForWell4(double o) {
    if (o == _rOffsetForWell4) return;
    assert(o == null || o is double);
    var old = _rOffsetForWell4;
    _rOffsetForWell4 = o;
    if (hasListener)
      this.sendChangeEvent(new base.PropertyChangedEvent(
          this, Vocabulary.rOffsetForWell4_DP, old, _rOffsetForWell4));
  }

  double get rOffsetForWell5 => _rOffsetForWell5;

  set rOffsetForWell5(double o) {
    if (o == _rOffsetForWell5) return;
    assert(o == null || o is double);
    var old = _rOffsetForWell5;
    _rOffsetForWell5 = o;
    if (hasListener)
      this.sendChangeEvent(new base.PropertyChangedEvent(
          this, Vocabulary.rOffsetForWell5_DP, old, _rOffsetForWell5));
  }

  double get rOffsetForWell6 => _rOffsetForWell6;

  set rOffsetForWell6(double o) {
    if (o == _rOffsetForWell6) return;
    assert(o == null || o is double);
    var old = _rOffsetForWell6;
    _rOffsetForWell6 = o;
    if (hasListener)
      this.sendChangeEvent(new base.PropertyChangedEvent(
          this, Vocabulary.rOffsetForWell6_DP, old, _rOffsetForWell6));
  }

  double get rOffsetForWell7 => _rOffsetForWell7;

  set rOffsetForWell7(double o) {
    if (o == _rOffsetForWell7) return;
    assert(o == null || o is double);
    var old = _rOffsetForWell7;
    _rOffsetForWell7 = o;
    if (hasListener)
      this.sendChangeEvent(new base.PropertyChangedEvent(
          this, Vocabulary.rOffsetForWell7_DP, old, _rOffsetForWell7));
  }

  double get rOffsetForWell8 => _rOffsetForWell8;

  set rOffsetForWell8(double o) {
    if (o == _rOffsetForWell8) return;
    assert(o == null || o is double);
    var old = _rOffsetForWell8;
    _rOffsetForWell8 = o;
    if (hasListener)
      this.sendChangeEvent(new base.PropertyChangedEvent(
          this, Vocabulary.rOffsetForWell8_DP, old, _rOffsetForWell8));
  }

  double get rOffsetForWell9 => _rOffsetForWell9;

  set rOffsetForWell9(double o) {
    if (o == _rOffsetForWell9) return;
    assert(o == null || o is double);
    var old = _rOffsetForWell9;
    _rOffsetForWell9 = o;
    if (hasListener)
      this.sendChangeEvent(new base.PropertyChangedEvent(
          this, Vocabulary.rOffsetForWell9_DP, old, _rOffsetForWell9));
  }

  double get rOffsetForWell10 => _rOffsetForWell10;

  set rOffsetForWell10(double o) {
    if (o == _rOffsetForWell10) return;
    assert(o == null || o is double);
    var old = _rOffsetForWell10;
    _rOffsetForWell10 = o;
    if (hasListener)
      this.sendChangeEvent(new base.PropertyChangedEvent(
          this, Vocabulary.rOffsetForWell10_DP, old, _rOffsetForWell10));
  }

  double get rOffsetForWell11 => _rOffsetForWell11;

  set rOffsetForWell11(double o) {
    if (o == _rOffsetForWell11) return;
    assert(o == null || o is double);
    var old = _rOffsetForWell11;
    _rOffsetForWell11 = o;
    if (hasListener)
      this.sendChangeEvent(new base.PropertyChangedEvent(
          this, Vocabulary.rOffsetForWell11_DP, old, _rOffsetForWell11));
  }

  double get rOffsetForWell12 => _rOffsetForWell12;

  set rOffsetForWell12(double o) {
    if (o == _rOffsetForWell12) return;
    assert(o == null || o is double);
    var old = _rOffsetForWell12;
    _rOffsetForWell12 = o;
    if (hasListener)
      this.sendChangeEvent(new base.PropertyChangedEvent(
          this, Vocabulary.rOffsetForWell12_DP, old, _rOffsetForWell12));
  }

  dynamic get(String name) {
    switch (name) {
      case Vocabulary.rPosForFilter1_DP:
        return this.rPosForFilter1;
      case Vocabulary.rPosForFilter2_DP:
        return this.rPosForFilter2;
      case Vocabulary.rPosForFilter3_DP:
        return this.rPosForFilter3;
      case Vocabulary.rOffsetForWell1_DP:
        return this.rOffsetForWell1;
      case Vocabulary.rOffsetForWell2_DP:
        return this.rOffsetForWell2;
      case Vocabulary.rOffsetForWell3_DP:
        return this.rOffsetForWell3;
      case Vocabulary.rOffsetForWell4_DP:
        return this.rOffsetForWell4;
      case Vocabulary.rOffsetForWell5_DP:
        return this.rOffsetForWell5;
      case Vocabulary.rOffsetForWell6_DP:
        return this.rOffsetForWell6;
      case Vocabulary.rOffsetForWell7_DP:
        return this.rOffsetForWell7;
      case Vocabulary.rOffsetForWell8_DP:
        return this.rOffsetForWell8;
      case Vocabulary.rOffsetForWell9_DP:
        return this.rOffsetForWell9;
      case Vocabulary.rOffsetForWell10_DP:
        return this.rOffsetForWell10;
      case Vocabulary.rOffsetForWell11_DP:
        return this.rOffsetForWell11;
      case Vocabulary.rOffsetForWell12_DP:
        return this.rOffsetForWell12;
      default:
        return super.get(name);
    }
  }

  set(String name, dynamic value) {
    switch (name) {
      case Vocabulary.rPosForFilter1_DP:
        this.rPosForFilter1 = value as double;
        return;
      case Vocabulary.rPosForFilter2_DP:
        this.rPosForFilter2 = value as double;
        return;
      case Vocabulary.rPosForFilter3_DP:
        this.rPosForFilter3 = value as double;
        return;
      case Vocabulary.rOffsetForWell1_DP:
        this.rOffsetForWell1 = value as double;
        return;
      case Vocabulary.rOffsetForWell2_DP:
        this.rOffsetForWell2 = value as double;
        return;
      case Vocabulary.rOffsetForWell3_DP:
        this.rOffsetForWell3 = value as double;
        return;
      case Vocabulary.rOffsetForWell4_DP:
        this.rOffsetForWell4 = value as double;
        return;
      case Vocabulary.rOffsetForWell5_DP:
        this.rOffsetForWell5 = value as double;
        return;
      case Vocabulary.rOffsetForWell6_DP:
        this.rOffsetForWell6 = value as double;
        return;
      case Vocabulary.rOffsetForWell7_DP:
        this.rOffsetForWell7 = value as double;
        return;
      case Vocabulary.rOffsetForWell8_DP:
        this.rOffsetForWell8 = value as double;
        return;
      case Vocabulary.rOffsetForWell9_DP:
        this.rOffsetForWell9 = value as double;
        return;
      case Vocabulary.rOffsetForWell10_DP:
        this.rOffsetForWell10 = value as double;
        return;
      case Vocabulary.rOffsetForWell11_DP:
        this.rOffsetForWell11 = value as double;
        return;
      case Vocabulary.rOffsetForWell12_DP:
        this.rOffsetForWell12 = value as double;
        return;
      default:
        super.set(name, value);
    }
  }

  List<String> getPropertyNames() =>
      new List.from(super.getPropertyNames(), growable: true)
        ..addAll(PROPERTY_NAMES);

  Focus_LogicalWellPos_t copy() =>
      new Focus_LogicalWellPos_t.json(this.toJson());
  Map toJson() {
    var m = super.toJson() as Map;
    m[Vocabulary.KIND] = Vocabulary.Focus_LogicalWellPos_t_CLASS;
    if (this.subKind != null &&
        this.subKind != Vocabulary.Focus_LogicalWellPos_t_CLASS)
      m[Vocabulary.SUBKIND] = this.subKind;
    else
      m.remove(Vocabulary.SUBKIND);
    m[Vocabulary.rPosForFilter1_DP] = rPosForFilter1;
    m[Vocabulary.rPosForFilter2_DP] = rPosForFilter2;
    m[Vocabulary.rPosForFilter3_DP] = rPosForFilter3;
    m[Vocabulary.rOffsetForWell1_DP] = rOffsetForWell1;
    m[Vocabulary.rOffsetForWell2_DP] = rOffsetForWell2;
    m[Vocabulary.rOffsetForWell3_DP] = rOffsetForWell3;
    m[Vocabulary.rOffsetForWell4_DP] = rOffsetForWell4;
    m[Vocabulary.rOffsetForWell5_DP] = rOffsetForWell5;
    m[Vocabulary.rOffsetForWell6_DP] = rOffsetForWell6;
    m[Vocabulary.rOffsetForWell7_DP] = rOffsetForWell7;
    m[Vocabulary.rOffsetForWell8_DP] = rOffsetForWell8;
    m[Vocabulary.rOffsetForWell9_DP] = rOffsetForWell9;
    m[Vocabulary.rOffsetForWell10_DP] = rOffsetForWell10;
    m[Vocabulary.rOffsetForWell11_DP] = rOffsetForWell11;
    m[Vocabulary.rOffsetForWell12_DP] = rOffsetForWell12;
    return m;
  }
}
