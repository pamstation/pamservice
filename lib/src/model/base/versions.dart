part of ps_model_base;

class VersionsBase extends base.Base {
  static const List<String> PROPERTY_NAMES = const [
    Vocabulary.pamservice_OP,
    Vocabulary.psserver_OP,
    Vocabulary.linux_OP
  ];
  Version _pamservice;
  Version _psserver;
  LinuxVersion _linux;

  VersionsBase() : super() {
    this.noEventDo(() {
      this.pamservice = new Version();
      this.psserver = new Version();
      this.linux = new LinuxVersion();
    });
  }

  VersionsBase.json(Map m) : super.json(m) {
    this.noEventDo(() {
      this.subKind = m[Vocabulary.SUBKIND] != null
          ? m[Vocabulary.SUBKIND] as String
          : (m[Vocabulary.KIND] != Vocabulary.Versions_CLASS
              ? m[Vocabulary.KIND]
              : null) as String;
      if (m[Vocabulary.pamservice_OP] == null)
        this.pamservice = new Version();
      else
        this.pamservice =
            VersionBase.fromJson(m[Vocabulary.pamservice_OP] as Map);
      if (m[Vocabulary.psserver_OP] == null)
        this.psserver = new Version();
      else
        this.psserver = VersionBase.fromJson(m[Vocabulary.psserver_OP] as Map);
      if (m[Vocabulary.linux_OP] == null)
        this.linux = new LinuxVersion();
      else
        this.linux = LinuxVersionBase.fromJson(m[Vocabulary.linux_OP] as Map);
    });
  }

  static Versions createFromJson(Map m) => VersionsBase.fromJson(m);
  static Versions fromJson(Map m) {
    final kind = m[Vocabulary.KIND] as String;
    switch (kind) {
      case Vocabulary.Versions_CLASS:
        return new Versions.json(m);
      default:
        throw new ArgumentError.value(
            kind, "bad kind for class Versions in fromJson constructor");
    }
  }

  String get kind => Vocabulary.Versions_CLASS;
  Version get pamservice => _pamservice;

  set pamservice(Version o) {
    if (o == _pamservice) return;
    assert(o == null || o is Version);
    if (_pamservice != null) _pamservice.parent = null;
    if (o != null) o.parent = this;
    var old = _pamservice;
    _pamservice = o;
    if (hasListener)
      this.sendChangeEvent(new base.PropertyChangedEvent(
          this, Vocabulary.pamservice_OP, old, _pamservice));
  }

  Version get psserver => _psserver;

  set psserver(Version o) {
    if (o == _psserver) return;
    assert(o == null || o is Version);
    if (_psserver != null) _psserver.parent = null;
    if (o != null) o.parent = this;
    var old = _psserver;
    _psserver = o;
    if (hasListener)
      this.sendChangeEvent(new base.PropertyChangedEvent(
          this, Vocabulary.psserver_OP, old, _psserver));
  }

  LinuxVersion get linux => _linux;

  set linux(LinuxVersion o) {
    if (o == _linux) return;
    assert(o == null || o is LinuxVersion);
    if (_linux != null) _linux.parent = null;
    if (o != null) o.parent = this;
    var old = _linux;
    _linux = o;
    if (hasListener)
      this.sendChangeEvent(new base.PropertyChangedEvent(
          this, Vocabulary.linux_OP, old, _linux));
  }

  dynamic get(String name) {
    switch (name) {
      case Vocabulary.pamservice_OP:
        return this.pamservice;
      case Vocabulary.psserver_OP:
        return this.psserver;
      case Vocabulary.linux_OP:
        return this.linux;
      default:
        return super.get(name);
    }
  }

  set(String name, dynamic value) {
    switch (name) {
      case Vocabulary.pamservice_OP:
        this.pamservice = value as Version;
        return;
      case Vocabulary.psserver_OP:
        this.psserver = value as Version;
        return;
      case Vocabulary.linux_OP:
        this.linux = value as LinuxVersion;
        return;
      default:
        super.set(name, value);
    }
  }

  List<String> getPropertyNames() =>
      new List.from(super.getPropertyNames(), growable: true)
        ..addAll(PROPERTY_NAMES);

  Versions copy() => new Versions.json(this.toJson());
  Map toJson() {
    var m = super.toJson() as Map;
    m[Vocabulary.KIND] = Vocabulary.Versions_CLASS;
    if (this.subKind != null && this.subKind != Vocabulary.Versions_CLASS)
      m[Vocabulary.SUBKIND] = this.subKind;
    else
      m.remove(Vocabulary.SUBKIND);
    m[Vocabulary.pamservice_OP] =
        pamservice == null ? null : pamservice.toJson();
    m[Vocabulary.psserver_OP] = psserver == null ? null : psserver.toJson();
    m[Vocabulary.linux_OP] = linux == null ? null : linux.toJson();
    return m;
  }
}
