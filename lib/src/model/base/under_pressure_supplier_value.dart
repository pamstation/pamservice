part of ps_model_base;

class UnderPressureSupplierValueBase extends base.Base {
  static const List<String> PROPERTY_NAMES = const [Vocabulary.value_DP];
  double _value;

  UnderPressureSupplierValueBase() : super() {
    this.noEventDo(() {
      this.value = 0.0;
    });
  }

  UnderPressureSupplierValueBase.json(Map m) : super.json(m) {
    this.noEventDo(() {
      this.subKind = m[Vocabulary.SUBKIND] != null
          ? m[Vocabulary.SUBKIND] as String
          : (m[Vocabulary.KIND] != Vocabulary.UnderPressureSupplierValue_CLASS
              ? m[Vocabulary.KIND]
              : null) as String;
      this.value = (m[Vocabulary.value_DP] as num).toDouble();
    });
  }

  static UnderPressureSupplierValue createFromJson(Map m) =>
      UnderPressureSupplierValueBase.fromJson(m);
  static UnderPressureSupplierValue fromJson(Map m) {
    final kind = m[Vocabulary.KIND] as String;
    switch (kind) {
      case Vocabulary.UnderPressureSupplierValue_CLASS:
        return new UnderPressureSupplierValue.json(m);
      default:
        throw new ArgumentError.value(kind,
            "bad kind for class UnderPressureSupplierValue in fromJson constructor");
    }
  }

  String get kind => Vocabulary.UnderPressureSupplierValue_CLASS;
  double get value => _value;

  set value(double o) {
    if (o == _value) return;
    assert(o == null || o is double);
    var old = _value;
    _value = o;
    if (hasListener)
      this.sendChangeEvent(new base.PropertyChangedEvent(
          this, Vocabulary.value_DP, old, _value));
  }

  dynamic get(String name) {
    switch (name) {
      case Vocabulary.value_DP:
        return this.value;
      default:
        return super.get(name);
    }
  }

  set(String name, dynamic value) {
    switch (name) {
      case Vocabulary.value_DP:
        this.value = value as double;
        return;
      default:
        super.set(name, value);
    }
  }

  List<String> getPropertyNames() =>
      new List.from(super.getPropertyNames(), growable: true)
        ..addAll(PROPERTY_NAMES);

  UnderPressureSupplierValue copy() =>
      new UnderPressureSupplierValue.json(this.toJson());
  Map toJson() {
    var m = super.toJson() as Map;
    m[Vocabulary.KIND] = Vocabulary.UnderPressureSupplierValue_CLASS;
    if (this.subKind != null &&
        this.subKind != Vocabulary.UnderPressureSupplierValue_CLASS)
      m[Vocabulary.SUBKIND] = this.subKind;
    else
      m.remove(Vocabulary.SUBKIND);
    m[Vocabulary.value_DP] = value;
    return m;
  }
}
