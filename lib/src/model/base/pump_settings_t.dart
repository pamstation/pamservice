part of ps_model_base;

class PumpSettings_tBase extends base.Base {
  static const List<String> PROPERTY_NAMES = const [
    Vocabulary.uValveOpenTime_DP,
    Vocabulary.uPressureSettlingDelay_DP,
    Vocabulary.uFluidFlowDelay_DP,
    Vocabulary.rBrokenMembranePressure_DP,
    Vocabulary.uMaximumWaitingTime_DP
  ];
  int _uValveOpenTime;
  int _uPressureSettlingDelay;
  int _uFluidFlowDelay;
  double _rBrokenMembranePressure;
  int _uMaximumWaitingTime;

  PumpSettings_tBase() : super() {
    this.noEventDo(() {
      this.uValveOpenTime = 0;
      this.uPressureSettlingDelay = 0;
      this.uFluidFlowDelay = 0;
      this.rBrokenMembranePressure = 0.0;
      this.uMaximumWaitingTime = 0;
    });
  }

  PumpSettings_tBase.json(Map m) : super.json(m) {
    this.noEventDo(() {
      this.subKind = m[Vocabulary.SUBKIND] != null
          ? m[Vocabulary.SUBKIND] as String
          : (m[Vocabulary.KIND] != Vocabulary.PumpSettings_t_CLASS
              ? m[Vocabulary.KIND]
              : null) as String;
      this.uValveOpenTime = m[Vocabulary.uValveOpenTime_DP] as int;
      this.uPressureSettlingDelay =
          m[Vocabulary.uPressureSettlingDelay_DP] as int;
      this.uFluidFlowDelay = m[Vocabulary.uFluidFlowDelay_DP] as int;
      this.rBrokenMembranePressure =
          (m[Vocabulary.rBrokenMembranePressure_DP] as num).toDouble();
      this.uMaximumWaitingTime = m[Vocabulary.uMaximumWaitingTime_DP] as int;
    });
  }

  static PumpSettings_t createFromJson(Map m) => PumpSettings_tBase.fromJson(m);
  static PumpSettings_t fromJson(Map m) {
    final kind = m[Vocabulary.KIND] as String;
    switch (kind) {
      case Vocabulary.PumpSettings_t_CLASS:
        return new PumpSettings_t.json(m);
      default:
        throw new ArgumentError.value(
            kind, "bad kind for class PumpSettings_t in fromJson constructor");
    }
  }

  String get kind => Vocabulary.PumpSettings_t_CLASS;
  int get uValveOpenTime => _uValveOpenTime;

  set uValveOpenTime(int o) {
    if (o == _uValveOpenTime) return;
    assert(o == null || o is int);
    var old = _uValveOpenTime;
    _uValveOpenTime = o;
    if (hasListener)
      this.sendChangeEvent(new base.PropertyChangedEvent(
          this, Vocabulary.uValveOpenTime_DP, old, _uValveOpenTime));
  }

  int get uPressureSettlingDelay => _uPressureSettlingDelay;

  set uPressureSettlingDelay(int o) {
    if (o == _uPressureSettlingDelay) return;
    assert(o == null || o is int);
    var old = _uPressureSettlingDelay;
    _uPressureSettlingDelay = o;
    if (hasListener)
      this.sendChangeEvent(new base.PropertyChangedEvent(this,
          Vocabulary.uPressureSettlingDelay_DP, old, _uPressureSettlingDelay));
  }

  int get uFluidFlowDelay => _uFluidFlowDelay;

  set uFluidFlowDelay(int o) {
    if (o == _uFluidFlowDelay) return;
    assert(o == null || o is int);
    var old = _uFluidFlowDelay;
    _uFluidFlowDelay = o;
    if (hasListener)
      this.sendChangeEvent(new base.PropertyChangedEvent(
          this, Vocabulary.uFluidFlowDelay_DP, old, _uFluidFlowDelay));
  }

  double get rBrokenMembranePressure => _rBrokenMembranePressure;

  set rBrokenMembranePressure(double o) {
    if (o == _rBrokenMembranePressure) return;
    assert(o == null || o is double);
    var old = _rBrokenMembranePressure;
    _rBrokenMembranePressure = o;
    if (hasListener)
      this.sendChangeEvent(new base.PropertyChangedEvent(
          this,
          Vocabulary.rBrokenMembranePressure_DP,
          old,
          _rBrokenMembranePressure));
  }

  int get uMaximumWaitingTime => _uMaximumWaitingTime;

  set uMaximumWaitingTime(int o) {
    if (o == _uMaximumWaitingTime) return;
    assert(o == null || o is int);
    var old = _uMaximumWaitingTime;
    _uMaximumWaitingTime = o;
    if (hasListener)
      this.sendChangeEvent(new base.PropertyChangedEvent(
          this, Vocabulary.uMaximumWaitingTime_DP, old, _uMaximumWaitingTime));
  }

  dynamic get(String name) {
    switch (name) {
      case Vocabulary.uValveOpenTime_DP:
        return this.uValveOpenTime;
      case Vocabulary.uPressureSettlingDelay_DP:
        return this.uPressureSettlingDelay;
      case Vocabulary.uFluidFlowDelay_DP:
        return this.uFluidFlowDelay;
      case Vocabulary.rBrokenMembranePressure_DP:
        return this.rBrokenMembranePressure;
      case Vocabulary.uMaximumWaitingTime_DP:
        return this.uMaximumWaitingTime;
      default:
        return super.get(name);
    }
  }

  set(String name, dynamic value) {
    switch (name) {
      case Vocabulary.uValveOpenTime_DP:
        this.uValveOpenTime = value as int;
        return;
      case Vocabulary.uPressureSettlingDelay_DP:
        this.uPressureSettlingDelay = value as int;
        return;
      case Vocabulary.uFluidFlowDelay_DP:
        this.uFluidFlowDelay = value as int;
        return;
      case Vocabulary.rBrokenMembranePressure_DP:
        this.rBrokenMembranePressure = value as double;
        return;
      case Vocabulary.uMaximumWaitingTime_DP:
        this.uMaximumWaitingTime = value as int;
        return;
      default:
        super.set(name, value);
    }
  }

  List<String> getPropertyNames() =>
      new List.from(super.getPropertyNames(), growable: true)
        ..addAll(PROPERTY_NAMES);

  PumpSettings_t copy() => new PumpSettings_t.json(this.toJson());
  Map toJson() {
    var m = super.toJson() as Map;
    m[Vocabulary.KIND] = Vocabulary.PumpSettings_t_CLASS;
    if (this.subKind != null && this.subKind != Vocabulary.PumpSettings_t_CLASS)
      m[Vocabulary.SUBKIND] = this.subKind;
    else
      m.remove(Vocabulary.SUBKIND);
    m[Vocabulary.uValveOpenTime_DP] = uValveOpenTime;
    m[Vocabulary.uPressureSettlingDelay_DP] = uPressureSettlingDelay;
    m[Vocabulary.uFluidFlowDelay_DP] = uFluidFlowDelay;
    m[Vocabulary.rBrokenMembranePressure_DP] = rBrokenMembranePressure;
    m[Vocabulary.uMaximumWaitingTime_DP] = uMaximumWaitingTime;
    return m;
  }
}
