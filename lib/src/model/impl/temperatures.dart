part of ps_model;

class Temperatures extends TemperaturesBase {
  Temperatures() : super();
  Temperatures.json(Map m) : super.json(m);

  double coverLeft;
  double coverMiddle;
  double coverRight;
  double bottomLeft;
  double bottomMiddle;
  double bottomRight;

  static List<String> get temperatureNames => [
        'coverLeft',
        'coverMiddle',
        'coverRight',
        'bottomLeft',
        'bottomMiddle',
        'bottomRight'
      ];

  List<double> get temperatures => [
        coverLeft,
        coverMiddle,
        coverRight,
        bottomLeft,
        bottomMiddle,
        bottomRight
      ];
}
