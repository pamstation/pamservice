part of ps_model;

class Version extends VersionBase {
  Version() : super();
  Version.json(Map m) : super.json(m);

  String getString() {
    return '${name} : ${major}.${minor}.${patch} ${date}';
  }
}
