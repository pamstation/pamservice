import 'dart:async';
import 'dart:convert';
import 'dart:io';
import 'package:xml/xml.dart' as xml;
import 'package:http/http.dart' as http;
import 'package:path/path.dart' as libpath;

class PS12 {
  Uri baseUri;
  Button button;
  StreamController<ProtocolMessage> _streamController;

  Stream<ProtocolMessage> messages() => _streamController.stream;

  PS12(this.baseUri) {
    _streamController = StreamController.broadcast(
        onCancel: _onMessageCancel, onListen: _onMessageListen);
    button = new Button(baseUri.replace(path: '/api/button'));
  }

  bool _listen = false;

  int _messageCount = 0;

  void _onMessageCancel() {
    _listen = false;
  }

  _onMessageListen() async {
    _listen = true;

    while (_listen) {
      var msgs = await messageQueue(index: _messageCount);

      _messageCount += msgs.length;

      for (var msg in msgs) {
        if (_listen) {
          _streamController.add(msg);
        }
      }

      await Future.delayed(Duration(seconds: 1));
    }
  }

  Protocol protocolFromFile(String file) =>
      new Protocol.fromFile(baseUri, file);

  Future<List<ProtocolMessage>> messageQueue({int index: 0}) async {
    var url = baseUri.replace(path: '/api/msgqueue');
    var resp = await http.post(url, body: index.toString());
    if (resp.statusCode != 200) {
      var error = new PSError.fromXml(resp.statusCode, resp.body);
      throw error;
    }

    var document = xml.parse(resp.body);

    var ll = document.findAllElements('list');

    if (ll.isNotEmpty) {
      var list = ll.first;

      return list.children
          .map((each) => ProtocolMessage.fromXmlNode(each))
          .cast<ProtocolMessage>()
          .toList();
    }

    return [];
  }

  Future<Protocol> getCurrentProtocol() async {
    var url = baseUri.replace(path: '/api/protocol');
    var resp = await http.get(url);
    if (resp.statusCode != 200) {
      var error = new PSError.fromXml(resp.statusCode, resp.body);
      if (error.code == 'protocol.null') return null;
      throw error;
    }
    return new Protocol(baseUri, resp.body);
  }

  Future ping() async {
    var url = baseUri.replace(path: '/api/ping');
    var resp = await http.get(url);
    if (resp.statusCode != 200) {
      throw 'ping fails : ${resp.body}';
    }
  }

  Future<FocusSettings> getFocusSetting() async {
    var url = baseUri.replace(path: '/api/focussettings');
    var resp = await http.get(url);
    if (resp.statusCode != 200) {
      throw 'getFocusSetting fails : ${resp.body}';
    }
    return new FocusSettings.fromXML(resp.body);
  }

  Future setFocusSetting(FocusSettings focusSettings) async {
    var url = baseUri.replace(path: '/api/focussettings');
    var resp = await http.post(url,
        body: focusSettings.toXML(),
        headers: {'content-type': 'application/x-www-form-urlencoded'});
    if (resp.statusCode != 200) {
      throw 'setFocusSetting fails : resp.statusCode ${resp.statusCode} : "${resp.body}" ${resp.headers}';
    }
  }

  Future<String> get status async {
    var url = baseUri.replace(path: '/api/status');
    var resp = await http.get(url);
    if (resp.statusCode != 200) {
      throw 'status fails : ${resp.body}';
    }
    var state;
    var document = xml.parse(resp.body);
    var serverElement = document.findAllElements('server').first;
    serverElement.attributes.forEach((a) {
      if (a.name.local == 'state') {
        state = a.value;
      }
    });
    return state;
  }

  Future turnOn() async {
    var url = baseUri.replace(path: '/api/turnOn');
    var resp = await http.get(url);
    if (resp.statusCode != 200) {
      throw 'turnOn fails : ${resp.body}';
    }
  }

  Future shutdown() async {
    var url = baseUri.replace(path: '/api/shutdown');
    var resp = await http.get(url);
    if (resp.statusCode != 200) {
      throw 'shutdown fails : ${resp.body}';
    }
  }
}

class Button {
  Uri uri;
//  bool _status;
  StreamController<bool> _controller;

  int stateId;

  Button(this.uri) {
    stateId = -1;
    bool hasListener;
    _controller = new StreamController(onListen: () {
      hasListener = true;
      new Timer.periodic(new Duration(seconds: 1), (t) {
        if (!hasListener) {
          t.cancel();
          return;
        }
        Future.sync(() async {
          var btnStatus = await buttonStatus;
          if (stateId != btnStatus.stateId) {
            stateId = btnStatus.stateId;
            _controller.add(btnStatus.asBool);
          }
        });
      });
    }, onCancel: () {
      hasListener = false;
    });
  }

  Stream<bool> get onStateChange => _controller.stream;

  Future<bool> get status async => (await buttonStatus).asBool;

  Future<ButtonStatus> get buttonStatus async {
    var resp = await http.get(uri);
    if (resp.statusCode != 200) throw 'buttom status fails : ${resp.body}';
    var document = xml.parse(resp.body);
    return ButtonStatus.fromElement(document.findAllElements('button').first);
  }

  Future press() async {
    var resp = await http.put(uri);
    if (resp.statusCode != 200) {
      throw 'buttom press fails : ${resp.body}';
    }
  }
}

class ButtonStatus {
  String status;
  int stateId;

  ButtonStatus.fromElement(xml.XmlElement buttonElement) {
    buttonElement.attributes.forEach((a) {
      if (a.name.local == 'status') {
        status = a.value;
      }
      if (a.name.local == 'stateId') {
        stateId = int.parse(a.value);
      }
    });
  }

  bool get asBool => status != '0';
}

class PSError {
  String code;
  String description;

  PSError.fromXml(int httpStatus, String xmlStr) {
    code = 'unknown';
    description = '';
    xml.XmlDocument document;
    try {
      document = xml.parse(xmlStr);
      var buttonElement = document.findAllElements('error').first;
      buttonElement.attributes.forEach((a) {
        if (a.name.local == 'code') {
          code = a.value;
        }
        if (a.name.local == 'description') {
          description = a.value;
        }
      });
    } catch (e, st) {
      print(e);
      print(st);
      code = 'unknown';
      description = 'httpStatus ${httpStatus} : $xmlStr';
    }
  }
}

class Protocol {
  static const String initialized = 'initialized';
  static const String running = 'running';
  static const String aborting = 'aborting';
  static const String aborted = 'aborted';
  static const String completed = 'completed';

  Uri baseUri;

  String xmlProtocol;
//  List<ProtocolMessage> _messages;
  StreamController<ProtocolMessage> _streamController;

  Protocol(this.baseUri, this.xmlProtocol) {
    _streamController = StreamController.broadcast(
        onCancel: _onMessageCancel, onListen: _onMessageListen);
//    _messages = [];
  }

  void setPath(String path) {
    xmlProtocol = xmlProtocol.replaceAll('{{path}}', path);
  }

  void setBarcodes(List<String> barcodes) {
    assert(barcodes.length == 3);
    xmlProtocol = xmlProtocol.replaceAll('{{barcode1}}', barcodes[0]);
    xmlProtocol = xmlProtocol.replaceAll('{{barcode2}}', barcodes[1]);
    xmlProtocol = xmlProtocol.replaceAll('{{barcode3}}', barcodes[2]);
  }

  Protocol.fromFile(this.baseUri, String file) {
    xmlProtocol = new File(file).readAsStringSync();
    _streamController = StreamController.broadcast(
        onCancel: _onMessageCancel, onListen: _onMessageListen);
//    _messages = [];
  }

  Future load() async {
    var url = baseUri.replace(path: '/api/protocol');
    var resp = await http.put(url,
        body: xmlProtocol,
        headers: {'content-type': 'application/x-www-form-urlencoded'});
    if (resp.statusCode != 200) {
      throw 'load fails : ${resp.body}';
    }
  }

  Stream<ProtocolMessage> protocolMessages() => _streamController.stream;

  Stream<ImageMessage> images() => protocolMessages()
      .where((msg) => msg is ImageMessage)
      .cast<ImageMessage>();

  bool _listen = false;
  int _messageCount = 0;

  void _onMessageCancel() {
    _listen = false;
  }

  _onMessageListen() async {
    _listen = true;

    try {
      while (_listen) {
        var msgs = await msgQueue(index: _messageCount);

        _messageCount += msgs.length;

        for (var msg in msgs) {
          if (_listen) {
            _streamController.add(msg);
          }
        }

        if (await isDone()) {
          _streamController.close();
          _listen = false;
          return;
        }

        await Future.delayed(Duration(seconds: 1));
      }
    } catch (e) {
      _streamController.addError(e);
    }
  }

  Future<List<ProtocolMessage>> msgQueue({int index: 0}) async {
    var url = baseUri.replace(path: '/api/protocol/msgqueue');
    var resp = await http.post(url, body: index.toString());
    if (resp.statusCode != 200) {
      throw 'msgQueue fails : ${resp.body}';
    }
    var document = xml.parse(resp.body);

    var list = document.findAllElements('list').first;

    return list.children
        .map((each) => ProtocolMessage.fromXmlNode(each))
        .cast<ProtocolMessage>()
        .toList();
  }

  Future saveImage(String uri, String localFolder) async {
    var url = baseUri.replace(path: uri);
    var resp = await http.get(url);
    if (resp.statusCode != 200) {
      throw 'saveImage fails : ${resp.body}';
    }

    var file = File(libpath.join(localFolder, uri));
    file.parent.createSync(recursive: true);
    file.writeAsBytesSync(resp.bodyBytes);
  }

  Future<String> getState() async {
    var msg = await msgQueue();
    var list = msg.whereType<StateMessage>();
    if (list.isEmpty) throw 'invalid state';
    return list.last.value;
  }

  Future<ErrorMessage> getError() async {
    var msg = await msgQueue();
    var list = msg.whereType<ErrorMessage>();
    if (list.isEmpty) return null;
    return list.first;
  }

  Future run() async {
    var url = baseUri.replace(path: '/api/protocol/run');
    var resp = await http.get(url);
    if (resp.statusCode != 200) {
      throw 'run fails : ${resp.body}';
    }
  }

  Future wait() async {
    while (!await isDone()) {
      var error = await getError();
      if (error != null) throw error;
      await new Future.delayed(new Duration(seconds: 5));
    }
  }

  Future<bool> isDone() async {
    var state = await getState();
    return state == completed || state == aborted;
  }

  Future unload() async {
    var url = baseUri.replace(path: '/api/protocol/unload');
    var resp = await http.get(url);
    if (resp.statusCode != 200) {
      throw 'unload fails : ${resp.body}';
    }
  }

  Future abort() async {
    var url = baseUri.replace(path: '/api/protocol/abort');
    var resp = await http.get(url);
    if (resp.statusCode != 200) {
      throw 'abort fails : ${resp.body}';
    }
  }
}

class FocusSettings {
  FocusSettings.fromXML(String str) {
    var document = xml.parse(str);

    print(str);

    var focusSettingElement = document.findAllElements('FocusSettings').first;

    focusSettingElement.attributes.forEach((a) {
      if (a.name.local == 'rPosForFilter1') {
        rPosForFilter1 = double.parse(a.value);
      } else if (a.name.local == 'rPosForFilter2') {
        rPosForFilter2 = double.parse(a.value);
      } else if (a.name.local == 'rPosForFilter3') {
        rPosForFilter3 = double.parse(a.value);
      } else if (a.name.local == 'rOffsetForDisp1') {
        rOffsetForDisp1 = double.parse(a.value);
      } else if (a.name.local == 'rOffsetForDisp2') {
        rOffsetForDisp2 = double.parse(a.value);
      } else if (a.name.local == 'rOffsetForDisp3') {
        rOffsetForDisp3 = double.parse(a.value);
      }
    });
  }
  double rPosForFilter1;
  double rPosForFilter2;
  double rPosForFilter3;

  double rOffsetForDisp1;
  double rOffsetForDisp2;
  double rOffsetForDisp3;

  String toXML() {
    return '<?xml version="1.0"?><settings><FocusSettings rPosForFilter1="$rPosForFilter1" '
        'rPosForFilter2="$rPosForFilter2" '
        'rPosForFilter3="$rPosForFilter3" '
        'rOffsetForDisp1="$rOffsetForDisp1" '
        'rOffsetForDisp2="$rOffsetForDisp2" '
        'rOffsetForDisp3="$rOffsetForDisp3"/></settings>';
  }
}

class ProtocolMessage {
  ProtocolMessage();
  factory ProtocolMessage.fromXmlNode(xml.XmlElement node) {
    ProtocolMessage msg;
    var name = node.name.local;
    switch (name) {
      case 'image':
        msg = new ImageMessage()..path = node.getAttribute('path');
        break;
      case 'protocolState':
        msg = new StateMessage()..value = node.getAttribute('value');
        break;
      case 'run':
        msg = new CurrentStepIndexMessage()
          ..index = int.parse(node.getAttribute('currentStepIndex'));
        break;
      case 'error':
        msg = new ErrorMessage()
          ..id = node.getAttribute('id')
          ..description = node.getAttribute('description');
        break;
      case 'incrementCycle':
        msg = new IncrementCycleMessage()
          ..cycle = int.parse(node.getAttribute('cycle'));
        break;
      case 'brokenMembrane':
        msg = new BrokenMembraneMessage()
          ..wellBitSet = int.parse(node.getAttribute('wellBitSet'));
        break;
      default:
        msg = new UnknownMessage()
          ..name = node.name.local
          ..content = node.toXmlString(pretty: true);
        break;
    }

    return msg;
  }
}

class ErrorMessage extends ProtocolMessage {
  String id;
  String description;

  String toString() => '${this.runtimeType}($id, $description)';
}

class ImageMessage extends ProtocolMessage {
  String path;
  String toString() => '${this.runtimeType}(path=$path)';
}

class StateMessage extends ProtocolMessage {
  String value;
  String toString() => '${this.runtimeType}(value=$value)';
}

class CurrentStepIndexMessage extends ProtocolMessage {
  int index;
  String toString() => '${this.runtimeType}(index=$index)';
}

class IncrementCycleMessage extends ProtocolMessage {
  int cycle;
  String toString() => '${this.runtimeType}(cycle=$cycle)';
}

class BrokenMembraneMessage extends ProtocolMessage {
  int wellBitSet;
  String toString() => '${this.runtimeType}(wellBitSet=$wellBitSet)';
}

class UnknownMessage extends ProtocolMessage {
  String name;
  String content;
  String toString() => '${this.runtimeType}(name=$name,content=$content)';
}
