import 'package:logging/logging.dart';

void initLogger(String thread, int logLevel) {
  var level = Level.LEVELS.firstWhere((l) => l.value == logLevel);
  Logger.root.level = level;

  var lastLog = new DateTime.now();

  Logger.root.onRecord.listen((LogRecord rec) {
    var date = new DateTime.now();
    print(
        '${thread} : ${rec.time} : ${date.difference(lastLog).inMicroseconds} : ${rec.level.name} : ${rec.loggerName} : ${rec.message}');
    if (rec.error != null) {
      print(rec.error);
    }
    if (rec.stackTrace != null) {
      print(rec.stackTrace);
    }

    lastLog = date;
  });
}
