part of ps.model.api;

class Controllable {
  String name;
  bool enabled;
  bool error_status;
  int error_number;
  double target_value;
  double actual_value;
  double min_value;
  double max_value;
}

class Versions {
  Version pamservice;
  Version psserver;
  LinuxVersion linux;
}

class LinuxVersion {
  String sysname;
  String nodename;
  String release;
  String version;
  String machine;
}

class Version {
  String name;
  int major;
  int minor;
  int patch;
  String tag;
  String date;
  String commit;
}

class Mode {
  bool isStub;
  bool isStubBoard1;
  bool isStubBoard2;
  bool isStubBoard3;
}

class BrokenWellBitSet {
  double value;
}

class OverPressureSupplierValue {
  double value;
}

class UnderPressureSupplierValue {
  double value;
}

class Temperatures {
  double coverLeft;
  double coverMiddle;
  double coverRight;
  double bottomLeft;
  double bottomMiddle;
  double bottomRight;
}

class WellPressures {
  List<int> wells;
  List<double> pressures;
  bool aspiratePumpStatus;
  double aspiratePressure;
}

class XYPos {
  double x;
  double y;
}

class Settings {
  String unitName;
  Wagon_LogicalXYPos_t wagon_logicalXYPos_t;
  Filter_LogicalPos_t filter_logicalPos_t;
  Focus_LogicalPos_t focus_logicalPos_t;
  Focus_LogicalWellPos_t focus_logicalWellPos_t;
  LEDIntensity_t ledIntensity_t;
  Load_Unload_Settings_t load_unload_settings_t;
  PumpSettings_t pumpSettings_t;
  AspirationSettings_t aspirationSettings_t;
  DispenseSettings_t dispenseSettings_t;
  List<Sensor_Calibration_Data_t> sensorCalibrationData_t;
  List<ControllableSettings> controllableSettings;
}

class Sensor_Calibration_Data_t {
  /**<Object id                                                                     */
  int eSensorObj;
  /**<calibration defined default value, (now) only used for heatables              */
  double rSysOffset;
  /**<calibration defined default value, (now) only used for heatables              */
  double rSysGain;
  /**<calibration defined default value, (now) only used for heatables              */
  double rSysGain2;
  /**<calibration (analog sensor) or hardware (encoder-type) defined default value  */
  double rSensOffset;
  /**<calibration (analog sensor) or hardware (encoder-type) defined default value  */
  double rSensGain;
  /**<calibration (analog sensor) or hardware (encoder-type) defined default value  */
  double rSensGain2;
  /**<hardware (AD-concerter) defined value                                         */
  double rADConversion;
  /**<ESW (controller sample rate) defined value                                    */
  double rTsample;
}

class ControllableSettings {
  int eCtrlObj; /**<Controller id                                                                                                  */
  int eSensObj; /**<Sensor id where SI-to-LU-conversion factors (= calibrationdata) are to be stored, see DefaultGenerator::convertSItoLU */
  int eCurrSensObj; /**<Current Sensor id where Ampere-to-LU-conversion factors are to be stored, see DefaultGenerator::convertSItoLU */

  int uWaitReadyTimeout; /**<in [ms]  ; ( see PamStationTimeoutException )                                                                  */
  int iDefaultPwm; /**<default PWM value (range [-200, 200]) for DispenseHeads, AspirateHead                                                                         */
  double
      rMaxProperty; /**<highest allowable setpoint in [�C], [Barg], [mm], [�l] ; ( see PamStationRangeException )                      */
  double
      rMinProperty; /**<lowest  allowable setpoint in [�C], [Barg], [mm], [�l] ; ( see PamStationRangeException )                      */
  double
      rTrackingError; /**highest allowable difference from setpoint in [�C] or [mm] during moving to targetValue for NIOS-positionables and heatables    */
  double rTolerance;
  /**<highest allowable difference from setpoint in [�C], [Barg], [mm], [�l], otherwise the controller should throw an Exception;
                                      * The ControllableListener::onActualValueChanged resolution is derived from this value                           */
  double
      rMaxCurrent; /**<highest allowable input in [A] for current-loop (if implemented)  */
  double
      rMaxVelocity; /**<highest allowable velocity (in [�C/ s] (heatables) or [mm/s] (NIOS-positionables) or not used ); ( see PamStationRangeException ) */
  double
      rMinVelocity; /**<lowest  allowable velocity (in [�C/ s] (heatables) or [mm/s] (NIOS-positionables) or not used ); ( see PamStationRangeException ) */
  double
      rMaxAcceleration; /**<highest allowable acceleration (in [�C/ s2] (heatables) or [mm/s2] (NIOS-positionables) or not used ); ( see PamStationRangeException ) */
  double
      rMinAcceleration; /**<lowest  allowable acceleration (in [�C/ s2] (heatables) or [mm/s2] (NIOS-positionables) or not used ); ( see PamStationRangeException ) */
  double
      rDefaultVelocity; /**<default           velocity (in [�C/ s] (heatables) or [mm/s] (NIOS-positionables) or not used );                                  */
  double
      rDefaultAcceleration; /**<default           acceleration (in [�C/ s2] (heatables) or [mm/s2] (NIOS-positionables) or not used );                                  */
  int uSettleTime; /**settle time in [ms] */

  int sd_KPp; /**P value of PID setpoint loop (if implemented for controller, else 0)*/
  int sd_KIp; /**I value of PID setpoint loop (if implemented for controller, else 0)*/
  int sd_KDp; /**D value of PID setpoint loop (if implemented for controller, else 0)*/
  int sd_Integ_Lim_max; /**Max I value of PID setpoint loop (if implemented for controller, else 0)*/
  int sd_Integ_Lim_min; /**Min I value of PID setpoint loop (if implemented for controller, else 0)*/
  int KPi; /**P value of PI current loop (if implemented for controller, else 0)*/
  int KIi; /**I value of PI current loop (if implemented for controller, else 0)*/
  double DefaultVel; /**default velocity in [LU]*/
  double DefaultAcc; /**default acceleration in [LU]*/

  int sf_KPp; /**scale factor for P value of PID setpoint loop (if implemented for controller, else 0)*/
  int sf_KIp; /**scale factor for I value of PID setpoint loop (if implemented for controller, else 0)*/
  int sf_KDp; /**scale factor for D value of PID setpoint loop (if implemented for controller, else 0)*/
  int va_scale; /**scale factor for velocity and acceleration*/
  int T_Settletime; /**settle time after setpoint generation is finished in [LU]*/

  int Allow_Homing; /**homable or not*/
  int Homing_tick_count; /**number of interrupt tick to wait*/
  int Homing_Speed_PWM; /**home sensor seek speed [-200-200]*/
  int Homing_Seeking_PWM; /**index puls seek speed [-200-200]*/

  int max_output; /**max input value for current controller, [LU]*/
  int min_output; /**min input value for current controller, [LU]*/
  int max_setpoint; /**max for setpoint generator in [LU] */
  int min_setpoint; /**min for setpoint generator in [LU] */
  int max_tracking_error; /**max tracking error permitted when moving to setpoint, [LU]*/
  int max_tolerance_error; /**max tolerance error permitted in steady state, [LU]*/
  int max_dc_i; /**max dc value for current loop*/
  int max_dc_pwm; /**max current value for driver PWM*/
  double du_max_vel; /**max velocity in [LU], may never be zero */
  double du_min_vel; /**min velocity in [LU], may never be zero */
  double du_max_acc; /**max acceleration in [LU], may never be zero */
  double du_min_acc; /**min acceleration in [LU], may never be zero */
}

class Wagon_LogicalXYPos_t {
  XYPos
      xyLoadPosition; /**<position for Well1 [mm,mm]                         */
  XYPos
      xyIncubationPosition; /**<position for Well1 [mm,mm]                         */
  XYPos
      xyFrontPanelPosition; /**<position for Well1 [mm,mm]                         */
  XYPos
      xyReadPosition; /**<position for Well1 [mm,mm]                         */
  XYPos
      xyAspiratePosition; /**<position for Well1 [mm,mm]                         */
  XYPos
      xyDispensePosition; /**<position for Well1 and DispenseHead1  [mm,mm]      */
  XYPos xyOffset_Well; /**<pitch between wells  [mm,0]                        */
  XYPos
      xyOffset_Disposable; /**<pitch between disposables [0, mm]                  */
  XYPos
      xyOffset_FocusGlass; /**<position of FocusGlass wrt top-well of disposable [mm,0] */
  XYPos
      xyOffset_DispenseHead1; /**<position of Head1 wrt Head1 [mm,mm] = [0,0]        */
  XYPos
      xyOffset_DispenseHead2; /**<position of Head2 wrt Head1 [0,mm]                 */
  XYPos
      xyOffset_DispenseHead3; /**<position of Head3 wrt Head1 [0,mm]                 */
  XYPos
      xyOffset_DispenseHead4; /**<position of Head4 wrt Head1 [0,mm]                 */
  XYPos
      xyOffset_Prime1; /**<position of Prime1 wrt Well1  [mm,mm]              */
  XYPos
      xyOffset_Prime2; /**<position of Prime2 wrt Well1  [mm,mm]              */
}

class Filter_LogicalPos_t {
  double rPosFilter1; /**<position of Filter1 [mm]  */
  double rPosFilter2; /**<position of Filter2 [mm]  */
  double rPosFilter3; /**<position of Filter3 [mm]  */
}

class Focus_LogicalPos_t {
  double rPosForFilter1; /**<focusposition for Filter1 [mm]             */
  double rPosForFilter2; /**<focusposition for Filter2 [mm]             */
  double rPosForFilter3; /**<focusposition for Filter3 [mm]             */
  double rOffsetForDisp1; /**<offset for wells on disposable1 [mm]=[0]   */
  double rOffsetForDisp2; /**<offset for wells on disposable1 [mm]       */
  double rOffsetForDisp3; /**<offset for wells on disposable1 [mm]       */
}

class Focus_LogicalWellPos_t {
  double rPosForFilter1; /**<focusposition for Filter1 [mm]             */
  double rPosForFilter2; /**<focusposition for Filter2 [mm]             */
  double rPosForFilter3; /**<focusposition for Filter3 [mm]             */
  double rOffsetForWell1; /**<offset for well1 [mm]=[0]   */
  double rOffsetForWell2;
  double rOffsetForWell3;
  double rOffsetForWell4;
  double rOffsetForWell5;
  double rOffsetForWell6;
  double rOffsetForWell7;
  double rOffsetForWell8;
  double rOffsetForWell9;
  double rOffsetForWell10;
  double rOffsetForWell11;
  double rOffsetForWell12;
}

class LEDIntensity_t {
  int uIntensity_LED1; /**<PWM 0-200%, 0 is off, 200 is max intensity  */
  int uIntensity_LED2; /**<PWM 0-200%, 0 is off, 200 is max intensity  */
  int uIntensity_LED3; /**<PWM 0-200%, 0 is off, 200 is max intensity  */
}

class WagonHeater_Settings_t {
  double rBottomGain; /**<a, 1st order gain for the BottomHeaters-group */
  double rCoverGain; /**<b, 1st order gain for the CoverHeaters-group */
  double rBottomGain2; /**<c, 2nd order gain for the BottomHeaters-group */
  double rCoverGain2; /**<d, 2nd order gain for the CoverHeaters-group */
  double rOffset; /**<e, offset */
  double
      rCoverBottomOffset; /**<setpoint offset between Cover&Bottom to prevent evaporation */
  double
      rMinCoverTemp; /**<to types of calculation are used depending on whether Disposable setpoint */
}

class DispenseHeadHeater_Settings_t {
  double rGain; /*a, 1st order gain */
  double rGain2; /*c, 2nd order */
  double rOffset; /*e, offset */
}

class BoardInfo_t {
  int iPS12_nr; /**<Instrument serial number, set at manufacturing (0)YMMXX*/
  int iBrd_nr_high; /**<Board serial number, byte 5&4 from [5,4,3,2,1,0,]*/
  int iBrd_nr_medium; /**<Board serial number, byte 3&2 from [5,4,3,2,1,0,]*/
  int iBrd_nr_low; /**<Board serial number, byte 1&0 from [5,4,3,2,1,0,]*/
  int iFW_version; /**<Firmware version number, hardcoded by programmer*/
  int iESW_version; /**<Embedded software version number, hardcoded by programmer*/
  double rFlashDate; /**<Most recent FlashDate, set by pimcore, YYMMDD*/
}

class PS12_Version_t {
  BoardInfo_t tICCBInfo;
  BoardInfo_t tSYCBInfo;
  BoardInfo_t tWSCBInfo;
}

class Load_Unload_Settings_t {
  double
      rDefaultOverPressure; /**<[barg], default setpoint for OverPressureSupplier   */
  double
      rDefaultUnderPressure; /**<[barg], default setpoint for UnderPressureSupplier  */
  double
      rInitWagonTemperature; /**<[�C], at the start of each experiment, the Wagon is first heated/cooled to this temperature */
  double
      rSafeUnloadTemperature; /**<[�C], wagonHeater::checkUnloadTemperatureSafe() throws a PamStationSafetyExceptionis if before unloading wagonHeater::getActualValue() > rSafeUnloadTemperature */
}

class PumpSettings_t {
  int uValveOpenTime; /**<[ms], time valve should be open to allow pressure building up under the well   */
  int uPressureSettlingDelay; /**<[ms], time to allow selected pressure building up in the disposable system     */
  int uFluidFlowDelay; /**<[ms], time to wait after closing the valves (before checking on broken membranes) */
  double rBrokenMembranePressure;
  /**<[barg], well (under- and over)pressures under this value are considered as broken membrane pressures\n
                                      * The setting for rBrokenMembranePressure must be between 0 and the Min( rDefaultOverPressure, abs(rDefaultUnderPressure) ), otherwise a BrokenMembraneException is thrown each Pump-action;*/
  int uMaximumWaitingTime; /**<[ms], when Disposable::waitPumpReady() exceeds this time, an PamStationTimeoutException is thrown     */
}

class AspirationSettings_t {
  double
      rPressureUnderlimit; /**<[barg], highest allowable underpressure-level. When encountering underpressures below this level, the apirate-tubing system is considered blocked (e.g.  -0.3 barg ) and a PamStationSafetyException is thrown */
  double
      rPressureUpperLimit; /**<[barg], lowest  allowable underpressure-level. When encountering underpressures above this level, the apirate-tubing system is considered leaking (e.g. - 0.03 barg) and a PamStationSafetyException is thrown */
  int uAspirateTubeCleanDelay; /**<[ms], time aspiratePump should remain "on" after the actual aspiration of the well, to clean the apirate-tubing system */
  int uAspPressBuildupDelay; /**<[ms], delay to wait after the pump is switched on, to allow the aspiratePressure to build up */
}

class DispenseSettings_t {
  int uHeatableHeadBitSet; /**<[-] , a bitset indicating which heads are heatable. (f.e. 0b0101 = 5 = Head1&3 are heatable) */
  double
      rPrimeAmount; /**<[�l], kind of default dispenseAmount, used in PrimeStep                                      */
  double
      rRetractAmount; /**<[�l], amount the syringe needs to retract to avoid dripping                                  */
  int uRetractDelay; /**<[ms], delay between dispensing and retracting, to avoid dripping                             */
}
