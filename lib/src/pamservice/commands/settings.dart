part of pamgene.pamservice;

class SettingsCommand extends Command {
  final name = "settings";
  final description = "Get, set and flash settings.";

  SettingsCommand() {
    addSubcommand(new GetSettingsCommand());
    addSubcommand(new SetSettingsCommand());
    addSubcommand(new FlashSettingsCommand());
  }
}

class ModeCommand extends Command {
  final name = "mode";
  final description = "Get, set instrument mode ie emulation.";

  ModeCommand() {
    addSubcommand(new GetModeCommand());
    addSubcommand(new SetModeCommand());
  }
}

class GetModeCommand extends Command {
  final name = "get";
  final description = "Get mode.";

  GetModeCommand() {}

  Future run() async {
    var m = await Pamservice().machines.current();
    var client = iohttp.IOClient();
    var uri = Uri.parse('http://${m.ip}:8080/api/pamservice/mode')
        .replace(queryParameters: {'cmd': 'mode'});

    var response = await client.get(uri);
    if (response.statusCode != 200) {
      throw 'mode.get.failed.${response.statusCode}, ${response.body}';
    }

    var mode = api.Mode.json(json.decode(response.body));

    print(JsonEncoder.withIndent('  ').convert(mode));
  }
}

class SetModeCommand extends Command {
  final name = "set";
  final description = "Set mode.";

  SetModeCommand() {
    argParser..addFlag('stub', abbr: 's', defaultsTo: true);
    argParser..addFlag('stubBoard1', defaultsTo: false);
    argParser..addFlag('stubBoard2', defaultsTo: false);
    argParser..addFlag('stubBoard3', defaultsTo: false);
  }

  Future run() async {
    var m = await Pamservice().machines.current();
    var client = iohttp.IOClient();
    var uri = Uri.parse('http://${m.ip}:8080/api/pamservice/mode')
        .replace(queryParameters: {'cmd': 'mode'});

    var mode = api.Mode()
      ..isStub = argResults['stub']
      ..isStubBoard1 = argResults['stubBoard1']
      ..isStubBoard2 = argResults['stubBoard2']
      ..isStubBoard3 = argResults['stubBoard3'];

    var response = await client.put(uri, body: json.encode(mode.toJson()));
    if (response.statusCode != 200) {
      throw 'mode.set.failed.${response.statusCode}, ${response.body}';
    }
  }
}

class FlashSettingsCommand extends Command {
  final name = "flash";
  final description = "Flash settings.";

  FlashSettingsCommand() {}

  void run() async {
    var m = await Pamservice().machines.current();
    var client = iohttp.IOClient();
    var uri = Uri.parse('http://${m.ip}:8080/api/pamservice/settings')
        .replace(queryParameters: {'cmd': 'flash'});
    var settings = await Pamservice().machines.getMachineSettings(m);

    var response = await client.put(uri, body: json.encode(settings.toJson()));
    if (response.statusCode != 200) {
      throw 'settings.flash.failed.${response.statusCode}, ${response.body}';
    }
  }
}

class SetSettingsCommand extends Command {
  final name = "set";
  final description = "Set settings.";

  SettingsCommand() {}

  Future run() async {
    var m = await Pamservice().machines.current();
    var client = iohttp.IOClient();
    var uri = Uri.parse('http://${m.ip}:8080/api/pamservice/settings')
        .replace(queryParameters: {'cmd': 'settings'});
    var settings = await Pamservice().machines.getMachineSettings(m)
      ..unitName = m.name;

    var response = await client.put(uri, body: json.encode(settings.toJson()));
    if (response.statusCode != 200) {
      throw 'settings.set.failed.${response.statusCode}, ${response.body}';
    }
  }
}

class GetSettingsCommand extends Command {
  final name = "get";
  final description = "Get settings.";

  SettingsCommand() {}

  Future run() async {
    var m = await Pamservice().machines.current();
    var uri = Uri.parse('http://${m.ip}:8080/api/pamservice/settings')
        .replace(queryParameters: {'cmd': 'settings'});
    var client = iohttp.IOClient();

    var response = await client.get(uri);
    if (response.statusCode != 200) {
      throw 'settings.get.failed.${response.statusCode}, ${response.body}';
    }
//    print(JsonEncoder.withIndent('  ').convert(json.decode(response.body)));
    var calibration = api.Settings.json(json.decode(response.body));
    var file = await Pamservice().machines.setMachineSettings(m, calibration);
    print('setting save -- ${file}');
  }
}
