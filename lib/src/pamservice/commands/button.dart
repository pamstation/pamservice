part of pamgene.pamservice;

class ButtonCommand extends Command {
  final name = "button";
  final description = "Button commands.";

  ButtonCommand() {
    addSubcommand(new ButtonStateCommand());
    addSubcommand(new ButtonPressCommand());
  }
}

class ButtonPressCommand extends Command {
  final name = "press";
  final description = "Press the button.";

  Future run() async {
    var m = await Pamservice().machines.current();
    var ps12 = new ps12run.PS12(Uri.parse('http://${m.ip}:8080'));
    await ps12.button.press();
  }
}

class ButtonStateCommand extends Command {
  final name = "status";
  final description = "Get button status.";

  Future run() async {
    var m = await Pamservice().machines.current();
    var ps12 = new ps12run.PS12(Uri.parse('http://${m.ip}:8080'));
    print(await ps12.button.status);
  }
}
