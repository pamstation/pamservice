part of pamgene.pamservice;

class ConfigCommand extends Command {
  final name = "config";
  final description = "Display current config.";

  ConfigCommand() {
    argParser.addFlag('config', abbr: 'c');
  }

  void run() {
    var config = utilconfig.Config();
    config.asMap().forEach((k, v) {
      print('${k}=${v}');
    });
  }
}
