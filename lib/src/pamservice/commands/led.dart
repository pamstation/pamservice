part of pamgene.pamservice;

class LedCommand extends Command {
  final name = "led";
  final description = "Turn on or off led.";

  LedCommand() {
    addSubcommand(new LedOnCommand());
    addSubcommand(new LedOffCommand());
  }
}

class LedOnCommand extends Command {
  final name = "on";
  final description = "Turn on led.";

  LedOnCommand() {
    argParser
      ..addOption('led',
          abbr: 'l', allowed: ['1', '2', '3', 'all'], defaultsTo: '1');
  }

  Future run() async {
    var led = argResults['led'];

    var m = await Pamservice().machines.current();

    var uri = Uri.parse('http://${m.ip}:8080/api/pamservice/led')
        .replace(queryParameters: {'led': led, 'status': 'on'});

    var client = iohttp.IOClient();

    var response = await client.get(uri);

    if (response.statusCode != 200) {
      throw 'led.on.failed.${response.statusCode}, ${response.body}';
    }
  }
}

class LedOffCommand extends Command {
  final name = "off";
  final description = "Turn off led.";

  LedOffCommand() {
    argParser
      ..addOption('led',
          abbr: 'l', allowed: ['1', '2', '3', 'all'], defaultsTo: '1');
  }

  Future run() async {
    var led = argResults['led'];

    var m = await Pamservice().machines.current();

    var uri = Uri.parse('http://${m.ip}:8080/api/pamservice/led')
        .replace(queryParameters: {'led': led, 'status': 'off'});

    var client = iohttp.IOClient();

    var response = await client.get(uri);

    if (response.statusCode != 200) {
      throw 'led.on.failed.${response.statusCode}, ${response.body}';
    }
  }
}
