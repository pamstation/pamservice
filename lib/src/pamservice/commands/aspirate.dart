part of pamgene.pamservice;

class AspirateCommand extends Command {
  final name = "aspirate";
  final description = "Pamstation aspirate commands.";

  AspirateCommand() {
    addSubcommand(RunAspirateCommand());
    addSubcommand(RunAspirateUpCommand());
    addSubcommand(RunAspirateDownCommand());
    addSubcommand(AspirateStatusCommand());
    addSubcommand(AspirateOnCommand());
    addSubcommand(AspirateOffCommand());
  }
}

class AspirateStatusCommand extends Command {
  final name = "status";
  final description = "Aspirate pump status.";

  Future run() async {
    var m = await Pamservice().machines.current();

    var uri = Uri.parse('http://${m.ip}:8080/api/pamservice/pressure')
        .replace(queryParameters: {'wellBitSet': '0'});

    var client = iohttp.IOClient();

    var response = await client.get(uri);

    if (response.statusCode != 200) {
      if (response.statusCode == 400) {
        throw 'pump.status.failed, ${response.body}';
      } else {
        throw 'pump.status.failed.${response.statusCode}, ${response.body}';
      }
    }

    var pressures = api.WellPressures.json(json.decode(response.body));

    print(pressures.aspiratePumpStatus);
  }
}

class AspirateOnCommand extends Command {
  final name = "on";
  final description = "Turn aspirate pump on.";

//  RunPumpCommand(){}

  Future run() async {
    var m = await Pamservice().machines.current();

    var uri = Uri.parse('http://${m.ip}:8080/api/pamservice/pressure')
        .replace(queryParameters: {'aspiratePumpStatus': '1'});

    var client = iohttp.IOClient();

    var response = await client.put(uri);

    if (response.statusCode != 200) {
      if (response.statusCode == 400) {
        throw 'pump.on.failed, ${response.body}';
      } else {
        throw 'pump.on.failed.${response.statusCode}, ${response.body}';
      }
    }
  }
}

class AspirateOffCommand extends Command {
  final name = "off";
  final description = "Turn aspirate pump off.";

  Future run() async {
    var m = await Pamservice().machines.current();

    var uri = Uri.parse('http://${m.ip}:8080/api/pamservice/pressure')
        .replace(queryParameters: {'aspiratePumpStatus': '0'});

    var client = iohttp.IOClient();

    var response = await client.put(uri);

    if (response.statusCode != 200) {
      if (response.statusCode == 400) {
        throw 'pump.off.failed, ${response.body}';
      } else {
        throw 'pump.off.failed.${response.statusCode}, ${response.body}';
      }
    }
  }
}

class RunAspirateUpCommand extends Command {
  final name = "up";
  final description = "Aspirate positioner up.";

  Future run() async {
    var m = await Pamservice().machines.current();

    var uri = Uri.parse('http://${m.ip}:8080/api/pamservice/aspirate')
        .replace(queryParameters: {'cmd': 'up'});

    var client = iohttp.IOClient();

    var response = await client.post(uri);

    if (response.statusCode != 200) {
      throw 'aspirate.up.failed.${response.statusCode}, ${response.body}';
    }
  }
}

class RunAspirateDownCommand extends Command {
  final name = "down";
  final description = "Aspirate positioner down.";

  Future run() async {
    var m = await Pamservice().machines.current();

    var uri = Uri.parse('http://${m.ip}:8080/api/pamservice/aspirate')
        .replace(queryParameters: {'cmd': 'down'});

    var client = iohttp.IOClient();

    var response = await client.post(uri);

    if (response.statusCode != 200) {
      throw 'aspirate.down.failed.${response.statusCode}, ${response.body}';
    }
  }
}


class RunAspirateCommand extends Command {
  final name = "run";
  final description = "Run aspirate step.";

  RunAspirateCommand() {
    argParser
      ..addOption('wellBitSet', abbr: 'w', defaultsTo: '1')
      ..addOption('aspirateTime', abbr: 't', defaultsTo: '1000')
      ..addOption('pumpUpTime', abbr: 'p', defaultsTo: '1000');
  }

  Future run() async {
    var wellBitSet = int.parse(argResults['wellBitSet']);
    var aspirateTime = int.parse(argResults['aspirateTime']);
    var pumpUpTime = int.parse(argResults['pumpUpTime']);

    var m = await Pamservice().machines.current();

    var uri = Uri.parse('http://${m.ip}:8080/api/pamservice/aspirate')
        .replace(queryParameters: {
      'wellBitSet': wellBitSet.toString(),
      'aspirateTime': aspirateTime.toString(),
      'pumpUpTime': pumpUpTime.toString()
    });

    var client = iohttp.IOClient();

    var response = await client.get(uri);

    if (response.statusCode != 200) {
      if (response.statusCode == 400) {
        throw 'aspirate.run.broken.membrane.failed, ${response.body}';
      } else {
        throw 'aspirate.run.failed.${response.statusCode}, ${response.body}';
      }
    }
  }
}
