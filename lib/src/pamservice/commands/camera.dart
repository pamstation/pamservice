part of pamgene.pamservice;

class CameraCommand extends Command {
  final name = "camera";
  final description = "Pamstation camera.";

  CameraCommand() {
    addSubcommand(new ImageCommand());
    addSubcommand(new FocusCommand());
    addSubcommand(new SnapshotCommand());
  }
}

class ImageCommand extends Command {
  final name = "read";
  final description = "Take an image.";

  ImageCommand() {
    argParser
      ..addOption('well', defaultsTo: '1', allowed: [
        '1',
        '2',
        '3',
        '4',
        '5',
        '6',
        '7',
        '8',
        '9',
        '10',
        '11',
        '12'
      ])
      ..addOption('filter',
          abbr: 'f', defaultsTo: '1', allowed: ['1', '2', '3'])
      ..addOption('exposureTime', abbr: 'e', defaultsTo: '200');
  }

  Future run() async {
    var well = argResults['well'];
    var filter = argResults['filter'];
    var exposureTime = argResults['exposureTime'];

    var m = await Pamservice().machines.current();

    var uri = Uri.parse('http://${m.ip}:8080/api/pamservice/camera').replace(
        queryParameters: {
          'cmd': 'read',
          'well': well,
          'filter': filter,
          'exposureTime': exposureTime
        });

    var client = iohttp.IOClient();

    var response = await client.get(uri);

    if (response.statusCode != 200) {
      throw 'camera.read.failed.${response.statusCode}, ${response.body}';
    }

    var dir = await Directory.systemTemp.createTemp('pamservice');
    var file = '${dir.path}/image.tiff';

    File(file).writeAsBytesSync(response.bodyBytes);

    var cmd = Pamservice().config['open.image.cmd'] as String;

    print(file);

    if (cmd != null && cmd.isNotEmpty) {
      await Process.start(cmd, [file]);
    }
  }
}

class SnapshotCommand extends Command {
  final name = "snapshot";
  final description = "Take an image inplace.";

  SnapshotCommand() {
    argParser..addOption('exposureTime', abbr: 'e', defaultsTo: '200');
  }

  Future run() async {
    var exposureTime = argResults['exposureTime'];

    var m = await Pamservice().machines.current();

    var uri = Uri.parse('http://${m.ip}:8080/api/pamservice/camera').replace(
        queryParameters: {'cmd': 'snapshot', 'exposureTime': exposureTime});

    var client = iohttp.IOClient();

    var response = await client.get(uri);

    if (response.statusCode != 200) {
      throw 'camera.snapshot.failed.${response.statusCode}, ${response.body}';
    }

    var dir = await Directory.systemTemp.createTemp('pamservice');
    var file = '${dir.path}/image.tiff';

    File(file).writeAsBytesSync(response.bodyBytes);

    var cmd = Pamservice().config['open.image.cmd'] as String;

    print(file);

    if (cmd != null && cmd.isNotEmpty) {
      await Process.start(cmd, [file]);
    }
  }
}

class FocusCommand extends Command {
  final name = "focus";
  final description = "Compute best focus position for a well.";

  FocusCommand() {
    argParser
      ..addOption('wellBitSet', abbr: 'w', defaultsTo: '1')
      ..addOption('filter',
          abbr: 'f', defaultsTo: '1', allowed: ['1', '2', '3'])
      ..addOption('exposureTime', abbr: 'e', defaultsTo: '200')
      ..addOption('focusRange', abbr: 'r', defaultsTo: '0.5');
  }

  Future run() async {
    var wellBitSet = argResults['wellBitSet'];
    var filter = argResults['filter'];
    var exposureTime = argResults['exposureTime'];
    var focusRange = argResults['focusRange'];

    var m = await Pamservice().machines.current();

    var uri = Uri.parse('http://${m.ip}:8080/api/pamservice/camera')
        .replace(queryParameters: {
      'cmd': 'auto.focus',
      'wellBitSet': wellBitSet,
      'filter': filter,
      'exposureTime': exposureTime,
      'focusRange': focusRange
    });

    var client = iohttp.IOClient();

    var response = await client.get(uri);

    if (response.statusCode != 200) {
      throw 'camera.focus.failed.${response.statusCode}, ${response.body}';
    }

    var focus_LogicalWellPos_t =
        api.Focus_LogicalWellPos_t.json(json.decode(response.body));

    print(
        JsonEncoder.withIndent('  ').convert(focus_LogicalWellPos_t.toJson()));

//    var value = focus_LogicalWellPos_t.toJson()["rOffsetForWell${well}"];
//    print("rOffsetForWell${well} = ${value}");
  }
}
