part of pamgene.pamservice;

class ControllableCommand extends Command {
  final name = "ctrl";
  final description = "Pamstation controllable commands.";

  ControllableCommand() {
    argParser
      ..addOption('name',
          abbr: 'n', allowed: ['wagon.x', 'wagon.y'], defaultsTo: 'wagon.x');
  }

  Future run() async {
    var ctrl_name = argResults['name'];

    var m = await Pamservice().machines.current();

    var uri = Uri.parse('http://${m.ip}:8080/api/pamservice/ctrl')
        .replace(queryParameters: {
      'ctrl_name': ctrl_name,
    });

    var client = iohttp.IOClient();

    var response = await client.get(uri);

    if (response.statusCode != 200) {
      throw 'controllable.get.failed.${response.statusCode}, ${response.body}';
    }
    var ctrl = api.Controllable.json(json.decode(response.body));

    print(JsonEncoder.withIndent('  ').convert(ctrl.toJson()));
  }
}
