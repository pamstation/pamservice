part of pamgene.pamservice;

class VersionCommand extends Command {
  final name = "version";
  final description = "Display current version.";

  ConfigCommand() {
    argParser.addFlag('version', abbr: 'v');
  }

  Future run() async {
    print(api.Version.json(version.VERSION).getString());

    Machine m;
    try {
      m = await Pamservice().machines.current();

      var uri = Uri.parse('http://${m.ip}:8080/api/version');

      var client = iohttp.IOClient();

      var response = await client.get(uri);

      if (response.statusCode != 200) {
        print(api.Version.json(version.VERSION).getString());
        print(
            'psserver : ${name}.failed.${response.statusCode}, ${response.body}');
      } else {
        var versions = api.Versions.json(json.decode(response.body));

        print(versions.psserver.getString());
        print(versions.linux.toJson());
      }
    } on SocketException catch (_) {
      print('failed to connect to psserver : ${m.name} ${m.ip}');
    } catch (e) {
      print(e);
    }
  }
}
