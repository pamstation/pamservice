part of pamgene.pamservice;

class ProtocolCommand extends Command {
  final name = "protocol";
  final description = "Protocol commands.";

  ProtocolCommand() {
    addSubcommand(new RunProtocolCommand());
    addSubcommand(new RunAbortProtocolCommand());
    addSubcommand(new RunUnloadProtocolCommand());
    addSubcommand(new ProtocolMessageCommand());
    addSubcommand(new GetProtocolCommand());
  }
}

class GetProtocolCommand extends Command {
  final name = "get";
  final description = "Get current protocol.";

  Future run() async {
    var m = await Pamservice().machines.current();
    var ps12 = new ps12run.PS12(Uri.parse('http://${m.ip}:8080'));
    var protocol = await ps12.getCurrentProtocol();
    if (protocol == null) {
      throw 'protocol.get.no.current.protocol';
    }
    print(protocol.xmlProtocol);
  }
}

class ProtocolMessageCommand extends Command {
  final name = "listen";
  final description = "Get current protocol messages.";

  Future run() async {
    var m = await Pamservice().machines.current();
    var ps12 = new ps12run.PS12(Uri.parse('http://${m.ip}:8080'));
    var protocol = await ps12.getCurrentProtocol();
    if (protocol == null) {
      throw 'protocol.message.no.current.protocol';
    }
    await for (var msg in protocol.protocolMessages()) {
      print(msg.toString());
    }
  }
}

class RunUnloadProtocolCommand extends Command {
  final name = "unload";
  final description = "Unload the current protocol.";

  Future run() async {
    var m = await Pamservice().machines.current();
    var ps12 = new ps12run.PS12(Uri.parse('http://${m.ip}:8080'));
    var protocol = await ps12.getCurrentProtocol();
    if (protocol == null) {
      return;
    }
    await protocol.unload();
  }
}

class RunAbortProtocolCommand extends Command {
  final name = "abort";
  final description = "Abort the running protocol.";

  Future run() async {
    var m = await Pamservice().machines.current();
    var ps12 = new ps12run.PS12(Uri.parse('http://${m.ip}:8080'));
    var protocol = await ps12.getCurrentProtocol();
    if (protocol == null) {
      return;
    }
    await protocol.abort();
  }
}

class RunProtocolCommand extends Command {
  final name = "run";
  final description = "Run protocol.";

  RunProtocolCommand() {
    argParser
      ..addOption('file', abbr: 'f', defaultsTo: '', help: 'Protocol file.')
      ..addOption('outputDir',
          abbr: 'o', defaultsTo: 'run/output', help: 'Output directory.')
      ..addOption('repeat',
          abbr: 'r', defaultsTo: '1', help: 'Number of repeated run.')
      ..addOption('press',
          abbr: 'p',
          defaultsTo: 'true',
          help: 'Automatically press the button.');
  }

  Future run() async {
    var autoPress = argResults['press'] == 'true';
    var repeat = int.tryParse(argResults['repeat']);
    if (repeat == null) throw 'repeat parameter is undefined';

    var file = argResults['file'];
    if (!File(file).existsSync()) throw 'protocol file $file does not exist.';

    var outputDir = argResults['outputDir'];
    Directory(outputDir).createSync(recursive: true);

    var m = await Pamservice().machines.current();

    var ps12 = new ps12run.PS12(Uri.parse('http://${m.ip}:8080'));

    try {
      await ps12.ping();
    } catch (e) {
      print('connection to server failed');
      rethrow;
    }

//    print('Checking server status');

    var status = await ps12.status;

    if (status == '0') {
      await ps12.turnOn();
    } else if (status == '1') {
      var currentProtocol = await ps12.getCurrentProtocol();
      if (currentProtocol != null) {
        print('unloading current protocol');
        await currentProtocol.unload();
      }
    } else if (status == '2') {
      throw 'A protocol is already running ... ';
    }

    var protocolFile = file;

    var date = new DateTime.now();
    var imageDir = libpath.join(
        '${date.year}${date.month}${date.day}${date.hour}${date.minute}-${libpath.basenameWithoutExtension(protocolFile)}');

    var imageResultDir = libpath.join(outputDir, imageDir);

    new Directory(imageResultDir).createSync(recursive: true);

    var runId = '${date.year.toString().substring(3)}'
        '${date.month.toString().padLeft(2, '0')}'
        '${date.day.toString().padLeft(2, '0')}'
        '${date.hour.toString().padLeft(2, '0')}'
        '${date.minute.toString().padLeft(2, '0')}';

//    print('======== output');
//    print('runId = ${runId}');
//    print('images = ${imageResultDir}');
//    print('========');
//    print('');

    var buttonSubscription = ps12.button.onStateChange.listen((b) {
      if (b) {
        if (autoPress) {
          ps12.button.press();
        } else {
          print('press enter to continue');
          stdin.readLineSync();
          ps12.button.press();
        }
      }
    });

    print('======== run');

    try {
      var runNumber = repeat;

      for (var i = 0; i < runNumber; i++) {
        StreamSubscription protocolSubscription;
        try {
          var prefix = '${runId}${i.toString().padLeft(2, '0')}';

          var protocol = ps12.protocolFromFile(protocolFile)
            ..setPath(imageDir)
            ..setBarcodes(['${prefix}1', '${prefix}2', '${prefix}3']);

          print('progress : ${i + 1}/${runNumber}');

          await protocol.load();
          await protocol.run();

          protocolSubscription = protocol.protocolMessages().listen((msg) {
            if (msg is ps12run.ImageMessage) {
              var uri = msg.path.replaceAll('\\', '/');
              print(File(libpath.join(imageResultDir, uri)).path);
              protocol.saveImage(uri, imageResultDir);
            } else {
              print(msg);
            }
          });

          await protocol.wait();
          await protocol.unload();
        } catch (e, st) {
          protocolSubscription?.cancel();
          print(e);
          print(st);
        }
      }
    } finally {
      buttonSubscription.cancel();
    }
  }
}
