part of pamgene.pamservice;

class DispenseCommand extends Command {
  final name = "dispense";
  final description = "Pamstation dispense commands.";

  DispenseCommand() {
    addSubcommand(RunDispenseCommand());
    addSubcommand(DispenseNowCommand());
  }
}

class DispenseNowCommand extends Command {
  final name = "now";
  final description = "Dispense testing ...";

  DispenseNowCommand() {
    argParser
      ..addOption('dispenseHead', abbr: 'd', defaultsTo: '2')
      ..addOption('amount', abbr: 'a', defaultsTo: '30');
  }

  Future run() async {
    var dispenseHead = int.parse(argResults['dispenseHead']);
    var amount = double.parse(argResults['amount']);

    var m = await Pamservice().machines.current();

    var uri = Uri.parse('http://${m.ip}:8080/api/pamservice/dispense')
        .replace(queryParameters: {
      'dispenseHead': dispenseHead.toString(),
      'amount': amount.toString(),
    });

    var client = iohttp.IOClient();

    var response = await client.post(uri);

    if (response.statusCode != 200) {
      throw 'dispense.now.failed.${response.statusCode}, ${response.body}';
    }
  }
}

class RunDispenseCommand extends Command {
  final name = "run";
  final description = "Run dispense step.";

  RunDispenseCommand() {
    argParser
      ..addOption('wellBitSet', abbr: 'w', defaultsTo: '1')
      ..addOption('dispenseHead', abbr: 'd', defaultsTo: '1')
      ..addOption('amount', abbr: 'a', defaultsTo: '10')
      ..addOption('temperature', abbr: 't', defaultsTo: '0')
      ..addOption('waitTimeAfterLastWellDispensed',
          abbr: 'l', defaultsTo: '1000');
  }

  Future run() async {
    var wellBitSet = int.parse(argResults['wellBitSet']);
    var dispenseHead = int.parse(argResults['dispenseHead']);
    var amount = double.parse(argResults['amount']);
    var temperature = double.parse(argResults['temperature']);
    var waitTimeAfterLastWellDispensed =
        int.parse(argResults['waitTimeAfterLastWellDispensed']);

    var m = await Pamservice().machines.current();

    var uri = Uri.parse('http://${m.ip}:8080/api/pamservice/dispense')
        .replace(queryParameters: {
      'wellBitSet': wellBitSet.toString(),
      'dispenseHead': dispenseHead.toString(),
      'amount': amount.toString(),
      'temperature': temperature.toString(),
      'waitTimeAfterLastWellDispensed':
          waitTimeAfterLastWellDispensed.toString()
    });

    var client = iohttp.IOClient();

    var response = await client.get(uri);

    if (response.statusCode != 200) {
      if (response.statusCode == 400) {
        throw 'dispense.run.broken.membrane.failed, ${response.body}';
      } else if (response.statusCode == 401) {
        throw 'dispense.run.bad.head, ${response.body}';
      } else {
        throw 'dispense.run.failed.${response.statusCode}, ${response.body}';
      }
    }
  }
}
