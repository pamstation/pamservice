part of pamgene.pamservice;

class WatchCommand extends Command {
  final name = "watch";
  final description = "Watch a pamstation.";

  WatchCommand() {
    addSubcommand(new WatchTemperatureCommand());
    addSubcommand(new WatchPressureCommand());
    addSubcommand(new WatchValvesCommand());
  }
}

class WatchTemperatureCommand extends Command {
  final name = "temperature";
  final description = "Watch a pamstation temperature.";

  WatchTemperatureCommand() {
    argParser.addOption('temperature',
        abbr: 't', defaultsTo: 'none', help: 'set wagon heater temperature');
    argParser.addOption('every',
        abbr: 'e', defaultsTo: '2', help: 'every n seconds');
  }

  Future run() async {
    var temperature = argResults['temperature'];
    var every = int.parse(argResults['every']);

    var m = await Pamservice().machines.current();

    var uri = Uri.parse('http://${m.ip}:8080/api/pamservice/temperature')
        .replace(queryParameters: {'temperature': temperature});

    var client = iohttp.IOClient();
    var completer = Completer();

    var colnames = ['time']..addAll(api.Temperatures.temperatureNames);
    print(colnames.join(','));

    var start = DateTime.now();

    Timer.periodic(Duration(seconds: every), (t) async {
      var response = await client.get(uri);
      uri = Uri.parse('http://${m.ip}:8080/api/pamservice/temperature')
          .replace(queryParameters: {'temperature': 'none'});
      if (response.statusCode != 200) {
        t.cancel();
        completer.completeError(
            'watch.temp.failed.${response.statusCode}, ${response.body}');
      }
      var temperatures = api.Temperatures.json(json.decode(response.body));

      var values = [
        DateTime.now().difference(start).inSeconds.toString()
      ]..addAll(temperatures.temperatures.map((p) => p.toStringAsPrecision(4)));

      print(values.join(','));
    });
    return completer.future;
  }
}

class WatchValvesCommand extends Command {
  final name = "valves";
  final description = "Watch pamstation valves pressure.";

  WatchValvesCommand() {
    argParser.addOption('every',
        abbr: 'e', defaultsTo: '2', help: 'every n seconds');
  }

  // [run] may also return a Future.
  Future run() async {
    var every = int.parse(argResults['every']);

    var client = iohttp.IOClient();

    var m = await Pamservice().machines.current();
    var uriVacuum = Uri.parse('http://${m.ip}:8080/api/pamservice/valves')
        .replace(queryParameters: {'cmd': 'get.vacuum'});
    var uriPressure = Uri.parse('http://${m.ip}:8080/api/pamservice/valves')
        .replace(queryParameters: {'cmd': 'get.pressure'});

    var completer = Completer();

    bool isFirst = true;
    var start = DateTime.now();

    Timer.periodic(Duration(seconds: every), (t) async {
      var vacuumResponse = await client.get(uriVacuum);
      if (vacuumResponse.statusCode != 200) {
        t.cancel();
        completer.completeError(
            'watch.valves.vacuum.failed.${vacuumResponse.statusCode}, ${vacuumResponse.body}');
      }
      var pressureResponse = await client.get(uriPressure);
      if (pressureResponse.statusCode != 200) {
        t.cancel();
        completer.completeError(
            'watch.valves.pressure.failed.${pressureResponse.statusCode}, ${pressureResponse.body}');
      }

      var vacuumValue =
          api.UnderPressureSupplierValue.json(json.decode(vacuumResponse.body));
      var pressureValue = api.OverPressureSupplierValue.json(
          json.decode(pressureResponse.body));

      if (isFirst) {
        isFirst = false;
        var colnames = ['time', 'pressure', 'vacuum'];
        print(colnames.join(','));
      }

      var values = [
        DateTime.now().difference(start).inSeconds.toString(),
        pressureValue.value.toStringAsPrecision(4),
        vacuumValue.value.toStringAsPrecision(4)
      ];

      print(values.join(','));
    });
    return completer.future;
  }
}

class WatchPressureCommand extends Command {
  final name = "pressure";
  final description = "Watch pamstation wells pressure.";

  WatchPressureCommand() {
    argParser..addOption('wellBitSet', abbr: 'w', defaultsTo: '0');
    argParser.addOption('every',
        abbr: 'e', defaultsTo: '2', help: 'every n seconds');
  }

  // [run] may also return a Future.
  Future run() async {
    var wellBitSet = int.parse(argResults['wellBitSet']);
    var every = int.parse(argResults['every']);

    var m = await Pamservice().machines.current();

    var uri = Uri.parse('http://${m.ip}:8080/api/pamservice/pressure')
        .replace(queryParameters: {'wellBitSet': wellBitSet.toString()});

    var client = iohttp.IOClient();
    var completer = Completer();

    bool isFirst = true;
    var start = DateTime.now();

    Timer.periodic(Duration(seconds: every), (t) async {
      var response = await client.get(uri);
      if (response.statusCode != 200) {
        t.cancel();
        completer.completeError(
            'watch.pressure.failed.${response.statusCode}, ${response.body}');
      }
      var pressures = api.WellPressures.json(json.decode(response.body));

      if (isFirst) {
        isFirst = false;
        var colnames = ['time', 'aspiratePumpStatus', 'aspiratePressure']
          ..addAll(
              pressures.wells.map((p) => 'W${p.toString().padLeft(2, '0')}'));
        print(colnames.join(','));
      }

      var values = [
        DateTime.now().difference(start).inSeconds.toString(),
        pressures.aspiratePumpStatus.toString(),
        pressures.aspiratePressure.toStringAsPrecision(4)
      ]..addAll(pressures.pressures.map((p) => p.toStringAsPrecision(4)));

      print(values.join(','));
    });
    return completer.future;
  }
}
