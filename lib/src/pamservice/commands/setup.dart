part of pamgene.pamservice;

class SetupCommand extends Command {
  final name = "setup";
  final description = "Setup psserver.";

  SetupCommand() {
    argParser.addOption('version', abbr: 'v', defaultsTo: '1.0.3');
    argParser.addOption('dry-run', defaultsTo: 'false', hide: true);
  }

  Future run() async {
    var m = await Pamservice().machines.current();

    var setupDir = Directory('.setup');
    if (!setupDir.existsSync()) {
      setupDir.createSync();
    }

//    var psServerSshKey = File(".ssh/psserver.key");
//
//    if (!psServerSshKey.existsSync())
//      throw "ssh key file .ssh/psserver.key does not exist";

    var ver = argResults['version'] as String;
    var dryrun = argResults['dry-run'] == 'true';

    if (!dryrun) {
      var psServerDir = Directory(".setup/psserver");

      var psserverGit = 'git@bitbucket.org:pamstation/psserver.git';
      psserverGit = 'https://amaurel@bitbucket.org/pamstation/psserver.git';

      if (!psServerDir.existsSync()) {
//      await startProcess(
//          'ssh-agent',
//          [
//            'bash',
//            '-c',
//            'ssh-add ${psServerSshKey.absolute.path}; git clone git@bitbucket.org:pamstation/psserver.git'
//          ],
//          workingDirectory: setupDir.path);

        await startProcess('git', ['clone', psserverGit],
            workingDirectory: setupDir.path);
      }

//    await startProcess(
//        'ssh-agent',
//        [
//          'bash',
//          '-c',
//          'ssh-add ${psServerSshKey.absolute.path}; git checkout tags/${version}'
//        ],
//        workingDirectory: psServerDir.path);

      await startProcess('git', ['checkout', 'master'],
          workingDirectory: psServerDir.path);

      await startProcess('git', ['pull'], workingDirectory: psServerDir.path);

      await startProcess('git', ['checkout', 'tags/${ver}'],
          workingDirectory: psServerDir.path);

      await startProcess('scp', ['-r', './setup', 'root@${m.ip}:~/'],
          workingDirectory: psServerDir.path);

      await startProcess(
          'ssh', ['root@${m.ip}', 'chmod +x /home/root/setup/install.sh'],
          workingDirectory: psServerDir.path);

      await startProcess('ssh', ['root@${m.ip}', '/home/root/setup/install.sh'],
          workingDirectory: psServerDir.path);

      print('Wait for psserver to start and store version ...');
      await Future.delayed(Duration(seconds: 2));
    }

    print('Storing version ...');

    try {
      api.Version.json(version.VERSION);
      var uri = Uri.parse('http://${m.ip}:8080/api/version');

      var client = iohttp.IOClient();

      var response = await client.get(uri);

      if (response.statusCode != 200) {
        print(
            'psserver : ${name}.failed.${response.statusCode}, ${response.body}');
      } else {
        var versions = api.Versions.json(json.decode(response.body));

        versions.pamservice = api.Version.json(version.VERSION);

        print(versions.pamservice.getString());
        print(versions.psserver.getString());
        print(versions.linux.toJson());

        Pamservice().machines.setPSServerVersion(m, versions);
      }
    } catch (e) {
      rethrow;
    }

    print('Done');
  }
}
