part of pamgene.pamservice;

class PumpCommand extends Command {
  final name = "pump";
  final description = "Pamstation pump commands.";

  PumpCommand() {

    addSubcommand(RunPumpCommand());
  }
}



class RunPumpCommand extends Command {
  final name = "run";
  final description = "Run pump cycles.";

  RunPumpCommand() {
    argParser
      ..addOption('wellBitSet', abbr: 'w', defaultsTo: '1')
      ..addOption('pumpCycles', abbr: 'c', defaultsTo: '2')
      ..addOption('extraUpTime', abbr: 'u', defaultsTo: '1000')
      ..addOption('extraDownTime', abbr: 'd', defaultsTo: '1000');
  }

  Future run() async {
    var wellBitSet = argResults['wellBitSet'];
    var pumpCycles = argResults['pumpCycles'];
    var extraUpTime = argResults['extraUpTime'];
    var extraDownTime = argResults['extraDownTime'];

    var m = await Pamservice().machines.current();

    var uri = Uri.parse('http://${m.ip}:8080/api/pamservice/pump')
        .replace(queryParameters: {
      'wellBitSet': wellBitSet,
      'pumpCycles': pumpCycles,
      'extraUpTime': extraUpTime,
      'extraDownTime': extraDownTime
    });

    var client = iohttp.IOClient();

    var response = await client.get(uri);

    if (response.statusCode != 200) {
      if (response.statusCode == 400) {
        throw 'pump.run.broken.membrane.failed, ${response.body}';
      } else {
        throw 'pump.run.failed.${response.statusCode}, ${response.body}';
      }
    }
  }
}
