part of pamgene.pamservice;

class WagonCommand extends Command {
  final name = "wagon";
  final description = "Pamstation wagon commands.";

  WagonCommand() {
    addSubcommand(new WagonPositionCommand());
    addSubcommand(new WagonMoveCommand());
  }
}

class WagonPositionCommand extends Command {
  final name = "go";
  final description = "Get wagon position or move to logical position.";

  WagonPositionCommand() {
    argParser
      ..addOption('position',
          abbr: 'p',
          allowed: [
            'home',
            'load',
            'loadSafe',
            'read',
            'prime',
            'dispense',
            'incubation',
            'front',
            'aspirate'
          ],
          defaultsTo: 'home')
      ..addOption('well', abbr: 'w', defaultsTo: '1', allowed: [
        '1',
        '2',
        '3',
        '4',
        '5',
        '6',
        '7',
        '8',
        '9',
        '10',
        '11',
        '12'
      ])
      ..addOption('dispenseHead',
          abbr: 'd', defaultsTo: '2', allowed: ['1', '2', '3', '4']);
  }

  Future run() async {
    var position = argResults['position'];
    var well = argResults['well'];
    var dispenseHead = argResults['dispenseHead'];

    var m = await Pamservice().machines.current();

    var uri = Uri.parse('http://${m.ip}:8080/api/pamservice/wagon').replace(
        queryParameters: {
          'position': position,
          'well': well,
          'dispenseHead': dispenseHead
        });

    var client = iohttp.IOClient();

    var response = await client.get(uri);

    if (response.statusCode != 200) {
      throw 'wagon.go.failed.${response.statusCode}, ${response.body}';
    }
  }
}

class WagonMoveCommand extends Command {
  final name = "move";
  final description = "Move wagon.";

  WagonMoveCommand() {
    argParser..addOption('x', abbr: 'x')..addOption('y', abbr: 'y');
  }

  Future run() async {
    var m = await Pamservice().machines.current();

    var queryParameters = {'position': 'move'};

    if (argResults['x'] != null) {
      queryParameters['x'] = argResults['x'];
    }
    if (argResults['y'] != null) {
      queryParameters['y'] = argResults['y'];
    }

    var uri = Uri.parse('http://${m.ip}:8080/api/pamservice/wagon')
        .replace(queryParameters: queryParameters);

    var client = iohttp.IOClient();

    var response = await client.get(uri);

    if (response.statusCode != 200) {
      throw 'wagon.go.failed.${response.statusCode}, ${response.body}';
    }

    var pos = api.XYPos.json(json.decode(response.body));
    print('${pos.x},${pos.y}');
  }
}
