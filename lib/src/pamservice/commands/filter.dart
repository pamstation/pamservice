part of pamgene.pamservice;

class FilterCommand extends Command {
  final name = "filter";
  final description = "Pamstation filter.";

  FilterCommand() {
    addSubcommand(MoveCommand());
  }
}

class MoveCommand extends Command {
  final name = "move";
  final description = "Move filter.";

  MoveCommand() {
    argParser..addOption('position', abbr: 'p');
  }

  Future run() async {
    var position = argResults['position'];

    var m = await Pamservice().machines.current();

    var uri = Uri.parse('http://${m.ip}:8080/api/pamservice/camera')
        .replace(queryParameters: {
      'cmd': 'move.filter',
      'position': position,
    });

    var client = iohttp.IOClient();

    var response = await client.get(uri);

    if (response.statusCode != 200) {
      throw 'filter.move.failed.${response.statusCode}, ${response.body}';
    }
  }
}
