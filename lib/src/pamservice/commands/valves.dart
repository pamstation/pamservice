part of pamgene.pamservice;

class ValvesCommand extends Command {
  final name = "valves";
  final description = "Pamstation valves.";

  ValvesCommand() {
    addSubcommand(new OpenValvesCommand());
    addSubcommand(new CloseValvesCommand());
    addSubcommand(new GetValvesCommand());
    addSubcommand(new SetValvesCommand());
  }
}

class OpenValvesCommand extends Command {
  final name = "open";
  final description = "Open pamstation pressure or vacuum supply or wells.";

  OpenValvesCommand() {
    addSubcommand(new OpenWellCommand());
    addSubcommand(new OpenPressureCommand());
    addSubcommand(new OpenVacuumCommand());
  }
}

class GetValvesCommand extends Command {
  final name = "get";
  final description = "Get pamstation pressure or vacuum supply.";

  GetValvesCommand() {
    addSubcommand(new GetVacuumCommand());
    addSubcommand(new GetPressureCommand());
  }
}

class SetValvesCommand extends Command {
  final name = "set";
  final description = "Set pamstation pressure or vacuum supply.";

  SetValvesCommand() {
    addSubcommand(new SetVacuumCommand());
    addSubcommand(new SetPressureCommand());
  }
}

class SetVacuumCommand extends Command {
  final name = "vacuum";
  final description = "Set vacuum value.";

  SetVacuumCommand() {
    argParser..addOption('value', abbr: 'v');
  }

  Future run() async {
    var value = double.tryParse(argResults['value']);
    if (value == null) throw 'valves.set.vacuum.value.not.a.number';

    var m = await Pamservice().machines.current();

    var uri = Uri.parse('http://${m.ip}:8080/api/pamservice/valves').replace(
        queryParameters: {'cmd': 'set.vacuum', 'value': value.toString()});

    var client = iohttp.IOClient();

    var response = await client.get(uri);

    if (response.statusCode != 200) {
      throw 'valves.set.vacuum.failed.${response.statusCode}, ${response.body}';
    }
  }
}

class SetPressureCommand extends Command {
  final name = "pressure";
  final description = "Set pressure value.";

  SetPressureCommand() {
    argParser..addOption('value', abbr: 'v');
  }

  Future run() async {
    var value = double.tryParse(argResults['value']);
    if (value == null) throw 'valves.set.pressure.value.not.a.number';

    var m = await Pamservice().machines.current();

    var uri = Uri.parse('http://${m.ip}:8080/api/pamservice/valves').replace(
        queryParameters: {'cmd': 'set.pressure', 'value': value.toString()});

    var client = iohttp.IOClient();

    var response = await client.get(uri);

    if (response.statusCode != 200) {
      throw 'valves.set.pressure.failed.${response.statusCode}, ${response.body}';
    }
  }
}

class GetVacuumCommand extends Command {
  final name = "vacuum";
  final description = "Get vacuum value.";

  Future run() async {
    var m = await Pamservice().machines.current();

    var uri = Uri.parse('http://${m.ip}:8080/api/pamservice/valves')
        .replace(queryParameters: {'cmd': 'get.vacuum'});

    var client = iohttp.IOClient();

    var response = await client.get(uri);

    if (response.statusCode != 200) {
      throw 'valves.get.vacuum.failed.${response.statusCode}, ${response.body}';
    }

    var value = api.UnderPressureSupplierValue.json(json.decode(response.body));

    print(value.value);
  }
}

class GetPressureCommand extends Command {
  final name = "pressure";
  final description = "Get pressure value.";

  Future run() async {
    var m = await Pamservice().machines.current();

    var uri = Uri.parse('http://${m.ip}:8080/api/pamservice/valves')
        .replace(queryParameters: {'cmd': 'get.pressure'});

    var client = iohttp.IOClient();

    var response = await client.get(uri);

    if (response.statusCode != 200) {
      throw 'valves.get.pressure.failed.${response.statusCode}, ${response.body}';
    }

    var value = api.OverPressureSupplierValue.json(json.decode(response.body));

    print(value.value);
  }
}

class CloseWellCommand extends Command {
  final name = "well";
  final description = "Close selected well valves.";

  CloseWellCommand() {
    argParser..addOption('wellBitSet', abbr: 'w', defaultsTo: '1');
  }

  Future run() async {
    var wellBitSet = argResults['wellBitSet'];

    var m = await Pamservice().machines.current();

    var uri = Uri.parse('http://${m.ip}:8080/api/pamservice/valves').replace(
        queryParameters: {'cmd': 'close.well', 'wellBitSet': wellBitSet});

    var client = iohttp.IOClient();

    var response = await client.get(uri);

    if (response.statusCode != 200) {
      throw 'valves.close.well.failed.${response.statusCode}, ${response.body}';
//      if (response.statusCode == 400) {
//        throw 'valves.open.well.failed, ${response.body}';
//      } else {
//        throw 'valves.open.well.failed.${response.statusCode}';
//      }
    }
  }
}

class OpenWellCommand extends Command {
  final name = "well";
  final description = "Open selected well valves.";

  OpenWellCommand() {
    argParser..addOption('wellBitSet', abbr: 'w', defaultsTo: '1');
  }

  Future run() async {
    var wellBitSet = argResults['wellBitSet'];

    var m = await Pamservice().machines.current();

    var uri = Uri.parse('http://${m.ip}:8080/api/pamservice/valves').replace(
        queryParameters: {'cmd': 'open.well', 'wellBitSet': wellBitSet});

    var client = iohttp.IOClient();

    var response = await client.get(uri);

    if (response.statusCode != 200) {
      throw 'valves.open.well.failed.${response.statusCode}, ${response.body}';
//      if (response.statusCode == 400) {
//        throw 'valves.open.well.failed, ${response.body}';
//      } else {
//        throw 'valves.open.well.failed.${response.statusCode}';
//      }
    }
  }
}

class CloseValvesCommand extends Command {
  final name = "close";
  final description = "Close valves.";

  CloseValvesCommand() {
    addSubcommand(new CloseAllValvesCommand());
    addSubcommand(new CloseVacuumCommand());
    addSubcommand(new ClosePressureCommand());
    addSubcommand(new CloseWellCommand());
  }
}

class CloseAllValvesCommand extends Command {
  final name = "all";
  final description = "Close all valves.";

  CloseAllValvesCommand() {}

  Future run() async {
    var m = await Pamservice().machines.current();

    var uri = Uri.parse('http://${m.ip}:8080/api/pamservice/valves')
        .replace(queryParameters: {'cmd': 'close'});

    var client = iohttp.IOClient();

    var response = await client.get(uri);

    if (response.statusCode != 200) {
      throw 'valves.close.failed.${response.statusCode}, ${response.body}';
    }
  }
}

class OpenPressureCommand extends Command {
  final name = "pressure";
  final description = "Open pressure.";

  OpenPressureCommand() {}

  Future run() async {
    var m = await Pamservice().machines.current();

    var uri = Uri.parse('http://${m.ip}:8080/api/pamservice/valves')
        .replace(queryParameters: {'cmd': 'open.pressure'});

    var client = iohttp.IOClient();

    var response = await client.get(uri);

    if (response.statusCode != 200) {
      throw 'valves.open.pressure.failed.${response.statusCode}, ${response.body}';
//      if (response.statusCode == 400) {
//        throw 'valves.open.well.failed, ${response.body}';
//      } else {
//        throw 'valves.open.well.failed.${response.statusCode}';
//      }
    }
  }
}

class ClosePressureCommand extends Command {
  final name = "pressure";
  final description = "Close pressure.";

  ClosePressureCommand() {}

  Future run() async {
    var m = await Pamservice().machines.current();

    var uri = Uri.parse('http://${m.ip}:8080/api/pamservice/valves')
        .replace(queryParameters: {'cmd': 'close.pressure'});

    var client = iohttp.IOClient();

    var response = await client.get(uri);

    if (response.statusCode != 200) {
      throw 'valves.close.pressure.failed.${response.statusCode}, ${response.body}';
//      if (response.statusCode == 400) {
//        throw 'valves.open.well.failed, ${response.body}';
//      } else {
//        throw 'valves.open.well.failed.${response.statusCode}';
//      }
    }
  }
}

class OpenVacuumCommand extends Command {
  final name = "vacuum";
  final description = "Open vacuum.";

  OpenVacuumCommand() {}

  Future run() async {
    var m = await Pamservice().machines.current();

    var uri = Uri.parse('http://${m.ip}:8080/api/pamservice/valves')
        .replace(queryParameters: {'cmd': 'open.vacuum'});

    var client = iohttp.IOClient();

    var response = await client.get(uri);

    if (response.statusCode != 200) {
      throw 'valves.open.vacuum.failed.${response.statusCode}, ${response.body}';
    }
  }
}

class CloseVacuumCommand extends Command {
  final name = "vacuum";
  final description = "Close vacuum.";

  CloseVacuumCommand() {}

  Future run() async {
    var m = await Pamservice().machines.current();

    var uri = Uri.parse('http://${m.ip}:8080/api/pamservice/valves')
        .replace(queryParameters: {'cmd': 'close.vacuum'});

    var client = iohttp.IOClient();

    var response = await client.get(uri);

    if (response.statusCode != 200) {
      throw 'valves.close.vacuum.failed.${response.statusCode}, ${response.body}';
//      if (response.statusCode == 400) {
//        throw 'valves.open.well.failed, ${response.body}';
//      } else {
//        throw 'valves.open.well.failed.${response.statusCode}';
//      }
    }
  }
}
