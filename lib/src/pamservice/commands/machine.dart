part of pamgene.pamservice;

class MachineCommand extends Command {
  // The [name] and [description] properties must be defined by every
  // subclass.
  final name = "machine";
  final description = "Pamstation machine.";

  MachineCommand() {
    addSubcommand(new MachineListCommand());
    addSubcommand(new MachineCurrentCommand());
  }
}

class MachineListCommand extends Command {
  // The [name] and [description] properties must be defined by every
  // subclass.
  final name = "ls";
  final description = "Display a list of available pamstation machine.";

  MachineListCommand() {
    // [argParser] is automatically created by the parent class.
//    argParser.addFlag('ls', abbr: 'l');
  }

  // [run] may also return a Future.
  Future run() async {
    var list = await Pamservice().machines.list();
    if (list.isEmpty) {
      print('no machine configured');
    } else {
      for (var m in list) {
        print('${m.name} ${m.ip} ');
      }
    }
  }
}

class MachineCurrentCommand extends Command {
  final name = "current";
  final description = "Display or set the current pamstation.";

  MachineCurrentCommand() {
    argParser.addOption('name', abbr: 'n', defaultsTo: '');
  }

  // [run] may also return a Future.
  Future run() async {
    var name = argResults['name'] as String;
    if (name == null || name.isEmpty) {
      var m = await Pamservice().machines.current();
      if (m == null) {
        print('no current machine defined');
      } else {
        print(m.name);
        print(m.ip);
      }
    } else {
      Pamservice().machines.setCurrent(name);
      print('set machine ${name} as current');
    }
  }
}
