part of pamgene.pamservice;

class MessageCommand extends Command {
  final name = "listen";
  final description = "Listen to the pamstation messages.";

  Future run() async {
    var m = await Pamservice().machines.current();
    var ps12 = new ps12run.PS12(Uri.parse('http://${m.ip}:8080'));
    await for (var msg in ps12.messages()) {
      print(msg);
    }
  }
}

class StatusCommand extends Command {
  final name = "status";
  final description = "Pamstation status.";

  StatusCommand() {}

  Future run() async {
    var m = await Pamservice().machines.current();

    var uri = Uri.parse('http://${m.ip}:8080/api/status');

    var client = iohttp.IOClient();

    var response = await client.get(uri);

    if (response.statusCode != 200) {
      throw '${name}.failed.${response.statusCode}, ${response.body}';
    }

    print(response.body);
  }
}

class TurnOnCommand extends Command {
  final name = "on";
  final description = "Turn on current pamstation.";

  TurnOnCommand() {}

  Future run() async {
    var m = await Pamservice().machines.current();

    var uri = Uri.parse('http://${m.ip}:8080/api/turnOn');

    var client = iohttp.IOClient();

    var response = await client.get(uri);

    if (response.statusCode != 200) {
      throw '${name}.failed.${response.statusCode}, ${response.body}';
    }
  }
}

class ShutdownCommand extends Command {
  final name = "off";
  final description = "Shutdown current pamstation.";

  ShutdownCommand() {}

  Future run() async {
    var m = await Pamservice().machines.current();

    var uri = Uri.parse('http://${m.ip}:8080/api/shutdown');

    var client = iohttp.IOClient();

    var response = await client.get(uri);

    if (response.statusCode != 200) {
      throw '${name}.failed.${response.statusCode}, ${response.body}';
    }
  }
}
