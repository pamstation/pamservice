library pamgene.pamservice;

import 'dart:io';
import 'dart:async';
import 'dart:convert';
import 'package:path/path.dart' as libpath;
import 'package:logging/logging.dart';
import 'package:args/command_runner.dart';
import 'package:pamservice/src/pamservice/config/config.dart';
import 'package:pamservice/src/util/config/config.dart' as utilconfig;
import 'package:pamservice/src/util/logger/logger.dart' as utillog;
import 'package:yaml/yaml.dart';
import 'package:http/io_client.dart' as iohttp;
import 'package:pamservice/ps_model.dart' as api;
import 'package:pamservice/dart_ps12.dart' as ps12run;
import 'package:pamservice/version.dart' as version;

part 'commands/config.dart';
part 'commands/watch.dart';
part 'commands/machine.dart';
part 'commands/camera.dart';
part 'commands/valves.dart';
part 'commands/wagon.dart';
part 'commands/settings.dart';
part 'commands/status.dart';
part 'commands/led.dart';
part 'commands/pump.dart';
part 'commands/aspirate.dart';
part 'commands/dispense.dart';
part 'commands/protocol.dart';
part 'commands/button.dart';
part 'commands/setup.dart';
part 'commands/version.dart';
part 'commands/controllable.dart';
part 'commands/filter.dart';

class Pamservice {
  static Logger logger = new Logger('Pamservice');

  StreamSubscription signals;
  CommandRunner commandRunner;

  Pamservice() {
//    initLogger();

    initConfig(null);

    initLogger();

    signals = watchProcessSignal();

    commandRunner =
        new CommandRunner("pamservice", "Pamstation service software.")
          ..addCommand(VersionCommand())
          ..addCommand(ConfigCommand())
          ..addCommand(SettingsCommand())
          ..addCommand(ModeCommand())
          ..addCommand(WatchCommand())
          ..addCommand(MachineCommand())
          ..addCommand(WagonCommand())
          ..addCommand(CameraCommand())
          ..addCommand(ValvesCommand())
          ..addCommand(StatusCommand())
          ..addCommand(MessageCommand())
          ..addCommand(TurnOnCommand())
          ..addCommand(ShutdownCommand())
          ..addCommand(PumpCommand())
          ..addCommand(AspirateCommand())
          ..addCommand(DispenseCommand())
          ..addCommand(LedCommand())
          ..addCommand(ProtocolCommand())
          ..addCommand(ButtonCommand())
          ..addCommand(SetupCommand())
          ..addCommand(ControllableCommand())..addCommand(FilterCommand());
  }

  utilconfig.Config get config => utilconfig.Config();

  Machines get machines => Machines(config['machine.dir'] as String);

  void run(List<String> args) async {
    try {
      await commandRunner.run(args);
    } catch (e) {
      await signals.cancel();
      if (config['error.show.stacktrace'] == 'true')
        rethrow;
      else
        print('Command failed : $e');
    }
    await signals.cancel();
  }

  void initLogger() {
    var config = new utilconfig.Config();
    int logLevel = 2000;
    if (config['log.level'] != null) {
      logLevel = int.parse(config['log.level'] as String);
    }
    utillog.initLogger('Main', logLevel);
  }

  StreamSubscription watchProcessSignal() {
    return ProcessSignal.sigint.watch().listen((evt) {
      exit(126);
    });
  }
}

class WaitCommand extends Command {
  // The [name] and [description] properties must be defined by every
  // subclass.
  final name = "wait";
  final description = "Wait 5 seconds.";

  WaitCommand() {
    // [argParser] is automatically created by the parent class.
    argParser.addFlag('wait', abbr: 'w');
  }

  // [run] may also return a Future.
  Future run() async {
    // [argResults] is set before [run()] is called and contains the options
    // passed to this command.
    await Future.delayed(Duration(seconds: 5));
  }
}

class Machines {
  String machineDir;
  String machinesFile;
  String currentMachineFile;

  Machines(this.machineDir) {
    if (machineDir != null) {
      var file = libpath.join(machineDir, 'machines.yaml');
      if (File(file).existsSync()) {
        machinesFile = file;
      }
      file = libpath.join(machineDir, 'current');
      if (File(file).existsSync()) {
        currentMachineFile = file;
      }
    }
  }

  Future<api.Settings> getMachineSettings(Machine m) async {
    var folder = libpath.join(machineDir, m.name);
    var file = libpath.join(folder, 'settings.json');
    if (!File(file).existsSync()) {
      throw 'Settings file not found in ${file}';
    }
    return api.Settings.json(json.decode(File(file).readAsStringSync()));
  }

  Future<String> setMachineSettings(Machine m, api.Settings settings) async {
    if (settings.unitName != m.name) {
      print(
          'WARNING -- settings.unitName "${settings.unitName}" is different from machine.name "${m.name}"');
      print(
          "WARNING -- locally overriding settings.unitName by machine.name ...");
      print(
          'WARNING -- you can now run "pamservice settings set" commmand to set correct unitName "${m.name}"');
      settings..unitName = m.name;
    }

    var folder = libpath.join(machineDir, m.name);
    Directory(folder).createSync(recursive: true);
    var file = libpath.join(folder, 'settings.json');
    File(file)
        .writeAsStringSync(JsonEncoder.withIndent('  ').convert(settings));
    return file;
  }

  Future<String> setPSServerVersion(Machine m, api.Versions versions) async {
    var folder = libpath.join(machineDir, m.name);
    Directory(folder).createSync(recursive: true);
    var file = libpath.join(folder, 'versions.json');
    File(file)
        .writeAsStringSync(JsonEncoder.withIndent('  ').convert(versions));
    return file;
  }

  Future<List<Machine>> list() async {
    if (machinesFile == null) return [];
    var map = loadYaml(File(machinesFile).readAsStringSync()) as Map;
    var list = map['machines'];
    return list.map((m) => Machine.fromJson(m)).cast<Machine>().toList();
  }

  bool hasCurrentMachine() {
    var currentPs = Platform.environment['CURRENT_PS'];
    if (currentPs != null && currentPs.isNotEmpty) {
      return true;
    }
    return currentMachineFile != null;
  }

  Future<Machine> current() async {
    var currentPs = Platform.environment['CURRENT_PS'];

    if (currentPs != null && currentPs.isNotEmpty) {
      return findMachineByName(currentPs);
    }
    if (currentMachineFile == null) throw 'no current machine defined';
    var map = loadYaml(File(currentMachineFile).readAsStringSync()) as Map;
    var m = map['current'];
    return Machine.fromJson(m);
  }

  Future<Machine> findMachineByName(String name) async {
    var machines = await list();
    var machine =
        machines.firstWhere((m) => m.name == name, orElse: () => null);
    if (machine == null) throw 'machine $name not found';
    return machine;
  }

  Future<Machine> setCurrent(String name) async {
    var machine = await findMachineByName(name);
    if (currentMachineFile == null) {
      currentMachineFile = libpath.join(machineDir, 'current');
    }
    File(currentMachineFile).writeAsStringSync('''current:
  name: ${machine.name}
  ip: ${machine.ip}
    ''');
    return machine;
  }
}

class Machine {
  String name;
  String ip;

  Machine(this.name, this.ip);
  Machine.fromJson(m) {
    name = m['name'] as String;
    ip = m['ip'] as String;
    if (name == null) throw 'machine.name.required';
//    if (ip == null) throw 'machine.ip.required';
    if (ip == null) ip = name;
  }
}

Future startProcess(String executable, List<String> arguments,
    {String workingDirectory, Map<String, String> environment}) async {
  print('start --  in ${workingDirectory}');
  print('${executable} ${arguments} ${environment}');
  var process = await Process.start(executable, arguments,
      runInShell: false,
      workingDirectory: workingDirectory,
      environment: environment);
  stdout.addStream(process.stdout);
  stderr.addStream(process.stderr);

  var exit = await process.exitCode;

  if (exit != 0) throw '$executable failed -- $exit';
}
