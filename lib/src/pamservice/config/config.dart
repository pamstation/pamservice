import 'dart:io';
import 'package:path/path.dart' as pathlib;
import 'package:yaml/yaml.dart';
import 'default_config.dart' as tconf;

import 'package:logging/logging.dart';

import '../../util/config/config.dart' as utilconfig;

void initConfig([String configFile]) {
  var logger = new Logger('initConfig');

  var config = new utilconfig.Config()
    ..initialize(loadYaml(tconf.DEFAULT_CONFIG) as Map);

  var fileLocations = ['pamservice.yaml'];

//  if (Platform.isLinux)
//    fileLocations.add('/etc/pamgene/pamservice/pamservice.yaml');

  if (configFile != null) {
    fileLocations.add(configFile);
  } else if (Platform.environment.containsKey('PAMSERVICE_CONFIG_PATH')) {
    fileLocations.add(Platform.environment['PAMSERVICE_CONFIG_PATH']);
  }
//  else {
//    Uri configFolderUri;
//    configFolderUri = Platform.script;
//
////    print('configFolderUri $configFolderUri');
//
//    if (configFolderUri.isScheme('file')) {
//      fileLocations.add(configFolderUri
//          .replace(
//              pathSegments: new List.from(configFolderUri.pathSegments)
//                ..removeLast()
//                ..add('pamservice.yaml'))
//          .toFilePath());
//    }
//  }

  var configPath = fileLocations.firstWhere(
      (path) => new File(path as String).existsSync(),
      orElse: () => null);

//  print('Loading config from ${configPath}');

  if (configPath == null) {
    logger.config(
        'Could not find config file in any locations ${fileLocations}, use default config');
  } else {
    logger.config('Loading config from ${configPath}');

    config.override(
        loadYaml(new File(configPath as String).readAsStringSync()) as Map);
  }

  // user config
  String userConfigPath;
  if (Platform.isLinux) {
    userConfigPath = '~/pamgene/pamservice/pamservice.yaml';
  } else if (Platform.isWindows) {
    userConfigPath =
        '${Platform.environment["HOMEDRIVE"]}${Platform.environment["HOMEPATH"]}\\pamgene\\pamservice\\pamservice.yaml';
  }

  if (new File(userConfigPath).existsSync()) {
    try {
      logger.config('Loading user config from ${userConfigPath}');
      config.override(
          loadYaml(new File(userConfigPath).readAsStringSync()) as Map);
    } catch (e, st) {
      logger.severe('User config failed : ${userConfigPath}, exit', e, st);
      exit(1);
    }
  } else {
    logger.config('No user config provided at ${userConfigPath}');
  }

//  config.addAll(Platform.environment);

  // convert relative path to absolute

  var pathList = [
    'machine.dir',
  ];

  var mm = {};
  pathList.forEach((dir) {
    var path = config[dir] as String;
    if (!pathlib.isAbsolute(path)) {
      path = pathlib.absolute(path);
    }

    path = pathlib.normalize(path);
    path = pathlib.toUri(path).toFilePath(windows: Platform.isWindows);
    mm[dir] = path;
  });

  config.addAll(mm);

//  print('Config -------------------');
//  config.asMap().forEach((k, v) {
//    print('${k}=${v}');
//  });
//  print('Config -------------------');
}
