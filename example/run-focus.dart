import 'dart:io';
import 'package:pamservice/dart_ps12.dart';
import 'package:path/path.dart' as libpath;
import 'package:yaml/yaml.dart' as yaml;

main() async {
  var config = new Map.from(
      yaml.loadYaml(new File('bin/config.yaml').readAsStringSync()));

  print('======== config :');
  config.forEach((k, v) {
    print('$k=$v');
  });
  print('========');
  print('');

  var ps12 = new PS12(Uri.parse(config['psserver.url']));

//  try {
//    await ps12.ping();
//  } catch (e) {
//    print('Connection to psserver failed');
//    rethrow;
//  }

  print('Checking server status');

  var status = await ps12.status;

  if (status == '0') {
    await ps12.turnOn();
  } else if (status == '1') {
    var currentProtocol = await ps12.getCurrentProtocol();
    if (currentProtocol != null) {
      print('unloading current protocol');
      await currentProtocol.unload();
    }
  } else if (status == '2') {
    throw 'A protocol is already running ... ';
  }

  var dataDir = config['data.dir'];
  var protocolFile = config['protocol'];
  List<double> focusValues = new List.from(config['focus.values']);

  var date = new DateTime.now();
  var baseDir = libpath.join(dataDir,
      '${date.year}${date.month}${date.day}${date.hour}${date.minute}-${libpath.basenameWithoutExtension(protocolFile)}');

  var imageDir = baseDir.replaceAll('/', '\\');
  new Directory(imageDir).createSync(recursive: true);

  var annotationFile = new File(libpath.join(baseDir, 'focus-annotation.txt'));

  var runId = '${date.year.toString().substring(3)}'
      '${date.month.toString().padLeft(2, '0')}'
      '${date.day.toString().padLeft(2, '0')}'
      '${date.hour.toString().padLeft(2, '0')}'
      '${date.minute.toString().padLeft(2, '0')}';

  print('======== output');
  print('runId = ${runId}');
  print('annotation file = ${annotationFile.path}');
  print('images = ${imageDir}');
  print('========');
  print('');

  var count = 1;

  var annotation = new StringBuffer()
    ..write('focusRunId')
    ..write('\t')
    ..write('Barcode')
    ..write('\t')
    ..write('PamChip Location')
    ..write('\t')
    ..write('focus')
    ..write('\r\n');

  List<Protocol> protocols = focusValues.map((focus) {
    var prefix = '${runId}${count.toString().padLeft(2, '0')}';

    count++;

    var protocol = ps12.protocolFromFile(protocolFile)
      ..setPath(imageDir)
      ..setBarcodes(['${prefix}1', '${prefix}2', '${prefix}3']);

    annotation
      ..write(runId)
      ..write('\t')
      ..write('${prefix}1')
      ..write('\t')
      ..write('1')
      ..write('\t')
      ..write(focus)
      ..write('\r\n');
    annotation
      ..write(runId)
      ..write('\t')
      ..write('${prefix}2')
      ..write('\t')
      ..write('2')
      ..write('\t')
      ..write(focus)
      ..write('\r\n');
    annotation
      ..write(runId)
      ..write('\t')
      ..write('${prefix}3')
      ..write('\t')
      ..write('3')
      ..write('\t')
      ..write(focus)
      ..write('\r\n');

    return protocol;
  }).toList();

  annotationFile.writeAsStringSync(annotation.toString());

  var sub = ps12.button.onStateChange.listen((b) {
    if (b) {
      print('Button is enabled, press');
      ps12.button.press();
    }
  });

  print('======== run');

  var focusSettings0 = await ps12.getFocusSetting();

  print('original focusSettings : ${focusSettings0.toXML()}');

  try {
    var focusSettings = await ps12.getFocusSetting();

    count = 0;
    for (var protocol in protocols) {
      print('progress : ${count+1}/${protocols.length}');

      focusSettings
        ..rOffsetForDisp1 = focusValues[count]
        ..rOffsetForDisp2 = focusValues[count]
        ..rOffsetForDisp3 = focusValues[count];

      count++;

      print('setting focusSettings : ${focusSettings.toXML()}');
      await ps12.setFocusSetting(focusSettings);

      print('loading protocol ...');
      await protocol.load();

      print('running protocol ...');
      await protocol.run();

      print('waiting protocol ...');
      await protocol.wait();

      print('unloading protocol ...');
      await protocol.unload();
    }
  } finally {
    print('setting original focusSettings : ${focusSettings0.toXML()}');
    await ps12.setFocusSetting(focusSettings0);
    sub.cancel();
  }

  print('======== done');
}
