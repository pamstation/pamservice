import 'dart:io';
import 'package:pamservice/dart_ps12.dart';
import 'package:yaml/yaml.dart' as yaml;

main() async {
  var config = yaml.loadYaml(new File('bin/config.yaml').readAsStringSync());

  var ps12 = new PS12(Uri.parse(config['psserver.url']));

  var runNumber = config['run.number'];

  for (var i = 0; i < runNumber; i++) {
    var currentProtocol = await ps12.getCurrentProtocol();

    if (currentProtocol != null) await currentProtocol.unload();

    var protocol = ps12.protocolFromFile(config['protocol'])
      ..setPath('C:\\DATA\\psserver\\focus')
      ..setBarcodes(['00000000', '11111111', '22222222']);

    await protocol.load();

    print('protocol.load ok');

    var sub = ps12.button.onStateChange.listen((b) {
      if (b) {
        print('Button is enabled, press');
        ps12.button.press();
      }
    });

    try {
      await protocol.run();
      print('protocol.run ok');
      await protocol.wait();
      print('protocol.wait ok');
    } finally {
      sub.cancel();
    }
  }
}
