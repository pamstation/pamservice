import 'dart:io';
import 'package:pamservice/dart_ps12.dart';
import 'package:yaml/yaml.dart' as yaml;

main() async {
  var config = yaml.loadYaml(new File('bin/config.yaml').readAsStringSync());

  var ps12 = new PS12(Uri.parse(config['psserver.url']));

  var focusSettings = await ps12.getFocusSetting();

  print(focusSettings.toXML());

  focusSettings.rOffsetForDisp1 += 0.1;

  await ps12.setFocusSetting(focusSettings);

  var focusSettings2 = await ps12.getFocusSetting();

  assert(focusSettings.toXML() == focusSettings2.toXML());

  print('ps12 set focus setting ok');

}
