import 'dart:io';
import 'package:pamservice/dart_ps12.dart';
import 'package:yaml/yaml.dart' as yaml;

main() async {
  var config = yaml.loadYaml(new File('bin/config.yaml').readAsStringSync());

  var ps12 = new PS12(Uri.parse(config['psserver.url']));

  await ps12.ping();
  print('ping ok');

  print('ps12 status ${await ps12.status}');

  var button = ps12.button;

  print('button ${await button.status}');

  var focusSettings = await ps12.getFocusSetting();

  print(focusSettings.toXML());


}
