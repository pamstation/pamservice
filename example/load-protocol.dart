import 'dart:io';
import 'package:pamservice/dart_ps12.dart';
import 'package:yaml/yaml.dart' as yaml;

main() async {
  var config = yaml.loadYaml(new File('bin/config.yaml').readAsStringSync());

  var ps12 = new PS12(Uri.parse(config['psserver.url']));

  var currentProtocol = await ps12.getCurrentProtocol();

  if (currentProtocol != null) await currentProtocol.unload();

  var protocol = ps12.protocolFromFile('bin/protocols/loadUnload.xml')
    ..setPath('C:\\DATA\\psserver\\focus')
    ..setBarcodes(['00000000', '11111111', '22222222']);

  await protocol.load();

  print('protocol.load ok');

  currentProtocol = await ps12.getCurrentProtocol();

  assert(currentProtocol != null);

//  assert(currentProtocol.xmlProtocol == protocol.xmlProtocol);

  await currentProtocol.unload();

  print('protocol.unload ok');

  currentProtocol = await ps12.getCurrentProtocol();

  assert(currentProtocol == null);
}
