import 'dart:io';
import 'package:pamservice/dart_ps12.dart';
import 'package:path/path.dart' as libpath;
import 'package:yaml/yaml.dart' as yaml;

main() async {
  var config = new Map.from(
      yaml.loadYaml(new File('bin/config-loop.yaml').readAsStringSync()));

  print('======== config :');
  config.forEach((k, v) {
    print('$k=$v');
  });
  print('========');
  print('');

  var ps12 = new PS12(Uri.parse(config['psserver.url']));

//  try {
//    await ps12.ping();
//  } catch (e) {
//    print('Connection to psserver failed');
//    rethrow;
//  }

  print('Checking server status');

  var status = await ps12.status;

  if (status == '0') {
    await ps12.turnOn();
  } else if (status == '1') {
    var currentProtocol = await ps12.getCurrentProtocol();
    if (currentProtocol != null) {
      print('unloading current protocol');
      await currentProtocol.unload();
    }
  } else if (status == '2') {
    throw 'A protocol is already running ... ';
  }

  var dataDir = config['data.dir'];
  var protocolFile = config['protocol'];

  var date = new DateTime.now();
  var baseDir = libpath.join(dataDir,
      '${date.year}${date.month}${date.day}${date.hour}${date.minute}-${libpath.basenameWithoutExtension(protocolFile)}');

  var imageDir = baseDir.replaceAll('/', '\\');
  new Directory(imageDir).createSync(recursive: true);

  var runId = '${date.year.toString().substring(3)}'
      '${date.month.toString().padLeft(2, '0')}'
      '${date.day.toString().padLeft(2, '0')}'
      '${date.hour.toString().padLeft(2, '0')}'
      '${date.minute.toString().padLeft(2, '0')}';

  print('======== output');
  print('runId = ${runId}');
  print('images = ${imageDir}');
  print('========');
  print('');

  var sub = ps12.button.onStateChange.listen((b) {
    if (b) {
      print('Button is enabled, press');
      ps12.button.press();
    }
  });

  print('======== run');

  try {
    var runNumber = config['run.number'];

    for (var i = 0; i < runNumber; i++) {
      try {
        var prefix = '${runId}${i.toString().padLeft(2, '0')}';

        var protocol = ps12.protocolFromFile(protocolFile)
          ..setPath(imageDir)
          ..setBarcodes(['${prefix}1', '${prefix}2', '${prefix}3']);

        print('progress : ${i+1}/${runNumber}');

        print('loading protocol ...');
        await protocol.load();

        print('running protocol ...');
        await protocol.run();

        print('waiting protocol ...');
        await protocol.wait();

        print('unloading protocol ...');
        await protocol.unload();
      } catch (e){
        print(e);
      }
    }
  } finally {
    sub.cancel();
  }

  print('======== done');
}
