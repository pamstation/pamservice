import 'dart:io';
import 'package:pamservice/dart_ps12.dart';
import 'package:yaml/yaml.dart' as yaml;

main() async {
  var config = yaml.loadYaml(new File('bin/config.yaml').readAsStringSync());

  var ps12 = new PS12(Uri.parse(config['psserver.url']));

  var status = await ps12.status;

  if (status == '1'){
    print('status $status ');
    await ps12.shutdown();
    print('shutdown ok');
  }

  status = await ps12.status;

  if (status == '0'){
    print('status $status ');
    await ps12.turnOn();
    print('turnOn ok');
  }

}
