# Setup

Install chocolatey. In an admin shell run the following 
 

```
@"%SystemRoot%\System32\WindowsPowerShell\v1.0\powershell.exe" -NoProfile -InputFormat None -ExecutionPolicy Bypass -Command "iex ((New-Object System.Net.WebClient).DownloadString('https://chocolatey.org/install.ps1'))" && SET "PATH=%PATH%;%ALLUSERSPROFILE%\chocolatey\bin"

```

Install dart-sdk using chocolatey, in a new admin shell run the following

```
choco install -y dart-sdk --version 2.4.0
```

Install git

```
choco install -y git
```

Then clone this repo. Open git command and execute the following

``` 
cd C:\PamSoft\PS12
git clone https://bitbucket.org/pamstation/pamservice.git
cd pamservice
pub get
```

# Start and stop psserver


```bash
systemctl start psserver
systemctl stop psserver
systemctl restart psserver
systemctl status psserver
```

# Usage

## Config

see bin/config.yaml


## Run the focus protocol

Start PSServer.exe.

``` 
cd C:\PamSoft\PS12\dart_ps12
run.bat
```

# Run loop

see bin/config-loop
 

``` 
cd C:\PamSoft\PS12\dart_ps12
run-loop.bat
```

# Pamservice

```bash
pub global activate --overwrite --source path ~/dev/pg/pamservice/ --executable=pamservice 
pub global activate --source path C:\PamSoft\PS12\pamservice
 
pub global activate --overwrite --executable=pamservice  --source git https://bitbucket.org/pamstation/pamservice.git 

pub global activate --overwrite --source git https://github.com/pamgene/pub.git 
pampub global activate --overwrite --source git https://bitbucket.org/pamstation/pamservice.git 1.1.1 

```
 

## examples

```bash
pub global deactivate pamservice
pub global activate --overwrite --source path ~/dev/pg/pamservice/ --executable=pamservice 

cd bin

pamservice --help
pamservice -h

pamservice mode get
# switching to emulation mode
pamservice mode set --no-stub
# switching to normal mode
pamservice mode set --stub
# mode update need a shutdown and restart to be taken into account

pamservice status
pamservice on
pamservice shutdown

pamservice ctrl -n wagon.x
pamservice ctrl -n wagon.y

pamservice wagon -h
pamservice wagon go -h
pamservice wagon move -h

pamservice wagon move
pamservice wagon go -p home
pamservice wagon go -p front
pamservice wagon go -p load
pamservice wagon go -p loadSafe
pamservice wagon move
pamservice wagon go -p read -w 2
pamservice wagon move

pamservice led on

pamservice camera read -h
pamservice camera read
pamservice camera read -w 2 -e 100
pamservice camera read -w 10 -e 100 -f 2

pamservice watch -h
pamservice watch temperature
pamservice watch temperature -t 37 -e 5

pamservice watch pressure
pamservice watch pressure -e -1
pamservice watch pressure -w 3 -e 5 

pamservice pump status
pamservice pump on
pamservice pump off

pamservice machine -h
pamservice machine ls
pamservice machine current
pamservice machine current -n dev

pamservice settings -h
pamservice settings get

pamservice valves close
pamservice valves open well --wellBitSet=3
pamservice valves open pressure
pamservice valves open vacuum

pamservice valves get pressure
pamservice valves get vacuum

pamservice valves set pressure --value=2
pamservice valves set vacuum --value=2

pamservice led on

pamservice camera focus -h
pamservice camera focus

pamservice camera snapshot
pamservice camera snapshot -e 150

pamservice pump run -h
pamservice dispense run -h
pamservice dispense now -h
pamservice dispense now --amount 50

pamservice aspirate run -h
pamservice aspirate up 
pamservice aspirate down 

pamservice mode set --stub
pamservice mode set --no-stub --no-stubBoard1 --no-stubBoard2 --no-stubBoard3

pamstation filter move -p 200

```

## scenario

```bash

pamservice machine ls
pamservice machine current
pamservice machine current --name myps12
pamservice status
pamservice on

pamservice settings get
# edit settings file
pamservice settings set

```


## current machine

```bash
# on windows
# set CURRENT_PS=localhost
CURRENT_PS=localhost
pamservice machine current

```


# run protocol

```bash
pamservice status
pamservice on
pamservice listen

pamservice protocol run -h
pamservice protocol run -f protocols/read-3filters.xml
# here user will be asked to press the button
pamservice protocol run -f protocols/read-3filters.xml --press=false
pamservice protocol run -f protocols/read-3filters.xml --press=false --repeat=2

pamservice protocol abort
pamservice protocol unload

pamservice protocol listen

pamservice button press
```


```bash

dart --snapshot-kind=kernel --snapshot=bin/pamservice.dart.snapshot.dart2 bin/pamservice.dart
 
dart2aot bin/pamservice.dart psclient/bin/pamservice.dart.aot
dartaotruntime bin/pamservice.dart.aot
```
Training test AWi 11-01-24