import 'package:sci_api_gen/api_lib_mirror.dart' as apiMirror;
import 'package:sci_api_gen/src/model/api_model_generator.dart';
import 'package:pamservice/ps_model_api.dart';

main() async {
  var libraryName = 'ps_model';
  var directory = 'lib';

  var builder = new apiMirror.ApiLibraryMirror();

  var apiLib = builder.build('ps.model.api');

  var gen = new ClassGenerator(apiLib, directory, libraryName, 'base.Base', [
    {'lib': 'dart:async'},
    {'lib': 'dart:typed_data'},
    {'lib': 'dart:collection'},
    {'lib': 'dart:convert'},
    {'lib': 'dart:math', 'as': 'math'},
    {'lib': 'package:logging/logging.dart'},
    {'lib': 'package:async/async.dart', 'as': 'async'},
    {'lib': 'package:uuid/uuid.dart', 'as': 'uuid'},
    {'lib': 'package:collection/collection.dart', 'as': 'collection'},
    {'lib': 'package:sci_api_gen/sci_base.dart', 'as': 'base'},
    {'lib': '${libraryName}_base.dart'},
  ], []);

  await gen.run();
}
