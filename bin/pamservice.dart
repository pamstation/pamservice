import 'dart:io';
import 'package:pamservice/src/pamservice/pamservice.dart';

main(List<String> args) async {
  await Pamservice().run(args);
//  await Pamservice().run(['config']);
//  await Pamservice().run(['machine', 'ls']);
  exit(0);
}