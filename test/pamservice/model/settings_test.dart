import 'dart:convert' as prefix0;

import 'package:pamservice/src/pamservice/config/config.dart';
import 'package:pamservice/src/util/config/config.dart' as utilconfig;
import 'package:test/test.dart';
import 'package:pamservice/ps_model.dart' as api;
import 'dart:convert';

import 'package:http/io_client.dart' as iohttp;

void main() {
  group('A group of config tests', () {
    setUp(() {});

    test('Test model', () async {
      var calibration = api.Settings();

      var client = iohttp.IOClient();
      var response = await client.get('http://127.0.0.1:8080/api/pamservice/settings');

      expect(response.statusCode, equals(200));

      calibration = api.Settings.json(json.decode(response.body));


      calibration.wagon_logicalXYPos_t.xyLoadPosition
        ..x = 42.0
        ..y = -42.0;

      calibration.wagon_logicalXYPos_t.xyFrontPanelPosition
        ..x = 42.0
        ..y = -42.0;

      response = await client.put('http://127.0.0.1:8080/api/pamservice/settings',
          body: json.encode(calibration.toJson()));
      expect(response.statusCode, equals(200));

      response = await client.get('http://127.0.0.1:8080/api/pamservice/settings');

      expect(response.statusCode, equals(200));

      calibration = api.Settings.json(json.decode(response.body));

      print(JsonEncoder.withIndent('  ').convert(calibration.toJson()));


      expect(calibration.wagon_logicalXYPos_t.xyLoadPosition.x, equals(42.0));
      expect(calibration.wagon_logicalXYPos_t.xyLoadPosition.y - 42.42,
          lessThanOrEqualTo(double.minPositive));
    });
  });
}
