import 'dart:io';
import 'package:pamservice/src/pamservice/config/config.dart';
import 'package:pamservice/src/pamservice/pamservice.dart';
import 'package:pamservice/src/util/config/config.dart' as utilconfig;
import 'package:test/test.dart';

void main() {
  group('A group of machines tests', () {
    setUp(() {});

    test('List', () async {
      var list = await Pamservice().machines.list();
      expect(list, isEmpty);

      var dir = await Directory.systemTemp.createTemp();
      var file = File('${dir.path}/machines.yaml');
      file.writeAsStringSync(''' 
machines:
  - name: local
    ip: 127.0.0.1
  - name: dev
    ip: 10.110.253.97      
      ''');

      list = await Machines(dir.path).list();
      expect(list, isNotEmpty);
      expect(list.length, equals(2));
      expect(list.first.name, equals('local'));
      expect(list.first.ip, equals('127.0.0.1'));

    });
  });
}
