import 'package:pamservice/src/pamservice/config/config.dart';
import 'package:pamservice/src/util/config/config.dart' as utilconfig;
import 'package:test/test.dart';

void main() {
  group('A group of config tests', () {

    setUp(() {

    });

    test('Test config', () {
      initConfig(null);
      var config = utilconfig.Config();

      expect((config['machine.dir'] as String).isNotEmpty, isTrue);
     });
  });
}
