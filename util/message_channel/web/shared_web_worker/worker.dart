//import 'package:message_channel/message_channel.dart' as mc;
import 'dart:js' as js;

void main(args, messageChannelPort) {
  print("worker started $messageChannelPort");

  var mc = new js.JsObject(js.context["MessageChannel"], []);
  print("mc $mc");

  var port = mc["port2"];
  print("port $port");

  var args = new js.JsArray.from([
    'raw sendport',
    new js.JsArray.from([port])
  ]);

  js.context.callMethod("postMessage", [
    args,
    new js.JsArray.from([port])
  ]);
}

//'raw sendport'
