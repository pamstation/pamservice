import 'dart:html' as html;
import 'dart:isolate' as isolate;

import 'package:message_channel/message_channel.dart' as mc;

int childWindowId = 0;

void main() {
  mc.MessageChannel channel = new mc.SharedWorkerMessageChannel("mychannel");
  channel.onMessage.listen((msg) {
    html
        .querySelector("#list")
        .children
        .add(new html.Element.html("<li>${msg.toString()}</li>"));
  });

  html.querySelector("#in").onInput.listen((_) {
    channel.sendMessage(
        {"in": (html.querySelector("#in") as html.InputElement).value});
  });

  html.querySelector("#btn").onClick.listen((_) {
    html.window.open(Uri.base.toString(), "child window $childWindowId");
    childWindowId++;
  });

  isolate.Isolate.spawnUri(Uri.parse("worker.dart"), [], "mychannel");
}
