//Création d'une zone d'affichage
var elem = document.createElement("output");
elem.value = "Log :";
document.body.appendChild(elem);

if(window.SharedWorker){
//le navigateur supporte les shared-workers
  var w = new SharedWorker("worker.js");
  w.port.onmessage = function(e){
    elem.innerHTML += "<br />" + e.data;
  };
  w.port.postMessage("Bonjour");

}else{
  elem.textContent="Votre navigateur ne "+
    "supporte pas les shared-workers ☹";
}