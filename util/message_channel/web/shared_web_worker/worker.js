 
var channels = new HashTable({});
 

//nouvelle connexion
onconnect=function(e){

  var port = e.ports[0];
  
  port.onmessage = function(event){
    if (event.data["type"] == "registerChannel"){
      var channelName = event.data["channelName"];
      var ports;
      if (channels.hasItem(channelName)){
        ports = channels.getItem(channelName) ;
      } else {
        ports = [];
        channels.setItem(channelName, ports);
      }
      ports[ports.length] = port;
    } else if (event.data["type"] == "message"){
        
      var channelName = event.data["channelName"];
       
      if (channels.hasItem(channelName)){
        var ports = channels.getItem(channelName) ;
        for ( index = 0; index < ports.length ; index++){
          var p = ports[index];
          if (p != port) p.postMessage(event.data);
 
        }
      } else {
      //port.postMessage("!!!!   channels.hasItem ");
      }
    }
  };
  
}; 


function HashTable(obj)
{
    this.length = 0;
    this.items = {};
    for (var p in obj) {
        if (obj.hasOwnProperty(p)) {
            this.items[p] = obj[p];
            this.length++;
        }
    }

    this.setItem = function(key, value)
    {
        var previous = undefined;
        if (this.hasItem(key)) {
            previous = this.items[key];
        }
        else {
            this.length++;
        }
        this.items[key] = value;
        return previous;
    }

    this.getItem = function(key) {
        return this.hasItem(key) ? this.items[key] : undefined;
    }

    this.hasItem = function(key)
    {
        return this.items.hasOwnProperty(key);
    }
   
    this.removeItem = function(key)
    {
        if (this.hasItem(key)) {
            previous = this.items[key];
            this.length--;
            delete this.items[key];
            return previous;
        }
        else {
            return undefined;
        }
    }

    this.keys = function()
    {
        var keys = [];
        for (var k in this.items) {
            if (this.hasItem(k)) {
                keys.push(k);
            }
        }
        return keys;
    }

    this.values = function()
    {
        var values = [];
        for (var k in this.items) {
            if (this.hasItem(k)) {
                values.push(this.items[k]);
            }
        }
        return values;
    }

    this.each = function(fn) {
        for (var k in this.items) {
            if (this.hasItem(k)) {
                fn(k, this.items[k]);
            }
        }
    }

    this.clear = function()
    {
        this.items = {}
        this.length = 0;
    }
}