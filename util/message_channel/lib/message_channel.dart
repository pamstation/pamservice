library message_channel;

import 'dart:html';
import 'dart:convert';
import 'dart:async';
//import 'dart:isolate';
import 'dart:js' as js;
import 'package:logging/logging.dart';

part 'src/shared_web_worker/web_worker.dart';
part 'src/window/window_channel.dart';
part 'src/http/server.dart';

abstract class MessageChannel<T> {
  Stream get onMessage;
  void sendMessage(T data);
  String get name;
  void close();
}

class BaseMessageChannel<T> implements MessageChannel<T> {
  StreamController _controller = new StreamController.broadcast();
  Stream get onMessage => _controller.stream;

  void sendMessage(T data) {}

  String get name => '';

  @override
  void close() {
    _controller.close();
  }
}

class Message {
  String type;
  Object value;
  Message.fromJson(Map m) {
    type = m["type"];
    value = m["value"];
  }
  Message(this.type, this.value);
  Map toJson() => {'type': type, 'value': value};
  String toString() => this.toJson().toString();
}
