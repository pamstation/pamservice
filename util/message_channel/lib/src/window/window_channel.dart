part of message_channel;

class WindowMessageChannel implements MessageChannel<Map> {
  static final log = new Logger("WindowMessageChannel");

  WindowBase _window;
  String _name;

  WindowMessageChannel(this._window, this._name) {
//    log.finest('creating channel named: $_window');
    window.addEventListener("message", _onmessage);
//    log.finest('channel named: $name created');
  }

  void _onmessage(MessageEvent msg) {
//    log.finest('_onmessage ${msg.data}');
    var m = json.decode(msg.data);
    if (m is Map && m["channelName"] == this._name) {
      _controller.add(m["data"]);
    }
  }

  StreamController _controller = new StreamController.broadcast();
  Stream get onMessage => _controller.stream;

  void sendMessage(Map data) {
//    log.finest('sendMessage $data');
    var jsonData = data;
    var message = json.encode({"channelName": this._name, "data": jsonData});
    this._window.postMessage(message, window.location.origin);
  }

  @override
  String get name => _name;

  @override
  void close() {
    window.removeEventListener("message", _onmessage);
    _controller.close();
  }
}
