part of message_channel;

abstract class HttpMessageChannelServer {
  MessageChannel _channel;
  StreamSubscription _channelSub;
  HttpMessageChannelServer(this._channel) {
    _channelSub = _channel.onMessage.listen(_onMessage);
  }

  MessageChannel get channel => _channel;

  void close() {
    _channelSub.cancel();
  }

  void _onMessage(Map msgRequest) {
    var requestId = msgRequest["requestId"];
    HttpMessageChannelRequest request =
        new HttpMessageChannelRequest.fromMessageRequest(msgRequest);
    HttpMessageChannelResponse response =
        new HttpMessageChannelResponse.fromRequestId(requestId);

    processRequest(request, response).then((response) {
      _channel.sendMessage(response.msgResponse);
    });
  }

  Future<HttpMessageChannelResponse> processRequest(
      HttpMessageChannelRequest request, HttpMessageChannelResponse response);
}

class HttpMessageChannelResponse {
  Map msgResponse;
//  HttpMessageChannelResponse.fromMap(this.msgResponse);
  HttpMessageChannelResponse.fromJson(this.msgResponse);
  HttpMessageChannelResponse.fromRequestId(id) {
    msgResponse = {"requestId": id};
  }
  Object get body => msgResponse["body"];
  set body(Object b) {
    msgResponse["body"] = b;
  }

  Map get headers => msgResponse["headers"];
  set headers(Map h) {
    msgResponse["headers"] = h;
  }

  int get statusCode => msgResponse["statusCode"];
  set statusCode(int s) {
    msgResponse["statusCode"] = s;
  }

  Map toJson() => msgResponse;
}

class HttpMessageChannelRequest {
  Map msgRequest;
  HttpMessageChannelRequest.fromJson(this.msgRequest);
  HttpMessageChannelRequest.fromMessageRequest(this.msgRequest);

  HttpMessageChannelRequest.fromRequestID(id) {
    msgRequest = {"requestId": id};
  }

  String get verb => msgRequest["verb"];
  set verb(String v) {
    msgRequest["verb"] = v;
  }

  String get url => msgRequest["url"];
  set url(String v) {
    msgRequest["url"] = v;
  }

  Map get headers => msgRequest["headers"];
  set headers(Map v) {
    msgRequest["headers"] = v;
  }

  Object get body => msgRequest["body"];
  set body(Object b) {
    msgRequest["body"] = b;
  }

  Map toJson() => msgRequest;
}
