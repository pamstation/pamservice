var peers = [];
 
onconnect=function(e){
 
  var port = e.ports[0];
  peers[peers.length] = port;
  
  port.onmessage = function(event){
    for ( index = 0; index < peers.length ; index++){
          var p = peers[index];
          if (p != port) p.postMessage(event.data);
    }
  };
  
}; 

 