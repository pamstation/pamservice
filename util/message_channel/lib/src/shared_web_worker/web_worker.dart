part of message_channel;

class SharedWorkerMessageChannel implements MessageChannel {
  static final log = new Logger("SharedWorkerMessageChannel");

  static Map CHANNELS = {};

  static SharedWorkerMessageChannel SINGLETON;

  String name;
  js.JsObject worker;

  factory SharedWorkerMessageChannel(String name) {
    if (CHANNELS.containsKey(name)) return CHANNELS[name];
    var c = new SharedWorkerMessageChannel._(name);
    CHANNELS[name] = c;
    return c;
  }

  SharedWorkerMessageChannel._(this.name) {
    var uri = Uri.base.replace(
        path:
            '/packages/message_channel/src/shared_web_worker/shared_webworker_channel.js');
//    uri =
//        'http://localhost:8081/packages/message_channel/src/shared_web_worker/shared_webworker_channel.js';
    log.finest('creating channel named: $name');
    worker = new js.JsObject(
        js.context["SharedWorker"], [uri.toString(), this.name]);
    worker["port"]["onmessage"] = new js.JsFunction.withThis(_onmessage);
    log.finest('channel named: $name created');
  }

  void _onmessage(p, MessageEvent msg) {
    log.finest('_onmessage ${msg.data}');
    _controller.add(msg.data);
  }

  StreamController _controller = new StreamController.broadcast();
  Stream get onMessage => _controller.stream;

  void sendMessage(data) {
    log.finest('sendMessage $data');
    var jdata;
    if (data is Map || data is Iterable) {
      jdata = new js.JsObject.jsify(data);
    } else if (data is String) {
      jdata = data;
    } else {
      jdata = new js.JsObject.jsify(data.toJson());
//      throw "$this unknown data type";
    }
    worker["port"].callMethod("postMessage", [jdata]);
  }

  @override
  void close() {
    _controller.close();
    worker = null;
  }
}
