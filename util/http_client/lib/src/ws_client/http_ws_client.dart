import 'dart:async';
import 'package:web_socket_channel/html.dart';
import 'package:async/async.dart';

import '../channel/multiplex_channel.dart';
import '../../web_socket_client.dart';

class HttpWebSocketClient implements WebSocketClient {
  ChannelClient _client;
  final String uri;

  HttpWebSocketClient(this.uri);

  void createClient() {
    if (_client != null) return;
    _client = new ChannelClient(
        new HtmlWebSocketChannel.connect(uri, binaryType: BinaryType.list));
    _client.stream.drain().whenComplete(() {

      _client = null;
    });
  }

  Stream<Map> send(String endPoint, Map data) {
    return this.sendStream(endPoint, new Stream.fromIterable([data]));
  }

  Stream<Map> sendStream(String endPoint, Stream<Map> stream) {
    if (_client == null) {
      return StreamCompleter.fromFuture(new Future(() {
        createClient();
        return basicSendStream(endPoint, stream);
      }));
    } else {
      return basicSendStream(endPoint, stream);
    }
  }

  Stream<Map> basicSendStream(String endPoint, Stream<Map> stream) =>
      _client.send(new ChannelClientRequest(endPoint, stream)).stream;

  Future close() => _client.close();
}
