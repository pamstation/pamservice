part of http_webworker;

class HttpMessageChannelClient implements api.HttpClient {
  final logger = new Logger("HttpMessageChannelClient");
  int _requestID = 0;
  mc.MessageChannel<Map> _channel;
  Map<String, Completer<api.Response>> _completers = {};

  HttpMessageChannelClient(this._channel) {
    _channel.onMessage.listen(_onMessage);
  }

  Uri resolveUri(Uri uri, String path) => api.HttpClient.ResolveUri(uri, path);

  WebSocketChannel webSocketChannel(uri) => throw 'not impl';

  void _onMessage(msgResponse) {
    var requestId = msgResponse["requestId"];
    _completers[requestId]
        .complete(new _HttpMessageChannelResponse.fromJson(msgResponse));
  }

  String newRequestID() {
    _requestID++;
    return _requestID.toString();
  }

  void _registerCompleter(Completer<api.Response> completer, String requestID) {
    _completers[requestID] = completer;
  }

  Future<api.Response> _verb(String verb, url,
      {Map<String, String> headers,
      body,
      Encoding encoding,
      String responseType}) {
    var compl = new Completer<api.Response>();
    var id = newRequestID();
    _registerCompleter(compl, id);
    var request = new mc.HttpMessageChannelRequest.fromRequestID(id);
    request.verb = verb;
    request.url = url as String;
    request.headers = headers;
    request.body = body;

    _channel.sendMessage(request.toJson());
    return compl.future;
  }

  void close({bool force}) {}
  Future<api.Response> delete(url, {Map<String, String> headers}) =>
      _verb("delete", url, headers: headers);
  Future<api.Response> get(url,
          {Map<String, String> headers, String responseType}) =>
      _verb("get", url, headers: headers, responseType: responseType);
  Future<api.Response> head(url, {Map<String, String> headers}) =>
      _verb("head", url, headers: headers);
  Future<api.Response> post(url,
          {Map<String, String> headers,
          body,
          String responseType,
          Encoding encoding}) =>
      _verb("post", url,
          headers: headers,
          body: body,
          responseType: responseType,
          encoding: encoding);
  Future<api.Response> put(url,
          {Map<String, String> headers,
          body,
          String responseType,
          Encoding encoding}) =>
      _verb("put", url,
          headers: headers,
          body: body,
          responseType: responseType,
          encoding: encoding);

  @override
  Future<api.StreamResponse> getStream(url,
      {Map<String, String> headers,
      body,
      String responseType,
      Encoding encoding: utf8}) {
    throw new UnimplementedError();
  }
}

class _HttpMessageChannelResponse extends mc.HttpMessageChannelResponse
    implements api.Response {
  _HttpMessageChannelResponse.fromJson(msgResponse)
      : super.fromJson(msgResponse as Map);
}
