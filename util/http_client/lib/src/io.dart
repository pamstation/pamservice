part of http_client_io;

class BaseClientImpl extends http.BaseClient {
  http.Client _client;

  http.Client get client {
    if (_client == null) _client = new http.Client();
    return _client;
  }

  bool _followRedirects = false;
  BaseClientImpl({http.Client client, bool followRedirects: false}) {
    _client = client;
    _followRedirects = followRedirects;
  }

  @override
  Future<http.StreamedResponse> send(http.BaseRequest request) {
//    logger.finest("send ${request.method} ${request.url}");
    request.followRedirects = _followRedirects;
    request.maxRedirects = 5;
    return client.send(request);
  }

//X509Certificate
  set badCertificateCallback(bool callback(cert, String host, int port)) {

//    InstanceMirror myClassInstanceMirror = reflect(client);
//    var _inner =
//        MirrorSystem.getSymbol('_inner', myClassInstanceMirror.type.owner);
//    var ioclientmirror = myClassInstanceMirror.getField(_inner);
//    ioclientmirror.setField(#badCertificateCallback, callback);
  }

  void close() {
    if (_client != null) {
      _client.close();
      _client = null;
    }
  }
}

class LoadBalancerBaseClientImpl extends BaseClientImpl {
  final logger = new Logger("LoadBalancerBaseClientImpl");
  List<Uri> _uris;
  List<Uri> _failedUri = [];
  int _uriIndex;
  Timer _timer;

  LoadBalancerBaseClientImpl(List<Uri> list,
      {http.Client client, bool followRedirects: false})
      : super(client: client, followRedirects: followRedirects) {
    uris = list;
    _timer = new Timer.periodic(new Duration(seconds: 5), _onTimer);
  }

  set uris(List<Uri> list) {
    _uris = list;
    _uriIndex = 0;
    _failedUri = [];
  }

  int get _maxRetry => _uris.length + _failedUri.length;

  void close() {
    super.close();
    if (_timer != null) _timer.cancel();
    _timer = null;
  }

  _onTimer(Timer t) {
    _uris.addAll(_failedUri);
    _failedUri = [];
  }

  @override
  Future<http.StreamedResponse> send(http.BaseRequest request) {
    return _send(request, 0);
  }

  Future<http.StreamedResponse> _send(http.BaseRequest request, int retry) {
    var currentUri;
    return new Future.sync(() {
      var uri = getLoadBalancedRequest(request);
      var i = _uriIndex - 1;
      currentUri = _uris[i];
      return super.send(uri);
    }).catchError((e) {
      if (e is iolib.SocketException) {
        var failedUri = currentUri;
        _failedUri.add(failedUri as Uri);
        _uris.remove(failedUri);
        if (retry < _maxRetry) {
          return _send(request, retry++);
        } else
          throw e;
      }
    });
  }

  http.BaseRequest getLoadBalancedRequest(http.BaseRequest request) {
    if (_uris == null || _uris.isEmpty) return request;
    http.Request req =
        new http.Request(request.method, getLoadBalancedUri(request.url));

    req.persistentConnection = request.persistentConnection;
    req.followRedirects = request.followRedirects;

    req.maxRedirects = request.maxRedirects;
    request.headers.forEach((k, v) {
      req.headers[k] = v;
    });

    if (request is http.Request) {
      req.encoding = request.encoding;
      req.bodyBytes = request.bodyBytes;
    }

    return req;
  }

  Uri getLoadBalancedUri(Uri requestUri) {
//    logger.finest("getLoadBalancedUri requestUri $requestUri");
    if (_uris == null || _uris.isEmpty) return requestUri;
    if (_uriIndex >= _uris.length) _uriIndex = 0;
//    logger.finest("getLoadBalancedUri _uris $_uris _uriIndex $_uriIndex");
    var answer = _uris[_uriIndex];
    answer = requestUri.replace(
        scheme: answer.scheme, host: answer.host, port: answer.port);
    _uriIndex++;
//    logger.finest("getLoadBalancedUri answer $answer");
    return answer;
  }
}

class HttpIOClient implements api.HttpClient {
  final log = new Logger("HttpIOClient");

  static api.HttpClient setAsCurrent() {
    return api.HttpClient.setCurrent(new HttpIOClient());
  }

  WebSocketChannel webSocketChannel(uri) => new IOWebSocketChannel.connect(uri);

  BaseClientImpl client; //= new BaseClientImpl(); //new http.Client();

  factory HttpIOClient(
      {http.Client client,
      List<Uri> loadBalancedUris,
      bool followRedirects: true}) {
    if (loadBalancedUris != null) {
      return new HttpIOClient._(new LoadBalancerBaseClientImpl(loadBalancedUris,
          client: client, followRedirects: followRedirects));
    } else {
      return new HttpIOClient._(
          new BaseClientImpl(client: client, followRedirects: followRedirects));
    }
  }

  HttpIOClient._(this.client);

  Uri resolveUri(Uri uri, String path) => api.HttpClient.ResolveUri(uri, path);

  set badCertificateCallback(bool callback(cert, String host, int port)) {
    client.badCertificateCallback = callback;
  }

  Future<api.Response> head(url, {Map<String, String> headers}) {
//    log.finest("head $url $headers");

    return client
        .head(url, headers: headers)
        .then((rep) => new IOResponse(rep));
  }

  Future<api.Response> get(url,
      {Map<String, String> headers,
      body,
      String responseType,
      Encoding encoding: utf8}) {
//    log.finest("get $url $headers");
    if (body != null) {
      var uri = url is Uri ? url : Uri.parse(url as String);
      var request = new http.Request("GET", uri);
      if (body != null) request.body = body as String;
      if (headers != null) {
        request.headers.addAll(headers);
      }
      if (encoding != null) {
        request.encoding = encoding;
      }
      return client
          .send(request)
          .then(http.Response.fromStream)
          .then((response) {
        return new IOResponse(response, responseType: responseType);
      });
    } else {
      return client
          .get(url, headers: headers)
          .then((rep) => new IOResponse(rep, responseType: responseType));
    }
  }

  Future<api.Response> post(url,
      {Map<String, String> headers,
      body,
      String responseType,
      Encoding encoding: utf8}) {
//    log.finest("post $url $headers");
    return client
        .post(url, headers: headers, body: body, encoding: encoding)
        .then((rep) => new IOResponse(rep, responseType: responseType));
  }

  Future<api.Response> put(url,
      {Map<String, String> headers,
      body,
      String responseType,
      Encoding encoding: utf8}) {
//    log.finest("put $url $headers");
    return client
        .put(url, headers: headers, body: body, encoding: encoding)
        .then((rep) => new IOResponse(rep, responseType: responseType));
  }

  Future<api.Response> delete(url, {Map<String, String> headers}) {
//    log.finest("delete $url $headers");
    return client
        .delete(url, headers: headers)
        .then((rep) => new IOResponse(rep));
  }

  void close({bool force}) {
//    log.finest("close");

    if (force != null && force) client.close();
  }

  Future<api.Response> search(url,
      {Map<String, String> headers,
      body,
      String responseType,
      Encoding encoding}) {
//    log.finest("search $url $headers $body");
    var uri = url is Uri ? url : Uri.parse(url as String);
    var request = new http.Request("SEARCH", uri);
    if (body != null) request.body = body as String;
    if (headers != null) {
      request.headers.addAll(headers);
    }
    if (encoding != null) {
      request.encoding = encoding;
    }
    return client.send(request).then(http.Response.fromStream).then((response) {
      return new IOResponse(response, responseType: responseType);
    });
  }

  @override
  Future<api.StreamResponse> getStream(url,
      {Map<String, String> headers,
      body,
      String responseType,
      Encoding encoding: utf8}) {
    throw new UnimplementedError();
  }
}

class IOResponse extends api.Response {
  http.Response response;
  String responseType;

  IOResponse(this.response, {String responseType}) {
    this.responseType = responseType;
  }

  int get statusCode => response.statusCode;
  Map get headers => response.headers;
  Object get body {
    if (responseType == "arraybuffer") return response.bodyBytes;
    return response.body;
  }
}
