part of multiplex_channel;

abstract class RequestHandler {
  request(ChannelServerRequest request);
  void close() {}
}

class ErrorRequestHandler extends RequestHandler {
  final String error;

  ErrorRequestHandler(this.error);

  request(ChannelServerRequest request) async {
    return new ChannelServerResponse(
        new Stream.fromFuture(new Future.error(error)));
  }
}

class ServerRequest {
  Stream<Map> stream;
}

class ServerResponse {
  Stream<Map> stream;
}

class ChannelServer {
  static Logger logger = new Logger('ChannelServer');
  StreamChannel _channel;

  Map<String, RequestHandler> _requestHandlers;

  Stream<_Message> _stream;
  StreamSubscription<_Message> _streamSub;
  StreamGroup<_Message> _streamGroup;

  StreamController<_Message> _commandController;

  ChannelServer(this._channel) {
    _requestHandlers = {};

    _streamGroup = new StreamGroup();

    _commandController = new StreamController(sync: true);
    _streamGroup.add(_commandController.stream);

    _stream = _channel.stream
        .map((each) => new _Message.fromJson(TSON.decode(each) as Map))
        .asBroadcastStream();

    this._channel.sink.addStream(
        _streamGroup.stream.map((each) => TSON.encode(each.toJson())));

    _streamSub = _stream.listen(_onRequest, onError: _onError, onDone: _onDone);
  }

  void _onRequest(_Message msg) {
//    print('$this _onRequest msg ${msg}');
    if (!_requestHandlers.containsKey(msg.channelId)) {
      if (msg.type == ENDPOINT) {
        requestHandlerForEndPoint(
            msg.data['endPoint'] as String, msg.channelId);
      }
    }
  }

  requestHandlerForEndPoint(String endPoint, String channelId) {
    RequestHandler handler = _requestHandlers[endPoint];
    if (handler == null) {
      handler = new ErrorRequestHandler('Endpoint : $endPoint not found');
    }
    var request = new ChannelServerRequest(
        new _OutputStream(channelId, _stream, _commandController).stream);

    scicancel.runCancelZoned(() {
      Stream outputStream;

      var res = handler.request(request);

      if (res is Future) {
        outputStream =
            StreamCompleter.fromFuture(res.then((res) => res.stream as Stream));
      } else if (res is ChannelServerResponse) {
        outputStream = res.stream;
      } else {
        throw 'error bad response';
      }

      if (logger.isLoggable(Level.FINE)) {
        var watch = new Stopwatch()..start();
        var start = new DateTime.now();
        var count = 0;
        outputStream = outputStream.asBroadcastStream();
        var listen = (each) {};

        if (logger.isLoggable(Level.FINEST)) {
          listen = (each) {
            logger.finest(
                'EVENT -- endPoint = ${endPoint} -- channelId = ${channelId} '
                '-- count = ${count} '
                '-- ms ${watch.elapsedMilliseconds}');
            count++;
            watch.reset();
          };
        }

        outputStream.listen(listen, onDone: () {
          logger.fine(
              'DONE -- endPoint = ${endPoint} -- channelId = ${channelId} '
              '-- ms ${new DateTime.now().difference(start).inMilliseconds}');
          watch.reset();
        }, onError: (e, st) {
          logger.severe(
              'ERROR -- endPoint = ${endPoint} -- channelId = ${channelId} '
              '-- ms ${new DateTime.now().difference(start).inMilliseconds} '
              '-- error = ${e} ');
          if (e is ServiceError && e.isUserAbortError) {
          } else {
            logger.severe(Trace.format(st as StackTrace));
            watch.stop();
          }
        });
      }

      _streamGroup.add(new _InputStreamConsumer(
              channelId, outputStream.cast<Map>()  , _stream)
          .stream);
    });
  }

  void registerEndPoint(String endPoint, RequestHandler handler) {
    _requestHandlers[endPoint] = handler;
  }

  void _onError(Object e, StackTrace st) {
    print('$this _onError $e');
    print(st);
    close();
  }

  void _onDone() {
    close();
  }

  void close() {
    if (_commandController != null) _commandController.close();
    if (_streamGroup != null) _streamGroup.close();
    if (_streamSub != null) _streamSub.cancel();
    if (_requestHandlers != null)
      _requestHandlers.values.forEach((each) => each.close());
    _commandController = null;
    _streamGroup = null;
    _streamSub = null;
    _stream = null;
    _channel = null;
    _requestHandlers = null;
  }
}

class ChannelServerRequest {
  final Stream<Map> stream;

  ChannelServerRequest(this.stream);
}

class ChannelServerResponse {
  final Stream<dynamic> stream;
  ChannelServerResponse(this.stream);
}
