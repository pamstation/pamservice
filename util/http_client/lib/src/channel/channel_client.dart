part of multiplex_channel;

class ChannelClient {
  static Logger logger = new Logger('ChannelClient');

  StreamChannel _channel;

  Stream<_Message> _stream;

  StreamGroup<_Message> _streamGroup;

  StreamController<_Message> _commandController;

  ChannelClient(this._channel) {
    _streamGroup = new StreamGroup();
    _commandController = new StreamController();
    _streamGroup.add(_commandController.stream);
    _stream = _channel.stream.map((each) {
      var watch = new Stopwatch()..start();
//      logger.finest('_Message -- ${each.runtimeType}');
      var msg = new _Message.fromJson(TSON.decode(each) as Map);
      logger.finest('_Message 2 -- ${watch.elapsedMilliseconds} ms');
      return msg;
    }).asBroadcastStream();

    _channel.sink
        .addStream(_streamGroup.stream.map((msg) => TSON.encode(msg.toJson())));
  }

  Stream get stream => _stream;

  Future close() async {
    logger.finest('close');
    if (_channel == null) return;
    _channel.sink.close();
    _channel = null;
    _stream = null;
    _commandController.close();
    _commandController = null;
  }

  ChannelClientResponse send(ChannelClientRequest request) {
    var channelId = new Uuid().v4().toString();
    var input = new _InputStreamConsumer(channelId, request.stream, _stream);
    _commandController
        .add(new _Message(channelId, ENDPOINT, {'endPoint': request.endPoint}));

    _streamGroup.add(input.stream);

    return new ChannelClientResponse(
        new _OutputStream(channelId, _stream, _commandController, input.done)
            .stream);
  }
}

class ChannelClientRequest {
  final String endPoint;
  final Stream<Map> stream;

  ChannelClientRequest(this.endPoint, this.stream);
}

class ChannelClientResponse {
  final Stream<Map> stream;
  ChannelClientResponse(this.stream);
}
