import 'dart:async';

import 'dart:convert';
import 'dart:io';
//import 'dart:collection' as col;
import 'package:tson/tson.dart' as TSON;
//import 'package:test/test.dart';

main() async {
  var file = new File('test/largeTson.bin');

  var list = new List<dynamic>.generate(10000, (i) => {'hello': i});


  var stream = new Stream.fromIterable(list)
      .transform(new TSON.TsonStreamEncoderTransformer())
      .transform(base64.encoder)
      .transform(ascii.encoder);

  var sink = file.openWrite();
  await sink.addStream(stream);
  sink.close();

  var result = await file
      .openRead()
      .transform(ascii.decoder)
      .transform(base64.decoder)
      .transform(new TSON.TsonStreamDecoderTransformer())
      .toList();

  if (json.encode(list) != json.encode(result)) throw 'not equals';
}
