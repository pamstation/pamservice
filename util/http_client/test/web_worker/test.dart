import 'dart:html' as html;
import 'dart:js' as js;

void main() {
  var worker = new html.Worker("test_worker.dart.js");
  worker.onMessage.listen(onMessage);
  worker.postMessage(new js.JsArray.from(["dart", "hello"]));
}

void onMessage(html.MessageEvent event) {
  print(event.data);
}
