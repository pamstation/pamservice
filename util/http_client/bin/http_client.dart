import 'package:http_client/http_browser_client.dart';

void main() {
  print("Hello, World!");

  HttpBrowserClient client = new HttpBrowserClient();

  client.get("http://mw.zmantic.com/index.html").then((res) {
    print(res.statusCode);
    print(res.headers);
    print(res.body);
  });
}
