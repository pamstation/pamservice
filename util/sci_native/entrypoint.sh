#!/usr/bin/env bash

mkdir -p /sci_clib/bin/Release/jessie

g++ -c -m64 -I/usr/lib/dart/bin -I/usr/lib/dart/include /sci_clib/sci_c.cc -fPIC -Wall -Ofast
gcc -m64 -o/sci_clib/bin/Release/jessie/libsci_c.so sci_c.o -shared