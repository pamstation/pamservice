#ifndef MACROS_H_
#define MACROS_H_

#ifdef _WIN32
#include <stdint.h>
#endif

#define SART_LIST_LIST_LENGTH \
\
      Dart_TypedData_Type type; \
      void* data1; \
      void* data2; \
      intptr_t len1; \
      intptr_t len2; \
\
      Dart_EnterScope(); \
\
      Dart_Handle keys_handle = HandleError(Dart_GetNativeArgument(arguments, 0)); \
      Dart_Handle order_handle = HandleError(Dart_GetNativeArgument(arguments, 1)); \
\
      if (HandleError2(Dart_TypedDataAcquireData(keys_handle, &type, &data1, &len1))){ \
        if (HandleError2(Dart_TypedDataAcquireData(order_handle, &type, &data2, &len2))){


#define END_LIST_LIST_LENGTH \
            HandleError(Dart_TypedDataReleaseData(order_handle)); \
        } else { \
        } \
        HandleError(Dart_TypedDataReleaseData(keys_handle)); \
    } else { \
    } \
\
    Dart_ExitScope();


#define SART_LIST_LENGTH \
\
      Dart_TypedData_Type type; \
      void* data1; \
      intptr_t len1; \
\
      Dart_EnterScope(); \
\
      Dart_Handle keys_handle = HandleError(Dart_GetNativeArgument(arguments, 0)); \
\
      if (HandleError2(Dart_TypedDataAcquireData(keys_handle, &type, &data1, &len1))){


#define END_LIST_LENGTH \
        HandleError(Dart_TypedDataReleaseData(keys_handle)); \
    } else { \
    } \
\
    Dart_ExitScope();




#define SART_LIST_LIST_LIST_LENGTH \
\
      Dart_TypedData_Type type; \
      void* data1; \
      void* data2; \
      void* data3; \
      intptr_t len1; \
      intptr_t len2; \
      intptr_t len3; \
\
      Dart_EnterScope(); \
\
      Dart_Handle data1_handle = HandleError(Dart_GetNativeArgument(arguments, 0)); \
      Dart_Handle data2_handle = HandleError(Dart_GetNativeArgument(arguments, 1)); \
      Dart_Handle data3_handle = HandleError(Dart_GetNativeArgument(arguments, 3)); \
\
      if (HandleError2(Dart_TypedDataAcquireData(data1_handle, &type, &data1, &len1))){ \
        if (HandleError2(Dart_TypedDataAcquireData(data2_handle, &type, &data2, &len2))){


#define END_LIST_LIST__LIST_LENGTH \
            HandleError(Dart_TypedDataReleaseData(order_handle)); \
        } else { \
        } \
        HandleError(Dart_TypedDataReleaseData(keys_handle)); \
    } else { \
    } \
\
    Dart_ExitScope();


#define ENTERSCOPE \
    Dart_EnterScope(); \
    Dart_Handle acquiredTypeData[255] = {NULL};

#define EXITSCOPE \
    RELEASE_ALL_TYPE_DATA \
    Dart_ExitScope();

#define ACQUIRE_TYPE_DATA_ARG(INDEX, EXPECTEDTYPE) \
    Dart_TypedData_Type type ## INDEX; \
    void* data ## INDEX; \
    intptr_t lenArg ## INDEX; \
    Dart_Handle arg_handle ## INDEX = HandleError(Dart_GetNativeArgument(arguments, INDEX)); \
    HandleError(Dart_TypedDataAcquireData(arg_handle ## INDEX, &type ## INDEX, &data ## INDEX, &lenArg ## INDEX)); \
    acquiredTypeData[INDEX] = arg_handle ## INDEX; \
    if (EXPECTEDTYPE != type ## INDEX){ \
         THROW("Argument index must be a " #EXPECTEDTYPE) \
    }


#define RELEASE_TYPE_DATA_ARG(INDEX) \
    if (arg_handle ## INDEX) { \
        if (acquiredTypeData[INDEX]) { \
            acquiredTypeData[INDEX] = NULL; \
            HandleError(Dart_TypedDataReleaseData(arg_handle ## INDEX)); \
            arg_handle ## INDEX = NULL; \
        } \
    }


#define UINT64_ARG(INDEX) \
    bool argFits ## INDEX; \
    uint64_t arg ## INDEX; \
    Dart_Handle arg_handle ## INDEX = HandleError(Dart_GetNativeArgument(arguments, INDEX)); \
    if (! Dart_IsInteger(arg_handle ## INDEX)) { \
        Dart_PropagateError(Dart_NewApiError("Argument " #INDEX " must be an int")); \
    } \
    HandleError(Dart_IntegerFitsIntoUint64(arg_handle ## INDEX, &argFits ## INDEX)); \
    if (!argFits ## INDEX) { \
        THROW("Argument " #INDEX " must fit into an uint64") \
    } \
    HandleError(Dart_IntegerToUint64(arg_handle ## INDEX, &arg ## INDEX));

#define UINT32_ARG(INDEX) \
    bool argFits ## INDEX; \
    uint64_t argTmp ## INDEX; \
    uint32_t arg ## INDEX; \
    Dart_Handle arg_handle ## INDEX = HandleError(Dart_GetNativeArgument(arguments, INDEX)); \
    if (! Dart_IsInteger(arg_handle ## INDEX)) { \
        THROW("Argument " #INDEX " must be an int") \
    } \
    HandleError(Dart_IntegerFitsIntoUint64(arg_handle ## INDEX, &argFits ## INDEX)); \
    if (!argFits ## INDEX) { \
        THROW("Argument " #INDEX " must fit into an uint64") \
     } \
    HandleError(Dart_IntegerToUint64(arg_handle ## INDEX, &argTmp ## INDEX)); \
    if (argTmp ## INDEX > UINT32_MAX) { \
        THROW("Argument " #INDEX " must fit into an uint32") \
    } \
    arg ## INDEX = argTmp ## INDEX;


#define SET_RETURN_UINT64_VALUE(VALUE) \
    RELEASE_ALL_TYPE_DATA \
    Dart_SetReturnValue(arguments, HandleError(Dart_NewIntegerFromUint64(VALUE)));

#define RELEASE_ALL_TYPE_DATA \
    for (int i = 0 ; i < 255 ; i++){ \
         if (acquiredTypeData[i]) { \
            HandleError(Dart_TypedDataReleaseData(acquiredTypeData[i])); \
            acquiredTypeData[i] = NULL; \
         } \
    }

#define THROW(MSG) \
    RELEASE_ALL_TYPE_DATA \
    Dart_ThrowException(Dart_NewStringFromCString("Error : " MSG));

#endif