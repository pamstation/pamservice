
//#include "stdafx.h"


#include <string.h>
#include <stdlib.h>
#include <stdio.h>

#ifdef _WIN32
#include "windows.h"
#else
#include <stdbool.h>
#include <dlfcn.h>
#include <unistd.h>
#include <sys/mman.h>
#endif

#include "macro.h"
#include "dart_api.h"

Dart_NativeFunction ResolveName(Dart_Handle name, int argc, bool* auto_setup_scope);

DART_EXPORT Dart_Handle sci_c_Init(Dart_Handle parent_library) {
  if (Dart_IsError(parent_library)) { return parent_library; }

  Dart_Handle result_code = Dart_SetNativeResolver(parent_library, ResolveName, NULL);
  if (Dart_IsError(result_code)) return result_code;

  return Dart_Null();
}

Dart_Handle HandleError(Dart_Handle handle) {
  if (Dart_IsError(handle)) Dart_PropagateError(handle);
  return handle;
}

int HandleError2(Dart_Handle handle) {
  if (Dart_IsError(handle)){
    Dart_PropagateError(handle);
    return 0;
  }
  return 1;
}

void GetPageSize(Dart_NativeArguments arguments) {
  Dart_Handle result;
#if _WIN32
  SYSTEM_INFO si;
#endif

  Dart_EnterScope();
#if _WIN32
  GetSystemInfo(&si);
  result = Dart_NewInteger(si.dwPageSize);
#else
  result = Dart_NewInteger(sysconf(_SC_PAGESIZE));
#endif
  Dart_SetReturnValue(arguments, result);
  Dart_ExitScope();
}

int32_t max_start_int32(int32_t* starts, size_t nelm){
    if (nelm < 1) return 0;
    int32_t max = 0;
    int32_t start = starts[0];
    for (size_t i = 1; i < nelm; ++i) {
      int32_t end = starts[i];
      int32_t len = end - start;
      start = end;
      if (len > max) max = len;
    }
    return max;
}

// This function is an utility used to sort uint8string using radix sort.
// Read bytes using starts, and extract the nth_uint32 from each string, store the result into keys.
// If nth_uint32 = 0, keys will contain the first 4 bytes (ie: uint32) of each string
// If nth_uint32 = 1, keys will contain the bytes from 4 to 7 (ie: uint32) of each string
// Etc ..
// nelm : number of string stored in bytes = length of keys = length of starts -1
void slice_uint8string_uint32(uint8_t* bytes, uint32_t* starts, uint32_t nth_uint32, uint32_t* keys, size_t nelm){

  if (nelm == 0) return;

  uint32_t uint32Index = nth_uint32;
  uint32_t start32 = (uint32Index) * 4;

  uint32_t start = starts[0];

  for (size_t i = 0; i < nelm; i++) {

    uint32_t end = starts[i + 1];

    uint32_t start2 = start32 + start > end ? end : start32 + start;
    uint32_t end2 = start32 + start + 4 > end ? end : start32 + start + 4;

    uint32_t len = end2 - start2;

    if (len > 0) {

      uint32_t key = 0;
      uint32_t shift = 3;

      for (uint32_t k = start2; k < end2; k++) {
        key = key | (bytes[k] << (8 * shift));
        shift--;
      }

      keys[i] = key;

    } else {
      keys[i] = 0;
    }
    start = end;
  }
}

void fill_sequence_uint32(uint32_t* list, size_t nelm){
  for (size_t i = 0; i < nelm; ++i) {
    list[i]=i;
  }
}

void fill_sequence_int32(int32_t* list, size_t nelm){
  for (size_t i = 0; i < nelm; ++i) {
    list[i]=i;
  }
}


void set_range_uint32(uint32_t* list, size_t start, size_t end, uint32_t* from){
  for (size_t i = start; i < end; ++i) {
      list[i] = from[i];
   }
}

void set_range_int32(int32_t* list, size_t start, size_t end, int32_t* from){
  for (size_t i = start; i < end; ++i) {
      list[i] = from[i];
   }
}

void reorder_uint32(uint32_t* keys, int32_t* order, size_t nelm){
    uint32_t * tmp = new uint32_t[nelm]();
    for (size_t i = 0; i < nelm; ++ i)
                tmp[i] = keys[order[i]];


    for (size_t i = 0; i < nelm; ++ i)
                    keys[i] = tmp[i];

    delete[] tmp;
}

void reorder_int32(int32_t* keys, int32_t* order, size_t nelm){
    int32_t * tmp = new int32_t[nelm]();
    for (size_t i = 0; i < nelm; ++ i)
                tmp[i] = keys[order[i]];


    for (size_t i = 0; i < nelm; ++ i)
                    keys[i] = tmp[i];

    delete[] tmp;
}


void reverse_reorder(int32_t* order, size_t nelm){
    int32_t * tmp = new int32_t[nelm]();
    for (size_t i = 0; i < nelm; ++ i)
        tmp[order[i]] = i;

    for (size_t i = 0; i < nelm; ++ i)
        order[i] = tmp[i];

    delete[] tmp;
}

void unreorder_uint32(uint32_t* keys, int32_t* order, size_t nelm){
    uint32_t * tmp = new uint32_t[nelm]();
    for (size_t i = 0; i < nelm; ++ i)
                tmp[order[i]] = keys[i];

    for (size_t i = 0; i < nelm; ++ i)
                    keys[i] = tmp[i];

    delete[] tmp;
}

void unreorder_int32(int32_t* keys, int32_t* order, size_t nelm){
    int32_t * tmp = new int32_t[nelm]();
    for (size_t i = 0; i < nelm; ++ i)
                tmp[order[i]] = keys[i];

    for (size_t i = 0; i < nelm; ++ i)
                    keys[i] = tmp[i];

    delete[] tmp;
}

void reorder_float32(float* keys, uint32_t* order, size_t nelm){
    float * tmp = new float[nelm]();
    for (size_t i = 0; i < nelm; ++ i)
                tmp[i] = keys[order[i]];

    for (size_t i = 0; i < nelm; ++ i)
       keys[i] = tmp[i];

    delete[] tmp;
}

void unreorder_float32(float* keys, uint32_t* order, size_t nelm){
    float * tmp = new float[nelm]();
    for (size_t i = 0; i < nelm; ++ i)
                tmp[order[i]] = keys[i];

    for (size_t i = 0; i < nelm; ++ i)
                    keys[i] = tmp[i];

    delete[] tmp;
}

void reorder_float64(double* keys, int32_t* order, size_t nelm){
    double * tmp = new double[nelm]();
    for (size_t i = 0; i < nelm; ++ i)
                tmp[i] = keys[order[i]];

    for (size_t i = 0; i < nelm; ++ i)
       keys[i] = tmp[i];

    delete[] tmp;
}

void unreorder_float64(double* keys, int32_t* order, size_t nelm){
    double * tmp = new double[nelm]();
    for (size_t i = 0; i < nelm; ++ i)
                tmp[order[i]] = keys[i];

    for (size_t i = 0; i < nelm; ++ i)
                    keys[i] = tmp[i];

    delete[] tmp;
}



void reorder_uint8string(uint8_t * bytes, uint32_t* starts, int32_t* order, size_t nelm){

    if (nelm < 1) return;

    size_t nBytes = starts[nelm];
    size_t nStarts = nelm+1;

    uint8_t * tmpBytes = new uint8_t[nBytes]();
    uint32_t * tmpStarts = new uint32_t[nStarts]();

    size_t currentByteIndex = 0;

    for (size_t i = 0; i < nelm; ++ i) {
        const size_t j = order[i];
        size_t start = starts[j];
        size_t end = starts[j+1];

        tmpStarts[i] = currentByteIndex;

        for (size_t k = start; k < end; ++ k) {
            tmpBytes[currentByteIndex] = bytes[k];
            currentByteIndex++;
        }
        tmpStarts[i+1] = currentByteIndex;
    }

    for (size_t i = 0; i < nBytes; ++ i)
        bytes[i] = tmpBytes[i];

    for (size_t i = 0; i < nStarts; ++ i)
        starts[i] = tmpStarts[i];

    delete[] tmpBytes;
    delete[] tmpStarts;
}

size_t count_sequence_int32(int32_t* sortedValues, size_t nelm){

    if (nelm < 1) return 0;

    int32_t value = sortedValues[0];
    size_t count = 1;

    for (size_t i = 1; i < nelm; ++ i) {
        if (sortedValues[i] != value){
            count++;
            value = sortedValues[i];
        }
    }

    return count;
}

size_t count_sequence_double(double * sortedValues, size_t nelm){

    if (nelm < 1) return 0;

    float value = sortedValues[0];
    size_t count = 1;

    for (size_t i = 1; i < nelm; ++ i) {
        if (sortedValues[i] != value){
            count++;
            value = sortedValues[i];
        }
    }

    return count;
}



inline size_t _count_sequence_uint8string(uint8_t * sortedValues, int32_t* starts, size_t from, size_t to){

    if (to < 1) return 0;

    size_t count = 1;

    int32_t start1 = starts[from];
    int32_t start2 = starts[from + 1];

    for (size_t i = from + 1; i < to; ++ i) {

        const int32_t end1 = start2;
        const int32_t end2 = starts[i+1];

        const int32_t len1 = end1 - start1;
        const int32_t len2 = end2 - start2;

        if (len1 == len2){

            for (int32_t k = 0; k < len1 ; k++){
                const uint8_t byte1 =  sortedValues[k+start1];
                const uint8_t byte2 =  sortedValues[k+start2];
                if (byte1 != byte2){
                    count++;
                    break;
                }
            }

        } else {
            count++;
        }

        start1 = end1;
        start2 = end2;
    }

    return count;
}

size_t count_sequence_uint8string(uint8_t * sortedValues, int32_t* starts, int32_t* superStarts, size_t superStarts_len){
    size_t count = 0;

    if (superStarts_len < 2) return count;

    size_t slen = superStarts_len - 1;
    for (size_t i = 0; i < slen; i++) {
        count += _count_sequence_uint8string(sortedValues, starts, superStarts[i], superStarts[i + 1]);
    }
    return count;
}

inline bool uint8string_equalsAt(uint8_t * sortedValues, int32_t* starts, size_t i, size_t j){

    if (i == j) return true;

    int32_t starti = starts[i];
    int32_t endi = starts[i + 1];

    int32_t startj = starts[j];
    int32_t endj = starts[j + 1];

    int32_t len = endi - starti;

    if (len != (endj - startj)) return false;

    for (int32_t ii = 0; ii < len; ii++) {
      if (sortedValues[ii + starti] != sortedValues[ii + startj]) return false;
    }

    return true;
}

void starts_uint8string_with_starts(uint8_t * sortedValues, int32_t* starts,
                                    int32_t* string_starts,
                                    int32_t* super_string_starts,
                                    size_t super_starts_len){

    if (super_starts_len < 1) return;

    string_starts[0] = 0;
    size_t currentStartIndex = 1;

    size_t len = super_starts_len -1;

    for (size_t k = 0; k < len; k++) {
      int32_t from = super_string_starts[k];
      int32_t to = super_string_starts[k + 1];
      int32_t current = from;
      size_t count = 0;
      for (int32_t i = from; i < to; i++) {
        if (uint8string_equalsAt(sortedValues,starts, i, current)) {
          count++;
        } else {
          string_starts[currentStartIndex] = string_starts[currentStartIndex - 1] + count;
          currentStartIndex++;
          current = i;
          count = 1;
        }
      }
      string_starts[currentStartIndex] = string_starts[currentStartIndex - 1] + count;
      currentStartIndex++;
    }
}


int radix_order_double(double * fkeys, int32_t * in_order, size_t nelm){
     if (nelm <= 1) {
         return 0;
       }

      int32_t * order = in_order;
      const uint64_t * keys = reinterpret_cast<const uint64_t*>(fkeys);

      uint32_t offset1[2048] = { 0 };
      uint32_t offset2[2048] = { 0 };
      uint32_t offset3[2048] = { 0 };
      uint32_t offset4[2048] = { 0 };
      uint32_t offset5[1024] = { 0 };
      uint32_t offset6[1024] = { 0 };

      bool sorted = true;
      // count the number of values in each bucket
      ++offset1[keys[0] & 2047];
      ++offset2[(keys[0] >> 11) & 2047];
      ++offset3[(keys[0] >> 22) & 2047];
      ++offset4[(keys[0] >> 33) & 2047];
      ++offset5[(keys[0] >> 44) & 1023];

      uint32_t k = (keys[0] >> 48);
          // convert -NaN to +NaN
      if (k == 65528){
        k = 32760;
      }
      k = k >> 6;
      ++offset6[k];

      for (uint32_t j = 1; j < nelm; ++j) {
        ++offset1[keys[j] & 2047];
        ++offset2[(keys[j] >> 11) & 2047];
        ++offset3[(keys[j] >> 22) & 2047];
        ++offset4[(keys[j] >> 33) & 2047];
        ++offset5[(keys[j] >> 44) & 1023];
        uint32_t k = (keys[j] >> 48);
              // convert -NaN to +NaN
          if (k == 65528){
            k = 32760;
          }
          k = k >> 6;
          ++offset6[k];

        sorted = sorted && (keys[j] >= keys[j - 1]);
      }
      if (sorted) {
        fill_sequence_int32(order, nelm);
        return 0;
      }

      // determine the starting positions for each bucket
      uint32_t max1 = offset1[0];
      uint32_t max2 = offset2[0];
      uint32_t max3 = offset3[0];
      uint32_t max4 = offset4[0];
      uint32_t max5 = offset5[0];
      uint32_t max6 = offset6[1023];
      uint32_t prev1 = offset1[0];
      uint32_t prev2 = offset2[0];
      uint32_t prev3 = offset3[0];
      uint32_t prev4 = offset4[0];
      uint32_t prev5 = offset5[0];
      uint32_t prev6 = offset6[1023];
      offset1[0] = 0;
      offset2[0] = 0;
      offset3[0] = 0;
      offset4[0] = 0;
      offset5[0] = 0;
    //  offset6[0] = 0;
      for (uint32_t j = 1; j < 1024; ++j) {
        const uint32_t cnt1 = offset1[j];
        const uint32_t cnt2 = offset2[j];
        const uint32_t cnt3 = offset3[j];
        const uint32_t cnt4 = offset4[j];
        const uint32_t cnt5 = offset5[j];

        offset1[j] = prev1;
        offset2[j] = prev2;
        offset3[j] = prev3;
        offset4[j] = prev4;
        offset5[j] = prev5;

        prev1 += cnt1;
        prev2 += cnt2;
        prev3 += cnt3;
        prev4 += cnt4;
        prev5 += cnt5;

        if (max1 < cnt1) max1 = cnt1;
        if (max2 < cnt2) max2 = cnt2;
        if (max3 < cnt3) max3 = cnt3;
        if (max4 < cnt4) max4 = cnt4;
        if (max5 < cnt5) max5 = cnt5;
      }
      for (uint32_t j = 1024; j < 2048; ++j) {
        const uint32_t cnt1 = offset1[j];
        const uint32_t cnt2 = offset2[j];
        const uint32_t cnt3 = offset3[j];
        const uint32_t cnt4 = offset4[j];
        offset1[j] = prev1;
        offset2[j] = prev2;
        offset3[j] = prev3;
        offset4[j] = prev4;
        prev1 += cnt1;
        prev2 += cnt2;
        prev3 += cnt3;
        prev4 += cnt4;
        if (max1 < cnt1) max1 = cnt1;
        if (max2 < cnt2) max2 = cnt2;
        if (max3 < cnt3) max3 = cnt3;
        if (max4 < cnt4) max4 = cnt4;
      }

      for (uint32_t j = 1022; j > 511; --j) {
        const uint32_t cnt6 = offset6[j];
        prev6 += cnt6;
        offset6[j] = prev6;
        if (max6 < cnt6) max6 = cnt6;
      }

      for (uint32_t j = 0; j < 512; ++j) {
        const uint32_t cnt6 = offset6[j];
        offset6[j] = prev6;
        prev6 += cnt6;
        if (max6 < cnt6) max6 = cnt6;
      }

      if (max1 == nelm &&
          max2 == nelm &&
          max3 == nelm &&
          max4 == nelm &&
          max5 == nelm &&
          max6 == nelm) {
        fill_sequence_int32(order, nelm);
        return 0;
      }

      int32_t * tmpOrder = new int32_t[nelm]();

      // distribution 1
      if (max1 < nelm) {
        for (uint32_t j = 0; j < nelm; ++j) {
          const uint32_t k = keys[j] & 2047;
          const uint32_t pos = offset1[k];
          offset1[k]++;
          tmpOrder[pos] = j;
        }

        int32_t * tmp = tmpOrder;
        tmpOrder = order;
        order = tmp;
      } else {
        fill_sequence_int32(order, nelm);
      }

    //  print(keys);
    //  print(order);

      // distribution 2
      if (max2 < nelm) {
        for (uint32_t j = 0; j < nelm; ++j) {
          const uint32_t i = order[j];
          const uint32_t k = (keys[i] >> 11) & 2047;
          const uint32_t pos = offset2[k];
          offset2[k]++;
          tmpOrder[pos] = i;
        }

        int32_t * tmp = tmpOrder;
        tmpOrder = order;
        order = tmp;
      }

    //  print(order);

      // distribution 3
      if (max3 < nelm) {
        for (uint32_t j = 0; j < nelm; ++j) {
          const uint32_t i = order[j];
          const uint32_t k = (keys[i] >> 22) & 2047;
          const uint32_t pos = offset3[k];
          offset3[k]++;
          tmpOrder[pos] = i;
        }

        int32_t * tmp = tmpOrder;
        tmpOrder = order;
        order = tmp;
      }

      //////////////////////////////////////////////////////////////////////////

    //  print(order);

      // distribution 4
      if (max4 < nelm) {
        for (uint32_t j = 0; j < nelm; ++j) {
          const uint32_t i = order[j];
          const uint32_t k = (keys[i] >> 33) & 2047;
          const uint32_t pos = offset4[k];
          offset4[k]++;
          tmpOrder[pos] = i;
        }

        int32_t * tmp = tmpOrder;
        tmpOrder = order;
        order = tmp;
      }

    //  print(keys);
    //  print(order);

      // distribution 5
      if (max5 < nelm) {
        for (uint32_t j = 0; j < nelm; ++j) {
          const uint32_t i = order[j];
          const uint32_t k = (keys[i] >> 44) & 1023;
          const uint32_t pos = offset5[k];
          offset5[k]++;
          tmpOrder[pos] = i;
        }

        int32_t * tmp = tmpOrder;
        tmpOrder = order;
        order = tmp;
      }

    //  print(order);

      // distribution 6
      if (max6 < nelm) {
        for (uint32_t j = 0; j < nelm; ++j) {
          const uint32_t i = order[j];
          //const uint32_t k = (keys[i] >> 54);

          uint32_t k = (keys[i] >> 48);
            // convert -NaN to +NaN
          if (k == 65528){
            k = 32760;
          }
          k = k >> 6;

          if (k < 512) {
            // positive value
            const uint32_t pos = offset6[k];
            tmpOrder[pos] = i;
            offset6[k]++;
          } else {
            offset6[k]--;
            const uint32_t pos = offset6[k];
            tmpOrder[pos] = i;
          }
        }

        int32_t * tmp = tmpOrder;
        tmpOrder = order;
        order = tmp;
      }

    //  print(order);

    if (in_order == order){
        if (tmpOrder) delete[] tmpOrder;
    } else {
        for (uint32_t j = 0; j < nelm; ++j) {
            in_order[j] = order[j];
        }
        if (order) delete[] order;

    }



      return 0;
}

void radix_order_float(float * fkeys, uint32_t * order, size_t nelm){

  if (nelm <= 1) {
    return;
  }

   const uint32_t * keys = reinterpret_cast<const uint32_t*>(fkeys);

  uint32_t offset1[2048] = { 0 };
  uint32_t offset2[2048] = { 0 };
  uint32_t offset3[1024] = { 0 };

  bool sorted = true;
  // count the number of values in each bucket
  ++offset1[keys[0] & 2047];
  ++offset2[(keys[0] >> 11) & 2047];
  ++offset3[(keys[0] >> 22)];

  for (uint32_t j = 1; j < nelm; ++j) {
    ++offset1[keys[j] & 2047];
    ++offset2[(keys[j] >> 11) & 2047];
    ++offset3[(keys[j] >> 22)];
    sorted = sorted && (keys[j] >= keys[j - 1]);
  }
  if (sorted) {
    fill_sequence_uint32(order, nelm);
    return;
  }

  // determine the starting positions for each bucket
  uint32_t max1 = offset1[0];
  uint32_t max2 = offset2[0];
  uint32_t max3 = offset3[1023];
  uint32_t prev1 = offset1[0];
  uint32_t prev2 = offset2[0];
  uint32_t prev3 = offset3[1023];
    offset1[0] = 0;
    offset2[0] = 0;
    for (uint32_t j = 1; j < 2048; ++j) {
      const uint32_t cnt1 = offset1[j];
      const uint32_t cnt2 = offset2[j];
      offset1[j] = prev1;
      offset2[j] = prev2;
      prev1 += cnt1;
      prev2 += cnt2;
      if (cnt1 > max1) max1 = cnt1;
      if (cnt2 > max2) max2 = cnt2;
    }
    for (uint32_t j = 1022; j > 511; --j) {
      const uint32_t cnt3 = offset3[j];
      prev3 += cnt3;
      offset3[j] = prev3;
      if (cnt3 > max3) max3 = cnt3;
    }
    for (uint32_t j = 0; j < 512; ++j) {
      const uint32_t cnt3 = offset3[j];
      offset3[j] = prev3;
      prev3 += cnt3;
      if (cnt3 > max3) max3 = cnt3;
    }
    if (max1 == nelm && max2 == nelm && max3 == nelm) {
      fill_sequence_uint32(order, nelm);
      return;
    }

    // distribution 1
    if (max1 < nelm) {
      for (uint32_t j = 0; j < nelm; ++j) {
        const uint32_t k = keys[j] & 2047;
        const uint32_t pos = offset1[k];
        offset1[k]++;
        order[pos] = j;
      }
    } else {
      fill_sequence_uint32(order, nelm);
    }

    uint32_t* tmpOrder = NULL;

    // distribution 2
    if (max2 < nelm) {
      if (!tmpOrder) tmpOrder = new uint32_t[nelm]();

      for (uint32_t j = 0; j < nelm; ++j) {
        const uint32_t i = order[j];
        const uint32_t k = (keys[i] >> 11) & 2047;
        const uint32_t pos = offset2[k];
        offset2[k]++;
        tmpOrder[pos] = i;
      }
    } else if (max3 < nelm) {
      if (!tmpOrder) tmpOrder = new uint32_t[nelm]();
      set_range_uint32(tmpOrder, 0, nelm, order);
    }

    // distribution 3
    if (max3 < nelm) {
      for (uint32_t j = 0; j < nelm; ++j) {
        const uint32_t i = tmpOrder[j];
        const uint32_t k = (keys[i] >> 22);

        if (k < 512) {
          // positive value
          const uint32_t pos = offset3[k];
          order[pos] = i;
          offset3[k]++;
        } else {
          offset3[k]--;
          const uint32_t pos = offset3[k];
          order[pos] = i;
        }
      }
    } else {
      if (max2 < nelm) {
          set_range_uint32(order, 0, nelm, tmpOrder);
      }
    }

  if (tmpOrder) delete[] tmpOrder;
}

void radix_order_uint32(uint32_t* keys, int32_t* order, size_t nelm){

  if (nelm <= 1) {
    return;
  }

  uint32_t offset1[2048] = { 0 };
  uint32_t offset2[2048] = { 0 };
  uint32_t offset3[1024] = { 0 };

  bool sorted = true;
  // count the number of values in each bucket
  ++offset1[keys[0] & 2047];
  ++offset2[(keys[0] >> 11) & 2047];
  ++offset3[(keys[0] >> 22)];

  for (size_t j = 1; j < nelm; ++j) {
    ++offset1[keys[j] & 2047];
    ++offset2[(keys[j] >> 11) & 2047];
    ++offset3[(keys[j] >> 22)];
    sorted = sorted && (keys[j] >= keys[j - 1]);
  }
  if (sorted) {
    fill_sequence_int32(order, nelm);
    return;
  }

  // determine the starting positions for each bucket
  uint32_t max1 = offset1[0];
  uint32_t max2 = offset2[0];
  uint32_t max3 = offset3[0];
  uint32_t prev1 = offset1[0];
  uint32_t prev2 = offset2[0];
  uint32_t prev3 = offset3[0];
  offset1[0] = 0;
  offset2[0] = 0;
  offset3[0] = 0;

  for (uint32_t j = 1; j < 1024; ++j) {
    const uint32_t cnt1 = offset1[j];
    const uint32_t cnt2 = offset2[j];
    const uint32_t cnt3 = offset3[j];
    offset1[j] = prev1;

    offset2[j] = prev2;
    offset3[j] = prev3;
    prev1 += cnt1;

    prev2 += cnt2;
    prev3 += cnt3;
    if (max1 < cnt1) max1 = cnt1;
    if (max2 < cnt2) max2 = cnt2;
    if (max3 < cnt3) max3 = cnt3;

  }


  for (uint32_t j = 1024; j < 2048; ++j) {
    const uint32_t cnt1 = offset1[j];
    const uint32_t cnt2 = offset2[j];
    offset1[j] = prev1;
    offset2[j] = prev2;
    prev1 += cnt1;
    prev2 += cnt2;
    if (max1 < cnt1) max1 = cnt1;
    if (max2 < cnt2) max2 = cnt2;
  }


  if (max1 == nelm && max2 == nelm && max3 == nelm) {
    fill_sequence_int32(order, nelm);
    return;
  }



  // distribution 1
  if (max1 < nelm) {
     for (size_t j = 0; j < nelm; ++j) {
       const uint32_t ii = keys[j]&2047;
       const uint32_t pos = offset1[ii];
       order[pos] = j;
       offset1[ii]++;
     }
  } else {
     fill_sequence_int32(order, nelm);
  }

  int32_t * tmpOrder = NULL;

  // distribution 2
  if (max2 < nelm) {
    if (!tmpOrder) tmpOrder = new int32_t[nelm]();

    for (size_t j = 0; j < nelm; ++j) {
      const uint32_t i = order[j];
      const uint32_t ii = (keys[i]>>11)&2047;
      const uint32_t pos = offset2[ii];
      tmpOrder[pos] = i;
      offset2[ii]++;
    }
  } else if (max3 < nelm) {
    if (!tmpOrder) tmpOrder = new int32_t[nelm]();
    set_range_int32(tmpOrder, 0, nelm, order);
  }

   // distribution 3
  if (max3 < nelm) {
    for (size_t j = 0; j < nelm; ++j) {
        const uint32_t i = tmpOrder[j];
         const uint32_t ii = (keys[i]>>22);
        const uint32_t pos = offset3[ii];
        order[pos] = i;
        offset3[ii]++;
    }
  } else {
     if (max2 < nelm) {
        set_range_int32(order, 0, nelm, tmpOrder);
    }
  }

  if (tmpOrder)  delete[] tmpOrder;
}

void radix_order_int32(int32_t* keys, int32_t* order, size_t nelm){

  if (nelm <= 1) {
    return;
  }

  uint32_t offset1[2048] = { 0 };
  uint32_t offset2[2048] = { 0 };
  uint32_t offset3[1024] = { 0 };

  bool sorted = true;
  // count the number of values in each bucket
  ++offset1[keys[0] & 2047];
  ++offset2[(keys[0] >> 11) & 2047];
  ++offset3[(keys[0] >> 22) + 512];

  for (size_t j = 1; j < nelm; ++j) {
    ++offset1[keys[j] & 2047];
    ++offset2[(keys[j] >> 11) & 2047];
    ++offset3[(keys[j] >> 22)+512];
    sorted = sorted && (keys[j] >= keys[j - 1]);
  }
  if (sorted) {
    fill_sequence_int32(order, nelm);
    return;
  }

  // determine the starting positions for each bucket
  uint32_t max1 = offset1[0];
  uint32_t max2 = offset2[0];
  uint32_t max3 = offset3[0];
  uint32_t prev1 = offset1[0];
  uint32_t prev2 = offset2[0];
  uint32_t prev3 = offset3[0];
  offset1[0] = 0;
  offset2[0] = 0;
  offset3[0] = 0;

  for (uint32_t j = 1; j < 1024; ++j) {
    const uint32_t cnt1 = offset1[j];
    const uint32_t cnt2 = offset2[j];
    const uint32_t cnt3 = offset3[j];
    offset1[j] = prev1;
    offset2[j] = prev2;
    offset3[j] = prev3;
    prev1 += cnt1;
    prev2 += cnt2;
    prev3 += cnt3;
    if (max1 < cnt1) max1 = cnt1;
    if (max2 < cnt2) max2 = cnt2;
    if (max3 < cnt3) max3 = cnt3;
  }


  for (uint32_t j = 1024; j < 2048; ++j) {
    const uint32_t cnt1 = offset1[j];
    const uint32_t cnt2 = offset2[j];
    offset1[j] = prev1;
    offset2[j] = prev2;
    prev1 += cnt1;
    prev2 += cnt2;
    if (max1 < cnt1) max1 = cnt1;
    if (max2 < cnt2) max2 = cnt2;
  }


  if (max1 == nelm && max2 == nelm && max3 == nelm) {
    fill_sequence_int32(order, nelm);
    return;
  }

  // distribution 1
  if (max1 < nelm) {
     for (size_t j = 0; j < nelm; ++j) {
       const uint32_t ii = keys[j]&2047;
       const uint32_t pos = offset1[ii];
       order[pos] = j;
       offset1[ii]++;
     }
  } else {
     fill_sequence_int32(order, nelm);
  }

  int32_t * tmpOrder = NULL;

  // distribution 2
  if (max2 < nelm) {
    if (!tmpOrder) tmpOrder = new int32_t[nelm]();

    for (size_t j = 0; j < nelm; ++j) {
      const uint32_t i = order[j];
      const uint32_t ii = (keys[i]>>11)&2047;
      const uint32_t pos = offset2[ii];
      tmpOrder[pos] = i;
      offset2[ii]++;
    }
  } else if (max3 < nelm) {
    if (!tmpOrder) tmpOrder = new int32_t[nelm]();
    set_range_int32(tmpOrder, 0, nelm, order);
  }

   // distribution 3
  if (max3 < nelm) {
    for (size_t j = 0; j < nelm; ++j) {
        const uint32_t i = tmpOrder[j];
         const uint32_t ii = (keys[i]>>22) + 512;
        const uint32_t pos = offset3[ii];
        order[pos] = i;
        offset3[ii]++;
    }
  } else {
     if (max2 < nelm) {
        set_range_int32(order, 0, nelm, tmpOrder);
    }
  }

  if (tmpOrder)  delete[] tmpOrder;
}


uint32_t max_uint32(uint32_t * list, size_t nelm){
    uint32_t max = 0;
    for (size_t i = 0 ; i < nelm ; i++) {
        if (max < list[i]) max = list[i];
    }
    return max;
}

uint32_t max_int32(int32_t * list, size_t nelm){
    int32_t max = 0;
    for (size_t i = 0 ; i < nelm ; i++) {
        if (max < list[i]) max = list[i];
    }
    return max;
}

void Set_range_uint32(Dart_NativeArguments arguments) {
    ENTERSCOPE

    ACQUIRE_TYPE_DATA_ARG(0, Dart_TypedData_kUint8)
    UINT64_ARG(1)
    UINT64_ARG(2)
    ACQUIRE_TYPE_DATA_ARG(3, Dart_TypedData_kUint32)

    uint32_t* list = reinterpret_cast<uint32_t*>(data0);
    uint32_t * from = reinterpret_cast<uint32_t*>(data3);
    size_t start = arg1;
    size_t end = arg2;

    set_range_uint32(list, start, end, from);

    EXITSCOPE
}

void RadixOrderUint32(Dart_NativeArguments arguments) {
    ENTERSCOPE


    ACQUIRE_TYPE_DATA_ARG(0, Dart_TypedData_kUint32)
    ACQUIRE_TYPE_DATA_ARG(1, Dart_TypedData_kInt32)

    uint32_t * list = reinterpret_cast<uint32_t*>(data0);
    int32_t * order = reinterpret_cast<int32_t*>(data1);

    size_t list_len = lenArg0;
    size_t order_len = lenArg1;
//    size_t len = list_len < order_len ? list_len : order_len;
    if (list_len < order_len){
        THROW("order length is greater the list length")
    }


    radix_order_uint32(list, order, order_len);

    EXITSCOPE
}

void RadixOrderInt32(Dart_NativeArguments arguments) {
    ENTERSCOPE


    ACQUIRE_TYPE_DATA_ARG(0, Dart_TypedData_kInt32)
    ACQUIRE_TYPE_DATA_ARG(1, Dart_TypedData_kInt32)

    int32_t * list = reinterpret_cast<int32_t*>(data0);
    int32_t * order = reinterpret_cast<int32_t*>(data1);

    size_t list_len = lenArg0;
    size_t order_len = lenArg1;
//    size_t len = list_len < order_len ? list_len : order_len;
    if (list_len < order_len){
        THROW("order length is greater the list length")
    }


    radix_order_int32(list, order, order_len);

    EXITSCOPE
}

void RadixOrderDouble(Dart_NativeArguments arguments) {
    ENTERSCOPE

    ACQUIRE_TYPE_DATA_ARG(0, Dart_TypedData_kFloat64)
    ACQUIRE_TYPE_DATA_ARG(1, Dart_TypedData_kInt32)

    double * list = reinterpret_cast<double*>(data0);
    int32_t * order = reinterpret_cast<int32_t*>(data1);

    size_t list_len = lenArg0;
    size_t order_len = lenArg1;

    if (order_len > 0 &&  list_len < order_len){
        THROW("order length is greater the list length")
    }

    radix_order_double(list, order, order_len);

    EXITSCOPE
}

void Reorder_uint32(Dart_NativeArguments arguments) {
    ENTERSCOPE

    ACQUIRE_TYPE_DATA_ARG(0, Dart_TypedData_kUint32)
    ACQUIRE_TYPE_DATA_ARG(1, Dart_TypedData_kInt32)

    uint32_t * list = reinterpret_cast<uint32_t*>(data0);
    int32_t * order = reinterpret_cast<int32_t*>(data1);

    size_t list_len = lenArg0;
    size_t order_len = lenArg1;
    if (list_len < order_len){
        THROW("order length is greater the list length")
    }


//    uint32_t max = max_int32(order, order_len);
//    if (order_len > 0 && list_len <= max){
//        THROW("order contains values greater then the length of list")
//    }

    reorder_uint32(list, order, order_len);

    EXITSCOPE
}

void Reorder_int32(Dart_NativeArguments arguments) {
    ENTERSCOPE

    ACQUIRE_TYPE_DATA_ARG(0, Dart_TypedData_kInt32)
    ACQUIRE_TYPE_DATA_ARG(1, Dart_TypedData_kInt32)

    int32_t * list = reinterpret_cast<int32_t*>(data0);
    int32_t * order = reinterpret_cast<int32_t*>(data1);

    size_t list_len = lenArg0;
    size_t order_len = lenArg1;
    if (list_len < order_len){
        THROW("order length is greater the list length")
    }


//    uint32_t max = max_int32(order, order_len);
//    if (order_len > 0 && list_len <= max){
//        THROW("order contains values greater then the length of list")
//    }

    reorder_int32(list, order, order_len);

    EXITSCOPE
}

void Unreorder_uint32(Dart_NativeArguments arguments) {
    ENTERSCOPE

    ACQUIRE_TYPE_DATA_ARG(0, Dart_TypedData_kUint32)
    ACQUIRE_TYPE_DATA_ARG(1, Dart_TypedData_kInt32)

    uint32_t * list = reinterpret_cast<uint32_t*>(data0);
    int32_t * order = reinterpret_cast<int32_t*>(data1);

    size_t list_len = lenArg0;
    size_t order_len = lenArg1;
    if (list_len < order_len){
        THROW("order length is greater the list length")
    }

//    uint32_t max = max_int32(order, order_len);
//    if (order_len > 0 &&  list_len <= max){
//        THROW("order contains values greater then the length of list")
//    }

    unreorder_uint32(list, order, order_len);

    EXITSCOPE
}

void Unreorder_int32(Dart_NativeArguments arguments) {
    ENTERSCOPE

    ACQUIRE_TYPE_DATA_ARG(0, Dart_TypedData_kInt32)
    ACQUIRE_TYPE_DATA_ARG(1, Dart_TypedData_kInt32)

    int32_t * list = reinterpret_cast<int32_t*>(data0);
    int32_t * order = reinterpret_cast<int32_t*>(data1);

    size_t list_len = lenArg0;
    size_t order_len = lenArg1;
    if (list_len < order_len){
        THROW("order length is greater the list length")
    }

//    uint32_t max = max_int32(order, order_len);
//    if (order_len > 0 &&  list_len <= max){
//        THROW("order contains values greater then the length of list")
//    }

    unreorder_int32(list, order, order_len);

    EXITSCOPE
}

void Reorder_float64(Dart_NativeArguments arguments) {
    ENTERSCOPE

    ACQUIRE_TYPE_DATA_ARG(0, Dart_TypedData_kFloat64)
    ACQUIRE_TYPE_DATA_ARG(1, Dart_TypedData_kInt32)

    double * list = reinterpret_cast<double*>(data0);
    int32_t * order = reinterpret_cast<int32_t*>(data1);

    size_t list_len = lenArg0;
    size_t order_len = lenArg1;
    if (list_len < order_len){
        THROW("order length is greater the list length")
    }

//    uint32_t max = max_int32(order, order_len);
//    if (order_len > 0 &&  list_len <= max){
//        THROW("order contains values greater then the length of list")
//    }

    reorder_float64(list, order, order_len);

    EXITSCOPE
}

void Unreorder_float64(Dart_NativeArguments arguments) {

    ENTERSCOPE

    ACQUIRE_TYPE_DATA_ARG(0, Dart_TypedData_kFloat64)
    ACQUIRE_TYPE_DATA_ARG(1, Dart_TypedData_kInt32)

    double * list = reinterpret_cast<double*>(data0);
    int32_t * order = reinterpret_cast<int32_t*>(data1);

    size_t list_len = lenArg0;
    size_t order_len = lenArg1;
    if (list_len < order_len){
        THROW("order length is greater the list length")
    }

//    uint32_t max = max_int32(order, order_len);
//    if (order_len > 0 &&  list_len <= max){
//        THROW("order contains values greater then the length of list")
//    }

    unreorder_float64(list, order, order_len);

    EXITSCOPE
}

void Reverse_reorder(Dart_NativeArguments arguments) {
    ENTERSCOPE

    ACQUIRE_TYPE_DATA_ARG(0, Dart_TypedData_kInt32)

    int32_t * list = reinterpret_cast<int32_t*>(data0);
    reverse_reorder(list, lenArg0);

    EXITSCOPE
}

void Fill_sequence_int32(Dart_NativeArguments arguments) {
    ENTERSCOPE

    ACQUIRE_TYPE_DATA_ARG(0, Dart_TypedData_kInt32)

    int32_t * list = reinterpret_cast<int32_t*>(data0);
    fill_sequence_int32(list, lenArg0);

    EXITSCOPE
}

void Max_start_int32(Dart_NativeArguments arguments) {

    ENTERSCOPE

    ACQUIRE_TYPE_DATA_ARG(0, Dart_TypedData_kInt32)

    int32_t * list = reinterpret_cast<int32_t*>(data0);
    int32_t max = max_start_int32(list, lenArg0);

    SET_RETURN_UINT64_VALUE(max)

    EXITSCOPE
}

void Count_sequence_int32(Dart_NativeArguments arguments) {
    ENTERSCOPE

    ACQUIRE_TYPE_DATA_ARG(0, Dart_TypedData_kInt32)

    int32_t * list = reinterpret_cast<int32_t*>(data0);
    size_t max = count_sequence_int32(list, lenArg0);

    SET_RETURN_UINT64_VALUE(max)

    EXITSCOPE
}

void Count_sequence_double(Dart_NativeArguments arguments) {
    ENTERSCOPE

    ACQUIRE_TYPE_DATA_ARG(0, Dart_TypedData_kFloat64)

    double * list = reinterpret_cast<double*>(data0);
    size_t max = count_sequence_double(list, lenArg0);

    SET_RETURN_UINT64_VALUE(max)

    EXITSCOPE
}

void Reorder_uint8string(Dart_NativeArguments arguments) {
    ENTERSCOPE

    ACQUIRE_TYPE_DATA_ARG(0, Dart_TypedData_kUint8)
    ACQUIRE_TYPE_DATA_ARG(1, Dart_TypedData_kInt32)
    UINT64_ARG(2)
    ACQUIRE_TYPE_DATA_ARG(3, Dart_TypedData_kInt32)

    uint8_t * bytes = reinterpret_cast<uint8_t*>(data0);
    uint32_t * starts = reinterpret_cast<uint32_t*>(data1);
    int32_t * order = reinterpret_cast<int32_t*>(data3);

    size_t bytes_len = lenArg0;
    size_t starts_len = lenArg1;
    size_t nelm = arg2;

    if (starts_len <= nelm){
        THROW("starts_len <= nelm")
    }

    if (bytes_len < starts[nelm]){
        THROW("bytes_len < starts[nelm]")
    }

    reorder_uint8string(bytes, starts, order, nelm);

    EXITSCOPE
}

void Slice_uint8string_uint32(Dart_NativeArguments arguments) {

    ENTERSCOPE

    ACQUIRE_TYPE_DATA_ARG(0, Dart_TypedData_kUint8)
    ACQUIRE_TYPE_DATA_ARG(1, Dart_TypedData_kInt32)
    UINT32_ARG(2)
    UINT32_ARG(3)
    ACQUIRE_TYPE_DATA_ARG(4, Dart_TypedData_kUint32)

    uint8_t* bytes = reinterpret_cast<uint8_t*>(data0);
    // true type is int32, we can interpret it as uint32, is it safe ?
    uint32_t * starts = reinterpret_cast<uint32_t*>(data1);
    uint32_t * keys = reinterpret_cast<uint32_t*>(data4);

    size_t bytes_len = lenArg0;
    size_t starts_len = lenArg1;
    size_t nelm = arg2;
    uint32_t nth_uint32 = arg3;
    size_t keys_len = lenArg4;

    if (starts_len <= nelm){
        THROW("starts_len <= nelm")
    }

    if (bytes_len < starts[nelm]){
        THROW("bytes_len < starts[nelm]")
    }

    if (keys_len < nelm){
        THROW("keys_len < nelm")
    }

    slice_uint8string_uint32(bytes, starts, nth_uint32, keys, nelm);

    EXITSCOPE
}

void Count_sequence_uint8string(Dart_NativeArguments arguments) {

    ENTERSCOPE

    ACQUIRE_TYPE_DATA_ARG(0, Dart_TypedData_kUint8)
    ACQUIRE_TYPE_DATA_ARG(1, Dart_TypedData_kInt32)
    ACQUIRE_TYPE_DATA_ARG(2, Dart_TypedData_kInt32)



    uint8_t * bytes = reinterpret_cast<uint8_t*>(data0);
    int32_t * starts = reinterpret_cast<int32_t*>(data1);
    int32_t * superStarts = reinterpret_cast<int32_t*>(data2);

    int32_t superStarts_len = lenArg2;

//    int32_t bytes_len = lenArg0;
//    int32_t starts_len = lenArg1;
//    int32_t from = arg2;
//    int32_t to = arg3;
//
//    if (to < from){
//        THROW("to < from")
//    }
//
//    if (starts_len <= to){
//        THROW("starts_len <= nelm")
//    }
//
//    if (bytes_len < starts[to]){
//        THROW("bytes_len < starts[nelm]")
//    }

    size_t max = count_sequence_uint8string(bytes, starts, superStarts, superStarts_len);

    SET_RETURN_UINT64_VALUE(max)

    EXITSCOPE
}


void Starts_uint8string_with_starts(Dart_NativeArguments arguments) {

    ENTERSCOPE

    ACQUIRE_TYPE_DATA_ARG(0, Dart_TypedData_kUint8)
    ACQUIRE_TYPE_DATA_ARG(1, Dart_TypedData_kInt32)
    ACQUIRE_TYPE_DATA_ARG(2, Dart_TypedData_kInt32)
    ACQUIRE_TYPE_DATA_ARG(3, Dart_TypedData_kInt32)
    UINT32_ARG(4)

    uint8_t * bytes = reinterpret_cast<uint8_t*>(data0);
    int32_t * starts = reinterpret_cast<int32_t*>(data1);

    int32_t * string_starts = reinterpret_cast<int32_t*>(data2);
    int32_t * super_string_starts = reinterpret_cast<int32_t*>(data3);

    int32_t starts_list_len = lenArg1;
    int32_t string_starts_len = lenArg2;
    int32_t super_starts_list_len = lenArg3;

    int32_t super_starts_len = arg4;

    if (starts_list_len > 0 && string_starts_len > 0){
        if (super_starts_list_len < super_starts_len){
            THROW("super_starts_list_len < super_starts_len")
        }

        starts_uint8string_with_starts(bytes, starts, string_starts, super_string_starts, super_starts_len);
    }

    EXITSCOPE
}



struct FunctionLookup {
  const char* name;
  Dart_NativeFunction function;
};

FunctionLookup function_list[] = {
  {"RadixOrderUint32", RadixOrderUint32},
  {"RadixOrderInt32", RadixOrderInt32},
  {"RadixOrderDouble", RadixOrderDouble},
  {"Reorder_uint32", Reorder_uint32},
  {"Reorder_int32", Reorder_int32},
  {"Unreorder_uint32", Unreorder_uint32},
  {"Unreorder_int32", Unreorder_int32},
  {"Reorder_float64", Reorder_float64},
  {"Unreorder_float64", Unreorder_float64},

  {"Count_sequence_uint8string", Count_sequence_uint8string},
  {"Starts_uint8string_with_starts", Starts_uint8string_with_starts},

  {"Reverse_reorder", Reverse_reorder},
  {"Fill_sequence_int32", Fill_sequence_int32},


  {"Max_start_int32", Max_start_int32},
  {"Count_sequence_int32", Count_sequence_int32},
  {"Count_sequence_double", Count_sequence_double},

  {"Reorder_uint8string", Reorder_uint8string},
  {"Set_range_uint32", Set_range_uint32},
  {"Slice_uint8string_uint32", Slice_uint8string_uint32},


  {NULL, NULL}};

Dart_NativeFunction ResolveName(Dart_Handle name, int argc, bool* auto_setup_scope) {
  if (!Dart_IsString(name)) return NULL;
  Dart_NativeFunction result = NULL;
  Dart_EnterScope();
  const char* cname;
  HandleError(Dart_StringToCString(name, &cname));

  for (int i=0; function_list[i].name != NULL; ++i) {
    if (strcmp(function_list[i].name, cname) == 0) {
      result = function_list[i].function;
      break;
    }
  }
  Dart_ExitScope();
  return result;
}
