import 'package:test/test.dart';
import 'dart:typed_data';
import 'dart:math' as math;

import 'package:sci_util/src/util/list/list.dart' as scilist;

import 'package:sci_native/sci_native.dart';

main() {
  test('reorder_uint32 : ', () {
    var list = new Uint32List.fromList([10, 20, 30]);
    var order = new Int32List.fromList([1, 2, 0]);

    SciCLib.reorder_uint32(list, order);
    expect(list, equals([20, 30, 10]));

    SciCLib.unreorder_uint32(list, order);
    expect(list, equals([10, 20, 30]));

    order = new Int32List.fromList([1, 2, 0, 3]);
//
    expect(() => SciCLib.reorder_uint32(list, order), throws);
    expect(() => SciCLib.unreorder_uint32(list, order), throws);

//    order = new Int32List.fromList([1, 2, 3]);
//    expect(() => SciCLib.reorder_uint32(list, order), throws);
//    expect(() => SciCLib.unreorder_uint32(list, order), throws);

    order = new Int32List(3);
    SciCLib.fill_sequence_int32(order);
    expect(order, equals([0, 1, 2]));

    order = new Int32List.fromList([1, 2, 0, 3]);
    SciCLib.reverse_reorder(order);
    expect(order, equals([2, 0, 1, 3]));

    order = new Int32List.fromList([0, 2, 4, 7]);
    var maxStartLength = SciCLib.max_start_int32(order);
    expect(maxStartLength, equals(3));

    order = new Int32List.fromList([0, 2, 4, 7]);
    var distinct = SciCLib.count_sequence_int32(order);
    expect(distinct, equals(4));
  });

  test('reorder_float64 : ', () {
    var list = new Float64List.fromList([10.0, 20.0, 30.0]);
    var order = new Int32List.fromList([1, 2, 0]);

    SciCLib.reorder_float64(list, order);
    expect(list, equals([20.0, 30.0, 10.0]));

    SciCLib.unreorder_float64(list, order);
    expect(list, equals([10.0, 20.0, 30.0]));

    order = new Int32List.fromList([1, 2, 0, 3]);

    expect(() => SciCLib.reorder_float64(list, order), throws);
    expect(() => SciCLib.unreorder_float64(list, order), throws);

//    order = new Int32List.fromList([1, 2, 3]);
//    expect(() => SciCLib.reorder_float64(list, order), throws);
//    expect(() => SciCLib.unreorder_float64(list, order), throws);

    list = new Float64List.fromList([10.0, 20.0, 20.0, 30.0]);
    var distinct = SciCLib.count_sequence_double(list);
    expect(distinct, equals(3));
  });

  test('radixOrderUint32 : ', () {
    var rnd = new math.Random();

    var nRows = math.pow(10, 6);
    var list = new Uint32List(nRows);
    var order = new Int32List(nRows);

    for (var i = 0; i < list.length; i++) list[i] = rnd.nextInt(nRows);

    SciCLib.radixOrderUint32(list, order);
    SciCLib.reorder_uint32(list, order);

    expect(scilist.isSorted(list), isTrue);
  });

  test('radixOrderInt32 : ', () {
    var rnd = new math.Random();

    var nRows = math.pow(10, 6);
    var list = new Int32List(nRows);
    var order = new Int32List(nRows);

    for (var i = 0; i < list.length; i++) list[i] = rnd.nextInt(nRows);

    SciCLib.radixOrderInt32(list, order);
    SciCLib.reorder_int32(list, order);

    expect(scilist.isSorted(list), isTrue);
  });

  test('radixOrderFloat64 : ', () {
    var rnd = new math.Random();

    var nRows = math.pow(10, 6);
    var list = new Float64List(nRows);
    var order = new Int32List(nRows);

    for (var i = 0; i < list.length; i++) list[i] = rnd.nextDouble();

    SciCLib.radixOrderDouble(list, order);
    SciCLib.reorder_float64(list, order);

    expect(scilist.isSorted(list), isTrue);
  });
}
