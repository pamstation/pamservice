import 'package:test/test.dart';
import 'dart:typed_data';
import 'dart:math' as math;

import 'package:sci_util/src/util/list/list.dart' as scilist;

import 'package:sci_native/sci_native.dart';

main() {
  test('count_sequence_uint8string : ', () {
    //
    int count_sequence(List<String> l, List<int> superStart) {
      var list = new scilist.Uint8StringList.fromString(l);
      return SciCLib.count_sequence_uint8string(
          list.bytesView, list.startsView, new Int32List.fromList(superStart));
    }

    expect(count_sequence([], []), equals(0));
    expect(count_sequence([], [0, 0]), equals(0));
    expect(count_sequence(['1'], [0, 1]), equals(1));
    expect(count_sequence(['1', '1'], [0, 2]), equals(1));
    expect(count_sequence(['1', '1', '2'], [0, 3]), equals(2));
    expect(count_sequence(['1', '1', '2', '2'], [0, 4]), equals(2));
    expect(count_sequence(['1', '1', '2', '2'], [2, 4]), equals(1));
    expect(count_sequence(['1', '1', '2', '2'], [1, 3]), equals(2));

    expect(count_sequence(['1', '1'], [0, 1, 2]), equals(2));
    expect(count_sequence(['1', '1', '1'], [0, 1, 3]), equals(2));
    expect(count_sequence(['1', '2', '1'], [0, 2, 3]), equals(3));
  });

  test('starts_uint8string_with_starts : ', () {
    //

    Int32List starts_with_starts(List<String> l, List<int> sStarts) {
      var list = new scilist.Uint8StringList.fromString(l);
      Int32List superStarts = new Int32List.fromList(sStarts);

      var count = SciCLib.count_sequence_uint8string(
          list.bytesView, list.startsView, superStarts);

      List starts = new Int32List(count + 1);

      SciCLib.starts_uint8string_with_starts(list.bytesView, list.startsView,
          starts, superStarts, superStarts.length);

      return starts;
    }

    expect(starts_with_starts([], []), equals([0]));
    expect(starts_with_starts(['1'], []), equals([0]));
    expect(starts_with_starts(['1'], [0]), equals([0]));
    expect(starts_with_starts(['1'], [0, 1]), equals([0, 1]));
    expect(starts_with_starts(['1', '1'], [0, 2]), equals([0, 2]));
    expect(starts_with_starts(['1', '1', '1'], [0, 2]), equals([0, 2]));
    expect(starts_with_starts(['1', '1', '1'], [0, 2, 3]), equals([0, 2, 3]));
    expect(starts_with_starts(['1', '1', '1', '2'], [0, 2, 4]),
        equals([0, 2, 3, 4]));
    expect(starts_with_starts(['1', '1', '1', '2', '2'], [0, 2, 5]),
        equals([0, 2, 3, 5]));
  });

  test('slice_uint8string_uint32 : ', () {
    var list = new scilist.Uint8StringList.fromString(['a', 'b', 'c']);

    var keys = new Uint32List(list.length);
    expect(
        () => SciCLib.slice_uint8string_uint32(
            list.bytesView, list.startsView, list.length + 1, 0, keys),
        throws);

    keys = new Uint32List(list.length - 1);
    expect(
        () => SciCLib.slice_uint8string_uint32(
            list.bytesView, list.startsView, list.length, 0, keys),
        throws);

    var slice = new Uint32List(list.length);
    var nth_uint32 = 0;
    var bd = new ByteData.view(slice.buffer);
    bd.setUint8(3, 'a'.codeUnits.first);
    bd.setUint8(7, 'b'.codeUnits.first);
    bd.setUint8(11, 'c'.codeUnits.first);

    keys = new Uint32List(list.length);
    SciCLib.slice_uint8string_uint32(
        list.bytesView, list.startsView, list.length, nth_uint32, keys);

    expect(keys, equals(slice));

    nth_uint32 = 1;
    SciCLib.slice_uint8string_uint32(
        list.bytesView, list.startsView, list.length, nth_uint32, keys);
    expect(keys, equals([0, 0, 0]));
  });
}
