library sci_native_base;

import 'dart:typed_data';

import "dart-ext:sci_c";


//import "dart-ext:/Users/admin/AppData/Local/Programs/Tercen/sci_c";
//import "dart-ext:/home/alex/dev/bitbucket/tercen/sci/sci_native/lib/src/sci_c";
//import "dart-ext:/usr/lib/sci_c";

//sudo cp libsci_c.so /usr/lib/libsci_c.so

class SciCLib {
  static void radixOrderInt32(Int32List keys, Int32List order)
      native "RadixOrderInt32";

  static void radixOrderUint32(Uint32List keys, Int32List order)
  native "RadixOrderUint32";

  static void radixOrderDouble(Float64List keys, Int32List order)
      native "RadixOrderDouble";

  static void reorder_uint32(Uint32List keys, Int32List order)
      native "Reorder_uint32";

  static void reorder_int32(Int32List keys, Int32List order)
  native "Reorder_int32";

  static void unreorder_uint32(Uint32List keys, Int32List order)
      native "Unreorder_uint32";

  static void unreorder_int32(Int32List keys, Int32List order)
  native "Unreorder_int32";

  static void reorder_float64(Float64List keys, Int32List order)
      native "Reorder_float64";

  static void unreorder_float64(Float64List keys, Int32List order)
      native "Unreorder_float64";

  static void reverse_reorder(Int32List order) native "Reverse_reorder";
  static void fill_sequence_int32(Int32List order) native "Fill_sequence_int32";

  static int max_start_int32(Int32List starts) native "Max_start_int32";
  static int count_sequence_int32(Int32List list) native "Count_sequence_int32";
  static int count_sequence_double(Float64List list)
      native "Count_sequence_double";

  static int count_sequence_uint8string(Uint8List bytes, Int32List starts,
      Int32List superStarts) native "Count_sequence_uint8string";

  static int starts_uint8string_with_starts(
      Uint8List bytes,
      Int32List starts,
      Int32List starts_string,
      Int32List super_starts_string,
      int super_starts_len) native "Starts_uint8string_with_starts";

  static void slice_uint8string_uint32(
      Uint8List bytes,
      Int32List starts,
      int len,
      int nth_uint32,
      Uint32List keys) native "Slice_uint8string_uint32";

  static void reorder_uint8string(Uint8List bytes, Int32List starts, int len,
      Int32List order) native "Reorder_uint8string";
}
