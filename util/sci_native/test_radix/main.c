#include <string.h>
#include <stdlib.h>
#include <stdio.h>
#include <stdbool.h>
#include <stdint.h>
#include <time.h>

int main()
{
    printf("Hello world!\n");

    size_t nelm = 10;
    uint32_t* keys = malloc(nelm * sizeof(uint32_t));
    uint32_t* order = malloc(nelm * sizeof(uint32_t));

    srand(time(NULL));

    for (size_t j = 0; j < nelm; ++j) {
        uint32_t r = nelm * (RAND_MAX - rand()) / RAND_MAX;
        keys[j] = r;
    }

    radix_order_uint32(keys,order,nelm);

   for (size_t j = 0; j < nelm; ++j) {
        printf("%u \n", keys[order[j]]);
    }

    return 0;
}

void radix_order_uint32(uint32_t* keys, uint32_t* order, size_t nelm){

    printf("radix_order_uint32 started nelm = %u \n" , (uint32_t)nelm);

  if (nelm <= 1) {
    return;
  }

  uint32_t offset1[2048] = { 0 };
  uint32_t offset2[2048] = { 0 };
  uint32_t offset3[1024] = { 0 };

//  for (size_t j = 0; j < 2048; ++j) {
//    printf(" j=%u offset1=%u \n", (int)j, (int)offset1[j] );
//  }


  bool sorted = true;
  // count the number of values in each bucket
  ++offset1[keys[0] & 2047];
  ++offset2[(keys[0] >> 11) & 2047];
  ++offset3[(keys[0] >> 22)];

  for (size_t j = 1; j < nelm; ++j) {
    ++offset1[keys[j] & 2047];
    ++offset2[(keys[j] >> 11) & 2047];
    ++offset3[(keys[j] >> 22)];
    sorted = sorted && (keys[j] >= keys[j - 1]);
  }
  if (sorted) {
    fill_sequence_uint32(order, nelm);
    return;
  }

//  uint32_t sum = 0;
//  for (size_t j = 0; j < 2048; ++j) {
//    sum = sum + offset1[j];
//  }
//
//
//printf("sum offset1 %u\n", sum);


    printf("determine the starting positions for each bucket\n");
  // determine the starting positions for each bucket
  uint32_t max1 = offset1[0];
  uint32_t max2 = offset2[0];
  uint32_t max3 = offset3[0];
  uint32_t prev1 = offset1[0];
  uint32_t prev2 = offset2[0];
  uint32_t prev3 = offset3[0];
  offset1[0] = 0;
  offset2[0] = 0;
  offset3[0] = 0;

//  for (uint32_t j = 0; j < 10; ++j) {
//    printf(" j=%u offset1=%u \n",  j,  offset1[j] );
//  }


  for (uint32_t j = 1; j < 1024; ++j) {

//    printf("1 j=%u offset1=%u \n",  j,  offset1[j] );

//    printf("2 j=%u prev1=%u \n",  j,  prev1 );

    const uint32_t cnt1 = offset1[j];
    const uint32_t cnt2 = offset2[j];
    const uint32_t cnt3 = offset3[j];
    offset1[j] = prev1;

//    printf("2 j=%u offset1=%u \n",  j,  offset1[j] );

    offset2[j] = prev2;
    offset3[j] = prev3;
    prev1 += cnt1;

//    printf("3 j=%u offset1=%u \n",  j,  offset1[j] );

    prev2 += cnt2;
    prev3 += cnt3;
    if (max1 < cnt1) max1 = cnt1;
    if (max2 < cnt2) max2 = cnt2;
    if (max3 < cnt3) max3 = cnt3;

//    printf("4 j=%u offset1=%u \n",  j,  offset1[j] );
  }



  printf("2\n");

  for (uint32_t j = 1024; j < 2048; ++j) {
    const uint32_t cnt1 = offset1[j];
    const uint32_t cnt2 = offset2[j];
    offset1[j] = prev1;
    offset2[j] = prev2;
    prev1 += cnt1;
    prev2 += cnt2;
    if (max1 < cnt1) max1 = cnt1;
    if (max2 < cnt2) max2 = cnt2;
  }



  printf("3\n");

  if (max1 == nelm && max2 == nelm && max3 == nelm) {
    fill_sequence_uint32(order, nelm);
    return;
  }

  printf("4\n");

  //uint32_t * tmpOrder;
  uint32_t tmpOrder[nelm];

  // distribution 1
  if (max1 < nelm) {
     printf("distribution 1 max1 < nelm\n");
    for (size_t j = 0; j < nelm; ++j) {
//      printf("distribution 1 j %u\n", (uint32_t)j);
      const uint32_t ii = keys[j]&2047;
//      printf("distribution 1 2 ii %u\n", ii);
      const uint32_t pos = offset1[ii];
//      printf("distribution 1 3 pos %u\n", pos);
      order[pos] = j;
//      printf("distribution 1 4 j %u\n", (uint32_t)j);
      offset1[ii]++;
//      printf("distribution 1 5 j %u\n", (uint32_t)j);
    }
  } else {
    printf("distribution 1 fill_sequence_uint32\n");
    fill_sequence_uint32(order, nelm);
  }

  printf("distribution 2\n");

//  print(keys);
//  print(order);

  // distribution 2
  if (max2 < nelm) {
    //if (!tmpOrder) tmpOrder = new uint32_t[nelm]();
    for (size_t j = 0; j < nelm; ++j) {

      const uint32_t i = order[j];
      const uint32_t ii = (keys[i]>>11)&2047;
      const uint32_t pos = offset2[ii];
      tmpOrder[pos] = i;
      offset2[ii]++;
    }
  } else if (max3 < nelm) {
    //if (!tmpOrder) tmpOrder = new uint32_t[nelm]();

    set_range_uint32(tmpOrder, 0, nelm, order);
  }


printf("distribution 3 tmpOrder[0] %u\n", tmpOrder[0]);
  // distribution 3
  if (max3 < nelm) {
    for (size_t j = 0; j < nelm; ++j) {
        const uint32_t i = tmpOrder[j];
         const uint32_t ii = (keys[i]>>22);
        const uint32_t pos = offset3[ii];
        order[pos] = i;
        offset3[ii]++;
    }
  } else {
//    set_range_uint32(order, 0, nelm, tmpOrder);
    if (max2 < nelm) {
        set_range_uint32(order, 0, nelm, tmpOrder);
    } else {
//        fill_sequence_uint32(order, nelm);
    }
//    if (!tmpOrder){
//        fill_sequence_uint32(order, nelm);
//    } else {
//        set_range_uint32(order, 0, nelm, tmpOrder);
//    }
  }
printf("done\n");
  //if (tmpOrder)  delete[] tmpOrder;
}


void set_range_uint32(uint32_t* list, size_t start, size_t end, uint32_t* from){
    for (size_t i = start; i < end; ++i) {
        list[i] = from[i];
    }
}

void fill_sequence_uint32(uint32_t* list, size_t nelm){
    for (size_t i = 0; i < nelm; ++i) {
        list[i]= (uint32_t)i;
    }
}
