import 'package:sci_native/sci_native.dart';

import 'dart:typed_data';
import 'dart:math' as math;
import 'package:sci_util/src/util/list/list.dart' as scilist;

main() {
  test();
}

test() {
  var rnd = new math.Random();

  var nRows = math.pow(10, 6);
  var list = new Float64List(nRows);

  var order = new Int32List(nRows);

  for (var i = 0; i < list.length; i++)
    list[i] = (rnd.nextDouble() * 1000).roundToDouble();

  var watch = new Stopwatch()..start();
  SciCLib.radixOrderDouble(list, order);
  SciCLib.reorder_float64(list, order);
  watch.stop();
  print('radixOrderDouble : size = ${list.lengthInBytes / 1000000} MB');
  print('radixOrderDouble : elapsedMicroseconds = ${watch.elapsedMicroseconds}');
  print(
      'radixOrderDouble : ${(nRows*4 ~/ (watch.elapsedMicroseconds/1000000)) / 1000000} MB/s ');

  var elapsed = watch.elapsedMicroseconds;

  watch.start();

  final starts = scilist.starts(list);
  final distinct = new Float64List(starts.length - 1);
  for (var i = 0; i < distinct.length; i++) {
    distinct[i] = list[starts[i]];
  }
  watch.stop();
  print('groupBy : ${distinct.length}');
  print(
      'groupBy : elapsedMicroseconds = ${watch.elapsedMicroseconds - elapsed}');
  print(
      'groupBy : ${(nRows*4 ~/ ((watch.elapsedMicroseconds - elapsed)/1000000)) / 1000000} MB/s ');
}
