//import 'package:sci_native/sci_native.dart';
//
//import 'dart:typed_data';
//import 'dart:math' as math;
//import 'package:sci_util/src/util/list/list.dart' as scilist;
//
////import 'package:parallel/parallel.dart' as parallel;
//
//main() {
//  test();
////  test2();
//
////  print(order);
//}
//
//void printBenchmark(String name, int lengthInBytes, int elapsedMicroseconds) {
//  print('----------------------------------------------');
//  print('$name');
//  print('----------------------------------------------');
//  print('${lengthInBytes / 1000000} MB');
//  print('elapsedMicroseconds = ${elapsedMicroseconds}');
//  print('${(lengthInBytes ~/ (elapsedMicroseconds/1000000)) / 1000000} MB/s ');
//}
//
//test() {
//  var rnd = new math.Random();
//
//  var nRows = math.pow(10, 6);
//  var list = new Uint32List(nRows);
//
//  var order = new Int32List(nRows);
//
//  for (var i = 0; i < list.length; i++)
//    list[i] = rnd.nextInt(list.length ~/ 10);
//
//  list = new Uint32List(nRows);
//  for (var i = 0; i < list.length; i++)
//    list[i] = rnd.nextInt(list.length ~/ 10);
//
//  var watch = new Stopwatch()..start();
//  SciCLib.radixOrderUint32(list, order);
//  SciCLib.reorder_uint32(list, order);
//  watch.stop();
//
//  printBenchmark(
//      'radix_order_uint32', list.lengthInBytes, watch.elapsedMicroseconds);
//
//  var elapsed = watch.elapsedMicroseconds;
//
//  watch.start();
////  var sequence = SciCLib.count_sequence_uint32(list);
////  watch.stop();
////  print('count_sequence_uint32 : $sequence');
////  print(
////      'count_distinct_uint32 : elapsedMicroseconds = ${watch.elapsedMicroseconds - elapsed}');
////  print(
////      'count_distinct_uint32 : ${(nRows*4 ~/ ((watch.elapsedMicroseconds - elapsed)/1000000)) / 1000000} MB/s ');
//
//  final starts = scilist.starts(list);
//  final distinct = new Uint32List(starts.length - 1);
//  for (var i = 0; i < distinct.length; i++) {
//    distinct[i] = list[starts[i]];
//  }
//  watch.stop();
//  print('groupBy : ${distinct.length}');
//  print(
//      'groupBy : elapsedMicroseconds = ${watch.elapsedMicroseconds - elapsed}');
//  print(
//      'groupBy : ${(nRows*4 ~/ ((watch.elapsedMicroseconds - elapsed)/1000000)) / 1000000} MB/s ');
//}
//
//class ParallelRadix {
//  Uint32List call(Map<String, Uint32List> map) {
//    Uint32List keys = map['keys'];
//    Uint32List order = map['order'];
//    SciCLib.radixOrderUint32(keys, order);
//    return order;
//  }
//}
//
//test1() {
//  var rnd = new math.Random();
//
//  var nRows = math.pow(10, 8);
//  var list = new Uint32List(nRows);
//
//  var order = new Uint32List(nRows);
//
//  for (var i = 0; i < list.length; i++)
//    list[i] = rnd.nextInt(list.length ~/ 10);
//
//  list = new Uint32List(nRows);
//  for (var i = 0; i < list.length; i++)
//    list[i] = rnd.nextInt(list.length ~/ 10);
//
//  var watch = new Stopwatch()..start();
//
//  var splitLen = 4096 * 10;
//  var splitN = (list.length / splitLen).floor() + 1;
//  var splits = scilist.prefixSum(new List.filled(splitN, splitLen));
//  splits[splits.length - 1] = list.length;
////  print(splits);
//
//  print('splits.length = ${splits.length}');
//
//  var start = splits[0];
//  for (var i = 1; i < splits.length; i++) {
//    final end = splits[i];
//    final len = end - start;
//    var list_view =
//        new Uint32List.view(list.buffer, start * list.elementSizeInBytes, len);
//    var order_view =
//        new Uint32List.view(order.buffer, start * list.elementSizeInBytes, len);
//    SciCLib.radixOrderUint32(list_view, order_view);
//    start = end;
//  }
//
////  SciCLib.radixOrderUint32(list, order);
//
//  watch.stop();
//  print(
//      'radix_order_uint32 : elapsedMicroseconds = ${watch.elapsedMicroseconds}');
//  print(
//      'radix_order_uint32 : ${(nRows*4 ~/ (watch.elapsedMicroseconds/1000000)) / 1000000} MB/s ');
//}
