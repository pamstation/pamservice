import 'dart:isolate';
//import 'dart:typed_data';
import 'dart:async';
import 'package:sci_isolate/http_isolate_server.dart';
import 'package:sci_isolate/http_isolate.dart';

void main(List<String> args, SendPort replyTo) {
  print("HttpIsolateServer started");
  new HttpIsolateServer(replyTo, (IsolateHttpRequest request) {
    print("HttpIsolateServer request ${request.url}");
//    return IsolateHttpResponse.fromResponseType(200, {},  "hello from HttpIsolateServer");
    return new Future.value(new IsolateHttpResponse.fromString(
        200, {}, "hello from HttpIsolateServer"));
  });

  new Future.sync(() {});
}
