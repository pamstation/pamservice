import 'package:sci_isolate/http_isolate_client.dart';
//import 'dart:async';
import 'dart:typed_data';
import 'package:logging/logging.dart';

main() {
  initLog();

  var client = new HttpIsolateClient(Uri.parse("server_shelf.dart"));

  client.get("hello").then((response) {
    print("${response.statusCode}");
    print("${response.headers}");
    print("${response.body}");
  });

  client.put("hello2", body: new Uint8List.fromList([1, 2])).then((response) {
    print("${response.statusCode}");
    print("${response.headers}");
    print("${response.body}");
  });
}

void initLog() {
  Logger.root.level = Level.ALL;
  Logger.root.onRecord.listen((LogRecord rec) {
    print('${rec.level.name}:${rec.loggerName}: ${rec.time}: ${rec.message}');
    if (rec.error != null) {
      print(rec.error);
    }
    if (rec.stackTrace != null) {
      print(rec.stackTrace);
    }
  });
}
