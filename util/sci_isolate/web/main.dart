import 'package:sci_isolate/isolate_client.dart';
import 'dart:async';
import 'dart:typed_data';
import 'package:logging/logging.dart';

IsolateClient client;

main() {
  initLog();

  client = new IsolateClient("server.dart");
  client.request((request, response) {
//    request.addStream(new Stream.fromIterable(["hello from request"]));
    var bytes = new Uint8List.fromList([0, 1, 2, 3, 4, 5, 6, 7, 8, 9]);
//    var bytes = [0,1,2,3,4,5,6,7,8,9];
    request.addStream(new Stream.fromIterable([
      {
        "msg": {"data": "hello"},
        "bytes": bytes
      }
    ]));
    response.stream.listen((data) {
      print("recieved $data");
      print(bytes);
    });
  });
//  var request = new IsolateClientRequest();
//  client.response(request).then((response) {
//    response.stream.listen((data) {
//      print("recieved $data");
//    });
//
//    request.addStream(new Stream.fromIterable(["hello from request"]));
//  });
}

void initLog() {
//  Logger mainLog = new Logger("Main");
  Logger.root.level = Level.ALL;
  Logger.root.onRecord.listen((LogRecord rec) {
    print('${rec.level.name}:${rec.loggerName}: ${rec.time}: ${rec.message}');
    if (rec.error != null) {
      print(rec.error);
    }
    if (rec.stackTrace != null) {
      print(rec.stackTrace);
    }
  });
}
