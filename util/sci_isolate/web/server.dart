import 'dart:isolate';
import 'dart:async';
import 'package:sci_isolate/isolate_server.dart';
import 'package:logging/logging.dart';

IsolateServer server;

void main(List<String> args, SendPort replyTo) {
  server = new IsolateServer(replyTo, requestHandler:
      (IsolateServerRequest request, IsolateServerResponse response) {
    return request.stream.toList().then((list) {
      return response.addStream(new Stream.fromIterable([list.toString()]));
    });
//    return response.addStream(new Stream.fromIterable(["hello from nimes"]));
//    return request.stream.pipe(response);
  });

  initLog();
}

void initLog() {
  Logger.root.level = Level.ALL;
  Logger.root.onRecord.listen((LogRecord rec) {
    server.sendLog(
        '${rec.level.name}:${rec.loggerName}: ${rec.time}: ${rec.message}');
    if (rec.error != null) {
      server.sendLog(rec.error.toString());
    }
    if (rec.stackTrace != null) {
      server.sendLog(rec.stackTrace.toString());
    }
  });
}
