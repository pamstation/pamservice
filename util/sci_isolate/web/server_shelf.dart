import 'dart:isolate';
import 'dart:async';
import 'dart:convert';
import 'dart:typed_data';
import 'package:logging/logging.dart';

import 'package:shelf/shelf.dart' as shelf;

import 'package:sci_isolate/shelf_isolate.dart' as scishelf;
import 'package:sci_isolate/shelf_isolate_log.dart' as scishelflog;

Logger mainLogger;

void main(List<String> args, SendPort replyTo) {
  initLog();

  mainLogger = new Logger("MAIN");

  new Future.sync(() {});
  var handler = const shelf.Pipeline()
      .addMiddleware(scishelflog.logRequests())
      .addHandler(_echoRequest);

  scishelf.serve(handler, replyTo).then((server) {
    print('Serving at http://${server}');
  });
}

Future<shelf.Response> _echoRequest(shelf.Request request) {
  return new Future.sync(() {
    request
        .read()
        .fold([], (List initial, list) => initial..addAll(list)).then((list) {
      print(list);
    }).catchError((e, st) {
      mainLogger.severe("_echoRequest", e, st);
    });

    var body = new Uint8List.fromList(
        utf8.encode('Request for ${request.url} method ${request.method} '));
    new Timer(new Duration(seconds: 2), () {
      print("_echoRequest  body.length ${body.length}");
    });
    return new shelf.Response.ok(new Stream.fromIterable([body]));
  });
}

void initLog() {
  Logger.root.level = Level.ALL;
  Logger.root.onRecord.listen((LogRecord rec) {
    print('${rec.level.name}:${rec.loggerName}: ${rec.time}: ${rec.message}');
    if (rec.error != null) {
      print(rec.error);
    }
    if (rec.stackTrace != null) {
      print(rec.stackTrace);
    }
  });
}
