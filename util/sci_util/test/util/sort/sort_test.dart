import 'package:test/test.dart';
import 'package:sci_util/src/util/list/list.dart' as scilist;
import 'dart:typed_data';
//import 'dart:io';
import 'dart:math' as math;

main() {
  setUp(() {});

  test('00 : unint32', () {
    var list = new Uint32List.fromList([2048, 2, 0, 1, 0]);
    var order = scilist.order(list);
    scilist.reorder(list, order);
    expect(scilist.isSorted(list), isTrue);
  });

  test('01 : uint32 order', () {
    var rnd = new math.Random();

    var nRows = math.pow(10, 6);
    var list = new Uint32List(nRows);

    var order;

    for (var i = 0; i < list.length; i++)
      list[i] = rnd.nextInt(list.length ~/ 10);

    new List(20).forEach((_) {
      var tmpList = new Uint32List.fromList(list);
      var order = scilist.order(tmpList);
      scilist.reorder(tmpList, order);
    });

    var watch = new Stopwatch()..start();
    order = scilist.order(list);
    scilist.reorder(list, order);
    watch.stop();
    printBenchmark(
        'uint32 order', list.lengthInBytes, watch.elapsedMicroseconds);

    expect(scilist.isSorted(list), isTrue);
  });

  test('01 : uint32 reorder/unreorder', () {
    var rnd = new math.Random();

    var nRows = math.pow(10, 6);
    var list = new Uint32List(nRows);

    var order;

    for (var i = 0; i < list.length; i++)
      list[i] = rnd.nextInt(list.length ~/ 10);

    new List(20).forEach((_) {
      var tmpList = new Uint32List.fromList(list);
      var order = scilist.order(tmpList);
      scilist.reorder(tmpList, order);
      scilist.unreorder(tmpList, order);
    });

    list = new Uint32List(nRows);
    for (var i = 0; i < list.length; i++)
      list[i] = rnd.nextInt(list.length ~/ 10);

    var originalList = new Uint32List.fromList(list);

    var watch = new Stopwatch()..start();
    order = scilist.order(list);
    scilist.reorder(list, order);
    scilist.unreorder(list, order);
    watch.stop();
    printBenchmark('uint32 reorder/unreorder', list.lengthInBytes,
        watch.elapsedMicroseconds);

    expect(list, equals(originalList));
  });

  test('qsort', () {
    var rnd = new math.Random();

    var nRows = math.pow(10, 6);
    var list = new Uint32List(nRows);

    for (var i = 0; i < list.length; i++)
      list[i] = rnd.nextInt(list.length ~/ 10);

    new List(20).forEach((_) {
      var tmpList = new Uint32List.fromList(list);
      tmpList.sort();
    });

    var watch = new Stopwatch()..start();

    watch
      ..reset()
      ..start();
    list.sort();
    watch.stop();
    printBenchmark('qsort', list.lengthInBytes, watch.elapsedMicroseconds);
  });
}

void printBenchmark(String name, int lengthInBytes, int elapsedMicroseconds) {
  print('----------------------------------------------');
  print('$name');
  print('----------------------------------------------');
  print('${lengthInBytes / 1000000} MB');
  print('elapsedMicroseconds = ${elapsedMicroseconds}');
  print('${(lengthInBytes ~/ (elapsedMicroseconds/1000000)) / 1000000} MB/s ');
}
