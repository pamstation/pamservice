import 'package:test/test.dart';
import 'package:sci_util/src/util/list/list.dart' as scilist;
import 'package:sci_util/src/util/sort/radix/radix_sort.dart' as radix;
import 'dart:typed_data';
import 'dart:math' as math;

main() {
  setUp(() {});



  test('01 : int32', () {

    var list = new Int32List.fromList([1,2,3,2,3,3]);

    var order = scilist.order(list);
    scilist.reorder(list,order);
    expect(scilist.isSorted(list), isTrue);

    print(list);

  });

}
