import 'package:test/test.dart';
import 'package:sci_util/src/util/list/list.dart' as scilist;
import 'package:sci_util/src/util/sort/radix/radix_sort.dart' as radix;
import 'dart:typed_data';
import 'dart:math' as math;

main() {
  setUp(() {});



  test('01 : uint32', () {
    var rnd = new math.Random();

    var nRows = math.pow(10, 6);
    var list = new Uint32List(nRows);

    var order;

    for (var i = 0; i < list.length; i++)
      list[i] = rnd.nextInt(list.length ~/ 10);

    new List(10).forEach((_) {
      order = radix.order_uint32(list);
      scilist.sort(list, order);
    });

    list = new Uint32List(nRows);
    for (var i = 0; i < list.length; i++)
      list[i] = rnd.nextInt(list.length ~/ 10);

    var watch = new Stopwatch()..start();
    order = radix.order_uint32(list);
    var sortedList = scilist.sort(list, order);
    watch.stop();
    print('uint32 : elapsedMicroseconds = ${watch.elapsedMicroseconds}');
    print(
        'uint32 : ${(sortedList.length*4 ~/ (watch.elapsedMicroseconds/1000000)) / 1000000} MB/s ');

    expect(scilist.isSorted(sortedList), isTrue);
  });

  test('qsort', () {
    var rnd = new math.Random();

    var nRows = math.pow(10, 3);
    var list = new Uint32List(nRows);

    var order;

    for (var i = 0; i < list.length; i++)
      list[i] = rnd.nextInt(list.length ~/ 10);

    new List(10).forEach((_) {
      order = radix.order_uint32(list);
      scilist.sort(list, order);
      new Uint32List.fromList(list).sort();
    });

    list = new Uint32List(nRows);
    for (var i = 0; i < list.length; i++)
      list[i] = rnd.nextInt(list.length ~/ 10);

    var watch = new Stopwatch()..start();
    order = radix.order_uint32(list);
    var sortedList = scilist.sort(list, order);
    watch.stop();
    print('uint32 : elapsedMicroseconds = ${watch.elapsedMicroseconds}');
    print(
        'uint32 : ${(sortedList.length*4 ~/ (watch.elapsedMicroseconds/1000000)) / 1000000} MB/s ');

    expect(scilist.isSorted(sortedList), isTrue);

    var list2 = new Uint32List.fromList(list);
    watch
      ..reset()
      ..start();
    list2.sort();
    watch.stop();

    print('qsort : elapsedMilliseconds = ${watch.elapsedMicroseconds}');
    print(
        'qsort : ${(list.length*4 ~/ (watch.elapsedMicroseconds/1000000)) / 1000000} MB/s ');
  });
}
