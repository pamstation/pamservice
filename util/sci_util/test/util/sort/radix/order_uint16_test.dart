import 'package:test/test.dart';
import 'package:sci_util/src/util/list/list.dart' as scilist;
import 'package:sci_util/src/util/sort/radix/radix_sort.dart' as radix;
import 'dart:typed_data';
import 'dart:math' as math;

main() {
  setUp(() {});

  test('00', () {
    var list = [255, 254, 0, 1, 0];
    var order = radix.order_uint16(list);
    var list2 = scilist.sort(list, order);
    expect(scilist.isSorted(list2), isTrue);
  });

  test('01', () {
    var rnd = new math.Random();
    var list = new Uint16List(100);
    var order = scilist.newInt32List(list.length );
    for (var i = 0; i < list.length; i++)
      list[i] = rnd.nextInt(list.length ~/ 10);

    order = radix.order_uint16(list);
    list = scilist.sort(list, order);
    expect(scilist.isSorted(list), isTrue);
  });
}
