import 'package:test/test.dart';
import 'package:sci_util/src/util/list/list.dart' as scilist;
import 'package:sci_util/src/util/sort/radix/radix_sort.dart' as radix;
import 'dart:math' as math;
import 'dart:typed_data';

main() {
  setUp(() {});
  test('00 : slice_uint8string_uint32', () {
    var list = new scilist.Uint8StringList.fromString(['a', 'b', 'c']);

    var slice = new Uint32List(list.length);
    var nth_uint32 = 0;
    var bd = new ByteData.view(slice.buffer);
    bd.setUint8(3, 'a'.codeUnits.first);
    bd.setUint8(7, 'b'.codeUnits.first);
    bd.setUint8(11, 'c'.codeUnits.first);

    var keys = radix.slice_uint8string_uint32(
        list.bytesView, list.startsView, list.length, nth_uint32);

    expect(keys, equals(slice));

    nth_uint32 = 1;
    keys = radix.slice_uint8string_uint32(
        list.bytesView, list.startsView, list.length, nth_uint32);
    expect(keys, equals([0, 0, 0]));
  });

  test('01 : order_uint8String', () {
    var list = new scilist.Uint8StringList.fromString(
        ['123456789', '12345', '1234', '1', '0']);

    var order = radix.order_uint8String(list);
    scilist.reorder(list, order);
    expect(scilist.isSorted(list), isTrue);
  });

  test('02 : order_uint8String', () {
    var rnd = new math.Random(42);
    var nRows = math.pow(10, 6);
    var list = new scilist.Uint8StringList.reserv(nRows);
    for (var i = 0; i < nRows; i++) list.add(rnd.nextInt(nRows).toString());

    new List(10).forEach((_) {
      var order = scilist.order(list);
      list.reorder(order);
      scilist.reorder(list, order);
    });

    list = new scilist.Uint8StringList.reserv(nRows);
    for (var i = 0; i < nRows; i++) list.add(rnd.nextInt(nRows).toString());

    var watch = new Stopwatch()..start();
    var order = scilist.order(list);
    scilist.reorder(list, order);
    watch.stop();
    print(
        'order_uint8String : elapsedMicroseconds = ${watch.elapsedMicroseconds}');
    print(
        'order_uint8String : ${(list.length ~/ (watch.elapsedMicroseconds/1000000)) / 1000000} MB/s ');

    expect(scilist.isSorted(list), isTrue);
  });

  test('03 : order_uint8String', () {
    var rnd = new math.Random(42);
    var nRows = math.pow(10, 6);
    var list = new scilist.Uint8StringList.reserv(nRows);
    for (var i = 0; i < nRows; i++)
      list.add(rnd.nextInt(nRows).toString() +
          rnd.nextInt(nRows).toString() +
          rnd.nextInt(nRows).toString() +
          rnd.nextInt(nRows).toString() +
          rnd.nextInt(nRows).toString() +
          rnd.nextInt(nRows).toString());

    var list2 = list.copy();

    var watch = new Stopwatch()..start();

    var order = scilist.order(list);
    print(
        'order_uint8String : elapsedMicroseconds = ${watch.elapsedMilliseconds}');
    watch.reset();

    scilist.reorder(list, order);
    expect(scilist.isSorted(list), isTrue);

    var order2 = scilist.mergeSort(list2);

    print(
        'order_uint8String : elapsedMicroseconds = ${watch.elapsedMilliseconds}');

    scilist.reorder(list2, order2);
    expect(scilist.isSorted(list2), isTrue);
  });
}
