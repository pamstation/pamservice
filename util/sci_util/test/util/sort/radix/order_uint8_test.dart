import 'package:test/test.dart';
import 'package:sci_util/src/util/list/list.dart' as scilist;
import 'package:sci_util/src/util/sort/radix/radix_sort.dart' as radix;
import 'dart:typed_data';
import 'dart:math' as math;

main() {
  setUp(() {});

  test('00', () {
    var list = <int>[255, 254, 0, 1, 0];
    var order = scilist.newInt32List(list.length );
    order = radix.order_uint8(list);
    var list2 = scilist.sort(list, order);
    expect(scilist.isSorted(list2), isTrue);
  });

  test('01 uint8', () {
    var rnd = new math.Random();
    var nRows = math.pow(10, 6);
    var list = new Uint8List(nRows);
    for (var i = 0; i < list.length; i++) list[i] = rnd.nextInt(255);

    new List(10).forEach((_) {
      var order = radix.order_uint8(list);
      scilist.sort(list, order);
    });

    var watch = new Stopwatch()..start();
    var order = radix.order_uint8(list);
    list = scilist.sort(list, order);
    watch.stop();
    print('uint8 : elapsedMicroseconds = ${watch.elapsedMicroseconds}');
    print(
        'uint8 : ${(list.length ~/ (watch.elapsedMicroseconds/1000000)) / 1000000} MB/s ');
    expect(scilist.isSorted(list), isTrue);
  });
}
