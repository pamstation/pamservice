import 'package:test/test.dart';
import 'package:sci_util/src/util/list/list.dart' as scilist;
import 'package:sci_util/src/util/sort/radix/radix_sort.dart' as radix;
import 'dart:typed_data';
import 'dart:math' as math;

main() {
  setUp(() {});

  test('00', () {
    var list = new Float32List.fromList([2048.0, -2.0, 0.0, 1.0, 0.0]);
    var order = radix.order_float32(list);
    print('order $order');
    list = scilist.sort(list, order);
    print('sorted list $list');
    expect(scilist.isSorted(list), isTrue);
  });

  test('01', () {
    var rnd = new math.Random();
    var nRows = math.pow(10, 6);
    var list = new Float32List(nRows);

    for (var i = 0; i < list.length; i++) list[i] = rnd.nextDouble();

    var list2 = new Float32List(nRows);

    new List(10).forEach((_) {
      var order = scilist.order(list);
      scilist.reorder(list, order);
//      var order = radix.order_float32(list);
//      scilist.sort(list, order);
      for (var i = 0; i < list.length; i++) list[i] = rnd.nextDouble();
      list.sort();
    });

    for (var i = 0; i < list.length; i++) list[i] = rnd.nextDouble();

    var watch = new Stopwatch()..start();
    var order = scilist.order(list);
//    var order = radix.order_float32(list);
    scilist.reorder(list, order);
    watch.stop();
    print('radix : elapsedMicroseconds = ${watch.elapsedMicroseconds}');
    print(
        'radix : ${(list.length*4 ~/ (watch.elapsedMicroseconds/1000000)) / 1000000} MB/s ');

    list2 = new Float32List.fromList(list);
    watch
      ..reset()
      ..start();
    list2.sort();
    watch.stop();

    print('qsort : elapsedMilliseconds = ${watch.elapsedMicroseconds}');
    print(
        'qsort : ${(list.length*4 ~/ (watch.elapsedMicroseconds/1000000)) / 1000000} MB/s ');

    expect(scilist.isSorted(list), isTrue);
  });
}
