import 'package:test/test.dart';
import 'package:sci_util/src/util/list/list.dart' as scilist;
import 'package:sci_util/src/util/sort/radix/radix_sort.dart' as radix;
import 'dart:typed_data';
import 'dart:math' as math;

bool assertIsSorted(List<Comparable> list) {
  if (list.isEmpty) return true;
  for (var i = 1; i < list.length; i++) {
    if (list[i - 1].compareTo(list[i]) > 0) throw 'bad order at ${i}';
  }
  return true;
}

main() {
  setUp(() {});

  test('00', () {
    var list = new Float64List.fromList(
        [2048.0, -2.0, 0.11766924225733832, 0.11763292125287761, 1.0, 0.0]);
    list = new Float64List.fromList(
        [0.11766924225733832, 0.11763292125287761, 0.0]);
    var order = radix.order_float64(list);
    print('order $order');
    list = scilist.sort(list, order);
    print('sorted list $list');
    expect(scilist.isSorted(list), isTrue);
  });

//  test('nan', () {
//    expect(double.infinity > 0.0, isTrue);
//    expect(double.infinity > double.negativeInfinity, isTrue);
//    expect(double.negativeInfinity < 0.0, isTrue);
//
//    print(double.nan.compareTo(0.0));
//    print(0.0.compareTo(-double.nan));
//
//    print((-double.nan).compareTo(double.nan));
//    print(double.nan == (double.nan));
//
//    print('+nan');
//    var ll = new Float64List.fromList([double.nan]);
////    ll = new Float64List.fromList([double.negativeInfinity]);
//    dynamic b = new Uint8List.view(ll.buffer);
//    print(b);
//    b = new Uint64List.view(ll.buffer);
//    print(b[0].toRadixString(2));
//    print(b[0] >> 48);
//
//    print('-nan');
//      ll = new Float64List.fromList([-double.nan]);
////    ll = new Float64List.fromList([double.negativeInfinity]);
//      b = new Uint8List.view(ll.buffer);
//    print(b);
//    b = new Uint64List.view(ll.buffer);
//    print(b[0].toRadixString(2));
//    print(b[0] >> 48);
//
//    print('negativeInfinity');
//    ll = new Float64List.fromList([double.negativeInfinity]);
//    b = new Uint8List.view(ll.buffer);
//    print(b);
//    b = new Uint64List.view(ll.buffer);
//    print(b[0].toRadixString(2));
//    print(b[0] >> 48);
//
//    print('INFINITY');
//    ll = new Float64List.fromList([double.infinity]);
//    b = new Uint8List.view(ll.buffer);
//    print(b);
//    b = new Uint64List.view(ll.buffer);
//    print(b[0].toRadixString(2));
//    print(b[0] >> 48);
//
//    ll = new Float64List.fromList([10.0]);
//    b = new Uint8List.view(ll.buffer);
//    print(b);
//
//    var list = new Float64List.fromList([
//      double.infinity,
//      double.nan,
//      0.0,
//      10.0,
//      double.nan,
//      double.negativeInfinity
//    ]);
//    var order = radix.order_float64(list);
//    print('order $order');
//    scilist.reorder(list, order);
//    print('sorted list $list');
//    expect(scilist.isSorted(list), isTrue);
//
//    scilist.unreorder(list, order);
//
//    order = scilist.order(list);
//    print('order $order');
//    scilist.reorder(list, order);
//    print('sorted list $list');
//    expect(scilist.isSorted(list), isTrue);
//  });

  test('01', () {
    var rnd = new math.Random(42);
    var nRows = math.pow(10, 3);
    var list = new Float64List(nRows);

    for (var i = 0; i < list.length; i++)
      list[i] = rnd.nextInt(1000000) * rnd.nextDouble();

    var order = radix.order_float64(list);

    list = scilist.sort(list, order);

    expect(assertIsSorted(list), isTrue);
  });

  test('01', () {
    var rnd = new math.Random();
    var nRows = math.pow(10, 4);
    var list = new Float64List(nRows);

    for (var i = 0; i < list.length; i++) list[i] = rnd.nextDouble();

    var list2 = new Float64List(nRows);

    new List(10).forEach((_) {
      for (var i = 0; i < list.length; i++) list[i] = rnd.nextDouble();
      var order = scilist.order(list);
      scilist.reorder(list, order);
      for (var i = 0; i < list.length; i++) list[i] = rnd.nextDouble();
      list.sort();
    });

    for (var i = 0; i < list.length; i++) list[i] = rnd.nextDouble();
    list2 = new Float64List.fromList(list);

    var watch = new Stopwatch()..start();
    var order = scilist.order(list);
    scilist.reorder(list, order);
    watch.stop();
    print('radix : elapsedMicroseconds = ${watch.elapsedMicroseconds}');
    print(
        'radix : ${(list.length*4 ~/ (watch.elapsedMicroseconds/1000000)) / 1000000} MB/s ');

    watch
      ..reset()
      ..start();
    list2.sort();
    watch.stop();

    print('qsort : elapsedMilliseconds = ${watch.elapsedMicroseconds}');
    print(
        'qsort : ${(list.length*4 ~/ (watch.elapsedMicroseconds/1000000)) / 1000000} MB/s ');

    expect(scilist.isSorted(list), isTrue);
  });
}
