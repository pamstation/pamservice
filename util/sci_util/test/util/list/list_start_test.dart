import 'package:test/test.dart';
import 'package:sci_util/src/util/list/list.dart' as scilist;
import 'dart:typed_data';

main() {
  setUp(() {});

  test('starts', () {
    expect(scilist.starts(new Float64List.fromList([])), equals([0]));
    expect(scilist.starts(new Float64List.fromList([1.0])), equals([0, 1]));
    expect(
        scilist.starts(new Float64List.fromList([1.0, 1.0])), equals([0, 2]));
    expect(scilist.starts(new Float64List.fromList([1.0, 1.0, 2.0])),
        equals([0, 2, 3]));

    expect(scilist.starts(new Int32List.fromList([])), equals([0]));
    expect(scilist.starts(new Int32List.fromList([1])), equals([0, 1]));
    expect(scilist.starts(new Int32List.fromList([1, 1])), equals([0, 2]));
    expect(
        scilist.starts(new Int32List.fromList([1, 1, 2])), equals([0, 2, 3]));
  });

  test('startsWithStarts', () {
    expect(scilist.startsWithStarts(new Float64List(0), new Int32List(0)),
        equals([0]));
    expect(
        scilist.startsWithStarts(
            new Float64List.fromList([1.0]), new Int32List(0)),
        equals([0]));
    expect(
        scilist.startsWithStarts(
            new Float64List.fromList([1.0]), new Int32List.fromList([0])),
        equals([0]));
    expect(
        scilist.startsWithStarts(
            new Float64List.fromList([1.0]), new Int32List.fromList([0, 1])),
        equals([0, 1]));
    expect(
        scilist.startsWithStarts(new Float64List.fromList([1.0, 1.0]),
            new Int32List.fromList([0, 2])),
        equals([0, 2]));
    expect(
        scilist.startsWithStarts(new Float64List.fromList([1.0, 1.0, 1.0]),
            new Int32List.fromList([0, 2])),
        equals([0, 2]));
    expect(
        scilist.startsWithStarts(new Float64List.fromList([1.0, 1.0, 1.0]),
            new Int32List.fromList([0, 2, 3])),
        equals([0, 2, 3]));
    expect(
        scilist.startsWithStarts(new Float64List.fromList([1.0, 1.0, 1.0, 2.0]),
            new Int32List.fromList([0, 2, 4])),
        equals([0, 2, 3, 4]));
    expect(
        scilist.startsWithStarts(
            new Float64List.fromList([1.0, 1.0, 1.0, 2.0, 2.0]),
            new Int32List.fromList([0, 2, 5])),
        equals([0, 2, 3, 5]));

  });
}
