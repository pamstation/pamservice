import 'package:test/test.dart';
import 'package:sci_util/src/util/list/list.dart' as scilist;
import 'dart:typed_data';
//import 'dart:io';
import 'dart:math' as math;

main() {
  setUp(() {});

  test('00 : PStringList', () {
    math.Random rng = new math.Random();

    var randList = new List.generate(1000, (i)=>rng.nextInt(10).toString());
    var randList2 = new List.generate(1000, (i)=>rng.nextInt(10).toString());

    var list = new scilist.PStringList.fromString(randList);
    var list2 = new scilist.PStringList.fromString(randList2);

    expect(scilist.isSorted(list), isFalse);
    expect(scilist.isSorted(list2), isFalse);

    var order = scilist.order(list);

    scilist.reorder(list, order);

    expect(scilist.isSorted(list), isTrue);

    scilist.reorder(list2, order);
    order = scilist.order(list2);

    expect(scilist.isSorted(list2), isFalse);
    scilist.reorder(list2, order);

    expect(scilist.isSorted(list2), isTrue);


  });

  test('00 : PStringList', () {
    math.Random rng = new math.Random();

    var randList = new List.generate(1000, (i)=>rng.nextInt(10).toString());
    var randList2 = new List.generate(1000, (i)=>rng.nextInt(10).toString());

    var list = new scilist.PStringList.fromString(randList);
    var list2 = new scilist.PStringList.fromString(randList2);

    expect(scilist.isSorted(list), isFalse);
    expect(scilist.isSorted(list2), isFalse);

    var order = scilist.order(list);

    scilist.reorder(list, order);

    expect(scilist.isSorted(list), isTrue);
//////////////////////////
    scilist.reorder(list2, order);

    var randIndex = new List.generate(1000, (i)=>rng.nextInt(1000));

    var selectedList1 = list2.selectByRids(new Int32List.fromList(randIndex));
    list2.rebuildView();
    var selectedList2 = list2.selectByRids(new Int32List.fromList(randIndex));

    expect(selectedList1, equals(selectedList2));




  });

  test('01 : PStringList', () {
    var list = new scilist.PStringList.fromString(['b', 'c', 'a']);

    expect(list.length, equals(3));

    list.add('hey');

    expect(list.length, equals(4));

    list.reorder(new Int32List.fromList([3, 1, 0, 2]));

    print(list);

//    list.equalsTo('b', (i, flag) {
//      print('i=$i flag=$flag');
//    });

//    var order = list.order();
    var order = scilist.order(list);
    print(order);
    scilist.reorder(list, order);

//    list.reorder(order);

    print(list);

    print(list.getOrder());

    var ii;
    list.equalsTo('b', (i, flag) {
      print('i=$i flag=$flag');
      if (flag){
        expect(list[i], equals('b'));
      }
    });


  });
}
