part of sci.csv.parser;

// Character constants.
const int _LF = 10;
const int _CR = 13;

/**
 * A [StreamTransformer] that splits a [String] into individual lines.
 *
 * A line is terminated by either a CR (U+000D), a LF (U+000A), a
 * CR+LF sequence (DOS line ending),
 * and a final non-empty line can be ended by the end of the string.
 *
 * The returned lines do not contain the line terminators.
 */

class CSVSplitter extends Converter<String, List<String>> {
  final CSVParams params;

  const CSVSplitter(this.params);

  List<String> convert(String lines) {
    int start = 0;
    int end = lines.length;
    int sliceStart = 0;
    int char = 0;

    int _sep = params.separator.codeUnitAt(0);
    int _quote = params.quote.isEmpty ? null : params.quote.codeUnitAt(0);

    var result = <String>[];
    var resultLine = <String>[];

    void _addValue(String line, int start, int end) {
      if (resultLine == null) resultLine = [];
      if (start > end) {
        resultLine.add('');
      } else {
        resultLine.add(line.substring(start, end));
      }
    }

    void _pushValues() {
      if (resultLine == null) return;
      result.addAll(resultLine);
      resultLine = null;
    }

    bool _inQuote = false;
    bool _inSep = true;

    for (int i = start; i < end; i++) {
      int previousChar = char;
      char = lines.codeUnitAt(i);

      if (char == _quote) {
        if (previousChar == _CR) {
          _addValue(lines, sliceStart, i - 1);
          _pushValues();
          sliceStart = i;
          _inSep = true;
          _inQuote = true;
        } else {
          _inQuote = !_inQuote;
          if (_inQuote) {
            sliceStart = i + 1;
          } else {
            _addValue(lines, sliceStart, i);
            _inSep = false;
          }
        }
      } else {
        if (_inQuote) continue;
        if (char == _sep) {
          if (_inSep) {
            _addValue(lines, sliceStart, i);
            sliceStart = i + 1;
          } else {
            _inSep = true;
            sliceStart = i + 1;
          }
        } else {
          if (char == _CR) continue;

          if (char == _LF) {
            if (_inSep) {
              if (previousChar == _CR) {
                _addValue(lines, sliceStart, i - 1);
              } else {
                _addValue(lines, sliceStart, i);
              }
            }

            _pushValues();
            sliceStart = i + 1;
            _inSep = true;
          } else if (previousChar == _CR) {
            _addValue(lines, sliceStart, i - 1);
            _pushValues();
            sliceStart = i;
            _inSep = true;
          }
        }
      }
    }

    if (_inSep && sliceStart < end) {
      _addValue(lines, sliceStart, end);
    }

    _pushValues();

    return result;
  }

  StringConversionSink startChunkedConversion(Sink<List<String>> sink) {
//    if (sink is! StringConversionSink) {
//      sink = new StringConversionSink.from(sink);
//    }
    return new _CSVSplitterSink(sink, this.params);
  }
}

class _CSVSplitterSink extends StringConversionSinkBase {
  final CSVParams params;
  final Sink<List<String>> _sink;

  /// The carry-over from the previous chunk.
  ///
  /// If the previous slice ended in a line without a line terminator,
  /// then the next slice may continue the line.
  String _carry;
  List<String> _listCarry;

  /// Whether to skip a leading LF character from the next slice.
  ///
  /// If the previous slice ended on a CR character, a following LF
  /// would be part of the same line termination, and should be ignored.
  ///
  /// Only `true` when [_carry] is `null`.
  bool _skipLeadingLF = false;
  bool _inQuote = false;
  bool _inSep = true;

  int _sep;
  int _quote;

  _CSVSplitterSink(this._sink, this.params) {
    _sep = params.separator.codeUnitAt(0);
    _quote = params.quote.isEmpty ? null : params.quote.codeUnitAt(0);
  }

  void addSlice(String chunk, int start, int end, bool isLast) {
    end = RangeError.checkValidRange(start, end, chunk.length);
    // If the chunk is empty, it's probably because it's the last one.
    // Handle that here, so we know the range is non-empty below.
    if (start >= end) {
      if (isLast) close();
      return;
    }

    if (_carry != null) {
      assert(!_skipLeadingLF);
      chunk = _carry + chunk.substring(start, end);
      start = 0;
      end = chunk.length;
      _carry = null;
    } else if (_skipLeadingLF) {
      if (chunk.codeUnitAt(start) == _LF) {
        start += 1;
      }
      _skipLeadingLF = false;
    }
    _addLines(chunk, start, end);
    if (isLast) close();
  }

  void close() {
    if (_carry != null) {
      if (_listCarry == null) {
        _listCarry = [];
      }
      _listCarry.add(_carry);
    }

    if (_listCarry != null) {
      // last value can be null
      if (_listCarry.length < nCols) {
        _listCarry.add('');
      }

      _sink.add(_listCarry);
      _listCarry = null;
    }
    _sink.close();
  }

  void _addLines(String lines, int start, int end) {
    int sliceStart = start;
    int char = 0;

    for (int i = start; i < end; i++) {
      int previousChar = char;
      char = lines.codeUnitAt(i);

      if (char == _quote) {
        if (previousChar == _CR) {
          _addValue(lines, sliceStart, i - 1);
          _pushValues();
          sliceStart = i;
          _inSep = true;
          _inQuote = true;
        } else {
          _inQuote = !_inQuote;
          if (_inQuote) {
            sliceStart = i + 1;
          } else {
            _addValue(lines, sliceStart, i);
            _inSep = false;
            sliceStart = i + 1;
          }
        }
      } else {
        if (_inQuote) continue;
        if (char == _sep) {
          if (_inSep) {
            _addValue(lines, sliceStart, i);
            sliceStart = i + 1;
          } else {
            _inSep = true;
            sliceStart = i + 1;
          }
        } else {
          if (char == _CR) continue;

          if (char == _LF) {
            if (_inSep) {
              if (previousChar == _CR) {
                _addValue(lines, sliceStart, i - 1);
              } else {
                _addValue(lines, sliceStart, i);
              }
            }

            _pushValues();
            sliceStart = i + 1;
            _inSep = true;
          } else if (previousChar == _CR) {
            _addValue(lines, sliceStart, i - 1);
            _pushValues();
            sliceStart = i;
            _inSep = true;
          }
        }
      }
    }

    if (sliceStart < end) {
      _carry = lines.substring(sliceStart, end);
    } else {
      _skipLeadingLF = (char == _CR);
    }
  }

  void _addValue(String line, int start, int end) {
    if (_listCarry == null) {
      _listCarry = [];
    }

    if (start > end) {
      _listCarry.add('');
    } else {
      _listCarry.add(line.substring(start, end));
    }
  }

  int nCols;

  void _pushValues() {
    if (_listCarry != null) {
      if (nCols == null) nCols = _listCarry.length;
      _sink.add(_listCarry);
      _listCarry = null;
    }
  }
}
