part of sci.csv.parser;

class Parser {
  StreamController<CVSParserEvent> _controller;
  Stream<CVSParserEvent> get onEvent => _controller.stream;
  CVSParserValuesEvent _valuesEvent = new CVSParserValuesEvent();
  int _lineNumber = 0;
//  int _columnIndex = 0;

  String _separator;
  bool _isFirstLine = true;
  int _nCols;
  bool _ignoreEmptyLine;

  StreamSubscription _sub;
  Stream<List<int>> _stream;

  Parser(this._separator, this._stream, {bool ignoreEmptyLine: true}) {
    _ignoreEmptyLine = ignoreEmptyLine;
    _controller = new StreamController<CVSParserEvent>(
        onListen: _onListen,
        onPause: _onPause,
        onResume: _onResume,
        onCancel: _onCancel,
        sync: true);
  }

  _onListen() {
    Stream _lineSplitterStream =
        new LineSplitter().bind(_stream.transform(utf8.decoder));
    _sub = _lineSplitterStream.listen(_onData,
        onError: _controller.addError, onDone: _emitEOF);
  }

  _onPause() {
    _sub.pause();
  }

  _onResume() {
    _sub.resume();
  }

  _onCancel() {
    _sub.cancel();
  }

  void _onData(String line) {
    line = line.trim();
    if (line.isEmpty && _ignoreEmptyLine) return;
    var values = line.split(_separator).map((v) => v.trim()).toList();
    if (_isFirstLine) {
      _emitHeader(values);
      _isFirstLine = false;
      _nCols = values.length;
    } else {
      if (values.length != _nCols) {
        _error(
            "wrong number of columns at line $_lineNumber, found ${values.length} columns expected $_nCols.");
      }
      _emitValues(values);
    }
  }

//  void _emitStart() {
//    _controller.add(new CVSParserStartEvent());
//  }

  void _emitHeader(List<String> headers) {
    _controller.add(new CVSParserHeaderEvent(headers));
  }

  void _emitValues(List<String> values) {
//    _valueEvent = new CVSParserValueEvent();
    _valuesEvent.values = values;
    _controller.add(_valuesEvent);
  }

//  void _emitLine() {
//
//    _lineEvent.lineNumber = this._lineNumber;
//    _controller.add(_lineEvent);
//    _lineNumber++;
//    _columnIndex = 0;
//  }

  void _emitEOF() {
    _controller.add(new CVSParserEndEvent());
    _controller.close();
  }

  void _error(e) {
    _sub.cancel();
    _controller.addError(e);
    _controller.close();
  }
}
