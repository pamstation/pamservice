library sci.csv.parser;

import 'dart:async';
import 'dart:convert';

import 'package:logging/logging.dart';

import '../error/error.dart';

part 'csv_quote_line_splitter.dart';

part 'parser.dart';

class CSVParams {
  final String separator;
  final String encoding;
  final String quote;

  Converter<List<int>, String> _decoder;

  factory CSVParams.fromJson(Map m) =>
      new CSVParams(m['separator'], m['encoding'], m['quote']);

  factory CSVParams.utf8(String separator, [String quote = '']) =>
      new CSVParams(separator, utf8.name, quote);

  factory CSVParams.latin1(String separator, [String quote = '']) =>
      new CSVParams(separator, latin1.name, quote);

  CSVParams(this.separator, this.encoding, this.quote) {
    if (separator == null) throw new ArgumentError.notNull('separator');
    if (encoding == null) throw new ArgumentError.notNull('encoding');
    if (quote == null) throw new ArgumentError.notNull('quote');

//    if (!(encoding == utf8.name || encoding == latin1.name))
//      throw new ArgumentError.value(encoding, 'bad encoding');

    if (!(quote == '' || quote == '"' || quote == "'"))
      throw new ArgumentError.value(quote, 'bad quote');

    if (!(separator == '\t' || separator == ',' || separator == ';'))
      throw new ArgumentError.value(separator, 'bad separator');
  }

  set decoder(Converter<List<int>, String> c){
    _decoder  = c;
  }

  Converter<List<int>, String> get decoder {
//
    if (_decoder != null) return _decoder;

    if (encoding == utf8.name)
      return utf8.decoder;
    else if (encoding == latin1.name)
      return latin1.decoder;
    else
      throw new ArgumentError.value(encoding, 'bad encoding');
  }

//  Converter<String, List<int>> get encoder {
//    if (encoding == utf8.name)
//      return utf8.encoder;
//    else if (encoding == latin1.name)
//      return latin1.encoder;
//    else
//      throw new ArgumentError.value(encoding, 'bad encoding');
//  }

  Map toJson() =>
      {'separator': separator, 'encoding': encoding, 'quote': quote};
}

class CSVParser {
  static final Logger log = new Logger("CSVParser");

  StreamController<CVSParserEvent> _controller;
  Stream<CVSParserEvent> get onEvent => _controller.stream;
  CVSParserValuesEvent _valuesEvent = new CVSParserValuesEvent();
  int _lineNumber = 0;
//  int _columnIndex = 0;

  CSVParams _params;
  bool _isFirstLine = true;
  int _nCols;
  bool _ignoreEmptyLine;

  StreamSubscription _sub;
  Stream<List<int>> _stream;

  static Future<String> guessSeparator(Stream<List<int>> stream) async {
    List<String> lines = await stream
        .transform(utf8.decoder)
        .transform(const LineSplitter())
        .take(10)
        .toList();

    var reduced = lines.join(new String.fromCharCodes([13, 10])); // CRLF

    var reducedBytes = utf8.encoder.convert(reduced);

    var list = [',', ';', '\t'];
    var nCols = 0;

    String guess;

    for (var sep in list) {
      var params = new CSVParams.utf8(sep, '"');
      var reducedStream = new Stream.fromIterable([reducedBytes]);

      var parser = new CSVParser(params, reducedStream);

      try {
        CVSParserHeaderEvent evt = await parser.onEvent
            .firstWhere((evt) => evt is CVSParserHeaderEvent);
        if (evt.headers.length > nCols) {
          guess = sep;
          nCols = evt.headers.length;
        }
      } catch (e) {
        print('CSVParser $e');
      }
    }

    return guess;
  }

  CSVParser(this._params, this._stream, {bool ignoreEmptyLine: true}) {
    if (this._params == null) throw "CSVParser : _params is null";
    _ignoreEmptyLine = ignoreEmptyLine;
    _controller = new StreamController<CVSParserEvent>(
        onListen: _onListen,
        onPause: _onPause,
        onResume: _onResume,
        onCancel: _onCancel,
        sync: true);
  }

  _onListen() {
    log.finest("_onListen");

    _sub = _params.decoder
        .fuse<List<String>>(new CSVSplitter(_params))
        .bind(_stream)
        .listen(_onData,
            onError: _controller.addError,
            onDone: _emitEOF,
            cancelOnError: true);
  }

  _onPause() {
    log.finest("_onPause");
    _sub.pause();
  }

  _onResume() {
    log.finest("_onResume");
    _sub.resume();
  }

  _onCancel() {
    log.finest("_onCancel");
    _sub.cancel();
  }

  void _onData(List<String> values) {
    if (_controller.isClosed) return;
    _lineNumber++;
    if (_isFirstLine) {
      _emitHeader(values);
      _isFirstLine = false;
      _nCols = values.length;
    } else {
      if (values.length != _nCols) {
        var err = new ServiceError(400, "csv.parser.missing.column",
            "Wrong number of columns at line $_lineNumber, found ${values.length} columns expected $_nCols.");
        _error(err);
      }
      _emitValues(values);
    }
  }

  void _emitHeader(List<String> headers) {
    if (_controller.isClosed) return;
    _controller.add(new CVSParserHeaderEvent(headers));
  }

  void _emitValues(List<String> values) {
    if (_controller.isClosed) return;
    _valuesEvent.values = values;
    _controller.add(_valuesEvent);
  }

  void _emitEOF() {
    _controller.add(new CVSParserEndEvent());
    _controller.close();
  }

  void _error(e) {
    log.warning("", e);
    _sub.cancel();
    _controller.addError(e);
    _controller.close();
  }
}

abstract class CVSParserEvent {}

class CVSParserStartEvent extends CVSParserEvent {}

class CVSParserHeaderEvent extends CVSParserEvent {
  List<String> headers;
  CVSParserHeaderEvent(this.headers);
}

class CVSParserValuesEvent extends CVSParserEvent {
  List<String> values;
}

class CVSParserEndEvent extends CVSParserEvent {}
