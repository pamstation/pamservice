import 'dart:async';

class TreeNode<T> {
  final T object;
  final Map _properties = {};
  TreeNode<T> parent;
  final List<TreeNode<T>> children = [];

  final StreamController _controller =
      new StreamController.broadcast(sync: true);

  TreeNode(this.object);

  bool get isRoot => parent == null;

  void addChild(TreeNode<T> child) {
    children.add(child);
    child.parent = this;
  }

  getProperty(String name) => _properties[name];
  setProperty(String name, value) {
    _properties[name] = value;
    _controller.add(null);
  }

  Stream get onChange => _controller.stream;

  bool removeChild(TreeNode<T> child) => children.remove(child);

  bool get hasChildren => children.isNotEmpty;

  Iterable<TreeNode<T>> get ancestors {
    var list = <TreeNode<T>>[];
    if (isRoot) return list;
    parent._getAncestors(list);
    return list;
  }

  void _getAncestors(List<TreeNode<T>> ancestors) {
    ancestors.add(this);
    if (isRoot) return;
    parent._getAncestors(ancestors);
  }
}
