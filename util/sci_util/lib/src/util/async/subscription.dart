import 'dart:async';

class SubscriptionHelper {
  Map<Object, List<StreamSubscription>> _subscriptions = {};

  bool hasSubscriptions(o) => _subscriptions.containsKey(o);

  void addSubscription(o, StreamSubscription sub) {
    var subs = _subscriptions.putIfAbsent(o, () => []);
    subs.add(sub);
  }

  Future<bool> removeSubscriptions(o) {
    var subs = _subscriptions.remove(o);
    if (subs == null) return new Future.value(false);
    return Future
        .wait(subs.map((s) => new Future.sync(() => s.cancel())))
        .then((_) => true);
  }

  Future releaseSubscriptions() {
    var keys = new List.from(_subscriptions.keys);
    return Future.wait(keys.map(removeSubscriptions)).whenComplete(() {
      _subscriptions = {};
    });
  }
}
