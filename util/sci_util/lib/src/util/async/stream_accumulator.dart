library sci_stream_accumulator;

import 'dart:async';
import 'dart:math';
import 'dart:collection';

import 'package:logging/logging.dart';
import 'package:async/async.dart' as async;

import '../list/list.dart';

//import 'package:sci_util/src/util/csf/morton.dart' as morton;

abstract class SplitRangeStream<K, V> {
  Stream<Pair<K, V>> select(K range);
}

class LazySplitRangeStream<K, V> implements SplitRangeStream<K, V> {
  Future<SplitRangeStream<K, V>> _future;

  LazySplitRangeStream(this._future);

  Stream<Pair<K, V>> select(K range) => new async.LazyStream(
      () => _future.then((splitStream) => splitStream.select(range)));

  Future<SplitRangeStream<K, V>> get inner => _future;
}

abstract class SplitRangeAccumulator<K, V> {
  Stream<Pair<K, V>> get stream;
  Iterable<Pair<K, V>> select(K range);
}

abstract class SplitRangeAccumulatorBase<K, V>
    implements SplitRangeAccumulator<K, V> {
  //
  static Logger logger = new Logger('SplitRangeAccumulatorBase');

  StreamController<Pair<K, V>> controller;
  StreamController streamChangedController;
  Map<K, List<Pair<K, V>>> ranges;
  Queue<K> rangeQueue;
  K currentRange;

//  int rangeMaxLength;
  int rangeMaxLength;

  SplitRangeStream<K, V> _splitRangeStream;

  StreamSubscription _sub;
  Stopwatch _watch;
  bool _loading = false;

  SplitRangeAccumulatorBase(this._splitRangeStream,
      {this.rangeMaxLength: 1024, bool isBroadcast: true}) {
    //
    if (isBroadcast) {
      controller = new StreamController.broadcast(
          onCancel: onControllerCancel, sync: false);
    } else {
      controller =
          new StreamController(onCancel: onControllerCancel, sync: false);
    }

    streamChangedController = new StreamController.broadcast(sync: true);
    rangeQueue = new Queue();
    ranges = createRanges();
  }

  Future release() => new Future.sync(clear);

  Map<K, List<Pair<K, V>>> createRanges() => new HashMap<K, List<Pair<K, V>>>();

  void onControllerCancel() {
    clear();
  }

  SplitRangeStream<K, V> get splitRangeStream => _splitRangeStream;

  set splitRangeStream(SplitRangeStream<K, V> splitStream) {
    if (splitStream == _splitRangeStream) return;
//    if (splitStream is LazySplitRangeStream<K, V>) {
//      splitStream.inner.whenComplete(() {
//        streamChangedController.add(null);
//      });
//    }
    _splitRangeStream = splitStream;
    clear();
  }

  void clear() {
    rangeQueue = new Queue();
    ranges = createRanges();
    currentRange = null;
    cancelSub();
    streamChangedController.add(null);
  }

  Stream<Pair<K, V>> get stream => controller.stream;

  Stream get onStreamChanged => streamChangedController.stream;

  void onRangePair(Pair<K, V> rangePair) {
     if (controller.hasListener) {
      var list = ranges.putIfAbsent(rangePair.first, () => []);
      list.add(rangePair);
      controller.add(rangePair);
      _watch
        ..reset()
        ..start();
    } else {
      clear();
    }
  }

  void onDone() {
    _watch.stop();
    _watch = null;
    currentRange = null;
    cancelSub();
//    runQueue();
  }

  bool _isCanceling = false;

  void cancelSub([bool runQ = true]) {
    _loading = false;
    if (_sub == null) {
      if (runQ) runQueue();
      return;
    }

    if (_isCanceling) return;
    _isCanceling = true;
    _sub.cancel().whenComplete(() {
      _sub = null;
      _isCanceling = false;
      if (runQ) runQueue();
    });
  }

  void onError(e, st) {
    controller.addError(e, st);
    currentRange = null;
    cancelSub(false);
  }

  Future loadRange(K range) {
    assert(_sub == null);
    assert(!_loading);
    _loading = true;
    _watch = new Stopwatch()..start();
    return new Future.sync(() async {
      _sub = (await _splitRangeStream)
          .select(range)
          .listen(onRangePair, onDone: onDone, onError: onError);
    });
  }

  void runQueue() {
    if (rangeQueue.isEmpty || _sub != null || _loading || _isCanceling) return;
    currentRange = rangeQueue.removeFirst();
    loadRange(currentRange);
  }

  Iterable<Pair<K, V>> select(K range) {
//    logger.finest('$this -- select2 -- range ${range}');
    if (range != null) query(range);
    return _select(range);
  }

  Iterable<Pair<K, V>> _select(K range) sync* {
    Iterable<K> keys;
    if (range == null) {
      keys = ranges.keys;
    } else {
      keys = ranges.keys.where((k) => intersects(k, range));
    }

    for (var k in keys) {
      var pairList = ranges[k];
      for (var pair in pairList) {
        yield pair;
      }
    }
  }

  Future<Iterable<K>> split(K range) =>
      new Future.sync(() => splitRange(range, rangeMaxLength));

  query(K range) async {

//    var watch = new Stopwatch()..start();
    var splittedRanges = await split(range);

    splittedRanges = splittedRanges
        .where((r) => currentRange != r && !ranges.containsKey(r));

    rangeQueue.clear();
    rangeQueue.addAll(splittedRanges);

    runQueue();

//    logger.finest('$this -- query -- range ${range} -- ${watch.elapsedMilliseconds} ms' );
  }

  bool intersects(K k1, K k2);
  Iterable<K> splitRange(K range, int rangeMaxLength);
}

class SplitRange<V> extends SplitRangeAccumulatorBase<Range, V> {
  SplitRange(SplitRangeStream<Range, V> splitRangeStream,
      {int rangeMaxLength: 100})
      : super(splitRangeStream, rangeMaxLength: rangeMaxLength);

  bool intersects(Range k1, Range k2) => k1.intersect(k2);

  Map<Range, List<Pair<Range, V>>> createRanges() =>
      new SplayTreeMap((  k1,   k2) => k1.start.compareTo(k2.start));

  Iterable<Range> splitRange(Range range, int rangeMaxLength) sync* {
    var s = rangeMaxLength * (range.start ~/ rangeMaxLength);
    var required = range.start + range.len;

    while (s < required) {
      yield new Range(s, rangeMaxLength);
      s += rangeMaxLength;
    }
  }
}

class SplitRectangleStream<V>
    extends SplitRangeAccumulatorBase<Rectangle<int>, V> {
  SplitRectangleStream(SplitRangeStream<Rectangle, V> splitRangeStream,
      {int rangeMaxLength: 1024, bool isBroadcast: false})
      : super(splitRangeStream,
            rangeMaxLength: rangeMaxLength, isBroadcast: isBroadcast);

  bool intersects(Rectangle k1, Rectangle k2) {
    return (k1.left < k2.left + k2.width &&
        k2.left < k1.left + k1.width &&
        k1.top < k2.top + k2.height &&
        k2.top < k1.top + k1.height);
  }

  Iterable<Rectangle<int>> splitRange(
      Rectangle<int> range, int rangeMaxLength) {
    if (range.width < 1 || range.height < 1) return [];
    var nx0 = range.left ~/ rangeMaxLength;
    var ny0 = range.top ~/ rangeMaxLength;

    var nx1 = (range.right / rangeMaxLength).ceil();
    var ny1 = (range.bottom / rangeMaxLength).ceil();

    if (nx0 == nx1) nx1++;
    if (ny0 == ny1) ny1++;

    var list = <Rectangle<int>>[];

    for (var i = nx0; i < nx1; i++) {
      var x = i * (rangeMaxLength);
      for (var j = ny0; j < ny1; j++) {
        var y = j * (rangeMaxLength);
        list.add(new Rectangle(x, y, rangeMaxLength, rangeMaxLength));
      }
    }

    print('*********** TODO : JS fails, not impl ***********');

//    throw 'TODO : JS fails, not impl';

//    list.sort((rec1, rec2) => morton
//        .xy2d_morton(rec1.left, rec1.top)
//        .compareTo(morton.xy2d_morton(rec2.left, rec2.top)));

    return list;
  }
}
