import 'dart:async';

Future retry(Duration timeout, Duration sleep, Future fun()) async {
  var done = false;

  var result;

  var timeoutDateTime = new DateTime.now().add(timeout);

  while (!done) {
    if (new DateTime.now().isAfter(timeoutDateTime)) throw 'retry timeout';

    try {
      result = await fun();
      done = true;
    } catch (e) {
      await new Future.delayed(sleep);
    }
  }

  return result;
}
