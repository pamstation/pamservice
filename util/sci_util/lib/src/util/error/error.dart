class ServiceError {
  Map _data;
  ServiceError(int statusCode, String error, [String reason]) {
    if (statusCode == null) {
      statusCode = -42;
      print("$this : warning statusCode is null");
    }
    _data = {};
    _data["statusCode"] = statusCode;
    _data["error"] = error == null ? '' : error;
    _data["reason"] = reason == null ? error : reason;
  }
  ServiceError.fromObject(int statusCode, String error, Map reasonObject) {
    if (statusCode == null) throw "sci_error.Error statusCode is null";
    _data = {};
    _data["statusCode"] = statusCode;
    _data["error"] = error;
    _data["reasonObject"] = reasonObject;
  }
  ServiceError.fromJson(this._data);
  factory ServiceError.fromError(e) {
    if (e is ServiceError) return e;
    return new ServiceError.unknown(e.toString());
  }
  ServiceError.abort(String error, [String reason]) : this(-1, error, reason);
  ServiceError.unknown([String reason]) : this(500, 'unknown', reason);
  ServiceError.conflict(String error, [String reason])
      : this(409, error, reason);

  ServiceError.bad(String error, [String reason]) : this(400, error, reason);
  ServiceError.forbidden(String error, [String reason])
      : this(401, error, reason);

  ServiceError.notFound(String error, [String reason])
      : this(404, error, reason);
  ServiceError.userAbort(String error) : this(-1, error, "User abort");

  String toString() => "${this.runtimeType}($_data)";

  Map toJson() => _data;
  Map get reasonObject => _data["reasonObject"];
  String get reason => _data["reason"];
  String get error => _data["error"];
  int get statusCode => _data["statusCode"];
  bool get isLogged => _data["isLogged"] == null ? false : _data["isLogged"];
  set isLogged(bool b) {
    _data["isLogged"] = b;
  }

  bool get isBadRequestError => statusCode == 400;
  bool get isUnauthorizedError => statusCode == 401;
  bool get isConflictError => statusCode == 409;
  bool get isNotFoundError => statusCode == 404;
  bool get isUnknownError => statusCode == 500;
  bool get isUserAbortError => statusCode == -1;
  bool get isTimeoutError => statusCode == 504;

  @override
  int get hashCode => statusCode.hashCode;

  @override
  bool operator ==(other) =>
      other is ServiceError &&
      other.statusCode == statusCode &&
      other.error == error;
}

class ServiceErrors extends ServiceError {
  ServiceErrors.fromJson(Map data) : super.fromJson(data);
  ServiceErrors(
      int statusCode, String error, String reason, List<ServiceError> errors)
      : super(statusCode, error, reason) {
    _data["errors"] = errors.map((e) => e.toJson()).toList();
  }

  List<ServiceError> get errors => (_data["errors"] as List)
      .map((m) => new ServiceError.fromJson(m))
      .toList();
}

class CouchError extends ServiceError {
  CouchError.fromJson(Map data) : super.fromJson(data);
  CouchError(int statusCode, String error, String reason)
      : super(statusCode, error, reason);

  bool get isNotFoundError =>
      super.isNotFoundError || (error == "not_found" && reason == "missing");
  bool get isDeletedError => error == "not_found" && reason == "deleted";
  bool get isUnknownError => error == "unknown";
}

class TimeoutError extends ServiceError {
  TimeoutError() : super(504, "time.out", "time out.");
  TimeoutError.fromJson(Map m) : super.fromJson(m);
}

class KillError extends ServiceError {
  KillError() : super(-1, "kill", "kill");
  KillError.fromJson(Map m) : super.fromJson(m);
}
