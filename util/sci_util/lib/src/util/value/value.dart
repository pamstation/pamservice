import 'dart:async';

typedef T GetValueHolderCallback<T>();
typedef void SetValueHolderCallback<T>(T value);

abstract class ObjectProperties<T> {
  T get(String name);
  set(String name, T value);

  Value<T> getPropertyAsValue(String name);
}

abstract class Value<T> {
  StreamController _controller;
  Value() {
    _controller = new StreamController.broadcast(onListen: onListen,onCancel:onCancel,  sync: true);
  }
  T get value;
  set value(T v);
  Stream get onChange => _controller.stream;
  void onListen(){}
  void onCancel(){}
  void sendChangeEvent() => _controller.add(null);
  Future release() async {
    _controller.close();
  }
}

class ValueHolder<T> extends Value<T> {
  T _value;
  ValueHolder(this._value);
  T get value => _value;
  set value(T v) {
    _value = v;
  }
}

class ValueCallback<T> extends Value<T> {
  final GetValueHolderCallback<T> getCallback;
  final SetValueHolderCallback<T> setCallback;

  ValueCallback(this.getCallback, this.setCallback);

  T get value => getCallback();
  set value(T v) {
    setCallback(v);
  }
}
