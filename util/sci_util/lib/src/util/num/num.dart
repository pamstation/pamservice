library sci_util_num;

import 'dart:typed_data' as td;
import 'package:intl/intl.dart' as intl;

import '../type/type.dart' as scitype;

const double EPSILONFLOAT64 = double.minPositive;
const double MAXFLOAT64 = double.maxFinite;
const double MINFLOAT64 = -double.maxFinite;

const double DOUBLE_NULL = double.nan;
const int UINT32_NULL = 4294967295;
const int INT32_NULL =    -2147483648; //2147483647
const int UINT64_NULL = 2^64-1;
const int UINT8_NULL = 255;
const int INT8_NULL = -128;
const int UINT16_NULL = 65535;
const int INT16_NULL = -32768;
const int INT64_NULL = -9223372036854775808;

const String STR_NAN = "NaN";
const String STR_INFINITY = "Infinity";
const String STR_MINFINITY = "-Infinity";

intl.NumberFormat _FORMAT = new intl.NumberFormat("##0.0#", "en_US");
intl.NumberFormat _SCIENTIFIC_FORMAT = new intl.NumberFormat.scientificPattern()
  ..maximumFractionDigits = 2;

num nullForTyped(String type) {
  switch (type) {
    case scitype.UINT16:
      return UINT16_NULL;
    case scitype.UINT8:
      return UINT8_NULL;
    case scitype.DOUBLE:
      return DOUBLE_NULL;
    case scitype.INT64:
      return INT64_NULL;
    case scitype.INT32:
      return INT32_NULL;
    case scitype.UINT32:
      return UINT32_NULL;
    case scitype.UINT64:
      return UINT64_NULL;
    default:
      throw new ArgumentError.value(type, 'bad type');
  }
}

num nullForTypedData(td.TypedData list) {
  if (list is td.Uint16List) {
    return UINT16_NULL;
  } else if (list is td.Uint8List) {
    return UINT8_NULL;
  } else if (list is td.Float64List) {
    return DOUBLE_NULL;
  } else if (list is td.Int64List) {
    return INT64_NULL;
  } else if (list is td.Int32List) {
    return INT32_NULL;
  } else if (list is td.Uint32List) {
    return UINT32_NULL;
  } else if (list is td.Uint64List) {
    return UINT64_NULL;
  } else {
    throw 'nullForTypedData : unknown type data : ${list.runtimeType}';
  }
}

bool isFloat64Null(double value) => value.isNaN;

bool isInt32Null(int value) {
  return (INT32_NULL == value);
}

bool isUInt64Null(int value) {
  return (UINT64_NULL == value);
}

bool isUInt16Null(int value) {
  return (UINT16_NULL == value);
}

bool isUInt8Null(int value) {
  return (UINT8_NULL == value);
}

bool isInt8Null(int value) {
  return (INT8_NULL == value);
}

bool isInt16Null(int value) {
  return (INT16_NULL == value);
}

String formatFloat64(double value) {
  if (!value.isFinite) return value.toString();

  var format = value.abs() < 0.01 ? _SCIENTIFIC_FORMAT : _FORMAT;
  return format.format(value);
}

String formatInt32(int value) {
  if (isInt32Null(value)) return STR_NAN;
  return value.toString();
}

String displayString(num value, {bool naAsEmptyString: false}) {
  // in js every thing is a double
  if (value is int) {
    if (isInt32Null(value)) return naAsEmptyString ? '' : STR_NAN;
    return value.toString();
  } else {
    if (value.isNaN) return naAsEmptyString ? '' : STR_NAN;
    return value.toString();
  }
}

String formatNum(num value) {
  // in js every thing is a double
  if (value is int) {
    return formatInt32(value);
  } else {
    return formatFloat64(value);
  }
}

//fillInteger32AsByteArrayIn(int anInteger, List aList) {
//  for (int i = 0; i < 4; i++) {
//    int offset = (3 - i) * 8;
//    aList.add((anInteger >> offset) & 0xFF);
//  }
//}

//fillFloat32AsByteArrayIn(double aDouble, List aList) {
//  var fL = new td.Float64List(1);
//  fL[0] = aDouble;
//  aList.addAll(new td.Uint8List.view(fL.buffer, 0, 4));
//}
