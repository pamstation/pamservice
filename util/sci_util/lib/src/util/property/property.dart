import 'dart:async';
import '../value/value.dart';
import 'package:sci_api_gen/sci_base.dart' as base;

class Property extends Value<dynamic> {
  static const String TEXT_AREA_DISPLAY_TYPE = 'textarea';
  static const String TEXT_DISPLAY_TYPE = 'text';
  static const String PASSWORD_DISPLAY_TYPE = 'password';
  static const String CHECK_BOX_DISPLAY_TYPE = 'checkbox';

  static const String STRING_VALUE_TYPE = 'string';
  static const String DOUBLE_VALUE_TYPE = 'double';
  static const String BOOL_VALUE_TYPE = 'bool';

  String name;
  dynamic _value;
  String displayName;
  String displayType;
  bool enable;
  String valueType;

  Property(this.name, this._value,
      {this.displayName,
      this.displayType: TEXT_DISPLAY_TYPE,
      this.enable: true,
      this.valueType: STRING_VALUE_TYPE}) {
    if (displayName == null) displayName = name;
    if (enable == null) enable = true;
    if (valueType == null) valueType = STRING_VALUE_TYPE;
    if (displayType == null) displayType = TEXT_DISPLAY_TYPE;
  }

  @override
  Stream get onChange {
    if (_value is Value) {
      return _value.onChange;
    }
    return super.onChange;
  }

  @override
  dynamic get value {
    if (_value is Value) {
      return _value.value;
    }
    return _value;
  }

  @override
  set value(dynamic v) {
    if (_value is Value) {
      _value.value = v;
    } else {
      _value = v;
    }
  }

  @override
  Future release(){
    if (_value is Value) {
      return _value.release();
    }
    return super.release();
  }
}

class EnumerationProperty extends Property {
  List<dynamic> enumeration;
  List<String> displayEnumeration;

  EnumerationProperty(String name, value, this.enumeration,
      {String displayName, String displayType, this.displayEnumeration})
      : super(name, value, displayName: displayName, displayType: displayType) {
    if (displayEnumeration == null) displayEnumeration = enumeration;
    assert(displayEnumeration.length == enumeration.length);
  }
}

class PropertyList {
  List<Property> _properties;
  PropertyList() {
    _properties = [];
  }

  void addObject(base.Base object, String propertyName,
      {String label: '',
      String displayType,
      bool enable,
      List<dynamic> enumeration,
      List<String> displayEnumeration}) {
    var value = object.getPropertyAsValue(propertyName);

    if (enumeration != null) {
      _properties.add(new EnumerationProperty(propertyName, value, enumeration,
          displayName: label,
          displayType: displayType,
          displayEnumeration: displayEnumeration));
    } else {
      if (displayType == null) {
        displayType = Property.TEXT_DISPLAY_TYPE;
      }

      _properties.add(new Property(propertyName, value,
          displayName: label,
          displayType: displayType,
          enable: enable,
          valueType: getValueType(value.value)));
    }
  }

  String getValueType(dynamic v) {
    var valueType;

    if (v is String) {
      valueType = Property.STRING_VALUE_TYPE;
    } else if (v is double) {
      valueType = Property.DOUBLE_VALUE_TYPE;
    } else if (v is bool) {
      valueType = Property.BOOL_VALUE_TYPE;
    } else {
      throw 'bad value type';
    }
    return valueType;
  }

  void add(String name, value,
      [String displayName, String displayType, bool enable]) {
    _properties.add(new Property(name, value,
        displayName: displayName, displayType: displayType, enable: enable));
  }

  void addEnumeration(String name, value, List enumeration,
      [String displayName,
      String displayType,
      List<String> displayEnumeration,
      bool enable]) {
    _properties.add(new EnumerationProperty(name, value, enumeration,
        displayName: displayName,
        displayType: displayType,
        displayEnumeration: displayEnumeration));
  }

  List<Property> get properties => _properties;

  bool hasProperty(String name) => this._properties.any((p) => p.name == name);
  Property getProperty(String name) =>
      _properties.firstWhere((p) => p.name == name);
}
