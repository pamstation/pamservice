library sci_file;

import 'dart:async';
import 'dart:convert';
import 'dart:typed_data' as td;
import 'package:crypto/crypto.dart' as crypto;
//import 'package:crypto/src/digest_sink.dart' as cryptosink;

class FileUtil {
  static String value2LongPath(String value) {
    if (value.length < 2) {
      return value;
    }

    var sb = new StringBuffer();
    if (value.length >= 2) {
      sb..write(value.substring(0, 2))..write('/');
    }
    if (value.length >= 4) {
      sb..write(value.substring(2, 4))..write('/');
    }
//    if (value.length >= 6) {
//      sb..write(value.substring(4, 6))..write('/');
//    }
//    if (value.length >= 8) {
//      sb..write(value.substring(6, 8))..write('/');
//    }

    sb.write(value);
    return sb.toString();
  }

  static String getParent(String path){
    if (!path.startsWith('/')) throw 'FileUtil : relative path cannot get parent directory';
    var list = path.split('/');

    list.removeLast();
    var sb = new StringBuffer('/');
    list.forEach((each){
      sb..write(each)..write('/');
    });
    return sb.toString();
  }
}

/// A sink used to get a digest value out of [Hash.startChunkedConversion].
class DigestSink extends Sink<crypto.Digest> {
  /// The value added to the sink, if any.
  crypto.Digest get value {
    assert(_value != null);
    return _value;
  }

  crypto.Digest _value;

  /// Adds [value] to the sink.
  ///
  /// Unlike most sinks, this may only be called once.
  void add(crypto.Digest value) {
    assert(_value == null);
    _value = value;
  }

  void close() {
    assert(_value != null);
  }
}

abstract class FileRepository {
  static FileRepository CURRENT;
  static setCurrent(FileRepository r) {
    CURRENT = r;
  }

  factory FileRepository() {
    if (CURRENT == null) throw "FileRepository not initialized CURRENT == null";
    return CURRENT;
  }
  Future<File> getFile(String url);
  File createFile(String url);
  Directory createDirectory(String url);
  Directory createTempDirectory();
  Future deleteFile(String url);
}

abstract class Entity {
  String get url;
  Future<bool> exists();
  bool get isFile;
  Future delete();
}

abstract class Directory extends Entity {
  Future<Directory> create({bool recursive});
  Future<List<Entity>> list({bool recursive: false, bool followLinks: true});
  File createFile(String url);
  Directory createDirectory(String url);
  Future delete({bool recursive: true});
  Future rename(String url);
  Directory get parent;
}

/**
 * The modes in which a File can be opened.
 */
class FileMode {
  /// The mode for opening a file only for reading.
  static const READ = const FileMode._internal(0);

  /// Mode for opening a file for reading and writing. The file is
  /// overwritten if it already exists. The file is created if it does not
  /// already exist.
  static const WRITE = const FileMode._internal(1);

  /// Mode for opening a file for reading and writing to the
  /// end of it. The file is created if it does not already exist.
  static const APPEND = const FileMode._internal(2);

  /// Mode for opening a file for writing *only*. The file is
  /// overwritten if it already exists. The file is created if it does not
  /// already exist.
  static const WRITE_ONLY = const FileMode._internal(3);

  /// Mode for opening a file for writing *only* to the
  /// end of it. The file is created if it does not already exist.
  static const WRITE_ONLY_APPEND = const FileMode._internal(4);
  final int _mode;

  const FileMode._internal(this._mode);

  int get mode => _mode;
}

/// The mode for opening a file only for reading.
const READ = FileMode.READ;

/// The mode for opening a file for reading and writing. The file is
/// overwritten if it already exists. The file is created if it does not
/// already exist.
const WRITE = FileMode.WRITE;

/// The mode for opening a file for reading and writing to the
/// end of it. The file is created if it does not already exist.
const APPEND = FileMode.APPEND;

/// Mode for opening a file for writing *only*. The file is
/// overwritten if it already exists. The file is created if it does not
/// already exist.
const WRITE_ONLY = FileMode.WRITE_ONLY;

/// Mode for opening a file for writing *only* to the
/// end of it. The file is created if it does not already exist.
const WRITE_ONLY_APPEND = FileMode.WRITE_ONLY_APPEND;

abstract class File extends Entity {
  static String compose(String path, String subpath) {
    var buffer = new StringBuffer();
    buffer.write(path);
    if (!path.endsWith("/")) {
      buffer.write("/");
    }
    subpath = subpath.startsWith("/") ? subpath.substring(1) : subpath;
    buffer.write(subpath);
    return buffer.toString();
  }

  Directory get parent;

  Future<String> md5() {
    var innerSink = new DigestSink();
    var outerSink = crypto.md5.startChunkedConversion(innerSink);
    return this.stream().then((s) {
      return s.forEach(outerSink.add).then((_) {
        outerSink.close();
        return innerSink.value.toString();
      });
    });

//    var md5 = new crypto.MD5();
//    return this.stream().then((s) {
//      return s.forEach(md5.add).then((_) {
//        return crypto.CryptoUtils.bytesToHex(md5.close());
//      });
//    });
  }

  Future<int> length();
//  Future append(td.Uint8List bytes);
  FileSink openWrite({FileMode mode: APPEND});
  Future<ReadAccessFile> open();
  Future<Stream<List<int>>> stream();
  Future<List<int>> readAsBytes() {
    return this.open().then((raf) {
      return raf.length().then((len) {
        var buffer = new td.Uint8List(len);
        return raf.readInto(buffer).then((l) {
          return buffer;
        });
      });
    });
  }

  Future<String> readAsString() {
    return readAsBytes().then((codeUnits) {
      return utf8.decode(codeUnits);
    });
  }

  Future<FileStat> fileStats() async {
    throw 'not implemented';
  }
}

class FileStat {
  final int size;
  final DateTime modified;
  FileStat(this.size, this.modified);
}

abstract class FileSink {
  void add(td.Uint8List bytes);
  Future flush();
  Future close();
  Future addStream(Stream<List<int>> stream) async {
    await for (var value in stream) {
      add(value);
    }
  }
}

abstract class ReadAccessFile {
//  int positionSync();
  Future<int> length();
  Future<ReadAccessFile> setPosition(int position);
  Future<int> readInto(List<int> buffer, [int start, int end]);
  Future close();
}

class FileException {
  String msg;
  FileException(this.msg);
}
