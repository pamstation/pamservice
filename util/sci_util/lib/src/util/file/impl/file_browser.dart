library sci_file_browser;

import 'dart:async';
import 'dart:math' as math;
import 'dart:html' as html;
import 'dart:typed_data' as td;
import 'package:logging/logging.dart';

import 'package:sci_util/src/util/list/list.dart' as scilist;

import '../file.dart' as sciapi;

class FileRepositoryImpl implements sciapi.FileRepository {
  @override
  sciapi.Directory createDirectory(String url) {
    return new DirectoryImpl(url);
  }

  @override
  sciapi.File createFile(String url) {
    // TODO: implement createFile
    throw "not implemented";
  }

  @override
  Future deleteFile(String url) {
    // TODO: implement deleteFile
    throw "not implemented";
  }

  @override
  Future<sciapi.File> getFile(String url) {
    // TODO: implement getFile
    throw "not implemented";
  }

  @override
  sciapi.Directory createTempDirectory() {
    throw "not implemented";
  }
}

//void _handleFileSystemError(html.FileError e, st) {
void _handleFileSystemError(e, st) {

  html.window.alert("Error in library sci_file_browser");

//  var msg = '';
//  switch (e.code) {
//    case html.FileError.QUOTA_EXCEEDED_ERR:
//      msg = 'QUOTA_EXCEEDED_ERR';
//      break;
//    case html.FileError.NOT_FOUND_ERR:
//      msg = 'NOT_FOUND_ERR';
//      break;
//    case html.FileError.SECURITY_ERR:
//      msg = 'SECURITY_ERR';
//      break;
//    case html.FileError.INVALID_MODIFICATION_ERR:
//      msg = 'INVALID_MODIFICATION_ERR';
//      break;
//    case html.FileError.INVALID_STATE_ERR:
//      msg = 'INVALID_STATE_ERR';
//      break;
//    default:
//      msg = 'Unknown Error';
//      break;
//  }
//
//  html.window.alert("Error in library sci_file_browser : $msg");
}

Future<html.FileSystem> _getFileSystem() {
  var complete = new Completer();
  html.window
      .requestFileSystem(1024 * 1024, persistent: true)
      .then((html.FileSystem fs) {
    var _filesystem = fs;
    html.window.navigator.persistentStorage.requestQuota(100 * 1024 * 1024,
        (int received_size) {
      if (received_size < 1024 * 1024) {
        complete.completeError("Wasn't granted enough size!");
      } else {
        complete.complete(_filesystem);
      }
    }, (e) {
      complete.completeError("Error: Wasn't granted enough size!");
    });
  });
  return complete.future;

//  return html.window.requestFileSystem(1024 * 1024, persistent: true).then((html.FileSystem filesystem) {
//    return filesystem;
//
//  }, onError: _handleFileSystemError);
}

class DirectoryImpl extends sciapi.Directory {
  static Logger log = new Logger("DirectoryImpl");

  String _url;

  DirectoryImpl(this._url);

  sciapi.Directory get parent {
    return new DirectoryImpl(sciapi.FileUtil.getParent(_url));
  }

  @override
  Future<sciapi.Directory> create({bool recursive}) {
    log.finest("create : $url");
    return _getFileSystem().then((fs) {
      return fs.root.createDirectory(url);
    }).then((_) {
      return this;
    }, onError: _handleFileSystemError);
  }

  Future<html.DirectoryEntry> _entry() {
    return _getFileSystem().then((fs) {
      return fs.root.getDirectory(url) as html.DirectoryEntry;
    });
  }

  @override
  Future<bool> exists() {
    log.finest("exists : $url");
    return _getFileSystem().then((fs) {
      return fs.root.getDirectory(url).then((_) {
        return true;
      }, onError: (e) {
        return false;
      });
    });
  }

  @override
  bool get isFile => false;

  Future<html.DirectoryReader> _reader() {
    return _entry().then((entry) {
      return entry.createReader();
    });
  }

  @override
  Future<List<sciapi.Entity>> list(
      {bool recursive: false, bool followLinks: true}) {
    log.finest("list : $url");
    return _reader().then((reader) {
      return reader.readEntries().then((list_entries) {
        return list_entries.map((entry) {
          if (entry.isDirectory)
            return (new DirectoryImpl(entry.fullPath));
          else
            return (new FileImpl(entry.fullPath));
        }).toList();
      });
    });
  }

  @override
  String get url => _url;

  @override
  sciapi.File createFile(String url) {
    if (url.startsWith('/'))
      return new FileImpl(url);
    else
      return new FileImpl(sciapi.File.compose(this.url, url));
  }

  @override
  sciapi.Directory createDirectory(String url) {
    if (url.startsWith('/'))
      return new DirectoryImpl(url);
    else
      return new DirectoryImpl(sciapi.File.compose(this.url, url));
  }

  @override
  Future delete({bool recursive: true}) {
    return this._entry().then((entry) {
      return entry.removeRecursively();
    });
  }

  @override
  Future rename(String url) {
    throw "not implemented";
  }
}

class FileImpl extends sciapi.File {
  static final Logger log = new Logger("FileImpl");
  String _url;
  html.File __file;

  FileImpl.fromFile(this.__file) {
    _url = __file.relativePath;
  }

  FileImpl(this._url);

  sciapi.Directory get parent {
    return new DirectoryImpl(sciapi.FileUtil.getParent(_url));
  }

  Future append2(td.Uint8List bytes) {
    log.finest("append $url : bytes length ${bytes.length}");
    return this.exists().then((b) {
      if (!b) return this._createFileEntry();
    }).then((_) {
      return _createWriter().then((wr) {
        wr.seek(wr.length);
        var blob = new html.Blob([bytes]); //,, "application/octet-binary"
        wr.write(blob);
        return wr.onWriteEnd.first;
      });
    });
  }

  Future delete() {
    log.finest("delete $url");
    return this.exists().then((b) {
      if (b)
        return this._fileEntry().then((fe) {
          return fe.remove();
        });
    });
  }

  Future<html.FileWriter> _createWriter() {
    return this._fileEntry().then((fe) {
      return fe.createWriter();
    });
  }

  @override
  Future<int> length() {
//    log.finest("length $url");
    return _file().then((f) => f.size);
  }

  Future<html.FileEntry> _fileEntry() {
    return _getFileSystem().then((fs) {
      return fs.root.getFile(url) as html.FileEntry;
    });
  }

  Future<html.FileEntry> _createFileEntry() {
    return _getFileSystem().then((fs) {
      return fs.root.createFile(url) as html.FileEntry;
    });
  }

  Future<html.File> _file() {
    if (__file != null) return new Future.value(__file);
    return _fileEntry().then((entry) {
      return entry.file().then((f) {
        __file = f;
        return __file;
      });
    });
  }

  @override
  Future<sciapi.ReadAccessFile> open() {
    log.finest("open $url");
    var r = new ReadAccessFileImpl(this);
    return r._open();
  }

  @override
  String get url => _url;

  @override
  Future<bool> exists() {
//    log.finest("exists $url");
    return _getFileSystem().then((fs) {
      return fs.root.getFile(url).then((_) {
        return true;
      }, onError: (e) {
        return false;
      });
    });
  }

  @override
  bool get isFile => true;

  @override
  Future<Stream<List<int>>> stream() {
    return this._file().then((file) {
      return new FileStream(file).stream;
    });
  }

  @override
  sciapi.FileSink openWrite({sciapi.FileMode mode: sciapi.APPEND}) {
    return new FileSinkImpl(this);
  }
}

class FileSinkImpl extends sciapi.FileSink {
  FileImpl _file;
  scilist.Uint8List _storage;

  FileSinkImpl(this._file) {
    _storage = new scilist.Uint8List.reserv(4096);
  }

  @override
  void add(td.Uint8List bytes) {
    _storage.addAll(bytes);
//    _file.append2(bytes);
  }

  @override
  Future close() {
    td.Uint8List bytes = new td.Uint8List.fromList(_storage);
    _storage = null;
    _file.append2(bytes);
    return new Future.value();
  }

  @override
  Future flush() {
    return new Future.value();
  }
}

class ReadAccessFileImpl implements sciapi.ReadAccessFile {
  static final log = new Logger("ReadAccessFileImpl");

  FileImpl _file;
  int _position;

  ReadAccessFileImpl(this._file) {
    _position = 0;
  }

  Future<ReadAccessFileImpl> _open() {
    return new Future.value(this);
  }

  @override
  Future<int> length() => _file.length();

  @override
  Future<int> readInto(List<int> buffer, [int start, int end]) {
    var completer = new Completer();

    this._file._file().then((html.File htmlfile) {
      if (start == null) start = 0;
      if (end == null) end = buffer.length;

      int desiredLen = end - start;
      int lenToEnd = htmlfile.size - this._position;
      int len = math.min(desiredLen, lenToEnd);

      if (log.level >= Level.WARNING && len != desiredLen) {
        log.warning(
            "partial read : $desiredLen bytes requested but only $len bytes are available");
      }

      var _file_reader = new html.FileReader();

      _file_reader.onLoadEnd.listen((evt) {
        if (!completer.isCompleted) {
          var uint8buffer = _file_reader.result as List<int>;

          var minLen = math.min(len, uint8buffer.length);

          if (len != minLen) {
            completer.completeError(
                "failed to read requested length : read $minLen bytes expected to read $len bytes");
          } else {
            buffer.setRange(start, start + minLen, uint8buffer);
            completer.complete(minLen);
          }
        }
      }, onError: completer.completeError);

      _file_reader.readAsArrayBuffer(
          htmlfile.slice(this._position, this._position + len));
    }, onError: completer.completeError);

    return completer.future;
  }

  @override
  Future<sciapi.ReadAccessFile> setPosition(int position) {
    return this._file.length().then((len) {
      if (position >= len)
        throw "$this set wrong position $position it is larger then file size $len";
      return new Future.sync(() {
        _position = position;
        return this;
      });
    });
  }

  @override
  Future close() {
    return new Future.value();
  }
}

class FileStream {
  static final Logger log = new Logger("FileStream");
  static const int DEFAULT_CHUNCK_SIZE = 1024 * 1000 * 5;

  html.File _file;
  int _chunkSize;
  int _chunk = 0;

  StreamController<List<int>> _controller;
  Stream<List<int>> get stream => _controller.stream;

  FileStream(this._file, {int chunkSize: DEFAULT_CHUNCK_SIZE}) {
    log.finest(
        "constructor : file ${_file.relativePath} chunkSize : $chunkSize");
    this._chunkSize = chunkSize;
    _controller = new StreamController<List<int>>(
        onListen: _onListen,
        onPause: _onPause,
        onResume: _onResume,
        onCancel: _onCancel);
  }

  _onListen() {
    log.finest("_onListen");
    _doRead();
  }

  _onPause() {
    log.finest("_onPause");
  }

  _onResume() {
    log.finest("_onResume");
    _doRead();
  }

  _onCancel() {
    log.finest("_onCancel");
  }

  _close() {
    log.finest("_close");
    if (_controller.isClosed) return;
    _controller.close();

//          _file.close();
  }

  Future _readFuture;

  void _doRead() {
    if (_readFuture == null) {
      _readFuture = _read().catchError(_error);
    } else {
      _readFuture.then((_) {
        _readFuture = _read().catchError(_error);
      });
    }
  }

  Future _read() {
    return new Future.sync(() {
      log.finest(
          "_read : _controller.isPaused : ${_controller.isPaused}  _controller.isClosed : ${ _controller.isClosed} ");
      if (_controller.isPaused || _controller.isClosed) return true;

      int start = _chunk * _chunkSize;
      log.finest("_read : start $start _file.size : ${_file.size} ");
      if (start > _file.size) {
        log.finest("_read start > _file.size close controller");
        _close();
        return true;
      }

      int end =
          start + _chunkSize >= _file.size ? _file.size : start + _chunkSize;
      var len = end - start;

      if (len == 0) {
        log.finest("_read len $len : closing the stream");
        _close();
        return true;
      }

      log.finest("_read start reading at $start to $end");

      html.FileReader _file_reader = new html.FileReader();

//      var _blob = _file.slice(start, end);
      Future onLoadFuture =
          _file_reader.onLoad.first.then((html.ProgressEvent e) {
        if (_file_reader.readyState != html.FileReader.DONE) {
          throw ("e.target.readyState != html.FileReader.DONE");
        } else {
          if (!(_controller.isClosed || _controller.isClosed)) {
            td.Uint8List buffer = _file_reader.result;
            if (len != buffer.length) {
              throw ("$this failed to read chunck at $start to $end : buffer length : ${buffer.length} expected $len");
            } else {
              _controller.add(buffer.sublist(0));
              _chunk++;
              Timer.run(_doRead);
            }
          }
        }
//        _blob.close();
      });
      _file_reader.readAsArrayBuffer(_file.slice(start, end));
      return onLoadFuture;
    });
  }

//  void _fileReaderError(e) {
//    log.warning("_fileReaderError", e);
//    _controller.addError(e);
//    _close();
//  }

  void _error(e, st) {
    log.warning("_error", e, st);
    _controller.addError(e);
    _close();
  }
}
