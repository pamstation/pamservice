library sci_file_memory;

import 'dart:async';
import 'dart:typed_data' as td;
import 'dart:math';

import 'package:sci_util/src/util/list/list.dart' as scilist;

import '../file.dart' as sciapi;

class FileRepositoryImpl implements sciapi.FileRepository {
  @override
  sciapi.Directory createDirectory(String url) {
    return new DirectoryImpl(url);
  }

  @override
  sciapi.File createFile(String url) {
    // TODO: implement createFile
    throw "not implemented";
  }

  @override
  Future deleteFile(String url) {
    // TODO: implement deleteFile
    throw "not implemented";
  }

  @override
  Future<sciapi.File> getFile(String url) {
    // TODO: implement getFile.
    throw "not implemented";
  }

  @override
  sciapi.Directory createTempDirectory() {
    throw "not implemented";
  }
}

class FileSystem {
  static FileSystem _FS;
  static FileSystem get FS {
    if (_FS == null) _FS = new FileSystem._();
    return _FS;
  }

  Map<String, sciapi.Entity> _entries;
//  factory FileSystem(){
//    if (FS == null) FS = new FileSystem._();
//    return FS;
//  }
  FileSystem._() {
    _entries = {};
  }

  void _addEntry(sciapi.Entity entity) {
     _entries[entity.url] = entity;
  }

  void _removeEntry(sciapi.Entity entity) {
    if (_entries.containsKey(entity.url)) {
      _entries.remove(entity.url);
    }

    if (!entity.isFile) {
      new List.from(_entries.values)
          .where((k) => k.url.startsWith(entity.url))
          .forEach((childEntity) => this._removeEntry(childEntity));
    }
  }

  bool existsDirectory(String url) {
    sciapi.Entity entry = _entries[url];
    return entry != null && !entry.isFile;
  }

  bool existsFile(String url) {
    sciapi.Entity entry = _entries[url];
    return entry != null && entry.isFile;
  }

  FileImpl getFile(String url) {
    return _entries[url];
  }

  Future<List<sciapi.Entity>> list(DirectoryImpl dir,
      {bool recursive: false, bool followLinks: true}) {
    return new Future.sync(() {
      var list = <sciapi.Entity>[];
      _entries.keys.forEach((k) {
        if (k.startsWith(dir.url)) {
          if (recursive) {
            list.add(_entries[k]);
          } else {
            var ks = k.substring(dir.url.length);
            if (ks.startsWith("/")) ks = ks.substring(1);
            if (!ks.contains("/")) {
              list.add(_entries[k]);
            }
          }
        }
      });
      return list;
    });
  }
}

class DirectoryImpl extends sciapi.Directory {
  String _url;

  DirectoryImpl(this._url);

  sciapi.Directory get parent {
    return new DirectoryImpl(sciapi.FileUtil.getParent(_url));
  }

  @override
  Future rename(String url) {
    throw "not implemented";
  }

  @override
  Future<sciapi.Directory> create({bool recursive}) {
    return new Future.sync(() {
      FileSystem.FS._addEntry(this);
      return this;
    });
  }

  @override
  Future<bool> exists() {
    return new Future.value(FileSystem.FS.existsDirectory(this.url));
  }

  @override
  bool get isFile => false;

  @override
  Future<List<sciapi.Entity>> list(
      {bool recursive: false, bool followLinks: true}) {
    return FileSystem.FS.list(this, recursive: recursive);
  }

  @override
  String get url => _url;

  @override
  sciapi.File createFile(String url) {
    return new FileImpl(url);
  }

  @override
  sciapi.Directory createDirectory(String url) {
    return new DirectoryImpl(url);
  }

  @override
  Future delete({bool recursive: true}) {
    return new Future.sync(() {
      FileSystem.FS._removeEntry(this);
    });
  }

  String toString() {
    return "${this.runtimeType} $url";
  }
}

class FileImpl extends sciapi.File {
  String _url;
  scilist.Uint8List _storage;

  sciapi.Directory get parent {
    return new DirectoryImpl(sciapi.FileUtil.getParent(_url));
  }

  factory FileImpl(String url) {
    if (FileSystem.FS.existsFile(url)) {
      return FileSystem.FS.getFile(url);
    } else {
      return new FileImpl._(url);
    }
  }

  FileImpl._(this._url) {
    _storage = new scilist.Uint8List.reserv(4096);
  }

  String toString() {
    return "${this.runtimeType} $url";
  }

  void _ensureExists() {
    if (FileSystem.FS.existsFile(this.url)) {
      var f = FileSystem.FS.getFile(url);
      if (f == this) return;
      this._storage = f._storage;
      return;
    } else
      FileSystem.FS._addEntry(this);
  }

//  @override
  Future append2(td.Uint8List bytes) {
    _ensureExists();
    return new Future.sync(() => _storage.addAll(bytes));
  }

  Future replace(td.Uint8List bytes) {
    _ensureExists();
    _storage.clear();
    return new Future.sync(() => _storage.addAll(bytes));
  }

  @override
  String get url => _url;

  @override
  Future<int> length() {
    return new Future.value(_storage.length);
  }

  @override
  Future<bool> exists() {
    return new Future.value(FileSystem.FS.existsFile(this.url));
  }

  @override
  Future<sciapi.ReadAccessFile> open() {
    _ensureExists();
    return new Future.value(new ReadAccessFileImpl(this));
  }

  @override
  bool get isFile => true;

  @override
  Future<Stream<List<int>>> stream() {
    return new Future.value(new Stream.fromIterable([this._storage]));
  }

  @override
  Future delete() {
    throw "not yet impl";
  }

  sciapi.FileSink openWrite({sciapi.FileMode mode: sciapi.APPEND}) =>
      new FileSinkImpl(this, mode: mode);
}

class FileSinkImpl extends sciapi.FileSink {
  FileImpl _file;
  scilist.Uint8List _storage;
  sciapi.FileMode mode;

  FileSinkImpl(this._file, {this.mode}) {
    _storage = new scilist.Uint8List.reserv(4096);
  }

  @override
  void add(td.Uint8List bytes) {
    _storage.addAll(bytes);
//    _file.append2(bytes);
  }


  @override
  Future close() {
    td.Uint8List bytes = new td.Uint8List.fromList(_storage);
    _storage = null;
    if (this.mode == sciapi.FileMode.APPEND) {
      _file.append2(bytes);
    } else {
      _file.replace(bytes);
    }

    return new Future.value();
  }

  @override
  Future flush() {
    return new Future.value();
  }
}

class ReadAccessFileImpl implements sciapi.ReadAccessFile {
  FileImpl _file;
  int _position;

  ReadAccessFileImpl(this._file) {
    _position = 0;
  }

  @override
  Future<int> length() => new Future.value(this._file._storage.length);

  @override
  Future<int> readInto(List<int> buffer, [int start, int end]) {
    return new Future.sync(() {
      if (buffer is! List ||
          (start != null && start is! int) ||
          (end != null && end is! int)) {
        throw new ArgumentError();
      }
      if (start == null) start = 0;
      if (end == null) end = buffer.length;
      int length = end - start;
      length = min(length, this._file._storage.length - _position);

      buffer.setRange(start, start + length, this._file._storage, _position);

      return length;
    });
  }

  @override
  Future<sciapi.ReadAccessFile> setPosition(int position) {
    return new Future.sync(() {
      _position = position;
      return this;
    });
  }

  @override
  Future close() {
    return new Future.value();
  }
//
//  @override
//  int positionSync() => _position;
}
