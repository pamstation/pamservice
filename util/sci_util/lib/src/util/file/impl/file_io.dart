library sci_file_io;

import 'dart:async';
import 'dart:typed_data' as td;
import 'dart:io' as io;
import 'package:uuid/uuid.dart' as uuid;

import '../file.dart' as sciapi;
import 'package:logging/logging.dart';

class FileRepositoryImpl implements sciapi.FileRepository {
  static Logger logger = new Logger("FileRepositoryImpl");

  String directory;

  FileRepositoryImpl([this.directory]) {
    if (this.directory == null) {
      this.directory = io.Directory.systemTemp.path;
    } else {
      new io.Directory(this.directory).createSync(recursive: true);
    }
  }

  @override
  sciapi.File createFile(String url) {
    if (url.startsWith(directory)) {
      return new FileImpl(url);
    } else
      return new FileImpl(sciapi.File.compose(directory, url));
  }

  @override
  sciapi.Directory createTempDirectory() {
    var id = new uuid.Uuid().v4().toString();
    return createDirectory('tmp/${id}');
  }

  @override
  sciapi.Directory createDirectory(String url) {
    if (url.startsWith(directory)) {
      return new DirectoryImpl(url);
    } else
      return new DirectoryImpl(sciapi.File.compose(directory, url));
  }

  @override
  Future deleteFile(String url) {
    throw "$this deleteFile not yet implemented";
  }

  @override
  Future<sciapi.File> getFile(String url) {
    throw "$this getFile not yet implemented";
  }
}

class DirectoryImpl extends sciapi.Directory {
  String _url;
  io.Directory _io_dir;

  DirectoryImpl(this._url) {
    _io_dir = new io.Directory(_url);
  }

  sciapi.Directory get parent {
    return new DirectoryImpl(sciapi.FileUtil.getParent(_url));
  }

  @override
  Future rename(String url) {
    return _io_dir.rename(url);
  }

  @override
  Future<sciapi.Directory> create({bool recursive: false}) {
    return _io_dir.create(recursive: recursive).then((_) {
      return this;
    });
  }

  @override
  sciapi.Directory createDirectory(String url) {
    if (url.startsWith('/'))
      return new DirectoryImpl(url);
    else
      return new DirectoryImpl(sciapi.File.compose(this.url, url));
  }

  @override
  sciapi.File createFile(String url) {
    if (url.startsWith('/'))
      return new FileImpl(url);
    else
      return new FileImpl(sciapi.File.compose(this.url, url));
  }

  @override
  Future delete({bool recursive: true}) {
    return _io_dir.delete(recursive: recursive);
  }

  @override
  Future<bool> exists() {
    return _io_dir.exists();
  }

  @override
  bool get isFile => false;

  @override
  Future<List<sciapi.Entity>> list(
      {bool recursive: false, bool followLinks: true}) {
    return this
        ._io_dir
        .list(recursive: recursive, followLinks: followLinks)
        .toList()
        .then((List<io.FileSystemEntity> list) {
      return list.map((fe) {
        if (fe is io.File) {
          return new FileImpl(fe.path);
        } else {
          return new DirectoryImpl(fe.path);
        }
      }).toList();
    });
  }

  @override
  String get url => _url;
}

class FileImpl extends sciapi.File {
  String _url;
  io.File _io_file;

  FileImpl(this._url) {
    _io_file = new io.File(_url);
    _io_file;
  }

  sciapi.Directory get parent {
    return new DirectoryImpl(sciapi.FileUtil.getParent(_url));
  }

  Future<sciapi.FileStat> fileStats() async {
    var io_stat = await _io_file.stat();
    return new sciapi.FileStat(io_stat.size, io_stat.modified);
  }

  Future append2(td.Uint8List bytes) {
    return _io_file.writeAsBytes(bytes, mode: io.FileMode.APPEND, flush: true);
  }

  @override
  Future<int> length() {
    return _io_file.length();
  }

  @override
  Future<sciapi.ReadAccessFile> open() {
    var r = new ReadAccessFileIo(this);
    return r._open();
  }

  @override
  String get url => _url;

  @override
  Future<bool> exists() {
    return _io_file.exists();
  }

  @override
  bool get isFile => true;

  @override
  Future<Stream<List<int>>> stream() async {
    return _io_file.openRead();
  }

  @override
  Future<List<int>> readAsBytes() {
    return _io_file.readAsBytes();
  }

  @override
  Future delete() {
    return _io_file.delete();
  }

  @override
  sciapi.FileSink openWrite({sciapi.FileMode mode: sciapi.APPEND}) {
    return new FileSinkImpl(this, mode: mode);
  }

  @override
  String toString() {
    return '${this.runtimeType}(${this._url})';
  }
}

class FileSinkImpl extends sciapi.FileSink {
  static Logger logger = new Logger('FileSinkImpl');
  FileImpl _file;

  io.IOSink _io_sink;
  sciapi.FileMode _mode;

  FileSinkImpl(this._file, {sciapi.FileMode mode: sciapi.APPEND}) {
    _mode = mode;
    _io_sink = this._file._io_file.openWrite(mode: _ioMode);
  }

  io.FileMode get _ioMode {
    if (_mode == sciapi.APPEND) return io.APPEND;
    if (_mode == sciapi.READ) return io.READ;
    if (_mode == sciapi.WRITE) return io.WRITE;
    if (_mode == sciapi.WRITE_ONLY) return io.WRITE_ONLY;
    if (_mode == sciapi.WRITE_ONLY_APPEND) return io.WRITE_ONLY_APPEND;
    throw '$this unknown mode : ${_mode}';
  }

  @override
  void add(td.Uint8List bytes) => _io_sink.add(bytes);

  Future addStream(Stream<List<int>> stream) async {
//    var watch = new Stopwatch()..start();
    var result = await _io_sink.addStream(stream);

//    logger.fine(
//        '${this._file.url} addStream : elapsedMilliseconds ${watch.elapsedMilliseconds}');
    return result;
  }

  @override
  Future close() async {
//    var watch = new Stopwatch()..start();
    var result = await _io_sink.close();

//    logger.fine(
//        '${this._file.url} close : elapsedMilliseconds ${watch.elapsedMilliseconds}');
    return result;
  }

  @override
  Future flush() async {
//    var watch = new Stopwatch()..start();
    var result = await _io_sink.flush();
//    logger.fine(
//        '${this._file.url} flush : elapsedMilliseconds ${watch.elapsedMilliseconds}');
    return result;
  }

  String toString() {
    return '${this.runtimeType}(${this._file.url})';
  }
}

class ReadAccessFileIo implements sciapi.ReadAccessFile {
  FileImpl _file;
  io.RandomAccessFile _raf;

  ReadAccessFileIo(this._file);

  Future<ReadAccessFileIo> _open() async {
    _raf = await _file._io_file.open(mode: io.FileMode.READ);
    return this;
  }

  @override
  Future<int> length() => _raf.length();

  int lengthSync() => _raf.lengthSync();

  @override
  Future<int> readInto(List<int> buffer, [int start = 0, int end]) {
//    print(
//        '$this readInto position ${_raf.positionSync()} -- buffer.length ${buffer.length}');
    return _raf.readInto(buffer, start, end);
  }

  @override
  Future<sciapi.ReadAccessFile> setPosition(int position) {
//    print('$this setPosition $position');
    return _raf.setPosition(position).then((_) => this);
  }

  @override
  Future close() {
    return _raf.close();
  }

  String toString() {
    return '${this.runtimeType}(${_file.url})';
  }
}
