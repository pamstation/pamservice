library sci_util_type;

import 'dart:typed_data' as td;

const String UINT8STRING = "uint8string";
const String STRING = "string";


const String UINT8 = "uint8";
const String UINT16 = "uint16";
const String UINT32 = "uint32";
const String UINT64 = "uint64";
const String INT8 = "int8";
const String INT16 = "int16";
const String INT32 = "int32";
const String INT64 = "int64";
const String DOUBLE = "double";
const String BITMAP = "bitmap";

const List<String> TYPES = const [
  STRING,

  UINT8,
  UINT16,
  UINT32,
  UINT64,
  INT8,
  INT16,
  INT32,
  INT64,
  DOUBLE,
  BITMAP
];

void validateType(String type) {
  if (!TYPES.contains(type)) throw new ArgumentError.value(type, 'bad type');
}

bool isString(String type) => STRING == type || UINT8STRING == type;

bool isUint32(String type) => UINT32 == type;
bool isUint64(String type) => UINT64 == type;
bool isUint16(String type) => UINT16 == type;
bool isUint8(String type) => UINT8 == type;

bool isDouble(String type) => DOUBLE == type;
bool isInt32(String type) => INT32 == type;
bool isInt16(String type) => INT16 == type;
bool isInt8(String type) => INT8 == type;

String typeForTypedData(td.TypedData list) {
  if (list is td.Uint32List) {
    return UINT32;
  } else if (list is td.Uint16List) {
    return UINT16;
  } else if (list is td.Uint8List) {
    return UINT8;
  } else if (list is td.Float64List) {
    return DOUBLE;
  } else if (list is td.Int64List) {
    return INT64;
  } else if (list is td.Uint64List) {
    return UINT64;
  } else if (list is td.Int32List) {
    return INT32;
  } else {
    throw 'typeForTypedData : unknown type data : ${list.runtimeType}';
  }
}

String mergeType(String type1, String type2) {
  if (type2 != null && type2 == type1) return type2;

  if (isDouble(type1) || isDouble(type2)) {
    if (isDouble(type1) && isDouble(type2))
      return DOUBLE;
    else
      throw 'merge type conflict : $type1 / $type2';
  }

  if (isString(type1) || isString(type2)) {
    if (isString(type1) && isString(type2))
      return UINT8STRING;
    else
      throw 'merge type conflict : $type1 / $type2';
  }

  if (isUint32(type1) || isUint32(type2)) return UINT32;
  if (isUint16(type1) || isUint16(type2)) return UINT16;
  if (isUint8(type1) && isUint8(type2)) return UINT8;

  throw 'merge type conflict : $type1 / $type2';
}

//String getType(td.TypedData data) {
//  if (data is td.Uint8List) return UINT8;
//  if (data is td.Uint16List) return UINT16;
//  if (data is td.Uint32List) return UINT32;
//  if (data is td.Float64List) return FLOAT;
//  throw 'unknown data type';
//}

String formatByteStorage(int bytes) {
  if (bytes < 1000) {
    return '${bytes} B';
  } else if (bytes < 1000000) {
    return '${(bytes/1000).toStringAsFixed(2)} kB';
  } else {
    return '${(bytes/1000000).toStringAsFixed(2)} MB';
  }
}