part of sci.detect.encoding;

abstract class CheckEncoding {
  int encoding;
  void process(List<int> pBuffer);
  void close();
  bool get hasEncoding => encoding != null;
}

class _CheckUTF8 extends CheckEncoding {
  bool null_suggests_binary_ = true;
  bool only_saw_ascii_range = true;

  int more_chars;

  _CheckUTF8(this.null_suggests_binary_);

  void process(List<int> pBuffer) {
    // utf8 Valid sequences
    // 0xxxxxxx  ascii
    // 110xxxxx 10xxxxxx  2-byte
    // 1110xxxx 10xxxxxx 10xxxxxx  3-byte
    // 11110xxx 10xxxxxx 10xxxxxx 10xxxxxx  4-byte
    //
    // Width in utf8
    // Decimal		Width
    // 0-127		1 byte
    // 194-223		2 bytes
    // 224-239		3 bytes
    // 240-244		4 bytes
    //
    // Subsequent chars are in the range 128-191

    if (encoding != null) return;

    var size = pBuffer.length;

    only_saw_ascii_range = true;
    int pos = 0;

    while (pos < size) {
      int ch = pBuffer[pos++];

      if (ch == 0 && null_suggests_binary_) {
        encoding = DetectTextEncoding.None;
        return;
      } else if (ch <= 127) {
        // 1 byte
        more_chars = 0;
      } else if (ch >= 194 && ch <= 223) {
        // 2 Byte
        more_chars = 1;
      } else if (ch >= 224 && ch <= 239) {
        // 3 Byte
        more_chars = 2;
      } else if (ch >= 240 && ch <= 244) {
        // 4 Byte
        more_chars = 3;
      } else {
        encoding = DetectTextEncoding.None; // Not utf8
        return;
      }

      // Check secondary chars are in range if we are expecting any
      while (more_chars > 0 && pos < size) {
        only_saw_ascii_range = false; // Seen non-ascii chars now

        ch = pBuffer[pos++];
        if (ch < 128 || ch > 191) {
          encoding = DetectTextEncoding.None;
          return;
        } // Not utf8

        --more_chars;
      }
    }
  }

  void close() {
    if (encoding != null) return;
    // If we get to here then only valid UTF-8 sequences have been processed

    // If we only saw chars in the range 0-127 then we can't assume utf8 (the caller will need to decide)
    if (only_saw_ascii_range) {
      encoding = DetectTextEncoding.ascii;
      return;
    } else {
      encoding = DetectTextEncoding.UTF8_NOBOM;
      return;
    }
  }
}

class _CheckUTF16NewlineChars extends CheckEncoding {
  int le_control_chars = 0;
  int be_control_chars = 0;

  void process(List<int> pBuffer) {
    if (encoding != null) return;

    var size = pBuffer.length;

//    if (size < 2) {
//      encoding = DetectTextEncoding.None;
//      return;
//    }

// Reduce size by 1 so we don't need to worry about bounds checking for pairs of bytes
    size--;

    int ch1, ch2;

    int pos = 0;
    while (pos < size) {
      ch1 = pBuffer[pos++];
      ch2 = pBuffer[pos++];

      if (ch1 == 0) {
        if (ch2 == 0x0a || ch2 == 0x0d) ++be_control_chars;
      } else if (ch2 == 0) {
        if (ch1 == 0x0a || ch1 == 0x0d) ++le_control_chars;
      }

// If we are getting both LE and BE control chars then this file is not utf16
      if (le_control_chars > 0 && be_control_chars > 0) {
        encoding = DetectTextEncoding.None;
        return;
      }
    }
  }

  void close() {
    if (le_control_chars > 0) {
      encoding = DetectTextEncoding.UTF16_LE_NOBOM;
    } else if (be_control_chars > 0) {
      encoding = DetectTextEncoding.UTF16_BE_NOBOM;
    } else {
      encoding = DetectTextEncoding.None;
    }
  }
}

class _CheckUTF16ASCII extends CheckEncoding {
  num utf16_expected_null_percent_ = 70;
  num utf16_unexpected_null_percent_ = 10;

  int num_odd_nulls = 0;
  int num_even_nulls = 0;

  int accSize = 0;

  void process(List<int> pBuffer) {
    if (encoding != null) return;

    var size = pBuffer.length;
    accSize += size;

// Get even nulls
    int pos = 0;
    while (pos < size) {
      if (pBuffer[pos] == 0) num_even_nulls++;

      pos += 2;
    }

// Get odd nulls
    pos = 1;
    while (pos < size) {
      if (pBuffer[pos] == 0) num_odd_nulls++;

      pos += 2;
    }
  }

  void close() {
    if (encoding != null) return;
    double even_null_threshold = (num_even_nulls * 2.0) / accSize;
    double odd_null_threshold = (num_odd_nulls * 2.0) / accSize;
    double expected_null_threshold = utf16_expected_null_percent_ / 100.0;
    double unexpected_null_threshold = utf16_unexpected_null_percent_ / 100.0;

// Lots of odd nulls, low number of even nulls
    if (even_null_threshold < unexpected_null_threshold &&
        odd_null_threshold > expected_null_threshold) {
      encoding = DetectTextEncoding.UTF16_LE_NOBOM;
      return;
    }

// Lots of even nulls, low number of odd nulls
    if (odd_null_threshold < unexpected_null_threshold &&
        even_null_threshold > expected_null_threshold) {
      encoding = DetectTextEncoding.UTF16_BE_NOBOM;
      return;
    }

// Don't know
    encoding = DetectTextEncoding.None;
  }
}

class _DoesContainNulls {
  bool flag = false;

  void process(List<int> pBuffer) {
    if (flag) return;
    var size = pBuffer.length;

    int pos = 0;
    while (pos < size) {
      if (pBuffer[pos++] == 0) {
        flag = true;
        return;
      }
    }
  }

}
