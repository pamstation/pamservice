library sci.detect.encoding;
// https://github.com/AutoIt/text-encoding-detect/blob/master/TextEncodingDetect-C%2B%2B/TextEncodingDetect/text_encoding_detect.cpp

import 'dart:async';
import 'dart:convert';

part 'detect_encoding_acc.dart';

class DetectStreamEncoding {
  Future<int> detect(Stream<List<int>> stream) {

    utf8;

    var completer = new Completer();
    var decode = new DetectTextEncoding();
    StreamSubscription sub;
    bool checkBOM = true;
    sub = stream.listen(
        (pBuffer) {
          decode.process(pBuffer, checkBOM:checkBOM);
          checkBOM = false;
          if (decode.hasEncoding) {
            sub.cancel();
            if (!completer.isCompleted) completer.complete(decode.encoding);
          }
        },
        onError: completer.completeError,
        onDone: () {
          decode.close();
          completer.complete(decode.encoding);
        });

    return completer.future;
  }

//  Stream<List<int>>
}

class DetectTextEncoding {
  static int None = 0; // Unknown or binary
  static int ANSI = 1; // 0-255
  static int ascii = 2; // 0-127
  static int UTF8_BOM = 3; // utf8 with BOM
  static int UTF8_NOBOM = 4; // utf8 without BOM
  static int UTF16_LE_BOM = 5; // UTF16 LE with BOM
  static int UTF16_LE_NOBOM = 6; // UTF16 LE without BOM
  static int UTF16_BE_BOM = 7; // UTF16-BE with BOM
  static int UTF16_BE_NOBOM = 8; // UTF16-BE without BOM

  List<int> utf16_bom_le_;
  List<int> utf16_bom_be_;
  List<int> utf8_bom_;

  _CheckUTF8 _checkUTF8;
  _CheckUTF16NewlineChars _checkUTF16NewlineChars;
  _CheckUTF16ASCII _checkUTF16ASCII;
  _DoesContainNulls _doesContainNulls;

  int encoding;

  bool get hasEncoding => encoding != null;

  DetectTextEncoding() {
    utf16_bom_le_ = [0xFF, 0xFE];
    utf16_bom_be_ = [0xFE, 0xFF];
    utf8_bom_ = [0xEF, 0xBB, 0xBF];

    _checkUTF8 = new _CheckUTF8(true);
    _checkUTF16NewlineChars = new _CheckUTF16NewlineChars();
    _checkUTF16ASCII = new _CheckUTF16ASCII();
    _doesContainNulls = new _DoesContainNulls();
  }

  int GetBOMLengthFromEncodingMode(int encoding) {
    int length = 0;

    if (encoding == UTF16_BE_BOM || encoding == UTF16_LE_BOM)
      length = 2;
    else if (encoding == UTF8_BOM) length = 3;

    return length;
  }

  int CheckBOM(List<int> pBuffer) {
    var size = pBuffer.length;
    // Check for BOM
    if (size >= 2 &&
        pBuffer[0] == utf16_bom_le_[0] &&
        pBuffer[1] == utf16_bom_le_[1]) {
      return UTF16_LE_BOM;
    } else if (size >= 2 &&
        pBuffer[0] == utf16_bom_be_[0] &&
        pBuffer[1] == utf16_bom_be_[1]) {
      return UTF16_BE_BOM;
    } else if (size >= 3 &&
        pBuffer[0] == utf8_bom_[0] &&
        pBuffer[1] == utf8_bom_[1] &&
        pBuffer[2] == utf8_bom_[2]) {
      return UTF8_BOM;
    } else {
      return None;
    }
  }

  void process(List<int> pBuffer, {bool checkBOM: true}) {
    if (encoding != null) return;

    // First check if we have a BOM and return that if so

    if (checkBOM) {
      var encoding = CheckBOM(pBuffer);
      if (encoding != None) {
        this.encoding = encoding;
        return;
      }
    }

    // Now check for valid utf8
    _checkUTF8..process(pBuffer);
    if (_checkUTF8.hasEncoding && _checkUTF8.encoding != None) {
      this.encoding = _checkUTF8.encoding;
      return;
    }

    // Now try UTF16

    _checkUTF16NewlineChars..process(pBuffer);
    if (_checkUTF16NewlineChars.hasEncoding &&
        _checkUTF16NewlineChars.encoding != None) {
      this.encoding = _checkUTF16NewlineChars.encoding;
      return;
    }

    _checkUTF16ASCII..process(pBuffer);
    if (_checkUTF16ASCII.hasEncoding && _checkUTF16ASCII.encoding != None) {
      this.encoding = _checkUTF16ASCII.encoding;
      return;
    }

    // ANSI or None (binary) then
    _doesContainNulls.process(pBuffer);
  }

  void close() {
    // Now check for valid utf8
    _checkUTF8..close();
    if (_checkUTF8.hasEncoding && _checkUTF8.encoding != None) {
      this.encoding = _checkUTF8.encoding;
      return;
    }

    // Now try UTF16

    _checkUTF16NewlineChars..close();
    if (_checkUTF16NewlineChars.hasEncoding &&
        _checkUTF16NewlineChars.encoding != None) {
      this.encoding = _checkUTF16NewlineChars.encoding;
      return;
    }

    _checkUTF16ASCII..close();
    if (_checkUTF16ASCII.hasEncoding && _checkUTF16ASCII.encoding != None) {
      this.encoding = _checkUTF16ASCII.encoding;
      return;
    }

    if (!_doesContainNulls.flag) {
      this.encoding = ANSI;
    } else {
      // Found a null, return based on the preference in null_suggests_binary_
      if (_checkUTF8.null_suggests_binary_)
        this.encoding = None;
      else
        this.encoding = ANSI;
    }
  }

  int detectEncoding(List<int> pBuffer, {bool checkBOM: true}) {
    _checkUTF8 = new _CheckUTF8(true);
    _checkUTF16NewlineChars = new _CheckUTF16NewlineChars();
    _checkUTF16ASCII = new _CheckUTF16ASCII();
    _doesContainNulls = new _DoesContainNulls();

    // First check if we have a BOM and return that if so

    if (checkBOM) {
      var encoding = CheckBOM(pBuffer);
      if (encoding != None) return encoding;
    }

    // Now check for valid utf8
    _checkUTF8
      ..process(pBuffer)
      ..close();
    if (_checkUTF8.hasEncoding && _checkUTF8.encoding != None)
      return _checkUTF8.encoding;

    // Now try UTF16

    _checkUTF16NewlineChars
      ..process(pBuffer)
      ..close();
    if (_checkUTF16NewlineChars.hasEncoding &&
        _checkUTF16NewlineChars.encoding != None)
      return _checkUTF16NewlineChars.encoding;

    _checkUTF16ASCII
      ..process(pBuffer)
      ..close();
    if (_checkUTF16ASCII.hasEncoding && _checkUTF16ASCII.encoding != None)
      return _checkUTF16ASCII.encoding;

    // ANSI or None (binary) then
    _doesContainNulls.process(pBuffer);

    if (!_doesContainNulls.flag)
      return ANSI;
    else {
      // Found a null, return based on the preference in null_suggests_binary_
      if (_checkUTF8.null_suggests_binary_)
        return None;
      else
        return ANSI;
    }
  }
}
