import 'package:path/path.dart' as pathlib;

String longPath(String value) {
  if (value.length < 2) {
    return value;
  }

  var path = value;

  if (value.length >= 2) {
    path = pathlib.join(value.substring(0, 2), path);
  }
  if (value.length >= 4) {
    path = pathlib.join(value.substring(2, 4), path);
  }
  if (value.length >= 6) {
    path = pathlib.join(value.substring(4, 6), path);
  }

  return path;
}
