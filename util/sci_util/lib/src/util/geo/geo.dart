library sci_util_geo;

import 'dart:math' as math;

class Point2D {
  num x;
  num y;

  factory Point2D.fromPoint(Point2D p) {
    return new Point2D(p.x, p.y);
  }

  Point2D.zero() : this(0.0, 0.0);

  Point2D(this.x, this.y);

  Point2D copy() => new Point2D(x, y);

  Rectangle toRectangle() =>
      new Rectangle.fromCorner(this.x, this.x, this.y, this.y);

  Point2D toInt() => new Point2D(this.x.toInt(), this.y.toInt());
  Point2D round() => new Point2D(this.x.round(), this.y.round());

  Point2D.fromJson(Map json) {
    x = json["x"];
    y = json["y"];
  }

  bool get isPositive => x >= 0 && y >= 0;

  Point2D normalized() {
    var n = norm();
    //if (n == 0.0) throw new Exception("divided by zero");
    return new Point2D(x / n, y / n);
  }

  Point2D operator +(Point2D p) {
    return new Point2D(this.x + p.x, this.y + p.y);
  }

  void plus(Point2D o) {
    x += o.x;
    y += o.y;
  }

  void multScalar(double d) {
    x *= d;
    y *= d;
  }

  num dot(Point2D p) {
    return x*p.x + y*p.y;
  }

  Point2D operator *(num p) {
    return new Point2D(this.x * p, this.y * p);
  }

  Point2D operator -(Point2D p) {
    return new Point2D(this.x - p.x, this.y - p.y);
  }

  Point2D divide(Point2D p) {
    return new Point2D(this.x / p.x, this.y / p.y);
  }

  Point2D operator /(num p) {
    return new Point2D(this.x / p, this.y / p);
  }

  num norm() {
    return math.sqrt(math.pow(this.x, 2) + math.pow(this.y, 2));
  }

  String toString() {
    return "${this.runtimeType}($x,$y)";
  }

  Map toJson() {
    return {"x": x, "y": y};
  }

  int get hashCode {
    return x.hashCode + y.hashCode;
  }

  bool operator ==(other) {
    return other is Point2D && other.x == x && other.y == y;
  }
}

//class Point3D implements Point2D {
//  double x;
//  double y;
//  double z;
//  Point3D(num a, num b, num c) {
//    this.x = a.toDouble();
//    this.y = b.toDouble();
//    this.z = c.toDouble();
//  }
//  Point3D.zero() : this(0.0, 0.0, 0.0);
//  bool get isPositive => x >= 0 && y >= 0 && z >= 0;
//
//  Rectangle toRectangle() => null;
//  Point3D toInt() => new Point3D(x.toInt(), y.toInt(), z.toInt());
//  Point3D round() => new Point3D(x.round(), y.round(), z.round());
//
//  Point3D ortho() {
//    Point3D answer;
//    if (x > 0) {
//      if (y > 0) {
//        answer = new Point3D(-y, x, z);
//      } else {
//        answer = new Point3D(-y, x, z);
//      }
//    } else {
//      if (y > 0) {
//        answer = new Point3D(y, -x, z);
//      } else {
//        answer = new Point3D(y, -x, z);
//      }
//    }
//    return answer.normalized();
//  }
//
//  Point3D operator -(Point3D o) {
//    return new Point3D(x - o.x, y - o.y, z - o.z);
//  }
//
//  String toString() {
//    return '$runtimeType($x,$y,$z)';
//  }
//
//  Point3D abs() {
//    return new Point3D(x.abs(), y.abs(), z.abs());
//  }
//
//  void minus(Point3D o) {
//    x -= o.x;
//    y -= o.y;
//    z -= o.z;
//  }
//
//  void plus(Point3D o) {
//    x += o.x;
//    y += o.y;
//    z += o.z;
//  }
//
//  Point3D operator +(Point3D o) {
//    return new Point3D(x + o.x, y + o.y, z + o.z);
//  }
//
//  Point3D divide(Point3D o) {
//    return new Point3D(x / o.x, y / o.y, z / o.z);
//  }
//
//  double operator *(Point3D o) {
//    return (x * o.x) + (y * o.y) + (z * o.z);
//  }
//
//  double norm() {
//    return math.pow((x * x) + (y * y) + (z * z), 0.5);
//  }
//
//  Point3D normalized() {
//    var n = norm();
//    //if (n == 0.0) throw new Exception("divided by zero");
//    return new Point3D(x / n, y / n, z / n);
//  }
//
//  void multScalar(double d) {
//    x *= d;
//    y *= d;
//    z *= d;
//  }
//
//  Point3D copy() {
//    return new Point3D(x, y, z);
//  }
//
//  void copyPoint(Point3D p) {
//    x = p.x;
//    y = p.y;
//    z = p.z;
//  }
//
//  bool isNaN() {
//    return x.isNaN || y.isNaN || z.isNaN;
//  }
//
//  Point3D operator /(num p) {
//    return new Point3D(this.x / p, this.y / p, this.z / p);
//  }
//
//  Object toJson() {
//    return {"x": x, "y": y, "z": z};
//  }
//}

class Rectangle2 implements Rectangle {
  Point2D topLeft;
  Point2D size;

  Rectangle2(this.topLeft, this.size);

  Point2D get bottomRight => new Point2D(xmax, ymax);

  num get xmax => topLeft.x + size.x;
  num get xmin => topLeft.x;
  num get ymax => topLeft.y + size.y;
  num get ymin => topLeft.y;

  num get left => xmin;
  num get right => xmax;
  num get top => ymin;
  num get bottom => ymax;

//  Rectangle2(this.topLeft, this.bottomRight);

  Rectangle2.fromTopLeft(num xmin, num ymin, num width, num height) {
    topLeft = new Point2D(xmin, ymin);
    size = new Point2D(width, height);
  }
  Rectangle2.fromCorner(num xmin, num xmax, num ymin, num ymax) {
    topLeft = new Point2D(xmin, ymin);
    size = new Point2D(xmax - xmin, ymax - ymin);
  }

  String toString() => "${this.runtimeType}($topLeft,$bottomRight)";

  bool containsPoint(Point2D p) => contains(p.x, p.y);

  bool contains(num x, num y) =>
      (x >= xmin && x <= xmax && y >= ymin && y <= ymax);

  bool intersect(Rectangle rec) =>
      _intersect(this, rec) || _intersect(rec, this);

  bool _intersect(Rectangle rec1, Rectangle rec2) {
    return rec1.contains(rec2.xmin, rec2.ymax) ||
        rec1.contains(rec2.xmin, rec2.ymin) ||
        rec1.contains(rec2.xmax, rec2.ymax) ||
        rec1.contains(rec2.xmax, rec2.ymin) ||
        rec1.contains(rec2.center.x, rec2.center.y);
  }

  int get hashCode => topLeft.hashCode + bottomRight.hashCode;

  bool operator ==(other) {
    return other is Rectangle && other.topLeft == topLeft && other.size == size;
  }

  void move(Point2D point) => topLeft.plus(point);

  Map toJson() {
    return {"xmin": xmin, "xmax": xmax, "ymin": ymin, "ymax": ymax};
  }

  void set center(Point2D _center) {
    throw "not implemented";
  }

  Point2D get center => this.topLeft + (this.size / 2);

  num get height => size.y;

  num get width => size.x;

  @override
  Rectangle inset(num x, num y) {
    if (width < 0 || height < 0) return _asPositiveSize().inset(x, y);
    return new Rectangle2.fromTopLeft(
        xmin + x, ymin + y, width - 2 * x, height - 2 * y);
  }

  Rectangle _asPositiveSize() {
    return new Rectangle2.fromTopLeft(
        math.min(xmin, xmax), math.min(ymin, ymax), width.abs(), height.abs());
  }

  @override
  Rectangle computeInstersectionRectangle(Rectangle rectangle) {
    // TODO: implement computeInstersectionRectangle
    return null;
  }

  Rectangle round() {
    int x = xmin.round();
    int y = ymin.round();
    int w = (xmin + width).round() - x;
    int h = (ymin + height).round() - y;
    return new Rectangle2.fromTopLeft(x, y, w, h);
  }
}

class Rectangle {
  Point2D center;
  Point2D size;

  factory Rectangle(center, size) {
    throw "Rectangle ... deprecated";
  }

  factory Rectangle.fromCorner(num xmin, num xmax, num ymin, num ymax) {
//    return new Rectangle2.fromCorner(xmin, xmax, ymin, ymax);
    var rec = new Rectangle._fromCorner(xmin, xmax, ymin, ymax);
    //return new Rectangle2(rec.topLeft, rec.size);
    return rec;
  }

  factory Rectangle.fromTopLeft(num xmin, num ymin, num width, num height) {
    var rec = new Rectangle._fromTopLeft(xmin, ymin, width, height);
//    return new Rectangle2(rec.topLeft, rec.size);
    //return new Rectangle2.fromTopLeft(xmin, ymin, width, height);
    return rec;
  }

  Rectangle.fromCenter(this.center, this.size);

  Rectangle._(this.center, this.size);

  Rectangle._fromCorner(num xmin, num xmax, num ymin, num ymax) {
    size = new Point2D(xmax - xmin, ymax - ymin);
    center = new Point2D(xmin + size.x / 2, ymin + size.y / 2);
  }

  Rectangle._fromTopLeft(num xmin, num ymin, num width, num height) {
    size = new Point2D(width, height);
    center = new Point2D(xmin + size.x / 2, ymin + size.y / 2);
  }

  Rectangle inset(num x, num y) {
    var w;
    if (width < 0) {
      w = width + 2 * x;
    } else {
      w = width - 2 * x;
    }

    var h;
    if (height < 0) {
      h = height + 2 * y;
    } else {
      h = height - 2 * y;
    }

    return new Rectangle._(new Point2D(center.x, center.y), new Point2D(w, h));
//    return new Rectangle.fromCorner(xmin + x, xmax - x, ymin + y, ymax - y);
  }

  Point2D get topLeft => center - size / 2;
  Point2D get bottomRight => center + size / 2;

  num get width => size.x;
  num get height => size.y;

  num get left => xmin;
  num get right => xmax;
  num get top => ymin;
  num get bottom => ymax;

  num get xmin => this.center.x - this.size.x / 2;
  num get xmax => this.center.x + this.size.x / 2;

  num get ymin => this.center.y - this.size.y / 2;
  num get ymax => this.center.y + this.size.y / 2;

  String toString() {
    return "${this.runtimeType}($center,$size)";
  }

  bool containsPoint(Point2D p) => contains(p.x, p.y);
  bool contains(num x, num y) {
    return ((center.x - x).abs() <= size.x / 2 &&
        (center.y - y).abs() <= size.y / 2);
  }

  bool intersect(Rectangle other) {
    return (left <= other.left + other.width &&
        other.left <= left + width &&
        top <= other.top + other.height &&
        other.top <= top + height);

//    return _intersect(this, rec) || _intersect(rec, this);
  }

//  bool intersect2(Rectangle other) {
//    return (left <= other.left + other.width &&
//        other.left <= left + width &&
//        top <= other.top + other.height &&
//        other.top <= top + height);
//  }

  bool _intersect(Rectangle rec1, Rectangle rec2) {
    return rec1.contains(rec2.xmin, rec2.ymax) ||
        rec1.contains(rec2.xmin, rec2.ymin) ||
        rec1.contains(rec2.xmax, rec2.ymax) ||
        rec1.contains(rec2.xmax, rec2.ymin) ||
        rec1.contains(rec2.center.x, rec2.center.y);
  }

  int get hashCode {
    return center.hashCode + size.hashCode;
  }

  bool operator ==(other) {
    return other is Rectangle && other.center == center && other.size == size;
  }

  void move(Point2D point) {
    center.plus(point);
  }

  Map toJson() {
    return {"xmin": xmin, "xmax": xmax, "ymin": ymin, "ymax": ymax};
  }

  Rectangle computeInstersectionRectangle(Rectangle rectangle) {
    var left;
    var right;
    var top;
    var bottom;

//    if (width < 0){
//        width = width.abs();
//        x -=  width;
//      }
//
//      if (height < 0){
//        height = height.abs();
//          y -=  height;
//        }

    left = math.max(rectangle.xmin, this.xmin);
    right = math.min(rectangle.xmax, this.xmax);

    if (left >= right) return null;

    top = math.max(rectangle.ymin, this.ymin);
    bottom = math.min(rectangle.ymax, this.ymax);

    if (bottom >= top) return null;

    return new Rectangle.fromCorner(left, right, top, bottom);
  }

}
