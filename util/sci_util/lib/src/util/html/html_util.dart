import 'dart:html';

CanvasElement _CANVAS;
const String DEFAULT_FONT = '14px "Helvetica Neue", Helvetica, Arial, sans-serif';

CanvasElement getCanvas(){
  if (_CANVAS == null) _CANVAS = new CanvasElement(width: 1, height: 1);
  return _CANVAS;
}

TextMetrics measureText(String text, {String font:DEFAULT_FONT}){
  var canvas = getCanvas();
  var ctx = canvas.context2D;
  ctx.font = font;
  return ctx.measureText(text);
}

num measureTextWidth(String text, {String font:DEFAULT_FONT}){
  return  measureText(text, font: font).width;
}