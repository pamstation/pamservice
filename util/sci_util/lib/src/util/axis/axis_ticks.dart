import 'dart:math' as math;

class AxisTicks {
  static double tickSpacing(double range, int maxNumberOfTicks) {
//    if (!range.isFinite){
//      return 0.0;
//    }
    // calculate an initial guess at step size
    if (range == 0) {
      return 1.0;
    }

    assert(range > 0, 'AxisTicks tickSpacing : range $range must be positive');

    assert(maxNumberOfTicks > 0,
        'AxisTicks tickSpacing : maxNumberOfTicks $maxNumberOfTicks must be positive');

    var tempStep = range / maxNumberOfTicks;

    // get the magnitude of the step size
    var mag = (math.log(tempStep) / math.ln10).floor();
    var magPow = math.pow(10, mag);

    // calculate most significant digit of the new step size
    var magMsd = (tempStep / magPow + 0.5).roundToDouble();

    // promote the MSD to either 1, 2, or 5
    if (magMsd > 5.0)
      magMsd = 10.0;
    else if (magMsd > 2.0)
      magMsd = 5.0;
    else if (magMsd > 1.0) magMsd = 2.0;

    return magMsd * magPow;
  }

  static List<num> getMinMaxTicks(
      num minValue, num maxValue, int maxNumberOfTicks) {
    var tick = tickSpacing(maxValue - minValue, maxNumberOfTicks);
    assert(tick > 0);
    var minTick = (minValue / tick).floor() * tick;
    var maxTick = (maxValue / tick).ceil() * tick;

    assert(minTick <= minValue);
    assert(maxTick >= maxValue);
    return [minTick, maxTick, tick];
  }
}
