import 'dart:math' as math;
import 'dart:convert';
import 'package:crypto/crypto.dart' as crypto;

String generateSSHA(String password) {
  var secureRandom = new math.Random.secure();
  var salt = new List.generate(4, (_) => secureRandom.nextInt(256));
  return generateSSHAWithSalt(password, salt);
}

String generateSSHAWithSalt(String password, List<int> salt) {
  var digest =
      crypto.sha1.convert([]..addAll(utf8.encode(password))..addAll(salt));
  var hashPlusSalt = new List.from(digest.bytes)..addAll(salt);
  var ssha = '{SSHA}${base64.encode(hashPlusSalt)}';
  return ssha;
}

List<int> getSalt(String ssha) {
  if (!ssha.startsWith('{SSHA}')) throw 'bad ssha format';
  var bytes = base64.decode(ssha.substring('{SSHA}'.length));
  return bytes.sublist(bytes.length - 4);
}

bool checkSSHA(String ssha, String password) {
  var salt = getSalt(ssha);
  var ssha2 = generateSSHAWithSalt(password, salt);
  return ssha == ssha2;
}

main() {
  var pwd = 'mm';
  var ssha = generateSSHA(pwd);
  print(ssha);
  print(checkSSHA(ssha, pwd));

  pwd = 'mm';
  ssha = '{SSHA}5BJZ9dFC0NlDdbYdEbipHYn56lCwNYb6';

  print(checkSSHA(ssha, pwd));


  var tt = 'e1NTSEF9azVoNXllWmlEUlhkRkZETGFIRWtBM3dzazVEbVdjVTk=';

  var pp = utf8.decode(base64.decode(tt));

  print(pp);


}

//    import java.security.MessageDigest;
//    import java.security.NoSuchAlgorithmException;
//    import java.security.SecureRandom;
//    import java.util.Base64;
//
//    private static final int SALT_LENGTH = 4;
//
//    public static String generateSSHA(byte[] password)
//    throws NoSuchAlgorithmException {
//    SecureRandom secureRandom = new SecureRandom();
//    byte[] salt = new byte[SALT_LENGTH];
//    secureRandom.nextBytes(salt);
//
//    MessageDigest crypt = MessageDigest.getInstance("SHA-1");
//    crypt.reset();
//    crypt.update(password);
//    crypt.update(salt);
//    byte[] hash = crypt.digest();
//
//    byte[] hashPlusSalt = new byte[hash.length + salt.length];
//    System.arraycopy(hash, 0, hashPlusSalt, 0, hash.length);
//    System.arraycopy(salt, 0, hashPlusSalt, hash.length, salt.length);
//
//    return new StringBuilder().append("{SSHA}")
//        .append(Base64.getEncoder().encodeToString(hashPlusSalt))
//        .toString();
//    }
