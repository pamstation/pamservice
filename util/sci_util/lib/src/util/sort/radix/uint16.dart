import 'dart:typed_data';

import '../../list/list.dart' as scilist;

final _HISTOGRAM = new Uint32List(65536);

Int32List order_uint16(List<int> keys) {
  final nelm = keys.length;

  if (nelm == 0) {
    return scilist.newInt32List(nelm);
  }
  if (nelm == 1) {
    return scilist.newInt32List(nelm);
  }

  var order = scilist.newInt32List(nelm);

  for (var j = 0; j < nelm; ++j) order[j] = j;

  final offset1 = _HISTOGRAM..fillRange(0, 65536, 0);
  for (var j = 0; j < offset1.length; ++j) offset1[j] = 0;

  // count the number of values in each bucket
  offset1[keys[0]] = 1;

  for (var i = 1; i < nelm; ++i) {
    ++offset1[keys[i]];
  }

  // determine the starting positions for each bucket

  var prev1 = offset1[0];
  offset1[0] = 0;

  for (var j = 1; j < 256; ++j) {
    final cnt1 = offset1[j];
    offset1[j] = prev1;
    prev1 += cnt1;
  }

  // distribution 1
  for (var i = 0; i < nelm; ++i) {
    final k = keys[i];
    var pos = offset1[k];
    offset1[k]++;
    order[pos] = i;
  }

  return order;
}
