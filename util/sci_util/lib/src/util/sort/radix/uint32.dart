import 'dart:typed_data';

import '../../list/list.dart' as scilist;

final _HISTOGRAM1 = new Uint32List(2048);
final _HISTOGRAM2 = new Uint32List(2048);
final _HISTOGRAM3 = new Uint32List(1024);

Int32List order_uint32(Uint32List keys) {
  var nelm = keys.length;
  if (nelm < 2) {
    return new Int32List(nelm);
  }

  var order = scilist.newInt32List(nelm);
  List<int> tmpOrder;

  var offset1 = _HISTOGRAM1..fillRange(0, 2048, 0); // 11-bit
  var offset2 = _HISTOGRAM2..fillRange(0, 2048, 0); // 11-bit
  var offset3 = _HISTOGRAM3..fillRange(0, 1024, 0); // 10-bit

  bool sorted = true;
  // count the number of values in each bucket
  ++offset1[keys[0] & 2047];
  ++offset2[(keys[0] >> 11) & 2047];
  ++offset3[(keys[0] >> 22)];

  for (var j = 1; j < nelm; ++j) {
    ++offset1[keys[j] & 2047];
    ++offset2[(keys[j] >> 11) & 2047];
    ++offset3[(keys[j] >> 22)];
    sorted = sorted && (keys[j] >= keys[j - 1]);
  }
  if (sorted) {
    scilist.fill_sequence(order);
    return order;
  }

  // determine the starting positions for each bucket
  var max1 = offset1[0];
  var max2 = offset2[0];
  var max3 = offset3[0];
  var prev1 = offset1[0];
  var prev2 = offset2[0];
  var prev3 = offset3[0];
  offset1[0] = 0;
  offset2[0] = 0;
  offset3[0] = 0;
  for (var j = 1; j < 1024; ++j) {
    final cnt1 = offset1[j];
    final cnt2 = offset2[j];
    final cnt3 = offset3[j];
    offset1[j] = prev1;
    offset2[j] = prev2;
    offset3[j] = prev3;
    prev1 += cnt1;
    prev2 += cnt2;
    prev3 += cnt3;
    if (max1 < cnt1) max1 = cnt1;
    if (max2 < cnt2) max2 = cnt2;
    if (max3 < cnt3) max3 = cnt3;
  }
  for (var j = 1024; j < 2048; ++j) {
    final cnt1 = offset1[j];
    final cnt2 = offset2[j];
    offset1[j] = prev1;
    offset2[j] = prev2;
    prev1 += cnt1;
    prev2 += cnt2;
    if (max1 < cnt1) max1 = cnt1;
    if (max2 < cnt2) max2 = cnt2;
  }

  if (max1 == nelm && max2 == nelm && max3 == nelm) {
    scilist.fill_sequence(order);
    return order;
  }

  tmpOrder = scilist.newInt32List(nelm);


  // distribution 1
  if (max1 < nelm) {
    for (var j = 0; j < nelm; ++j) {
      var k = keys[j] & 2047;
      var pos = offset1[k];
      offset1[k]++;
      tmpOrder[pos] = j;
    }

    var tmp = tmpOrder;
    tmpOrder = order;
    order = tmp;

  } else {
    scilist.fill_sequence(order);
  }

//  print(keys);
//  print(order);

  // distribution 2
  if (max2 < nelm) {
     for (var j = 0; j < nelm; ++j) {
      final i = order[j];
      final k = (keys[i] >> 11) & 2047;
      final pos = offset2[k];
      offset2[k]++;
      tmpOrder[pos] = i;
    }

     var tmp = tmpOrder;
     tmpOrder = order;
     order = tmp;
   }

  // distribution 3
  if (max3 < nelm) {
    for (var j = 0; j < nelm; ++j) {
      final i = order[j];
      var k = (keys[i] >> 22);
      var pos = offset3[k];
      offset3[k]++;
      tmpOrder[pos] = i;
    }

    order = tmpOrder;

  }

  return order;
}

