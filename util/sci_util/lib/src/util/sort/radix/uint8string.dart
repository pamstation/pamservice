import 'dart:typed_data';

import '../../list/list.dart' as scilist;

//final _HISTOGRAM = new Uint32List(256);

// This function is an utility used to sort uint8string using radix sort.
// Read bytes using starts, and extract the nth_uint32 from each string, store the result into keys.
// If nth_uint32 = 0, keys will contain the first 4 bytes (ie: uint32) of each string
// If nth_uint32 = 1, keys will contain the bytes from 4 to 7 (ie: uint32) of each string
// Etc ..
// nelm : number of string stored in bytes = length of keys = length of starts -1
Uint32List slice_uint8string_uint32(
    Uint8List bytes, Int32List starts, int nelm, int nth_uint32) {
  if (nelm < 1) return new Uint32List(nelm);

  Uint32List keys = new Uint32List(nelm);

  var uint32Index = nth_uint32;
  var start32 = (uint32Index) * 4;

  var start = starts[0];

  for (var i = 0; i < nelm; i++) {
    var end = starts[i + 1];

    var start2 = start32 + start > end ? end : start32 + start;
    var end2 = start32 + start + 4 > end ? end : start32 + start + 4;

    var len = end2 - start2;

    if (len > 0) {
      var key = 0;
      var shift = 3;

      for (var k = start2; k < end2; k++) {
        key = key | (bytes[k] << (8 * shift));
        shift--;
      }

      keys[i] = key;
    } else {
      keys[i] = 0;
    }
    start = end;
  }

  return keys;
}

Int32List order_uint8String(scilist.Uint8StringList list) {
  final nelm = list.length;
  if (nelm == 0) {
    return new Int32List(0);
  }
  if (nelm == 1) {
    return new Int32List(1);
  }

  return order_uint8String32(list.bytesView, list.startsView);
}

// each string has 1 one character
List<int> order_uint8String32(Uint8List bytes, Int32List starts) {
  //
  if (Endian.host == Endian.big)
    throw 'order_uint8String32 does not support Endianness.BIG_ENDIAN ';

  var nelm = starts.length - 1;
  if (nelm == 0) {
    return new Int32List(0);
  }
  if (nelm == 1) {
    return new Int32List(1);
  }

  var watch = new Stopwatch()..start();

  var maxLenKey = scilist.max_start(starts);

//  print(
//      'order_uint8String32 -- max_start nelm ${nelm} -- maxLenKey $maxLenKey -- ${watch.elapsedMilliseconds} ms');

  if (maxLenKey == 0) {
    return scilist.newIntSequence(nelm);
  }

  var maxLenKey32 = (maxLenKey / 4).ceil();
  var uint32Index = maxLenKey32 - 1;

  watch
    ..reset()
    ..start();

  var keys = scilist.slice_uint8string_uint32(bytes, starts, nelm, uint32Index);

  var order = scilist.order(keys);

  for (var uint32Index = maxLenKey32 - 2; uint32Index >= 0; uint32Index--) {
    watch
      ..reset()
      ..start();

    keys = scilist.slice_uint8string_uint32(bytes, starts, nelm, uint32Index);

//    print(
//        'order_uint8String32 -- slice_uint8string_uint32 nelm ${nelm} uint32Index ${uint32Index} -- ${watch.elapsedMilliseconds} ms');

    assert(keys.length == nelm);

    watch
      ..reset()
      ..start();

    scilist.reorder(keys, order);

//    print(
//        'order_uint8String32 -- reorder nelm ${nelm} uint32Index ${uint32Index} -- ${watch.elapsedMilliseconds} ms');

    watch
      ..reset()
      ..start();

    var tmpOrder = scilist.order(keys);

//    print(
//        'order_uint8String32 -- order nelm ${nelm} uint32Index ${uint32Index} -- ${watch.elapsedMilliseconds} ms');

    watch
      ..reset()
      ..start();

    scilist.reorder(order, tmpOrder);

//    print(
//        'order_uint8String32 -- reorder2 nelm ${nelm} uint32Index ${uint32Index} -- ${watch.elapsedMilliseconds} ms');
  }

  return order;
}
