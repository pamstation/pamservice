import 'dart:typed_data';

import '../../list/list.dart' as scilist;

final _HISTOGRAM = new Uint32List(256);

Int32List order_uint8(List<int> keys) {
  var nelm = keys.length;
  if (nelm < 2) {
    return new Int32List(nelm);
  }

  var order = scilist.newInt32List(nelm);

  final offset1 = _HISTOGRAM..fillRange(0, 256, 0);

  // count the number of values in each bucket

  offset1[keys[0]] = 1;

  bool sorted = true;
  for (var i = 1; i < nelm; ++i) {
    ++offset1[keys[i]];
    sorted = sorted && (keys[i] >= keys[i - 1]);
  }
  if (sorted) {
    scilist.fill_sequence(order);
    return order;
  }

  // determine the starting positions for each bucket

  var maxv = offset1[0];
  var prev1 = offset1[0];
  offset1[0] = 0;

  for (var j = 1; j < 256; ++j) {
    final cnt1 = offset1[j];
    offset1[j] = prev1;
    prev1 += cnt1;
    if (cnt1 > maxv) maxv = cnt1;
  }

  // distribution 1
  if (maxv < nelm) {
    for (var i = 0; i < nelm; ++i) {
      final k = keys[i];
      var pos = offset1[k];
      offset1[k]++;
      order[pos] = i;
    }
  } else {
    scilist.fill_sequence(order);
  }

  return order;
}
