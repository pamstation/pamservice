library sci_util_radix_sort;

//import 'dart:typed_data';

export './int32.dart';
export './uint8.dart';
export './uint16.dart';
export './uint32.dart';
export './uint64.dart';
export './float32.dart';
export './float64.dart';
export './uint8string.dart';

//void radix(int byte, int N, Uint32List source, Uint32List dest) {
//  var count = new Uint8List(256);
//  var index = new Uint8List(256);
//
//  for (var i = 0; i < N; ++i) count[((source[i]) >> (byte * 8)) & 0xff]++;
//
//  index[0] = 0;
//  for (var i = 1; i < 256; ++i) index[i] = index[i - 1] + count[i - 1];
//
//  for (var i = 0; i < N; ++i)
//    dest[index[((source[i]) >> (byte * 8)) & 0xff]++] = source[i];
//}
//
//void radixsort(Uint32List source) {
//  var N = source.length;
//  var temp = new Uint32List(N);
//  radix(0, N, source, temp);
//  radix(1, N, temp, source);
//  radix(2, N, source, temp);
//  radix(3, N, temp, source);
//}
