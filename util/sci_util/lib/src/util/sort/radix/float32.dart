import 'dart:typed_data';

import '../../list/list.dart' as scilist;

final _HISTOGRAM1 = new Uint32List(2048);
final _HISTOGRAM2 = new Uint32List(2048);
final _HISTOGRAM3 = new Uint32List(1024);

//Uint32List ikeys = new Uint32List.view(keys.buffer);
Int32List order_float32(Float32List fkeys) {
  var nelm = fkeys.length;
  if (nelm <= 1) {
    return new Int32List(nelm);
  }
  Uint32List keys = new Uint32List.view(fkeys.buffer, fkeys.offsetInBytes, fkeys.length);
  var order = scilist.newInt32List(nelm);
  List<int> tmpOrder;

  var offset1 = _HISTOGRAM1..fillRange(0, 2048, 0); // 11-bit
  var offset2 = _HISTOGRAM2..fillRange(0, 2048, 0); // 11-bit
  var offset3 = _HISTOGRAM3..fillRange(0, 1024, 0); // 10-bit
  bool sorted = true;
  // count the number of values in each bucket
  offset1[(keys[0]) & 2047] = 1;
  offset2[((keys[0]) >> 11) & 2047] = 1;
  offset3[((keys[0]) >> 22)] = 1;
  for (var j = 1; j < nelm; ++j) {
    var key = keys[j];
    ++offset1[key & 2047];
    ++offset2[(key >> 11) & 2047];
    ++offset3[(key >> 22)];
    sorted = sorted && (keys[j] >= keys[j - 1]);
  }
  if (sorted) {
    scilist.fill_sequence(order);
    return order;
  }

  // determine the starting positions for each bucket
  var prev1 = offset1[0];
  var prev2 = offset2[0];
  var prev3 = offset3[1023];
  var max1 = offset1[0];
  var max2 = offset2[0];
  var max3 = offset3[1023];
  offset1[0] = 0;
  offset2[0] = 0;
  for (var j = 1; j < 2048; ++j) {
    final cnt1 = offset1[j];
    final cnt2 = offset2[j];
    offset1[j] = prev1;
    offset2[j] = prev2;
    prev1 += cnt1;
    prev2 += cnt2;
    if (cnt1 > max1) max1 = cnt1;
    if (cnt2 > max2) max2 = cnt2;
  }
  for (var j = 1022; j > 511; --j) {
    final cnt3 = offset3[j];
    prev3 += cnt3;
    offset3[j] = prev3;
    if (cnt3 > max3) max3 = cnt3;
  }
  for (var j = 0; j < 512; ++j) {
    final cnt3 = offset3[j];
    offset3[j] = prev3;
    prev3 += cnt3;
    if (cnt3 > max3) max3 = cnt3;
  }
  if (max1 == nelm && max2 == nelm && max3 == nelm) {
    scilist.fill_sequence(order);
    return order;
  }

  // distribution 1
  if (max1 < nelm) {
    for (var j = 0; j < nelm; ++j) {
      var k = keys[j] & 2047;
      var pos = offset1[k];
      offset1[k]++;
      order[pos] = j;
    }
  } else {
    scilist.fill_sequence(order);
  }

  // distribution 2
  if (max2 < nelm) {
    if (tmpOrder == null) tmpOrder = scilist.newInt32List(nelm );

    for (var j = 0; j < nelm; ++j) {
      final i = order[j];
      var k = (keys[i] >> 11) & 2047;
      var pos = offset2[k];
      offset2[k]++;
      tmpOrder[pos] = i;
    }
  } else if (max3 < nelm) {
    if (tmpOrder == null) tmpOrder = scilist.newInt32List(nelm );
    tmpOrder.setRange(0, nelm, order);
  }

  // distribution 3
  if (max3 < nelm) {
    for (var j = 0; j < nelm; ++j) {
      final i = tmpOrder[j];
      var k = (keys[i] >> 22);

      if (k < 512) {
        // positive value
        var pos = offset3[k];
        order[pos] = i;
        offset3[k]++;
      } else {
        offset3[k]--;
        var pos = offset3[k];
        order[pos] = i;
      }
    }
  } else {
    if (max2 < nelm) {
      order.setRange(0, nelm, tmpOrder);
    }
  }

  return order;
}
