import 'dart:typed_data';

import '../../list/list.dart' as scilist;
import './uint32.dart';

final _HISTOGRAM1 = new Uint32List(2048);
final _HISTOGRAM2 = new Uint32List(2048);
final _HISTOGRAM3 = new Uint32List(2048);
final _HISTOGRAM4 = new Uint32List(2048);
final _HISTOGRAM5 = new Uint32List(1024);
final _HISTOGRAM6 = new Uint32List(1024);

Int32List order_float64(Float64List fkeys) {
  var nelm = fkeys.length;
  if (nelm < 2) {
    return new Int32List(nelm);
  }

  Uint64List keys = new Uint64List.view(fkeys.buffer);

  var order = scilist.newInt32List(nelm);
  List<int> tmpOrder;

//  int iii = 18446744073709551615;
//
//  int maxint64 = 9223372036854775807;


  var offset1 = _HISTOGRAM1..fillRange(0, 2048, 0); // 11-bit
  var offset2 = _HISTOGRAM2..fillRange(0, 2048, 0); // 11-bit
  var offset3 = _HISTOGRAM3..fillRange(0, 2048, 0); // 11-bit
  var offset4 = _HISTOGRAM4..fillRange(0, 2048, 0); // 11-bit
  var offset5 = _HISTOGRAM5..fillRange(0, 1024, 0); // 10-bit
  var offset6 = _HISTOGRAM6..fillRange(0, 1024, 0); // 10-bit

  bool sorted = true;
  // count the number of values in each bucket
  ++offset1[keys[0] & 2047];
  ++offset2[(keys[0] >> 11) & 2047];
  ++offset3[(keys[0] >> 22) & 2047];
  ++offset4[(keys[0] >> 33) & 2047];
  ++offset5[(keys[0] >> 44) & 1023];

  var k = (keys[0] >> 48);
  // convert -NaN to +NaN
  if (k == 65528) {
    k = 32760;
  }

  k = k >> 6;
  ++offset6[k];

  for (var j = 1; j < nelm; ++j) {
    ++offset1[keys[j] & 2047];
    ++offset2[(keys[j] >> 11) & 2047];
    ++offset3[(keys[j] >> 22) & 2047];
    ++offset4[(keys[j] >> 33) & 2047];
    ++offset5[(keys[j] >> 44) & 1023];
//    ++offset6[(keys[j] >> 54)];
    var k = (keys[j] >> 48);
    // convert -NaN to +NaN
    if (k == 65528) {
      k = 32760;
    }

    k = k >> 6;
    ++offset6[k];

    sorted = sorted && (keys[j] >= keys[j - 1]);
  }

  if (sorted) {
    scilist.fill_sequence(order);
    return order;
  }

  // determine the starting positions for each bucket
  var max1 = offset1[0];
  var max2 = offset2[0];
  var max3 = offset3[0];
  var max4 = offset4[0];
  var max5 = offset5[0];
  var max6 = offset6[1023];
  var prev1 = offset1[0];
  var prev2 = offset2[0];
  var prev3 = offset3[0];
  var prev4 = offset4[0];
  var prev5 = offset5[0];
  var prev6 = offset6[1023];
  offset1[0] = 0;
  offset2[0] = 0;
  offset3[0] = 0;
  offset4[0] = 0;
  offset5[0] = 0;
//  offset6[0] = 0;
  for (var j = 1; j < 1024; ++j) {
    final cnt1 = offset1[j];
    final cnt2 = offset2[j];
    final cnt3 = offset3[j];
    final cnt4 = offset4[j];
    final cnt5 = offset5[j];

    offset1[j] = prev1;
    offset2[j] = prev2;
    offset3[j] = prev3;
    offset4[j] = prev4;
    offset5[j] = prev5;

    prev1 += cnt1;
    prev2 += cnt2;
    prev3 += cnt3;
    prev4 += cnt4;
    prev5 += cnt5;

    if (max1 < cnt1) max1 = cnt1;
    if (max2 < cnt2) max2 = cnt2;
    if (max3 < cnt3) max3 = cnt3;
    if (max4 < cnt4) max4 = cnt4;
    if (max5 < cnt5) max5 = cnt5;
  }
  for (var j = 1024; j < 2048; ++j) {
    final cnt1 = offset1[j];
    final cnt2 = offset2[j];
    final cnt3 = offset3[j];
    final cnt4 = offset4[j];
    offset1[j] = prev1;
    offset2[j] = prev2;
    offset3[j] = prev3;
    offset4[j] = prev4;
    prev1 += cnt1;
    prev2 += cnt2;
    prev3 += cnt3;
    prev4 += cnt4;
    if (max1 < cnt1) max1 = cnt1;
    if (max2 < cnt2) max2 = cnt2;
    if (max3 < cnt3) max3 = cnt3;
    if (max4 < cnt4) max4 = cnt4;
  }

  for (var j = 1022; j > 511; --j) {
    final cnt6 = offset6[j];
    prev6 += cnt6;
    offset6[j] = prev6;
    if (max6 < cnt6) max6 = cnt6;
  }

  for (var j = 0; j < 512; ++j) {
    final cnt6 = offset6[j];
    offset6[j] = prev6;
    prev6 += cnt6;
    if (max6 < cnt6) max6 = cnt6;
  }

  if (max1 == nelm &&
      max2 == nelm &&
      max3 == nelm &&
      max4 == nelm &&
      max5 == nelm &&
      max6 == nelm) {
    scilist.fill_sequence(order);
    return order;
  }

  tmpOrder = scilist.newInt32List(nelm);

  // distribution 1
  if (max1 < nelm) {
    for (var j = 0; j < nelm; ++j) {
      var k = keys[j] & 2047;
      var pos = offset1[k];
      offset1[k]++;
      tmpOrder[pos] = j;
    }

    var tmp = tmpOrder;
    tmpOrder = order;
    order = tmp;
  } else {
    scilist.fill_sequence(order);
  }

//  print(keys);
//  print(order);

  // distribution 2
  if (max2 < nelm) {
    for (var j = 0; j < nelm; ++j) {
      final i = order[j];
      final k = (keys[i] >> 11) & 2047;
      final pos = offset2[k];
      offset2[k]++;
      tmpOrder[pos] = i;
    }

    var tmp = tmpOrder;
    tmpOrder = order;
    order = tmp;
  }

//  print(order);

  // distribution 3
  if (max3 < nelm) {
    for (var j = 0; j < nelm; ++j) {
      final i = order[j];
      var k = (keys[i] >> 22) & 2047;
      var pos = offset3[k];
      offset3[k]++;
      tmpOrder[pos] = i;
    }

    var tmp = tmpOrder;
    tmpOrder = order;
    order = tmp;
  }

  //////////////////////////////////////////////////////////////////////////

//  print(order);

  // distribution 4
  if (max4 < nelm) {
    for (var j = 0; j < nelm; ++j) {
      final i = order[j];
      var k = (keys[i] >> 33) & 2047;
      var pos = offset4[k];
      offset4[k]++;
      tmpOrder[pos] = i;
    }

    var tmp = tmpOrder;
    tmpOrder = order;
    order = tmp;
  }

//  print(keys);
//  print(order);

  // distribution 5
  if (max5 < nelm) {
    for (var j = 0; j < nelm; ++j) {
      final i = order[j];
      final k = (keys[i] >> 44) & 1023;
      final pos = offset5[k];
      offset5[k]++;
      tmpOrder[pos] = i;
    }

    var tmp = tmpOrder;
    tmpOrder = order;
    order = tmp;
  }

//  print(order);

  // distribution 6
  if (max6 < nelm) {
    for (var j = 0; j < nelm; ++j) {
      final i = order[j];
//      var k = (keys[i] >> 54);
      var k = (keys[i] >> 48);
      // convert -NaN to +NaN
      if (k == 65528) {
        k = 32760;
      }

      k = k >> 6;

      if (k < 512) {
        // positive value
        var pos = offset6[k];
        tmpOrder[pos] = i;
        offset6[k]++;
      } else {
        offset6[k]--;
        var pos = offset6[k];
        tmpOrder[pos] = i;
      }
    }

    order = tmpOrder;
  }

//  print(order);

  return order;
}
