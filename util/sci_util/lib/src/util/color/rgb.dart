library sci_util_rgb;

import 'dart:math' as math;

class RGB {
  static List<RGB> COLORS;
  static math.Random RND = new math.Random();

  static List<RGB> getStdColors() {
    var list = [];
    list
      ..add(new RGB.rgb(0, 0, 0))
      ..add(new RGB.rgb(255, 255, 255))
      ..add(new RGB.rgb(255, 0, 0))
      ..add(new RGB.rgb(0, 255, 0))
      ..add(new RGB.rgb(0, 0, 255))
      ..add(new RGB.rgb(255, 255, 0))
      ..add(new RGB.rgb(255, 0, 255))
      ..add(new RGB.rgb(0, 255, 255))
      ..add(new RGB.rgb(128, 0, 0))
      ..add(new RGB.rgb(0, 128, 0))
      ..add(new RGB.rgb(0, 0, 128))
      ..add(new RGB.rgb(0, 204, 255))
      ..add(new RGB.rgb(153, 51, 102));
    return list;
  }

  static List<RGB> getColors() {
    if (COLORS == null) {
      COLORS = [];
      COLORS.add(new RGB.rgb(255, 255, 255));
      COLORS.add(new RGB.rgb(0, 0, 255));
      COLORS.add(new RGB.rgb(0, 255, 0));
      COLORS.add(new RGB.rgb(0, 255, 255));
      COLORS.add(new RGB.rgb(0, 128, 0));
      COLORS.add(new RGB.rgb(128, 0, 128));
      COLORS.add(new RGB.rgb(0, 0, 128));
      COLORS.add(new RGB.rgb(128, 0, 0));
      COLORS.add(new RGB.rgb(0, 128, 128));
      COLORS.add(new RGB.rgb(128, 128, 0));
      COLORS.add(new RGB.rgb(255, 0, 255));
      COLORS.add(new RGB.rgb(62, 0, 255));
      COLORS.add(new RGB.rgb(62, 255, 0));
      COLORS.add(new RGB.rgb(255, 255, 62));
      COLORS.add(new RGB.rgb(62, 255, 255));
      COLORS.add(new RGB.rgb(0, 128, 62));
      COLORS.add(new RGB.rgb(128, 62, 128));
      COLORS.add(new RGB.rgb(62, 0, 128));
      COLORS.add(new RGB.rgb(128, 62, 0));
      COLORS.add(new RGB.rgb(62, 128, 128));
      COLORS.add(new RGB.rgb(128, 128, 62));
      COLORS.add(new RGB.rgb(62, 62, 255));
      COLORS.add(new RGB.rgb(255, 255, 0));
      COLORS.add(new RGB.rgb(175, 255, 80));
      COLORS.add(new RGB.rgb(255, 0, 0));
      COLORS.add(new RGB.rgb(0, 137, 255));
      COLORS.add(new RGB.rgb(190, 255, 65));
      COLORS.add(new RGB.rgb(255, 1, 0));
      COLORS.add(new RGB.rgb(86, 255, 169));
      COLORS.add(new RGB.rgb(0, 0, 189));
      COLORS.add(new RGB.rgb(255, 0, 0));
      COLORS.add(new RGB.rgb(206, 255, 49));
      COLORS.add(new RGB.rgb(0, 111, 255));
      COLORS.add(new RGB.rgb(255, 0, 0));
      COLORS.add(new RGB.rgb(212, 255, 43));
      COLORS.add(new RGB.rgb(0, 0, 214));
      COLORS.add(new RGB.rgb(255, 81, 0));
      COLORS.add(new RGB.rgb(0, 85, 255));
      COLORS.add(new RGB.rgb(149, 255, 106));
      COLORS.add(new RGB.rgb(255, 0, 0));
      COLORS.add(new RGB.rgb(0, 14, 255));
      COLORS.add(new RGB.rgb(157, 255, 98));
      COLORS.add(new RGB.rgb(255, 0, 0));
      COLORS.add(new RGB.rgb(85, 85, 255));
      COLORS.add(new RGB.rgb(85, 255, 85));
      COLORS.add(new RGB.rgb(85, 255, 255));
      COLORS.add(new RGB.rgb(85, 171, 85));
      COLORS.add(new RGB.rgb(171, 85, 171));
      COLORS.add(new RGB.rgb(85, 85, 171));
      COLORS.add(new RGB.rgb(171, 85, 85));
      COLORS.add(new RGB.rgb(85, 171, 171));
      COLORS.add(new RGB.rgb(171, 171, 85));
      COLORS.add(new RGB.rgb(255, 85, 255));
      COLORS.add(new RGB.rgb(127, 85, 255));
      COLORS.add(new RGB.rgb(127, 255, 85));
      COLORS.add(new RGB.rgb(255, 255, 127));
      COLORS.add(new RGB.rgb(127, 255, 255));
      COLORS.add(new RGB.rgb(85, 171, 127));
      COLORS.add(new RGB.rgb(171, 127, 171));
      COLORS.add(new RGB.rgb(127, 85, 171));
      COLORS.add(new RGB.rgb(171, 127, 85));
      COLORS.add(new RGB.rgb(127, 171, 171));
      COLORS.add(new RGB.rgb(171, 171, 127));
      COLORS.add(new RGB.rgb(127, 127, 255));
      COLORS.add(new RGB.rgb(255, 255, 85));
      COLORS.add(new RGB.rgb(202, 255, 139));
      COLORS.add(new RGB.rgb(255, 85, 85));
      COLORS.add(new RGB.rgb(85, 177, 255));
      COLORS.add(new RGB.rgb(212, 255, 129));
      COLORS.add(new RGB.rgb(255, 86, 85));
      COLORS.add(new RGB.rgb(143, 255, 198));
      COLORS.add(new RGB.rgb(85, 85, 211));
      COLORS.add(new RGB.rgb(255, 85, 85));
      COLORS.add(new RGB.rgb(223, 255, 118));
      COLORS.add(new RGB.rgb(85, 159, 255));
      COLORS.add(new RGB.rgb(255, 85, 85));
      COLORS.add(new RGB.rgb(227, 255, 114));
      COLORS.add(new RGB.rgb(85, 85, 228));
      COLORS.add(new RGB.rgb(255, 139, 85));
      COLORS.add(new RGB.rgb(85, 142, 255));
      COLORS.add(new RGB.rgb(185, 255, 156));
      COLORS.add(new RGB.rgb(255, 85, 85));
      COLORS.add(new RGB.rgb(85, 95, 255));
      COLORS.add(new RGB.rgb(190, 255, 151));
      COLORS.add(new RGB.rgb(255, 85, 85));
    }
    return COLORS;
  }

  static RGB getColorAt(int index) {
    List aList = getColors();
    if (index < aList.length) return aList[index];
    return new RGB.randomColor();
  }

  int red;
  int green;
  int blue;

  RGB() {
    red = 0;
    green = 0;
    blue = 0;
  }

  static double _normXYZRGB(double c) {
    if (c > 0.0031308)
      return (1.055 * (math.pow(c, (1 / 2.4))) - 0.055) * 255;
    else
      return (12.92 * c) * 255;
  }

  factory RGB.fromXYZ(double x, double y, double z) {
    var xx = x / 100.0;
    var yy = y / 100.0;
    var zz = z / 100.0;
    var red = _normXYZRGB(xx * 3.2406 + yy * -1.5372 + zz * -0.4986).round();
    var green = _normXYZRGB(xx * -0.9689 + yy * 1.8758 + zz * 0.0415).round();
    var blue = _normXYZRGB(xx * 0.0557 + yy * -0.2040 + zz * 1.0570).round();

//    if (red > 255) print("red $red");
//    if (green > 255) print("green $green");
//    if (blue > 255) print("blue $blue");

//    print("$red , $green , $blue");

    var min = math.min(red, green);
    min = math.min(min, blue);
    if (min < 0) {
      red -= min;
      green -= min;
      blue -= min;
    }

    var mm = 1.0;
    if (red.abs() > 255) mm = 255 / red.abs();
    if (green.abs() > 255) mm = math.min(mm, 255 / green.abs());
    if (blue.abs() > 255) mm = math.min(mm, 255 / blue.abs());

//    print(mm);

    red = (red * mm).round();
    green = (green * mm).round();
    blue = (blue * mm).round();

    red = math.max(math.min(red, 255), 0);
    green = math.max(math.min(green, 255), 0);
    blue = math.max(math.min(blue, 255), 0);

    return new RGB.rgb(red, green, blue);
  }
  RGB.black() : this.rgb(0, 0, 0);
  RGB.white() : this.rgb(255, 255, 255);
  RGB.r() : this.rgb(255, 0, 0);
  RGB.g() : this.rgb(0, 255, 0);
  RGB.b() : this.rgb(0, 0, 255);

  RGB.fromJson(Map m) {
    this.red = m["r"];
    this.green = m["g"];
    this.blue = m["b"];
  }

  RGB.fromMap(Map m) {
    if (m['value'] != null) {
      int rgb = m['value'];
      this.red = (rgb >> 16) & 255;
      this.green = (rgb >> 8) & 255;
      this.blue = (rgb) & 255;
    } else {
      this.red = m["r"];
      this.green = m["g"];
      this.blue = m["b"];
    }
  }

  RGB.rgb(this.red, this.green, this.blue) {
    try {
      assert(red >= 0 && red < 256);
      assert(green >= 0 && green < 256);
      assert(blue >= 0 && blue < 256);
    } catch (e) {
      print("RGB.rgb : $red $green $blue");
      throw e;
    }
  }

  RGB.integer(int rgb) {
    this.red = (rgb >> 16) & 255;
    this.green = (rgb >> 8) & 255;
    this.blue = (rgb) & 255;
  }

  RGB.randomColor() {
    this.red = RND.nextInt(256);
    this.green = RND.nextInt(256);
    this.blue = RND.nextInt(256);
  }

  int get asInteger =>
      ((red & 255) << 16) | ((green & 255) << 8) | (blue & 255);

  String get css => "rgb($red,$green,$blue)";

  String get cssHex => toHexString(toInt());

  toInt() {
    int c = 0;
    c = setRed(c, red);
    c = setGreen(c, green);
    c = setBlue(c, blue);
    return c;
  }

  int setRed(int couleur, int valRouge) {
    return (couleur & 0x00FFFF) | (valRouge << 16);
  }

  int setGreen(int couleur, int valVert) {
    return (couleur & 0xFF00FF) | (valVert << 8);
  }

  int setBlue(int couleur, int valBleu) {
    return (couleur & 0xFFFF00) | valBleu;
  }

  String toHexString(int nb) {
    if (nb == 0) return "000000";
    String s = nb.toRadixString(16);
    if (nb < 256) {
      s = "0000$s";
    } else if (nb < 65536) {
      s = "00$s";
    }
    if (s.length != 6) {
      var len = 6 - s.length;
      for (int i = 0; i < len; i++) {
        s = "0$s";
      }
    }

    return "#$s";
  }

  Map toJson() => {"r": red, "g": green, "b": blue};

  @override
  bool operator ==(o) => o is RGB && o.toInt() == this.toInt();
  @override
  int get hashCode => this.toInt();

  int interpolate(RGB aColorEnd, double aNormValue) {
//    return interpolateRGB(aColorEnd, aNormValue);
    return this
        .toXYZ()
        .toCieLuv()
        .interpolate(aColorEnd.toXYZ().toCieLuv(), aNormValue)
        .toRGB()
        .asInteger;

//    return this.toCieLab().interpolate(aColorEnd.toCieLab(), aNormValue).toRGB().asInteger;
//    return this.toMsh().interpolate(aColorEnd.toMsh(), aNormValue).toRGB().asInteger;
  }

  int interpolateRGB(RGB aColorEnd, double aNormValue) {
    int r = (this.red + (aNormValue * (aColorEnd.red - this.red))).round();
    int g =
        (this.green + (aNormValue * (aColorEnd.green - this.green))).round();
    int b = (this.blue + (aNormValue * (aColorEnd.blue - this.blue))).round();
    return ((r & 255) << 16) | ((g & 255) << 8) | (b & 255);
  }

  CieLabColor toCieLab() {
    return new CieLabColor.fromRGB(red, green, blue);
  }

  XYZColor toXYZ() {
    return new XYZColor.fromRGB(red, green, blue);
  }

  MshColor toMsh() {
    return toCieLab().toMsh();
  }
}

class RGBf {
  double red;
  double green;
  double blue;

  RGBf.rgb(this.red, this.green, this.blue);

  RGBf() {
    red = 0.0;
    green = 0.0;
    blue = 0.0;
  }

  int get redInt => (red * 255).round().toInt();
  int get greenInt => (green * 255).round().toInt();
  int get blueInt => (blue * 255).round().toInt();

  String get css => "rgb($redInt,$greenInt,$blueInt)";

  String get cssHex => "#${toHexString(toInt)}";

  int get toInt {
    int c = 0;
    c = setRed(c, redInt);
    c = setGreen(c, redInt);
    c = setBlue(c, blueInt);
    return c;
  }

  int setRed(int couleur, int valRouge) {
    return (couleur & 0x00FFFF) | (valRouge << 16);
  }

  int setGreen(int couleur, int valVert) {
    return (couleur & 0xFF00FF) | (valVert << 8);
  }

  int setBlue(int couleur, int valBleu) {
    return (couleur & 0xFFFF00) | valBleu;
  }

  String toHexString(int nb) {
    String s = nb.toRadixString(16);
    if (s.length < 6) {
      StringBuffer buffer = new StringBuffer();
      int l = 6 - s.length;
      for (int i = 0; i < l; i++) buffer.write(0);
      buffer.write(s);
      return buffer.toString();
    } else
      return s;
  }
}

class HSLColor {
  double H;
  double S;
  double L;

  HSLColor.from(this.H, this.S, this.L);

  HSLColor.fromHueRad(double h, this.S, this.L) {
    var npi = (h / (2 * math.pi)).floor();
    var rpi = (h - 2 * math.pi * npi);
    H = rpi / (2 * math.pi);
    print(H);
    assert(H >= 0.0);
    assert(H <= 1.0);
  }

  HSLColor.fromRGB(int R, int G, int B) {
    var var_R = (R / 255); //RGB from 0 to 255
    var var_G = (G / 255);
    var var_B = (B / 255);

    var var_Min = math.min(var_R, math.min(var_G, var_B)); //Min. value of RGB
    var var_Max = math.max(var_R, math.min(var_G, var_B)); //Max. value of RGB
    var del_Max = var_Max - var_Min; //Delta RGB value

    L = (var_Max + var_Min) / 2;

    if (del_Max == 0) //This is a gray, no chroma...
    {
      H = 0.0; //HSL results from 0 to 1
      S = 0.0;
    } else //Chromatic data...
    {
      if (L < 0.5)
        S = del_Max / (var_Max + var_Min);
      else
        S = del_Max / (2 - var_Max - var_Min);

      var del_R = (((var_Max - var_R) / 6) + (del_Max / 2)) / del_Max;
      var del_G = (((var_Max - var_G) / 6) + (del_Max / 2)) / del_Max;
      var del_B = (((var_Max - var_B) / 6) + (del_Max / 2)) / del_Max;

      if (var_R == var_Max)
        H = del_B - del_G;
      else if (var_G == var_Max)
        H = (1 / 3) + del_R - del_B;
      else if (var_B == var_Max) H = (2 / 3) + del_G - del_R;

      if (H < 0) H += 1;
      if (H > 1) H -= 1;
    }
  }

  RGB toRGB() {
    var R;
    var G;
    var B;
    if (S == 0) //HSL from 0 to 1
    {
      R = L * 255; //RGB results from 0 to 255
      G = L * 255;
      B = L * 255;
    } else {
      var var_2;
      if (L < 0.5)
        var_2 = L * (1 + S);
      else
        var_2 = (L + S) - (S * L);

      var var_1 = 2 * L - var_2;

      R = (255 * Hue_2_RGB(var_1, var_2, H + (1 / 3)));
      G = 255 * Hue_2_RGB(var_1, var_2, H);
      B = 255 * Hue_2_RGB(var_1, var_2, H - (1 / 3));
    }

    return new RGB.rgb(R.round(), G.round(), B.round());
  }

  num Hue_2_RGB(num v1, num v2, num vH) //Function Hue_2_RGB
  {
    if (vH < 0) vH += 1;
    if (vH > 1) vH -= 1;
    if ((6 * vH) < 1) return (v1 + (v2 - v1) * 6 * vH);
    if ((2 * vH) < 1) return (v2);
    if ((3 * vH) < 2) return (v1 + (v2 - v1) * ((2 / 3) - vH) * 6);
    return (v1);
  }
}

class HSLPalette {
  double maxSat = 1.0;
  double minSat = 0.8;
  double maxLight = 0.8;
  double minLight = 0.6;

  HSLColor at(int ii) {
    var i = ii;

    var rs = (1 + math.sin(ii)) / 2; //(i % 100) / 99;
    var rl = (1 + math.cos(ii)) / 2; //(i % 100) / 99;

    var h = convertRad(new Huei().hue(i));

    var s = maxSat - rs * (maxSat - minSat);
    var l = maxLight - rl * (maxLight - minLight);

    return new HSLColor.from(h, s, l);
  }

  double convertRad(double rad) {
    rad += 3;
    var npi = (rad / (2 * math.pi)).floor();
    var rpi = (rad - 2 * math.pi * npi);
    var hh = rpi / (2 * math.pi);
    return hh;
  }
}

class Huei {
  int ki(int i) {
    return (math.log(1 + i) / math.ln2).ceil();
  }

  int ni(int i) {
    return math.pow(2, ki(i));
  }

  int Ni(int i) {
    return math.pow(2, ki(i) - 1);
  }

  int ji(int i) {
    return i - Ni(i);
  }

  double huei(int i) {
    return math.pi * 2 / ni(i);
  }

  double hue(int i) {
    if (i == 0) return 0.0;

    var n_i = ni(i);
    var n_i2 = n_i ~/ 2;

    var h_i = huei(i);
    var j_i = ji(i);

    var h_i_1 = huei(n_i2 - 1);

    var r_i = j_i % 4;
    var d_i = j_i ~/ 4;

    var h_i_0 = (d_i + 1) * h_i_1 + h_i;

//    h_i_0 += math.pi / 2;

    if (d_i.isEven) {
      if (r_i == 0) {
        return h_i_0;
      } else if (r_i == 1) {
        return h_i_0 + math.pi;
      } else if (r_i == 2) {
        return h_i_0 + math.pi / 2.0;
      } else {
        return h_i_0 - math.pi / 2.0;
      }
    } else {
      if (r_i == 0) {
        return h_i_0 + math.pi;
      } else if (r_i == 1) {
        return h_i_0;
      } else if (r_i == 2) {
        return h_i_0 - math.pi / 2.0;
      } else {
        return h_i_0 + math.pi / 2.0;
      }
    }
  }
}

class XYZColor {
  double x;
  double y;
  double z;

  XYZColor.from(this.x, this.y, this.z);

  static double _normRGB(int c) {
    var cc = c / 255.0;
    if (cc > 0.04045)
      cc = math.pow((cc + 0.055) / 1.055, 2.4);
    else
      cc = cc / 12.92;
    return cc * 100.0;
  }

  static double _normCieLab(double c) {
    var c3 = math.pow(c, 3);
    if (c3 > 0.008856)
      return c3;
    else
      return (c - 16.0 / 116.0) / 7.787;
  }

  XYZColor.fromCieLab(double l, double a, double b) {
    var yy = (l + 16.0) / 116.0;
    var xx = a / 500.0 + yy;
    var zz = yy - b / 200.0;
    yy = _normCieLab(yy);
    xx = _normCieLab(xx);
    zz = _normCieLab(zz);
    x = xx * CieLabColor.REF_X_D65;
    y = yy * CieLabColor.REF_Y_D65;
    z = zz * CieLabColor.REF_Z_D65;
  }

  XYZColor.fromRGB(int r, int g, int b) {
    var rr = _normRGB(r);
    var gg = _normRGB(g);
    var bb = _normRGB(b);

    x = rr * 0.4124 + gg * 0.3576 + bb * 0.1805;
    y = rr * 0.2126 + gg * 0.7152 + bb * 0.0722;
    z = rr * 0.0193 + gg * 0.1192 + bb * 0.9505;
  }

  RGB toRGB() {
    return new RGB.fromXYZ(x, y, z);
  }

  CieLabColor toCieLab() {
    return new CieLabColor.fromXYZ(x, y, z);
  }

  CieLuvColor toCieLuv() {
    return new CieLuvColor.fromXYZ(x, y, z);
  }
}

class CieLCHuvColor {
  double l;
  double c;
  double h;

  CieLCHuvColor.from(this.l, this.c, this.h);
  CieLCHuvColor.fromLuv(this.l, double u, double v) {
    c = math.pow(u * u + v * v, 0.5);
    h = math.atan2(v, u);
  }

  CieLuvColor toLuv() {
    var u = math.cos(h) * c;
    var v = math.sin(h) * c;
    return new CieLuvColor.from(l, u, v);
  }
}

class CieLuvColor {
  static const double REF_X_D65 = 95.047;
  static const double REF_Y_D65 = 100.0;
  static const double REF_Z_D65 = 108.883;

  double l;
  double u;
  double v;

  CieLuvColor.fromXYZ(double x, double y, double z) {
    var var_U = (4.0 * x) / (x + (15.0 * y) + (3.0 * z));
    var var_V = (9.0 * y) / (x + (15.0 * y) + (3.0 * z));
    var var_Y = y / 100.0;

    if (var_Y > 0.008856) {
      var_Y = math.pow(var_Y, 1 / 3);
    } else {
      var_Y = (7.787 * var_Y) + (16 / 116);
    }

    var ref_U =
        (4 * REF_X_D65) / (REF_X_D65 + (15 * REF_Y_D65) + (3 * REF_Z_D65));
    var ref_V =
        (9 * REF_Y_D65) / (REF_X_D65 + (15 * REF_Y_D65) + (3 * REF_Z_D65));

    l = (116 * var_Y) - 16;
    u = 13 * l * (var_U - ref_U);
    v = 13 * l * (var_V - ref_V);

    if (!u.isFinite) {
      u = 0.0;
    }
    if (!v.isFinite) {
      v = 0.0;
    }

    assert(l.isFinite);
    assert(u.isFinite);
    assert(v.isFinite);
  }

  CieLuvColor.from(this.l, this.u, this.v);

  XYZColor toXYZ() {
    var var_Y = (l + 16) / 116;
    if (math.pow(var_Y, 3) > 0.008856)
      var_Y = math.pow(var_Y, 3);
    else
      var_Y = (var_Y - 16 / 116) / 7.787;

    var ref_X = REF_X_D65; //Observer= 2°, Illuminant= D65
    var ref_Y = REF_Y_D65;
    var ref_Z = REF_Z_D65;

    var ref_U = (4 * ref_X) / (ref_X + (15 * ref_Y) + (3 * ref_Z));
    var ref_V = (9 * ref_Y) / (ref_X + (15 * ref_Y) + (3 * ref_Z));

    var var_U = u / (13 * l) + ref_U;
    var var_V = v / (13 * l) + ref_V;

    var Y = var_Y * 100;
    var X = -(9 * Y * var_U) / ((var_U - 4) * var_V - var_U * var_V);
    var Z = (9 * Y - (15 * var_V * Y) - (var_V * X)) / (3 * var_V);

    if (!X.isFinite) {
      X = 0.0;
    }
    if (!Y.isFinite) {
      Y = 0.0;
    }

    if (!Z.isFinite) {
      Z = 0.0;
    }

    return new XYZColor.from(X, Y, Z);
  }

  CieLuvColor interpolate2(CieLuvColor aColorEnd, double aNormValue) {
    var ll = (this.l + (aNormValue * (aColorEnd.l - this.l)));
    var uu = (this.u + (aNormValue * (aColorEnd.u - this.u)));
    var vv = (this.v + (aNormValue * (aColorEnd.v - this.v)));
    return new CieLuvColor.from(ll, uu, vv);
  }

  CieLuvColor interpolate(CieLuvColor aColorEnd, double aNormValue) {
    return this.interpolate2(aColorEnd, aNormValue);
//    if (this.l > aColorEnd.l) {
//      return aColorEnd.interpolate3(this, (1-aNormValue));
//    } else {
//      return this.interpolate3(aColorEnd, aNormValue);
//    }
  }

  CieLuvColor interpolate3(CieLuvColor aColorEnd, double aNormValue) {
    var L = (this.l +
        (aNormValue *
            (aColorEnd.l -
                this.l))); //this.l * aNormValue + (aColorEnd.l * (1-aNormValue));
    var L0 = 100; // (this.l + aColorEnd.l)/3;
    var r = math.pow(aNormValue, L / L0);
    r = aNormValue;
//    r = math.pow(aNormValue, 1 / (aNormValue + 0.5));
// print(L0/L);
// print("L $L , L0 $L0 : l $l le ${aColorEnd.l} aNormValue $aNormValue r $r");
    var ll = (this.l + (r * (aColorEnd.l - this.l)));
    var uu = (this.u + (r * (aColorEnd.u - this.u)));
    var vv = (this.v + (r * (aColorEnd.v - this.v)));
//      var uu = (this.u + (aNormValue * (aColorEnd.u - this.u)));
//      var vv = (this.v + (aNormValue * (aColorEnd.v - this.v)));
    return new CieLuvColor.from(ll, uu, vv);
  }

  RGB toRGB() {
    return this.toXYZ().toRGB();
  }

  CieLCHuvColor toLCHuv() {
    return new CieLCHuvColor.fromLuv(l, u, v);
  }
}

class CieLabColor {
  static const double REF_X_D65 = 95.047;
  static const double REF_Y_D65 = 100.0;
  static const double REF_Z_D65 = 108.883;
  double l;
  double a;
  double b;

  static double _normXYZ(double c) {
    if (c > 0.008856)
      return math.pow(c, 1 / 3);
    else
      return (7.787 * c) + (16 / 116);
  }

  factory CieLabColor.fromRGB(int r, int g, int b) {
    var xyz = new XYZColor.fromRGB(r, g, b);
    return new CieLabColor.fromXYZ(xyz.x, xyz.y, xyz.z);
  }

  CieLabColor.from(this.l, this.a, this.b);

  CieLabColor.fromXYZ(double x, double y, double z) {
    var xx = _normXYZ(x / REF_X_D65);
    var yy = _normXYZ(y / REF_Y_D65);
    var zz = _normXYZ(z / REF_Z_D65);
    l = (116.0 * yy) - 16.0;
    a = 500 * (xx - yy);
    b = 200 * (yy - zz);
  }

  CieLabColor interpolate(CieLabColor aColorEnd, double aNormValue) {
    var ll = (this.l + (aNormValue * (aColorEnd.l - this.l)));
    var aa = (this.a + (aNormValue * (aColorEnd.a - this.a)));
    var bb = (this.b + (aNormValue * (aColorEnd.b - this.b)));
    return new CieLabColor.from(ll, aa, bb);
  }

  RGB toRGB() {
    return toXYZ().toRGB();
  }

  XYZColor toXYZ() {
    return new XYZColor.fromCieLab(l, a, b);
  }

  MshColor toMsh() {
    return new MshColor.fromCieLag(l, a, b);
  }

  CieLchColor toCieLch() => new CieLchColor.fromCieLab(l, a, b);
}

//http://www.sandia.gov/~kmorel/documents/ColorMaps/ColorMapsExpanded.pdf
class MshColor {
  double m;
  double s;
  double h;

  MshColor.fromCieLag(double l, double a, double b) {
    m = math.pow((l * l + a * a + b * b), 0.5);
    s = math.acos(l / m);
    if (a > 0 && b >= 0) {
      h = math.atan(b / a);
    } else if (a > 0 && b < 0) {
      h = math.atan(b / a) + 2 * math.pi;
    } else if (a < 0) {
      h = math.atan(b / a) + math.pi;
    } else if (a == 0 && b > 0) {
      h = math.pi / 2.0;
    } else if (a == 0 && b < 0) {
      h = math.pi * 3 / 2.0;
    }
  }

  CieLabColor toCieLab() {
    var l = m * math.cos(s);
    var a = m * math.sin(s) * math.cos(h);
    var b = m * math.sin(s) * math.sin(h);
    return new CieLabColor.from(l, a, b);
  }

  MshColor.from(this.m, this.s, this.h);

  MshColor interpolateWhite(double aNormValue) {
    return new MshColor.from(m, s * aNormValue, h);
  }

  MshColor interpolate(MshColor aColorEnd, double aNormValue) {
    var mm = (this.m + (aNormValue * (aColorEnd.m - this.m)));
    var ss = (this.s + (aNormValue * (aColorEnd.s - this.s)));
    var hh = (this.h + (aNormValue * (aColorEnd.h - this.h)));
    return new MshColor.from(mm, ss, hh);
  }

  RGB toRGB() {
    return toCieLab().toRGB();
  }
}

class CieLchColor {
  double l;
  double c;
  double h;

  CieLchColor.fromCieLab(double ll, double a, double b) {
    l = ll;
    c = math.pow(a * a + b * b, 0.5);
    if (a > 0 && b >= 0) {
      h = math.atan(b / a);
    } else if (a > 0 && b < 0) {
      h = math.atan(b / a) + 2 * math.pi;
    } else if (a < 0) {
      h = math.atan(b / a) + math.pi;
    } else if (a == 0 && b > 0) {
      h = math.pi / 2.0;
    } else if (a == 0 && b < 0) {
      h = math.pi * 3 / 2.0;
    }
  }

  CieLchColor.from(this.l, this.c, this.h);

  CieLchColor interpolateWhite(double aNormValue) {
//    var t = 2*math.pi - h*aNormValue;
    return new CieLchColor.from(l, c, h * aNormValue);
  }

  CieLabColor toCieLab() {
    var a = math.cos(h) * c;
    var b = math.sin(h) * c;
    return new CieLabColor.from(l, a, b);
  }

  RGB toRGB() {
    return toCieLab().toRGB();
  }
}
