library sci_jet_palette;

import 'dart:typed_data';
import 'dart:math' as math;

import '../num/num.dart' as scinum;
import '../list/list.dart' as scilist;
//import 'dart:convert';
//import 'dart:io';

import './rgb.dart' as scirgb;

export '../colorhash/colorhash.dart';

Uint32List COLOR_LIST_1 = new Uint32List.fromList([
  2062516,
  14883356,
  3383340,
  10931939,
  11722634,
  16489113,
  16629615,
  16744192,
  13284054,
  6962586,
  16777113,
  11622696,
  1810039,
  14245634,
  7696563,
  15149450,
  6727198,
  15117058,
  10909213,
  6710886,
  14948892,
  3636920,
  5091146,
  9981603,
  16744192,
  16777011,
  10901032,
  16220607,
  10066329,
  14948892,
  3636920,
  5091146,
  9981603,
  16744192,
  16777011,
  10901032,
  16220607
]);

Uint32List COLOR_LIST_2 = new Uint32List.fromList([
  255,
  65280,
  65535,
  32768,
  8388736,
  128,
  8388608,
  32896,
  8421376,
  16711935,
  4063487,
  4128512,
  16777022,
  4128767,
  32830,
  8404608,
  4063360,
  8404480,
  4096128,
  8421438,
  4079359,
  16776960,
  11534160,
  16711680,
  35327,
  12517185,
  16711936,
  5701545,
  189,
  16711680,
  13565745,
  28671,
  16711680,
  13958955,
  214,
  16732416,
  22015,
  9830250,
  16711680,
  3839,
  10354530,
  16711680,
  5592575,
  5635925,
  5636095,
  5614421,
  11228587,
  5592491,
  11228501,
  5614507,
  11250517,
  16733695,
  8345087,
  8388437,
  16777087,
  8388607,
  5614463,
  11239339,
  8345003,
  11239253,
  8367019,
  11250559,
  8355839,
  16777045,
  13303691,
  16733525,
  5616127,
  13959041,
  16733781,
  9437126,
  5592531,
  16733525,
  14679926,
  5611519,
  16733525,
  14942066,
  5592548,
  16747349,
  5607167,
  12189596,
  16733525,
  5595135,
  12517271,
  16733525
]);

//main() {
//  var file = new File(
//      '/home/alex/dev/bitbucket/tercen/sci/sci_util/lib/src/util/color/color_list1.json');
//  var str = file.readAsStringSync();
//  var list =
//      new ColorPaletteList.fromJson(json.decode(file.readAsStringSync()));
//
//  for (var i = 0; i < list.nColors; i++) {
//    print('${list.getColorAt(i).asInteger},');
//  }
//}
//
//class ColorPaletteList {
//  Uint8List _r;
//  Uint8List _g;
//  Uint8List _b;
//
//  int nColors;
//
//  ColorPaletteList.fromJson(Map m) {
//    List colorsList = m["colors"];
//    nColors = colorsList.length;
//    _r = new Uint8List(colorsList.length);
//    _g = new Uint8List(colorsList.length);
//    _b = new Uint8List(colorsList.length);
//
//    for (int i = 0; i < nColors; i++) {
//      var c = new scirgb.RGB.fromJson(colorsList[i]);
//      _r[i] = c.red;
//      _g[i] = c.green;
//      _b[i] = c.blue;
//    }
//  }
//
//  scirgb.RGB getColorAt(int i) => i < this.nColors
//      ? new scirgb.RGB.rgb(_r[i], _g[i], _b[i])
//      : new scirgb.RGB.randomColor();
//}

Uint32List getRamp2Colors(Float64List values, double minValue, double maxValue,
    int minColor, int maxColor) {
  final len = values.length;
  final colors = new Uint32List(len);

  final range = maxValue - minValue;
  scirgb.RGB aColorStart = new scirgb.RGB.integer(minColor);
  scirgb.RGB aColorEnd = new scirgb.RGB.integer(maxColor);

  if (range == 0) {
    var iColor = aColorStart.interpolateRGB(aColorEnd, 0.5);
    for (var i = 0; i < len; i++) colors[i] = iColor;
  } else {
    for (var i = 0; i < len; i++) {
      final value = values[i];

      if (!value.isNaN) {
        var aNormValue =
            math.max(0.0, math.min(1.0, (value - minValue) / range));
        var iColor = aColorStart.interpolateRGB(aColorEnd, aNormValue);
        colors[i] = iColor;
      } else {
        colors[i] = 255;
      }
    }
  }

  return colors;
}

Uint32List getRamp3Colors(
    Float64List values,
    double minValue,
    double middleValue,
    double maxValue,
    int minColor,
    int middleColor,
    int maxColor) {
  final len = values.length;
  final colors = new Uint32List(len);

  var rgbMinColor = new scirgb.RGB.integer(minColor);
  var rgbMiddleColor = new scirgb.RGB.integer(middleColor);
  var rgbMaxColor = new scirgb.RGB.integer(maxColor);

  for (var i = 0; i < len; i++) {
    final value = values[i];

    if (!value.isNaN) {
      double aNormValue;
      scirgb.RGB aColorStart;
      scirgb.RGB aColorEnd;
      if (value < middleValue) {
        if (middleValue - minValue == 0) {
          aNormValue = 0.5;
        } else {
          aNormValue = math.max(0.0,
              math.min(1.0, (value - minValue) / (middleValue - minValue)));
        }
        aColorStart = rgbMinColor;
        aColorEnd = rgbMiddleColor;
      } else {
        if (maxValue - middleValue == 0) {
          aNormValue = 0.5;
        } else {
          aNormValue = math.max(0.0,
              math.min(1.0, (value - middleValue) / (maxValue - middleValue)));
        }
        aColorStart = rgbMiddleColor;
        aColorEnd = rgbMaxColor;
      }
      colors[i] = aColorStart.interpolateRGB(aColorEnd, aNormValue);
    }else {
      colors[i] = 255;
    }
  }

  return colors;
}

Uint32List getColors(List<int> colorList, Int32List levels) {
  final len = levels.length;
  final clen = colorList.length;
  var colors = new Uint32List(len);

  var order = scilist.order(levels);

  scilist.reorder(levels, order);

  var starts = scilist.starts(levels);

  final slen = starts.length - 1;

  for (var i = 0; i < slen; i++) {
    var start = starts[i];
    var end = starts[i + 1];
    var level = levels[start];

    int color;
    if (scinum.isInt32Null(level)){
      color = 255;
    } else  if (level < clen) {
      color = colorList[level];
      color = color << 8 | 255;
    } else {
      color = colorList[level % clen];
      color = color << 8 | 255;
    }

    for (var k = start; k < end; k++) {
      colors[k] = color;
    }
  }

  scilist.reverse_reorder(order);

  scilist.reorder(levels, order);

  scilist.reorder(colors, order);

  return colors;
}

/*
return rgba as integer
 */

Uint32List getJetColors(
    Float64List colorValues, double minValue, double maxValue) {
  var watch = new Stopwatch()..start();

  final len = colorValues.length;
  final colors = new Uint32List(len);

  if (minValue.isNaN || maxValue.isNaN) {
    return colors;
  }

  final diff = maxValue - minValue;

  if (diff == 0) {
    var c = getJetColor(0.5);
    for (var i = 0; i < len; i++) colors[i] = c;
  } else {
    for (var i = 0; i < len; i++) {
      var cValue = colorValues[i];
      if (cValue.isNaN) {
        colors[i] = 255; //255;
      } else {
        var value = math.max(0.0, math.min(1.0, (cValue - minValue) / (diff)));

        colors[i] = getJetColor(value);
      }
    }
  }

//  print('getJetColors -- ${watch.elapsedMilliseconds} ms');

  return colors;
}

int getJetColor(double value) {
  double a;
  int r = 0;
  int g = 0;
  int b = 0;
  if (value < 0.125) {
//    a = aNormValue / 0.125;
//    b = ((0.5 + (0.5 * a)) * 255).toInt();
    a = value / 0.25;
    b = ((0.5 + a) * 255).toInt();
  } else {
    if (value < 0.375) {
//      a = (aNormValue - 0.125) / 0.25;
//      g = (a * 255).toInt();
      a = (value - 0.125) * 1020;
      g = (a).toInt();
      b = 255;
    } else {
      if (value < 0.625) {
        a = (value - 0.375) / 0.25;
        r = (a * 255).toInt();
        g = 255;
        b = ((1 - a) * 255).toInt();
      } else {
        if (value < 0.875) {
          a = (value - 0.625) / 0.25;
          r = 255;
          g = ((1 - a) * 255).toInt();
        } else {
//          a = (aNormValue - 0.875) / 0.125;
//          r = ((1 - (0.5 * a)) * 255).toInt();
          a = (value - 0.875) / 0.25;
          r = ((1 - a) * 255).toInt();
        }
      }
    }
  }
//  assert(r <= 255);
//  assert(g <= 255);
//  assert(b <= 255);

  return (r << 24) | (g << 16) | (b << 8) | 255;
//  return ((r & 255) << 24) | ((g & 255) << 16) | ((b & 255) << 8) | 255;
}
