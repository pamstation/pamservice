part of sci_util_list;

td.Uint32List slice_uint8string_uint32(
        td.Uint8List bytes, td.Int32List starts, int nelm, int nth_uint32) =>
    SciList
        .getCurrent()
        .slice_uint8string_uint32(bytes, starts, nelm, nth_uint32);

List selectListByRids(List list, List<int> rids) =>
    SciList.getCurrent().selectListByRids(list, rids);

List<td.Int32List> joinRids(td.Int8List compList, td.Int32List leftStarts,
        td.Int32List rightStarts, int nelm, bool isOuterJoin) =>
    SciList
        .getCurrent()
        .joinRids(compList, leftStarts, rightStarts, nelm, isOuterJoin);

bool isSorted(List<Comparable> list) => SciList.getCurrent().isSorted(list);

td.Int32List randomInt32List(int n, [int max]) =>
    SciList.getCurrent().randomInt32List(n, max);

td.Float64List randomFloat32List(int len, double mean, double std_dev) =>
    SciList.getCurrent().randomFloat32List(len, mean, std_dev);

td.Float64List newFloat64List(int length, [double initial]) =>
    SciList.getCurrent().newFloat64List(length, initial);

td.Uint32List newUint32List(int length, [int initial]) =>
    SciList.getCurrent().newUint32List(length, initial);

Int32List newInt32Buffer(int length) =>
    new Int32List.reserv(length);

Uint8List newUint8Buffer(int length) =>
    new Uint8List.reserv(length);

td.Int32List newInt32List(int length, [int initial]) =>
    SciList.getCurrent().newInt32List(length, initial);

td.Uint64List newUint64List(int length, [int initial]) =>
    SciList.getCurrent().newUint64List(length, initial);

td.Int64List newInt64List(int length, [int initial]) =>
    SciList.getCurrent().newInt64List(length, initial);

td.Uint8List newUint8List(int length, [int initial]) =>
    SciList.getCurrent().newUint8List(length, initial);

td.Uint16List newUint16List(int length, [int initial]) =>
    SciList.getCurrent().newUint16List(length, initial);

void fillList(List list, value) => SciList.getCurrent().fillList(list, value);

td.Int32List newIntSequence(int len) =>
    SciList.getCurrent().newIntSequence(len);

bool isSequence(td.Int32List rids) => SciList.getCurrent().isSequence(rids);

void fill_sequence(List<int> seq) => SciList.getCurrent().fill_sequence(seq);

void fill_sequence64(td.Uint64List seq) =>
    SciList.getCurrent().fill_sequence64(seq);

void orderOnStarts(
        td.Int32List order, td.Int32List starts, List<Comparable> data,
        {int direction: 1}) =>
    SciList
        .getCurrent()
        .orderOnStarts(order, starts, data, direction: direction);

td.Int32List invertedOrderList(td.Int32List order) =>
    SciList.getCurrent().invertedOrderList(order);

Iterable<List<T>> split<T>(List list, int split) =>
    SciList.getCurrent().split<T>(list, split);

Iterable<Range> toRanges(List<int> ridRanges) =>
    SciList.getCurrent().toRanges(ridRanges);

void reverse_reorder(td.Int32List order) =>
    SciList.getCurrent().reverse_reorder(order);

td.Float64List add(td.Float64List data1, td.Float64List data2) {
  assert(data1.length == data2.length);
  final len = data1.length;
  var data = newFloat64List(len);

  for (var i = 0; i < len; i++) {
    data[i] = data1[i] + data2[i];
  }
  return data;
}

td.Float64List substract(List<double> data1, List<double> data2) {
  assert(data1.length == data2.length);
  final len = data1.length;
  var data = newFloat64List(len);

  for (var i = 0; i < len; i++) {
    data[i] = data1[i] - data2[i];
  }
  return data;
}

td.Float64List replaceNonFinite(List<double> data1, double value ) {
  final len = data1.length;
  var data = newFloat64List(len);

  for (var i = 0; i < len; i++) {
    if (data1[i].isFinite) {
      data[i] = data1[i];
    } else {
      data[i] = value;
    }
  }
  return data;
}

td.Float64List mean(List<double> values, List<int> order, List<int> starts) {
  final len = starts.length - 1;
  var meanValues = newFloat64List(len);

  for (var i = 0; i < len; i++) {
    final end = starts[i + 1];
    var mean = 0.0;
    for (var k = starts[i]; k < end; k++) mean += values[order[k]];
    meanValues[i] = mean / (end - starts[i]);
  }


  return meanValues;
}

void reorder(List keys, td.Int32List order) {
  if (keys is TypedList) {
    return SciList.getCurrent().reorder(keys.view as List, order);
  } else {
    return SciList.getCurrent().reorder(keys, order);
  }
}

void unreorder(List keys, td.Int32List order) {
  if (keys is TypedList) {
    return SciList.getCurrent().unreorder(keys.view as List, order);
  } else {
    return SciList.getCurrent().unreorder(keys, order);
  }
}

List sort(List data, td.Int32List order, [List answer]) =>
    SciList.getCurrent().sort(data, order, answer);

int max_start(td.Int32List starts) => SciList.getCurrent().max_start(starts);

td.Int32List prefixSum(td.Int32List list) =>
    SciList.getCurrent().prefixSum(list);

td.Float64List keepFinite(td.Float64List values) {
  var count = 0;
  values.forEach((v) {
    if (v.isFinite) count++;
  });
  var list = newFloat64List(count);
  if (count == values.length) {
    list.setRange(0, values.length, values);
  } else {
    count = 0;
    for (var i = 0; i < values.length; i++) {
      var v = values[i];
      if (v.isFinite) {
        list[count] = v;
        count++;
      }
    }
  }

  return list;
}

td.Int32List order(List data, {int direction: 1, bool stable: false}) {
//  if (data.length < 10000) {
//    if (data is TypedList) {
//      return mergeSort(data.view as List, direction: direction);
//
//    }
//
//    return mergeSort(data, direction: direction);
//  }
  if (data is TypedList) {
    return SciList
        .getCurrent()
        .order(data.view as List, direction: direction, stable: stable);
  } else {
    return SciList
        .getCurrent()
        .order(data, direction: direction, stable: stable);
  }
}

td.Int32List mergeSort(List data, {int direction: 1, bool stable: false}) {
  var order = newIntSequence(data.length);
//  if (isSorted(data)){
//    return order;
//  }


  if (data is Uint8StringList){
    collection.mergeSort(order, compare: data.compareAt);
  } else {
    collection.mergeSort(order);
  }
  return order;
}

int countIdenticalSegments(List data, [int from, int to]) =>
    SciList.getCurrent().countIdenticalSegments(data, from, to);

td.Int32List starts(List data) => SciList.getCurrent().starts(data);

td.Int32List startsWithStarts(List data, td.Int32List superStarts) =>
    SciList.getCurrent().startsWithStarts(data, superStarts);

void segmentCountDoOnFactor(
        List list, List dictionary, int aStart, int aStop, Function doMe) =>
    SciList
        .getCurrent()
        .segmentCountDoOnFactor(list, dictionary, aStart, aStop, doMe);

void segmentCountDoOn(
        Iterable<Comparable> list, int aStart, int aStop, void callback(value, int count)) =>
    SciList.getCurrent().segmentCountDoOn(list, aStart, aStop, callback);

//List asByteArray(List aList) => SciList.getCurrent().asByteArray(aList);

int findFirstIndexOnSortedList(Object object, List list, int start) =>
    SciList.getCurrent().findFirstIndexOnSortedList(object, list, start);

// (1,2,3,4,7,8,10) will output (true,4);(false,2);(true,2);(false,1);(true,1)
void findIncrementalSequences(List<int> orderedList, int start, int end,
        void fun(bool bit, int len)) =>
    SciList.getCurrent().findIncrementalSequences(orderedList, start, end, fun);

List<double> minMaxDouble(List<double> data) =>
    SciList.getCurrent().minMaxDouble(data);

List<double> normDouble(List<double> data) =>
    SciList.getCurrent().normDouble(data);