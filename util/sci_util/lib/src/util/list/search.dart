part of sci_util_list;

int binarySearchValue(
    List<Comparable> sortedValues, Comparable key, int imin, int imax) {
  while (imin < imax) {
    int imid = imin + ((imax - imin) ~/ 2);
    assert(imid < imax);
    if (sortedValues[imid].compareTo(key) < 0)
      imin = imid + 1;
    else
      imax = imid;
  }
  if ((imax == imin) && (sortedValues[imin] == key))
    return imin;
  else
    return -1;
}

int binarySearchFirstGreaterOrEqualsValue(
    List<Comparable> sortedValues, Comparable key, int imin, int imax) {
  for (int i = imin; i <= imax; i++) {
    if (key.compareTo(sortedValues[i]) <= 0) {
      return i;
    }
  }
  return -1;
}

int binarySearchFirstGreaterValue(
    List<Comparable> sortedValues, Comparable key, int imin, int imax) {
  for (int i = imin; i <= imax; i++) {
    if (key.compareTo(sortedValues[i]) < 0) {
      return i;
    }
  }
  return -1;
}

int binarySearchLastLessValue(
    List<Comparable> sortedValues, Comparable key, int imin, int imax) {

  if (key.compareTo(sortedValues[imin]) <= 0) {
    return -1;
  }

  for (int i = imin + 1; i <= imax; i++) {
    if (key.compareTo(sortedValues[i]) <= 0) {
      return i - 1;
    }
  }
  return imax;
}
