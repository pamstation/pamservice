part of sci_util_list;

//, StringList
abstract class Uint8StringList implements ListBase<String> {
  static const String TYPE = scitype.STRING;
  static const NULL = '';

//  factory Uint8StringList() {
//    return new PStringList();
//  }
//
//  factory Uint8StringList.reserv(int initiallength) {
//    return new PStringList.reserv(initiallength);
//  }
//
//  factory Uint8StringList.fromString(Iterable<String> l) {
//    return new PStringList.fromString(l);
//  }
//
//  factory Uint8StringList.fromList(Uint8List list, [Int32List starts]) {
//    return new PStringList.fromList(list, starts);
//  }
//
//  factory Uint8StringList.fromCStringList(CStringList list) {
//    return new PStringList.fromCStringList(list);
//  }
//
//  factory Uint8StringList.filled(int length, String fill) {
//    return new PStringList.filled(length, fill);
//  }
//
//  factory Uint8StringList.fromJson(Map map) {
//    return new PStringList.fromJson(map);
//  }

  factory Uint8StringList() {
    return new Uint8StringListImpl();
  }

  factory Uint8StringList.reserv(int initiallength) {
    return new Uint8StringListImpl.reserv(initiallength);
  }

  factory Uint8StringList.fromString(Iterable<String> l) {
    return new Uint8StringListImpl.fromString(l);
  }

  factory Uint8StringList.fromList(Uint8List list, [Int32List starts]) {
    return new Uint8StringListImpl.fromList(list, starts);
  }

  factory Uint8StringList.fromCStringList(CStringList list) {
    return new Uint8StringListImpl.fromTDList(list.toBytes(), list.starts);
  }

  factory Uint8StringList.filled(int length, String fill) {
    return new Uint8StringListImpl.filled(length, fill);
  }

  factory Uint8StringList.fromJson(Map map) {
    return new Uint8StringListImpl.fromJson(map);
  }

  void append(Uint8StringList list);

  void equalsTo(String value, void callback(int i, bool flag));

  td.Int32List order({int direction: 1, bool stable: false});

  void reorder(List<int> order);
  void basicReorder(List<int> order);

  Uint8StringList selectByRids(td.Int32List rids);
  Uint8StringList basicSelectByRids(td.Int32List rids);

  Uint8StringList copy();

  CStringList toCStringList();

  td.Uint8List get bytesView;
  td.Int32List get startsView;
  td.Uint8List get startsByteView;

  void addRepeat(String value, int n);

  bool equalsAt(int i, int j);
  int compareAt(int i, int j);

  void rebuildView();

  td.Uint8List bytesAt(int i);
  int bytesLengthAt(int i);
}

class Uint8StringListImpl extends ListBase<String> implements Uint8StringList {
  Uint8List _list;
  Int32List _starts;

  Uint8StringListImpl() {
    _list = new Uint8List.reserv(6 * 5);
    _starts = new Int32List.reserv(5 + 1);
    _starts.add(0);
  }

  Uint8StringListImpl.filled(int length, String fill) {
    _list = new Uint8List.reserv(6 * 5);
    _starts = new Int32List.reserv(5 + 1);
    _starts.add(0);
    addRepeat(fill, length);
  }

  Uint8StringListImpl.fromString(Iterable<String> l) {
    _list = new Uint8List.reserv(l.length * 6);
    _starts = new Int32List.reserv(l.length + 1);
    _starts.add(0);
    l.forEach(this.add);
  }

  Uint8StringListImpl.fromList(this._list, [this._starts]) {
    if (_starts == null) {
      _starts = _buildStarts();
    }
  }
  Uint8StringListImpl.fromTDList(td.Uint8List bytes, [td.Int32List starts]) {
    _list = new Uint8List.fromList(bytes);
    if (starts != null) _starts = new Int32List.fromList(starts);
    if (_starts == null) {
      _starts = _buildStarts();
    }
  }

  Uint8StringListImpl.reserv(int initiallength) {
    _list = new Uint8List.reserv(6 * initiallength);
    _starts = new Int32List.reserv(initiallength + 1);
    _starts.add(0);
  }

  Uint8StringListImpl.fromJson(Map map) {
    _list = new Uint8List.fromJson(map["list"]);
    if (map["starts"] == null) {
      _starts = _buildStarts();
    } else {
      _starts = new Int32List.fromJson(map["starts"]);
    }
  }

  void rebuildView(){}

  td.Int32List order({int direction: 1, bool stable: false}) {
    return SciList
        .getCurrent()
        .order_uint8String(this, direction: direction, stable: stable);
  }

//  Uint8StringListImpl copy(){
//    Uint8StringListImpl d = this;
//    var bytes = new Uint8List.fromList(d.list.toFixedSizeList());
//    var starts = new Int32List.fromList(d.starts.toFixedSizeList());
//    return new Uint8StringList.fromList(bytes, starts);
//  }

  CStringList toCStringList() =>
      new CStringList.fromBytes(this.list.toFixedSizeList());

  @override
  List<String> sublist(int start, [int end]) {
    if (end == null) end = length;

    final s = this.starts[start];
    final e = this.starts[end];

    final bytes = list.list.sublist(s, e);
    final sstarts = this.starts.list.sublist(start, end + 1);
    final firstStart = this.starts.list[start];
    final len = sstarts.length;
    for (var i = 0; i < len; i++) {
      sstarts[i] -= firstStart;
    }

    return new Uint8StringListImpl.fromList(
        new Uint8List.fromList(bytes), new Int32List.fromList(sstarts));
  }

  int compareAt(int i, int j){
    var starti = _starts[i];
    var endi = _starts[i + 1];

    var startj = _starts[j];
    var endj = _starts[j + 1];

    final len = math.min(endi - starti, endj - startj);

    final l = list._list;

    for (var ii = 0; ii < len; ii++) {
      var vi = l[ii + starti];
      var vj = l[ii + startj];
      var c = vi.compareTo(vj);
      if (c != 0) return c;
    }

    return (endi - starti) - (endj - startj);

  }

  bool equalsAt(int i, int j) {
    var starti = _starts[i];
    var endi = _starts[i + 1];

    var startj = _starts[j];
    var endj = _starts[j + 1];

    final len = endi - starti;

    if (len != (endj - startj)) return false;

    final l = list._list;

    for (var ii = 0; ii < len; ii++) {
      if (l[ii + starti] != l[ii + startj]) return false;
    }

    return true;
  }

  int bytesLengthAt(int i)=>  _starts[i + 1] - _starts[i];

  td.Uint8List bytesAt(int i) {
    var start = _starts[i];
    var end = _starts[i + 1];

    return new td.Uint8List.view(
        _list.list.buffer, _list.list.offsetInBytes + start, end - start - 1);
  }

  Int32List _buildStarts() {
    var len = 0;
    for (int i = 0; i < _list.length; i++) {
      if (_list[i] == 0) len++;
    }
    var _starts = new Int32List.reserv(len + 1);
    _starts.setLength(len + 1);
    _starts[0] = 0;
    var offset = 0;

    for (int i = 0; i < len; i++) {
      var start = offset;
      while (_list[offset] != 0) offset++;
      offset += 1;
      _starts[i + 1] = _starts[i] + (offset - start);
    }
    return _starts;
  }

  td.Int32List get startsView =>
      new td.Int32List.view(this.starts.list.buffer, 0, this.starts.length);

  td.Uint8List get startsByteView =>
      new td.Uint8List.view(this.starts.list.buffer, 0, this.starts.length * 4);

  td.Uint8List get bytesView =>
      new td.Uint8List.view(this.list.list.buffer, 0, this.list.length);

  Uint8StringListImpl copy() {
    var bytes = new Uint8List.fromList(_list.toFixedSizeList());
    var starts = new Int32List.fromList(_starts.toFixedSizeList());
    return new Uint8StringListImpl.fromList(bytes, starts);
  }

  Uint8StringListImpl basicSelectByRids(td.Int32List rids) {
    if (isSequence(rids)) {
      if (rids.first == 0 && rids.length == length) {
        return copy();
      }
    }

//    var watch = new Stopwatch()..start();

    final len = this.length;
    var nBytes = 0;
    final starts = this.starts.list;
    final nelm = rids.length;
    for (var i = 0; i < nelm; i++) {
      final k = rids[i];
      if (k >= len || scinum.isInt32Null(k)) {
        // empty string : add null terminated char
        nBytes++;
      } else {
        nBytes += starts[k + 1] - starts[k];
      }
    }

    var bytes = this.list.list;

    var newStarts = newInt32List(nelm + 1);
    var newBytes = newUint8List(nBytes);

    nBytes = 0;
    newStarts[0] = 0;

    for (var i = 0; i < nelm; i++) {
      final k = rids[i];
      if (k < len && !scinum.isInt32Null(k)) {
        final start = nBytes;
        var s = starts[k];
        var e = starts[k + 1];
        nBytes += e - s;
        newStarts[i + 1] = nBytes;

        var offset = start - s;

        for (var j = s; j < e; j++) {
          newBytes[offset + j] = bytes[j];
        }
      } else {
        // empty string
        nBytes++;
        newStarts[i + 1] = nBytes;
      }
    }

    var answer = new Uint8StringListImpl.fromList(
        new Uint8List.fromList(newBytes), new Int32List.fromList(newStarts));

//    print(
//        '${this.runtimeType} basicSelectByRids length ${length} rids.length ${rids.length} -- ${watch.elapsedMilliseconds} ms');
    return answer;
  }

  Uint8StringListImpl selectByRids(td.Int32List rids) =>
      selectListByRids(this, rids);

  void reorder(List<int> order) {
    SciList.getCurrent().reorder_uint8String(this, order);
  }

  void basicReorder(List<int> order) {
    var nelm = order.length;
    if (nelm > this.length) throw new RangeError.index(nelm, this);
    if (nelm < 1) return;

    var bytes = this.list.list;
    var starts = this.starts.list;

    var nBytes = starts[nelm];
    var nStarts = nelm + 1;

    var tmpBytes = newUint8List(nBytes);
    var tmpStarts = newInt32List(nStarts);

    var currentByteIndex = 0;

    for (var i = 0; i < nelm; ++i) {
      final j = order[i];
      var start = starts[j];
      var end = starts[j + 1];

      tmpStarts[i] = currentByteIndex;

      for (var k = start; k < end; ++k) {
        tmpBytes[currentByteIndex] = bytes[k];
        currentByteIndex++;
      }
      tmpStarts[i + 1] = currentByteIndex;
    }

    for (var i = 0; i < nBytes; ++i) bytes[i] = tmpBytes[i];

    for (var i = 0; i < nStarts; ++i) starts[i] = tmpStarts[i];

    this.list._list = bytes;
    this.starts._list = starts;
  }

  void unreorder(List<int> order) {
    reverse_reorder(order);
    reorder(order);
    reverse_reorder(order);
  }

  Map toJson() => {
        "type": Uint8StringList.TYPE,
        "list": _list.toJson(),
        "starts": _starts.toJson()
      };

  void _rangeCheck(int start, int end) {
    if (start < 0 || start > this.length) {
      throw new RangeError.range(start, 0, this.length);
    }
    if (end < start || end > this.length) {
      throw new RangeError.range(end, start, this.length);
    }
  }

  void setRange(int start, int end, Iterable<String> iterable,
      [int skipCount = 0]) {
    throw "$this not yey implemented";
//    _rangeCheck(start, end);
//    int length = end - start;
//    if (length == 0) return;
//
//    if (skipCount < 0) throw new ArgumentError(skipCount);
//
//    List otherList;
//    int otherStart;
//
//    if (iterable is List) {
//      otherList = iterable;
//      otherStart = skipCount;
//    } else {
//      otherList = iterable.skip(skipCount).toList(growable: false);
//      otherStart = 0;
//    }
//    if (otherStart + length > otherList.length) {
//      throw new StateError("Not enough elements");
//    }
//    if (otherStart < start) {
//      // Copy backwards to ensure correct copy if [from] is this.
//      for (int i = length - 1; i >= 0; i--) {
//        this[start + i] = otherList[otherStart + i];
//      }
//    } else {
//      for (int i = 0; i < length; i++) {
//        this[start + i] = otherList[otherStart + i];
//      }
//    }
  }

//  void forEachIndex(Function f) {
//    for (int i = 0; i < length; i++) f(i, this[i]);
//  }

  void addNull([int n]) {
    if (n != null) {
      addRepeat('', n);
    } else {
      this.add('');
    }
  }

  void equalsTo(String value, void callback(int i, bool flag)) {
    var bytes = utf8.encode(value);
    final blen = bytes.length;

    var len = _starts.length;
    for (var i = 1; i < len; i++) {
      final start = _starts[i - 1];
      final l = _starts[i] - start - 1; //null terminated : remove 0
      var flag = true;
      if (l != blen) {
        flag = false;
      } else {
        for (var k = blen - 1; k >= 0; k--) {
          if (_list[start + k] != bytes[k]) {
            flag = false;
            break;
          }
        }
      }
      callback(i, flag);
    }
  }

  bool equalsToAt(List<int> bytes, int i) {
    final blen = bytes.length;

    final start = _starts[i];
    final l = _starts[i + 1] - start - 1; //null terminated : remove 0
    var flag = true;
    if (l != blen) {
      flag = false;
    } else {
      for (var k = blen - 1; k >= 0; k--) {
        if (_list[start + k] != bytes[k]) {
          flag = false;
          break;
        }
      }
    }
    return flag;
  }

  void add(String value) {
    addUTF8Bytes(utf8.encode(value));
  }

  void addRepeat(String value, int n) {
    if (n < 1) return;
    var bytes = utf8.encode(value);

    final len = bytes.length;

    _list.ensureCapacityForMore(n * len + n);
    _starts.ensureCapacityForMore(n);

    var bytesData = _list._list;
    var bytesStarts = _starts._list;

    var offset = _starts.last;
    var sOffset = _starts.length;

    for (var i = 0; i < n; i++) {
      for (var k = 0; k < len; k++) {
        bytesData[offset + k] = bytes[k];
      }
      bytesData[offset + len] = 0;
      bytesStarts[sOffset] = bytesStarts[sOffset - 1] + len + 1;

      offset += len + 1;
      sOffset++;
    }

    _list._length += n * len + n;
    _starts._length += n;
  }

  void addUTF8Bytes(List<int> bytes) {
    var offset = _list.length;
    _list.setRange(offset, offset + bytes.length, bytes);
    _list.add(0);
    _starts.add(_starts.last + bytes.length + 1);
  }

  void addAll(Iterable<String> values) {
    if (values is Uint8StringListImpl) {
      append(values);
    } else {
      values.forEach(add);
    }
  }

  void append(Uint8StringList ll) {
    Uint8StringListImpl l = ll;
    if (l.isEmpty) return;

    _list.setRange(_list.length, _list.length + l.list.length, l.list.list);

    starts.ensureCapacityForMore(l.starts.length - 1);

    final len = l.starts.length;
    final offset = _starts.length - 1; //length

    final myStartList = _starts.list;
    final startList = l.starts.list;

    final lastStart = _starts.last;

    for (var i = 1; i < len; i++) {
      myStartList[offset + i] = startList[i] + lastStart;
    }

    _starts._length += len - 1;
  }

  void operator []=(int index, String value) {
    throw "$this not yey implemented";
  }

  String operator [](int i) {
    var start = _starts[i];
    var end = _starts[i + 1];

    return utf8.decode(new td.Uint8List.view(
        _list.list.buffer, _list.list.offsetInBytes + start, end - start - 1), allowMalformed:true);
  }

  Uint8List get list => _list;
  Int32List get starts => _starts;
  int get length => _starts.length - 1;
  set length(int i) {
    throw "$this not yet implemented";
  }

  int get lengthInBytes => _list.lengthInBytes + _starts.lengthInBytes;

  void clear() {
    list.clear();
    starts.clear();
    _starts.add(0);
  }

  int serializationSizeInBytes([int alignment]) {
    var paddingForStringValues = 0;
    if (alignment != null) {
      paddingForStringValues = _starts.last % alignment == 0
          ? 0
          : alignment - _starts.last % alignment;
    }
    return 4 + _starts.last + 4 * (_starts.length) + paddingForStringValues;
  }

  td.ByteBuffer serialize([int alignment]) {
    td.Uint8List ser = newUint8List(serializationSizeInBytes(alignment));
    td.ByteData bd = new td.ByteData.view(ser.buffer);
    int offset = 0;
    bd.setUint32(offset, length, td.Endian.little);
    offset = offset + 4;
    //first the starts
    td.Uint8List startsUint8 =
        new td.Uint8List.view(_starts.list.buffer, 0, 4 * (_starts.length));
    ser.setRange(offset, offset + startsUint8.length, startsUint8);
    offset = offset + startsUint8.length;
    td.Uint8List stringUint8 =
        new td.Uint8List.view(_list.list.buffer, 0, _starts.last);
    ser.setRange(offset, offset + stringUint8.length, stringUint8);
    return ser.buffer;
  }
}
