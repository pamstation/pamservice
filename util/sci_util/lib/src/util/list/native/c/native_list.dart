library native_list;

import 'package:sci_native/sci_native.dart' show SciCLib;

import '../dart/dart_list.dart' as dlib;

import 'dart:typed_data';
import '../../list.dart' as scilist;

class SciList extends dlib.SciList {
  static SciList CURRENT = new SciList();

  SciList() {
    print('native SciList');
  }

  static SciList getCurrent() => CURRENT;

  @override
  void fill_sequence(List<int> ilist) {
    checkCancel();
    Int32List list = ilist;
    SciCLib.fill_sequence_int32(list);
  }

  @override
  Int32List order(List<Comparable> list,
      {int direction: 1, bool stable: false}) {
    checkCancel();
    Int32List order;
    if (list is Uint32List) {
      order = new Int32List(list.length);
      SciCLib.radixOrderUint32(list, order);
    }
    if (list is Int32List) {
      order = new Int32List(list.length);
      SciCLib.radixOrderInt32(list, order);
    } else if (list is Float64List) {
      order = new Int32List(list.length);
      SciCLib.radixOrderDouble(list, order);
    } else
      return super.order(list, direction: direction, stable: stable);

    if (direction != 1) {
      order = order.reversed.toList();
    }
    return order;
  }

  @override
  void reorder_uint8String(scilist.Uint8StringList list, Int32List order) {
    checkCancel();
    SciCLib.reorder_uint8string(
        list.bytesView, list.startsView, list.length, order);
  }

  @override
  void reorder(List list, Int32List order) {
    checkCancel();
    if (list is Uint32List) {
      SciCLib.reorder_uint32(list, order);
    } else if (list is Int32List) {
      SciCLib.reorder_int32(list, order);
    } else if (list is Float64List) {
      SciCLib.reorder_float64(list, order);
    } else if (list is scilist.Uint8StringList) {
      list.reorder(order);
    } else {
      super.reorder(list, order);
    }
  }

  @override
  void unreorder(List list, Int32List order) {
    checkCancel();
    if (list is Uint32List) {
      SciCLib.unreorder_uint32(list, order);
    } else if (list is Int32List) {
      SciCLib.unreorder_int32(list, order);
    } else if (list is Float64List) {
      SciCLib.unreorder_float64(list, order);
    } else {
      super.unreorder(list, order);
    }
  }

  @override
  void reverse_reorder(Int32List order) {
    checkCancel();
    final nelm = order.length;
    var tmp = new Int32List(nelm);
    for (var i = 0; i < nelm; ++i) tmp[order[i]] = i;
    order.setRange(0, order.length, tmp);
  }

  @override
  int count_sequence(List list) {
    checkCancel();
    if (list is Int32List) {
      return SciCLib.count_sequence_int32(list);
    } else if (list is Float64List) {
      return SciCLib.count_sequence_double(list);
    } else if (list is scilist.Uint8StringList) {
      return SciCLib.count_sequence_uint8string(list.bytesView, list.startsView,
          new Int32List.fromList([0, list.length]));
    } else {
      return super.count_sequence(list);
    }
  }

  @override
  Int32List startsWithStarts(List<Comparable> data, Int32List superStarts) {
    checkCancel();
    if (data is scilist.Uint8StringList) {
      if (data.isEmpty) return newInt32List(1);

      var count = SciCLib.count_sequence_uint8string(
          data.bytesView, data.startsView, superStarts);

      List starts = newInt32List(count + 1);

      SciCLib.starts_uint8string_with_starts(data.bytesView, data.startsView,
          starts, superStarts, superStarts.length);

      return starts;
    } else {
      return super.startsWithStarts(data, superStarts);
    }
  }

  @override
  Int32List starts(List<Comparable> data) {
    checkCancel();
    if (data is scilist.Uint8StringList) {
      return startsWithStarts(data, new Int32List.fromList([0, data.length]));
    } else if (data is TypedData) {
      return super.starts(data);
    } else {
      throw 'typed data and Uint8StringList only';
    }
  }

  @override
  int maxStringLength(scilist.Uint8StringList list) {
    checkCancel();
    return super.maxStringLength(list);
  }

  @override
  int max_start(Int32List starts) {
    checkCancel();
    return SciCLib.max_start_int32(starts);
  }

// This function is an utility used to sort uint8string using radix sort.
// Read bytes using starts, and extract the nth_uint32 from each string, store the result into keys.
// If nth_uint32 = 0, keys will contain the first 4 bytes (ie: uint32) of each string
// If nth_uint32 = 1, keys will contain the bytes from 4 to 7 (ie: uint32) of each string
// Etc ..
// nelm : number of string stored in bytes = length of keys = length of starts -1
  @override
  Uint32List slice_uint8string_uint32(
      Uint8List bytes, Int32List starts, int nelm, int nth_uint32) {
    checkCancel();
//    print('native slice_uint8string_uint32 ');
    var keys = new Uint32List(nelm);
    SciCLib.slice_uint8string_uint32(bytes, starts, nelm, nth_uint32, keys);
    return keys;
  }
}
