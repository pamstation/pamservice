library dart_list;

import 'dart:async';

import 'dart:typed_data';
import 'dart:math';
import '../../list.dart' as scilist;
import '../../../sort/radix/radix_sort.dart' as scirad;
import '../../../num/num.dart' as scinum;
import '../../../cancel/cancel.dart' as cancel;

part 'compare.dart';

//class MemoryMonitorError {}
//
//class MemoryMonitorCancelError extends MemoryMonitorError {}
//
//class MemoryMonitorOutOfMemoryError extends MemoryMonitorError {}
//
//class MemoryMonitor {
//  bool isCancel;
//  List<TypedData> list;
//  int maxBytes;
//
//  MemoryMonitor() {
//    list = [];
//    isCancel = false;
//    maxBytes = 0;
//  }
//
//  void add(TypedData data) {
//    if (isCancel) throw new MemoryMonitorCancelError();
//    if (!canAdd(data)) throw new MemoryMonitorOutOfMemoryError();
//  }
//
//  int get totalBytes { }
//
//  bool canAdd(TypedData data) {
//    return data.lengthInBytes + totalBytes <= maxBytes;
//  }
//}

class SciList {
  static SciList CURRENT = new SciList();

  static SciList getCurrent() => CURRENT;

  SciList() {
    print('dart SciList');
  }

  void checkCancel() {
    cancel.checkCancel();
  }

  bool isSequence(Int32List rids) {
    if (rids.isEmpty) return false;
    if (rids.length == 1) {
      return true;
    }

    for (var i = 1; i < rids.length; i++) {
      if (rids[i] - rids[i - 1] != 1) return false;
    }

    return true;
  }

// list must be a TypedData or a Uint8StringList
  List selectListByRids(List list, List<int> rids) {
    checkCancel();
    if (list is scilist.Uint8StringList) return list.basicSelectByRids(rids);

    List result = newTypedDataListSameAs(list, rids.length);
    var nullValue = scinum.nullForTypedData(result as TypedData);

    final len = list.length;
    final ridsLen = rids.length;

    for (var i = 0; i < ridsLen; i++) {
      final rid = rids[i];
      if (rid < len && rid != scinum.INT32_NULL) {
        result[i] = list[rid];
      } else {
        result[i] = nullValue;
      }
    }

    return result;
  }

  scilist.Uint8StringList selectUint8StringListByRids(
      scilist.Uint8StringList list, List<int> rids) {
    checkCancel();
    return list.basicSelectByRids(rids);
  }

  List<Int32List> joinRids(Int8List compList, Int32List leftStarts,
      Int32List rightStarts, int nelm, bool isOuterJoin) {
    checkCancel();
    var leftRids = scilist.newInt32List(nelm);
    var rightRids = scilist.newInt32List(nelm);

    var result = [leftRids, rightRids];

    final leftlen = leftStarts.length - 1;
    final rightlen = rightStarts.length - 1;

    var leftIndex = 0;
    var rightIndex = 0;

    var compIndex = 0;
    var count = 0;

    while (leftIndex < leftlen && rightIndex < rightlen) {
      var startLeft = leftStarts[leftIndex];
      var startRight = rightStarts[rightIndex];

      int comp = compList[compIndex];
      compIndex++;

      if (comp == 0) {
        var endLeft = leftStarts[leftIndex + 1];
        var endRight = rightStarts[rightIndex + 1];
        for (var i = startLeft; i < endLeft; i++) {
          for (var j = startRight; j < endRight; j++) {
            leftRids[count] = i;
            rightRids[count] = j;
            count++;
          }
        }

        leftIndex++;
        rightIndex++;
      } else if (comp > 0) {
        rightIndex++;
      } else if (comp < 0) {
        if (isOuterJoin) {
          final endLeft = leftStarts[leftIndex + 1];
          for (var i = startLeft; i < endLeft; i++) {
            leftRids[count] = i;
            rightRids[count] = scinum.INT32_NULL;
            count++;
          }
        }
        leftIndex++;
      }
    }

    if (isOuterJoin) {
      while (leftIndex < leftlen) {
        final startLeft = leftStarts[leftIndex];
        final endLeft = leftStarts[leftIndex + 1];
        for (var i = startLeft; i < endLeft; i++) {
          leftRids[count] = i;
          rightRids[count] = scinum.INT32_NULL;
          count++;
        }
        leftIndex++;
      }
    }

    return result;
  }

  Int32List order_uint8String(scilist.Uint8StringList list,
      {int direction: 1, bool stable: false}){
    Int32List order = scirad.order_uint8String(list);
    if (direction != 1) {
      order = order.reversed.toList();
    }
    return order;
  }

  Int32List order(List<Comparable> list,
      {int direction: 1, bool stable: false}) {
    checkCancel();
    Int32List order;
    if (list is Int32List) {
      order = scirad.order_int32(list);
    } else if (list is Float64List) {
      order = scirad.order_float64(list);
    } else if (list is Float32List) {
      order = scirad.order_float32(list);
    } else if (list is Uint64List) {
      order = scirad.order_uint64(list);
    } else if (list is scilist.Uint8StringList) {
      order = list.order();
    } else if (list is Uint32List) {
      order = scirad.order_uint32(list);
    } else if (list is Uint16List) {
      order = scirad.order_uint16(list);
    }
// else if (list is Uint8List) {
//      order = scirad.order_uint8(list);
//    } else if (list is scilist.Uint8StringList) {
//      order = scirad.order_uint8String(list);
//    } else if (list is Uint32List) {
//      order = scirad.order_uint32(list);
//    }
    else
      throw 'order : unknown list type : ${list.runtimeType}';
    if (direction != 1) {
      order = order.reversed.toList();
    }
    return order;
  }

  List<num> newTypedDataListSameAs(List list, [int len]) {
    checkCancel();
    len = len == null ? list.length : len;
    if (list is Int32List) {
      return new Int32List(len);
    } else if (list is Uint32List) {
      return new Uint32List(len);
    } else if (list is Float64List) {
      return new Float64List(len);
    } else if (list is Float32List) {
      return new Float32List(len);
    } else if (list is Uint64List) {
      return new Uint64List(len);
    } else if (list is Uint8List) {
      return new Uint8List(len);
    } else if (list is Uint16List) {
      return new Uint16List(len);
    } else if (list is Int16List) {
      return new Int16List(len);
    } else if (list is Int64List) {
      return new Int64List(len);
    } else if (list is Int8List) {
      return new Int8List(len);
    }
    return new List(list.length);
  }

  Iterable<scilist.Range> toRanges(List<int> sortedRids) {
    var list = <scilist.Range>[];
    if (sortedRids.isEmpty) return list;

    var start = sortedRids[0];
    var rlen = 1;
    for (var i = 1; i < sortedRids.length; i++) {
      if (sortedRids[i] - sortedRids[i - 1] == 1) {
        rlen++;
      } else {
        list.add(new scilist.Range(start, rlen));
        rlen = 1;
        start = sortedRids[i];
      }
    }

    list.add(new scilist.Range(start, rlen));
    return list;
  }

//  Iterable<scilist.Range> toRanges(List<int> sortedRids) sync* {
//    var watch = new Stopwatch()..start();
//    if (sortedRids.isEmpty) return;
//    var start = sortedRids[0];
//    var rlen = 1;
//    for (var i = 1; i < sortedRids.length; i++) {
//      if (sortedRids[i] - sortedRids[i - 1] == 1) {
//        rlen++;
//      } else {
//        yield new scilist.Range(start, rlen);
//        rlen = 1;
//        start = sortedRids[i];
//      }
//    }
//
//    yield new scilist.Range(start, rlen);
//
//    print('scilist.toRanges -- ${watch.elapsedMilliseconds} ms');
//  }

  Iterable<List<T>> split<T>(List list, int split) sync* {
    checkCancel();
    var start = 0;
    while (start < list.length) {
      var end = min(start + split, list.length);
      var l = (list is TypedData)
          ? newTypedDataListSameAs(list, end - start)
          : new List<T>(end - start);

      l.setRange(0, l.length, list, start);
      start = end;
      yield l;
    }
  }

  void reorder_uint8String(scilist.Uint8StringList keys, Int32List order) {
    checkCancel();
    keys.basicReorder(order);
  }

  void reorder(List keys, Int32List order) {
    checkCancel();
    if (keys is scilist.Uint8StringList) {
      keys.reorder(order);
      return;
    }

    final nelm = keys.length;
    final tmp = newTypedDataListSameAs(keys);
    for (var i = 0; i < nelm; ++i) tmp[i] = keys[order[i]];
    keys.setRange(0, nelm, tmp);
  }

  void unreorder(List keys, Int32List order) {
    checkCancel();
    if (keys is scilist.Uint8StringList) {
      reverse_reorder(order);
      keys.reorder(order);
      reverse_reorder(order);
      return;
    }

    final nelm = keys.length;
    var tmp = newTypedDataListSameAs(keys);
    for (var i = 0; i < nelm; ++i) tmp[order[i]] = keys[i];
    keys.setRange(0, nelm, tmp);
  }

  void reverse_reorder(Int32List order) {
    checkCancel();
    final nelm = order.length;
    var tmp = new Int32List(nelm);
    for (var i = 0; i < nelm; ++i) tmp[order[i]] = i;
    order.setRange(0, nelm, tmp);
  }

  void fill_sequence(List<int> order) {
    checkCancel();
    final nelm = order.length;
    for (var i = 0; i < nelm; ++i) order[i] = i;
  }

  void fill_sequence64(Uint64List order) {
    checkCancel();
    final nelm = order.length;
    for (var i = 0; i < nelm; ++i) order[i] = i;
  }

  int count_sequence(List list) {
    checkCancel();
    final nelm = list.length;
    if (nelm < 1) return 0;

    var value = list[0];
    var count = 1;

    for (var i = 1; i < nelm; ++i) {
      if (list[i] != value) {
        count++;
        value = list[i];
      }
    }

    return count;
  }

  int maxStringLength(scilist.Uint8StringList list) {
    checkCancel();
    var starts = list.startsView;
//        new Int32List.view(list.starts.list.buffer, list.starts.length);
    return max_start(starts);
  }

  int max_start(Int32List starts) {
    checkCancel();
    final nelm = starts.length;
    if (nelm < 1) return 0;
    var max = 0;
    var start = starts[0];
    for (var i = 1; i < nelm; ++i) {
      var end = starts[i];
      var len = end - start;
      start = end;
      if (len > max) max = len;
    }
    return max;
  }

// This function is an utility used to sort uint8string using radix sort.
// Read bytes using starts, and extract the nth_uint32 from each string, store the result into keys.
// If nth_uint32 = 0, keys will contain the first 4 bytes (ie: uint32) of each string
// If nth_uint32 = 1, keys will contain the bytes from 4 to 7 (ie: uint32) of each string
// Etc ..
// nelm : number of string stored in bytes = length of keys = length of starts -1
  Uint32List slice_uint8string_uint32(
      Uint8List bytes, Int32List starts, int nelm, int nth_uint32) {
    checkCancel();
    return scirad.slice_uint8string_uint32(bytes, starts, nelm, nth_uint32);
  }

  bool isSorted(List<Comparable> list) {
    checkCancel();
    if (list.isEmpty) return true;
    for (var i = 1; i < list.length; i++) {
      if (list[i - 1].compareTo(list[i]) > 0) return false;
    }
    return true;
  }

  Int32List randomInt32List(int n, [int max]) {
    checkCancel();
    max = max == null ? 10000000 : max;
    var rnd = new Random();
    var list = newInt32List(n);
    for (int i = 0; i < list.length; i++) {
      list[i] = rnd.nextInt(max);
    }
    return list;
  }

  Float64List randomFloat32List(int len, double mean, double std_dev) {
    checkCancel();
    Random rnd = new Random();
    var list = newFloat64List(len);
    for (int i = 0; i < list.length; i++) {
      list[i] = ((rnd.nextDouble() - 0.5) * std_dev) + mean;
    }
    return list;
  }

  Float64List newFloat64List(int length, [double initial]) {
    checkCancel();
    cancel.checkArrayLength(length);
    var list = new Float64List(length);
    if (initial != null) fillList(list, initial);
    return list;
  }

  Int64List newInt64List(int length, [int initial]) {
    checkCancel();
    cancel.checkArrayLength(length);
    var list = new Int64List(length);
    if (initial != null) fillList(list, initial);
    return list;
  }

  Uint64List newUint64List(int length, [int initial]) {
    checkCancel();
    cancel.checkArrayLength(length);
    var list = new Uint64List(length);
    if (initial != null) fillList(list, initial);
    return list;
  }

  Int32List newInt32List(int length, [int initial]) {
    checkCancel();
    cancel.checkArrayLength(length);
    var list = new Int32List(length);
    if (initial != null) fillList(list, initial);
    return list;
  }

  Uint32List newUint32List(int length, [int initial]) {
    checkCancel();
    cancel.checkArrayLength(length);
    var list = new Uint32List(length);
    if (initial != null) fillList(list, initial);
    return list;
  }

  Uint8List newUint8List(int length, [int initial]) {
    checkCancel();
    var list = new Uint8List(length);
    if (initial != null) fillList(list, initial);
    return list;
  }

  Uint16List newUint16List(int length, [int initial]) {
    checkCancel();
    var list = new Uint16List(length);
    if (initial != null) fillList(list, initial);
    return list;
  }

  void fillList(List list, value) {
    checkCancel();
    for (int i = 0; i < list.length; i++) {
      list[i] = value;
    }
  }

  Int32List newIntSequence(int len) {
    checkCancel();
    var seq = newInt32List(len);
    this.fill_sequence(seq);
    return seq;
  }

//  void orderOnStarts(List<int> order, List<int> starts, List<Comparable> data,
//      {int direction: 1}) {
//    assert(order.length == data.length);
//    assert(starts.last == data.length);
//    fillSequence(order);
//    for (int i = 0; i < starts.length - 1; i++) {
//      var start = starts[i];
//      var end = starts[i + 1];
//      collection.mergeSort(order, start: start, end: end,
//          compare: (int i, int j) {
//            Comparable a = data[i];
//            Comparable b = data[j];
//            return direction * a.compareTo(b);
//          });
//    }
//  }

  void orderOnStarts(Int32List order, Int32List starts, List<Comparable> data,
      {int direction: 1}) {
    assert(order.length == data.length);
    assert(starts.last == data.length);

    fill_sequence(order);

    var listOffset = new scilist.ListOffset<int>(data, 0, 0);
    for (int i = 0; i < starts.length - 1; i++) {
      listOffset.start = starts[i];
      listOffset.end = starts[i + 1];
      listOffset.sort((int i, int j) {
        Comparable a = data[i];
        Comparable b = data[j];
        return direction * a.compareTo(b);
      });
    }
  }

  Int32List invertedOrderList(Int32List order) {
    var answer = newInt32List(order.length);
    answer.setRange(0, order.length, order);
    this.reverse_reorder(answer);
    return answer;
  }

  List sort(List data, Int32List order, [List answer]) {
    checkCancel();
    if (answer == null) {
      answer = newTypedDataListSameAs(data);
    }
    for (int i = 0; i < data.length; i++) {
      answer[i] = data[order[i]];
    }
    return answer;
  }

  Int32List prefixSum(Int32List list) {
    var prefixSum = newInt32List(list.length);
    if (list.isEmpty) return prefixSum;
    var start = list[0];
    for (var i = 1; i < list.length; i++) {
      prefixSum[i] = prefixSum[i - 1] + start;
      start = list[i];
    }
    return prefixSum;
  }

  int countIdenticalSegments(List<Comparable> data, [int from, int to]) {
    var _from = from == null ? 0 : from;
    var _to = to == null ? data.length : to;
    if (_from == to) return 0;

    var current = data[_from];
    var segmentCount = 1;
    for (int i = _from + 1; i < _to; i++) {
      var d = data[i];
      if (d.compareTo(current) != 0) {
        segmentCount++;
        current = d;
      }
    }
    return segmentCount;
  }

  bool listEqualsAt(List list, int i, int j) => list[i] == list[j];

  Int32List starts(List<Comparable> data) {
    checkCancel();
    if (data is scilist.Uint8StringList) {
      return _starts(data,
          (list, i, j) => (list as scilist.Uint8StringList).equalsAt(i, j));
    } else if (data is TypedData) {
      return _startsIdentical(data);
    } else {
      throw 'typed data and Uint8StringList only';
    }
  }

  Int32List startsWithStarts(List<Comparable> data, Int32List superStarts) {
    checkCancel();
    if (data is scilist.Uint8StringList) {
      return _startsWithStarts(data, superStarts,
          (list, i, j) => (list as scilist.Uint8StringList).equalsAt(i, j));
    } else if (data is TypedData) {
      return _startsWithStartsIdentical(data, superStarts);
    } else {
      throw 'typed data and Uint8StringList only';
    }
  }

  int _countEqualsSegments(List data, bool equalsAt(List list, int i, int j),
      [int from, int to]) {
    //
    var _from = from == null ? 0 : from;
    var _to = to == null ? data.length : to;
    if (_from == to) return 0;

    var current = _from;
    var segmentCount = 1;
    for (int i = _from + 1; i < _to; i++) {
      if (!equalsAt(data, current, i)) {
        segmentCount++;
        current = i;
      }
    }
    return segmentCount;
  }

  Int32List _starts(List data, bool equalsAt(List list, int i, int j)) {
    checkCancel();
    if (data.isEmpty) return newInt32List(1);
    int startLen = _countEqualsSegments(data, equalsAt) + 1;

    List starts = newInt32List(startLen);
    starts[0] = 0;
    var current = 0;
    var currentStartIndex = 1;
    var count = 0;
    final len = data.length;

    for (int i = 0; i < len; i++) {
      if (equalsAt(data, i, current)) {
        count++;
      } else {
        starts[currentStartIndex] = starts[currentStartIndex - 1] + count;
        currentStartIndex++;
        current = i;
        count = 1;
      }
    }
    starts[currentStartIndex] = starts[currentStartIndex - 1] + count;
    return starts;
  }

  Int32List _startsWithStarts(List data, Int32List superStarts,
      bool equalsAt(List list, int i, int j)) {
    checkCancel();
    if (data.isEmpty) return newInt32List(1);

    var startsLen = 1;
    for (int i = 0; i < superStarts.length - 1; i++) {
      startsLen += _countEqualsSegments(
          data, equalsAt, superStarts[i], superStarts[i + 1]);
    }
    List starts = newInt32List(startsLen);
    starts[0] = 0;
    var currentStartIndex = 1;

    for (int k = 0; k < superStarts.length - 1; k++) {
      int from = superStarts[k];
      int to = superStarts[k + 1];
      var current = from;
      var count = 0;
      for (int i = from; i < to; i++) {
        if (equalsAt(data, i, current)) {
          count++;
        } else {
          starts[currentStartIndex] = starts[currentStartIndex - 1] + count;
          currentStartIndex++;
          current = i;
          count = 1;
        }
      }
      starts[currentStartIndex] = starts[currentStartIndex - 1] + count;
      currentStartIndex++;
    }

    return starts;
  }

  int _countIdenticalSegments(List data, [int from, int to]) {
    //
    if (data is Float64List)
      return _countIdenticalSegmentsFloat64(data, from, to);
    var _from = from == null ? 0 : from;
    var _to = to == null ? data.length : to;
    if (_from == to) return 0;

    var current = data[_from];
    var segmentCount = 1;
    for (int i = _from + 1; i < _to; i++) {
      var d = data[i];
      if (d != current) {
        segmentCount++;
        current = d;
      }
    }
    return segmentCount;
  }

  int _countIdenticalSegmentsFloat64(List<double> data, [int from, int to]) {
    //
    var _from = from == null ? 0 : from;
    var _to = to == null ? data.length : to;
    if (_from == to) return 0;

    var current = data[_from];
    var segmentCount = 1;
    for (int i = _from + 1; i < _to; i++) {
      var d = data[i];
      if (!(d == current || (d.isNaN && current.isNaN))) {
        segmentCount++;
        current = d;
      }
    }
    return segmentCount;
  }

  Int32List _startsIdentical(List data) {
    if (data is Float64List) return _startsIdenticalFloat64(data);
    checkCancel();
    if (data.isEmpty) return newInt32List(1);
    int startLen = _countIdenticalSegments(data) + 1;

    List starts = newInt32List(startLen);
    starts[0] = 0;
    var current = data[0];
    var currentStartIndex = 1;
    var count = 0;
    final len = data.length;

    for (int i = 0; i < len; i++) {
      var d = data[i];
      if (d == current) {
        count++;
      } else {
        starts[currentStartIndex] = starts[currentStartIndex - 1] + count;
        currentStartIndex++;
        current = d;
        count = 1;
      }
    }
    if (currentStartIndex < starts.length)
      starts[currentStartIndex] = starts[currentStartIndex - 1] + count;
    return starts;
  }

  Int32List _startsIdenticalFloat64(List<double> data) {
    checkCancel();
    if (data.isEmpty) return newInt32List(1);
    int startLen = _countIdenticalSegments(data) + 1;

    List starts = newInt32List(startLen);
    starts[0] = 0;
    var current = data[0];
    var currentStartIndex = 1;
    var count = 0;
    final len = data.length;

    for (int i = 0; i < len; i++) {
      var d = data[i];
      if (d == current || (d.isNaN && current.isNaN)) {
        count++;
      } else {
        starts[currentStartIndex] = starts[currentStartIndex - 1] + count;
        currentStartIndex++;
        current = d;
        count = 1;
      }
    }
    if (currentStartIndex < starts.length)
      starts[currentStartIndex] = starts[currentStartIndex - 1] + count;
    return starts;
  }

  Int32List _startsWithStartsIdentical(List data, Int32List superStarts) {
    if (data is Float64List)
      return _startsWithStartsIdenticalFloat64(data, superStarts);
    checkCancel();
    if (data.isEmpty) return newInt32List(1);

    var count = 0;
    final slen = superStarts.length - 1;

    for (int i = 0; i < slen; i++) {
      count +=
          _countIdenticalSegments(data, superStarts[i], superStarts[i + 1]);
    }
    List starts = newInt32List(count + 1);
    starts[0] = 0;
    var currentStartIndex = 1;

    for (int k = 0; k < slen; k++) {
      int from = superStarts[k];
      int to = superStarts[k + 1];
      var current = data[from];
      var count = 0;
      for (int i = from; i < to; i++) {
        var d = data[i];
        if (d == current) {
          count++;
        } else {
          starts[currentStartIndex] = starts[currentStartIndex - 1] + count;
          currentStartIndex++;
          current = d;
          count = 1;
        }
      }
      if (currentStartIndex < starts.length)
        starts[currentStartIndex] = starts[currentStartIndex - 1] + count;
      currentStartIndex++;
    }

    return starts;
  }

  Int32List _startsWithStartsIdenticalFloat64(
      List<double> data, Int32List superStarts) {
    checkCancel();
    if (data.isEmpty) return newInt32List(1);

    var count = 0;
    final slen = superStarts.length - 1;

    for (int i = 0; i < slen; i++) {
      count +=
          _countIdenticalSegments(data, superStarts[i], superStarts[i + 1]);
    }
    List starts = newInt32List(count + 1);
    starts[0] = 0;
    var currentStartIndex = 1;

    for (int k = 0; k < slen; k++) {
      int from = superStarts[k];
      int to = superStarts[k + 1];
      var current = data[from];
      var count = 0;
      for (int i = from; i < to; i++) {
        var d = data[i];
        if (d == current || (d.isNaN && current.isNaN)) {
          count++;
        } else {
          starts[currentStartIndex] = starts[currentStartIndex - 1] + count;
          currentStartIndex++;
          current = d;
          count = 1;
        }
      }
      if (currentStartIndex < starts.length)
        starts[currentStartIndex] = starts[currentStartIndex - 1] + count;
      currentStartIndex++;
    }

    return starts;
  }

  void segmentCountDoOnFactor(
      List list, List dictionary, int aStart, int aStop, Function doMe) {
    if (list.length <= aStart) return;
    if (list.length <= aStop) throw new Exception('stop too large');

    var aVal = list[aStart];
    if (list.length <= (aStart + 1)) {
      doMe(dictionary[aVal], 1);
      return;
    }

    int aCount = 1;
    int aStop2 = aStop + 1;

    int i = aStart + 1;
    while (i < aStop2) {
      var aVal2 = list[i];
      if (aVal == aVal2) {
        aCount++;
      } else {
        doMe(dictionary[aVal], aCount);
        aVal = aVal2;
        aCount = 1;
      }
      i++;
    }

    doMe(dictionary[aVal], aCount);

    return;
  }

  void segmentCountDoOn(Iterable<Comparable> list, int start, int end,
      void callback(value, int count)) {
    var it = list.iterator;

    var i = 0;

    while (it.moveNext()) {
      if (i < start)
        i++;
      else
        break;
    }

    if (i < start) return;

    var current = it.current;
    var count = 1;
    while (it.moveNext()) {
      if (i <= end) {
        if (current.compareTo(it.current) == 0) {
          count++;
        } else {
          callback(current, count);
          current = it.current;
          count = 1;
        }
      } else {
        break;
      }
      i++;
    }

    callback(current, count);
  }

  int findFirstIndexOnSortedList(Object object, List list, int start) {
    int i = start;
    while (list[i] != object && i < list.length) i++;
    if (i < list.length) return i;
    return -1;
  }

// (1,2,3,4,7,8,10) will output (true,4);(false,2);(true,2);(false,1);(true,1)
  void findIncrementalSequences(
      List<int> orderedList, int start, int end, void fun(bool bit, int len)) {
    if (end - start < 1) return;
    var value = orderedList[start];
    var seqLen = 0;
    for (int i = start + 1; i < end; i++) {
      var previous = value;
      value = orderedList[i];
      var n = value - previous;
      if (n == 1) {
        seqLen++;
      } else {
        fun(true, seqLen + 1);
        seqLen = 0;
        fun(false, n - 1);
      }
    }
    fun(true, seqLen + 1);
  }

  List<double> minMaxDouble(List<double> data) {

    var minValue = double.maxFinite;
    var maxValue = -double.maxFinite;
    final len = data.length;
    for (int i = 0; i < len; i++) {
      minValue = min(minValue, data[i]);
      maxValue = max(maxValue, data[i]);
    }
    return [minValue, maxValue];
  }

  List<double> normDouble(List<double> data) {
    var minMax = minMaxDouble(data);
    var minValue = minMax.first;
    var maxValue = minMax.last;
    final len = data.length;
    for (var i = 0; i < len; i++) {
      final diff = maxValue - minValue;
      if (diff == 0) {
        data[i] = 0.5;
      } else {
        data[i] = ( (data[i] - minValue ) / diff) ;
      }
    }

    return data;
  }
}
