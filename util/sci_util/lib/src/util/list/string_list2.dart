part of sci_util_list;

class PStringList extends ListBase<String> implements Uint8StringList {
  Uint8StringListImpl _container;
  Int32List _order;

  bool _hasOrderChanged;

  PStringList() {
    _container = new Uint8StringListImpl.reserv(6);
    _initOrder();
  }

  PStringList.reserv(int initiallength) {
    _container = new Uint8StringListImpl.reserv(initiallength);
    _initOrder();
  }

  PStringList.fromString(Iterable<String> l) {
    _container = new Uint8StringListImpl.fromString(l);
    _initOrder();
  }

  PStringList.fromList(Uint8List list, [Int32List starts]) {
    _container = new Uint8StringListImpl.fromList(list, starts);
    _initOrder();
  }

  PStringList.fromCStringList(CStringList list) {
    _container =
        new Uint8StringListImpl.fromTDList(list.toBytes(), list.starts);
    _initOrder();
  }

  PStringList.filled(int length, String fill) {
    _container = new Uint8StringListImpl.filled(length, fill);
    _initOrder();
  }

  PStringList.fromJson(Map map) {
    _container = new Uint8StringListImpl.fromJson(map);
    _initOrder();
  }

  PStringList.fromUint8StringListImpl(Uint8StringListImpl list,
      [td.Int32List order]) {
    _container = list;
    if (order != null) {
      _order = new Int32List.fromList(order);
      _hasOrderChanged = true;
    } else {
      _initOrder();
    }
  }

  void _initOrder() {
    _order = new Int32List.reserv(_container.length);
    _order.setLength(_container.length, copy: false);
    var ll = _order._list;
    var len = ll.length;
    for (var i = 0; i < len; i++) ll[i] = i;
    _hasOrderChanged = false;
  }

  @override
  Uint8StringList copy() {
    return new PStringList.fromUint8StringListImpl(
        _container.copy(), _order.toFixedSizeList());
  }

  @override
  int get length => _order.length;

  @override
  set length(int i) {
    throw 'not impl';
  }

  @override
  void operator []=(int index, String value) {
    throw 'not impl';
  }

  void add(String value) {
    _order.add(length);
    _container.add(value);
  }

  @override
  void addAll(Iterable<String> values) {
    var len = length + values.length;
    for (var i = length; i < len; i++) {
      _order.add(i);
    }
    _container.addAll(values);
  }

  @override
  String operator [](int i) {
    return _container[_order[i]];
  }

  @override
  void append(Uint8StringList list) {
    PStringList l = list;

    var offset = length;
    var len = l.length;

    var lorder = l._order;

    for (var i = 0; i < len; i++) {
      _order.add(offset + lorder[i]);
    }

    _container.append(l._container);
  }

  @override
  bool equalsAt(int i, int j) {
    return _container.equalsAt(_order[i], _order[j]);
  }

  @override
  void addRepeat(String value, int n) {
    var len = length + n;
    for (var i = length; i < len; i++) {
      _order.add(i);
    }
    _container.addRepeat(value, n);
  }

  List<int> getOrder() => _order;

  @override
  List<String> sublist(int start, [int end]) {
    return new PStringList.fromUint8StringListImpl(
        _container.selectByRids(_order.sublistView(start, end)));
  }

  @override
  void basicReorder(List<int> order) {
    throw 'not impl';
  }

  @override
  void rebuildView() {
    if (_hasOrderChanged) {
      var len = length;
      _container.reorder(_order.sublistView(0, len));

      for (var i = 0; i < len; i++) {
        _order[i] = i;
      }
      _hasOrderChanged = false;
    }
  }

  @override
  td.Uint8List get startsByteView {
    rebuildView();
    return _container.startsByteView;
  }

  @override
  td.Int32List get startsView {
    rebuildView();
    return _container.startsView;
  }

  @override
  td.Uint8List get bytesView {
    rebuildView();
    return _container.bytesView;
  }

  @override
  CStringList toCStringList() {
    rebuildView();
    return _container.toCStringList();
  }

  @override
  Uint8StringList basicSelectByRids(td.Int32List rids) {
    var orids = new td.Int32List(rids.length);
    for (var i = 0; i < rids.length; i++) {
      orids[i] = _order[rids[i]];
    }
    return new PStringList.fromUint8StringListImpl(
        _container.basicSelectByRids(orids));
  }

  @override
  Uint8StringList selectByRids(td.Int32List rids) {
    var orids = new td.Int32List(rids.length);
    for (var i = 0; i < rids.length; i++) {
      orids[i] = _order[rids[i]];
    }
    return new PStringList.fromUint8StringListImpl(
        _container.selectByRids(orids));
  }

  @override
  void equalsTo(String value, void callback(int i, bool flag)) {
    var bytes = utf8.encode(value);
    var len = length;
    for (var i = 0; i < len; i++) {
      callback(i, _container.equalsToAt(bytes, _order[i]));
    }
  }

  @override
  void reorder(List<int> order) {
    SciList.getCurrent().reorder(_order.sublistView(0), order);
    _hasOrderChanged = true;
  }

  @override
  td.Int32List order({int direction: 1, bool stable: false}) {
    // we need stable sort !!!
    rebuildView();
    return _container.order(direction: direction, stable: stable);
  }

  @override
  int compareAt(int i, int j) {
    throw 'not impl';
  }

  @override
  td.Uint8List bytesAt(int i) {
    throw 'not impl';
  }

  @override
  int bytesLengthAt(int i) {
    throw 'not impl';
  }
}
