library sci_util_list;

import 'dart:typed_data' as td;
import 'dart:convert';
import 'dart:math' as math;
import 'dart:collection';
import '../type/type.dart' as scitype;
import '../num/num.dart' as scinum;
import 'package:quiver/core.dart';
import 'package:collection/collection.dart' as collection;
import 'package:tson/src/string_list.dart';

//import './native/c/native_list.dart';
//import './native/dart/dart_list.dart';
import './native/dart/dart_list.dart'
    if (dart.library.io)  './native/c/native_list.dart';

part 'helper.dart';
part 'search.dart';
part 'string_list.dart';
part 'string_list2.dart';

class Pair<T1, T2> {
  final T1 first;
  final T2 second;
  Pair(this.first, this.second);

  T1 get key => first;
  T2 get value => second;

  bool operator ==(final Object other) {
    if (other is Pair) {
      return first == other.first && second == other.second;
    }
    return false;
  }

  int get hashCode => hash2(first.hashCode, second.hashCode);

  String toString() => '${this.runtimeType}(${this.key}, ${this.value})';
}

class RangeIterator implements Iterator<int> {
  int _current;
  int start;
  int end;
  RangeIterator(this.start, this.end) {
    if (end < start) throw "end must be greater then start";
    _current = start - 1;
  }

  @override
  int get current => _current;

  @override
  bool moveNext() {
    _current++;
    return _current < end;
  }
}

abstract class WriteIterator<E> implements Iterator<E> {
  set current(E o);
  int get index;
  set index(int n);
  int get end;
  WriteIterator iterator(int n);
}

td.TypedData typedList(String type, int len) {
  td.TypedData buffer;
  if (type == Float64List.TYPE) {
    buffer = new td.Float64List(len);
  } else if (type == Uint8List.TYPE) {
    buffer = new td.Uint8List(len);
  } else if (type == Uint16List.TYPE) {
    buffer = new td.Uint16List(len);
  } else if (type == Uint32List.TYPE) {
    buffer = new td.Uint32List(len);
  } else {
    throw "TypedList : cannot create typed list unknown type $type";
  }
  return buffer;
}

List fromJson(Map m) {
  var buffer;
  var type = m['type'];
  if (type == Uint8StringList.TYPE) {
    buffer = new Uint8StringList.fromJson(m);
  } else if (type == Float64List.TYPE) {
    buffer = new Float64List.fromJson(m);
  } else if (type == Uint8List.TYPE) {
    buffer = new Uint8List.fromJson(m);
  } else if (type == Uint16List.TYPE) {
    buffer = new Uint16List.fromJson(m);
  } else if (type == Uint32List.TYPE) {
    buffer = new Uint32List.fromJson(m);
  } else {
    throw "TypedList : cannot create list unknown type $type";
  }
  return buffer;
}

List buffer(String type, int reserve) {
  var buffer;
  if (type == Uint8StringList.TYPE) {
    buffer = new Uint8StringList.reserv(reserve);
  } else if (type == Float64List.TYPE) {
    buffer = new Float64List.reserv(reserve);
  } else if (type == Uint8List.TYPE) {
    buffer = new Uint8List.reserv(reserve);
  } else if (type == Uint16List.TYPE) {
    buffer = new Uint16List.reserv(reserve);
  } else if (type == Uint32List.TYPE) {
    buffer = new Uint32List.reserv(reserve);
  }  else if (type == Int32List.TYPE) {
    buffer = new Int32List.reserv(reserve);
  } else {
    throw "TypedList : cannot create buffer unknown type $type";
  }
  return buffer;
}

abstract class TypedList<E> extends ListBase<E> {
//  static int _roundToPowerOf2(int v) {
//    assert(v > 0);
//    v--;
//    v |= v >> 1;
//    v |= v >> 2;
//    v |= v >> 4;
//    v |= v >> 8;
//    v |= v >> 16;
//    v++;
//    return v;
//  }

  static TypedList createListFromType(String type, int len) {
    switch (type) {
      case scitype.INT32:
        return new Int32List.reserv(len);
      case scitype.UINT32:
        return new Uint32List.reserv(len);
      case scitype.UINT16:
        return new Uint16List.reserv(len);
      case scitype.UINT8:
        return new Uint8List.reserv(len);
      case scitype.DOUBLE:
        return new Float64List.reserv(len);
      case scitype.INT64:
        throw new ArgumentError.value(type, 'not impl');
      case scitype.UINT64:
        return new Uint64List.reserv(len);
      default:
        throw new ArgumentError.value(type, 'bad type');
    }
  }

  List<E> _list;
  int _length;

  TypedList();
  TypedList.fromList(List<E> l) {
    _list = l;
    _length = _list.length;
  }
  TypedList.reserv(int initiallength) {
    _length = 0;
    _list = _newList(initiallength);
  }

  TypedList.fromJson(Map m) {
    List<E> jsonlist = m["list"];
    _length = m["length"];
    _list = this._newList(jsonlist.length);
    _list.setRange(0, _length, jsonlist);
  }

  List<E> get list => _list;
  String get type;

  td.TypedData get view;

  Map toJson() {
    Map answer = {
      "type": this.type,
      "list": _list,
      "length": _length,
    };
    return answer;
  }

  void setLength(int l, {bool copy: true}) {
    if (_list.length < l) {
      if (copy)
        resize(l);
      else
        _list = _newList(l);
    }
    _length = l;
  }

  bool get isEmpty => _length < 1;
  E get first => this[0];

  void swap(TypedList rhs) {
    var a = rhs._list;
    rhs._list = this._list;
    this._list = a;
    var len = rhs._length;
    rhs._length = this._length;
    this._length = len;
  }

  void copy(TypedList<E> fixedList) {
    if (_list.length < fixedList.length) {
      _list = _newList(fixedList.length);
    }
    _length = fixedList.length;
    _list.setRange(0, _length, fixedList._list);
  }

  List toFixedSizeList() {
    var fixedList = _newList(this.length);
    fixedList.setRange(0, _length, this._list);
    return fixedList;
  }

  void resize(int n) {
    int old = this.length;
    _length = n;
    var oldList = _list;
    _list = _newList(_length);
    _list.setRange(0, old, oldList);
  }

  E operator [](int index) {
    if (index >= _length) throw new RangeError.value(index);
    return _list[index];
  }

  void operator []=(int index, E value) {
    if (index >= _length) throw new RangeError.value(index);
    _list[index] = value;
  }

  void add(E value) {
    if (_length >= _list.length) _allocate();
    _list[_length] = value;
    _length++;
  }

  void addAll(Iterable<E> values) {
    values.forEach((v) => add(v));
  }

  void increaseSize(int len) {
    int oldLength = this._list.length;
    int newLength = math.max(len, oldLength) * 2;

//    int newLength = _roundToPowerOf2(len - this._list.length + oldLength) * 2;
    this.reserve(newLength);
  }

  void setRange(int start, int end, Iterable<E> iterable, [int skipCount = 0]) {
    if (end > this._list.length) increaseSize(end);
    _list.setRange(start, end, iterable, skipCount);
    _length = math.max(_length, end);
  }

  void _allocate() {
    if (_list.length == 0)
      reserve(1);
    else
      reserve(2 * _list.length);
  }

  List<E> _newList(int l);

  int get length => _length;

  set length(int l) => throw "not yet implemented";

  E get last => _list[_length - 1];

  Iterator<E> get iterator => new TypedListIterator<E>(this);

  void removeFrom(int start) {
    if (start >= _length) throw new RangeError.value(start);
    List newList = _newList(start);
    newList.setRange(0, start, _list);
    _list = newList;
    _length = start;
  }

  void clear() {
    _length = 0;
  }

  int size() => length;

  /// Increase the size of the array to have the capacity to store at least
  /// @c n elements.  If the current storage object does not have enough
  /// space, enlarge the storage object.
  void reserve(int n) {
    if (n > _list.length) {
      List newList = _newList(n);
      newList.setRange(0, _list.length, _list);
      _list = newList;
    }
  }

  void ensureCapacityForMore(int n) {
    reserve(_length + n);
  }

  void fillRange(int start, int end, [E fillValue]) {
    _list.fillRange(start, end, fillValue);
  }

  int get lengthInBytes;

  String join([String separator = ""]) {
    StringBuffer buffer = new StringBuffer();
    buffer.writeAll(this, separator);
    return buffer.toString();
  }
}

class TypedListIterator<E> implements WriteIterator<E> {
  TypedList _list;
  int _index = -1;
  TypedListIterator(this._list);
  E get current => this._list._list[_index];
  set current(E val) {
//    this._list[_index] = val;
    this._list._list[_index] = val;
    if (_index >= _list.length) _list._length = _index + 1;
  }

  bool moveNext() {
    _index++;
    return _index < _list.length;
  }

  set index(int n) => _index = n;

  int get index => _index;
  int get end => _list.length;

  WriteIterator iterator(int n) {
    TypedListIterator it = new TypedListIterator(_list);
    it.index = _index + n;
    return it;
  }
}

class Int8List extends TypedList<int> {
  static const String TYPE = scitype.INT8;
  Int8List.fromList(td.Uint8List l) : super.fromList(l);
  Int8List.reserv(int initiallength) : super.reserv(initiallength);
  Int8List.fromJson(Map m) : super.fromJson(m);
  List<int> _newList(int l) => new td.Int8List(l);
  td.Int8List get list => _list;
  int get lengthInBytes => _length;

  td.TypedData get view => new td.Int8List.view(list.buffer, 0, this.length);

  @override
  String get type => TYPE;
}

class Uint8List extends TypedList<int> {
  static const String TYPE = scitype.UINT8;
  Uint8List.fromList(td.Uint8List l) : super.fromList(l);
  Uint8List.reserv(int initiallength) : super.reserv(initiallength);
  Uint8List.fromJson(Map m) : super.fromJson(m);
  List<int> _newList(int l) => newUint8List(l);
  td.Uint8List get list => _list;
  int get lengthInBytes => _length;

  td.TypedData get view => new td.Uint8List.view(list.buffer, 0, this.length);

  @override
  String get type => TYPE;
}

class Uint16List extends TypedList<int> {
  static const String TYPE = scitype.UINT16;
  Uint16List.fromList(td.Uint16List l) : super.fromList(l);
  Uint16List.reserv(int initiallength) : super.reserv(initiallength);
  Uint16List.fromJson(Map m) : super.fromJson(m);
  List<int> _newList(int l) => newUint16List(l);
  td.Uint16List get list => _list;
  int get lengthInBytes => _length * 2;

  td.TypedData get view => new td.Uint16List.view(list.buffer, 0, this.length);

  @override
  String get type => TYPE;
}

class Int32List extends TypedList<int> {
  static const String TYPE = scitype.INT32;
  Int32List.fromList(td.Int32List l) : super.fromList(l);
  Int32List.reserv(int initiallength) : super.reserv(initiallength);
  Int32List.fromJson(Map m) : super.fromJson(m);

  td.TypedData get view => new td.Int32List.view(list.buffer, 0, this.length);

  td.Int32List sublistView(int start, [int end]) {
    return new td.Int32List.view(
        list.buffer, start, end == null ? length : end);
  }

  Int32List.copy(Int32List cop) {
    _length = cop.length;
    _list = _newList(_length);
    _list.setRange(0, _length, cop._list);
  }

  List<int> _newList(int l) => newInt32List(l);
  td.Int32List get list => _list;
  int get lengthInBytes => _length * 4;

  List<int> serialize() {
    return new td.Uint8List.view(
        (_list.sublist(0, _length) as td.TypedData).buffer);
  }

  @override
  String get type => TYPE;
}

class Uint32List extends TypedList<int> {
  static const String TYPE = scitype.UINT32;
  Uint32List.fromList(td.Uint32List l) : super.fromList(l);
  Uint32List.reserv(int initiallength) : super.reserv(initiallength);
  Uint32List.fromJson(Map m) : super.fromJson(m);

  td.TypedData get view => new td.Uint32List.view(list.buffer, 0, this.length);

  Uint32List.copy(Uint32List cop) {
    _length = cop.length;
    _list = _newList(_length);
    _list.setRange(0, _length, cop._list);
  }

  List<int> _newList(int l) => newUint32List(l);
  td.Uint32List get list => _list;
  int get lengthInBytes => _length * 4;

  List<int> serialize() {
    return new td.Uint8List.view(
        (_list.sublist(0, _length) as td.TypedData).buffer);
  }

  @override
  String get type => TYPE;
}

class Uint64List extends TypedList<int> {
  static const String TYPE = scitype.UINT64;
  Uint64List.fromList(td.Uint64List l) : super.fromList(l);
  Uint64List.reserv(int initiallength) : super.reserv(initiallength);
  Uint64List.fromJson(Map m) : super.fromJson(m);

  td.TypedData get view => new td.Uint64List.view(list.buffer, 0, this.length);

  Uint64List.copy(Uint64List cop) {
    _length = cop.length;
    _list = _newList(_length);
    _list.setRange(0, _length, cop._list);
  }

  List<int> _newList(int l) => newUint64List(l);
  td.Uint64List get list => _list;
  int get lengthInBytes => _length * 8;

  List<int> serialize() {
    return new td.Uint8List.view(
        (_list.sublist(0, _length) as td.TypedData).buffer);
  }

  @override
  String get type => TYPE;
}

class Float64List extends TypedList<double> {
  static const String TYPE = scitype.DOUBLE;
  Float64List.fromList(td.Float64List l) : super.fromList(l);
  Float64List.reserv(int initiallength) : super.reserv(initiallength);
  Float64List.fromJson(Map m) : super.fromJson(m);

  td.TypedData get view => new td.Float64List.view(list.buffer, 0, this.length);

  Float64List.copy(Float64List cop) {
    _length = cop.length;
    _list = _newList(_length);
    _list.setRange(0, _length, cop._list);
  }

  List<double> _newList(int l) => newFloat64List(l);
  td.Float64List get list => _list;
  int get lengthInBytes => _length * 4;
  @override
  String get type => TYPE;
}

abstract class StringList implements List<String> {
  void addNull();
  int get lengthInBytes;
//  void forEachIndex(Function f);
}

class ListOffset<E> extends ListBase<E> {
  List<E> list;
  int start;
  int end;

  ListOffset(this.list, this.start, this.end);
  int get length => end - start;

  @override
  E operator [](int index) {
    return list[index + start];
  }

  @override
  void operator []=(int index, E value) {
    list[index + start] = value;
  }

  @override
  void set length(int newLength) {
    throw "fixed size list";
  }
}

class ListWrapper<E> extends ListBase<E> {
  List<E> _list;
  List<int> _order;
//  List<int> _reverse;

  ListWrapper(this._list, [this._order]) {
    if (this._order == null) this._order = new IdentityList(this.length);
    assert(_list.length == _order.length);
  }

  List<int> sortOrder(List<int> sortedOrder, [List<int> swapOrder]) {
    assert(sortedOrder.length == length);
    var newOrder;
    if (swapOrder != null) {
      assert(swapOrder.length == length);
      newOrder = swapOrder;
    } else
      newOrder = newInt32List(sortedOrder.length);

//    if (_reverse == null) _reverse = AMListUtil.newInt32List(sortedOrder.length, sortedOrder.length-1);
//    for (int i = 0 ; i < length ; i++ ) _reverse[_order[i]] = i;

    for (int i = 0; i < length; i++) {
      newOrder[i] = _order[sortedOrder[i]];
    }

    var old = _order;
    _order = newOrder;
    return old;
  }

  void sortInnerList() {
    sort(_list, _order);
    _order = new IdentityList(this.length);
  }

  void unsortInnerList(List<int> o) {
    _order = o;
  }

  List<int> get order => _order;
  set order(List<int> o) {
    if (_list != null) assert(_list.length == o.length);
    _order = o;
  }

  List get list => _list;
  set list(List l) {
    if (_order != null) assert(l.length == _order.length);
    _list = l;
  }

  int indexAt(int index) => _order[index];

  @override
  operator [](int index) {
    return _list[indexAt(index)];
  }

  @override
  void operator []=(int index, value) {
    _list[indexAt(index)] = value;
  }

  @override
  void set length(int newLength) {
    throw "fixed size list";
  }

  @override
  int get length => _list.length;
}

class Range extends ListBase<int> {
  static Iterable<int> toIterable(Iterable<Range> ranges) {
    var len = ranges.fold(0, (sum, range) => sum + range.length);
    var rids = newInt32List(len);
    var i = 0;
    for (var range in ranges) {
      range.forEach((rid) {
        rids[i] = rid;
        i++;
      });
    }
    return rids;
  }

//  static Iterable<int> toIterable(Iterable<Range> ranges) sync* {
//    for (var range in ranges) {
//      yield* range;
//    }
//  }

  int start;
  int len;

  Range(this.start, this.len) {
    assert(start >= 0);
    assert(len >= 0);
  }
  Range.fromJson(Map m) {
    start = m['start'];
    len = m['len'];
  }
  Map toJson() {
    return {'start': start, 'len': len};
  }

  String toString() => '${this.runtimeType}(start:${start}, len:${len})';

  int get end => start + len;

  bool intersect(Range r) => (r.start < start + len && start < r.start + r.len);

  bool containsIndex(int x) => ((x >= this.start && x < this.end));

  @override
  bool contains(Object i) {
    return containsIndex(i);
  }

  @override
  int operator [](int index) {
    return start + index;
  }

  @override
  void operator []=(int index, int value) {
    throw "should not implement";
  }

  @override
  void set length(int newLength) {
    throw "should not implement";
  }

  @override
  int get length => len;

  @override
  bool operator ==(final Object other) {
    if (other is Range) {
      return start == other.start && len == other.len;
    }
    return false;
  }

  @override
  int get hashCode => hash2(start.hashCode, len.hashCode);
}

class DelegatedList<E> extends ListBase<E> {
  Function getFun;
  Function lengthFun;

  DelegatedList(this.lengthFun, this.getFun);

  @override
  E operator [](int index) {
    return getFun(index);
  }

  @override
  void operator []=(int index, E value) {
    throw "error this list is readonly";
  }

  @override
  void set length(int newLength) {
    throw "error cannot set length";
  }

  @override
  int get length => lengthFun();
}

class OneValueList<E> extends ListBase<E> {
  int _length;
  E value;

  OneValueList(this._length, this.value);

  @override
  E operator [](int index) {
    return value;
  }

  @override
  void operator []=(int index, E v) {
    value = v;
  }

  @override
  void set length(int newLength) {
    _length = newLength;
  }

  @override
  int get length => _length;
}

class IdentityList extends ListBase<int> {
  int _length;

  IdentityList(this._length);

  @override
  int operator [](int index) {
    return index;
  }

  @override
  void operator []=(int index, int value) {
    throw "error this list is read only";
  }

  @override
  void set length(int newLength) {
    _length = newLength;
  }

  // TODO: implement length
  @override
  int get length => _length;
}

class ListUin32List {
  Uint32List _starts;
  Uint32List _list;

  Map toJson() {
    return {
      "starts": _starts.toFixedSizeList(),
      "list": _list.toFixedSizeList()
    };
  }

  ListUin32List.fromJson(Map m) {
    _starts = new Uint32List.fromList(m["starts"]);
    _list = new Uint32List.fromList(m["list"]);
  }

  ListUin32List.reserv(int l) {
    _starts = new Uint32List.reserv(l);
    _list = new Uint32List.reserv(l);

    _starts.add(0);
  }

  int get length => _starts.length - 1;

  void add(List<int> list) {
    _starts.add(_starts[length] + list.length);
    list.forEach(_list.add);
  }

  List<int> operator [](int index) {
    if (index >= length) throw new RangeError.value(index);
    var s = _starts[index];
    var e = _starts[index + 1];
    return _list.sublist(s, e);
  }

  int get lengthInBytes {
    return _starts.lengthInBytes + _list.lengthInBytes;
  }
}
