import 'dart:typed_data';
import 'dart:async';
import 'dart:convert';
import 'package:tson/tson.dart' as TSON;
import 'package:sci_util/src/util/list/list.dart' as scilist;

import 'package:logging/logging.dart';

abstract class ContentCodec {
  String get name;

  Map<String, String> get contentTypeHeader;

  dynamic decode(dynamic input);
  dynamic encode(dynamic value);

  Stream<List<int>> encodeStream(dynamic value);
  dynamic decodeStream(Stream<List<int>> stream, [Encoding encoding]);

  String get responseType;
}

//class JsonContentCodec implements ContentCodec {
//  String get name => 'json';
//
//  Map<String, String> get contentTypeHeader =>
//      {'content-type': 'application/json'};
//
//  dynamic decode(dynamic source) => json.decode(
//      source is ByteBuffer ? utf8.decode(new Uint8List.view(source)) : source);
//
//  dynamic encode(dynamic value) => json.encode(value);
//
//  String get responseType => 'text';
//
//  Future<dynamic> decodeStream(Stream<List<int>> stream,
//      [Encoding encoding]) async {
//    if (encoding == null) encoding = utf8;
//    return json.decode(await encoding.decodeStream(stream));
//  }
//}

class TsonContentCodec implements ContentCodec {
  static Logger logger = new Logger('TsonContentCodec');
  String get name => 'tson';

  Map<String, String> get contentTypeHeader =>
      {'content-type': 'application/tson'};

  dynamic decode(dynamic source) => TSON.decode(source);

  dynamic encode(dynamic value){
    try {
      return TSON.encode(value);
    } catch (e,st){
      logger.severe('TSON encoding failed : ' + e.toString() + ' for value ${value}', st);
      throw 'TSON encoding failed : ' + e.toString();
    }
  }

  Stream<List<int>> encodeStream(dynamic value) =>
      new Stream.fromIterable([TSON.encode(value)]);

  String get responseType => 'arraybuffer';

  Future<dynamic> decodeStream(Stream<List<int>> stream,
      [Encoding encoding]) async {
    scilist.Uint8List bytes = await stream.fold(
        scilist.newUint8Buffer(1024),
        (buf, list) =>
            buf..setRange(buf.length, buf.length + list.length, list));


    return TSON.decode(bytes.view);
  }
}
