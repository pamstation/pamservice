import "dart:async";
import "dart:io" as io;

class Process {
  static Future<Process> start(String executable, List<String> arguments,
      {String workingDirectory,
        Map<String, String> environment,
        bool includeParentEnvironment: true,
        bool runInShell: false,
        io.ProcessStartMode mode: io.ProcessStartMode.NORMAL}) async {
    return new Process._(await io.Process.start(executable, arguments,
        workingDirectory: workingDirectory,
        environment: environment,
        runInShell: runInShell));
  }

  Process._(this.process);

  io.Process process;

  Future<int> get exitCode => process.exitCode;

  /**
   * Returns the standard output stream of the process as a [:Stream:].
   */
  Stream<List<int>> get stdout => process.stdout;

  /**
   * Returns the standard error stream of the process as a [:Stream:].
   */
  Stream<List<int>> get stderr => process.stderr;

  /**
   * Returns the standard input stream of the process as an [IOSink].
   */
  io.IOSink get stdin => process.stdin;

  /**
   * Returns the process id of the process.
   */
  int get pid => process.pid;

  /**
   * Kills the process.
   *
   * Where possible, sends the [signal] to the process. This includes
   * Linux and OS X. The default signal is [ProcessSignal.SIGTERM]
   * which will normally terminate the process.
   *
   * On platforms without signal support, including Windows, the call
   * just terminates the process in a platform specific way, and the
   * `signal` parameter is ignored.
   *
   * Returns `true` if the signal is successfully delivered to the
   * process. Otherwise the signal could not be sent, usually meaning
   * that the process is already dead.
   */
  bool kill([io.ProcessSignal signal = io.ProcessSignal.SIGTERM]) =>
      process.kill(signal);
}

class DockerExecProcess implements Process {
  static Future update(String containerName) async {
    var args = [
      'update',
      '--net=host',
      '-d',
      '--name',
      containerName,
    ];

    var process = await io.Process.start('docker', args);

    if (await process.exitCode == 0) {
      throw 'Failed to update container';
    }
  }

  static Future<Process> start(
      String container, String executable, List<String> arguments,
      {String workingDirectory,
        Map<String, String> environment,
        bool includeParentEnvironment: false,
        bool runInShell: false,
        io.ProcessStartMode mode: io.ProcessStartMode.NORMAL,
        String user}) async {
    var args0 = [
      'exec',
      '--workdir',
      workingDirectory,
    ];

    environment.forEach((k, v) {
      args0.addAll(['-e', '$k=$v']);
    });

    if (environment.containsKey('R_PACKRAT_CACHE_DIR')) {
      args0.addAll(
          ['-e', 'R_PACKRAT_CACHE_DIR=${environment['R_PACKRAT_CACHE_DIR']}']);
    }

    if (user != null) {
      args0..addAll(['--user', user]);
    }

    print('DockerExecProcess -- start -- docker ${args0.join(' ')} ');

    args0..addAll([container, 'R'])..addAll(arguments);

    var process = await io.Process.start('docker', args0);

    return new DockerExecProcess._(process, container);
  }

  String container;
  Completer<int> exitCodeCompleter;

  DockerExecProcess._(this.process, this.container) {
    exitCodeCompleter = new Completer();

    process.exitCode.then((exitCode) async {
      print('$this -- exitCode=$exitCode');

      if (exitCode == 0) {
        exitCodeCompleter.complete(exitCode);
      } else {
        // restart container
        var iprocess = await io.Process.start('docker', ['restart', container]);

        var iec = await iprocess.exitCode;

        if (iec != 0) {
          print('$this --docker restart exitCode=$iec');
        }

        exitCodeCompleter.complete(exitCode);
      }
    });
  }

  io.Process process;

  Future<int> get exitCode => exitCodeCompleter.future;

  /**
   * Returns the standard output stream of the process as a [:Stream:].
   */
  Stream<List<int>> get stdout => process.stdout;

  /**
   * Returns the standard error stream of the process as a [:Stream:].
   */
  Stream<List<int>> get stderr => process.stderr;

  /**
   * Returns the standard input stream of the process as an [IOSink].
   */
  io.IOSink get stdin => process.stdin;

  /**
   * Returns the process id of the process.
   */
  int get pid => process.pid;

  /**
   * Kills the process.
   *
   * Where possible, sends the [signal] to the process. This includes
   * Linux and OS X. The default signal is [ProcessSignal.SIGTERM]
   * which will normally terminate the process.
   *
   * On platforms without signal support, including Windows, the call
   * just terminates the process in a platform specific way, and the
   * `signal` parameter is ignored.
   *
   * Returns `true` if the signal is successfully delivered to the
   * process. Otherwise the signal could not be sent, usually meaning
   * that the process is already dead.
   */
  bool kill([io.ProcessSignal signal = io.ProcessSignal.SIGTERM]) =>
      process.kill(signal);
}