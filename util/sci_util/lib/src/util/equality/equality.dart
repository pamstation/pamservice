library sci_util_equality;

//import 'package:collection/equality.dart';

import 'package:collection/collection.dart';


bool equals(a, b) {
  if (a == b) return true;
  if (a is List && b is List) {
    return equalLists(a, b);
  } else if (a is Map && b is Map) {
    return equalMaps(a, b);
  } else
    return false;
}

bool equalMaps(Map m1, Map m2) {
  var answer = const SetEquality().equals(m1.keys.toSet(), m2.keys.toSet());
  if (!answer) return false;
  answer = m1.keys.any((k) {
    return !equals(m1[k], m2[k]);
  });
  return !answer;
}

bool equalLists(List a, List b) {
  if (a == b) return true;
  if (a == null) return false;
  if (b == null) return false;
  if (a.length != b.length) return false;
  for (int i = 0; i < a.length; i++) {
    if (!equals(a[i], b[i])) return false;
  }
  return true;
}
