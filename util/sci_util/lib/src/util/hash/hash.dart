import 'dart:convert';
import 'package:crypto/src/sha256.dart' as crypto;

String getBase64UrlSha256Hash(m) => base64Url.encode(getSha256Hash(m));

List<int> getSha256Hash(m) {
  if (! (m is List || m is Map)) throw 'getSha256Hash : bad type';
  return crypto.sha256.convert(utf8.encode(json.encode(m))).bytes;
}

