import 'dart:math' as math;
import 'dart:typed_data' as td;
import '../num/num.dart' as scinum;
import '../geo/geo.dart' as scigeo;

const double toRadDegree = math.pi / 180;

num floorBase(num x, num base) {
  num res = (math.log(x) / math.log(base));
  res = res.ceil();
  return math.pow(base, res);
}

num floorBase2(num x, int k) {
  num res = (math.log(k * x) / (k * math.ln2));
  res = res.ceil();
  return math.pow(2, res * k);
}

bool isPowerOf2(int n) {
  return floorBase(n, 2) == n;
}

double toRad(double degree) {
  return toRadDegree * degree;
}

double haversineD(
    double lati1, double lati2, double lon1, double lon2, double R) {
  double dLat = toRad(lati2 - lati1);
  double dLon = toRad(lon2 - lon1);
  double lat1 = toRad(lati1);
  double lat2 = toRad(lati2);

  double a = math.sin(dLat / 2) * math.sin(dLat / 2) +
      math.sin(dLon / 2) * math.sin(dLon / 2) * math.cos(lat1) * math.cos(lat2);
  double c = 2 * math.atan2(math.sqrt(a), math.sqrt(1 - a));
  double d = R * c;
  return d;
}

double sign(double x) {
  if (x < 0) return -1.0;
  if (x > 0) return 1.0;
  return 0.0;
}

void stdDev(List<num> data, void callback(m, dev)) {
  var m = mean(data);
  var answer = 0.0;
  data.forEach((each) {
    answer += math.pow((m - each), 2);
  });
  callback(m, math.pow(answer, 0.5) / (data.length - 1));
}

double mean(List<num> data) {
  var m = 0.0;
  data.forEach((each) {
    m += each;
  });
  return m / data.length;
}

//data must be sorted
void quartiles(List<num> data, void callback(double q1, double q2, double q3)) {
  if (data.isEmpty)
    return callback(
        scinum.DOUBLE_NULL, scinum.DOUBLE_NULL, scinum.DOUBLE_NULL);
  var len = data.length;
  if (len < 2)
    return callback(
        data.first.toDouble(), data.first.toDouble(), data.first.toDouble());
  if (len < 3)
    return callback(data.first.toDouble(), (data.first + data.last) / 2.0,
        data.last.toDouble());

  var q1, q2, q3;
  var q2_i = (len ~/ 2);
  if (len.isEven) {
    q2 = (data[q2_i - 1] + data[q2_i]) / 2;
  } else
    q2 = data[q2_i];

  var q1_i = (q2_i ~/ 2);
  var q3_i = q2_i + (q2_i - q1_i);
  if (q1_i.isEven) {
    if (q1_i < 1)
      q1 = data[q1_i];
    else
      q1 = (data[q1_i - 1] + data[q1_i]) / 2;
    q3 = (data[q3_i - 1] + data[q3_i]) / 2;
  } else {
    q1 = data[q1_i];
    q3 = data[q3_i];
  }

  callback(q1.toDouble(), q2.toDouble(), q3.toDouble());
}

// Returns 1 if the lines intersect, otherwise 0. In addition, if the lines
// intersect the intersection point may be stored in the floats i_x and i_y.
bool getLineIntersection(num p0_x, num p0_y, num p1_x, num p1_y, num p2_x,
    num p2_y, num p3_x, num p3_y, scigeo.Point2D result) {
  num s1_x, s1_y, s2_x, s2_y;
  s1_x = p1_x - p0_x;
  s1_y = p1_y - p0_y;
  s2_x = p3_x - p2_x;
  s2_y = p3_y - p2_y;

  num s, t;
  var ds = (-s2_x * s1_y + s1_x * s2_y);
  if (ds == 0.0) return false;

  s = (-s1_y * (p0_x - p2_x) + s1_x * (p0_y - p2_y)) / ds;
  t = (s2_x * (p0_y - p2_y) - s2_y * (p0_x - p2_x)) / ds;

  if (s >= 0 && s <= 1 && t >= 0 && t <= 1) {
    // Collision detected
    result.x = p0_x + (t * s1_x);
    result.y = p0_y + (t * s1_y);
    return true;
  }

  return false; // No collision
}

num pointLineDistance(
    scigeo.Point2D lineA, scigeo.Point2D lineB, scigeo.Point2D point) {
  var a = (lineA.y - lineB.y) / (lineB.x - lineA.x);
  var b = 1;
  var c = -lineA.y - a * lineA.x;

  var d =
      (a * point.x + b * point.y + c).abs() / (math.pow((a * a + b * b), 0.5));
  return d;
}

num pointSegmentDistance(scigeo.Point2D v, scigeo.Point2D w, scigeo.Point2D p) {
  var sqr = (x) {
    return x * x;
  };
  var dist2 = (v, w) {
    return sqr(v.x - w.x) + sqr(v.y - w.y);
  };

  var l2 = dist2(v, w);
  if (l2 == 0) return dist2(p, v);
  var t = ((p.x - v.x) * (w.x - v.x) + (p.y - v.y) * (w.y - v.y)) / l2;
  if (t < 0) return dist2(p, v);
  if (t > 1) return dist2(p, w);
  return math.pow(
      dist2(
          p, new scigeo.Point2D(v.x + t * (w.x - v.x), v.y + t * (w.y - v.y))),
      0.5);
}

td.Float64List randomList(int len,
    {double mean: 1000.0, double std: 50.0, int seed: 42, int precision: 3}) {
  var list = new td.Float64List(len);
  var rand = new math.Random(seed);

  var p = math.pow(10, precision);

  for (int i = 0; i < len; i++) {
    var v = ((rand.nextDouble() - 0.5) * std) + mean;
    var d = v - v.roundToDouble();
    v = v.roundToDouble() + ((p * d).roundToDouble() / p);
    list[i] = v;
  }

  return list;
}
