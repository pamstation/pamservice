import 'dart:typed_data';
import '../list/list.dart' as scilist;

class ColorHash {
  Map options;

  List<double> L;
  List<double> S;
  List<double> hueRangesMin;
  List<double> hueRangesMax;

  ColorHash({Map options}) {
    L = [0.35, 0.5, 0.65];
    S = [0.35, 0.5, 0.65];

    hueRangesMin = [0.0];
    hueRangesMax = [360.0];

//    if (options == null) {
//      options = {};
//    }
//
//    this.options = options;
//
//    options.putIfAbsent("lightness", ()=> [0.35, 0.5, 0.65]);
//    options.putIfAbsent("saturation", ()=> [0.35, 0.5, 0.65]);
//
//    L = options["lightness"];
//    S = options["saturation"];
//
//    if
  }

  int hashBytes(List<int> bytes) {
    final seed = 131;
    final seed2 = 137;

    // Note: Number.MAX_SAFE_INTEGER equals 9007199254740991
    final MAX_SAFE_INTEGER = 9007199254740991 ~/ seed2;

    var hash = 0;

    for (var i = 0; i < bytes.length; i++) {
      if (hash > MAX_SAFE_INTEGER) {
//        assert((hash ~/ seed2) * hash - hash > 255);

        hash = hash ~/ seed2;
      }

      hash = hash * seed + bytes[i];
    }

    // make hash more sensitive for short string like 'a', 'b', 'c'

    if (hash > MAX_SAFE_INTEGER) {
      hash = hash ~/ seed2;
    }

    hash = hash * seed + 67;

    return hash;
  }

  List<int> HSL2RGB(num H, num S, num L) {
    H /= 360;

    var q = L < 0.5 ? L * (1 + S) : L + S - L * S;
    var p = 2 * L - q;

    return [H + 1 / 3, H, H - 1 / 3].map((color) {
      if (color < 0) {
        color++;
      }
      if (color > 1) {
        color--;
      }
      if (color < 1 / 6) {
        color = p + (q - p) * 6 * color;
      } else if (color < 0.5) {
        color = q;
      } else if (color < 2 / 3) {
        color = p + (q - p) * 6 * (2 / 3 - color);
      } else {
        color = p;
      }
      return (color * 255).toInt();
    }).toList();
  }

  List<num> hslHash(int hash) {
    var H, S, L;

    if (hueRangesMin.isNotEmpty) {
      var i = hash % hueRangesMin.length;

      var rangeMin = hueRangesMin[i];
      var rangeMax = hueRangesMax[i];

      var hueResolution = 727; // note that 727 is a prime
      H = ((hash / hueRangesMax.length) % hueResolution) *
              (rangeMax - rangeMin) /
              hueResolution +
          rangeMin;
    } else {
      H = hash % 359; // note that 359 is a prime
    }
    hash = hash ~/ 360;
    S = this.S[hash % this.S.length];
    hash = hash ~/ this.S.length;
    L = this.L[hash % this.L.length];

    return [H, S, L];
  }

  List<num> hsl(List<int> bytes) => hslHash(hashBytes(bytes));

  List<int> rgb(List<int> bytes) {
    var hslValue = hsl(bytes);
    return HSL2RGB(hslValue[0], hslValue[1], hslValue[2]);
  }

  List<int> rgbHash(int hash) {
    var hslValue = hslHash(hash);
    return HSL2RGB(hslValue[0], hslValue[1], hslValue[2]);
  }

  Uint32List getColors(Int32List hashValues) {
//    var values = new Uint32List.view(hashValues.buffer);
    var colors = scilist.newUint32List(hashValues.length);

    for (var i = 0; i < hashValues.length; i++) {
      var rgb = rgbHash(hashValues[i]);
      colors[i] = (rgb[0] << 24) | (rgb[1] << 16) | (rgb[2] << 8) | 255;
    }

    return colors;
  }
}

main() {
  var colorH = new ColorHash();
//  var hsl = colorH.hsl('a'.codeUnits);
//
////  print(hsl);
//  print(colorH.hsl('a'.codeUnits));
//  print(colorH.hsl('al'.codeUnits));
//  print(colorH.hsl('ale'.codeUnits));
//  print(colorH.hsl('alex'.codeUnits));
//
//  print('///////// RGB ////////');
//
//  print(colorH.rgb('a'.codeUnits));
//  print(colorH.rgb('al'.codeUnits));
//  print(colorH.rgb('ale'.codeUnits));
//  print(colorH.rgb('alex'.codeUnits));

  var h =
      colorH.hashBytes([1, 49, 46, 49, 46, 48, 0, 10, 1, 0, 0, 0, 1, 66, 0]);

  print('hash ${[1, 49, 46, 49, 46, 48, 0, 10, 1, 0, 0, 0, 1, 66, 0]} $h');

  h = colorH.hashBytes([1, 49, 46, 49, 46, 48, 0, 10, 1, 0, 0, 0, 1, 66, 0]);

  print('hash ${[1, 49, 46, 49, 46, 48, 0, 10, 1, 0, 0, 0, 1, 79, 0]} $h');
}
