library sci_util_sub;

import 'dart:async';

class SubscriptionHelper {
  Map<Object, List<StreamSubscription>> _subscriptions = {};

  void addSubscription(o, StreamSubscription sub) {
    assert(sub != null);
    var subs = _subscriptions.putIfAbsent(o, () => []);
    subs.add(sub);
  }

  void removeSubscriptionsSync(o) {
    var subs = _subscriptions.remove(o);
    if (subs != null) {
      Future.wait(subs.map((s) => s.cancel()).where((f) => f != null));
    }
  }

  Future removeSubscriptions(o) {
    var subs = _subscriptions.remove(o);
    if (subs != null) {
      return Future.wait(subs.map((s) => s.cancel()).where((f) => f != null));
    } else
      return new Future.value();
  }

  Future releaseSubscriptions() {
    var keys = new List.from(_subscriptions.keys);
    return Future.wait(keys.map(removeSubscriptions)).then((_) {
      _subscriptions = {};
    });
  }
}
