//import 'dart:typed_data';
//import 'dart:async';
//import 'package:tson/tson.dart' as TSON;
//
//List<int> convert(object) {
//  var bytes = TSON.encode(object);
//
//  var result = new Uint8List(bytes.length + 4);
//  var byteData = new ByteData.view(bytes.buffer);
//
//  byteData.setUint32(0, bytes.length, Endian.little);
//  result.setRange(4, result.length, bytes);
//  return result;
//}
//
//class TsonStreamTransformer implements StreamTransformer<List<int>, Object> {
//  StreamTransformer _transformer;
//
//  List<int> _buffer = [];
//
//  TsonStreamTransformer() {
//    _transformer = new StreamTransformer.fromHandlers(
//        handleData: handleData,
//        handleError: handleError,
//        handleDone: handleDone);
//
//    _buffer = new Uint8List(0);
//  }
//
//  StreamTransformer<List<int>, Object> get transformer => _transformer;
//
//  void handleData(List<int> data, EventSink<Object> sink) {
//    _addBuffer(data);
//    _readObject(sink);
//  }
//
//  void _addBuffer(List<int> bytes) {
//    var tmp = new Uint8List(_buffer.length + bytes.length);
//    tmp.setRange(0, _buffer.length, _buffer);
//    tmp.setRange(_buffer.length, _buffer.length + bytes.length, bytes);
//    _buffer = tmp;
//  }
//
//  void _readObject(EventSink<Object> sink) {
//    if (_buffer.length < 4) return;
//
//    var len = _readLength();
//
//    if (len <= _buffer.length + 4) {
//      var bytes = _buffer.sublist(4, len + 4);
//      var object = TSON.decode(bytes);
//      sink.add(object);
//      _buffer = _buffer.sublist(len + 4);
//      _readObject(sink);
//    }
//  }
//
//  int _readLength() {
//    var lenBytes = new Uint8List(4);
//    lenBytes.setRange(0, 4, _buffer);
//    var byteData = new ByteData.view(lenBytes.buffer);
//    return byteData.getUint32(0, Endian.little);
//  }
//
//  void handleError(
//      Object error, StackTrace stackTrace, EventSink<Object> sink) {
//    sink.addError(error, stackTrace);
//  }
//
//  void handleDone(EventSink<Object> sink) {
//    _readObject(sink);
//    sink.close();
//  }
//
//  @override
//  Stream<Object> bind(Stream<List> stream) {
//    return _transformer.bind(stream);
//  }
//}
//
//class TsonSink {
//  StreamController<List<int>> _controller;
//
//  TsonSink() {
//    _controller = new StreamController();
//  }
//
//  Stream<List<int>> get stream => _controller.stream;
//
//  void _add(List<int> data) {
//    _controller.add(data);
//  }
//
//  void addObject(object) {
//    var bytes = TSON.encode(object);
//    _addLength(bytes.length);
//    this._add(TSON.encode(object));
//  }
//
//  void _addLength(int len) {
//    var byteData = new ByteData(4)..setUint32(0, len, Endian.little);
//    this._add(new Uint8List.view(byteData.buffer));
//  }
//
//  void close() {
//    _controller.close();
//  }
//}
