import 'dart:async';
import '../value/value.dart';
import '../error/error.dart';

class CancelError {}

bool isCancel() {
  var value = Zone.current['isCanceled'];
  if (value != null) {
    return value.value;
  }
  return false;
}

void checkCancel() {
  if (isCancel()) throw new ServiceError.abort('check.cancel');
}

void checkArrayLength(int length) {
  ValueHolder value = Zone.current['maxArrayLength'];
  if (value != null && length > value.value) {
    throw new ServiceError.forbidden('check.array.length',
        'Failed to create an array of length $length, cannot be greater than ${value.value}');
  }
}

void cancel() {
  var value = Zone.current['isCanceled'];
  if (value != null) {
    value.value = true;
  }
}

R runCancelZoned<R>(R body(), {int maxArrayLength: 100000000}) {
  return runZoned(body, zoneValues: {
    'isCanceled': new ValueHolder(false),
    'maxArrayLength': new ValueHolder(maxArrayLength)
  });
}
