import 'package:logging/logging.dart';
import 'dart:convert';


class Config {
  static Logger logger = new Logger('Config');

  static Config _CURRENT;
  Map _data;
  factory Config([Config config]) {
    if (config != null) _CURRENT = config;
    if (_CURRENT == null) _CURRENT = new Config._();
    return _CURRENT;
  }

  Config._() {
    _data = {};
  }

  void initLogger() {

    var strLogLevel = this['tercen.log.level'] == null ? this['log.level']: this['tercen.log.level'];

    if (strLogLevel == null) strLogLevel = '0';

    var logLevel = int.parse(strLogLevel);
    var level = Level.LEVELS.firstWhere((l) => l.value == logLevel);
    Logger.root.level = level;

    Logger.root.onRecord.listen((LogRecord rec) {
      print(
          '${rec.time} : ${rec.level.name} : ${rec.loggerName} : ${rec.message}');
      if (rec.error != null) {
        print(rec.error);
      }
      if (rec.stackTrace != null) {
        print(rec.stackTrace);
      }
    });
  }

  void override(Map mm) {
    // deep copy
    var m = json.decode(json.encode(mm));

    m.keys.forEach((key) {
      if (!_data.containsKey(key)) {
        logger.warning('override -- failed -- key ${key} is unknown');
      }
    });

    _data.keys.forEach((key) {
      if (m.containsKey(key)) {
        logger.config('override -- key ${key} is overrided');
        _data[key] = m[key];
      }
    });
  }

  void initialize(Map m) {
    // deep copy
    _data = json.decode(json.encode(m));

  }

  void addAll(Map m) {
    m.forEach((k,v){
      _data[k] = v;
    });
  }

  operator [](String name) {
    return _data[name];
  }

  Map asMap() => new Map.from(_data);
}
