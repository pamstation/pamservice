import 'dart:async';

class CacheEntry<T> {
  DateTime date;
  final T object;

  CacheEntry(this.object) {
    date = new DateTime.now();
  }
}

class Cache<K, T> {
  Map<K, CacheEntry<T>> _map;

  Cache() {
    _map = {};
  }

  void put(K key, T object, {Duration lifeTime: const Duration(seconds: 1)}) {
    _map[key] = new CacheEntry(object);
    if (lifeTime != null) {
      new Timer(lifeTime, () {
        remove(key);
      });
    }
  }

  T get(K key) {
    var entry = getCacheEntry(key);
    if (entry == null) return null;
    return entry.object;
  }

  CacheEntry<T> getCacheEntry(K key) {
    return _map[key];
  }

  void remove(K key) {
    _map.remove(key);
  }
}
