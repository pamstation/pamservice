library sci_util_geom;

import 'dart:math' as math;

class Point2D {
  num x;
  num y;

  factory Point2D.fromPoint(Point2D p) {
    return new Point2D(p.x, p.y);
  }

  Point2D.zero() : this(0.0, 0.0);

  Point2D(this.x, this.y);

  Rectangle toRectangle() =>
      new Rectangle.fromCorner(this.x, this.x, this.y, this.y);

  Point2D toInt() => new Point2D(this.x.toInt(), this.y.toInt());
  Point2D round() => new Point2D(this.x.round(), this.y.round());

  Point2D.fromJson(Map json) {
    x = json["x"];
    y = json["y"];
  }

  bool get isPositive => x >= 0 && y >= 0;

  Point2D normalized() {
    var n = norm();
    //if (n == 0.0) throw new Exception("divided by zero");
    return new Point2D(x / n, y / n);
  }

  Point2D operator +(Point2D p) {
    return new Point2D(this.x + p.x, this.y + p.y);
  }

  void plus(Point2D o) {
    x += o.x;
    y += o.y;
  }

  void multScalar(double d) {
    x *= d;
    y *= d;
  }

  Point2D operator -(Point2D p) {
    return new Point2D(this.x - p.x, this.y - p.y);
  }

  Point2D divide(Point2D p) {
    return new Point2D(this.x / p.x, this.y / p.y);
  }

  Point2D operator /(num p) {
    return new Point2D(this.x / p, this.y / p);
  }

  num norm() {
    return math.sqrt(math.pow(this.x, 2) + math.pow(this.y, 2));
  }

  String toString() {
    return "${this.runtimeType}($x,$y)";
  }

  Map toJson() {
    return {"x": x, "y": y};
  }

  int get hashCode {
    return x.hashCode + y.hashCode;
  }

  bool operator ==(other) {
    return other is Point2D && other.x == x && other.y == y;
  }
}

//class AM3DPoint implements Point2D {
//  num x;
//  num y;
//  double z;
//  AM3DPoint(num a, num b, num c) {
//    this.x = a.toDouble();
//    this.y = b.toDouble();
//    this.z = c.toDouble();
//  }
//  AM3DPoint.zero() : this(0.0, 0.0, 0.0);
//  bool get isPositive => x >= 0 && y >= 0 && z >= 0;
//
//  Rectangle toRectangle() => null;
//  AM3DPoint toInt() => new AM3DPoint(x.toInt(), y.toInt(), z.toInt());
//  AM3DPoint round() => new AM3DPoint(x.round(), y.round(), z.round());
//
//  AM3DPoint ortho() {
//    AM3DPoint answer;
//    if (x > 0) {
//      if (y > 0) {
//        answer = new AM3DPoint(-y, x, z);
//      } else {
//        answer = new AM3DPoint(-y, x, z);
//      }
//    } else {
//      if (y > 0) {
//        answer = new AM3DPoint(y, -x, z);
//      } else {
//        answer = new AM3DPoint(y, -x, z);
//      }
//    }
//    return answer.normalized();
//  }
//
//  AM3DPoint operator -(AM3DPoint o) {
//    return new AM3DPoint(x - o.x, y - o.y, z - o.z);
//  }
//
//  String toString() {
//    return '$runtimeType($x,$y,$z)';
//  }
//
//  AM3DPoint abs() {
//    return new AM3DPoint(x.abs(), y.abs(), z.abs());
//  }
//
//  void minus(AM3DPoint o) {
//    x -= o.x;
//    y -= o.y;
//    z -= o.z;
//  }
//
//  void plus(AM3DPoint o) {
//    x += o.x;
//    y += o.y;
//    z += o.z;
//  }
//
//  AM3DPoint operator +(AM3DPoint o) {
//    return new AM3DPoint(x + o.x, y + o.y, z + o.z);
//  }
//
//  AM3DPoint divide(AM3DPoint o) {
//    return new AM3DPoint(x / o.x, y / o.y, z / o.z);
//  }
//
//  double operator *(AM3DPoint o) {
//    return (x * o.x) + (y * o.y) + (z * o.z);
//  }
//
//  double norm() {
//    return math.pow((x * x) + (y * y) + (z * z), 0.5);
//  }
//
//  AM3DPoint normalized() {
//    var n = norm();
//    //if (n == 0.0) throw new Exception("divided by zero");
//    return new AM3DPoint(x / n, y / n, z / n);
//  }
//
//  void multScalar(double d) {
//    x *= d;
//    y *= d;
//    z *= d;
//  }
//
//  AM3DPoint copy() {
//    return new AM3DPoint(x, y, z);
//  }
//
//  void copyPoint(AM3DPoint p) {
//    x = p.x;
//    y = p.y;
//    z = p.z;
//  }
//
//  bool isNaN() {
//    return x.isNaN || y.isNaN || z.isNaN;
//  }
//
//  AM3DPoint operator /(num p) {
//    return new AM3DPoint(this.x / p, this.y / p, this.z / p);
//  }
//
//  Object toJson() {
//    return {"x": x, "y": y, "z": z};
//  }
//}

class Rectangle {
  Point2D topLeft;
  Point2D size;

  Rectangle(this.topLeft, this.size);

  Point2D get bottomRight => new Point2D(xmax, ymax);

  num get xmax => topLeft.x + size.x;
  num get xmin => topLeft.x;
  num get ymax => topLeft.y + size.y;
  num get ymin => topLeft.y;

  Rectangle.fromTopLeft(num xmin, num ymin, num width, num height) {
    topLeft = new Point2D(xmin, ymin);
    size = new Point2D(width, height);
  }
  Rectangle.fromCorner(num xmin, num xmax, num ymin, num ymax) {
    topLeft = new Point2D(xmin, ymin);
    size = new Point2D(xmax - xmin, ymax - ymin);
  }

  String toString() => "${this.runtimeType}($topLeft,$bottomRight)";

  bool containsPoint(Point2D p) => contains(p.x, p.y);

  bool contains(num x, num y) =>
      (x >= xmin && x <= xmax && y >= ymin && y <= ymax);

  bool intersect(Rectangle rec) =>
      _intersect(this, rec) || _intersect(rec, this);

  bool _intersect(Rectangle rec1, Rectangle rec2) {
    return rec1.contains(rec2.xmin, rec2.ymax) ||
        rec1.contains(rec2.xmin, rec2.ymin) ||
        rec1.contains(rec2.xmax, rec2.ymax) ||
        rec1.contains(rec2.xmax, rec2.ymin) ||
        rec1.contains(rec2.center.x, rec2.center.y);
  }

  int get hashCode => topLeft.hashCode + bottomRight.hashCode;

  bool operator ==(other) {
    return other is Rectangle && other.topLeft == topLeft && other.size == size;
  }

  void move(Point2D point) => topLeft.plus(point);

  Map toJson() {
    return {"xmin": xmin, "xmax": xmax, "ymin": ymin, "ymax": ymax};
  }

  void set center(Point2D _center) {
    throw "not implemented";
  }

  Point2D get center => this.topLeft + (this.size / 2);

  num get height => size.y;

  num get width => size.x;

  Rectangle inset(num x, num y) {
    if (width < 0 || height < 0) return _asPositiveSize().inset(x, y);
    return new Rectangle.fromTopLeft(
        xmin + x, ymin + y, width - 2 * x, height - 2 * y);
  }

  Rectangle _asPositiveSize() {
    return new Rectangle.fromTopLeft(
        math.min(xmin, xmax), math.min(ymin, ymax), width.abs(), height.abs());
  }

  Rectangle computeInstersectionRectangle(Rectangle rectangle) {
    // TODO: implement computeInstersectionRectangle
    return null;
  }

  Rectangle round() {
    int x = xmin.round();
    int y = ymin.round();
    int w = (xmin + width).round() - x;
    int h = (ymin + height).round() - y;
    return new Rectangle.fromTopLeft(x, y, w, h);
  }
}
