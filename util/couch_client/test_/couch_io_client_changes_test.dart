//library couch_client_test;
//
//import 'dart:async';
//import 'package:couch_client/couch_client.dart';
//import 'package:couch_client/couch_io_client.dart';
//import 'package:test/test.dart';
//import 'package:couch_client/src/server/password.dart' as pwdlib;
//
//import 'package:logging/logging.dart';
//
//CouchIOClient client = new CouchIOClient(
//    new Uri(
//        scheme: "https",
//        userInfo: 'app21103014.heroku:F8MT0N4Y3WDnMLR3g08tG7PO',
//        host: 'app21103014.heroku.cloudant.com'),
//    passGen: new pwdlib.PasswordGeneratorImpl([
//      0x00,
//      0x11,
//      0x22,
//      0x33,
//      0x44,
//      0x55,
//      0x66,
//      0x77,
//      0x88,
//      0x99,
//      0xAA,
//      0xBB,
//      0xCC,
//      0xDD,
//      0xEE,
//      0xFF
//    ]));
//
//var userid = "toto";
//var userdb = "dbtoto";
//
//void main() {
//  //sessionTest();
//
////  Logger mainLog = new Logger("Main");
//  Logger.root.level = Level.ALL;
//  Logger.root.onRecord.listen((LogRecord rec) {
//    print('${rec.level.name}:${rec.loggerName}: ${rec.time}: ${rec.message}');
//    if (rec.error != null) {
//      print(rec.error);
//    }
//    if (rec.stackTrace != null) {
//      print(rec.stackTrace);
//    }
//  });
//
//  client.couchServer.userDatabase.ensureExists().then((_) {
//    var users = [userid];
//    return Future.forEach(users, (user) {
//      return client.couchServer.userDatabase.userExists(user).then((flag) {
//        if (flag) return client.couchServer.userDatabase.deleteUser(user);
//      });
//    }).then((_) {
//      var dbs = [userdb];
//      return Future.forEach(dbs, (db) {
//        return client.database(db).exists.then((flag) {
//          if (flag) return client.database(db).delete();
//        });
//      });
//    });
//  }).then((_) {
//    client.database(userdb).create().then((_) {
//      sessionTest();
//    });
//  });
//}
//
//sessionTest() {
//  Document doc;
//
//  test("test 01 : create doc", () {
//    doc = new Document.fromMap({"testChanges": 1});
//    return client.database(userdb).postDocument(doc);
//  });
//
//  test("test 00 : changes on unknown doc", () {
//    var changes = expectAsync((ChangeResults results) {
//      print("test 00 : $results");
//      expect(results.results.isEmpty, isTrue);
//    });
//
//    client
//        .database(userdb)
//        .changes()
//        .longPool(
//            new ChangesQuery(doc_ids: ["12121212121212"], include_docs: true))
//        .then(expectAsync(changes));
//  });
//
//  test("test 02 : changes", () {
//    var changes = expectAsync((ChangeResults map) {
//      print("test 02 : $map");
//    });
//
//    client
//        .database(userdb)
//        .changes()
//        .longPool(new ChangesQuery(doc_ids: [doc.id], include_docs: true))
//        .then(expectAsync(changes));
//    return client.database(userdb).postDocument(doc).then(print);
//  });
//
//  test("test 03 : changes", () {
//    doc["testChanges"] = 2;
//
//    var changes = expectAsync((ChangeResults map) {
//      print("test 03 : $map");
//    });
//
//    client.database(userdb).updateDocument(doc).then((_) {
//      client
//          .database(userdb)
//          .changes()
//          .longPool(new ChangesQuery(doc_ids: [doc.id], include_docs: true))
//          .then(changes);
//    });
//  });
//
//  test("test 04 : changes stream", () {
//    int counter = 4;
//
//    var changes = expectAsync((ChangeResults map) {
//      print("test 04 : $map");
//      doc["testChanges"] = counter;
//      counter++;
//      client.database(userdb).updateDocument(doc);
//    }, count: 3);
//
//    ChangeStream stream = client.database(userdb).changes().longPoolStream(
//        new ChangesQuery(doc_ids: [doc.id], include_docs: true),
//        duration: 5 * 1000,
//        maxCount: 3);
//    stream.listen(changes);
//  });
//
////  test("test 05 : changes stream", (){
////
////    int counter = 0;
////    StreamSubscription<ChangeResults> sub;
////
////    var changes = expectAsync((ChangeResults map){
////
////      print("test 05 : $map");
////      if (counter == 0){
////        doc["testChanges"] = counter++;
////        client.database(userdb).updateDocument(doc);
////      } else if (counter == 1 ) {
////        expect(map.results.isNotEmpty, isTrue);
////      } else {
////        expect(map.results.isEmpty, isTrue);
////        sub.cancel();
////      }
////    }, count: 3);
////
////    ChangeStream stream = client.database(userdb).changes().longPoolStream(new ChangesQuery(doc_ids: [doc.id], include_docs: true), duration: 5*1000);
////    sub = stream.listen(changes);
////  });
//}
