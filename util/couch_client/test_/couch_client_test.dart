//library couch_client_test;
//
//import 'dart:async';
//import 'package:couch_client/couch_client.dart';
//import 'package:couch_client/couch_io_client.dart';
//import 'package:test/test.dart';
//
//import 'package:couch_client/src/server/password.dart' as pwdlib;
//
////CouchClient client = new CouchClient(new Uri.https('app21103014.heroku.cloudant.com', ''), 'gredlyourplandsterromano','02qa2y1ybgyT5mMDarTV8HcU');
//CouchIOClient client = new CouchIOClient(
//    new Uri(
//        scheme: "https",
//        userInfo: 'app21103014.heroku:F8MT0N4Y3WDnMLR3g08tG7PO',
//        host: 'app21103014.heroku.cloudant.com'),
//    passGen: new pwdlib.PasswordGeneratorImpl([
//      0x00,
//      0x11,
//      0x22,
//      0x33,
//      0x44,
//      0x55,
//      0x66,
//      0x77,
//      0x88,
//      0x99,
//      0xAA,
//      0xBB,
//      0xCC,
//      0xDD,
//      0xEE,
//      0xFF
//    ]));
//
//var userid = "toto";
//var userdb = "dbtoto";
//
//void main() {
//  //sessionTest();
//
//  client.couchServer.userDatabase.ensureExists().then((_) {
//    var users = ["user1", "user2", "user3", userid];
//    return Future.forEach(users, (user) {
//      return client.couchServer.userDatabase.userExists(user).then((flag) {
//        if (flag) return client.couchServer.userDatabase.deleteUser(user);
//      });
//    }).then((_) {
//      var dbs = ["user1", "user2", "user3", "race", userdb];
//      return Future.forEach(dbs, (db) {
//        return client.database(db).exists.then((flag) {
//          if (flag) return client.database(db).delete();
//        });
//      });
//    });
//  }).then((_) {
//    sessionTest();
//  });
//}
//
//sessionTest() {
//  test("test 01 : server", () {
//    return client.couchServer.get().then((Map server) {
//      print(server);
//    });
//  });
//
//  test("test 02 : session", () {
//    return client.session.get().then((Map session) {
//      print(session);
//    });
//  });
//
//  test("test 08 : create end user database", () {
//    return client.couchServer.userDatabase
//        .createUser(userid, "alex")
//        .then((Map session) {
//      return client.couchServer.createEndUserDatabase(userid, userdb).then((_) {
//        return client.couchServer.userDatabase
//            .getUserBasicCredential(userid)
//            .then((userifno) {
//          print("userifno $userifno");
//          CouchClient endClient = new CouchIOClient(new Uri(
//              scheme: "https",
//              userInfo: userifno,
//              host: 'app21103014.heroku.cloudant.com'));
//          Database userDb = endClient.database(userdb);
//          return userDb.postDocument(
//              new Document.fromMap({"_id": "id01", "hello": "you"}));
//        });
//      });
//    });
//  });
//
//  test("create users databases", () {
//    var users = ["user1", "user2", "user3"];
//    return Future.forEach(users, (user) {
//      return client.couchServer.userDatabase.createUser(user, user).then((_) {
//        return client.couchServer.createEndUserDatabase(user, user);
//      });
//    });
//  });
//
//  test("create race database", () {
//    var doc = new Document.fromMap({
//      "_id": "_design/race",
//      "language": "javascript",
//      "filters": {
//        "user1": "function(doc) {return doc.user == 'user1';}",
//        "user2": "function(doc) {return doc.user == 'user2';}",
//        "user3": "function(doc) {return doc.user == 'user3';}"
//      }
//    });
//    return client.database("race").create().then((_) {
//      return client.database("race").design("race").create(doc);
//    });
//  });
//
//  test("create replication", () {
//    var docs = [];
//    var race_uri = client.database("race").databaseUri;
//    var users = ["user1", "user2", "user3"];
//    docs = users
//        .map((user) => new ReplicationDocument(
//            race_uri, client.database(user).databaseUri,
//            filter: "race/$user", continuous: true, create_target: false))
//        .toList();
//    return client.couchServer.replicationDatabase.postDocuments(docs);
//  });
//
//  test("feed race", () {
//    var docs = [];
//    docs.add(new Document.fromMap({"user": "user1", "value": 42}));
//    docs.add(new Document.fromMap({"user": "user2", "value": 43}));
//    docs.add(new Document.fromMap({"user": "user1", "value": 45}));
//    docs.add(new Document.fromMap({"user": "user1", "value": 4}));
//    docs.add(new Document.fromMap({"user": "user2", "value": 40}));
//    docs.add(new Document.fromMap({"user": "user3", "value": 40}));
//    return client.database("race").postDocuments(docs);
//  });
//
////  test("test 03 : createDatabase", (){
////    return client.createDatabase("test").then((Map session){
////                print(session);
////              });
////      });
////
////  test("test 03 : setDatabaseSecurity", (){
////    var security = {"members":{"names":[userid], "roles":[]}};
////        return client.setDatabaseSecurity("test", security).then((Map security){
////                    print(security);
////                  });
////          });
////
////  test("test 03 : getDatabaseSecurity", (){
////      return client.getDatabaseSecurity("test").then((Map security){
////                  print(security);
////                });
////        });
////
////  test("test 03 : deleteDatabase", (){
////    return client.deleteDatabase("test").then((Map session){
////                  print(session);
////                });
////        });
////
////  test("test 04 : createApiKey", (){
////    return client.createApiKey().then((Map key){
////      print(key);
////    });
////  });
////
////  test("test 05 : createUser", (){
////    return client.userExists(userid).then((flag){
////      if (flag){
////        return client.deleteUser(userid).then((_){
////          return client.createUser(userid,"al").then((Map session){
////                    print(session);
////                  });
////        });
////      } else {
////        return client.createUser(userid,"al").then((Map session){
////          print(session);
////        });
////      }
////    });
////  });
////
////  test("test 06 : getUser", (){
////    return client.getUser(userid).then((Map session){
////      print(session);
////    });
////  });
////
////  test("test 07 : create end user database", (){
////    return client.getUser(userid).then((Map user){
////      print(user);
////      //return client.createDatabase(user["_id"]).then((Map m){
////        return client.setPermissions(user["_id"], user["apiKey"]["key"], ["_reader", "_writer"]).then((Map perm){
////          CouchClient endClient = new CouchClient(new Uri.https('app21103014.heroku.cloudant.com', ''), user["apiKey"]["key"],user["apiKey"]["password"]);
////          return endClient.login().then((_){
////            return endClient.putDocument(user["_id"], {"hello":"you"});
////          });
////        });
//////      }).then((_){
//////        return client.deleteDatabase(user["_id"]);
//////      });
////    });
////  });
//
////
////  test("test 06 : createUser2", (){
////    var userExist = false;
////    return client.createUser2(userid,"alex","a").then((Map session){
////      return client.userExists(userid).then((flag){
////        userExist = flag;
////      }).then((_){
////        return client.deleteUser(userid);
////      }).then((_){
////        expect(userExist, isTrue);
////      });
////    });
////  });
////
////  test("test 07 : create user session", (){
////      return client.createUser2(userid,"alex","a").then((Map session){
////        return client.getUser(userid).then((Map user){
////          print(user);
////          CouchClient endClient = new CouchClient(new Uri.https('app21103014.heroku.cloudant.com', ''),  userid , "a");
////          return endClient.login().then((_){
////            print(endClient.authSession);
////          });
////        }).then((_){
////          return client.deleteUser(userid);
////        }).then((_){
////
////        });
////      });
////    });
////
//}
