library couch_client_test;

//import 'dart:async';
import 'package:logging/logging.dart';
import 'package:couch_client/couch_client.dart';
import 'package:couch_client/couch_io_client.dart';
import 'package:test/test.dart';

CouchIOClient client = new CouchIOClient(new Uri(
    scheme: "http", userInfo: 'admin:password', host: '127.0.0.1', port: 5984));
String test_db_name = "test_temp_view";
Logger mainLog;
void main() {
  mainLog = new Logger("Main");
  Logger.root.level = Level.ALL;
  Logger.root.onRecord.listen((LogRecord rec) {
    print('${rec.level.name}:${rec.loggerName}: ${rec.time}: ${rec.message}');
    if (rec.error != null) {
      print(rec.error);
    }
    if (rec.stackTrace != null) {
      print(rec.stackTrace);
    }
  });

  var db = client.database(test_db_name);
  db.exists.then((flag) {
    if (!flag) {
      return db.create().then((_) {
        var docs = [];
        var doc = new Document.fromMap({"userId": "guest", "timestamp": 1});
        docs.add(doc);
        doc = new Document.fromMap({"userId": "guest", "timestamp": 2});
        docs.add(doc);
        doc = new Document.fromMap({"userId": "guest", "timestamp": 3});
        docs.add(doc);
        doc = new Document.fromMap({"userId": "alex", "timestamp": 1});
        docs.add(doc);
        doc = new Document.fromMap({"userId": "alex", "timestamp": 2});
        docs.add(doc);
        doc = new Document.fromMap({"userId": "alex", "timestamp": 3});
        docs.add(doc);
        return db.postDocuments(docs);
      });
    }
  }).then((_) {
    viewQueryTest();
  });
}

viewQueryTest() {
  test("test 01 : temp query all userId", () {
    var tempView = client.database(test_db_name).tempView;
    var map = '''
function(doc) {
  if (doc.userId) {
    emit(doc.userId, null);
  }
}
''';
    var tmpQuery = new ViewTempQuery(map: map);
    var query = new ViewQueryParam();
    return tempView.query(tmpQuery, query).then((answer) {
      mainLog.fine("test 01 : query all userId : $answer");
    });
  });

  test("02 : design", () {
    var designName = "mytestdesign";
    var viewName = "testview";
    Design design = client.database(test_db_name).design(designName);
    return design.exists.then((b) {
      if (!b) {
        var designDoc = new DesignDocument(design.name);
        var viewDoc = new DesignViewDocument(viewName);
        viewDoc.map = 'function(doc){ emit(doc.userId, doc.timestamp)}';
        designDoc.addView(viewDoc);
        return design.create(designDoc.toDocument());
      }
    }).then((_) {
      var view = design.view(viewName);
      var query = new ViewQueryParam();
      return view.query(query).then((answer) {
        mainLog.fine("02 : query design : $answer");
      });
    });
  });
}
