part of couch_client;

abstract class Session {
  Map<String,String> get authHeaders;
  Future<Map> get();
}

class SessionImpl implements Session {
  CouchClientImpl client;
  String basicAuth;

  SessionImpl(this.client) {
    if (server.userInfo != null && server.userInfo.length > 0) {
      basicAuth = "Basic ${base64.encode(server.userInfo.codeUnits)}";
    }
  }

  Map<String,String> get authHeaders {
    if (this.basicAuth != null)
      return {'Authorization': basicAuth};
    else
      return {};
  }

  Logger get log => this.client.log;

  void error(Response res) => this.client.error(res);
  HttpClient get http => client.client;
  Uri get server => client.server;

  Uri get sessionUri => server.resolve('_session');

  Future<Map> get() {
    return http.get(sessionUri, headers: authHeaders).then((Response res) {
      if (res.statusCode != 200) this.error(res);
      return json.decode(res.body);
    });
  }

  Future<Map> login() => get();

  Future<String> cookie(String username, String password) {
    log.fine("postSession $username");
    return http.post(this.sessionUri,
        body: {"name": username, "password": password}).then((Response res) {
      if (res.statusCode != 200) this.error(res);
      log.fine("postSession res ${res.body}");
      log.fine("postSession set-cookie ${res.headers["set-cookie"]}");
      return res.headers["set-cookie"];
    });
  }
}
