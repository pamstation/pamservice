part of couch_client;

class Server {
  CouchClientImpl client;
  UserDatabase userDatabase;
  ReplicationDatabase replicationDatabase;

  Server(this.client, {PasswordGenerator passGen}) {
    userDatabase = new UserDatabase(this, passGen);
    replicationDatabase = new ReplicationDatabase(this);
  }

  Logger get log => this.client.log;
  void error(Response res) => this.client.error(res);
  HttpClient get http => client.client;
  Map get authHeaders => client.session.authHeaders;
  Uri get server => client.server;

  Uri get apiKeyUri => new Uri.https("cloudant.com", "/api/generate_api_key");
  Uri get setPermissionsUri =>
      new Uri.https("cloudant.com", "/api/set_permissions");

  ServerDatabase database(String name) => new ServerDatabase(this, name);

  Future<Map> get() {
    return http.get(server, headers: authHeaders).then((Response res) {
      if (res.statusCode != 200) this.error(res);
      return json.decode(res.body);
    });
  }

  Future<Map> createApiKey() {
    return http.post(apiKeyUri, headers: authHeaders).then((Response res) {
      if (res.statusCode != 200) this.error(res);
      return json.decode(res.body);
    });
  }

  Future<Map> createEndUserDatabase(String userid, String dbname,
      {List<String> members}) {
    log.fine("createEndUserDatabase userid $userid $dbname");
    List<String> mem = members == null ? [userid] : members;
    if (!mem.contains(userid)) mem.add(userid);
    var end_user_db = this.database(dbname);
    return end_user_db.create().then((_) {
      return end_user_db.setSecurity({
        "couchdb_auth_only": true,
        "members": {"names": mem}
      });
    });
  }
}
