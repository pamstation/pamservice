part of couch_client;

class ServerDatabase extends DatabaseImpl {
  Server couchServer;

  ServerDatabase(Server couch_server, String name)
      : super(couch_server.client, name) {
    couchServer = couch_server;
  }

  Uri get databaseSecurityUri => this.resolveUri("_security");
  Uri get setPermissionsUri =>
      new Uri.https("cloudant.com", "/api/set_permissions");

  Future<Map> setSecurity(Map security) {
    log.fine("setDatabaseSecurity database $name security $security");
    var headers = new Map.from(authHeaders);
    headers["content-type"] = "application/json";
    return http
        .put(databaseSecurityUri, headers: headers, body: json.encode(security))
        .then((Response res) {
      if (res.statusCode != 200) this.error(res);
      return json.decode(res.body);
    });
  }

  Future<Map> getSecurity() {
    log.fine("getSecurity");
    return http
        .get(databaseSecurityUri, headers: authHeaders)
        .then((Response res) {
      if (res.statusCode != 200) this.error(res);
      return json.decode(res.body);
    });
  }

  Future<Map> addMembers(Set<String> members) {
    log.fine("addMembers $members");
    return getSecurity().then((Map sec) {
      Map mem = sec["members"];
      if (mem != null) {
        Set names = (mem["names"] as List).toSet();
        int s = names.length;
        names.addAll(members);
        if (names.length > s) {
          mem["names"] = names.toList();
        } else
          return {};
      } else {
        mem = {"names": members.toList()};
        sec["members"] = mem;
      }
      return setSecurity(sec);
    });
  }

  Future<Map> get databaseSecurity {
    return http
        .get(databaseSecurityUri, headers: authHeaders)
        .then((Response res) {
      if (res.statusCode != 200) this.error(res);
      return json.decode(res.body);
    });
  }

  Future<Map> setPermissions(String usernameOrApiKey, List roles) {
    return http.post(setPermissionsUri, headers: authHeaders, body: {
      "username": usernameOrApiKey,
      "database": "app21103014.heroku/$name",
      "roles": "_writer"
    }).then((Response res) {
      if (res.statusCode != 200) this.error(res);
      return json.decode(res.body);
    });
  }
}
