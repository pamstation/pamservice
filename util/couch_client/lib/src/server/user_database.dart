part of couch_client;

class UserDatabase extends ServerDatabase {
  static const String USER_ROLE = "user";

  PasswordGenerator _pwdGen;

  UserDatabase(Server couch_server, this._pwdGen)
      : super(couch_server, "_users");

  Future ensureExists() {
    return this.exists.then((flag) {
      if (!flag) return this.create();
    });
  }

  Future<Map> createUser(String userid, String displayName,
      {String namespace: "org.couchdb.user",
      String password,
      List<String> roles}) {
    log.fine("createUser userid $userid");

    Map pwd = _pwdGen.generatePassword(password);

    var headers = new Map.from(authHeaders);
    headers["content-type"] = "application/json";

    List user_roles = roles == null ? [USER_ROLE] : roles;

    return http
        .put(resolveUri("$namespace:$userid"),
            headers: headers,
            body: json.encode({
              "name": userid,
              "displayName": displayName,
              "encpwd": pwd["encpwd"],
              "password_sha": pwd["password_sha"],
              "salt": pwd["salt"],
              "roles": user_roles,
              "type": "user"
            }))
        .then((Response res) {
      if (res.statusCode != 201) this.error(res);
      return json.decode(res.body);
    });
  }

  Future<Document> getUser(String id, {String namespace: "org.couchdb.user"}) {
    return this.getDocument("$namespace:$id");
  }

  Future<Map> deleteUser(String id, {String namespace: "org.couchdb.user"}) {
    return getUser(id).then((Document user) {
      return this.deleteDocument("$namespace:$id", user.rev);
    });
  }

  Future<bool> userExists(String id, {String namespace: "org.couchdb.user"}) {
    return this.documentExists("$namespace:$id");
  }

  Future<String> getUserBasicCredential(String userid) {
    log.fine("getUserBasicCredential $userid");
    return this.getUser(userid).then((Document user) {
      var pwd = _pwdGen.decodePassword(user["encpwd"]);
      return "$userid:$pwd";
    });
  }
}
