//library couch_io_password;
//
//import 'dart:convert';
//import 'dart:math' as math;
//import "dart:typed_data";
//import 'package:couch_client/couch_client.dart';
//
//import 'package:cipher/cipher.dart';
////import "package:cipher/impl/server.dart";
//import "package:cipher/impl/client.dart";
//
//import 'package:crypto/crypto.dart' as crypto;
//
//class PasswordGeneratorImpl implements PasswordGenerator {
//  BlockCipher enccipher;
//  BlockCipher deccipher;
//
//  PasswordGeneratorImpl(List<int> lkey) {
//    initCipher();
//    if (lkey == null) lkey = new Uint8List(16);
//    Uint8List key = new Uint8List.fromList(lkey);
//    var params = new KeyParameter(key);
//    enccipher = new BlockCipher("AES")..init(true, params);
//    deccipher = new BlockCipher("AES")..init(false, params);
//  }
//
//  Map generatePassword([String password]) {
//    var pwd = password == null ? _generatePassword() : password;
//
//    var encpwd =
//        base64.encode(enccipher.process(new Uint8List.fromList(pwd.codeUnits)));
//    var salt = _generateSalt();
//    var sha = crypto.sha1;
//    var digest = sha.convert("$pwd$salt".codeUnits);
//    var password_sha = digest.toString();
//    return {"encpwd": encpwd, "password_sha": password_sha, "salt": salt};
//  }
//
//  String _generatePassword() {
//    StringBuffer sb = new StringBuffer();
//    var rnd = new math.Random();
//    for (int i = 0; i < 16; i++) {
//      sb.write(rnd.nextInt(9));
//    }
//    return sb.toString();
//  }
//
//  String _generateSalt() {
//    StringBuffer sb = new StringBuffer();
//    var rnd = new math.Random();
//    for (int i = 0; i < 16; i++) {
//      sb.write(rnd.nextInt(9));
//    }
//    return sb.toString();
//  }
//
//  String decodePassword(String encpwd) {
//    return new String.fromCharCodes(
//        deccipher.process(new Uint8List.fromList(base64.decode(encpwd))));
//  }
//}
