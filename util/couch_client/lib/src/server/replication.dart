part of couch_client;

class ReplicationDatabase extends ServerDatabase {
  Server couchServer;

  ReplicationDatabase(Server couch_Server) : super(couch_Server, "_replicator");

//  Future<ReplicationDocument> postDocument(ReplicationDocument doc) {
//    return super.postDocument(doc);
//  }
//
//  Future<List<ReplicationDocument>> postDocuments(
//      List<ReplicationDocument> docs) {
//    print(docs);
//    return super.postDocuments(docs);
//  }
}

class ReplicationDocument extends Document {
  ReplicationDocument(Uri source, Uri target,
      {bool cancel,
      bool continuous,
      bool create_target,
      List<String> doc_ids,
      String filter,
      Uri proxy,
      Map query_params,
      bool use_checkpoints}) {
    this["source"] = source.toString();
    this["target"] = target.toString();
//    String scheme,
//           this.userInfo: "",
//           String host: "",
//           port: 0,
//           String path,
//
//    Uri suri = new Uri(scheme: source.scheme, host: source.host, port: source.port, path: source.path);
//    Uri turi = new Uri(scheme: target.scheme, host: target.host, port: target.port, path: target.path);
//
//    this["source"] = {"url":suri.toString(), "headers": {"Authorization":"Basic ${CryptoUtils.bytesToBase64(source.userInfo.codeUnits)}"}};
//    this["target"] = {"url":turi.toString(), "headers": {"Authorization":"Basic ${CryptoUtils.bytesToBase64(target.userInfo.codeUnits)}"}};
//
//    this["user_ctx"] =  {
//      "name": "app21103014.heroku",
//      "roles": [
//        "_admin",
//        "_reader",
//        "_writer"
//      ]
//    };

    if (cancel != null) this["cancel"] = cancel;
    if (continuous != null) this["continuous"] = continuous;
    if (create_target != null) this["create_target"] = create_target;
    if (doc_ids != null) this["doc_ids"] = doc_ids;
    if (filter != null) this["filter"] = filter.toString();
    if (proxy != null) this["proxy"] = proxy.toString();
    if (query_params != null) this["query_params"] = query_params;
    if (use_checkpoints != null) this["use_checkpoints"] = use_checkpoints;
  }
}
