part of couch_client;

abstract class Database {
  String name;
  Future<bool> get exists;
  Future<Map> create();
  Future<Map> delete();
  Future<Document> postDocument(Document doc);
  Future<Document> putDocument(Document doc);
  Future<Document> updateDocument(Document doc, {String rev});
  Future queryAllDocs(QueryParams qp);
  Future queryAllDocsPost(List<String> keys, {bool include_docs: true});
  Future<List<Document>> postDocuments(List<Document> docs);
  Future<Document> getDocument(String id, {String ifNoneMatchRev});
  Future<bool> documentExists(String id);
  Future<Map> deleteDocument(String id, String rev);

  Design design(String design_name);
  ViewTemp get tempView;
  Uri get databaseUri;
  Changes changes();
  Replication get replication;
}

class DatabaseImpl implements Database {
  CouchClientImpl client;
  String name;

  DatabaseImpl(this.client, this.name);

  Design design(String design_name) => new DesignImpl(this, design_name);
  Changes changes() => new ChangesImpl(this);

  Logger get log => this.client.log;
  void error(Response res) => this.client.error(res);
  HttpClient get http => client.client;
  Map<String,String> get authHeaders => client.session.authHeaders;
  Uri get server => client.server;
  Uri resolveUri(String uri) => server.resolve("$name/$uri");
  Uri get databaseUri => server.resolve(name);
  Uri get bulkdatabaseUri => resolveUri("_bulk_docs");
  Uri documentUri(String id) => resolveUri("$id");

  Uri databaseAllDocsUri(QueryParams qp) {
    Uri answer = resolveUri("_all_docs");
    if (qp == null) return answer;
    answer = new Uri(
        scheme: answer.scheme,
        userInfo: answer.userInfo,
        host: answer.host,
        port: answer.port,
        pathSegments: answer.pathSegments,
        queryParameters: qp.params);
    return answer;
  }

  @override
  Future<bool> get exists {
    return http.head(databaseUri, headers: authHeaders).then((Response res) {
      if (res.statusCode != 200) return false;
      return true;
    });
  }

  @override
  Future<Map> create() {
    return http.put(databaseUri, headers: authHeaders).then((Response res) {
      if (res.statusCode != 201) this.error(res);
      return json.decode(res.body);
    });
  }

  @override
  Future<Map> delete() {
    return http.delete(databaseUri, headers: authHeaders).then((Response res) {
      if (res.statusCode != 200) this.error(res);
      return json.decode(res.body);
    });
  }

  static const String JSON_CONTENT_TYPE = "application/json";
//  static const String JSON_CONTENT_TYPE_UTF8 = "application/json ; charset=utf-8";

  Object decode(str) {}

  @override
  Future<Document> postDocument(Document doc) async {
    var headers = authHeaders;
    headers["content-type"] = JSON_CONTENT_TYPE;

    Response res = await http.post(databaseUri,
        headers: headers, body: json.encode(doc.toJson()));

    if (res.statusCode < 200 || res.statusCode >= 300) this.error(res);
    var jsonData = json.decode(res.body);
    doc.rev = jsonData["rev"];
    doc.id = jsonData["id"];

//    var tmp = await this.getDocument(doc.id);
//
//    print(tmp.toJson());

    return doc;
  }

  @override
  Future<Document> putDocument(Document doc) {
    var headers = authHeaders;
    headers["content-type"] = JSON_CONTENT_TYPE;
    return http
        .put(resolveUri(doc.id),
            headers: headers, body: json.encode(doc.toJson()))
        .then((Response res) {
      if (res.statusCode < 200 || res.statusCode >= 300) this.error(res);
      var jsonData = json.decode(res.body);
      doc.rev = jsonData["rev"];
      doc.id = jsonData["id"];
      return doc;
    });
  }

  @override
  Future<Document> updateDocument(Document doc, {String rev}) {
    String aRev = rev == null ? doc.rev : rev;
    if (aRev == null || doc.id == null)
      return new Future.error("id and/or rev must not be null");
    var headers = authHeaders;
    headers["If-Match"] = aRev;
    return http
        .put(documentUri(doc.id),
            headers: headers, body: json.encode(doc.toJson()))
        .then((Response res) {
      if (res.statusCode < 200 || res.statusCode >= 300) this.error(res);
      var jsonData = json.decode(res.body);
      doc.rev = jsonData["rev"];
      doc.id = jsonData["id"];
      return doc;
    });
  }

  /// couchdb do not provide charset in content type, so default is Latin1 on response
  @override
  Future queryAllDocs(QueryParams qp) {
    var headers = authHeaders;
//    headers["content-type"] = JSON_CONTENT_TYPE;
    return http
        .get(databaseAllDocsUri(qp), headers: headers)
        .then((Response res) {
      if (![200, 202].contains(res.statusCode)) this.error(res);

      return json.decode(res.body);
    });
  }

  /// couchdb do not provide charset in content type, so default is Latin1 on get
  @override
  Future queryAllDocsPost(List<String> keys, {bool include_docs: true}) async {
    var watch = new Stopwatch()..start();
    var headers = authHeaders;
    headers["content-type"] = JSON_CONTENT_TYPE;
    var url = resolveUri("_all_docs")
        .replace(queryParameters: {"include_docs": include_docs.toString()});

    var res = await http.post(url,
        headers: headers, body: json.encode({"keys": keys}));

    if (res.statusCode != 200) this.error(res);

//      return JSON.fuse(utf8).decoder.convert(res.body);

//    print('$this queryAllDocsPost -- ${watch.elapsedMilliseconds} ms');
    return json.decode(res.body);
  }

  Future<List<Document>> postDocuments(List<Document> docs) {
    return new Future.sync(() {
      var headers = authHeaders;
      headers["content-type"] = JSON_CONTENT_TYPE;
      Map map = {"docs": docs.map((doc) => doc.toJson()).toList()};
      return http
          .post(bulkdatabaseUri, headers: headers, body: json.encode(map))
          .then((Response res) {
        if (res.statusCode < 200 || res.statusCode >= 300) this.error(res);
        List list = json.decode(res.body);
        assert(list.length == docs.length);
        for (int i = 0; i < list.length; i++) {
          Map m = list[i];
          docs[i].rev = m["rev"];
          docs[i].id = m["id"];
        }
        return docs;
      });
    });
  }

  @override
  Future<Document> getDocument(String id, {String ifNoneMatchRev}) async {
//    var watch = new Stopwatch()..start();
    if (id == null || id.trim().isEmpty)
      throw new CouchClientException.from(
          400, 'couchdb.id.missing', 'id is required');

    var headers = authHeaders;
    if (ifNoneMatchRev != null) {
      headers["if-none-match"] = '"${ifNoneMatchRev}"';
    }
    var res = await http.get(documentUri(id), headers: headers);
//    print('$this getDocument -- ${watch.elapsedMilliseconds} ms');
    if (res.statusCode >= 200 && res.statusCode < 300) {
      return new Document.fromMap(json.decode(res.body));
    } else if (res.statusCode == 304 && ifNoneMatchRev != null)
      return null;
    else
      this.error(res);
  }

  @override
  Future<bool> documentExists(String id) {
    return http
        .head(documentUri(id), headers: authHeaders)
        .then((Response res) {
      if (res.statusCode < 200 || res.statusCode >= 300) return false;
      return true;
    });
  }

  Future<Map> deleteDocument(String id, String rev) {
    var headers = authHeaders;
    headers["If-Match"] = rev;
    return http.delete(documentUri(id), headers: headers).then((Response res) {
      if (res.statusCode < 200 || res.statusCode >= 300) this.error(res);
      return json.decode(res.body);
    });
  }

  @override
  Replication get replication {
    throw "not yet implemented";
  }

  @override
  ViewTemp get tempView => new ViewTempImpl(this);
}
