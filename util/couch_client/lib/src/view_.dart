part of couch_client;

abstract class View {
  Future query([ViewQueryParam query]);
}

abstract class ViewTemp {
  Future query(ViewTempQuery tempQuery, [ViewQueryParam query]);
}

class ViewTempImpl implements ViewTemp {
  DatabaseImpl database;

  ViewTempImpl(this.database);
  Logger get log => this.database.log;
  HttpClient get http => database.http;
  void error(Response res) => database.error(res);
  Map get authHeaders => database.authHeaders;
  Uri get viewUri => database.resolveUri("_temp_view");

  Uri queryUri(ViewQueryParam query) {
    Uri answer = viewUri;
    if (query == null) return answer;
    answer = new Uri(
        scheme: answer.scheme,
        userInfo: answer.userInfo,
        host: answer.host,
        port: answer.port,
        pathSegments: answer.pathSegments,
        queryParameters: query.args);
    return answer;
  }

  Future query(ViewTempQuery tempQuery, [ViewQueryParam query]) {
    var headers = new Map.from(authHeaders);
    headers["content-type"] = "application/json";
    return http
        .post(queryUri(query),
            headers: headers, body: json.encode(tempQuery.toJson()))
        .then((Response res) {
      if (res.statusCode != 200) this.error(res);
      return json.decode(res.body);
    });
  }
}

class ViewTempQuery {
  Map _data;
  ViewTempQuery({String map, String reduce}) {
    _data = {};
    if (map != null) {
      _data["map"] = map;
    }
    if (reduce != null) {
      _data["reduce"] = reduce;
    }
  }

  Map toJson() => _data;
}

class ViewImpl implements View {
  DesignImpl design;
  String name;

  ViewImpl(this.design, this.name);

  Logger get log => this.design.log;
  HttpClient get http => design.http;
  void error(Response res) => design.error(res);
  Map get authHeaders => design.authHeaders;
//  Uri resolveUri(String uri) => design.resolveUri("$uri");
  Uri resolveUri(String uri) => design.resolveUri("_view/$uri");
  Uri get viewUri => resolveUri(name);

  Uri queryUri(ViewQueryParam query) {
    Uri answer = viewUri;
    if (query == null) return answer;
    var qp = query.args;
    if (qp.containsKey('keys')) {
      qp = new Map.from(qp);
      qp.remove('keys');
    }
    answer = new Uri(
        scheme: answer.scheme,
        userInfo: answer.userInfo,
        host: answer.host,
        port: answer.port,
        pathSegments: answer.pathSegments,
        queryParameters: qp);
    return answer;
  }

  Future query([ViewQueryParam query]) async {
//    var watch = new Stopwatch()..start();
    var headers = new Map<String,String>.from(authHeaders);
    Response res;
    if (query.args.containsKey('keys')) {
      headers["content-type"] = "application/json";
      res = await http.post(queryUri(query),
          headers: headers, body: json.encode({'keys': query.args['keys']}));
    } else {
      res = await http.get(queryUri(query), headers: headers);
    }

    if (res.statusCode != 200) this.error(res);

//    print('$this query -- ${query} -- ${watch.elapsedMilliseconds} ms');
    return json.decode(res.body);
  }
}

class ViewQueryParam {
  Map<String, dynamic> args = {};

  ViewQueryParam(
      {bool descending,
      endkey,
      String endkey_docid,
      bool group,
      int group_level,
      bool include_docs,
      bool inclusive_end,
      key,
      List keys,
      int limit,
      bool reduce,
      int skip,
      String stale,
      startkey,
      String startkey_docid,
      bool update_seq}) {
    if (descending != null) args["descending"] = descending.toString();

    if (endkey_docid != null) args["endkey_docid"] = endkey_docid.toString();
    if (group != null) args["group"] = group.toString();
    if (group_level != null) args["group_level"] = group_level.toString();
    if (include_docs != null) args["include_docs"] = include_docs.toString();
    if (inclusive_end != null) args["inclusive_end"] = inclusive_end.toString();

    if (key != null) {
      if (key is String)
        args["key"] = '"$key"';
      else if (key is List) args["key"] = json.encode(key);
      else throw 'bad key';
    }

    // GET
//    if (keys != null) args["keys"] = json.encode(keys);

    // POST
     if (keys != null) args["keys"] = keys;

    if (limit != null) args["limit"] = limit.toString();
    if (reduce != null) args["reduce"] = reduce.toString();
    if (skip != null) args["skip"] = skip.toString();
    if (stale != null) args["stale"] = stale.toString();

    if (startkey_docid != null)
      args["startkey_docid"] = startkey_docid.toString();
    if (update_seq != null) args["update_seq"] = update_seq.toString();

    if (startkey != null) {
      if (startkey is String)
        args["startkey"] = '"$startkey"';
      else if (startkey is List) args["startkey"] = json.encode(startkey);
    }
    if (endkey != null) {
      if (endkey is String)
        args["endkey"] = '"$endkey"';
      else if (endkey is List) args["endkey"] = json.encode(endkey);
    }
  }
}
