part of couch_client;

abstract class Design {
  String get name;
  Future<bool> get exists;
  Future<Document> create(Document doc);
  Future<Document> update(Document doc);
  View view(String view_name);
  Future<DesignDocument> get();
}

class DesignDocument {
  Map _data;
  DesignDocument(String name, {String language: 'javascript'}) {
    _data = {"_id": "_design/${name}", 'language': language};
  }

  DesignDocument.fromJson(this._data);

  void addView(DesignViewDocument view) {
    _views[view.name] = view.toJson();
  }

  DesignViewDocument getView(String name) {
    if (!hasView(name)) throw 'view $name not found';
    return new DesignViewDocument.fromJson(_views[name])..name = name;
  }

  Map get _views {
    if (!_data.containsKey("views")) {
      _data["views"] = {};
    }
    return _data["views"];
  }

  bool hasView(String name) => _views.containsKey(name);

  Map toJson() => _data;

  Document toDocument() {
    return new Document.fromMap(toJson());
  }
}

class DesignViewDocument {
  String name;
  Map _data;
  DesignViewDocument(this.name) {
    _data = {};
  }

  DesignViewDocument.fromJson(this._data);

  String get map => _data["map"];
  set map(String code) {
    _data["map"] = code;
  }

  String get reduce => _data["reduce"];
  set reduce(String code) {
    _data["reduce"] = code;
  }

  Map toJson() => _data;
}

class DesignImpl implements Design {
  DatabaseImpl database;
  String name;

  DesignImpl(this.database, this.name);

  Logger get log => this.database.log;
  HttpClient get http => database.http;
  void error(Response res) => database.error(res);
  Map get authHeaders => database.authHeaders;
  Uri resolveUri(String uri) => database.resolveUri("_design/${name}/$uri");
  Uri get databaseDesignDocUri =>
      database.resolveUri("_design/${name}"); // resolveUri(name);

  @override
  Future<bool> get exists {
    return http
        .head(databaseDesignDocUri, headers: authHeaders)
        .then((Response res) {
      if (res.statusCode != 200) return false;
      return true;
    });
  }

  @override
  Future<DesignDocument> get() async {
    Response res = await http.get(databaseDesignDocUri, headers: authHeaders);
    if (res.statusCode != 200) {
      database.error(res);
    }
    var answer = json.decode(res.body);
    return new DesignDocument.fromJson(answer);
  }

  @override
  Future<Document> create(Document doc) {
    return this.database.putDocument(doc);
  }

  @override
  Future<Document> update(Document doc) {
    return this.database.updateDocument(doc, rev: doc.rev);
  }

  View view(String view_name) => new ViewImpl(this, view_name);
}
