part of couch_client;

String newUuid() {
  Uuid uuid = new Uuid();
  return uuid.v4();
}
