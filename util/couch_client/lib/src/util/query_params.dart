part of couch_client;

class QueryParams {
  static const String END_KEY_FIX = "\ufff0";
  Map<String, Object> _params;

  QueryParams(
      {bool descending: false,
      String endkey,
      bool include_docs: false,
      bool inclusive_end: true,
      String key,
      int limit,
      int skip: 0,
      String startkey}) {
    _params = {};
    if (descending != null) _params["descending"] = descending;
    if (endkey != null) _params["endkey"] = endkey;
    if (include_docs != null) _params["include_docs"] = include_docs;
    if (inclusive_end != null) _params["inclusive_end"] = inclusive_end;
    if (key != null) _params["key"] = key;
    if (limit != null) _params["limit"] = limit;
    if (skip != null) _params["skip"] = skip;
    if (startkey != null) _params["startkey"] = startkey;
  }

  Map get asMap {
    return _params;
  }

  Map get params {
    var answer = {};
    _params.forEach((k, v) {
      answer[k] = v.toString();
    });
    if (answer.containsKey("endkey")) {
      var endkey = answer["endkey"];
      answer["endkey"] = '"${endkey}"';
    }
    if (answer.containsKey("startkey")) {
      var startkey = answer["startkey"];
      answer["startkey"] = '"${startkey}"';
    }
    if (answer.containsKey("key")) {
      var key = answer["key"];
      answer["key"] = '"${key}"';
    }
    return answer;
  }
}
