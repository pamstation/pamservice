part of couch_client;

class Document {
  static final ID = "_id";
  static final REV = "_rev";
  Map _doc;

  Document.fromMap(Map m) {
    _doc = m;
  }

  Document.fromMap2(Map m) {
    _doc = new Map.from(m);
    if (m['id'] != null && (m['id'] as String).isNotEmpty) _doc[ID] = m['id'];
    if (m['rev'] != null && (m['rev'] as String).isNotEmpty)
      _doc[REV] = m['rev'];
    _doc..remove('id')..remove('rev');
  }

  Document({String an_id}) {
    _doc = new Map();
    if (an_id != null) _doc[ID] = an_id;
  }

  Object operator [](String key) => _doc[key];
  void operator []=(String key, value) {
    _doc[key] = value;
  }

  String get id => _doc[ID];
  set id(String an_id) => _doc[ID] = an_id;

  String get rev => _doc[REV];
  set rev(String revn) {
    if (revn != null)
      _doc[REV] = revn;
    else
      removeRev();
  }

  void removeRev() {
    if (_doc.containsKey(REV)) _doc.remove(REV);
  }

  bool get hasRev => rev != null;

  Map toJson() => _doc;
  Map toJson2() {
    var m = new Map.from(_doc);
    m['id'] = m['_id'];
    m['rev'] = m['_rev'];
    m..remove('_id')..remove('_rev');
    return m;
  }

  String toString() => _doc.toString();

  int get hashCode {
    if (this.rev == null || this.id == null) return super.hashCode;
    int result = 17;
    result = 37 * result + this.id.hashCode;
    result = 37 * result + this.rev.hashCode;
    return result;
  }

  // You should generally implement operator== if you override hashCode.
  bool operator ==(other) {
    if (other is! Document || this.rev == null || this.id == null) return false;
    Document d = other;
    return (d.id == this.id && d.rev == this.rev);
  }
}
