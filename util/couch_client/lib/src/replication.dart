part of couch_client;

abstract class Replication {
  Stream from({remote, Map options});
  Stream to({remote, Map options});
}

class ReplicationImpl implements Replication {
  Database database;

  ReplicationImpl(this.database);

  Stream from({remote, Map options}) {
    throw "not yet implemented";
  }

  Stream to({remote, Map options}) {
    throw "not yet implemented";
  }
}
