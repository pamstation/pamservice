part of couch_client;

abstract class CouchClient {
  Uri get server;
  Server get couchServer;
  Session get session;
  Database database(String name);
  HttpClient get httpClient;
}

class CouchClientImpl implements CouchClient {
  final Logger log = new Logger('CouchClient');

  Uri server;
  HttpClient client;
  Session session;
  Server couchServer;

  CouchClientImpl(this.server, {HttpClient client, PasswordGenerator passGen}) {
    session = new SessionImpl(this);
    couchServer = new Server(this, passGen: passGen);
    this.client = client;
  }

  HttpClient get httpClient => client;

  Database database(String name) => new DatabaseImpl(this, name);

  void error(Response res) {
    Map answer;
    try {

      answer = json.decode(res.body);
      if (!answer.containsKey("statusCode")) {
        answer["statusCode"] = res.statusCode;
      }
    } catch (e) {
      throw new CouchClientException({
        "error": "unknown",
        "reason": e.toString(),
        "statusCode": res.statusCode,
        "body": res.body
      });
    }
    throw new CouchClientException(answer);
  }
}

class CouchClientException {
  Map _data;
  CouchClientException(this._data);
  CouchClientException.from(int statusCode, String error, String reason){
    this._data = {};
    _data["statusCode"] = statusCode;
    _data["error"] = error;
    _data["reason"] = reason;

  }

  String toString() {
    return "CouchClientException($_data)";
  }

  Map toJson() => _data;

  String get reason => _data["reason"];
  String get error => _data["error"];
  int get statusCode => _data["statusCode"];

  bool get isBadRequestError => statusCode == 400;
  bool get isUnauthorizedError => statusCode == 401;
  bool get isConflictError => statusCode == 409;
  bool get isNotFoundError => error == "not_found" && reason == "missing";
  bool get isDeletedError => error == "not_found" && reason == "deleted";

  bool get isUnknownError => error == "unknown";
}
