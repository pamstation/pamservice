part of couch_client;

abstract class Changes {
  Future<ChangeResults> longPool(ChangesQuery query);
  ChangeStream longPoolStream(ChangesQuery query, {int duration, int maxCount});
}

class ChangesImpl implements Changes {
  DatabaseImpl database;

  ChangesImpl(this.database);

  Logger get log => this.database.log;
  HttpClient get http => database.http;
  void error(Response res) => database.error(res);
  Map get authHeaders => database.authHeaders;
  Uri get changeUri => database.resolveUri("_changes");

  Uri changeQueryUri(ChangesQuery query) {
    Uri answer = database.resolveUri("_changes");
    if (query == null) return answer;
    answer = new Uri(
        scheme: answer.scheme,
        userInfo: answer.userInfo,
        host: answer.host,
        port: answer.port,
        pathSegments: answer.pathSegments,
        queryParameters: query.params);
    return answer;
  }

  Future pool(ChangesQuery query) async {
    query.feed = ChangesQuery.FEED_POLL;
    return http
        .get(changeQueryUri(query), headers: authHeaders)
        .then((Response res) {
      if (res.statusCode != 200) this.error(res);
      return new ChangeResults.fromMap(json.decode(res.body));
    });
  }

  Future<ChangeResults> longPool(ChangesQuery query) {
    query.feed = ChangesQuery.FEED_LONGPOLL;
    log.finest("longPool ${changeQueryUri(query)}");
    return http
        .get(changeQueryUri(query), headers: authHeaders)
        .then((Response res) {
      if (res.statusCode != 200) this.error(res);
      return new ChangeResults.fromMap(json.decode(res.body));
    });
  }

  ChangeStream longPoolStream(ChangesQuery query,
      {int duration, int maxCount}) {
    throw 'mot impl';
//    return new LongPollChangeStream(this, query, duration, maxCount);
  }

  Stream continuous(ChangesQuery query) {
    query.feed = ChangesQuery.FEED_CONTINUOUS;
    return null;
  }
}

class ChangesQuery {
  static const String FEED_POLL = "normal";
  static const String FEED_LONGPOLL = "longpoll";
  static const String FEED_CONTINUOUS = "continuous";

  Map _params;
  ChangesQuery(
      {List<String> doc_ids,
      String filter,
      int heartbeat,
      bool include_docs,
      int limit,
      String since,
      bool descending,
      int timeout}) {
    _params = {};
    if (doc_ids != null) _params["doc_ids"] = doc_ids;
    if (filter != null) _params["filter"] = filter;
    if (heartbeat != null) _params["heartbeat"] = heartbeat;
    if (include_docs != null) _params["include_docs"] = include_docs;
    if (limit != null) _params["limit"] = limit;
    if (since != null) _params["since"] = since;
    if (descending != null) _params["descending"] = descending;
    if (timeout != null) _params["timeout"] = timeout;
  }

  String get feed => _params["feed"];
  set feed(String f) => _params["feed"] = f;

  String get since => _params["since"];
  set since(String f) => _params["since"] = f;

  Map<String, String> get params {
    var m = {};
    if (_params["feed"] != null) m["feed"] = _params["feed"];
    if (_params["doc_ids"] != null) {
      m["doc_ids"] = json.encode(_params["doc_ids"]);
      m["filter"] = "_doc_ids";
    }
    if (_params["filter"] != null) m["filter"] = _params["filter"];
    if (_params["heartbeat"] != null)
      m["heartbeat"] = _params["heartbeat"].toString();
    if (_params["include_docs"] != null)
      m["include_docs"] = _params["include_docs"].toString();
    if (_params["limit"] != null) m["limit"] = _params["limit"].toString();
    if (_params["since"] != null) m["since"] = _params["since"].toString();
    if (_params["descending"] != null)
      m["descending"] = _params["descending"].toString();
    if (_params["timeout"] != null)
      m["timeout"] = _params["timeout"].toString();
    return m;
  }
}

class ChangeResults {
  Map _map;
  ChangeResults.fromMap(this._map);

  List<ChangeResult> get results => (_map["results"] as List)
      .map((  m) => new ChangeResult.fromMap(m))
      .toList();
  String get last_seq => _map["last_seq"];
  int get pending => _map["pending"];

  String toString() => _map.toString();
}

class ChangeResult {
  Map _map;
  ChangeResult.fromMap(this._map);
  String get id => _map["id"];
  String get seq => _map["seq"];
  List<Map> get changes => _map["changes"];
  Map get doc => _map["doc"];
  String toString() => _map.toString();
}

abstract class ChangeStream extends Stream<ChangeResults> {
//  StreamSubscription<ChangeResults> _subscription;
  StreamController<ChangeResults> _controller;
  Timer _timer;
  int _interval;
  int _counter = 0;
  int _maxCount;

  ChangeStream(this._interval, [int max_count]) {
    this._maxCount = max_count;
    _controller = new StreamController<ChangeResults>(
        onListen: _startTimer,
        onPause: _stopTimer,
        onResume: _startTimer,
        onCancel: _stopTimer);
  }

//  StreamSubscription<ChangeResults> listen(void onData(ChangeResults results),
//      {void onError(Error error, StackTrace st),
//      void onDone(),
//      bool cancelOnError}) {
//    return _controller.stream.listen(onData,
//        onError: onError, onDone: onDone, cancelOnError: cancelOnError);
//  }

  void _startTimer() {
    _stopTimer();
    _timer = new Timer.periodic(new Duration(milliseconds: _interval), _tick);
  }

  void _stopTimer() {
    if (_timer != null) {
      _timer.cancel();
      _timer = null;
    }
  }

  void _tick(_) {
    requestChanges().then((ChangeResults changes) {
      _counter++;
      _controller.add(changes);
      if (_maxCount != null && _counter >= _maxCount) {
        _stopTimer();
        _controller.close();
      }
    }, onError: (e) {
      _stopTimer();
      _controller.addError(e);
    });
  }

  Future<ChangeResults> requestChanges();
}

abstract class LongPollChangeStream extends ChangeStream {
  ChangesQuery query;
  ChangesImpl changes;

  LongPollChangeStream(this.changes, this.query, int everyMilliseconds,
      [int max_count])
      : super(everyMilliseconds, max_count);

  Future<ChangeResults> requestChanges() {
    return this.changes.longPool(this.query).then((ChangeResults results) {
      this.query.since = results.last_seq;
      return results;
    });
  }
}
