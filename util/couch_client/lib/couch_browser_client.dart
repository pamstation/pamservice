library couch_browser_client;

import 'package:couch_client/couch_client.dart';
import 'package:http_client/http_browser_client.dart';

class CouchBrowserClient extends CouchClientImpl {
  CouchBrowserClient(Uri server,
      {bool withCredentials: false, PasswordGenerator passGen})
      : super(server, passGen: passGen) {
    client = new HttpBrowserClient(withCredentials: withCredentials);
  }
}
