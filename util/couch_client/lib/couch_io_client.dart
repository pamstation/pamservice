library couch_io_client;

import 'package:couch_client/couch_client.dart';
import 'package:http_client/http_io_client.dart';

import 'package:logging/logging.dart';

class CouchIOClient extends CouchClientImpl {
  final Logger log = new Logger('CouchIOClient');
  CouchIOClient(Uri server, {PasswordGenerator passGen})
      : super(server, client: new HttpIOClient(), passGen: passGen);
}
