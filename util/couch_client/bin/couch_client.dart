// import 'dart:async';
import 'package:couch_client/couch_client.dart';
import 'package:couch_client/couch_io_client.dart';

void main() {
  CouchIOClient client = new CouchIOClient(new Uri(
      scheme: "https",
      userInfo: 'app21103014.heroku:F8MT0N4Y3WDnMLR3g08tG7PO',
      host: 'app21103014.heroku.cloudant.com'));

  var user_doc = new Document.fromMap({
    "_id": "org.couchdb.user:106229596024700172564",
    "_rev": "1-acb37695da810ea653056253796dce49",
    "name": "106229596024700172564",
    "displayName": "Alex Zmantic",
    "encpwd": "FC15EHx8cITrq1ZDLJV6SQ==",
    "password_sha": "f86c92eba96a6212f3753908dc2e6b2adb5c0d40",
    "salt": "7602702687680076",
    "roles": ["user"],
    "type": "user"
  });
  client.couchServer.userDatabase.updateDocument(user_doc);
}
