part of sci.api.client.gen;

class UriServiceGen {
  static void process(Generator gen, ApiClass clazz, StringBuffer buffer) {
    ApiPersistentService meta =
        clazz.metadata.firstWhere((each) => each is ApiPersistentService);

    buffer
      ..writeln('Uri get uri => Uri.parse("${meta.uri}");')
      ..writeln('String get serviceName => "${meta.className}";')
      ..writeln('');
  }
}

class ServiceFactoryPropertyGen {
  static void process(Generator gen, ApiClass clazz, StringBuffer buffer) {
    ApiPersistentService meta =
    clazz.metadata.firstWhere((each) => each is ApiPersistentService);

    buffer
      ..writeln('ServiceFactoryBase factory;')

      ..writeln('');
  }
}

class JsonGen {
  static void process(Generator gen, ApiClass clazz, StringBuffer buffer) {
    ApiPersistentService meta =
        clazz.metadata.firstWhere((each) => each is ApiPersistentService);
    buffer
      ..writeln('Map toJson(${meta.className} object) => object.toJson();')
      ..writeln(
          '${meta.className} fromJson(Map m,{bool useFactory:true}) { if (m == null) return null; if (useFactory) return ${meta.className}Base.fromJson(m); return new ${meta.className}.json(m);}')
      ..writeln('');
  }
}

class CouchViewsGen {
  static void process(Generator gen, ApiClass clazz, ApiClass objectClazz,
      StringBuffer buffer) {
    List<ApiView> list =
        clazz.metadata.where((each) => each is ApiView).toList().cast<ApiView>();

    list.forEach((each) {
      processViewMethod(clazz, objectClazz, each, buffer);
    });
  }

  static void processViewMethod(ApiClass clazz, ApiClass objectClazz,
      ApiView metadataView, StringBuffer buffer) {
    ApiPersistentService meta =
        clazz.metadata.firstWhere((each) => each is ApiPersistentService);

    if (metadataView.startKeys != null) {
      buffer
        ..writeln(
            'Future<List<${meta.className}>> ${metadataView.name}({startKey, endKey, int limit:200, int skip:0, bool descending: true, bool useFactory: false, service.AclContext aclContext}){')
        ..writeln(
            'return findStartKeys("${metadataView.uri}",startKey:startKey,endKey:endKey,limit:limit,skip:skip,descending:descending,useFactory:useFactory,aclContext:aclContext);')
        ..writeln('}');
    } else if (metadataView.keys != null) {
      buffer
        ..writeln(
            'Future<List<${meta.className}>> ${metadataView.name}({List keys, bool useFactory: false, service.AclContext aclContext}){')
        ..writeln(
            'return findKeys("${metadataView.uri}",keys:keys,useFactory:useFactory,aclContext:aclContext);')
        ..writeln('}');
    }
  }
}

class MethodServiceGen {
  static void process(Generator gen, ApiClass clazz, StringBuffer buffer) {
    ApiPersistentService meta =
        clazz.metadata.firstWhere((each) => each is ApiPersistentService);
    clazz.methods
        .where((each) => !each.isConstructor)
        .forEach((each) => processMethod(gen, meta, each, buffer));
  }

  static void processWebSocketMethod(Generator gen, ApiPersistentService meta,
      ApiMethod method, StringBuffer buffer) {
    buffer.writeln('var params = {};');

    for (var arg in method.arguments) {
      arg.name;

      switch (arg.rangeType) {
        case 'num':
        case 'int':
        case 'double':
        case 'bool':
        case 'String':
          buffer.writeln('params["${arg.name}"] = ${arg.name};');
          break;
        case 'List':
          {
            if (arg.rangeTypes.isEmpty) {
              buffer.writeln('params["${arg.name}"] = ${arg.name};');
            } else {
              var rt = arg.rangeTypes.first;
              switch (rt) {
                case 'num':
                case 'int':
                case 'double':
                case 'bool':
                case 'String':
                  buffer.writeln('params["${arg.name}"] = ${arg.name};');
                  break;
                default:
                  buffer.writeln(
                      'params["${arg.name}"] = ${arg.name}.map((each)=>each.toJson()).toList();');
              }
            }

            break;
          }

        default:
          buffer.writeln('params["${arg.name}"] = ${arg.name}.toJson();');
      }
    }

    assert(method.returnTypes.isNotEmpty);

    buffer.writeln('''
      var decode = (s) => ${method.returnTypes.first}Base.fromJson(s as Map);
      return this.webSocketStream<${method.returnTypes.first}>(uri, params, decode);    
    } on ServiceError {
      rethrow;
    } catch (e, st) {
      onError(e, st);
    }
     ''');

    buffer.writeln('}');
  }

  static void processSimpleMethod(Generator gen, ApiPersistentService meta,
      ApiMethod method, StringBuffer buffer) {
    buffer.writeln('var params = {};');

    for (var arg in method.arguments) {
      arg.name;

      switch (arg.rangeType) {
        case 'num':
        case 'int':
        case 'double':
        case 'bool':
        case 'String':
          buffer.writeln('params["${arg.name}"] = ${arg.name};');
          break;
        case 'List':
          {
            if (arg.rangeTypes.isEmpty) {
              buffer.writeln('params["${arg.name}"] = ${arg.name};');
            } else {
              var rt = arg.rangeTypes.first;
              switch (rt) {
                case 'num':
                case 'int':
                case 'double':
                case 'bool':
                case 'String':
                  buffer.writeln('params["${arg.name}"] = ${arg.name};');
                  break;
                default:
                  buffer.writeln(
                      'params["${arg.name}"] = ${arg.name}.map((each)=>each.toJson()).toList();');
              }
            }

            break;
          }

        default:
          buffer.writeln('params["${arg.name}"] = ${arg.name}.toJson();');
      }
    }

    ApiResponseContentType responseContentType = method.metadata.firstWhere(
        (each) => each is ApiResponseContentType,
        orElse: () => const ApiResponseContentType('json'));

    var responseType = 'text';

    if (responseContentType.contentType == 'tson' ||
        method.returnType == 'Stream') {
      responseType = 'arraybuffer';
    }

    ApiRequestVerb requestVerb = method.metadata.firstWhere(
        (each) => each is ApiRequestVerb,
        orElse: () => const ApiRequestVerb('POST'));

    if (requestVerb.verb == 'GET') {
      buffer.writeln(
          'var geturi = getServiceUri(uri)'
              '.replace(queryParameters: {"params": json.encode(params)});');

      if (method.returnType == 'Stream') {
        buffer.writeln(
            'var resFut = client.get(geturi, '
                'responseType: contentCodec.responseType);');
      } else {
        buffer.writeln(
            'response = await client.get(geturi, '
                'responseType: contentCodec.responseType);');
      }
    } else if (requestVerb.verb == 'POST') {
      if (method.returnType == 'Stream') {
        buffer.writeln(
            'var resFut = client.post(getServiceUri(uri), '
                'responseType: contentCodec.responseType, '
                'body: contentCodec.encode(params));');
      } else {
        buffer.writeln(
            'response = await client.post(getServiceUri(uri), '
                'responseType: contentCodec.responseType,'
                ' body: contentCodec.encode(params));');
      }
    } else
      throw 'bad verb';
  }

  static void processMultipartMethod(Generator gen, ApiPersistentService meta,
      ApiMethod method, StringBuffer buffer) {
    buffer.writeln('var parts = new List<MultiPart>(${method.arguments.length});');

    for (var i = 0; i < method.arguments.length; i++) {
      var arg = method.arguments[i];
      switch (arg.rangeType) {
        case 'num':
        case 'int':
        case 'double':
        case 'bool':
        case 'String':
          buffer.writeln(
              'parts[$i] = new MultiPart({"Content-Type":"application/json"},'
                  ' string: json.encode([${arg.name}]));');
          break;
        case 'Stream':
          buffer.writeln(
              'parts[$i] = new MultiPart({"Content-Type":"application/octet-stream"},'
                  ' stream: ${arg.name}.cast<List<int>>() );');
          break;
        case 'List':
          {
            if (arg.rangeTypes.isEmpty) {
              buffer.writeln(
                  'parts[$i] = new MultiPart({"Content-Type":"application/json"},'
                      ' string: json.encode([${arg.name}]));');
            } else {
              var rt = arg.rangeTypes.first;
              switch (rt) {
                case 'num':
                case 'int':
                case 'double':
                case 'bool':
                case 'String':
                  buffer.writeln(
                      'parts[$i] = new MultiPart({"Content-Type":"application/json"},'
                          ' string: json.encode([${arg.name}]));');
                  break;

                default:
                  buffer.writeln(
                      'parts[$i] = new MultiPart({"Content-Type":"application/json"},'
                          ' string: json.encode([${arg.name}.map((each)=>each.toJson()).toList()]));');
              }
            }

            break;
          }

        default:
          buffer.writeln(
              'parts[$i] = new MultiPart({"Content-Type":"application/json"},'
                  ' string: json.encode([${arg.name}.toJson()]));');
      }
    }

    buffer
      ..writeln('var frontier = "ab63a1363ab349aa8627be56b0479de2";')
      ..writeln(
          'var bodyBytes = await new MultiPartMixTransformer(frontier).encode(parts);')
      ..writeln(
          'var headers = {"Content-Type": "multipart/mixed; boundary=\${frontier}"};')
      ..writeln(
          'response = await client.post(getServiceUri(uri),'
              ' headers: headers, body: bodyBytes, responseType: "arraybuffer");');
  }

  static void processMethod(Generator gen, ApiPersistentService meta,
      ApiMethod method, StringBuffer buffer) {
    buffer
      ..write(method.returnApiType.returnRange)
      ..write(' ')
      ..write(method.name)
      ..write('(')
      ..write(
          method.arguments.map((arg) => arg.range + ' ' + arg.name).join(','))
      ..write(', {service.AclContext aclContext}  ');

    if (method.returnType != 'Stream') {
      buffer..writeln(') async {');
    } else {
      buffer..writeln(') {');
    }

    buffer.writeln('var answer;');
    buffer.writeln('try {');
    buffer.writeln(' var response;');

    buffer.writeln(
        'var uri = Uri.parse( "${meta.uri}" + "/" + "${method.name}");');

    if (method.arguments.any((arg) => arg.rangeType == 'Stream')) {
      processMultipartMethod(gen, meta, method, buffer);
    } else {
      bool isWebSocket = method.metadata.any((each) => each is ApiWebSocket);

      if (isWebSocket) {
        processWebSocketMethod(gen, meta, method, buffer);
        return;
      } else {
        processSimpleMethod(gen, meta, method, buffer);
      }
    }

    if (method.returnType == 'Stream') {
      buffer.writeln(''' 
      resFut = resFut.then((response){
         if (response.statusCode != 200) onResponseError(response);
         return response;
      });
      ''');
    } else {
      buffer.writeln('''if (response.statusCode != 200) {
        onResponseError(response);
      } else {
     ''');
    }

    ApiResponseContentType responseContentType = method.metadata.firstWhere(
        (each) => each is ApiResponseContentType,
        orElse: () => const ApiResponseContentType('json'));

//    var decoder = responseContentType.contentType == 'json'
//        ? 'json.decode'
//        : 'TSON.decode';

    switch (method.returnApiType.name) {
      case 'Future':
        {
          if (method.returnApiType.templateTypes.isNotEmpty) {
            var templateType = method.returnApiType.templateTypes.first;
            var rt = templateType.name;
            switch (rt) {
              case 'num':
              case 'int':
              case 'double':
              case 'bool':
              case 'String':
                buffer.writeln(
                    'answer = (contentCodec.decode(response.body) as List).first;');
                break;
              case 'dynamic':
                buffer.writeln('answer = null;');
                break;
              case 'List':
                if (templateType.templateTypes.isNotEmpty) {
                  var listTemplateType = templateType.templateTypes.first;
                  var listTemplateTypeName = listTemplateType.name;
                  switch (listTemplateTypeName) {
                    case 'num':
                    case 'int':
                    case 'double':
                    case 'bool':
                    case 'String':
                      buffer.writeln('answer = contentCodec.decode(response.body);');
                      break;
                    case 'dynamic':
                      buffer.writeln('answer = null;');
                      break;
                    default:
                      buffer.writeln(
                          'answer = (contentCodec.decode(response.body) as List).map((m)=> ${listTemplateTypeName}Base.fromJson(m as Map)).toList();');
                  }
                }
                break;
              default:
                buffer.writeln(
                    'answer = ${rt}Base.fromJson(contentCodec.decode(response.body) as Map);');
            }
          }
          buffer.writeln('}');
          break;
        }
      case 'Stream':
        {
          buffer.writeln(
              'var resFut2 = resFut.then((response)=>new Stream.fromIterable([new Uint8List.view(response.body as ByteBuffer)]));');
          buffer.writeln('answer = new async.LazyStream(()=>resFut2).cast<List<int>>();');
          break;
        }
      default:
        throw 'not supported';
    }

    buffer.writeln('''} on ServiceError {
      rethrow;
    } catch (e, st) {
      onError(e, st);
    }''');

    if (method.returnApiType.name == 'Future'){
      // we are not returning a Future
      buffer.writeln(''' 
    return answer as ${method.returnApiType.templateTypes.first.returnRange};''');
    } else {
      buffer.writeln(''' 
    return answer as ${method.returnApiType.returnRange };''');
    }


    buffer..writeln('}')..writeln();
  }
}
