part of sci.api.client.gen;

class ServiceFactoryBaseClassDeclGen {
  static void process(Generator gen, ApiLibrary apiLib, String baseClassName,
      StringBuffer buffer) {
    buffer.write('class ServiceFactoryBase  implements api.ServiceFactory');

    buffer.writeln('{');

    for (var clazz in apiLib.classes.values){
      var propertyName = clazz.name.substring(0,1).toLowerCase() + clazz.name.substring(1);
      buffer.writeln('${clazz.name} ${propertyName};');
    }

    //cstr
    buffer.writeln('ServiceFactoryBase(){');

    for (var clazz in apiLib.classes.values){
      var propertyName = clazz.name.substring(0,1).toLowerCase() + clazz.name.substring(1);
      buffer.writeln('${propertyName} = new ${clazz.name}()..factory=this;');
    }

    //end ctsr
    buffer.writeln('}');

    buffer.writeln('Future initialize() async {}');

    buffer.writeln('Future initializeWith(Uri uri, [HttpClient client]) async {');

    buffer.writeln('if (client == null) client = new HttpClient();');

    for (var clazz in apiLib.classes.values){
      var propertyName = clazz.name.substring(0,1).toLowerCase() + clazz.name.substring(1);
      buffer.writeln('await ${propertyName}.initialize(uri, client);');
    }

    //end initialize
    buffer.writeln('}');

    // end class
    buffer.writeln('}');
  }
}

class ServiceFactoryClassDeclGen {
  static void process(Generator gen, ApiLibrary apiLib, String baseClassName,
      StringBuffer buffer) {
    buffer.write('class ServiceFactory extends ServiceFactoryBase');
    buffer.writeln('{');
    buffer.writeln('}');
  }
}