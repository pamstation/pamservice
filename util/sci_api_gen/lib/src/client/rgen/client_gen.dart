part of sci.api.client.gen;

class RServiceDocGen {
  static void process(Generator gen, ApiClass clazz, String baseClassName,
      StringBuffer buffer) {
    buffer..writeln("#' ${clazz.name}")..writeln("#'")..writeln("#' @export");
    buffer..write("#' @format \\code{\\link{R6Class}} object");
    if (clazz.superName != 'Object') {
      buffer..write(', super class \\code{\\link{${clazz.superName}}}');
    }
    if (clazz.allSubClasses.isNotEmpty) {
      buffer..write(', sub classes ');
      buffer
        ..write(clazz.allSubClasses.map((each) {
          return '\\code{\\link{${each.name}}}';
        }).join(', '));
    }

    buffer..writeln('.');

    clazz.parentClasses.forEach((p) {
      p.dataProperties.forEach((pp) {
        buffer
          ..writeln(
              "#' @field ${pp.name} ${pp.isFunctional ? '' : 'list '}of type ${pp.range} inherited from super class \\code{\\link{${p.name}}}.");
      });
    });

    clazz.dataProperties.forEach((pp) {
      buffer
        ..writeln(
            "#' @field ${pp.name} ${pp.isFunctional ? '' : 'list '}of type ${pp.range}.");
    });

    clazz.parentClasses.forEach((p) {
      p.objectProperties.forEach((pp) {
        buffer
          ..writeln(
              "#' @field ${pp.name} ${pp.isFunctional ? 'object ' : 'list '}of class \\code{\\link{${pp.range}}} inherited from super class \\code{\\link{${p.name}}}.");
      });
    });

    clazz.objectProperties.forEach((pp) {
      buffer
        ..writeln(
            "#' @field ${pp.name} ${pp.isFunctional ? 'object ' : 'list '}of class \\code{\\link{${pp.range}}}.");
    });

    buffer..writeln("#' @section Methods:");
    buffer..writeln("#' \\describe{");
    ApiPersistentService meta =
        clazz.metadata.firstWhere((each) => each is ApiPersistentService);
    clazz.methods.where((each) => !each.isConstructor).forEach((each) {

      buffer..write("#'    \\item{\\code{${each.name}(");
      buffer.write(each.arguments.map((each) => each.name).join(','));
      buffer.write(")}}");
      buffer.writeln("{method}");
    });
    buffer..writeln("#' }");
    buffer..writeln("#' ");
  }
}

class ClientGen {
  static void process(ClientGenerator gen, StringBuffer buffer) {
    buffer.writeln(
        "ServiceFactory <- R6::R6Class('ServiceFactory', public=list(");

    for (var clazz in gen.apiLib.classes.values) {
      var propertyName =
          clazz.name.substring(0, 1).toLowerCase() + clazz.name.substring(1);
      buffer.writeln('${propertyName} = NULL,');
    }

    buffer.writeln("initialize = function(baseRestUri){");
    buffer.writeln('client = AuthHttpClient\$new()');
    for (var clazz in gen.apiLib.classes.values) {
      var propertyName =
          clazz.name.substring(0, 1).toLowerCase() + clazz.name.substring(1);
      buffer.writeln(
          'self\$${propertyName} = ${clazz.name}\$new(baseRestUri, client)');
    }

    buffer.writeln("}");
    //end public
    buffer.writeln(')');
    // end class
    buffer.writeln(')');
  }
}

class ClientServiceGen {
  static void process(ClientGenerator gen, ApiClass clazz, String baseClassName,
      StringBuffer buffer) {
    ApiPersistentService meta =
        clazz.metadata.firstWhere((each) => each is ApiPersistentService);

    buffer.writeln(
        "${clazz.name} <- R6::R6Class('${clazz.name}', inherit = ${baseClassName}, public=list(");
    buffer.writeln("initialize = function(baseRestUri, client){");
    buffer.writeln("super\$initialize(baseRestUri, client)");
    buffer.writeln("self\$uri='${meta.uri}'");
    buffer.writeln("}");

    ApiClass objectClazz = gen.modelApiLib.classes[meta.className];

    CouchViewsGen.process(gen, clazz, objectClazz, buffer);

    MethodServiceGen.process(gen, clazz, buffer);

    //end public
    buffer.writeln(')');
    //end class
    buffer.writeln(')');
  }
}

class CouchViewsGen {
  static void process(ClientGenerator gen, ApiClass clazz, ApiClass objectClazz,
      StringBuffer buffer) {
    List<ApiView> list =
        clazz.metadata.where((each) => each is ApiView).toList().cast();

    list.forEach((each) {
      processViewMethod(clazz, objectClazz, each, buffer);
    });
  }

  static void processViewMethod(ApiClass clazz, ApiClass objectClazz,
      ApiView metadataView, StringBuffer buffer) {
    if (metadataView.startKeys != null) {
      buffer
        ..writeln(
            ', ${metadataView.name} = function(startKey=NULL,endKey=NULL,limit=20,skip=0, descending=TRUE, useFactory=FALSE){')
        ..writeln(
            'return (self\$findStartKeys("${metadataView.uri}",startKey=startKey,endKey=endKey,limit=limit,skip=skip,descending=descending,useFactory=useFactory))')
        ..writeln('}');
    } else if (metadataView.keys != null) {
      buffer
        ..writeln(
            ', ${metadataView.name} = function(keys=NULL, useFactory= FALSE){')
        ..writeln(
            'return (self\$findKeys("${metadataView.uri}",keys=keys,useFactory=useFactory))')
        ..writeln('}');
    }
  }
}

class MethodServiceGen {
  static void process(Generator gen, ApiClass clazz, StringBuffer buffer) {
    ApiPersistentService meta =
        clazz.metadata.firstWhere((each) => each is ApiPersistentService);
    clazz.methods
        .where((each) => !each.isConstructor)
        .forEach((each) => processMethod(gen, meta, each, buffer));
  }

  static void processSimpleMethod(Generator gen, ApiPersistentService meta,
      ApiMethod method, StringBuffer buffer) {
    buffer.writeln('params = list()');

    for (var arg in method.arguments) {
      arg.name;

      switch (arg.rangeType) {
        case 'int':
          buffer.writeln(
              'params[["${arg.name}"]] = unbox(as.integer(${arg.name}))');
          break;
        case 'num':
        case 'double':
        case 'bool':
        case 'String':
          buffer.writeln('params[["${arg.name}"]] = unbox(${arg.name})');
          break;
        case 'List':
          {
            if (arg.rangeTypes.isEmpty) {
              buffer.writeln('params[["${arg.name}"]] = ${arg.name}');
            } else {
              var rt = arg.rangeTypes.first;
              switch (rt) {
                case 'int':
                  buffer.writeln(
                      'params[["${arg.name}"]] = lapply(${arg.name}, function(each) unbox(as.integer(each))');
                  break;
                case 'num':
                case 'double':
                case 'bool':
                case 'String':
                  buffer.writeln(
                      'params[["${arg.name}"]] = lapply(${arg.name}, unbox)');
                  break;
                default:
                  buffer.writeln(
                      'params[["${arg.name}"]] = lapply(${arg.name}, function(each) each\$toTson() )');
              }
            }

            break;
          }

        default:
          buffer.writeln('params[["${arg.name}"]] = ${arg.name}\$toTson()');
      }
    }

    ApiRequestVerb requestVerb = method.metadata.firstWhere(
        (each) => each is ApiRequestVerb,
        orElse: () => const ApiRequestVerb('POST'));

    if (requestVerb.verb == 'GET') {
      buffer.writeln('url = self\$getServiceUri(uri)');
      buffer.writeln('url\$query = list(params=toJSON(params))');
      buffer.writeln('response = self\$client\$get(url)');
    } else if (requestVerb.verb == 'POST') {
      buffer.writeln('url = self\$getServiceUri(uri)');
      buffer.writeln('response = self\$client\$post(url, body=params)');
    } else
      throw 'bad verb';
  }

  static void processMultipartMethod(Generator gen, ApiPersistentService meta,
      ApiMethod method, StringBuffer buffer) {
    buffer.writeln('parts = list()');

    for (var i = 0; i < method.arguments.length; i++) {
      var arg = method.arguments[i];
      switch (arg.rangeType) {
        case 'num':
        case 'int':
        case 'double':
        case 'bool':
        case 'String':
          buffer.writeln(
              'parts[[${i + 1}]] = MultiPart\$new(list("content-type"=tson.scalar("application/json")), content=list(${arg.name}))');
          break;
        case 'Stream':
          buffer
            ..writeln('if (is.raw(bytes)){')
            ..writeln(
                'parts[[${i + 1}]] = MultiPart\$new(list("content-type"=tson.scalar("application/octet-stream")), content=${arg.name});')
            ..writeln('} else {')
            ..writeln(
                'parts[[${i + 1}]] = MultiPart\$new(list("content-type"=tson.scalar("application/tson")), content=${arg.name});')
            ..writeln('}');
          break;
        case 'List':
          {
            if (arg.rangeTypes.isEmpty) {
              buffer.writeln(
                  'parts[[${i + 1}]] = MultiPart\$new(list("content-type"=tson.scalar("application/json")), content=list(${arg.name}))');
            } else {
              var rt = arg.rangeTypes.first;
              switch (rt) {
                case 'num':
                case 'int':
                case 'double':
                case 'bool':
                case 'String':
                  buffer.writeln(
                      'parts[[${i + 1}]] = MultiPart\$new(list("content-type"=tson.scalar("application/json")), content=list(${arg.name}))');
                  break;

                default:
                  buffer.writeln(
                      'parts[[${i + 1}]] = MultiPart\$new(list("content-type"=tson.scalar("application/json")), content=list(lapply(${arg.name},function(each) each\$toTson())))');
              }
            }

            break;
          }

        default:
          buffer.writeln(
              'parts[[${i + 1}]] = MultiPart\$new(list("content-type"=tson.scalar("application/json")), content=list(${arg.name}\$toTson()))');
      }
    }

    buffer
      ..writeln(
          'response = self\$client\$multipart(self\$getServiceUri(uri), body=lapply(parts, function(part) part\$toTson()))');
  }

  static void processMethod(Generator gen, ApiPersistentService meta,
      ApiMethod method, StringBuffer buffer) {
    buffer
      ..write(', ')
      ..write(method.name)
      ..write('=function(')
      ..write(method.arguments.map((arg) => arg.name).join(','))
      ..writeln(') {');

    buffer.writeln('answer = NULL');

    buffer.writeln('response = NULL');

    buffer.writeln('uri = paste0( "${meta.uri}", "/", "${method.name}")');

    if (method.arguments.any((arg) => arg.rangeType == 'Stream')) {
      processMultipartMethod(gen, meta, method, buffer);
    } else {
      processSimpleMethod(gen, meta, method, buffer);
    }

    buffer.writeln('''if (response\$status != 200) {
        self\$onResponseError(response, "${method.name}");
      } else {''');

    ApiResponseContentType responseContentType = method.metadata.firstWhere(
        (each) => each is ApiResponseContentType,
        orElse: () => const ApiResponseContentType('json'));

    var decoder = 'response\$content';

    switch (method.returnType) {
      case 'Future':
        {
          if (!method.returnTypes.isEmpty) {
            var rt = method.returnTypes.first;
            switch (rt) {
              case 'num':
              case 'int':
              case 'double':
              case 'bool':
              case 'String':
                buffer.writeln('answer = response\$content[[1]];');
                break;
              case 'dynamic':
                buffer.writeln('answer = NULL;');
                break;
              default:
                buffer.writeln(
                    'answer = createObjectFromJson(response\$content)');
            }
          }
          break;
        }
      case 'Stream':
        {
          buffer.writeln('answer = response\$content');
          break;
        }
      default:
        throw 'not supported';
    }

    buffer.writeln('}');

    buffer.writeln('return (answer)');

    buffer..writeln('}');
  }
}
