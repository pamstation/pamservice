library sci.api.client.gen;

import 'dart:io';
import 'dart:convert';
import 'package:path/path.dart' as path;

import '../../api_lib.dart';
import '../../api_gen.dart';

part './gen/client_gen.dart';
part './gen/factory_gen.dart';

class ClientGenerator extends Generator {
  ApiLibrary modelApiLib;
  String modelLibraryName;

  ClientGenerator(
      ApiLibrary apiLib,
      this.modelApiLib,
      String directory,
      String libraryName,
      String baseClassName,
      this.modelLibraryName,
      List<Map<String, String>> importLibs,
      List<Map<String, String>> exportLibs)
      : super(apiLib, directory, libraryName, baseClassName, importLibs,
            exportLibs) {
    classFolder = 'src/client/impl';
    baseFolder = 'src/client/base';
  }

  run() async {
    process();
    await Process
        .run('dartfmt', ['-w', directory],
            workingDirectory: Directory.current.path)
        .then((result) {
      stdout.write(result.stdout);
      stderr.write(result.stderr);
    });
  }

  void process() {
    new Directory(path.join(directory, classFolder))
        .createSync(recursive: true);
    new Directory(path.join(directory, baseFolder)).createSync(recursive: true);

    generateBaseLibrary(false);
    generateLibrary();

    apiLib.classes.values.forEach(generateBase);
    apiLib.classes.values.forEach(generateClass);

    generateServiceFactoryBase();
    generateServiceFactory();
  }

  void generateServiceFactoryBase() {
    var filename = path.join(directory, baseFolder, 'factory.dart');
    var file = new File(filename);
    var buffer = new StringBuffer();

    PartOfBaseGen.process(this, libraryName, buffer);

    ServiceFactoryBaseClassDeclGen.process(this, apiLib, baseClassName, buffer);

    file.writeAsStringSync(buffer.toString());
  }

  void generateServiceFactory() {
    var filename = path.join(directory, classFolder, 'factory.dart');
    var file = new File(filename);
    if (file.existsSync()) return;
    var buffer = new StringBuffer();

    PartOfGen.process(this, libraryName, buffer);

    ServiceFactoryClassDeclGen.process(this, apiLib, baseClassName, buffer);

    file.writeAsStringSync(buffer.toString());
  }

  void generateBase(ApiClass clazz) {
    var filename =
        path.join(directory, baseFolder, '${filenameForBaseClass(clazz)}');
    var file = new File(filename);
    var buffer = new StringBuffer();

    PartOfBaseGen.process(this, libraryName, buffer);
    ServiceBaseClassDeclGen.process(this, clazz, baseClassName, buffer);
    ServiceFactoryPropertyGen.process(this, clazz, buffer);
    UriServiceGen.process(this, clazz, buffer);
    JsonGen.process(this, clazz, buffer);

    ApiPersistentService meta =
        clazz.metadata.firstWhere((each) => each is ApiPersistentService);

    ApiClass objectClazz = modelApiLib.classes[meta.className];

    CouchViewsGen.process(this, clazz, objectClazz, buffer);
    MethodServiceGen.process(this, clazz, buffer);
    EndClassGen.process(this, clazz, buffer);

    file.writeAsStringSync(buffer.toString());
  }

  void generateBaseLibrary(bool isModel) {
    var filename = path.join(directory, '${libraryName}_base.dart');
    var file = new File(filename);
    var buffer = new StringBuffer();
    LibClientServiceBaseGen.process(
        this, libraryName, this.modelLibraryName, baseFolder, buffer);

    file.writeAsStringSync(buffer.toString());
  }

  void generateLibrary() {
    var filename = path.join(directory, '${libraryName}.dart');
    var file = new File(filename);
    var buffer = new StringBuffer();

    LibClientGen.process(
        this, libraryName, importLibs, exportLibs, classFolder, buffer);

    file.writeAsStringSync(buffer.toString());
  }

  void generateClass(ApiClass clazz) {
    var filename =
        path.join(directory, classFolder, '${filenameForClass(clazz)}');
    var file = new File(filename);
    if (file.existsSync()) return;
    var buffer = new StringBuffer();
    PartOfGen.process(this, libraryName, buffer);
    ClassDeclGen.process(this, clazz, baseClassName, buffer);

    EndClassGen.process(this, clazz, buffer);
    file.writeAsStringSync(buffer.toString());
  }
}
