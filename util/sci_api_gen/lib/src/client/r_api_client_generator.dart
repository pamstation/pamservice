library sci.api.client.gen;

import 'dart:io';
import 'dart:async';
import 'dart:convert';
import 'package:path/path.dart' as path;

import '../../api_lib.dart';
import '../../api_gen.dart';

part './rgen/client_gen.dart';

class ClientGenerator extends Generator {
  ApiLibrary modelApiLib;
  String modelLibraryName;

  ClientGenerator(
      ApiLibrary apiLib,
      this.modelApiLib,
      String directory,
      String libraryName,
      String baseClassName,
      this.modelLibraryName,
      List<Map<String, String>> importLibs,
      List<Map<String, String>> exportLibs)
      : super(apiLib, directory, libraryName, baseClassName, importLibs,
            exportLibs) {
    classFolder = 'R';
    baseFolder = 'R';
  }

  @override
  Future run() async {
    process();
    var result = await Process.run('R', ['-e', 'formatR::tidy_dir()'],
        workingDirectory: path.join(directory, baseFolder));
    stdout.write(result.stdout);
    stderr.write(result.stderr);
  }

  void process() {
    new Directory(path.join(directory, classFolder))
        .createSync(recursive: true);
    new Directory(path.join(directory, baseFolder)).createSync(recursive: true);
    generateFactory();
    apiLib.classes.values.forEach(generateBase);
  }

  void generateBase(ApiClass clazz) {
    var filename = path.join(directory, baseFolder,
        '${filenameForBaseClass(clazz, extension: 'R')}');
    var file = new File(filename);
    var buffer = new StringBuffer();

    RServiceDocGen.process(this, clazz, baseClassName, buffer);
    ClientServiceGen.process(this, clazz, baseClassName, buffer);

    file.writeAsStringSync(buffer.toString());
  }

  void generateFactory() {
    var filename = path.join(directory, baseFolder,
        'service_factory.R');
    var file = new File(filename);
    var buffer = new StringBuffer();

    ClientGen.process(this, buffer);
    file.writeAsStringSync(buffer.toString());
  }
}
