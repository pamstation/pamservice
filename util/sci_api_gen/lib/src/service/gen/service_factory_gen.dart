part of sci.api.service.gen;

class ServiceFactoryBaseClassDeclGen {
  static void process(Generator gen, ApiLibrary apiLib, String baseClassName,
      StringBuffer buffer) {
    buffer.write('class ServiceFactoryBase ');

    buffer.writeln('{');

    for (var clazz in apiLib.classes.values) {
      var propertyName =
          clazz.name.substring(0, 1).toLowerCase() + clazz.name.substring(1);
      buffer.writeln('${clazz.name} ${propertyName};');
    }

    //cstr
    buffer.writeln('ServiceFactoryBase(){');

    for (var clazz in apiLib.classes.values) {
      var propertyName =
          clazz.name.substring(0, 1).toLowerCase() + clazz.name.substring(1);
      buffer.writeln(' ${propertyName} = new ${clazz.name}()..factory = this;');
    }

    //end ctsr
    buffer.writeln('}');

    buffer.writeln('Future initialize() async {}');

    buffer.writeln(
        'Future initializeWith(couch.CouchClient client, service.AccessManager accessManager ) async {');

    for (var clazz in apiLib.classes.values) {
      var propertyName =
          clazz.name.substring(0, 1).toLowerCase() + clazz.name.substring(1);
      buffer.writeln('${propertyName}.accessManager = accessManager;');
      buffer.writeln('await ${propertyName}.initialize(client);');
    }

    for (var clazz in apiLib.classes.values) {
      var propertyName =
          clazz.name.substring(0, 1).toLowerCase() + clazz.name.substring(1);
      buffer.writeln('await ${propertyName}.initialized();');
    }

    //end initialize
    buffer.writeln('}');

    // end class
    buffer.writeln('}');
  }
}

class ServiceFactoryClassDeclGen {
  static void process(Generator gen, ApiLibrary apiLib, String baseClassName,
      StringBuffer buffer) {
    buffer.write('class ServiceFactory extends ServiceFactoryBase ');
    buffer.writeln('{');
    buffer.writeln('}');
  }
}

class AbstractServiceFactoryClassDeclGen {
  static void process(Generator gen, ApiLibrary apiLib, String baseClassName,
      StringBuffer buffer) {
    buffer.writeln("import 'dart:async';");
    buffer.writeln("import 'sci_model.dart';");
    buffer.writeln("import 'package:sci_api_gen/sci_service.dart' as api;");

    buffer.write('abstract class ServiceFactory ');
    buffer.writeln('{');

    buffer.writeln('static ServiceFactory CURRENT; ');

    buffer.writeln('factory ServiceFactory()=> CURRENT; ');

    for (var clazz in apiLib.classes.values) {
      var propertyName =
          clazz.name.substring(0, 1).toLowerCase() + clazz.name.substring(1);
      buffer.writeln('${clazz.name} get ${propertyName};');
    }

    buffer.writeln('}');

    for (var clazz in apiLib.classes.values) {
      ApiPersistentService metadataService =
          clazz.metadata.firstWhere((each) => each is ApiPersistentService);

      buffer.write(
          'abstract class ${clazz.name} implements api.Service<${metadataService.className}> ');
      buffer.writeln('{');

      clazz.methods.where((method) => !method.isConstructor).forEach((method) {
        buffer
          ..write(method.returnApiType.returnRange)
          ..write(' ')
          ..write(method.name)
          ..write('(')
          ..write(method.arguments
              .map((arg) => arg.range + ' ' + arg.name)
              .join(','))
          ..write(', {api.AclContext aclContext});');
      });

      List<CouchdbView> list =
          clazz.metadata.where((each) => each is CouchdbView).toList().cast();

      list.where((each) => each is ApiView).forEach((each) {
        ApiView metadataView = each;
        ApiPersistentService meta =
            clazz.metadata.firstWhere((each) => each is ApiPersistentService);

        if (metadataView.startKeys != null) {
          buffer
            ..writeln(
                'Future<List<${meta.className}>> ${metadataView.name}({startKey,endKey, int limit:200, int skip:0, bool descending: true, bool useFactory:false, api.AclContext aclContext});');
        } else if (metadataView.keys != null) {
          buffer
            ..writeln(
                'Future<List<${meta.className}>> ${metadataView.name}({List keys, bool useFactory:false, api.AclContext aclContext});');
        }
      });

      buffer.writeln('}');
    }
  }
}
