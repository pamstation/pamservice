part of sci.api.service.gen;

class ServiceFactoryPropertyGen {
  static void process(Generator gen, ApiClass clazz, StringBuffer buffer) {
    buffer..writeln('ServiceFactoryBase factory;')..writeln('');
  }
}

class UriServiceGen {
  static void process(Generator gen, ApiClass clazz, StringBuffer buffer) {
    ApiPersistentService meta =
        clazz.metadata.firstWhere((each) => each is ApiPersistentService);

    buffer
      ..writeln('Uri get uri => Uri.parse("${meta.uri}");')
      ..writeln('String get serviceName => "${meta.className}";')
      ..writeln('String get dbName => "${meta.dbName}";')
      ..writeln('');
  }
}

class JsonGen {
  static void process(Generator gen, ApiClass clazz, StringBuffer buffer) {
    ApiPersistentService meta =
        clazz.metadata.firstWhere((each) => each is ApiPersistentService);
    buffer
      ..writeln('Map toJson(${meta.className} object) => object.toJson();')
      ..writeln(
          '${meta.className} fromJson(Map m,{bool useFactory:true}) {'
              ' if (useFactory) return ${meta.className}Base.fromJson(m); return new ${meta.className}.json(m);}')
      ..writeln('');
  }
}

class CouchViewsGen {
  static void process(Generator gen, ApiClass clazz, ApiClass objectClazz,
      StringBuffer buffer) {
    List<CouchdbView> list = List.castFrom<dynamic, CouchdbView>(
        clazz.metadata.where((each) => each is CouchdbView).toList());
    ;

    list.forEach((each) {
      var map;
      var reduce;
      if (each is ApiView) {
        // view must not reduce
        map = getMap(clazz, objectClazz, each);
        reduce = '';
      } else {
        map = each.map;
        reduce = each.reduce;
      }

      buffer
        ..writeln(
            "Map<String,String> get ${each.name}_view => {'name': '${each.name}', 'map': '''${map}''' , 'reduce': '''${reduce}''' };")
        ..writeln('');
    });

    buffer
      ..write('List<DesignView> get views => [')
      ..write(list
          .map((each) => 'new DesignView.fromJson(this.${each.name}_view)')
          .join(','))
      ..writeln('];');

    buffer.writeln('');

    list.where((each) => each is ApiView).forEach((each) {
      processViewMethod(clazz, objectClazz, each, buffer);
    });
  }

  static void processViewMethod(ApiClass clazz, ApiClass objectClazz,
      ApiView metadataView, StringBuffer buffer) {
    ApiPersistentService meta =
        clazz.metadata.firstWhere((each) => each is ApiPersistentService);

    if (metadataView.startKeys != null) {
      buffer
        ..writeln(
            'Future<List<${meta.className}>> ${metadataView.name}({startKey,endKey, int limit:200, int skip:0, bool descending: true, bool useFactory:false, service.AclContext aclContext}){')
        ..writeln(
            'return findStartKeys("${metadataView.name}",startKey:startKey,endKey:endKey,limit:limit,skip:skip,descending:descending,useFactory:useFactory,aclContext:aclContext);')
        ..writeln('}');
    } else if (metadataView.keys != null) {
      buffer
        ..writeln(
            'Future<List<${meta.className}>> ${metadataView.name}({List keys, bool useFactory:false, service.AclContext aclContext}){')
        ..writeln(
            'return findKeys("${metadataView.name}",keys:keys,useFactory:useFactory,aclContext:aclContext);')
        ..writeln('}');
    }
  }

  static String getMap(
      ApiClass clazz, ApiClass objectClazz, ApiView metadataView) {
    var map;

    if (metadataView.map != null) {
      map = metadataView.map;
    } else {
      var clazzList = [];
      if (metadataView.kinds.contains(objectClazz.name)) {
        clazzList.add(objectClazz);
      }

      clazzList
        ..addAll(objectClazz.allSubClasses
            .where((each) => metadataView.kinds.contains(each.name)));

      Set<ApiClass> kinds = clazzList.fold(
          new Set<ApiClass>(),
          (Set<ApiClass> kindsList, kindClazz) => kindsList
            ..add(kindClazz)
            ..addAll(kindClazz.allSubClasses));

      var sb = new StringBuffer();

      sb
        ..writeln('function(doc){')
        ..write('if(!doc.isDeleted && (')
        ..write(kinds.map((each) => "doc.kind ==='${each.name}'").join('||'))
        ..writeln(')){');

      if (metadataView.getKeys().length > 1) {
        sb
          ..write('emit([')
          ..write(metadataView.getKeys().map((k) => 'doc.${k}').join(','))
          ..writeln('], 1);');
      } else {
        sb
          ..write('emit(')
          ..write(metadataView.getKeys().map((k) => 'doc.${k}').join(','))
          ..writeln(', 1);');
      }

      sb..write('}')..write('}');

      map = sb.toString();
    }

    return map;
  }
}

class MethodServiceGen {
  static void process(Generator gen, ApiClass clazz, StringBuffer buffer) {
    clazz.methods
        .where((each) => !each.isConstructor)
        .forEach((each) => processMethod(gen, each, buffer));
  }

  static void processMethod(
      Generator gen, ApiMethod method, StringBuffer buffer) {
    buffer
      ..write(method.returnApiType.returnRange)
      ..write(' ')
      ..write(method.name)
      ..write('(')
      ..write(
          method.arguments.map((arg) => arg.range + ' ' + arg.name).join(','))
      ..writeln(', {service.AclContext aclContext}){throw "not impl";}')
      ..writeln();
  }
}
