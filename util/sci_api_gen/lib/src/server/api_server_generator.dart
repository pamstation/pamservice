library sci.api.shelf.gen;

import 'dart:io';
import 'dart:convert';
import 'package:path/path.dart' as path;

import '../../api_lib.dart';
import '../../api_gen.dart';

part './gen/server_gen.dart';

class ServerGenerator extends Generator {
  ApiLibrary modelApiLib;
  String modelLibraryName;

  ServerGenerator(
      ApiLibrary apiLib,
      this.modelApiLib,
      String directory,
      String libraryName,
      String baseClassName,
      this.modelLibraryName,
      List<Map<String, String>> importLibs,
      List<Map<String, String>> exportLibs)
      : super(apiLib, directory, libraryName, baseClassName, importLibs,
            exportLibs) {
    classFolder = 'src/server/impl';
    baseFolder = 'src/server/base';
  }

  run() async {
    process();
    await Process
        .run('dartfmt', ['-w', directory],
            workingDirectory: Directory.current.path)
        .then((result) {
      stdout.write(result.stdout);
      stderr.write(result.stderr);
    });
  }

  void process() {
    new Directory(path.join(directory, classFolder))
        .createSync(recursive: true);
    new Directory(path.join(directory, baseFolder)).createSync(recursive: true);

    generateBaseLibrary(false);
    generateLibrary();

//    apiLib.classes.values.forEach(generateBase);
//    apiLib.classes.values.forEach(generateClass);

    generateBaseRequestHandlers();
    generateRequestHandlers();
  }

  void generateBaseRequestHandlers() {
    var filename = path.join(directory, baseFolder, 'server.dart');
    var file = new File(filename);
    var buffer = new StringBuffer();

    PartOfBaseGen.process(this, libraryName, buffer);

    ServerGen.process(this, apiLib, buffer);

    file.writeAsStringSync(buffer.toString());

//    for (var clazz in apiLib.classes.values) {
//      var buffer = new StringBuffer();
//      PartOfBaseGen.process(this, libraryName, buffer);
//      RequestHandlerGen.process(this, apiLib, clazz, buffer);
//      var filename =
//          path.join(directory, baseFolder, '${filenameForClass(clazz)}');
//      var file = new File(filename);
//
//      file.writeAsStringSync(buffer.toString());
//    }
  }

  void generateRequestHandlers() {
//    for (var clazz in apiLib.classes.values) {
//      var buffer = new StringBuffer();
//      RequestHandlerGen.process(this, apiLib, clazz, buffer);
//      var filename = path.join(directory, classFolder, 'server.dart');
//      var file = new File(filename);
//      if (file.existsSync()) return;
//      file.writeAsStringSync(buffer.toString());
//    }
  }

  void generateBaseLibrary(bool isModel) {
    var filename = path.join(directory, '${libraryName}_base.dart');
    var file = new File(filename);
    var buffer = new StringBuffer();
    buffer.writeln('library ${libraryName}_base;');
    buffer.writeln("import 'dart:io' as io;");
    buffer.writeln("import 'dart:async';");
    buffer.writeln("import 'dart:convert';");
    buffer.writeln("import 'package:mime/mime.dart';");

    buffer.writeln(
        "import 'package:sci_api_gen/sci_server_base.dart';");

    buffer.writeln(
        "import 'package:http_client/src/channel/multiplex_channel.dart';");
    buffer.writeln(
        "import 'package:web_socket_channel/web_socket_channel.dart';");

    buffer.writeln("import 'package:shelf/shelf.dart' as shelf;");
    buffer.writeln("import 'package:shelf_router/shelf_router.dart' as route;");
    buffer.writeln("import 'package:sci_api_gen/sci_service.dart' as service;");
    buffer.writeln("import 'package:sci_api_gen/sci_shelf_base.dart';");
    buffer.writeln("import 'sci_shelf.dart';");
    buffer.writeln("import 'sci_service.dart';");
    buffer.writeln("import 'sci_model.dart';");
    buffer.writeln("import 'sci_model_base.dart';");

    for (var clazz in apiLib.classes.values) {
      buffer.writeln(
          "part '${path.join(baseFolder, '${filenameForClass(clazz)}')}';");
    }

    buffer.writeln("part '${path.join(baseFolder, 'server.dart')}';");

    file.writeAsStringSync(buffer.toString());
  }

  void generateLibrary() {
    var filename = path.join(directory, '${libraryName}.dart');
    var file = new File(filename);
    var buffer = new StringBuffer();

    buffer.writeln('library ${libraryName};');

    buffer.writeln("import 'dart:io' as io;");
    buffer.writeln("import 'dart:async';");
    buffer.writeln("import 'dart:convert';");
    buffer.writeln("import 'package:mime/mime.dart';");

    buffer.writeln("import 'package:shelf/shelf.dart' as shelf;");
    buffer.writeln("import 'package:shelf_router/shelf_router.dart' as route;");
    buffer.writeln("import 'package:sci_api_gen/sci_service.dart' as service;");
    buffer.writeln("import 'package:sci_api_gen/sci_shelf_base.dart';");
    buffer.writeln("import 'sci_shelf.dart';");
    buffer.writeln("import 'sci_service.dart';");
    buffer.writeln("import 'sci_model.dart';");
    buffer.writeln("import 'sci_model_base.dart';");
    buffer.writeln("import 'sci_shelf_base.dart';");

    buffer.writeln("part '${path.join(classFolder, 'server.dart')}';");

    file.writeAsStringSync(buffer.toString());
  }

  void generateClass(ApiClass clazz) {
    var filename =
        path.join(directory, classFolder, '${filenameForClass(clazz)}');
    var file = new File(filename);
    if (file.existsSync()) return;
    var buffer = new StringBuffer();
    PartOfGen.process(this, libraryName, buffer);
    ClassDeclGen.process(this, clazz, baseClassName, buffer);

    EndClassGen.process(this, clazz, buffer);
    file.writeAsStringSync(buffer.toString());
  }
}
