part of sci.api.shelf.gen;

class RequestHandlerGen {
  static void process(Generator gen, ApiLibrary apiLib, ApiClass serviceClazz,
      StringBuffer buffer) {
    buffer
        .writeln('class ${serviceClazz.name}Handler extends RequestHandler {');

    buffer.writeln('@override');
    buffer.writeln('void close() {}');

    buffer.writeln('@override');
    buffer.writeln(
        'Future<ChannelServerResponse> request(ChannelServerRequest request) async {');
    buffer.writeln('}');

    buffer.writeln('}');
  }
}

class ServerGen {
  static void process(Generator gen, ApiLibrary apiLib, StringBuffer buffer) {
    buffer.writeln('class Server {');

    buffer.writeln('ServiceFactory serviceFactory;');
    //cstr
    buffer.writeln('Server(){');
    buffer.writeln('serviceFactory = new ServiceFactory();');
    //end ctsr
    buffer.writeln('}');

    buffer.writeln('handleWebSocket(WebSocketChannel webSocketChannel){');

    buffer.writeln('var channelServer = new ChannelServer(webSocketChannel);');
    buffer.writeln('var serv;');
    buffer.writeln('var aclHolder = new AclHolder();');

    for (var serviceClass in apiLib.classes.values) {
      var propertyName = serviceClass.name.substring(0, 1).toLowerCase() +
          serviceClass.name.substring(1);

      buffer.writeln('serv = serviceFactory.${propertyName};');

      List<ApiUriResource> uriResources = serviceClass.metadata
          .where((each) => each is ApiUriResource)
          .toList();

      for (var uriResource in uriResources) {
        if (uriResource is ApiPersistentService) {
          buffer.writeln(
              'channelServer.registerEndPoint("create_${uriResource.uri}", new CreatePersistentServiceHandler()..service = serv..aclHolder=aclHolder);');
          buffer.writeln(
              'channelServer.registerEndPoint("get_${uriResource.uri}", new GetPersistentServiceHandler()..service = serv..aclHolder=aclHolder);');
          buffer.writeln(
              'channelServer.registerEndPoint("list_${uriResource.uri}", new ListPersistentServiceHandler()..service = serv..aclHolder=aclHolder);');
          buffer.writeln(
              'channelServer.registerEndPoint("update_${uriResource.uri}", new UpdatePersistentServiceHandler()..service = serv..aclHolder=aclHolder);');
          buffer.writeln(
              'channelServer.registerEndPoint("delete_${uriResource.uri}", new DeletePersistentServiceHandler()..service = serv..aclHolder=aclHolder);');
          buffer.writeln(
              'channelServer.registerEndPoint("patch_${uriResource.uri}", new PatchPersistentServiceHandler()..service = serv..aclHolder=aclHolder);');
        } else if (uriResource is ApiView) {
          if (uriResource.startKeys != null) {
            buffer.writeln(
                'channelServer.registerEndPoint("create_${uriResource.uri}", new ViewServiceHandler()..service = serv..aclHolder=aclHolder..func=serv.${uriResource.name});');
          } else {
            buffer.writeln(
                'channelServer.registerEndPoint("create_${uriResource.uri}", new View2ServiceHandler()..service = serv..aclHolder=aclHolder..func=serv.${uriResource.name});');
          }
        }
      }
    }

    buffer.writeln('}');

    buffer.writeln('}');
  }
}

class ShelfBaseClassDeclGen {
  static void process(Generator gen, ApiLibrary apiLib, String baseClassName,
      StringBuffer buffer) {
    buffer.write('class ShelfBase extends ShelfService ');

    buffer.writeln('{');

    buffer.writeln('ServiceFactory serviceFactory;');
    //cstr
    buffer.writeln('ShelfBase(){');
    buffer.writeln('serviceFactory = new ServiceFactory();');
    //end ctsr
    buffer.writeln('}');

    // shelf handler
    buffer.writeln('@override');
    buffer.writeln(
        'Future<shelf.Response> process(shelf.Request request, service.AclContext aclContext) async {');

    buffer.writeln('var path = request.requestedUri.path;');
    buffer.writeln('var serv;');

    for (var clazz in apiLib.classes.values) {
      buffer.writeln('// ====================== ${clazz.name};');

      var propertyName =
          clazz.name.substring(0, 1).toLowerCase() + clazz.name.substring(1);

      buffer.writeln('serv = serviceFactory.${propertyName};');

      List<ApiUriResource> uriResources =
          clazz.metadata.where((each) => each is ApiUriResource).toList();

      for (var uriResource in uriResources) {
        if (uriResource is ApiPersistentService) {
          processService(clazz, uriResource, buffer);
        } else if (uriResource is ApiView) {
          processView(clazz, uriResource, buffer);
        }
      }

      ApiPersistentService meta =
          clazz.metadata.firstWhere((each) => each is ApiPersistentService);
      clazz.methods
          .where((each) => !each.isConstructor)
          .forEach((each) => processMethod(gen, meta, each, buffer));
    }

    buffer.writeln(
        'return new shelf.Response(500, body: json.encode(new service.ServiceError(500,"","bad request").toJson()));');
    // end shelf handler
    buffer.writeln('}');

    // end class
    buffer.writeln('}');
  }

  static void processSimpleMethod(Generator gen, ApiPersistentService meta,
      ApiMethod method, StringBuffer buffer) {
    buffer.writeln('var params = json.decode(await request.readAsString());');

    var args = [];

    for (var arg in method.arguments) {
      switch (arg.rangeType) {
        case 'num':
        case 'int':
        case 'double':
        case 'bool':
        case 'String':
          args.add('params["${arg.name}"]');
          break;
        case 'List':
          {
            if (arg.rangeTypes.isEmpty) {
              args.add('params["${arg.name}"]');
            } else {
              var rt = arg.rangeTypes.first;
              switch (rt) {
                case 'num':
                case 'int':
                case 'double':
                case 'bool':
                case 'String':
                  args.add('params["${arg.name}"]');
                  break;
                default:
                  args.add(
                      '(params["${arg.name}"] as List).map((m)=> m == null ? null : new ${rt}Base.fromJson(m)).toList()');
              }
            }

            break;
          }

        default:
          args.add('params["${arg.name}"] == null ? null : new ${arg.rangeType}Base.fromJson(params["${arg.name}"])');
      }
    }

    buffer
      ..writeln('result = await serv.${method.name}(')
      ..write(args.join(','))
      ..writeln(', aclContext: aclContext);');
  }

  static void processMultipartMethod(Generator gen, ApiPersistentService meta,
      ApiMethod method, StringBuffer buffer) {
    buffer
      ..writeln(
          'if (!request.headers.containsKey(io.HttpHeaders.CONTENT_TYPE)) throw "content type is required";')
      ..writeln(
          'var contentType = io.ContentType.parse(request.headers[io.HttpHeaders.CONTENT_TYPE]);')
      ..writeln(
          'if (contentType.primaryType != "multipart") throw "multipart is required";')
      ..writeln('var boundary = contentType.parameters["boundary"];')
      ..writeln('if (boundary == null) throw "boundary is required";')
      ..writeln(
          'List<MimeMultipart> parts = await request.read().transform(new MimeMultipartTransformer(boundary)).toList();');

    var args = [];

    for (var i = 0; i < method.arguments.length; i++) {
      var arg = method.arguments[i];
      switch (arg.rangeType) {
        case 'num':
        case 'int':
        case 'double':
        case 'bool':
        case 'String':
          args.add(
              '((await json.fuse(utf8).decoder.bind( parts[${i}]).toList()).first as List).first');
          break;
        case 'Stream':
          args.add('parts[${i}]');
          break;
        case 'List':
          {
            if (arg.rangeTypes.isEmpty) {
              args.add('parts[${i}]');
            } else {
              var rt = arg.rangeTypes.first;
              switch (rt) {
                case 'num':
                case 'int':
                case 'double':
                case 'bool':
                case 'String':
                  args.add(
                      '((await json.fuse(utf8).decoder.bind( parts[${i}]).toList()).first as List).first');
                  break;
                default:
                  args.add(
                      '(((await json.fuse(utf8).decoder.bind( parts[${i}]).toList()).first as List).first as List).map((m)=> new ${rt}Base.fromJson(m)).toList()');
              }
            }

            break;
          }

        default:
          args.add(
              'new ${arg.rangeType}Base.fromJson(((await json.fuse(utf8).decoder.bind( parts[${i}]).toList()).first as List).first)');
      }
    }

    buffer
      ..writeln('result = await serv.${method.name}(')
      ..write(args.join(','))
      ..writeln(', aclContext: aclContext);');
  }

  static void processMethod(Generator gen, ApiPersistentService meta,
      ApiMethod method, StringBuffer buffer) {
    buffer
      ..writeln(
          'if (request.method == "POST" && path.endsWith("${meta.uri}" + "/" + "${method.name}")){')
      ..writeln('var result;');

    if (method.arguments.any((arg) => arg.rangeType == 'Stream')) {
      processMultipartMethod(gen, meta, method, buffer);
    } else {
      processSimpleMethod(gen, meta, method, buffer);
    }

    switch (method.returnType) {
      case 'Future':
        {
          if (!method.returnTypes.isEmpty) {
            var rt = method.returnTypes.first;
            switch (rt) {
              case 'num':
              case 'int':
              case 'double':
              case 'bool':
              case 'String':
                buffer.writeln(
                    'return new shelf.Response(200, body:json.encode([result]));');
                break;
              case 'dynamic':
                buffer.writeln('return new shelf.Response(200);');
                break;
              default:
                buffer.writeln(
                    'return new shelf.Response(200, body:json.encode(result?.toJson()));');
            }
          }
          break;
        }
      default:
        throw 'not supported';
    }

    buffer.writeln('}');
  }

  static void processService(
      ApiClass clazz, ApiPersistentService uriResource, StringBuffer buffer) {
    buffer.writeln(
        'if ( path.endsWith("${uriResource.uri}") || path.endsWith("${uriResource.uri}/list") ) {');

    buffer.writeln('return processService(request, serv, aclContext); ');

    //end if
    buffer.writeln('}');
  }

  static void processView(
      ApiClass clazz, ApiView uriResource, StringBuffer buffer) {
    buffer.writeln('if (path.endsWith("${uriResource.uri}")) {');
    buffer.writeln('var str = await request.readAsString();');
    buffer.writeln('''
    var useFactory =
    request.requestedUri.queryParameters["useFactory"] == 'true'
        ? true
        : false;''');

    if (uriResource.startKeys != null) {
      buffer.writeln('Map  params = json.decode(str);');

      buffer
        ..write('var list = await serv.${uriResource.name}(')
        ..write('startKey:params["startKey"],')
        ..write('endKey:params["endKey"],')
        ..write('limit:params["limit"],')
        ..write('skip:params["skip"],')
        ..write('descending:params["descending"],')
        ..write('useFactory:useFactory,')
        ..writeln('aclContext:aclContext);');
    } else {
      buffer.writeln('var  keys = json.decode(str);');
      buffer
        ..write('var list = await serv.${uriResource.name}(')
        ..write('keys:keys,')
        ..write('useFactory:useFactory,')
        ..writeln('aclContext:aclContext);');
    }

    buffer.writeln(
        'return new shelf.Response(200, body: json.encode(list.map((each)=>each?.toJson()).toList()));');

    buffer.writeln('}');
  }
}

class ShelfClassDeclGen {
  static void process(Generator gen, ApiLibrary apiLib, String baseClassName,
      StringBuffer buffer) {
    buffer.write('class Shelf extends ShelfBase ');
    buffer.writeln('{');
    buffer.writeln('}');
  }
}
