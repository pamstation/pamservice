part of sci.service;

abstract class AclContextFactory {
  static AclContextFactory _CURRENT;
  static setCurrent(AclContextFactory current) {
    _CURRENT = current;
  }

  factory AclContextFactory() {
    if (_CURRENT == null) _CURRENT = new AclContextFactoryImpl();
    return _CURRENT;
  }
//  AclContext aclContextFromJson(Map m);
  Future<AclContext> fromUsername(String username);
//  AclContext fromWorkflowId(String workflowId);
  AclContext root();
  Future<AclContext> fromAuthorization(String authorization);
}

abstract class AccessManager {
  Future<bool> canAccessObjects(
      String accessType, AclContext context, List objects);
  Future<bool> canAccess(String accessType, AclContext context, Acl acl);
}

//class AccessManagerImpl implements AccessManager {
//  Future<bool> canAccessObjects(
//      String accessType, AclContext context, List objects) async {
//    return true;
//  }
//
//  Future<bool> canAccess(String accessType, AclContext context, Acl acl) async {
//    return true;
//  }
//}

abstract class AclContext {
  static String ACL_CONTEXT_HEADER = "x-sci-acl-context";
  String get username;
  set username(String u);
  List<String> get roles;
  set roles(List<String> l);
  Acl get teamAcl;
  set teamAcl(Acl a);
  bool get isAdmin;
  Map toJson();
  Map toJsonWithAcl();
}

abstract class UserDocAclContext extends AclContext {
  String get resourceId;
  String get resourceType;
}

abstract class Acl {
  static const ROOT = Principal.ROOT;
  static const GUEST = Principal.GUEST;

  String get type;
  String get owner;
  String get resourceId;
  String get resourceType;
  List<Ace> get aces;
  bool get isPrivate;
  bool get isPublicWrite;
  bool get isPublicRead;

  bool canAccess(String accessType, String username);

  bool canAccessRead(String username);
  bool canAccessWrite(String username);
  bool canAccessDelete(String username);
  bool canAccessAdmin(String username);

  Ace findFirstAce(String principalId);

  Map toJson();
}

abstract class Ace {
  List<Principal> get principals;
  List<Privilege> get privileges;
  Map toJson();
}

abstract class Principal {
  static const ROOT = "admin";
  static const GUEST = "guest";

  static const EVERYBODY = "everybody";
  static const USER = "user";
  static const TEAM = "team";

  String get type;
  String get id;
  Map toJson();
}

abstract class Privilege {
  static const READWRITE = "readwrite";
  static const READ = "read";
  static const ADMIN = "admin";
  static const DELETE = "delete";
  String get type;
  Map toJson();

  bool get canRead;
  bool get canWrite;
  bool get canDelete;
  bool get canAdmin;

  bool hasAccessType(String type);
}
