part of sci.service;

class AclContextFactoryImpl implements AclContextFactory {


//  @override
//  AclContext aclContextFromJson(Map m) {
//    var type = m["type"];
//    if (type == UserDocAclContextImpl.TYPE) {
//      return new UserDocAclContextImpl._fromJson(m);
//    } else if (type == WorkflowAclContextImpl.TYPE) {
//      return new WorkflowAclContextImpl._fromJson(m);
//    } else if (type == EmbededWorkflowAclContext.TYPE) {
//      return new EmbededWorkflowAclContext._fromJson(m);
//    } else if (type == AclContextImpl.TYPE) {
//      return new AclContextImpl.fromJson(m);
//    } else {
//      throw "$this unknown aclContext type $type";
//    }
//  }

  @override
  AclContext root() {
    return new AclContextImpl(Acl.ROOT, null, null);
  }

  @override
  Future<AclContext> fromUsername(String username, {List<String> roles, Acl teamAcl}) async {
    return new AclContextImpl(username, roles, teamAcl);
  }
//
//  @override
//  AclContext fromWorkflowId(String workflowId) {
//    return new WorkflowAclContextImpl.fromWorkflowId(workflowId);
//  }

  @override
  Future<AclContext> fromAuthorization(String authorization) async {
    return new AclTokenContextImpl(authorization);
  }
}

class AclTokenContextImpl implements AclContext {
  String authorization;

  AclTokenContextImpl(this.authorization);

  @override
  String get username {}

  @override
  Map toJsonWithAcl() {}

  @override
  Map toJson() {}

  @override
  bool get isAdmin {}

  @override
  set teamAcl(Acl a) {}

  @override
  Acl get teamAcl {}

  @override
  set roles(List<String> l) {}

  @override
  List<String> get roles {}

  @override
  set username(String u) {}
}

class AclContextImpl implements AclContext {
  static const String TYPE = "AclContext";
  AclContextImpl(this.username, this.roles, this.teamAcl) {
    if (roles == null) roles = [];
    if (teamAcl == null) teamAcl = new AclImpl.memberShip();
  }

  AclContextImpl.fromJson(Map m) {
    username = m["username"];
    roles = m["roles"] == null ? [] : m["roles"];
    if (m["teamAcl"] == null) {
      teamAcl = new AclImpl.memberShip();
    } else {
      teamAcl = new AclImpl.fromJson(m["teamAcl"]);
    }
  }

  String username;
  List<String> roles;
  Acl teamAcl;

  bool get isAdmin {
    if (this.username == Acl.ROOT) return true;
    if (this.roles.contains(Acl.ROOT)) return true;
    return false;
  }

  String get type => TYPE;
  @override
  Map toJson() {
    return {"username": username, "type": type};
  }

  @override
  Map toJsonWithAcl() {
    var m = toJson();
    m['roles'] = roles;
    m['teamAcl'] = teamAcl.toJson();
    return m;
  }
}

class UserDocAclContextImpl implements UserDocAclContext {
  static const String TYPE = "UserDocAclContext";
  String resourceId;
  String resourceType;
  String username;
  List<String> roles = [];
  Acl teamAcl;

  factory UserDocAclContextImpl.fromJson(Map m) {
    var type = m["type"];
    if (type == UserDocAclContextImpl.TYPE) {
      return new UserDocAclContextImpl._fromJson(m);
    } else if (type == WorkflowAclContextImpl.TYPE) {
      return new WorkflowAclContextImpl._fromJson(m);
    } else if (type == EmbededWorkflowAclContext.TYPE) {
      return new EmbededWorkflowAclContext._fromJson(m);
    } else {
      throw 'UserDocAclContextImpl : unknown type $type';
    }
  }

  UserDocAclContextImpl._fromJson(Map m) {
    resourceId = m["resourceId"];
    resourceType = m["resourceType"];
    username = m["username"];
    if (m["teamAcl"] == null) {
      teamAcl = new AclImpl.memberShip();
    } else {
      teamAcl = new AclImpl.fromJson(m["teamAcl"]);
    }
  }

  UserDocAclContextImpl(this.resourceId, this.resourceType, this.username) {
    teamAcl = new AclImpl.memberShip();
  }

  bool get isAdmin {
    if (this.username == Acl.ROOT) return true;
    if (this.roles.contains(Acl.ROOT)) return true;
    return false;
  }

  String get type => TYPE;

  @override
  Map toJson() {
    return {
      "resourceId": resourceId,
      "resourceType": resourceType,
      "username": username,
      "type": type
    };
  }

  @override
  Map toJsonWithAcl() {
    var m = toJson();
    m['roles'] = roles;
    m['teamAcl'] = teamAcl.toJson();
    return m;
  }
}

class WorkflowAclContextImpl implements UserDocAclContextImpl {
  static const String TYPE = "workflow";
  String workflowId;
  List<String> resourceIds;
  String resourceType = AclImpl.WORKFLOW_RESOURCE_TYPE;
  String username;
  List<String> roles = [];
  Acl teamAcl;

  WorkflowAclContextImpl._fromJson(Map m) {
    workflowId = m["workflowId"];
    if (this.workflowId == null) throw new ArgumentError.notNull("workflowId");
    resourceIds = m["resourceIds"];
    resourceType = m["resourceType"];
    username = m["username"];
    if (m["teamAcl"] == null) {
      teamAcl = new AclImpl.memberShip();
    } else {
      teamAcl = new AclImpl.fromJson(m["teamAcl"]);
    }
  }

  WorkflowAclContextImpl.fromWorkflowId(this.workflowId) {
    if (this.workflowId == null) throw new ArgumentError.notNull("workflowId");
    teamAcl = new AclImpl.memberShip();
    username = Acl.ROOT;
  }
  WorkflowAclContextImpl(this.workflowId, this.resourceIds, this.username) {
    if (this.workflowId == null) throw new ArgumentError.notNull("workflowId");
    teamAcl = new AclImpl.memberShip();
  }
  bool get isAdmin {
    if (this.username == Acl.ROOT) return true;
    if (this.roles.contains(Acl.ROOT)) return true;
    return false;
  }

  String get type => TYPE;
  @override
  String get resourceId => workflowId;
  set resourceId(String id) {
    workflowId = id;
  }

  List<AclContext> get aclContexts {
    return resourceIds
        .map((id) => new EmbededWorkflowAclContext(
            workflowId, id, AclImpl.SCHEMA_RESOURCE_TYPE, this.username))
        .toList();
  }

  @override
  Map toJson() {
    return {
      "workflowId": workflowId,
      "resourceIds": resourceIds,
      "resourceType": resourceType,
      "username": username,
      "type": type,
    };
  }

  Map toJsonWithAcl() {
    var m = toJson();
    m['roles'] = roles;
    m['teamAcl'] = teamAcl.toJson();
    return m;
  }
}

class EmbededWorkflowAclContext extends UserDocAclContextImpl {
  static const String TYPE = "workflow_schema";
  String workflowId;
  EmbededWorkflowAclContext._fromJson(Map m) : super._fromJson(m) {
    workflowId = m["workflowId"];
  }

  EmbededWorkflowAclContext(
      this.workflowId, String resourceId, String resourceType, String username)
      : super(resourceId, resourceType, username);
  @override
  String get type => TYPE;
  @override
  Map toJson() {
    var map = super.toJson();
    map["workflowId"] = workflowId;
    return map;
  }
}

class AclImpl implements Acl {
  static const DEFAULT_ACL_TYPE = "default";
  static const GROUP_ACL_TYPE = "group";
  static const MEMBER_ACL_TYPE = "member";
  static const WORKFLOW_RESOURCE_TYPE = "workflow";
  static const SCHEMA_RESOURCE_TYPE = "schema";
  static const OPERATOR_RESOURCE_TYPE = "operator";
  static const PROJECT_RESOURCE_TYPE = "project";

  Map _data;

  factory AclImpl.fromJson(Map m) {
    var type = m["type"];
    if (type == DEFAULT_ACL_TYPE) {
      return new AclImpl.json(m);
    } else if (type == MEMBER_ACL_TYPE) {
      return new AclImpl.json(m);
    } else
      throw "unknown acl type $type";
  }

  AclImpl.json(this._data);

  factory AclImpl.workflow(String workflowId) {
    return new AclImpl(
        null, DEFAULT_ACL_TYPE, workflowId, WORKFLOW_RESOURCE_TYPE);
  }

  factory AclImpl.schema(String schemaId) {
    return new AclImpl(null, DEFAULT_ACL_TYPE, schemaId, SCHEMA_RESOURCE_TYPE);
  }

  factory AclImpl.operator(String operatorId) {
    return new AclImpl(
        null, DEFAULT_ACL_TYPE, operatorId, OPERATOR_RESOURCE_TYPE);
  }

  factory AclImpl.project(String ownerUsername) {
    return new AclImpl(
        ownerUsername, DEFAULT_ACL_TYPE, null, PROJECT_RESOURCE_TYPE);
  }

  factory AclImpl.memberShip([owner]) {
    return new AclImpl(owner, MEMBER_ACL_TYPE, null, null);
  }

  AclImpl(String owner, String type, String resourceId, String resourceType) {
    _data = {
      "owner": owner,
      "type": type,
      "resourceId": resourceId,
      "resourceType": resourceType,
      "aces": []
    };
  }

  @override
  List<Ace> get aces =>
      (_data["aces"] as List).map((m) => new AceImpl.fromJson(m)).toList();

  void addAce(AceImpl ace) {
    (_data["aces"] as List).add(ace.toJson());
  }

  @override
  Ace findFirstAce(String username) {
    return this.aces.firstWhere((ace) {
      if (ace.principals.isEmpty) return false;
      return ace.principals.first.id == username;
    }, orElse: () => null);
  }

  /*
  if privilege is null, the user is remove from the team
   */
  void setAce(Principal principal, Privilege priv) {
    var list = aces;
    AceImpl ace = list.firstWhere((ace) {
      if (ace.principals.isEmpty) return false;
      return ace.principals.first.id == principal.id;
    }, orElse: () => null);
    if (ace == null) {
      if (priv != null) {
        ace = new AceImpl()
          ..addPrincipal(principal)
          ..addPrivilege(priv);
        list.add(ace);
      }
    } else {
      if (priv != null) {
        ace
          ..removePrivileges()
          ..addPrivilege(priv);
      } else {
        list.remove(ace);
      }
    }
    _data["aces"] = list.map((ace) => ace.toJson()).toList();
  }

  void removeAce(AceImpl ace) {
    if (ace.principals.isEmpty) return;
    var username = ace.principals.first.id;
    _data["aces"] = aces
        .where((ac) {
          if (ac.principals.isEmpty) return true;
          return ac.principals.first.id != username;
        })
        .map((a) => a.toJson())
        .toList();
  }

  @override
  String get owner => _data["owner"];
  set owner(String o) {
    _data["owner"] = o;
  }

  @override
  String get resourceId => _data["resourceId"];

  @override
  String get resourceType => _data["resourceType"];

  @override
  Map toJson() => _data;

  @override
  String get type => _data["type"];

  @override
  bool get isPrivate => !this.aces.any((ace) => ace.principals.isNotEmpty);

  @override
  bool get isPublicRead {
    var flag = this
        .aces
        .any((ace) => ace.principals.any((p) => p.type == Principal.EVERYBODY));
    if (!flag) return false;
    return this
        .aces
        .any((ace) => ace.privileges.any((p) => p.type == Privilege.READ));
  }

  @override
  bool get isPublicWrite {
    var flag = this
        .aces
        .any((ace) => ace.principals.any((p) => p.type == Principal.EVERYBODY));
    if (!flag) return false;
    return this
        .aces
        .any((ace) => ace.privileges.any((p) => p.type == Privilege.READWRITE));
  }

  @override
  bool canAccessRead(String username) {
    if (username == null) return false;
    if (username == Acl.ROOT) return true;
    if (this.isPublicRead || this.isPublicWrite) return true;
    if (this.owner == username) return true;

    var ace = findFirstAce(username);

    if (ace == null) return false;
    var b = (ace as AceImpl).hasReadPrivilege;

    return b;
  }

  @override
  bool canAccessWrite(String username) {
    if (username == null) return false;
    if (username == Acl.ROOT) return true;
    if (this.isPublicWrite) return true;
    if (this.owner == username){
      return true;
    }
    var ace = findFirstAce(username);
    if (ace == null){
      return false;
    }
    return (ace as AceImpl).hasWritePrivilege;
  }

  @override
  bool canAccessDelete(String username) {
    if (username == null) return false;
    if (username == Acl.ROOT) return true;
    if (this.owner == username) return true;
    var ace = findFirstAce(username);
    if (ace == null) return false;
    return (ace as AceImpl).hasDeletePrivilege;
  }

  @override
  bool canAccessAdmin(String username) {
    if (username == null) return false;
    if (username == Acl.ROOT) return true;
    if (this.owner == username) return true;
    var ace = findFirstAce(username);
    if (ace == null) return false;
    return (ace as AceImpl).hasAdminPrivilege;
  }

  @override
  bool canAccess(String accessType, String username) {
    if (username == Acl.ROOT) return true;
    if (accessType == Privilege.READ) {
      return canAccessRead(username);
    } else if (accessType == Privilege.READWRITE) {
      return canAccessWrite(username);
    } else if (accessType == Privilege.DELETE) {
      return canAccessDelete(username);
    } else if (accessType == Privilege.ADMIN) {
      return canAccessAdmin(username);
    } else {
      throw new ServiceError(
          400, "acl.access.type.unknown", "acl.access.type.unknown");
    }
  }
}

class AceImpl implements Ace {
  Map _data;

  factory AceImpl.publicWrite() {
    var ace = new AceImpl();
    ace.addPrincipal(new PrincipalImpl.everybody());
    ace.addPrivilege(new PrivilegeImpl.readwrite());
    return ace;
  }

  factory AceImpl.publicRead() {
    var ace = new AceImpl();
    ace.addPrincipal(new PrincipalImpl.everybody());
    ace.addPrivilege(new PrivilegeImpl.read());
    return ace;
  }

  factory AceImpl.userReadWrite(String username) {
    var ace = new AceImpl();
    ace.addPrincipal(new PrincipalImpl.fromUsername(username));
    ace.addPrivilege(new PrivilegeImpl.readwrite());
    return ace;
  }

  factory AceImpl.userAdmin(String username) {
    var ace = new AceImpl();
    ace.addPrincipal(new PrincipalImpl.fromUsername(username));
    ace.addPrivilege(new PrivilegeImpl.admin());
    return ace;
  }

  AceImpl.fromJson(this._data);
  AceImpl() {
    _data = {"principals": [], "privileges": []};
  }

  @override
  List<Principal> get principals => (_data["principals"] as List)
      .map((m) => new PrincipalImpl.fromJson(m))
      .toList();

  void addPrincipal(PrincipalImpl principal) {
    (_data["principals"] as List).add(principal.toJson());
  }

  @override
  List<Privilege> get privileges => (_data["privileges"] as List)
      .map((m) => new PrivilegeImpl.fromJson(m))
      .toList();

  void addPrivilege(PrivilegeImpl privilege) {
    assert(privilege != null);
    (_data["privileges"] as List).add(privilege.toJson());
  }

  void removePrivileges() {
    (_data["privileges"] = []);
  }

  @override
  Map toJson() => _data;

  bool get hasReadPrivilege {
    return this.privileges.any((Privilege p) => p.canRead);
  }

  bool get hasWritePrivilege {
    return this.privileges.any((Privilege p) => p.canWrite);
  }

  bool get hasDeletePrivilege {
    return this.privileges.any((Privilege p) => p.canDelete);
  }

  bool get hasAdminPrivilege {
    return this.privileges.any((Privilege p) => p.canAdmin);
  }

  bool hasPrivilege(String type) {
    return this.privileges.any((Privilege p) => p.hasAccessType(type));
  }
}

class PrincipalImpl implements Principal {
  Map _data;

  PrincipalImpl.everybody() {
    _data = {"type": Principal.EVERYBODY};
  }
  PrincipalImpl.fromUsername(String username) {
    _data = {"id": username, "type": Principal.USER};
  }
  PrincipalImpl.fromTeamName(String teamName) {
    _data = {"id": teamName, "type": Principal.TEAM};
  }

  PrincipalImpl(String id, String type) {
    _data = {"id": id, "type": type};
  }

  PrincipalImpl.fromJson(this._data);

  @override
  String get id => _data["id"];

  @override
  Map toJson() => _data;

  @override
  String get type => _data["type"];
}

class PrivilegeImpl implements Privilege {
  Map _data;

  PrivilegeImpl.admin() {
    _data = {"type": Privilege.ADMIN};
  }

  PrivilegeImpl.readwrite() {
    _data = {"type": Privilege.READWRITE};
  }

  PrivilegeImpl.read() {
    _data = {"type": Privilege.READ};
  }

  PrivilegeImpl(String type) {
    _data = {"type": type};
  }

  PrivilegeImpl.fromJson(this._data);
  @override
  Map toJson() => _data;

  @override
  String get type => _data["type"];

  bool get canRead {
    return this.type == Privilege.READWRITE ||
        this.type == Privilege.READ ||
        this.type == Privilege.ADMIN;
  }

  bool get canWrite {
    return this.type == Privilege.ADMIN || this.type == Privilege.READWRITE;
  }

  bool get canDelete {
    return canWrite;
//    return this.type == Privilege.ADMIN ||
//        this.type == Privilege.READWRITEDELETE;
  }

  bool get canAdmin {
    return this.type == Privilege.ADMIN;
  }

  bool hasAccessType(String accessType) {
    if (accessType == Privilege.READ) return this.canRead;
    if (accessType == Privilege.READWRITE) return this.canWrite;
    if (accessType == Privilege.DELETE) return this.canDelete;
    if (accessType == Privilege.ADMIN) return this.canAdmin;
    return false;
  }
}
