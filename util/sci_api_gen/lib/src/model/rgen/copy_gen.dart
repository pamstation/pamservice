part of sci.api.model.gen;

class CopyGen {
  static void process(ClassGenerator gen, ApiClass clazz, StringBuffer buffer) {
    buffer.writeln('${clazz.name} copy() => new ${clazz.name}.json(this.toJson());');
  }
}
