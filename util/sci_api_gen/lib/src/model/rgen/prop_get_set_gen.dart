part of sci.api.model.gen;

class PropGetSetGen {
  static void process(ClassGenerator gen, ApiClass clazz, StringBuffer buffer) {
    clazz.dataProperties.forEach((p) {
      writePropertyGetterSetter(gen, clazz, buffer, p);
    });

    clazz.objectProperties.forEach((p) {
      writePropertyGetterSetter(gen, clazz, buffer, p);
    });

    buffer.writeln('');
  }

  static void writePropertyGetterSetter(
      ClassGenerator gen, ApiClass clazz, StringBuffer buffer, ApiProperty p) {
    var range = gen.getRange(p, clazz);

    buffer.writeln('${range} get ${p.name} => this._${p.name};');
    buffer.writeln('');

    if (!p.isFunctional) {
      return;
    }

    if (!p.hasParentProperty) {
      buffer.writeln('set ${p.name}(${range} o) {');

      buffer.writeln('assert(o == null || o is ${range});');

      if (!p.isData) {
//      buffer.writeln('if (base.Changed.LISTEN) this.unlisten(this._${p.name});');
        buffer.writeln('if (o != null) o.parent = this;');
      }

      buffer.writeln('var old = this._${p.name};');
      buffer.writeln('this._${p.name} = o;');

//    if (p is OwlObjectProperty) {
//      buffer.writeln('if (base.Changed.LISTEN) this.listen(this._${p.name});');
//    }

      buffer.writeln(
          'if (hasListener) this.sendChangeEvent(new base.PropertyChangedEvent(this, Vocabulary.${VocabularyClassGen.getVarPropertyName(p)}, old, this._${p.name}));');

      buffer.writeln('}');
      buffer.writeln('');
    }
  }
}

class PropGetSetByNameGen {
  static void process(ClassGenerator gen, ApiClass clazz, StringBuffer buffer) {
    var list = clazz.properties.where((p) => !p.hasParentProperty);

    if (list.isNotEmpty) {
      buffer.writeln('get(String name){');

      buffer.writeln('switch (name) {');
      list.forEach((p) {
        buffer.writeln(
            'case Vocabulary.${VocabularyClassGen.getVarPropertyName(p)} :');
        buffer.writeln('return this._${p.name};');
      });

      buffer.writeln('default : return super.get(name);');
      buffer.writeln('}');
      buffer.writeln('}');
      buffer.writeln('');
      buffer.writeln('');

      buffer.writeln('set(String name, Object value){');

      buffer.writeln('switch (name) {');
      clazz.dataProperties.where((p) => !p.hasParentProperty).forEach((p) {
        buffer.writeln(
            'case Vocabulary.${VocabularyClassGen.getVarPropertyName(p)} :');
        buffer.writeln('this._${p.name} = value;');
        buffer.writeln('return;');
      });
      clazz.objectProperties
          .where((p) => !p.hasParentProperty)
          .where((p) => p.isFunctional)
          .forEach((p) {
        buffer.writeln(
            'case Vocabulary.${VocabularyClassGen.getVarPropertyName(p)} :');
        buffer.writeln('this._${p.name} = value;');
        buffer.writeln('return;');
      });

      buffer.writeln('default : super.set(name, value);');
      buffer.writeln('}');
      buffer.writeln('}');
      buffer.writeln('');
      buffer.writeln('');
    }

//    buffer.writeln('get(String name){');
//
//    buffer.writeln('switch (name) {');
//    clazz.dataProperties.forEach((p) {
//      buffer.writeln(
//          'case Vocabulary.${VocabularyClassGen.getVarPropertyName(p)} :');
//      buffer.writeln('return this._${p.name};');
//    });
//    clazz.objectProperties.forEach((p) {
//      buffer.writeln(
//          'case Vocabulary.${VocabularyClassGen.getVarPropertyName(p)} :');
//      buffer.writeln('return this._${p.name};');
//    });
//
//    buffer.writeln(
//        'default : return super.get(name);');
//    buffer.writeln('}');
//    buffer.writeln('}');
//    buffer.writeln('');
//    buffer.writeln('');

//    buffer.writeln('set(String name, Object value){');
//
//    buffer.writeln('switch (name) {');
//    clazz.dataProperties.forEach((p) {
//      buffer.writeln(
//          'case Vocabulary.${VocabularyClassGen.getVarPropertyName(p)} :');
//      buffer.writeln('this._${p.name} = value;');
//      buffer.writeln('return;');
//    });
//    clazz.objectProperties.where((p) => p.isFunctional).forEach((p) {
//      buffer.writeln(
//          'case Vocabulary.${VocabularyClassGen.getVarPropertyName(p)} :');
//      buffer.writeln('this._${p.name} = value;');
//      buffer.writeln('return;');
//    });
//
//    buffer.writeln(
//        'default : super.set(name, value);');
//    buffer.writeln('}');
//    buffer.writeln('}');
//    buffer.writeln('');
//    buffer.writeln('');
  }
}
