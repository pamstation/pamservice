part of sci.api.model.gen;

class ToTsonGen {
  static void process(ClassGenerator gen, ApiClass clazz, StringBuffer buffer) {
//    buffer.writeln(', toTson = function() {return(self\$doTson(self))}');

    buffer.writeln(', toTson = function() {');
    buffer.writeln('m = super\$toTson()');

    writeJsonWriteKindProperty(buffer, clazz, 'm');

    var list = clazz.properties.where((p) => !p.hasParentProperty);

    list.forEach((p) {
      writeJsonWriteProperty(buffer, p, 'm');
    });

    buffer.writeln('return(m)');

    buffer.writeln('}');
  }

  static void writeJsonWriteKindProperty(buffer, ApiClass c, mapVar) {
    buffer.writeln("${mapVar}\$kind = tson.scalar('${c.name}')");
//    buffer.writeln(
//        'if (!is.null(self\$subKind) && self\$subKind != ${c.name}) ${mapVar}\$subKind = self\$subKind;');
//    buffer.writeln('else ${mapVar}.remove(Vocabulary.SUBKIND);');
  }

  static void writeJsonWriteProperty(buffer, ApiProperty p, mapVar) {
    final name = p.name;
    if (p.isFunctional) {
      if (p.isData) {
        if (p.range == 'dynamic'){
          buffer.writeln(
              '${mapVar}\$${p.name} = self\$${name}');
        } else {
          if (p.range == 'int'){
            buffer.writeln(
                '${mapVar}\$${p.name} = tson.int(self\$${name})');
          } else {
            buffer.writeln(
                '${mapVar}\$${p.name} = tson.scalar(self\$${name})');
          }

        }


      } else {
        buffer.write('if (!is.null(self\$${name}))');
        buffer.writeln(
            '${mapVar}\$${p.name} = self\$${name}\$toTson()');
      }
    } else {
      if (p.isData) {
        buffer.writeln(
            '${mapVar}\$${p.name} = lapply(self\$${name}, function(each) tson.scalar(each))'); // self\$${name}');
      } else {
        buffer.writeln(
            '${mapVar}\$${p.name} = lapply(self\$${name}, function(each) each\$toTson())');
      }
    }
  }
}
