part of sci.api.model.gen;

class RClassDocGen {
  static void process(Generator gen, ApiClass clazz, String baseClassName,
      StringBuffer buffer) {
    buffer..writeln("#' ${clazz.name}")..writeln("#'")..writeln("#' @export");
    buffer..write("#' @format \\code{\\link{R6Class}} object");
    if (clazz.superName != 'Object') {
      buffer..write(', super class \\code{\\link{${clazz.superName}}}');
    }
    if (clazz.allSubClasses.isNotEmpty) {
      buffer..write(', sub classes ');
      buffer
        ..write(clazz.allSubClasses.map((each) {
          return '\\code{\\link{${each.name}}}';
        }).join(', '));
    }

    buffer..writeln('.');

    clazz.parentClasses.forEach((p) {
      p.dataProperties.forEach((pp) {
        buffer
          ..writeln(
              "#' @field ${pp.name} ${pp.isFunctional ? '': 'list '}of type ${pp.range} inherited from super class \\code{\\link{${p.name}}}.");
      });
    });

    clazz.dataProperties.forEach((pp) {
      buffer
        ..writeln(
            "#' @field ${pp.name} ${pp.isFunctional ? '': 'list '}of type ${pp.range}.");
    });

    clazz.parentClasses.forEach((p) {
      p.objectProperties.forEach((pp) {
        buffer
          ..writeln(
              "#' @field ${pp.name} ${pp.isFunctional ? 'object ': 'list '}of class \\code{\\link{${pp.range}}} inherited from super class \\code{\\link{${p.name}}}.");
      });
    });

    clazz.objectProperties.forEach((pp) {
      buffer
        ..writeln(
            "#' @field ${pp.name} ${pp.isFunctional ? 'object ': 'list '}of class \\code{\\link{${pp.range}}}.");
    });
  }
}

class RClassDeclGen {
  static void process(Generator gen, ApiClass clazz, String baseClassName,
      StringBuffer buffer) {
    buffer.write(
        "${clazz.name} <- R6::R6Class('${clazz.name}', inherit = ${clazz.name}Base");
  }
}

class PublicGen {
  static void process(ClassGenerator gen, ApiClass clazz, StringBuffer buffer) {
    buffer.writeln(", public = list(");
  }
}

class EndPublicGen {
  static void process(ClassGenerator gen, ApiClass clazz, StringBuffer buffer) {
    buffer.writeln(")");
  }
}

class RBaseDeclGen {
  static void process(Generator gen, ApiClass clazz, String baseClassName,
      StringBuffer buffer) {
    var cname = '${clazz.name}';

//    buffer..writeln("#' ${cname}")..writeln("#'")..writeln("#' @export");
    buffer.writeln("${cname} <- R6::R6Class('${clazz.name}'");

    if (clazz.superName != 'Object') {
      buffer.writeln(", inherit = ${clazz.superName}");
    } else {
      buffer.writeln(", inherit = Base");
    }
  }
}

class REndClassGen {
  static void process(Generator gen, ApiClass clazz, StringBuffer buffer) {
    buffer.write(')');
  }
}

class ClassDefaultCtrGen {
  static void process(ClassGenerator gen, ApiClass clazz, StringBuffer buffer) {
    buffer.write('${clazz.name}(): super();');
    buffer.writeln('');
  }
}

class FactoryGen {
  static void process(ClassGenerator gen, StringBuffer buffer) {
    buffer
      ..writeln('createObjectFromJson = function(json){')
      ..writeln('if (is.null(json)) return (NULL)')
      ..writeln('kind = json\$kind');
    gen.apiLib.classes.values.forEach((each) {
      buffer.writeln(
          "if (kind == '${each.name}'){ return(${each.name}\$new(json=json))}");
    });
    buffer.writeln('stop("bad kind")');
    buffer.writeln('}');
  }
}

class PrintGen {
  static void process(ClassGenerator gen, ApiClass clazz, StringBuffer buffer) {
    var list = clazz.properties.where((p) => !p.hasParentProperty);

    buffer.writeln(', print = function(...){');
    buffer.writeln('cat(yaml::as.yaml(self\$toTson()))');
    buffer.writeln(' invisible(self)');
    buffer.writeln('}');
  }
}

class BaseDefaultCtrGen {
  static void process(ClassGenerator gen, ApiClass clazz, StringBuffer buffer) {
    var list = clazz.properties.where((p) => !p.hasParentProperty);
    if (list.isNotEmpty) buffer.writeln(',');
    buffer.writeln('initialize = function(json=NULL){');
//    buffer.writeln('super\$initialize(json=json)');
    buffer
        .write('if (!is.null(json)){self\$initJson(json)} else {self\$init()}');
    buffer.writeln('},');

    buffer.writeln('init = function(){');
    buffer.writeln('super\$init()');
    processDefault(gen, clazz, buffer);
    buffer.writeln('},');
    buffer.write('initJson = function(json){');
    buffer.writeln('super\$initJson(json)');
    processJson(gen, clazz, buffer);
    buffer.write('}');
  }

  static void processDefault(
      ClassGenerator gen, ApiClass clazz, StringBuffer buffer) {
    clazz.dataProperties.forEach((p) {
      if (p.isFunctional) {
        buffer
            .writeln('self\$${p.name} = ${gen.dataTypeDefaultValue(p.range)}');
      } else {
        buffer.writeln('self\$${p.name} = list()');
      }
    });

    clazz.objectProperties.forEach((p) {
      if (p.isFunctional) {
        buffer.writeln('self\$${p.name} = ${gen.getRange(p, clazz)}\$new()');
      } else {
        buffer.writeln('self\$${p.name} = list()');
      }
    });
  }

  static void processJson(
      ClassGenerator gen, ApiClass clazz, StringBuffer buffer) {
    clazz.dataProperties.forEach((p) {
      var range = gen.getRange(p, clazz);
      if (range == 'double') {
        buffer.writeln('self\$${p.name} = as.double(json\$${p.name})');
      } else {
        buffer.writeln('self\$${p.name} = json\$${p.name}');
      }
    });

    clazz.objectProperties.forEach((p) {
      if (p.isFunctional) {
        buffer
            .writeln('self\$${p.name} = createObjectFromJson(json\$${p.name})');
      } else {
        buffer.writeln(
            'self\$${p.name} = lapply(json\$${p.name}, createObjectFromJson)');
      }
    });
  }
}

class BaseJsonCtrGen {
  static void process(ClassGenerator gen, ApiClass clazz, StringBuffer buffer) {
    buffer.write('${clazz.name}Base.json(Map m) : super.json(m)');

    if (clazz.hasProperties) {
      buffer.writeln('{');
      buffer.writeln('this.noEventDo((){');
      buffer.writeln(
          'this.subKind = m[Vocabulary.SUBKIND] != null ? m[Vocabulary.SUBKIND] : (m[Vocabulary.KIND] != Vocabulary.${VocabularyClassGen.getVarClassName(clazz)} ? m[Vocabulary.KIND] : null);');
      clazz.dataProperties.forEach((p) {
        if (p.isFunctional) {
          var range = gen.getRange(p, clazz);
          if (range == 'double') {
            buffer.writeln(
                'this.${p.name} = m[Vocabulary.${VocabularyClassGen.getVarPropertyName(p)}].toDouble();');
          } else {
            buffer.writeln(
                'this.${p.name} = m[Vocabulary.${VocabularyClassGen.getVarPropertyName(p)}];');
          }
        } else {
          buffer.writeln(
              'this._${p.name} = new ${gen.getRange(p, clazz)}(m[Vocabulary.${VocabularyClassGen.getVarPropertyName(p)}]);');
          buffer.writeln('this._${p.name}.parent = this;');
        }
      });

      clazz.objectProperties.forEach((p) {
        if (p.isFunctional) {
          buffer.writeln(
              'if (m[Vocabulary.${VocabularyClassGen.getVarPropertyName(p)}] == null) this.${p.name} = new ${gen.getRange(p, clazz)}();');
          if (!p.hasParentProperty) {
            buffer.writeln(
                'else this.${p.name} = new ${p.range}Base.fromJson(m[Vocabulary.${VocabularyClassGen.getVarPropertyName(p)}]);');
          }
        } else {
          buffer.writeln(
              'this._${p.name} = new ${gen.getRange(p, clazz)}((m[Vocabulary.${VocabularyClassGen.getVarPropertyName(p)}] as List).map((each)=> new ${p.range}Base.fromJson(each)).toList());');
          buffer.writeln('this._${p.name}.parent = this;');
        }
//        if (p.range.directParentClass == clazz) {
//          buffer.writeln('if (this._${p.name} != null) this._${p.name}.parent = this;');
//        }
      });

      // end no event
      buffer.writeln('});');

      buffer.writeln('}');
    } else {
      buffer.write(';');
    }

    buffer.writeln('');
  }

  static void writeJsonReadProperty(buffer, ApiProperty p, mapVar) {
    final name = p.name;
    if (p.isFunctional) {
      if (p.isData) {
        buffer.writeln(
            'this._${name} = ${mapVar}[Vocabulary.${VocabularyClassGen.getVarPropertyName(p)}];');
      } else {
        buffer.writeln(
            'this.${name} = new ${p.range}.fromJson(${mapVar}[Vocabulary.${VocabularyClassGen.getVarPropertyName(p)}]);');
      }
    } else {
      if (p.isData) {
        buffer.writeln(
            'this._${name} = ${mapVar}[Vocabulary.${VocabularyClassGen.getVarPropertyName(p)}];');
      } else {
        buffer.writeln(
            'this._${name} = new base.ListChanged((${mapVar}[Vocabulary.${VocabularyClassGen.getVarPropertyName(p)}] as List).map((each)=> new ${p.range}.fromJson(each)).toList());');
      }
    }
  }
}

class ClassJsonCtrGen {
  static void process(ClassGenerator gen, ApiClass clazz, StringBuffer buffer) {
    buffer.write('${clazz.name}.json(Map m) : super.json(m);');

    buffer.writeln('');
  }

  static void writeJsonReadProperty(buffer, ApiProperty p, mapVar) {
    final name = p.name;
    if (p.isFunctional) {
      if (p.isData) {
        buffer.writeln(
            'this._${name} = ${mapVar}[Vocabulary.${VocabularyClassGen.getVarPropertyName(p)}];');
      } else {
        buffer.writeln(
            'this.${name} = new ${p.range}.fromJson(${mapVar}[Vocabulary.${VocabularyClassGen.getVarPropertyName(p)}]);');
      }
    } else {
      if (p.isData) {
        buffer.writeln(
            'this._${name} = ${mapVar}[Vocabulary.${VocabularyClassGen.getVarPropertyName(p)}];');
      } else {
        buffer.writeln(
            'this._${name} = new base.ListChanged((${mapVar}[Vocabulary.${VocabularyClassGen.getVarPropertyName(p)}] as List).map((each)=> new ${p.range}.fromJson(each)).toList());');
      }
    }
  }
}

class BaseFactoryJsonCtrGen {
  static void process(ClassGenerator gen, ApiClass clazz, StringBuffer buffer) {
    buffer.write('factory ${clazz.name}Base.fromJson(Map m) {');
    buffer.writeln('final kind = m[Vocabulary.KIND];');
    buffer.writeln('switch (kind) {');
    buffer.writeln(
        'case Vocabulary.${VocabularyClassGen.getVarClassName(clazz)} :');
    buffer.writeln('return new ${clazz.name}.json(m);');
    clazz.allSubClasses.forEach((c) {
      buffer.writeln(
          'case Vocabulary.${VocabularyClassGen.getVarClassName(c)} :');
      buffer.writeln('return new ${c.name}.json(m);');
    });
    buffer.writeln(
        'default : throw new ArgumentError.value(kind, "bad kind for class ${clazz.name} in fromJson constructor");');
    buffer.writeln('}');
    buffer.writeln('}');
    buffer.writeln('');
  }
}

class ClassFactoryJsonCtrGen {
  static void process(ClassGenerator gen, ApiClass clazz, StringBuffer buffer) {
    buffer.write('factory ${clazz.name}.fromJson(Map m) {');
    buffer.writeln('final kind = m[Vocabulary.KIND];');
    buffer.writeln('switch (kind) {');
    buffer.writeln(
        'case Vocabulary.${VocabularyClassGen.getVarClassName(clazz)} :');
    buffer.writeln('return new ${clazz.name}.json(m);');
    clazz.subClasses.forEach((c) {
      buffer.writeln(
          'case Vocabulary.${VocabularyClassGen.getVarClassName(c)} :');
      buffer.writeln('return new ${c.name}.json(m);');
    });
    buffer.writeln(
        'default : throw new ArgumentError.value(kind, "bad kind for class ${clazz.name} in fromJson constructor");');
    buffer.writeln('}');
    buffer.writeln('}');
    buffer.writeln('');
  }
}
