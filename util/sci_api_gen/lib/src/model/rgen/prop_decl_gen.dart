part of sci.api.model.gen;

class PropDeclGen {
  static void process(ClassGenerator gen, ApiClass clazz, StringBuffer buffer) {
    var list = clazz.properties.where((p) => !p.hasParentProperty);
    if (list.isNotEmpty)
      buffer.writeln(list.map((each) => '${each.name} = NULL').join(','));
  }
}

//class PropDeclGen2 {
//  static void process(ClassGenerator gen, ApiClass clazz, StringBuffer buffer) {
////    if (clazz.directParentClass != null && clazz.directParentClass.inherits()){
////
////    }
//
//    clazz.dataProperties.forEach((p) {
//      writePropertyDeclaration(gen, buffer, clazz, p);
//    });
//
//    clazz.objectProperties.forEach((p) {
//      writePropertyDeclaration(gen, buffer, clazz, p);
//    });
//
//    buffer.writeln('');
//  }
//
//  static void writePropertyDeclaration(
//      ClassGenerator gen, StringBuffer buffer, ApiClass clazz, ApiProperty p) {
//    buffer.writeln('${gen.getRange(p, clazz)} ${p.name};');
//  }
//}

//class ParentPropDeclGen {
//  static void process(ClassGenerator gen, OwlClass clazz, StringBuffer buffer) {
//
//    if (clazz.directParentClass == null) return;
//
//    buffer.writeln('${clazz.directParentClass.name} _parent;');
//    buffer.writeln('');
//
//  }
//
//}
