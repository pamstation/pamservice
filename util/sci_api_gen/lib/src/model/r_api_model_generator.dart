library sci.api.model.gen;

import 'dart:io';
import 'dart:async';
import 'package:path/path.dart' as path;

import '../../api_lib.dart';
import '../../api_gen.dart';

part 'rgen/class_cstr_gen.dart';
part 'rgen/init_gen.dart';
part 'rgen/prop_decl_gen.dart';
part 'rgen/prop_get_set_gen.dart';
part 'rgen/prop_names_gen.dart';
part 'rgen/to_json_gen.dart';
part 'rgen/copy_gen.dart';
part 'rgen/vocabulary_gen.dart';

class ClassGenerator extends Generator {
  ClassGenerator(
      ApiLibrary apiLib,
      String directory,
      String libraryName,
      String baseClassName,
      List<Map<String, String>> importLibs,
      List<Map<String, String>> exportLibs)
      : super(apiLib, directory, libraryName, baseClassName, importLibs,
            exportLibs) {
    classFolder = 'R';
    baseFolder = 'R';
  }

  String convertDataTypeRange(String range) {
    var dataType = apiLib.classes[range];

    if (dataType != null) {
      return 'Object';
    }

    return range;
  }

  String get listChangedClassName => 'base.ListChanged';

  String dataTypeDefaultValue(String range) {
    var dataType = apiLib.classes[range];

    if (dataType != null || range == 'Object') {
      return 'null';
    }

    switch (range) {
      case 'bool':
        return 'TRUE';
      case 'int':
        return '0';
      case 'num':
        return '0';
      case 'double':
        return '0.0';
      case 'String':
        return '""';
      case 'dynamic':
        return 'NULL';
      case 'Uint8List':
        return 'NULL';
      default:
        throw new ArgumentError.value(range, 'bad range');
    }
  }

  @override
  Future run() async {
    process();
    var result = await Process.run('R', ['-e', 'formatR::tidy_dir()'],
        workingDirectory: path.join(directory, baseFolder));
    stdout.write(result.stdout);
    stderr.write(result.stderr);
  }

  void process() {
    new Directory(path.join(directory, classFolder))
        .createSync(recursive: true);
    new Directory(path.join(directory, baseFolder)).createSync(recursive: true);

    generateFactory();
    apiLib.classes.values.forEach(generateBase);
  }

  void generateFactory() {
    var filename = path.join(directory, classFolder, 'model_factory.R');
    var file = new File(filename);
    var buffer = new StringBuffer();
    FactoryGen.process(this, buffer);
    file.writeAsStringSync(buffer.toString());
  }

  void generateBase(ApiClass clazz) {
    var filename = path.join(
        directory, baseFolder, '${filenameForClass(clazz, extension: 'R')}');
    var file = new File(filename);
    var buffer = new StringBuffer();

    RClassDocGen.process(this, clazz, baseClassName, buffer);
    RBaseDeclGen.process(this, clazz, baseClassName, buffer);

    PublicGen.process(this, clazz, buffer);

    PropDeclGen.process(this, clazz, buffer);

    // ctr
    BaseDefaultCtrGen.process(this, clazz, buffer);

    ToTsonGen.process(this, clazz, buffer);
    PrintGen.process(this, clazz, buffer);
    EndPublicGen.process(this, clazz, buffer);
    // end class

    REndClassGen.process(this, clazz, buffer);

    file.writeAsStringSync(buffer.toString());
  }

  String getRange(ApiProperty p, ApiClass classContext) {
    var range = p.range;
    if (!p.isFunctional) {
      if (p.isData) {
        range = 'base.ListChangedBase<${range}>';
      } else {
        range = 'base.ListChanged<${range}>';
      }
    }
    return range;
  }

  String getBaseRange(ApiProperty p, ApiClass classContext) {
    var range = p.range;
    if (!p.isData) {
      range = '${range}Base';
    }
    if (!p.isFunctional) {
      if (p.isData) {
        range = 'base.ListChangedBase<${range}>';
      } else {
        range = 'base.ListChanged<${range}>';
      }
    }
    return range;
  }

  String getPropertyRange(ApiProperty p) {
    var range = p.range;

    if (!p.isFunctional) {
      range = 'List<${range}>';
    }
    return range;
  }
}
