library sci.api.model.gen;

import 'dart:io';
import 'dart:async';
import 'package:path/path.dart' as path;

import '../../api_lib.dart';
import '../../api_gen.dart';

part 'gen/class_cstr_gen.dart';

part 'gen/init_gen.dart';
part 'gen/prop_decl_gen.dart';
part 'gen/prop_get_set_gen.dart';
part 'gen/prop_names_gen.dart';
part 'gen/to_json_gen.dart';
part 'gen/copy_gen.dart';
part 'gen/vocabulary_gen.dart';

class ClassGenerator extends Generator {
  ClassGenerator(ApiLibrary apiLib, String directory, String libraryName,
      String baseClassName, List<Map<String, String>> importLibs, List<Map<String, String>> exportLibs)
      : super(apiLib, directory, libraryName, baseClassName, importLibs,exportLibs) {
    classFolder = 'src/model/impl';
    baseFolder = 'src/model/base';
  }

  String convertDataTypeRange(String range) {
    var dataType = apiLib.classes[range];

    if (dataType != null) {
      return 'Object';
    }

    return range;
  }

  String get listChangedClassName => 'base.ListChanged';

  String dataTypeDefaultValue(String range) {
    var dataType = apiLib.classes[range];

    if (dataType != null || range == 'Object') {
      return 'null';
    }

    switch (range) {
      case 'bool':
        return 'true';
      case 'int':
        return '0';
      case 'num':
        return '0';
      case 'double':
        return '0.0';
      case 'String':
        return '""';
//      case 'CStringList':
      case 'Uint8List': return 'null';
//      case 'Uint16List':
//      case 'Uint32List':
//      case 'Int8List': return 'null';
//      case 'Int16List':
//      case 'Int32List':
//      case 'Int64List':
      case 'dynamic':
        return 'null';
      default:
        throw new ArgumentError.value(range, 'bad range');
    }
  }

  @override
  Future run() async {
    process();
    await Process
        .run('dartfmt', ['-w', directory],
            workingDirectory: Directory.current.path)
        .then((result) {
      stdout.write(result.stdout);
      stderr.write(result.stderr);
    });
  }

  void process() {
    new Directory(path.join(directory, classFolder))
        .createSync(recursive: true);
    new Directory(path.join(directory, baseFolder))
        .createSync(recursive: true);

    generateLibrary();
    generateBaseLibrary(true);
    generateVocabularyClass();

    apiLib.classes.values.forEach(generateBase);
    apiLib.classes.values.forEach(generateClass);
  }

  void generateBaseLibrary(bool isModel) {
    var filename = path.join(directory, '${libraryName}_base.dart');
    var file = new File(filename);
    var buffer = new StringBuffer();
    LibModelBaseGen.process(this, libraryName, baseFolder, buffer);

    file.writeAsStringSync(buffer.toString());
  }

  void generateVocabularyClass() {
    var filename = path.join(directory, baseFolder, 'vocabulary.dart');
    var file = new File(filename);
    var buffer = new StringBuffer();
    VocabularyClassGen.process(this, buffer);
    file.writeAsStringSync(buffer.toString());
  }

  void generateClass(ApiClass clazz) {
    var filename =
        path.join(directory, classFolder, '${filenameForClass(clazz)}');
    var file = new File(filename);
    if (file.existsSync()) return;
    var buffer = new StringBuffer();
    PartOfGen.process(this, libraryName, buffer);
    ClassDeclGen.process(this, clazz, baseClassName, buffer);
    ClassDefaultCtrGen.process(this, clazz, buffer);
    ClassJsonCtrGen.process(this, clazz, buffer);
    EndClassGen.process(this, clazz, buffer);
    file.writeAsStringSync(buffer.toString());
  }

  void generateBase(ApiClass clazz) {
    var filename =
        path.join(directory, baseFolder, '${filenameForBaseClass(clazz)}');
    var file = new File(filename);
    var buffer = new StringBuffer();

    PartOfBaseGen.process(this, libraryName, buffer);
    BaseDeclGen.process(this, clazz, baseClassName, buffer);
    // static
    PropNamesStaticGen.process(this, clazz, buffer);
    // properties
//    ParentPropDeclGen.process(this, clazz, buffer);
    PropDeclGen.process(this, clazz, buffer);
    // ctr
    BaseDefaultCtrGen.process(this, clazz, buffer);
    BaseJsonCtrGen.process(this, clazz, buffer);
    BaseFactoryJsonCtrGen.process(this, clazz, buffer);

    // init

//    InitGen.process(this, clazz, buffer);
//    InitJsonGen.process(this, clazz, buffer);

    // listen

//    ListenGen.process(this, clazz, buffer);

    // get set properties

//    ParentPropGetGen.process(this, clazz, buffer);
    PropGetSetGen.process(this, clazz, buffer);
    PropGetSetByNameGen.process(this, clazz, buffer);

    // getPropertyNames

    PropNamesGen.process(this, clazz, buffer);

    // copy()
    CopyGen.process(this, clazz, buffer);
    // toJson
    ToJsonGen.process(this, clazz, buffer);

    // end class

    EndClassGen.process(this, clazz, buffer);

    file.writeAsStringSync(buffer.toString());
  }

  String getRange(ApiProperty p, ApiClass classContext) {
    var range = p.range;
    if (!p.isFunctional) {
      if (p.isData) {
        range = 'base.ListChangedBase<${range}>';
      } else {
        range = 'base.ListChanged<${range}>';
      }
    }
    return range;
  }

  String getBaseRange(ApiProperty p, ApiClass classContext) {
    var range = p.range;
    if (!p.isData) {
      range = '${range}Base';
    }
    if (!p.isFunctional) {
      if (p.isData) {
        range = 'base.ListChangedBase<${range}>';
      } else {
        range = 'base.ListChanged<${range}>';
      }
    }
    return range;
  }

  String getPropertyRange(ApiProperty p) {
    var range = p.range;

    if (!p.isFunctional) {
      range = 'List<${range}>';
    }
    return range;
  }
}
