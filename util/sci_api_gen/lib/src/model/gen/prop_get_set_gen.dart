part of sci.api.model.gen;

class PropGetSetGen {
  static void process(ClassGenerator gen, ApiClass clazz, StringBuffer buffer) {
    writeKindGetter(gen, clazz, buffer);

    clazz.dataProperties.forEach((p) {
      writePropertyGetterSetter(gen, clazz, buffer, p);
    });

    clazz.objectProperties.forEach((p) {
      writePropertyGetterSetter(gen, clazz, buffer, p);
    });

    buffer.writeln('');
  }

  static void writeKindGetter(
      ClassGenerator gen, ApiClass clazz, StringBuffer buffer) {
    buffer.writeln(
        'String get kind => Vocabulary.${VocabularyClassGen.getVarClassName(clazz)};');
  }

  static void writePropertyGetterSetter(
      ClassGenerator gen, ApiClass clazz, StringBuffer buffer, ApiProperty p) {
    var range = gen.getRange(p, clazz);

    buffer.writeln('${range} get ${p.name} => _${p.name};');
    buffer.writeln('');

    if (!p.isFunctional) {
      return;
    }

    if (!p.hasParentProperty) {
      buffer.writeln('set ${p.name}(${range} o) {');

      buffer.writeln('if (o==_${p.name}) return;');
      buffer.writeln('assert(o == null || o is ${range});');

      if (!p.isData) {
        buffer.writeln('if (_${p.name} != null) _${p.name}.parent = null;');
        buffer.writeln('if (o != null) o.parent = this;');
      }

      buffer.writeln('var old = _${p.name};');
      buffer.writeln('_${p.name} = o;');

      buffer.writeln(
          'if (hasListener) this.sendChangeEvent(new base.PropertyChangedEvent(this, Vocabulary.${VocabularyClassGen.getVarPropertyName(p)}, old, _${p.name}));');

      buffer.writeln('}');
      buffer.writeln('');
    }
  }
}

class PropGetSetByNameGen {
  static void process(ClassGenerator gen, ApiClass clazz, StringBuffer buffer) {
    var list = clazz.properties.where((p) => !p.hasParentProperty);

    if (list.isNotEmpty) {
      buffer.writeln('dynamic get(String name){');

      buffer.writeln('switch (name) {');
      list.forEach((p) {
        buffer.writeln(
            'case Vocabulary.${VocabularyClassGen.getVarPropertyName(p)} :');
        buffer.writeln('return this.${p.name};');
      });

      buffer.writeln('default : return super.get(name);');
      buffer.writeln('}');
      buffer.writeln('}');
      buffer.writeln('');
      buffer.writeln('');

      buffer.writeln('set(String name, dynamic value){');

      buffer.writeln('switch (name) {');

      clazz.dataProperties.where((p) => !p.hasParentProperty).forEach((p) {
        buffer.writeln(
            'case Vocabulary.${VocabularyClassGen.getVarPropertyName(p)} :');
        if (p.isFunctional) {
          buffer.writeln('this.${p.name} = value as ${p.range};');
        } else {
          buffer.writeln('_${p.name}..clear()..addAll(value as Iterable<${p.range}>);');
        }

        buffer.writeln('return;');
      });

      clazz.objectProperties.where((p) => !p.hasParentProperty).forEach((p) {
        buffer.writeln(
            'case Vocabulary.${VocabularyClassGen.getVarPropertyName(p)} :');
        if (p.isFunctional) {
          buffer.writeln('this.${p.name} = value as ${p.range};');
        } else {
          buffer.writeln('_${p.name}..clear()..addAll(value as Iterable<${p.range}>);');
        }

        buffer.writeln('return;');
      });

      buffer.writeln('default : super.set(name, value);');
      buffer.writeln('}');
      buffer.writeln('}');
      buffer.writeln('');
      buffer.writeln('');
    }
  }
}
