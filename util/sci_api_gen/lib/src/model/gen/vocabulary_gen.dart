part of sci.api.model.gen;

class VocabularyClassGen {
  static const String KIND = 'KIND';
  static const String SUBKIND = 'SUBKIND';

  static String getVarPropertyName(ApiProperty p) {
    if (!p.isData) return '${p.name}_OP';
    return '${p.name}_DP';
  }

  static String getVarClassName(ApiClass p) {
    return '${p.name}_CLASS';
  }

  static void process(ClassGenerator gen, StringBuffer buffer) {
    buffer.writeln('part of ${gen.libraryName}_base;');
    buffer.writeln('');

    buffer.writeln('class Vocabulary {');
    buffer.writeln('static const String ${KIND} = "kind";');
    buffer.writeln('static const String ${SUBKIND} = "subKind";');

    gen.apiLib.classes.values.forEach((c) {
      buffer.writeln('static const String ${c.name}_CLASS = "${c.name}";');
    });
    buffer.writeln('');
    var unique = new Set();
    gen.apiLib.dataProperties.forEach((c) {
      var nn = getVarPropertyName(c);
      if (!unique.contains(nn)) {
        unique.add(nn);
        buffer.writeln('static const String ${nn} = "${c.name}";');
      }
    });
    buffer.writeln('');
    gen.apiLib.objectProperties.forEach((c) {
      var nn = getVarPropertyName(c);
      if (!unique.contains(nn)) {
        unique.add(nn);
        buffer.writeln('static const String ${nn} = "${c.name}";');
      }
    });
    buffer.writeln('}');
    buffer.writeln('');
  }
}
