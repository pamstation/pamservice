part of sci.api.model.gen;

class InitGen {
  static void process(ClassGenerator gen, ApiClass clazz, StringBuffer buffer) {
    buffer.write('initialize() {');

    clazz.dataProperties.forEach((p) {
      if (p.isFunctional) {
        buffer
            .writeln('this._${p.name} = ${gen.dataTypeDefaultValue(p.range)};');
      } else {
        buffer.writeln('this._${p.name} = new ListChanged([]);');
      }
    });

    clazz.objectProperties.forEach((p) {
      if (p.isFunctional) {
        buffer.writeln('this._${p.name} = new ${p.range}();');
      } else {
        buffer.writeln('this._${p.name} = new ListChanged([]);');
      }
    });

    buffer.writeln('_listen();');

    buffer.writeln('}');
    buffer.writeln('');
  }
}

class InitJsonGen {
  static void process(ClassGenerator gen, ApiClass clazz, StringBuffer buffer) {
    buffer.write('initializeFromJson(Map m) {');

    clazz.dataProperties.forEach((p) {
      writeJsonReadProperty(buffer, p, 'm');
    });

    clazz.objectProperties.forEach((p) {
      writeJsonReadProperty(buffer, p, 'm');
    });

    buffer.writeln('_listen();');

    buffer.writeln('}');
    buffer.writeln('');
  }

  static void writeJsonReadProperty(buffer, ApiProperty p, mapVar) {
    final name = p.name;
    if (p.isFunctional) {
      if (p.isData) {
        buffer.writeln(
            'this._${name} = ${mapVar}[Vocabulary.${VocabularyClassGen.getVarPropertyName(p)}];');
      } else {

        buffer.writeln(
            'this.${name} = new ${p.range}.fromJson(${mapVar}[Vocabulary.${VocabularyClassGen.getVarPropertyName(p)}]);');
      }
    } else {
      if (p.isData) {
        buffer.writeln(
            'this._${name} = ${mapVar}[Vocabulary.${VocabularyClassGen.getVarPropertyName(p)}];');
      } else {
         buffer.writeln(
            'this._${name} = new ListChanged((${mapVar}[Vocabulary.${VocabularyClassGen.getVarPropertyName(p)}] as List).map((each)=> new ${p.range}.fromJson(each)).toList());');
      }
    }
  }
}