part of sci.api.model.gen;

class PropNamesGen {
  static void process(ClassGenerator gen, ApiClass clazz, StringBuffer buffer) {
    buffer.writeln('List<String> getPropertyNames() =>');
    buffer.writeln(
        'new List.from(super.getPropertyNames(),growable: true)..addAll(PROPERTY_NAMES);');

    buffer.writeln('');
  }
}

class PropNamesStaticGen {
  static void process(ClassGenerator gen, ApiClass clazz, StringBuffer buffer) {
    var set = new Set();

    var list = clazz.properties.where((p) => !p.hasParentProperty);

    list.forEach((p) {
      set.add('Vocabulary.${VocabularyClassGen.getVarPropertyName(p)}');
    });

    buffer.write(
        'static const List<String> PROPERTY_NAMES = const [${set.join(',')}];');

    buffer.writeln('');
  }
}
