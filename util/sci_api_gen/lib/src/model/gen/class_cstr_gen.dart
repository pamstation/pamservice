part of sci.api.model.gen;

class ClassDefaultCtrGen {
  static void process(ClassGenerator gen, ApiClass clazz, StringBuffer buffer) {
    buffer.write('${clazz.name}(): super();');
    buffer.writeln('');
  }
}

class BaseDefaultCtrGen {
  static void process(ClassGenerator gen, ApiClass clazz, StringBuffer buffer) {
    buffer.write('${clazz.name}Base(): super() ');

    if (clazz.hasProperties) {
      buffer.writeln('{');
      buffer.writeln('this.noEventDo((){');
      clazz.dataProperties.forEach((p) {
        if (p.isFunctional) {
          buffer.writeln(
              'this.${p.name} = ${gen.dataTypeDefaultValue(p.range)};');
        } else {
          buffer.writeln('this._${p.name} = new ${gen.getRange(p, clazz)}();');
          buffer.writeln('this._${p.name}.parent = this;');
        }
      });

      clazz.objectProperties.forEach((p) {
        if (p.isFunctional) {
          buffer.writeln('this.${p.name} = new ${gen.getRange(p, clazz)}();');
        } else {
          buffer.writeln('this._${p.name} = new ${gen.getRange(p, clazz)}();');
          buffer.writeln('this._${p.name}.parent = this;');
        }
//
//        if (p.range.directParentClass == clazz) {
//          buffer.writeln('this._${p.name}.parent = this;');
//        }
      });

      // end no event
      buffer.writeln('});');

      buffer.writeln('}');
    } else {
      buffer.write(';');
    }

    buffer.writeln('');
  }
}

class BaseJsonCtrGen {
  static void process(ClassGenerator gen, ApiClass clazz, StringBuffer buffer) {
    buffer.write('${clazz.name}Base.json(Map m) : super.json(m)');

    if (clazz.hasProperties) {
      buffer.writeln('{');
      buffer.writeln('this.noEventDo((){');
      buffer.writeln(
          'this.subKind = m[Vocabulary.SUBKIND] != null ?'
              ' m[Vocabulary.SUBKIND]  as String :'
              ' (m[Vocabulary.KIND] != Vocabulary.${VocabularyClassGen.getVarClassName(clazz)} ? m[Vocabulary.KIND] : null) as String;');
      clazz.dataProperties.forEach((p) {
        if (p.isFunctional) {
          var range = gen.getRange(p, clazz);
          if (range == 'double') {
            buffer.writeln(
                'this.${p.name} = (m[Vocabulary.${VocabularyClassGen.getVarPropertyName(p)}] as num).toDouble();');
          } else {
            buffer.writeln(
                'this.${p.name} = m[Vocabulary.${VocabularyClassGen.getVarPropertyName(p)}] as ${p.range};');
          }
        } else {
          buffer.writeln(
              'this._${p.name} = new ${gen.getRange(p, clazz)}( (m[Vocabulary.${VocabularyClassGen.getVarPropertyName(p)}] as List).cast());');
          buffer.writeln('this._${p.name}.parent = this;');
        }
      });

      clazz.objectProperties.forEach((p) {
        if (p.isFunctional) {
          buffer.writeln(
              'if (m[Vocabulary.${VocabularyClassGen.getVarPropertyName(p)}] == null) this.${p.name} = new ${gen.getRange(p, clazz)}();');
          if (!p.hasParentProperty) {
            buffer.writeln(
                'else this.${p.name} =   ${p.range}Base.fromJson(m[Vocabulary.${VocabularyClassGen.getVarPropertyName(p)}] as Map);');
          }
        } else {
          buffer.writeln(
              'if (m[Vocabulary.${VocabularyClassGen.getVarPropertyName(p)}] == null)');
          buffer.writeln('this._${p.name} = new ${gen.getRange(p, clazz)}();');
          buffer.writeln(
              'else this._${p.name} = new ${gen.getRange(p, clazz)}((m[Vocabulary.${VocabularyClassGen.getVarPropertyName(p)}] as List).map((each)=> ${p.range}Base.createFromJson(each as Map)).toList());');
          buffer.writeln('this._${p.name}.parent = this;');
        }
//        if (p.range.directParentClass == clazz) {
//          buffer.writeln('if (this._${p.name} != null) this._${p.name}.parent = this;');
//        }
      });

      // end no event
      buffer.writeln('});');

      buffer.writeln('}');
    } else {
      buffer.write(';');
    }

    buffer.writeln('');
  }

  static void writeJsonReadProperty(buffer, ApiProperty p, mapVar) {
    final name = p.name;
    if (p.isFunctional) {
      if (p.isData) {
        buffer.writeln(
            'this._${name} = ${mapVar}[Vocabulary.${VocabularyClassGen.getVarPropertyName(p)}];');
      } else {
        buffer.writeln(
            'this.${name} = new ${p.range}.fromJson(${mapVar}[Vocabulary.${VocabularyClassGen.getVarPropertyName(p)}]);');
      }
    } else {
      if (p.isData) {
        buffer.writeln(
            'this._${name} = ${mapVar}[Vocabulary.${VocabularyClassGen.getVarPropertyName(p)}];');
      } else {
        buffer.writeln(
            'this._${name} = new base.ListChanged((${mapVar}[Vocabulary.${VocabularyClassGen.getVarPropertyName(p)}] as List).map((each)=>   ${p.range}Base.createFromJson(each as Map)).toList());');
      }
    }
  }
}

class ClassJsonCtrGen {
  static void process(ClassGenerator gen, ApiClass clazz, StringBuffer buffer) {
    buffer.write('${clazz.name}.json(Map m) : super.json(m);');

    buffer.writeln('');
  }

  static void writeJsonReadProperty(buffer, ApiProperty p, mapVar) {
    final name = p.name;
    if (p.isFunctional) {
      if (p.isData) {
        buffer.writeln(
            'this._${name} = ${mapVar}[Vocabulary.${VocabularyClassGen.getVarPropertyName(p)}];');
      } else {
        buffer.writeln(
            'this.${name} = new ${p.range}.fromJson(${mapVar}[Vocabulary.${VocabularyClassGen.getVarPropertyName(p)}]);');
      }
    } else {
      if (p.isData) {
        buffer.writeln(
            'this._${name} = ${mapVar}[Vocabulary.${VocabularyClassGen.getVarPropertyName(p)}];');
      } else {
        buffer.writeln(
            'this._${name} = new base.ListChanged((${mapVar}[Vocabulary.${VocabularyClassGen.getVarPropertyName(p)}] as List).map((each)=> ${p.range}Base.createFromJson(each as Map)).toList());');
      }
    }
  }
}

class BaseFactoryJsonCtrGen {
  static void process(ClassGenerator gen, ApiClass clazz, StringBuffer buffer) {
    buffer.writeln(
        'static ${clazz.name} createFromJson(Map m) => ${clazz.name}Base.fromJson(m);');

    buffer.write('static ${clazz.name} fromJson(Map m) {');
    buffer.writeln('final kind = m[Vocabulary.KIND] as String;');
    buffer.writeln('switch (kind) {');
    buffer.writeln(
        'case Vocabulary.${VocabularyClassGen.getVarClassName(clazz)} :');
    buffer.writeln('return new ${clazz.name}.json(m);');
    clazz.allSubClasses.forEach((c) {
      buffer.writeln(
          'case Vocabulary.${VocabularyClassGen.getVarClassName(c)} :');
      buffer.writeln('return new ${c.name}.json(m);');
    });
    buffer.writeln(
        'default : throw new ArgumentError.value(kind, "bad kind for class ${clazz.name} in fromJson constructor");');
    buffer.writeln('}');
    buffer.writeln('}');
    buffer.writeln('');
  }
}

class ClassFactoryJsonCtrGen {
  static void process(ClassGenerator gen, ApiClass clazz, StringBuffer buffer) {
    buffer.write('factory ${clazz.name}.fromJson(Map m) {');
    buffer.writeln('final kind = m[Vocabulary.KIND] as String;');
    buffer.writeln('switch (kind) {');
    buffer.writeln(
        'case Vocabulary.${VocabularyClassGen.getVarClassName(clazz)} :');
    buffer.writeln('return new ${clazz.name}.json(m);');
    clazz.subClasses.forEach((c) {
      buffer.writeln(
          'case Vocabulary.${VocabularyClassGen.getVarClassName(c)} :');
      buffer.writeln('return new ${c.name}.json(m);');
    });
    buffer.writeln(
        'default : throw new ArgumentError.value(kind, "bad kind for class ${clazz.name} in fromJson constructor");');
    buffer.writeln('}');
    buffer.writeln('}');
    buffer.writeln('');
  }
}

//class ListenGen {
//  static void process(ClassGenerator gen, ApiClass clazz, StringBuffer buffer) {
//
//    buffer.writeln('void _listen() {');
//
////    if (clazz.subClassOf.isNotEmpty) buffer.writeln('super._listen();');
////    clazz.objectProperties.forEach((p) {
////      buffer.writeln('this.listen(this._${p.name});');
////    });
//    buffer.writeln('}');
//    buffer.writeln('');
//  }
//}
