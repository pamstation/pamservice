part of sci.api.model.gen;

class ToJsonGen {
  static void process(ClassGenerator gen, ApiClass clazz, StringBuffer buffer) {
    buffer.writeln('Map toJson() {');
    buffer.write('var m = ');
    if (clazz.superName != null) {
      buffer.write('super.toJson() as Map;');
    } else {
      buffer.write('{};');
    }

    writeJsonWriteKindProperty(buffer, clazz, 'm');

    var list = clazz.properties.where((p) => !p.hasParentProperty);

    list.forEach((p) {
      writeJsonWriteProperty(buffer, p, 'm');
    });

    buffer.writeln('return m;');

    buffer.writeln('}');
  }

  static void writeJsonWriteKindProperty(buffer, ApiClass c, mapVar) {
    buffer.writeln(
        '${mapVar}[Vocabulary.KIND] = Vocabulary.${VocabularyClassGen.getVarClassName(c)};');
    buffer.writeln(
        'if (this.subKind != null && this.subKind != Vocabulary.${VocabularyClassGen.getVarClassName(c)}) ${mapVar}[Vocabulary.SUBKIND] = this.subKind;');
    buffer.writeln('else ${mapVar}.remove(Vocabulary.SUBKIND);');
  }

  static void writeJsonWriteProperty(buffer, ApiProperty p, mapVar) {
    final name = p.name;
    if (p.isFunctional) {
      if (p.isData) {
        buffer.writeln(
            '${mapVar}[Vocabulary.${VocabularyClassGen.getVarPropertyName(p)}] = ${name};');
      } else {
        buffer.writeln(
            '${mapVar}[Vocabulary.${VocabularyClassGen.getVarPropertyName(p)}] = ${name} == null ? null : ${name}.toJson();');
      }
    } else {
      if (p.isData) {
        buffer.writeln(
            '${mapVar}[Vocabulary.${VocabularyClassGen.getVarPropertyName(p)}] = ${name};');
      } else {
        buffer.writeln(
            '${mapVar}[Vocabulary.${VocabularyClassGen.getVarPropertyName(p)}] = ${name}.toJson();');
      }
    }
  }
}
