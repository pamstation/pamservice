part of sci.api.gen;

class PartOfGen {
  static void process(Generator gen, String libraryName, StringBuffer buffer) {
    buffer.writeln('part of ${libraryName};');
    buffer.writeln('');
  }
}

class PartOfBaseGen {
  static void process(Generator gen, String libraryName, StringBuffer buffer) {
    buffer.writeln('part of ${libraryName}_base;');
    buffer.writeln('');
  }
}

class PartOfMixinGen2 {
  static void process(Generator gen, String libraryName, StringBuffer buffer) {
    buffer.writeln('part of ${libraryName};');
    buffer.writeln('');
  }
}