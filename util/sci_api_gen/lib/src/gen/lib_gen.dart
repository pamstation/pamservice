part of sci.api.gen;

class LibGen {
  static void process(
      Generator gen,
      String libraryName,
      List<Map<String, String>> importLibs,
      String classFolder,
      StringBuffer buffer,
      {List<Map<String, String>> exportLibs}) {
    buffer.writeln('library ${libraryName};');
    buffer.writeln('');

    if (exportLibs != null) {
      exportLibs.forEach((maplib) {
        var lib = maplib['lib'];
        buffer.write('export "${lib}";');
      });
    }

    importLibs.forEach((maplib) {
      var lib = maplib['lib'];
      var libas = maplib['as'];
      buffer.write('import "${lib}"');
      if (libas != null) {
        buffer.writeln(' as ${libas};');
      } else {
        buffer.writeln(';');
      }
    });

    buffer.writeln('');

    gen.apiLib.classes.values.forEach((clazz) {
      var filename = path.join(classFolder, gen.filenameForClass(clazz));
      buffer.writeln("part '${filename}';");
    });
  }
}

class LibServiceGen {
  static void process(
      Generator gen,
      String libraryName,
      List<Map<String, String>> importLibs,
      List<Map<String, String>> exportLibs,
      String classFolder,
      StringBuffer buffer) {
    LibGen.process(gen, libraryName, importLibs, classFolder, buffer,
        exportLibs: exportLibs);
    var filename = path.join(classFolder, 'factory.dart');
    buffer.writeln("part '$filename';");
  }
}

class LibClientGen {
  static void process(
      Generator gen,
      String libraryName,
      List<Map<String, String>> importLibs,
      List<Map<String, String>> exportLibs,
      String classFolder,
      StringBuffer buffer) {
    LibGen.process(gen, libraryName, importLibs, classFolder, buffer,
        exportLibs: exportLibs);
    var filename = path.join(classFolder, 'factory.dart');
    buffer.writeln("part '$filename';");
  }
}

class LibServiceBaseGen {
  static void process(Generator gen, String libraryName, String importModelLib,
      String baseFolder, StringBuffer buffer) {
    var libs = [
      {'lib': "dart:async"},
      {'lib': 'package:couch_client/couch_client.dart', 'as': 'couch'},
      {'lib': 'package:sci_api_gen/sci_service_base.dart'},
      {'lib': 'package:sci_api_gen/sci_service.dart', 'as': 'service'},
      {'lib': 'package:sci_util/src/util/error/error.dart'},
      {'lib': "sci_service_factory.dart", 'as': 'api'},
      {'lib': "${libraryName}.dart"},
      {'lib': "${importModelLib}.dart"},
      {'lib': "${importModelLib}_base.dart"}
    ];
    LibGen.process(gen, '${libraryName}_base', libs, baseFolder, buffer);
    var filename = path.join(baseFolder, 'factory.dart');
    buffer.writeln("part '$filename';");
  }
}

class LibClientServiceBaseGen {
  static void process(Generator gen, String libraryName, String importModelLib,
      String baseFolder, StringBuffer buffer) {
    var libs = [
      {'lib': "dart:async"},
      {'lib': "dart:typed_data"},
      {'lib': "dart:convert"},
      {'lib': "package:async/async.dart", 'as': 'async'},
      {'lib': 'package:tson/tson.dart', 'as': 'TSON'},
      {'lib': 'package:http_client/http_client.dart'},
      {'lib': 'package:sci_api_gen/sci_client_base.dart'},
      {'lib': 'package:sci_api_gen/sci_service.dart', 'as': 'service'},
      {'lib': 'package:sci_util/src/util/error/error.dart'},
      {'lib': "sci_service_factory.dart", 'as': 'api'},
      {'lib': "${libraryName}.dart"},
      {'lib': "${importModelLib}.dart"},
      {'lib': "${importModelLib}_base.dart"}
    ];
    LibGen.process(gen, '${libraryName}_base', libs, baseFolder, buffer);
    var filename = path.join(baseFolder, 'factory.dart');
    buffer.writeln("part '$filename';");
  }
}

class LibModelBaseGen {
  static void process(Generator gen, String libraryName, String baseFolder,
      StringBuffer buffer) {
    var libs = [
      {'lib': 'dart:typed_data'},
      {'lib': 'package:sci_api_gen/sci_base.dart', 'as': 'base'},
      {'lib': 'package:sci_util/src/util/error/error.dart'},
      {'lib': "${libraryName}.dart"}
    ];
    LibGen.process(gen, '${libraryName}_base', libs, baseFolder, buffer);
    var filename = path.join(baseFolder, 'vocabulary.dart');
    buffer.writeln("part '${filename}';");
  }
}

//class LibMixinGen2 {
//  static void process(
//      Generator gen,
//      String libraryName,
//      List<Map<String, String>> importLibs,
//      String classFolder,
//      String mixinFolder,
//      StringBuffer buffer) {
//    buffer.writeln('library ${libraryName};');
//    buffer.writeln('');
//
//    gen.apiLib.classes.values.forEach((clazz) {
//      var filename = path.join(mixinFolder, gen.filenameForMixin(clazz));
//      buffer.writeln("part '${filename}';");
//    });
//  }
//}
