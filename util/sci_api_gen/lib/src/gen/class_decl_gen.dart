part of sci.api.gen;

class BaseDeclGen {
  static void process(Generator gen, ApiClass clazz, String baseClassName,
      StringBuffer buffer) {
    buffer.write('class ${clazz.name}Base ');
    var parentClass = clazz.superName;
    if (parentClass == 'Object') parentClass = baseClassName;
    buffer.write('extends ${parentClass} ');
    if (clazz.interfaces.isNotEmpty){
      buffer.write('implements ${clazz.interfaces.join(',')} ');
    }
    buffer.writeln('{');
    buffer.writeln('');
  }
}

class ServiceBaseClassDeclGen {
  static void process(Generator gen, ApiClass clazz, String baseClassName,
      StringBuffer buffer) {
    buffer.write('class ${clazz.name}Base ');
    var parentClass = clazz.superName;
    if (parentClass == 'Object') {
      ApiPersistentService metadataService =
          clazz.metadata.firstWhere((each) => each is ApiPersistentService);
      parentClass = baseClassName + '<${metadataService.className}>';
    }
    buffer.write('extends ${parentClass}  implements api.${clazz.name}');
    buffer.writeln('{');
    buffer.writeln('');

//    buffer.writeln('ContentCodec contentCodec = new JsonContentCodec();');
  }
}

class MixinDeclGen2 {
  static void process(Generator gen, ApiClass clazz, String baseClassName,
      StringBuffer buffer) {
    buffer.write('class ${clazz.name} ');
    var parentClass = clazz.superName;

    if (parentClass != null) buffer.write('extends ${parentClass} ');

    buffer.writeln('{');
    buffer.writeln('');
  }
}

class ClassDeclGen {
  static void process(Generator gen, ApiClass clazz, String baseClassName,
      StringBuffer buffer) {
    buffer.write('class ${clazz.name} extends ${clazz.name}Base ');

    buffer.writeln('{');
    buffer.writeln('');
  }
}

class ClassDeclGen2 {
  static void process(Generator gen, ApiClass clazz, String baseClassName,
      StringBuffer buffer) {
    buffer.write('class ${clazz.name} extends ${clazz.name} ');

    buffer.writeln('{');
    buffer.writeln('');
  }
}
