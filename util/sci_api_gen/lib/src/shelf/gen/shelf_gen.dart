part of sci.api.shelf.gen;

class ShelfBaseClassDeclGen {
  static void route(Generator gen, ApiLibrary apiLib, String baseClassName,
      StringBuffer buffer) {
    // shelf router
    buffer.writeln('@override');
    buffer.writeln('shelfRoute(){');

    buffer.writeln('var router = route.router();');
    for (var clazz in apiLib.classes.values) {
      buffer.writeln('// ====================== ${clazz.name};');

      var propertyName =
          clazz.name.substring(0, 1).toLowerCase() + clazz.name.substring(1);

      ApiPersistentService meta =
          clazz.metadata.firstWhere((each) => each is ApiPersistentService);
      clazz.methods.where((method) => !method.isConstructor).forEach((method) {
        bool isWebSocket = method.metadata.any((each) => each is ApiWebSocket);

        if (isWebSocket) {
          buffer.writeln('router.get("/${meta.uri}/${method.name}", '
              'webSocketHandler( (ws) => '
              'wsHandler("/${meta.uri}/${method.name}", ws) ));');
        } else {
          buffer.writeln('router.get("/${meta.uri}/${method.name}", handler);');
          buffer
              .writeln('router.post("/${meta.uri}/${method.name}", handler);');
        }
      });

      List<ApiUriResource> uriResources =
          clazz.metadata.where((each) => each is ApiUriResource).toList();

      for (var uriResource in uriResources) {
        if (uriResource is ApiPersistentService) {
          buffer.writeln('router.delete("/${uriResource.uri}", handler);');
          buffer.writeln('router.get("/${uriResource.uri}", handler);');
          buffer.writeln('router.post("/${uriResource.uri}", handler);');
          buffer.writeln('router.put("/${uriResource.uri}", handler);');
          buffer.writeln('router.post("/${uriResource.uri}/list", handler);');
        } else if (uriResource is ApiView) {
          buffer.writeln(
              'router.get("/${meta.uri}/${uriResource.uri}", handler);');
          buffer.writeln(
              'router.post("/${meta.uri}/${uriResource.uri}", handler);');
        }
      }
    }
    buffer.writeln('return router;');
    buffer.writeln('}');
  }

  static void processWSHandler(Generator gen, ApiLibrary apiLib,
      String baseClassName, StringBuffer buffer) {
    buffer.writeln('@override');
    buffer.writeln('wsHandler(String path,  WebSocketChannel webSocket,'
        ' service.AclContext aclContext, Map queryParameters){');

    buffer.writeln('var serv;');

    for (var clazz in apiLib.classes.values) {
      buffer.writeln('// ====================== ${clazz.name};');

      var propertyName =
          clazz.name.substring(0, 1).toLowerCase() + clazz.name.substring(1);

      ApiPersistentService meta =
          clazz.metadata.firstWhere((each) => each is ApiPersistentService);
      clazz.methods.where((method) => !method.isConstructor).forEach((method) {
        bool isWebSocket = method.metadata.any((each) => each is ApiWebSocket);

        if (isWebSocket) {
          buffer.writeln('serv = serviceFactory.${propertyName};');

          buffer.writeln(' if (path == "/${meta.uri}/${method.name}"){');
          buffer.writeln('var result;');
          processWSMethod(gen, meta, method, buffer);

          // we have result variable holding the stream

//          buffer.writeln('webSocket.sink.addStream(result.map((each)=>json.encode(each.toJson())));');

          buffer.writeln('addResultStreamToWebSocket(result.map((each) => '
              'TSON.encode(each?.toJson())) as Stream, webSocket);');

          buffer.writeln('}');
        }
      });
    }

    buffer.writeln('}');
  }

  static void process(Generator gen, ApiLibrary apiLib, String baseClassName,
      StringBuffer buffer) {
    buffer.write('class ShelfBase extends ShelfService ');

    buffer.writeln('{');

    buffer.writeln('ServiceFactory serviceFactory;');
    //cstr
    buffer.writeln('ShelfBase(){');
    buffer.writeln('serviceFactory = new ServiceFactory();');
    //end ctsr
    buffer.writeln('}');

    buffer.writeln(
        "Map<String,String> get jsonHeader => {'content-type':'application/json'};");
    buffer.writeln(
        "Map<String,String> get tsonHeader => {'content-type':'application/octet-stream'};");
    buffer.writeln(
        "Map<String,String> get csvHeader => {'content-type':'text/csv'};");
    buffer.writeln(
        "Map<String,String> get binaryHeader => {'content-type':'application/octet-stream'};");

    buffer.writeln("ContentCodec contentCodec = new TsonContentCodec();");

//    route(gen, apiLib, baseClassName, buffer);
    processWSHandler(gen, apiLib, baseClassName, buffer);

    // shelf handler
    buffer.writeln('@override');
    buffer.writeln('Future<shelf.Response> process(shelf.Request request,'
        ' service.AclContext aclContext) async {');

    buffer.writeln('var path = request.requestedUri.path;');
    buffer.writeln('var serv;');

    for (var clazz in apiLib.classes.values) {
      buffer.writeln('// ====================== ${clazz.name};');

      var propertyName =
          clazz.name.substring(0, 1).toLowerCase() + clazz.name.substring(1);

      buffer.writeln('serv = serviceFactory.${propertyName};');

      List<ApiUriResource> uriResources = clazz.metadata
          .where((each) => each is ApiUriResource)
          .toList()
          .cast();

      for (var uriResource in uriResources) {
        if (uriResource is ApiPersistentService) {
          processService(clazz, uriResource, buffer);
        } else if (uriResource is ApiView) {
          processView(clazz, uriResource, buffer);
        }
      }

      ApiPersistentService meta =
          clazz.metadata.firstWhere((each) => each is ApiPersistentService);
      clazz.methods
          .where((each) => !each.isConstructor)
          .forEach((each) => processMethod(gen, meta, each, buffer));
    }

    buffer.writeln(
        'return new shelf.Response(500, headers: contentCodec.contentTypeHeader,'
        ' body: json.encode(new ServiceError(500,"","bad request").toJson()));');
    // end shelf handler
    buffer.writeln('}');

    // end class
    buffer.writeln('}');
  }

  static void processSimpleMethod(Generator gen, ApiPersistentService meta,
      ApiMethod method, StringBuffer buffer) {
    ApiRequestVerb requestVerb = method.metadata.firstWhere(
        (each) => each is ApiRequestVerb,
        orElse: () => const ApiRequestVerb('POST'));

    bool isWebSocket = method.metadata.any((each) => each is ApiWebSocket);

    if (requestVerb.verb == 'GET' || isWebSocket) {
      buffer.writeln(
          'var params = json.decode(request.url.queryParameters["params"] as String);');
    } else if (requestVerb.verb == 'POST') {
      buffer.writeln(
          'var params = await contentCodec.decodeStream(request.read(), request.encoding);');
    } else
      throw 'bad verb';

    var args = [];

    for (var arg in method.arguments) {
      switch (arg.rangeType) {
        case 'num':
        case 'int':
        case 'double':
        case 'bool':
        case 'String':
          args.add('params["${arg.name}"]');
          break;
        case 'List':
          {
            if (arg.rangeTypes.isEmpty) {
              args.add('params["${arg.name}"]');
            } else {
              var rt = arg.rangeTypes.first;
              switch (rt) {
                case 'num':
                  args.add('params["${arg.name}"].cast<num>().toList()');
                  break;
                case 'int':
                args.add('params["${arg.name}"].cast<int>().toList()');
                break;
                case 'double':
                  args.add('params["${arg.name}"].cast<double>().toList()');
                  break;
                case 'bool':
                  args.add('params["${arg.name}"].cast<bool>().toList()');
                  break;
                case 'String':
                  args.add('params["${arg.name}"].cast<String>().toList()');
                  break;
                default:
                  args.add(
                      '(params["${arg.name}"] as List).map((m)=> ${rt}Base.fromJson(m as Map)).toList()');
              }
            }

            break;
          }

        default:
          args.add('${arg.rangeType}Base.fromJson(params["${arg.name}"] as Map)');
      }
    }

    var doAwait = 'await';
    if (method.returnType == 'Stream') {
      doAwait = '';
    }

    buffer
      ..writeln('result = ${doAwait} serv.${method.name}(')
      ..write(args.join(','))
      ..writeln(', aclContext: aclContext);');
  }

  static void processWSMethod(Generator gen, ApiPersistentService meta,
      ApiMethod method, StringBuffer buffer) {
    buffer.writeln('var params = json.decode(queryParameters["params"] as String);');

    var args = [];

    for (var arg in method.arguments) {
      switch (arg.rangeType) {
        case 'num':
        case 'int':
        case 'double':
        case 'bool':
        case 'String':
          args.add('params["${arg.name}"]');
          break;
        case 'List':
          {
            if (arg.rangeTypes.isEmpty) {
              args.add('params["${arg.name}"]');
            } else {
              var rt = arg.rangeTypes.first;
              switch (rt) {
                case 'num':
                case 'int':
                case 'double':
                case 'bool':
                case 'String':
                  args.add('params["${arg.name}"]');
                  break;
                default:
                  args.add(
                      '(params["${arg.name}"] as List).map((m)=> ${rt}Base.fromJson(m as Map)).toList()');
              }
            }

            break;
          }

        default:
          args.add('${arg.rangeType}Base.fromJson(params["${arg.name}"] as Map)');
      }
    }

    var doAwait = 'await';
    if (method.returnType == 'Stream') {
      doAwait = '';
    }

    buffer
      ..writeln('result = ${doAwait} serv.${method.name}(')
      ..write(args.join(','))
      ..writeln(', aclContext: aclContext);');
  }

  static void processMultipartMethod(Generator gen, ApiPersistentService meta,
      ApiMethod method, StringBuffer buffer) {
    buffer
      ..writeln(
          'if (!request.headers.containsKey(io.HttpHeaders.CONTENT_TYPE)) throw "content type is required";')
      ..writeln(
          'var contentType = io.ContentType.parse(request.headers[io.HttpHeaders.CONTENT_TYPE]);')
      ..writeln(
          'if (contentType.primaryType != "multipart") throw "multipart is required";')
      ..writeln('var boundary = contentType.parameters["boundary"];')
      ..writeln('if (boundary == null) throw "boundary is required";')
      ..writeln(
          'List<MimeMultipart> parts = await request.read().transform(new MimeMultipartTransformer(boundary)).toList();');

    var args = [];

    for (var i = 0; i < method.arguments.length; i++) {
      var arg = method.arguments[i];
      switch (arg.rangeType) {
        case 'num':
        case 'int':
        case 'double':
        case 'bool':
        case 'String':
          args.add(
              '((await json.fuse(utf8).decoder.bind( parts[${i}]).toList()).first as List).first');
          break;
        case 'Stream':
          args.add('parts[${i}]');
          break;
        case 'List':
          {
            if (arg.rangeTypes.isEmpty) {
              args.add('parts[${i}]');
            } else {
              var rt = arg.rangeTypes.first;
              switch (rt) {
                case 'num':
                case 'int':
                case 'double':
                case 'bool':
                case 'String':
                  args.add(
                      '((await json.fuse(utf8).decoder.bind( parts[${i}]).toList()).first as List).first');
                  break;
                default:
                  args.add(
                      '(((await json.fuse(utf8).decoder.bind( parts[${i}]).toList()).first as List).first as List).map((m)=> ${rt}Base.fromJson(m as Map)).toList()');
              }
            }

            break;
          }

        default:
          args.add(
              '${arg.rangeType}Base.fromJson(((await json.fuse(utf8).decoder.bind( parts[${i}]).toList()).first as List).first as Map)');
      }
    }

    buffer
      ..writeln('result = await serv.${method.name}(')
      ..write(args.join(','))
      ..writeln(', aclContext: aclContext);');
  }

  static void processMethod(Generator gen, ApiPersistentService meta,
      ApiMethod method, StringBuffer buffer) {
    ApiRequestVerb requestVerb = method.metadata.firstWhere(
        (each) => each is ApiRequestVerb,
        orElse: () => const ApiRequestVerb('POST'));

    buffer
      ..writeln(
          'if (request.method == "${requestVerb.verb}" && path.endsWith("${meta.uri}" + "/" + "${method.name}")){')
      ..writeln('var result;');

    if (method.arguments.any((arg) => arg.rangeType == 'Stream')) {
      processMultipartMethod(gen, meta, method, buffer);
    } else {
      processSimpleMethod(gen, meta, method, buffer);
    }

    ApiResponseContentType responseContentType = method.metadata.firstWhere(
        (each) => each is ApiResponseContentType,
        orElse: () => const ApiResponseContentType('tson'));

    var headers;
    if (responseContentType.contentType == 'json') {
      headers = 'jsonHeader';
    } else if (responseContentType.contentType == 'tson') {
      headers = 'tsonHeader';
    } else if (responseContentType.contentType == 'text/csv') {
      headers = 'csvHeader';
    } else if (responseContentType.contentType == 'application/octet-stream') {
      headers = 'binaryHeader';
    } else
      throw 'bad contentType';

    switch (method.returnType) {
      case 'Future':
        {
          var encoder;
          if (responseContentType.contentType == 'json') {
            encoder = 'var encoder = json.encode;';
          } else if (responseContentType.contentType == 'tson') {
            encoder =
                'var encoder = (object) => new Stream.fromIterable([TSON.encode(object)]);';
          } else
            throw 'bad contentType';

          buffer.writeln(encoder);

          if (!method.returnTypes.isEmpty) {
            var rt = method.returnTypes.first;
            switch (rt) {
              case 'num':
              case 'int':
              case 'double':
              case 'bool':
              case 'String':
                buffer.writeln(
                    'return new shelf.Response(200, headers: ${headers}, body:encoder([result]));');
                break;
              case 'List':
                buffer.writeln(
                    'return new shelf.Response(200, headers: ${headers}, body:encoder(result));');
                break;
              case 'dynamic':
                buffer.writeln(
                    'return new shelf.Response(200, headers: ${headers}, body:encoder([null]));');
                break;
              default:
                buffer.writeln(
                    'return new shelf.Response(200, headers: ${headers}, body:encoder(result?.toJson()));');
            }
          }
          break;
        }
      case 'Stream':
        {
          buffer.writeln(
              'return new shelf.Response(200, headers: ${headers}, body:result);');
          break;
        }
      default:
        throw 'not supported';
    }

    buffer.writeln('}');
  }

  static void processService(
      ApiClass clazz, ApiPersistentService uriResource, StringBuffer buffer) {
    buffer.writeln(
        'if ( path.endsWith("${uriResource.uri}") || path.endsWith("${uriResource.uri}/list") ) {');

    buffer.writeln('return processService(request, serv as  service.Service, aclContext); ');

    //end if
    buffer.writeln('}');
  }

  static void processView(
      ApiClass clazz, ApiView uriResource, StringBuffer buffer) {
    buffer.writeln('if (path.endsWith("${uriResource.uri}")) {');
    buffer.writeln(
        'var params = await contentCodec.decodeStream(request.read(), request.encoding);');
    buffer.writeln('''
    var useFactory =
    request.requestedUri.queryParameters["useFactory"] == 'true'
        ? true
        : false;''');

    if (uriResource.startKeys != null) {
      buffer
        ..write('var list = await serv.${uriResource.name}(')
        ..write('startKey:params["startKey"],')
        ..write('endKey:params["endKey"],')
        ..write('limit:params["limit"],')
        ..write('skip:params["skip"],')
        ..write('descending:params["descending"],')
        ..write('useFactory:useFactory,')
        ..writeln('aclContext:aclContext);');
    } else {
      buffer
        ..write('var list = await serv.${uriResource.name}(')
        ..write('keys:params,')
        ..write('useFactory:useFactory,')
        ..writeln('aclContext:aclContext);');
    }

    buffer.writeln(
        'return new shelf.Response(200, headers: contentCodec.contentTypeHeader,'
        ' body: contentCodec.encodeStream(list.map((each)=>each?.toJson()).toList()));');

    buffer.writeln('}');
  }
}

class ShelfClassDeclGen {
  static void process(Generator gen, ApiLibrary apiLib, String baseClassName,
      StringBuffer buffer) {
    buffer.write('class Shelf extends ShelfBase ');
    buffer.writeln('{');
    buffer.writeln('}');
  }
}
