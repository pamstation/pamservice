library sci.api.shelf.gen;

import 'dart:io';
import 'dart:async';
import 'dart:convert';
import 'package:path/path.dart' as path;

import '../../api_lib.dart';
import '../../api_gen.dart';

part './gen/shelf_gen.dart';

class ShelfGenerator extends Generator {
  ApiLibrary modelApiLib;
  String modelLibraryName;

  ShelfGenerator(
      ApiLibrary apiLib,
      this.modelApiLib,
      String directory,
      String libraryName,
      String baseClassName,
      this.modelLibraryName,
      List<Map<String, String>> importLibs,
      List<Map<String, String>> exportLibs)
      : super(apiLib, directory, libraryName, baseClassName, importLibs,
            exportLibs) {
    classFolder = 'src/shelf/impl';
    baseFolder = 'src/shelf/base';
  }

  run() async {
    process();
    await Process.run('dartfmt', ['-w', directory],
            workingDirectory: Directory.current.path)
        .then((result) {
      stdout.write(result.stdout);
      stderr.write(result.stderr);
    });
  }

  void process() {
    new Directory(path.join(directory, classFolder))
        .createSync(recursive: true);
    new Directory(path.join(directory, baseFolder)).createSync(recursive: true);

    generateBaseLibrary(false);
    generateLibrary();

//    apiLib.classes.values.forEach(generateBase);
//    apiLib.classes.values.forEach(generateClass);

    generateShelfBase();
    generateShelf();
  }

  void generateShelfBase() {
    var filename = path.join(directory, baseFolder, 'shelf.dart');
    var file = new File(filename);
    var buffer = new StringBuffer();

    PartOfBaseGen.process(this, libraryName, buffer);

    ShelfBaseClassDeclGen.process(this, apiLib, baseClassName, buffer);

    file.writeAsStringSync(buffer.toString());
  }

  void generateShelf() {
    var filename = path.join(directory, classFolder, 'shelf.dart');
    var file = new File(filename);
    if (file.existsSync()) return;
    var buffer = new StringBuffer();

    PartOfGen.process(this, libraryName, buffer);

    ShelfClassDeclGen.process(this, apiLib, baseClassName, buffer);

    file.writeAsStringSync(buffer.toString());
  }

  void generateBaseLibrary(bool isModel) {
    var filename = path.join(directory, '${libraryName}_base.dart');
    var file = new File(filename);
    var buffer = new StringBuffer();
    buffer.writeln('library ${libraryName}_base;');
    buffer.writeln("import 'dart:io' as io;");
    buffer.writeln("import 'dart:async';");
    buffer.writeln("import 'dart:convert';");
    buffer.writeln("import 'package:mime/mime.dart';");
    buffer.writeln(
        "import 'package:web_socket_channel/web_socket_channel.dart';");
    buffer.writeln("import 'package:shelf_web_socket/shelf_web_socket.dart';");

    buffer.writeln("import 'package:tson/tson.dart' as TSON;");

    buffer.writeln("import 'package:shelf/shelf.dart' as shelf;");
    buffer.writeln("import 'package:sci_api_gen/sci_service.dart' as service;");
    buffer.writeln("import 'package:sci_api_gen/sci_shelf_base.dart';");
    buffer.writeln("import 'sci_shelf.dart';");
    buffer.writeln("import 'sci_service.dart';");
    buffer.writeln("import 'sci_model.dart';");
    buffer.writeln("import 'sci_model_base.dart';");
    buffer.writeln("import 'package:sci_util/src/util/error/error.dart';");
    buffer.writeln(
        "import 'package:sci_util/src/util/codec/content_codec.dart';");

    buffer.writeln("part '${path.join(baseFolder, 'shelf.dart')}';");

    file.writeAsStringSync(buffer.toString());
  }

  void generateLibrary() {
    var filename = path.join(directory, '${libraryName}.dart');
    var file = new File(filename);
    var buffer = new StringBuffer();

    buffer.writeln('library ${libraryName};');

    buffer.writeln("import 'dart:io' as io;");
    buffer.writeln("import 'dart:async';");
    buffer.writeln("import 'dart:convert';");
    buffer.writeln("import 'package:mime/mime.dart';");

    buffer.writeln("import 'package:shelf/shelf.dart' as shelf;");

    buffer.writeln("import 'package:sci_api_gen/sci_service.dart' as service;");
    buffer.writeln("import 'package:sci_api_gen/sci_shelf_base.dart';");
    buffer.writeln("import 'sci_shelf.dart';");
    buffer.writeln("import 'sci_service.dart';");
    buffer.writeln("import 'sci_model.dart';");
    buffer.writeln("import 'sci_model_base.dart';");
    buffer.writeln("import 'sci_shelf_base.dart';");
    buffer.writeln("import 'package:sci_util/src/util/error/error.dart';");

    buffer.writeln("part '${path.join(classFolder, 'shelf.dart')}';");

    file.writeAsStringSync(buffer.toString());
  }

  void generateClass(ApiClass clazz) {
    var filename =
        path.join(directory, classFolder, '${filenameForClass(clazz)}');
    var file = new File(filename);
    if (file.existsSync()) return;
    var buffer = new StringBuffer();
    PartOfGen.process(this, libraryName, buffer);
    ClassDeclGen.process(this, clazz, baseClassName, buffer);

    EndClassGen.process(this, clazz, buffer);
    file.writeAsStringSync(buffer.toString());
  }
}
