library sci.api.gen;

import 'dart:io';
import 'dart:async';
import 'package:path/path.dart' as path;

import 'api_lib.dart';

part './src/gen/partof_gen.dart';
part './src/gen/class_decl_gen.dart';
part './src/gen/end_class_gen.dart';
part './src/gen/lib_gen.dart';

abstract class Generator {
  ApiLibrary apiLib;
  String directory;
  String libraryName;
  String baseClassName;
  List<Map<String, String>> importLibs;
  List<Map<String, String>> exportLibs;
  String classFolder;
  String baseFolder;

  Generator(this.apiLib, this.directory, this.libraryName, this.baseClassName,
      this.importLibs, this.exportLibs);

  String filenameForClass(ApiClass clazz, {String extension: 'dart'}) {
    Iterable<String> chars = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ'
        .codeUnits
        .map((code) => new String.fromCharCode(code));
    var filename = clazz.name;
    chars.forEach((char) {
      filename = filename.replaceAll(char, '_${char.toLowerCase()}');
    });
    if (filename.startsWith('_')) filename = filename.substring(1);
    return '${filename}.${extension}';
  }

  String filenameForBaseClass(ApiClass clazz, {String extension: 'dart'}) => filenameForClass(clazz, extension:extension);

  void generateLibrary() {
    var filename = path.join(directory, '${libraryName}.dart');
    var file = new File(filename);
    var buffer = new StringBuffer();

    LibGen.process(this, libraryName, importLibs, classFolder, buffer,
        exportLibs: exportLibs);

    file.writeAsStringSync(buffer.toString());
  }



  Future run();
}
