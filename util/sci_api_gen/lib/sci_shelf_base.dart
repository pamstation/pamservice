import 'dart:async';
import 'dart:convert';
import 'dart:io';
import 'package:tson/tson.dart' as TSON;
import 'package:shelf/shelf.dart' as shelf;
import 'sci_service.dart';
import 'package:logging/logging.dart';
import 'package:stack_trace/stack_trace.dart';
import 'sci_service.dart' as service;

import 'package:web_socket_channel/web_socket_channel.dart';
import 'package:web_socket_channel/io.dart';
import 'package:web_socket_channel/status.dart' as status;
import 'package:sci_util/src/util/error/error.dart';
import 'package:sci_util/src/util/codec/content_codec.dart';

abstract class TokenService {
  Future<String> getToken(String auth);
}

abstract class ShelfService {
  static Logger logger = new Logger("ShelfService");

  ContentCodec contentCodec = new TsonContentCodec();

//   Map<String, String> get textHeader => {'content-type': 'text/plain'};

  Future<shelf.Response> process(shelf.Request request, AclContext aclContext);

  Future<AclContext> getAclContext(shelf.Request request) async {
    var watch = new Stopwatch()..start();
    String auth;

    if (request.headers.containsKey('authorization')) {
      auth = request.headers["authorization"];
    } else if (request.url.queryParameters.containsKey('authorization')) {
      auth = request.url.queryParameters["authorization"];
    }

    var aclContext = await new AclContextFactory().fromAuthorization(auth);
    if (logger.isLoggable(Level.FINEST))
      logger.finest('getAclContext -- ${watch.elapsedMilliseconds} ms');
    return aclContext;
  }

  Future<AclContext> getAclContext2(HttpRequest request) async {
    String auth;

    if (request.headers['authorization'] != null) {
      auth = request.headers["authorization"].first;
    } else if (request.requestedUri.queryParameters
        .containsKey('authorization')) {
      auth = request.requestedUri.queryParameters["authorization"];
    }

    return new AclContextFactory().fromAuthorization(auth);
  }

//  int globalCount = 0;

  addResultStreamToWebSocket(Stream result, WebSocketChannel webSocket,
      [String path]) async {
    await webSocket.sink.addStream(result);

    // hack
    // behind nginx connection closed by the server cause missing data on heatmap
    // see : HttpClientService.webSocketStream
    webSocket.sink.add(TSON.encode({'kind': 'websocketdone'}));

    webSocket.stream.listen((evt) {
      if (evt == '__ping__') return;
      try {
        var obj = TSON.decode(evt);
        if (obj is Map && obj['kind'] == 'websocketdone') {
          webSocket.sink.close(1000, '');
        }
      } catch (e, st) {
        logger.severe('addResultStreamToWebSocket', e, st);
      }
    });
  }

  addResultStreamToWebSocket_old(Stream result, WebSocketChannel webSocket,
      [String path]) async {
    StreamSubscription sub;
    StreamSubscription timeoutSub;

//    var gcount = globalCount++;
//    var count = 0;

    sub = result.listen((event) {
//      count++;
//      logger.severe('addResultStreamToWebSocket -- count $gcount-$count');

      if (webSocket.closeCode != null) {
        sub.cancel();
        timeoutSub?.cancel();

//        logger.severe('addResultStreamToWebSocket -- webSocket.closeCode != null');
      } else {
        webSocket.sink.add(event);
      }
    }, onError: (e) {
//      logger.severe('addResultStreamToWebSocket -- onError', e);
      timeoutSub?.cancel();
      webSocket.sink
          .add(contentCodec.encode(new ServiceError.fromError(e).toJson()));
      webSocket.sink.close(1000, '');
    }, onDone: () {
//      logger.severe('addResultStreamToWebSocket -- onDone');
      timeoutSub?.cancel();

      // not sure why we need this ?
      // if we close now some data are missing !!!
      new Timer(new Duration(seconds: 1), () {
        webSocket.sink.close(1000, '');
      });
    });

    // we need this because of https://github.com/dart-lang/sdk/issues/25536
    // see: sci_client_base.dart
    // Stream webSocketStream(Uri uri, Map params, decode(object))
    timeoutSub = await webSocket.stream
        .timeout(new Duration(seconds: 5))
        .handleError((e) async {
//      logger.severe('addResultStreamToWebSocket -- timeoutSub handleError', e);
      if (webSocket.closeCode == null) {
//        logger.severe('addResultStreamToWebSocket -- timeoutSub webSocket.closeCode == null');
        webSocket.sink.close(1000, '');
      }

      await sub?.cancel();
    }).drain();
  }
//  shelfRoute();
//WebSocketChannel

  wsRequestHandler(HttpRequest request, CompressionOptions compression) async {
    var aclContext = await getAclContext2(request);
    var queryParameters = request.uri.queryParameters;
    var path = request.uri.path;

    WebSocket webSocket =
        await WebSocketTransformer.upgrade(request, compression: compression);

    wsHandler(
        path, new IOWebSocketChannel(webSocket), aclContext, queryParameters);
  }

  wsHandler(String path, WebSocketChannel webSocket,
      service.AclContext aclContext, Map queryParameters) {
//    print('webSocketHandler ${webSocket}');
//
//    webSocket.sink.add('{"kind":"DoneState"}');
//    webSocket.sink.add('{"kind":"DoneState"}');
//    webSocket.sink.add('{"kind":"DoneState"}');
//    webSocket.sink.add('{"kind":"DoneState"}');
    webSocket.sink.close(status.goingAway);
  }

  Future<shelf.Response> handler(shelf.Request request) async {
    shelf.Response response;

    try {
      var aclContext = await getAclContext(request);
      response = await process(request, aclContext);
    } on ServiceError catch (e, st) {
      var trace = new Trace.from(st).terse;
      logger.severe('onError', e, trace);

      response = new shelf.Response(e.statusCode,
          headers: contentCodec.contentTypeHeader,
          body: contentCodec.encodeStream(e?.toJson()));
    } catch (e, st) {
      var trace = new Trace.from(st).terse;
      logger.severe('onError', e, trace);

      response = new shelf.Response(500,
          headers: contentCodec.contentTypeHeader,
          body: contentCodec.encodeStream(
              new ServiceError(500, 'unknown', e.toString()).toJson()));
    }
    return response;
  }

  Future<shelf.Response> processService(
      shelf.Request request, Service service, AclContext aclContext) async {
    var path = request.requestedUri.path;
    switch (request.method) {
      case 'GET':
        return processGetService(request, service, aclContext);
      case 'POST':
        {
          shelf.Response resp;
          if (path.endsWith("list")) {
            resp = await processListService(request, service, aclContext);
          } else {
            resp = await processPostService(request, service, aclContext);
          }
          return resp;
        }
      case 'PUT':
        return processPutService(request, service, aclContext);
      case 'DELETE':
        return processDeleteService(request, service, aclContext);
      case 'PATCH':
        return processPatchService(request, service, aclContext);
      default:
        return new shelf.Response(500,
            headers: contentCodec.contentTypeHeader,
            body: contentCodec.encodeStream(
                new ServiceError.bad('process.service', 'bad request')
                    .toJson()));
    }
  }

  Future<shelf.Response> processPatchService(
      shelf.Request request, Service service, AclContext aclContext) async {
    return new shelf.Response(500,
        headers: contentCodec.contentTypeHeader,
        body: contentCodec.encodeStream(
            new ServiceError.bad('process.patch.service', 'not impl')
                .toJson()));
  }

  Future<shelf.Response> processGetService(
      shelf.Request request, Service service, AclContext aclContext) async {
    var id = request.requestedUri.queryParameters["id"];

    var result;

    var useFactory =
        request.requestedUri.queryParameters["useFactory"] == 'true'
            ? true
            : false;

    if (request.headers.containsKey('If-None-Match')) {
      result = await service.get(id,
          useFactory: useFactory,
          aclContext: aclContext,
          ifNoneMatchRev: request.headers['If-None-Match']);

      if (result == null) {
        return new shelf.Response(304);
      }
    } else {
      result =
          await service.get(id, useFactory: useFactory, aclContext: aclContext);
    }

    var headers = new Map<String, String>.from(contentCodec.contentTypeHeader);

    headers['etag'] = result.rev as String;

    return new shelf.Response(200,
        headers: headers, body: contentCodec.encodeStream(result?.toJson()));
  }

  Future<shelf.Response> processListService(
      shelf.Request request, Service service, AclContext aclContext) async {
    List<String> ids = new List<String>.from(await contentCodec.decodeStream(
        request.read(), request.encoding) as Iterable);
    var useFactory =
        request.requestedUri.queryParameters["useFactory"] == 'true'
            ? true
            : false;
    var objects =
        await service.list(ids, useFactory: useFactory, aclContext: aclContext);
    return new shelf.Response(200,
        headers: contentCodec.contentTypeHeader,
        body: contentCodec
            .encodeStream(objects.map((each) => each?.toJson()).toList()));
  }

  Future<shelf.Response> processPostService(
      shelf.Request request, Service service, AclContext aclContext) async {
    var object = service.fromJson(await contentCodec.decodeStream(
        request.read(), request.encoding) as Map);
    var rev = await service.update(object, aclContext: aclContext);
    return new shelf.Response(200,
        headers: contentCodec.contentTypeHeader,
        body: contentCodec.encodeStream([rev]));
  }

  Future<shelf.Response> processPutService(
      shelf.Request request, Service service, AclContext aclContext) async {
    var object = service.fromJson(await contentCodec.decodeStream(
        request.read(), request.encoding) as Map);
    var result = await service.create(object, aclContext: aclContext);
    return new shelf.Response(200,
        headers: contentCodec.contentTypeHeader,
        body: contentCodec.encodeStream(result?.toJson()));
  }

  Future<shelf.Response> processDeleteService(
      shelf.Request request, Service service, AclContext aclContext) async {
    var id = request.requestedUri.queryParameters["id"];
    var rev = request.requestedUri.queryParameters["rev"];
    await service.delete(id, rev, aclContext: aclContext);
    return new shelf.Response(200);
  }
}
