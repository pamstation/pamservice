library sci.service;

import 'dart:async';
import 'dart:convert';
import 'package:logging/logging.dart';
import 'package:path/path.dart' as pathlib;
import 'package:sci_util/src/util/config/config.dart';

import 'package:sci_util/src/util/error/error.dart';
import 'package:sci_util/src/util/path/long_path.dart' as lpath;

export 'package:sci_util/src/util/config/config.dart';

part 'src/acl/acl.dart';
part 'src/acl/impl/acl_impl.dart';

String value2LongPath(String value) => lpath.longPath(value);

//String getParent(String path) {
//
//  if (!path.startsWith('/')) throw 'relative path cannot get parent directory';
//  var list = path.split('/');
//
//  list.removeLast();
//  var sb = new StringBuffer('/');
//  list.forEach((each) {
//    sb..write(each)..write('/');
//  });
//  return sb.toString();
//}



class ServiceEvent {}

//class UpdateServiceEvent extends ServiceEvent {
//  String id;
//  String rev;
//}

class CreatedServiceEvent extends ServiceEvent {
  String id;
  String rev;
}

class DeleteServiceEvent extends ServiceEvent {
  String id;
  String rev;
}

abstract class Service<T> {
  Config get config;
  //relative uri
  Uri get uri;
  //full path uri
  Uri getServiceUri(Uri uri) => null;
  String get serviceName;
  Future<T> create(T object, {AclContext aclContext});
  Future<T> get(String id, {bool useFactory: true, AclContext aclContext, String ifNoneMatchRev});
  Future<List<T>> list(List<String> ids,
      {bool useFactory: true, AclContext aclContext});
  Future<String> update(T object, {AclContext aclContext});
  Future delete(String id, String rev,
      {AclContext aclContext, bool force: false});
  Future<String> patch(PatchCommands commands, {AclContext aclContext});
  T fromJson(Map m, {bool useFactory: true});
  Map toJson(T object);
}

class PatchCommands {
  final String id;
  final String rev;
  final List<PatchCommand> commands;

  PatchCommands(this.id, this.rev, this.commands);

  factory PatchCommands.fromJson(Map m) {
    var commands = (m['commands'] as List)
        .map((each) => new PatchCommand.fromJson(each as Map))
        .toList();
    return new PatchCommands(m['id'] as String, m['rev'] as String, commands);
  }

  Map toJson() => {
        'id': id,
        'rev': rev,
        'commands': commands.map((each) => each.toJson()).toList()
      };
}

class PatchCommand {
  final String path;
  final dynamic value;

  PatchCommand(this.path, this.value);

  factory PatchCommand.fromJson(Map m) =>
      new PatchCommand(m['path'] as String, m['value']);

  Map toJson() => {'path': path, 'value': value};
}
