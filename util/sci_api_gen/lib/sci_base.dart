library sci_changed;

import 'dart:async';
import "dart:collection";
import 'package:sci_util/src/util/value/value.dart';

import 'package:logging/logging.dart';

//abstract class ServiceDocumentObject {
//  String get id;
//
//}

class ChangedEvent<T extends EventSource> {
  final T source;
  ChangedEvent(this.source);
}

class UpdateEvent<T extends EventSource> extends ChangedEvent<T> {
  UpdateEvent(T source) : super(source);
}

class ViewChangedEvent<T extends EventSource> extends ChangedEvent<T> {
  ViewChangedEvent(T source) : super(source);
}

class PropertyChangedEvent<T extends EventSource> extends ChangedEvent<T> {
  final String propertyName;
  final Object oldValue;
  final Object value;
  Object emitter;
  PropertyChangedEvent(T source, this.propertyName, this.oldValue, this.value)
      : super(source);

  String toString() =>
      '${this.runtimeType}(${source}, ${emitter}, ${propertyName}, ${oldValue}, ${value})';
}

class ListPropertyChangedEvent<T extends EventSource>
    extends PropertyChangedEvent<T> {
  PropertyChangedEvent sourceEvent;

  ListPropertyChangedEvent.index(
      T source, PropertyChangedEvent sourceEvent, int index)
      : super(source, '@${index}/${sourceEvent.propertyName}',
            sourceEvent.oldValue, sourceEvent.value) {
    this.sourceEvent = sourceEvent;
  }

  ListPropertyChangedEvent(this.sourceEvent, T source, String propertyName,
      Object oldValue, Object value)
      : super(source, propertyName, oldValue, value);
}

//class ValueChangedEvent extends PropertyChangedEvent {
//  final Object oldValue;
//  final Object value;
//  ValueChangedEvent(String propertyName, this.oldValue, this.value) : super(source, propertyName);
//}

abstract class ListChangedEvent<T extends EventSource> extends ChangedEvent<T> {
  ListChangedEvent(T source) : super(source);
  String toString() => '${this.runtimeType}(${source})';
}

class ListClearChangedEvent<T extends EventSource> extends ListChangedEvent<T> {
  ListClearChangedEvent(T source) : super(source);
}

class ListInsertChangedEvent<T extends EventSource>
    extends ListChangedEvent<T> {
  Object element;
  int index;
  ListInsertChangedEvent(T source, this.index, this.element) : super(source);
  String toString() => '${this.runtimeType}(${source}, ${index}, ${element} )';
}

class ListAddChangedEvent<T extends EventSource> extends ListChangedEvent<T> {
  List<Object> elements;
  ListAddChangedEvent(T source, this.elements) : super(source);
  String toString() => '${this.runtimeType}(${source}, ${elements} )';
}

class ListRemoveChangedEvent<T extends EventSource>
    extends ListChangedEvent<T> {
  List<int> indexes;
  ListRemoveChangedEvent(T source, this.indexes) : super(source);
  String toString() => '${this.runtimeType}(${source}, ${indexes} )';
}

class ListElementChangedEvent<T extends EventSource> extends ChangedEvent<T> {
  ChangedEvent event;
  ListElementChangedEvent(T source, this.event) : super(source);

  ChangedEvent get sourceEvent {
    var evt = event;
    while (evt is ListElementChangedEvent) {
      evt = (evt as ListElementChangedEvent).event;
    }
    return evt;
  }
}

class Mixin {}

class ValueBase<T> extends Value<T> {
  StreamSubscription _sub;
  final Base<T> base;
  final String propertyName;

  ValueBase(this.base, this.propertyName);

  void onListen() {
    if (_sub != null) return;
    _sub = base.onChange.listen((evt) {
      if (evt is PropertyChangedEvent && evt.propertyName == propertyName) {
        sendChangeEvent();
      }
    });
  }

  void onCancel() {
    _sub.cancel();
    _sub = null;
  }

  T get value => base.get(propertyName);
  set value(T v) {
    base.set(propertyName, v);
  }

  Future release() async {
    await _sub?.cancel();
    return super.release();
  }
}

abstract class Base<T> extends EventSource implements ObjectProperties<T> {
  String subKind;
  Base();
  Base.json(Map m);

  String get kind;

  T get(String name) => throw new ArgumentError.value(name);
  void set(String name, T value) => throw new ArgumentError.value(name);

  Value<T> getPropertyAsValue(String name) => new ValueBase(this, name);

  String getKind() {
    if (subKind != null) return subKind;
    return kind;
  }

  List<String> getPropertyNames() => [];

  void onChildChanged(ChangedEvent evt) {
    if (!EventSource.LISTEN) return;
    if (this._noEvent) return;

    ChangedEvent new_evt = evt;

    if (evt is PropertyChangedEvent) {
      var source = evt.source;
      var emitter = evt.emitter;
      if (emitter == null) emitter = source;

      var name = this
          .getPropertyNames()
          .firstWhere((n) => this.get(n) == source, orElse: () => null);

//      if (name == null) {
//        print(this);
//      }

      assert(name != null);

      new_evt = new PropertyChangedEvent(
          this, '${name}/${evt.propertyName}', evt.oldValue, evt.value)
        ..emitter = emitter;
    }
    this.sendChangeEvent(new_evt);
  }
}

abstract class PersistentBase {
  String id;
  String rev;
  bool isDeleted;
  dynamic toJson();
}

//abstract class Node<P extends Node> implements EventSource {
//abstract class Node<P> implements EventSource {
//  P getParentNode() {
//    return this.getParent() as P;
//  }
//}

//extends Object with SubscriptionHelper
abstract class EventSource extends Object {
  static Logger logger = new Logger('EventSource');
  static bool LISTEN = true;
  static StreamController<ChangedEvent> NULL_STREAM_CONTROLLER =
      new StreamController.broadcast(sync: true);
  dynamic toJson() => {};


  Logger getLogger()=>logger;

  bool _noEvent = false;
  EventSource _parent;

  EventSource get parent => _parent;
  set parent(EventSource p) {
    _parent = p;
  }

  EventSource getParent() {
    if (parent == null) return null;
    if (parent is ListChanged || parent is ListChangedBase)
      return parent.parent;
    else
      return parent;
  }

  bool get hasListener =>
      (LISTEN && !_noEvent) &&
      ((__changedController != null && __changedController.hasListener) ||
          (_parent != null && _parent.hasListener));

  void onChildChanged(ChangedEvent evt) {}

  bool get noEvent => _noEvent;

  void noEventDo(void fun()) {
    if (!EventSource.LISTEN) return fun();
    var old = this._noEvent;
    this._noEvent = true;
    try {
      fun();
    } finally {
      this._noEvent = old;
    }
  }

  StreamController<ChangedEvent> __changedController;

  StreamController<ChangedEvent> get _changedController {
    if (!EventSource.LISTEN) return null;
    if (__changedController == null)
      __changedController = new StreamController.broadcast(sync: true);
    return __changedController;
  }

  Stream<ChangedEvent> get onChange =>
      _changedController == null ? null : _changedController.stream;

  void sendChangeEvent(ChangedEvent evt) {
    if (!EventSource.LISTEN) return;
    if (this._noEvent) return;

    if (_parent != null) {
      _parent.onChildChanged(evt);
    }

    if (__changedController == null) return;
    _changedController.add(evt);
  }
}

class ListChangedBase<T> extends ListBase<T> with EventSource {
  List<T> _list;

  ListChangedBase([List<T> list]) {
    list = list == null ? [] : list;
    _list = new List.from(list, growable: true);
  }

  T get first => _list.first;
  int get length => _list.length;
  bool get isEmpty => _list.isEmpty;
  set length(int i) => throw 'ListChanged : length not implemented';
  T operator [](int i) => _list[i];
  operator []=(int i, T o) {
    throw 'ListChanged : []= not implemented';
  }

  @override
  Iterable<T> where(bool test(T element)) => this._list.where(test);

  @override
  Iterable<E> map<E>(E f(T e)) => this._list.map(f);

//  Iterable/*<E>*/ map/*<E>*/(/*=E*/ f(T element)) => this._list.map(f);

  @override
  bool any(bool test(T element)) => this._list.any(test);

  @override
  void forEach(void action(T element)) => _list.forEach(action);

  @override
  List<T> toList({bool growable: true}) => _list.toList(growable: growable);

  void _basicAdd(T element) {
    _list.add(element);
  }

  void add(T element) {
    if (_list.contains(element)) return;

    _basicAdd(element);
    if (EventSource.LISTEN)
      this.sendChangeEvent(new ListAddChangedEvent(this, [element]));
  }

  void addAll(Iterable<T> l) {
    var elements = [];
    l.forEach((element) {
      if (!_list.contains(element)) ;
      _basicAdd(element);
      elements.add(element);
    });

    if (elements.isNotEmpty) {
      if (EventSource.LISTEN)
        this.sendChangeEvent(new ListAddChangedEvent(this, elements));
    }
  }

  void clear() {
    if (this.isEmpty) return;
    _list.clear();
    if (EventSource.LISTEN)
      this.sendChangeEvent(new ListClearChangedEvent(this));
  }

  void _basicRemove(Object element) {
    _list.remove(element);
  }

  bool remove(Object element) {

    var index = _list.indexOf(element as T);
    if (index < 0) return false;
    _basicRemove(element);
    if (EventSource.LISTEN)
      this.sendChangeEvent(new ListRemoveChangedEvent(this, [index]));
    return true;
  }

  void _basicInsert(int index, T element) {
    _list.insert(index, element);
  }

  void insert(int index, T element) {
    if (_list.contains(element)) return;
    _basicInsert(index, element);
    if (EventSource.LISTEN)
      this.sendChangeEvent(new ListInsertChangedEvent(this, index, element));
  }

  void insertAt(int index, T element, bool before) {
    var i = before ? index : index + 1;
    if (i >= this.length) {
      this.add(element);
    } else {
      this.insert(i, element);
    }
  }

  Object toJson() {
    return new List.from(_list);
  }
}

class ListChanged<T extends EventSource> extends ListBase<T> with EventSource {
  List<StreamSubscription> _subs;
  List<T> _list;

  ListChanged([List<T> list]) {
    list = list == null ? [] : list;
    _list = new List.from(list, growable: true);
    _list.forEach((each) => each.parent = this);
  }
  T get first => _list.first;
  int get length => _list.length;
  bool get isEmpty => _list.isEmpty;
  set length(int i) => throw 'ListChanged : length not implemented';
  T operator [](int i) => _list[i];
  operator []=(int i, T o) {
    throw 'ListChanged : []= not implemented';
  }

  Iterable<T> where(bool test(T element)) => this._list.where(test);

//  Iterable/*<E>*/ map/*<E>*/(/*=E*/ f(T element)) => this._list.map(f);
  @override
  Iterable<E> map<E>(E f(T e)) => this._list.map(f);

  bool any(bool test(T element)) => this._list.any(test);

  void forEach(void action(T element)) => _list.forEach(action);

  List<T> toList({bool growable: true}) => _list.toList(growable: growable);

  Future release() {
    return new Future.sync(() {
      if (_subs != null) {
        var subs = _subs;
        _subs = null;
        return Future.forEach(subs, (StreamSubscription sub) => sub.cancel());
      }
    });
  }

  void _basicAdd(T element) {
    _list.add(element);
    element.parent = this;
  }

  void add(T element) {
    if (_list.contains(element)) return;
    _basicAdd(element);

    if (EventSource.LISTEN)
      this.sendChangeEvent(new ListAddChangedEvent(this, [element]));
  }

  void addAll(Iterable<T> l) {
    var elements = [];
    l.forEach((element) {
      if (!_list.contains(element)) {
        _basicAdd(element);
        elements.add(element);
      }
    });

    if (elements.isNotEmpty) {
      if (EventSource.LISTEN)
        this.sendChangeEvent(new ListAddChangedEvent(this, elements));
    }
  }

  void clear() {
    if (this.isEmpty) return;
    _list.forEach((each) => each.parent = null);
    _list.clear();
    if (EventSource.LISTEN)
      this.sendChangeEvent(new ListClearChangedEvent(this));
  }

  bool remove(Object el) {
    var element = el as T;
    var index = _list.indexOf(element);
    if (index < 0) return false;
    _list.remove(element);

    element.parent = null;

    if (EventSource.LISTEN)
      this.sendChangeEvent(new ListRemoveChangedEvent(this, [index]));
    return true;
  }

  void _basicInsert(int index, T element) {
    _list.insert(index, element);
    element.parent = this;
  }

  void insert(int index, T element) {
    if (_list.contains(element)) return;
    _basicInsert(index, element);

    if (EventSource.LISTEN)
      this.sendChangeEvent(new ListInsertChangedEvent(this, index, element));
  }

  void insertAt(int index, T element, bool before) {
    var i = before ? index : index + 1;
    if (i >= this.length) {
      this.add(element);
    } else {
      this.insert(i, element);
    }
  }

  Object toJson() {
    return _list.map((e) => e.toJson()).toList();
  }

  void onChildChanged(ChangedEvent evt) {
    if (!EventSource.LISTEN) return;
    ChangedEvent new_evt = evt;
    if (evt is PropertyChangedEvent) {
      var emitter = evt.emitter;
      if (emitter == null) emitter = evt.source;
      var index = this._list.indexOf(evt.source as T);

      assert(index >= 0);

      new_evt = new ListPropertyChangedEvent.index(this, evt, index)
        ..emitter = emitter;
    } else {
      new_evt = new ListElementChangedEvent(this, evt);
    }
    this.sendChangeEvent(new_evt);
  }
}
