library sci.api.lib;

import 'dart:mirrors';

class ApiWebSocket {
  const ApiWebSocket();
}

class ApiRequestContentType {
  final String contentType;
  const ApiRequestContentType(this.contentType);
}

class ApiRequestVerb {
  final String verb;
  const ApiRequestVerb(this.verb);
}



class ApiResponseContentType {
  final String contentType;
  const ApiResponseContentType(this.contentType);
}

abstract class ApiUriResource {
  String get uri;
}

class ApiService implements ApiUriResource {
  final String uri;
  const ApiService({this.uri});
}

class ApiPersistentService implements ApiUriResource {
  final String className;
  final String uri;
  final String dbName;
  const ApiPersistentService({this.className, this.uri, this.dbName});
}

class CouchdbView {
  final String name;
  final String map;
  final String reduce;
  const CouchdbView({this.name, this.map, this.reduce: ''});
}

class ApiView implements ApiUriResource, CouchdbView {
  final String name;
  final String uri;
  final List<String> kinds;
  final List<String> startKeys;
  final List<String> keys;
  final String map;
  final String reduce;
  const ApiView(
      {this.name,
      this.uri,
      this.kinds,
      this.startKeys,
      this.keys,
      this.map,
      this.reduce: ''});

  List<String> getKeys() => startKeys == null ? keys : startKeys;
}

class ApiLibrary {
  String name;
  Map<String, ApiClass> classes;
  ApiLibrary(this.name) {
    classes = {};
  }

  void addClass(ApiClass clazz) {
    classes[clazz.name] = clazz;
  }

  Set<ApiProperty> get dataProperties => classes.values.fold(new Set(),
      (Set<ApiProperty> list, ApiClass each) => list..addAll(each.dataProperties));
  Set<ApiProperty> get objectProperties => classes.values.fold(new Set(),
      (Set<ApiProperty> list, ApiClass each) => list..addAll(each.objectProperties));
}

class ApiClass {
  ApiLibrary lib;
  String name;
  String superName;
  List<String> interfaces;
  List<ApiProperty> properties;
  List<ApiMethod> methods;

  List metadata;

  ApiClass(
      this.lib, this.name, this.superName, this.interfaces, this.metadata) {
    properties = [];
    methods = [];
  }

  void addProperty(ApiProperty p) {
    properties.add(p);
  }

  void addMethod(ApiMethod m) {
    methods.add(m);
  }

  bool get hasProperties => properties.isNotEmpty;

  List<ApiProperty> get dataProperties =>
      properties.where((each) => each.isData).toList();
  List<ApiProperty> get objectProperties =>
      properties.where((each) => !each.isData).toList();

  Set<ApiClass> get allSubClasses {
    var list = new Set<ApiClass>();
    this.subClasses.forEach((c) {
      list.addAll(c.allSubClasses);
    });
    list.addAll(this.subClasses);
    return list;
  }

  List<ApiClass> get subClasses {
    return this
        .lib
        .classes
        .values
        .where((c) => c.superName == this.name)
        .toList();
  }

  List<ApiClass> get parentClasses {
    List<ApiClass> list = [];
    var c = subClassOf;
    while (c != null) {
      list.add(c);
      c = c.subClassOf;
    }
    return list;
  }

  ApiClass get subClassOf {
    return this
        .lib
        .classes
        .values
        .firstWhere((c) => c.name == this.superName, orElse: () => null);
  }

  String toString() {
    return '${this.runtimeType}(${this.name}, ${this.superName}, ${properties})';
  }
}

class ApiProperty {
  ApiClass clazz;
  String name;
  String range;
  bool isFunctional;

  List metadata;

  ApiProperty(
      this.clazz, this.name, this.range, this.isFunctional, this.metadata) {
//    if (range == 'Object') throw 'bad range : range == Object on class ${clazz.name} property ${name}';
  }

  List<ApiProperty> get parentProperties => this.clazz.parentClasses.fold(
      [],
      (list, c) => list
        ..addAll(c.dataProperties.where((p) => p.name == this.name))
        ..addAll(c.objectProperties.where((p) => p.name == this.name)));

  bool get hasParentProperty => parentProperties.isNotEmpty;

  bool get isData => clazz.lib.classes[range] == null;

  String toString() {
    return '${this.runtimeType}($name, $range, $isFunctional)';
  }

  @override
  int get hashCode => name.hashCode;

  @override
  bool operator ==(other) {
    if (other is! ApiProperty) return false;
    return name == other.name &&
        range == other.range &&
        isFunctional == other.isFunctional;
  }
}

class ApiMethod {
  ApiClass clazz;
  bool isConstructor;
  String name;

  String get returnType => returnApiType.name;
  List<String> get returnTypes =>
      returnApiType.templateTypes.map((each) => each.name).toList();
  List<ApiArgument> arguments;

  ApiType returnApiType;

  List metadata;

  ApiMethod(this.clazz, this.name, this.isConstructor, this.metadata) {
    arguments = [];
  }

  String get returnRange => returnTypes.isEmpty
      ? returnType
      : returnType + '<' + returnTypes.join(',') + '>';

  void addArgument(ApiArgument a) {
    arguments.add(a);
  }
}

class ApiType {
  String name;
  List<ApiType> templateTypes;

  ApiType();
  ApiType.from(TypeMirror typeMirror) {
    name = MirrorSystem.getName(typeMirror.simpleName);
    templateTypes = typeMirror.typeArguments
        .map((templateTypeMirror) => new ApiType.from(templateTypeMirror))
        .toList();
  }

  String get returnRange {
    var sb = new StringBuffer();

    sb.write(name);
    if (templateTypes.isNotEmpty) {
      sb.write('<');
      sb.write(templateTypes.map((each) => each.returnRange).join(','));
      sb.write('>');
    }
    return sb.toString();
  }
}

class ApiArgument {
  String name;

  String rangeType;
  List<String> rangeTypes;

  ApiArgument(this.name, this.rangeType, this.rangeTypes);

  String get range => rangeTypes.isEmpty
      ? rangeType
      : rangeType + '<' + rangeTypes.join(',') + '>';
}
