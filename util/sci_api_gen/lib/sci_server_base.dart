//import 'dart:async';
//import 'dart:convert';
//import 'package:http_client/src/channel/multiplex_channel.dart';
//
//import 'sci_service.dart';
//import 'package:logging/logging.dart';
//import 'package:stack_trace/stack_trace.dart';
//
//Logger logger = new Logger("PersistentServiceHandler");
//
//abstract class TokenService {
//  Future<String> getToken(String auth);
//}
//
//class AclHolder {
//  AclContext aclContext;
//}
//
//class ServiceHandler extends RequestHandler {
//  Service service;
//  AclHolder aclHolder;
//
//  @override
//  void close() {}
//  @override
//  Future<ChannelServerResponse> request(ChannelServerRequest request) async {
//    throw 'not impl';
//  }
//}
//
//class PersistentServiceHandler extends ServiceHandler {
////  List<RequestHandler> createHandlers(Service service) {
////    List<RequestHandler> list = [
////      new CreatePersistentServiceHandler(),
////      new GetPersistentServiceHandler(),
////      new ListPersistentServiceHandler(),
////      new UpdatePersistentServiceHandler(),
////      new DeletePersistentServiceHandler(),
////      new PatchPersistentServiceHandler()
////    ];
////
////    list.forEach((each) => each.service = service);
////
////    return list;
////  }
//}
//
//class CreatePersistentServiceHandler extends PersistentServiceHandler {
//  @override
//  Future<ChannelServerResponse> request(ChannelServerRequest request) async {
//    var listParams = await request.stream.toList();
//    if (listParams.isEmpty) throw 'empty request';
//    var param = listParams.first;
//    logger.fine('request : param ${param}');
//
//    var object = service.fromJson(param);
//    var result = await service.create(object, aclContext: aclHolder.aclContext);
//
//    return new ChannelServerResponse(
//        new Stream.fromIterable([service.toJson(result)]));
//  }
//}
//
//class GetPersistentServiceHandler extends PersistentServiceHandler {
//  @override
//  Future<ChannelServerResponse> request(ChannelServerRequest request) async {
//    var listParams = await request.stream.toList();
//    if (listParams.isEmpty) throw 'empty request';
//    var params = listParams.first;
//
//    var id = params[0] as String;
//    var useFactory = params[1] as bool;
//
//    var result = await service.get(id,
//        useFactory: useFactory, aclContext: aclHolder.aclContext);
//
//    return new ChannelServerResponse(
//        new Stream.fromIterable([result.toJson()]));
//  }
//}
//
//class ListPersistentServiceHandler extends PersistentServiceHandler {
//  @override
//  Future<ChannelServerResponse> request(ChannelServerRequest request) async {
//    var listParams = await request.stream.toList();
//    if (listParams.isEmpty) throw 'empty request';
//    var params = listParams.first;
//
//    var ids = params[0] as List<String>;
//    var useFactory = params[1] as bool;
//
//    var objects = await service.list(ids,
//        useFactory: useFactory, aclContext: aclHolder.aclContext);
//
//    var list = objects.map((each) => each.toJson()).toList();
//    return new ChannelServerResponse(new Stream.fromIterable([list]));
//  }
//}
//
//class UpdatePersistentServiceHandler extends PersistentServiceHandler {
//  @override
//  Future<ChannelServerResponse> request(ChannelServerRequest request) async {
//    var listParams = await request.stream.toList();
//    if (listParams.isEmpty) throw 'empty request';
//    var params = listParams.first;
//    var object = service.fromJson(params[0] as Map);
//    var rev = await service.update(object, aclContext: aclHolder.aclContext);
//    return new ChannelServerResponse(new Stream.fromIterable([rev]));
//  }
//}
//
//class DeletePersistentServiceHandler extends PersistentServiceHandler {
//  @override
//  Future<ChannelServerResponse> request(ChannelServerRequest request) async {
//    var listParams = await request.stream.toList();
//    if (listParams.isEmpty) throw 'empty request';
//    var params = listParams.first;
//
//    var id = params[0] as String;
//    var rev = params[1] as String;
//
//    await service.delete(id, rev, aclContext: aclHolder.aclContext);
//
//    return new ChannelServerResponse(new Stream.fromIterable([]));
//  }
//}
//
//class PatchPersistentServiceHandler extends PersistentServiceHandler {}
//
//typedef Future ViewFunc(
//    {List startKey,
//    List endKey,
//    int limit,
//    int skip,
//    bool descending,
//    bool useFactory,
//    AclContext aclContext});
//
//class ViewServiceHandler extends PersistentServiceHandler {
//  ViewFunc func;
//  @override
//  Future<ChannelServerResponse> request(ChannelServerRequest request) async {
//    var listParams = await request.stream.toList();
//    if (listParams.isEmpty) throw 'empty request';
//    var params = listParams.first;
//
//    var startKey = params[0] as List;
//    var endKey = params[1] as List;
//    var limit = params[2] as int;
//    var skip = params[3] as int;
//    var descending = params[4] as bool;
//    var useFactory = params[5] as bool;
//
//    var list = await func(
//        startKey: startKey,
//        endKey: endKey,
//        limit: limit,
//        skip: skip,
//        descending: descending,
//        useFactory: useFactory,
//        aclContext: aclHolder.aclContext);
//
//    return new ChannelServerResponse(
//        new Stream.fromIterable([list.map((each) => each.toJson()).toList()]));
//  }
//}
//
//typedef Future ViewFunc2({List keys, bool useFactory, AclContext aclContext});
//
//class View2ServiceHandler extends PersistentServiceHandler {
//  ViewFunc2 func;
//  @override
//  Future<ChannelServerResponse> request(ChannelServerRequest request) async {
//    var listParams = await request.stream.toList();
//    if (listParams.isEmpty) throw 'empty request';
//    var params = listParams.first;
//
//    var keys = params[0] as List;
//    var useFactory = params[1] as bool;
//
//    var list = await func(
//        keys: keys, useFactory: useFactory, aclContext: aclHolder.aclContext);
//
//    return new ChannelServerResponse(
//        new Stream.fromIterable([list.map((each) => each.toJson()).toList()]));
//  }
//}
