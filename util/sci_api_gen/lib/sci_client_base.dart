import 'dart:async';
import "dart:convert";
import 'package:logging/logging.dart';
import 'package:stack_trace/stack_trace.dart';
import 'package:http_client/http_client.dart' as http;
import 'package:web_socket_channel/web_socket_channel.dart';
import 'package:sci_util/src/util/error/error.dart';
import 'package:sci_util/src/util/codec/content_codec.dart';
import 'package:tson/tson.dart' as TSON;

import './sci_base.dart' as base;
import 'sci_service.dart';

abstract class HttpClientService<T extends base.PersistentBase>
    extends Service<T> {
  static Logger logger = new Logger("HttpClientService");

  Uri baseRestUri;
  http.HttpClient client;

  ContentCodec contentCodec = new TsonContentCodec();

  StreamController<ServiceEvent> _controller;

  Logger getLogger() => logger;

  @override
  Config get config {
    return new Config();
  }

  Uri getServiceUri(Uri uri) =>
      http.HttpClient.ResolveUri(baseRestUri, uri.toString());

  Future initialize(Uri baseRestUri, http.HttpClient client) async {
    this.baseRestUri = baseRestUri;
    this.client = client;
    _controller = new StreamController.broadcast();
  }

  Stream<ServiceEvent> get onEvent => _controller.stream;

  void onResponseError(response) {
    var error;

    try {
      if (response.headers[http.ContentTypeHeaderValue.HEADER_CONTENT_TYPE] ==
          contentCodec.contentTypeHeader[
              http.ContentTypeHeaderValue.HEADER_CONTENT_TYPE]) {
        error = new ServiceError.fromJson(
            contentCodec.decode(response.body) as Map);
      } else {
        if (response.body is String) {
          throw new ServiceError(response.statusCode,
              "${this.serviceName}.client.unknown", response.body as String);
        } else if (response.body is List) {
          throw new ServiceError(
              response.statusCode,
              "${this.serviceName}.client.unknown",
              utf8.decode(response.body as List<int>));
        }
      }
    } catch (e) {
      if (response.body is String) {
        throw new ServiceError(response.statusCode,
            "${this.serviceName}.client.unknown", response.body as String);
      } else if (response.body is List) {
        throw new ServiceError(
            response.statusCode,
            "${this.serviceName}.client.unknown",
            utf8.decode(response.body as List<int>));
      }
    }
    throw error;
  }

  void onError(e, st) {
    var trace = new Trace.current().terse;
    logger.severe('onError', e, trace);

    if (e is http.HttpClientError) {
      throw new ServiceError(e.statusCode, e.error, e.reason);
    }

    throw new ServiceError(
        500, "${this.serviceName}.client.unknown", e.toString());
  }

//  Stream webSocketStream2(Uri uri, Map params, decode(object)) {
//    WebSocketChannel channel;
//
//    var scheme = getServiceUri(uri).scheme == 'http' ? 'ws' : 'wss';
//
//    var wsuri = getServiceUri(uri).replace(
//        scheme: scheme, queryParameters: {"params": json.encode(params)});
//
//    channel = client.webSocketChannel(wsuri);
//
//    return channel.stream.map((message) {
//      return decode(message);
//    });
//  }

  Stream<T> webSocketStream<T>(Uri uri, Map params, decode(object)) {
    WebSocketChannel channel;
    StreamController<T> controller;

    var scheme = getServiceUri(uri).scheme == 'http' ? 'ws' : 'wss';

    var wsuri = getServiceUri(uri).replace(
        scheme: scheme, queryParameters: {"params": json.encode(params)});

    Timer timer;
    StreamSubscription sub;

    controller = new StreamController<T>(
        sync: false,
        onListen: () {
          channel = client.webSocketChannel(wsuri);

//          channel.sink.add(contentCodec.encode(params));

          timer = new Timer.periodic(new Duration(seconds: 1), (t) {
            channel.sink.add('__ping__');
          });

          sub = channel.stream.listen((message) {
            var obj = TSON.decode(message);
            // hack
            // behind nginx connection closed by the server cause missing data on heatmap
            if (obj is Map && obj['kind'] == 'websocketdone') {
              controller.close();
              timer?.cancel();
              sub?.cancel();

              channel.sink.add(contentCodec.encode(obj));

              channel.sink.close(1000, '');
            } else {
              try {
                controller.add(decode(obj));
              } catch (e) {
                try {
                  controller.addError(new ServiceError.fromJson(obj as Map));
                } catch (ee) {
                  controller.addError(new ServiceError.fromError(ee));
                }

                sub?.cancel();
                controller.close();
                timer?.cancel();
                channel?.sink?.close(1000, '');
              }
            }
          }, onError: (e) {
            sub?.cancel();
            timer?.cancel();
            controller.addError(e);
          }, onDone: () {
            timer?.cancel();
            sub?.cancel();
            controller.close();
          }, cancelOnError: true);
        },
        onCancel: () {
          timer?.cancel();
          sub?.cancel();
          channel?.sink?.close(1000, '');
        });

    return controller.stream;
  }

  @override
  Future<T> create(T object, {AclContext aclContext}) async {
    T answer;
    try {
      var response = await client.put(getServiceUri(this.uri),
          body: contentCodec.encode(this.toJson(object)),
          responseType: contentCodec.responseType);
      if (response.statusCode != 200) {
        onResponseError(response);
      } else {
        answer = this.fromJson(contentCodec.decode(response.body) as Map);
      }
    } on ServiceError {
      rethrow;
    } catch (e, st) {
      onError(e, st);
    }

    sendCreatedEvent(answer.id, answer.rev);

    return answer;
  }

  void sendCreatedEvent(String id, [String rev]) {
    _controller.add(new CreatedServiceEvent()
      ..id = id
      ..rev = rev);
  }

  @override
  Future<String> rev(String id, {AclContext aclContext}) async {
    var doc = await get(id, aclContext: aclContext);
    return doc.rev;
  }

  @override
  Future<T> get(String id,
      {bool useFactory: true,
      AclContext aclContext,
      String ifNoneMatchRev}) async {
    T answer;
    try {
      var queryParameters = {'id': id};
      queryParameters['useFactory'] = useFactory.toString();

      var response = await client.get(
          getServiceUri(this.uri).replace(queryParameters: queryParameters),
          responseType: contentCodec.responseType);
      if (response.statusCode != 200) {
        onResponseError(response);
      } else {
        answer = this.fromJson(contentCodec.decode(response.body) as Map);
      }
    } on ServiceError {
      rethrow;
    } catch (e, st) {
      logger.severe('get', e, st);
      onError(e, st);
    }
    return answer;
  }

  @override
  Future<String> patch(PatchCommands commands, {AclContext aclContext}) async {
    throw 'not impl';
  }

  @override
  Future delete(String id, String rev,
      {AclContext aclContext, bool force: false}) async {
    try {
      var response = await client.delete(getServiceUri(this.uri)
          .replace(queryParameters: {'id': id, 'rev': rev}));
      if (response.statusCode != 200) {
        onResponseError(response);
      }
    } on ServiceError {
      rethrow;
    } catch (e, st) {
      onError(e, st);
    }

    _controller.add(new DeleteServiceEvent()
      ..id = id
      ..rev = rev);
  }

  @override
  Future<String> update(T object, {AclContext aclContext}) async {
    try {
      var response = await client.post(getServiceUri(this.uri),
          body: contentCodec.encode(this.toJson(object)),
          responseType: contentCodec.responseType);
      if (response.statusCode != 200) {
        onResponseError(response);
      } else {
        object.rev = contentCodec.decode(response.body)[0] as String;
      }
    } on ServiceError {
      rethrow;
    } catch (e, st) {
//      print(st);
      onError(e, st);
    }
    return object.rev;
  }

  @override
  Future<List<T>> list(List<String> ids,
      {AclContext aclContext, bool useFactory: true}) async {
    List<T> answer;
    try {
      var queryParameters = {'useFactory': useFactory.toString()};
      var response = await client.post(
          getServiceUri(this.uri).replace(
              path: getServiceUri(this.uri).path + '/list',
              queryParameters: queryParameters),
          body: contentCodec.encode(ids),
          responseType: contentCodec.responseType);
      if (response.statusCode != 200) {
        onResponseError(response);
      } else {
        answer = (contentCodec.decode(response.body) as List)
            .map((each) => this.fromJson(each as Map))
            .toList();
      }
    } on ServiceError {
      rethrow;
    } catch (e, st) {
      onError(e, st);
    }
    return answer;
  }

  Future<List<T>> findStartKeys(String viewName,
      {startKey,
      endKey,
      int limit: 20,
      int skip: 0,
      bool descending: true,
      bool useFactory: false,
      AclContext aclContext}) async {
    List<T> answer;
    try {
      var queryParameters = {'useFactory': useFactory.toString()};
      var response = await client.post(
          getServiceUri(this.uri).replace(
              path: getServiceUri(this.uri).path + '/${viewName}',
              queryParameters: queryParameters),
          body: contentCodec.encode({
            'startKey': startKey,
            'endKey': endKey,
            'limit': limit,
            'skip': skip,
            'descending': descending
          }),
          responseType: contentCodec.responseType);
      if (response.statusCode != 200) {
        onResponseError(response);
      } else {
        answer = (contentCodec.decode(response.body) as List)
            .map((each) => this.fromJson(each as Map))
            .toList();
      }
    } on ServiceError {
      rethrow;
    } catch (e, st) {
      onError(e, st);
    }
    return answer;
  }

  Future<List<T>> findKeys(String viewName,
      {List keys, bool useFactory: false, AclContext aclContext}) async {
    List<T> answer;
    try {
      var queryParameters = {'useFactory': useFactory.toString()};
      var response = await client.post(
          getServiceUri(this.uri).replace(
              path: getServiceUri(this.uri).path + '/${viewName}',
              queryParameters: queryParameters),
          body: contentCodec.encode(keys),
          responseType: contentCodec.responseType);
      if (response.statusCode != 200) {
        onResponseError(response);
      } else {
        answer = (contentCodec.decode(response.body) as List)
            .map((each) => each == null ? null : this.fromJson(each as Map))
            .toList();
      }
    } on ServiceError {
      rethrow;
    } catch (e, st) {
      onError(e, st);
    }
    return answer;
  }
}
