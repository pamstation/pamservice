import 'dart:async';
import 'package:logging/logging.dart';
import 'package:stack_trace/stack_trace.dart';
import 'package:sci_util/src/util/error/error.dart';
import './sci_base.dart' as base;

import "package:couch_client/couch_client.dart" as couch;

import 'sci_service.dart';

class DesignView {
  final String name;
  final String map;
  final String reduce;

  DesignView(this.name, this.map, this.reduce);

  factory DesignView.fromJson(Map m) => new DesignView(
      m['name'] as String, m['map'] as String, m['reduce'] as String);
}

abstract class CouchService<T extends base.PersistentBase> extends Service<T> {
  static Logger logger = new Logger("CouchService");
  static const DESIGN_DOC_NAME = "sciDesign";

  Config get config => new Config();
  String get dbName;
  couch.Database db;
  AccessManager accessManager;

  String get designDocName => DESIGN_DOC_NAME;
  List<DesignView> get views;

  Logger getLogger() => logger;

  void log(String message, [e, StackTrace st]) {
    logger.fine(message, e, st);
  }

  Future initialize(couch.CouchClient couchClient) async {
    db = couchClient.database(dbName);
    var doInitialize = config['tercen.couchdb.initialize'] == 'true';
    if (doInitialize) {
      if (!(await db.exists)) {
        logger.warning(
            "initialize : couch db ${db.name} does not exist, create it.");
        await db.create();
      }
      await _initializeQueryDesign();
    }
  }

  Future initialized() async {}

  Future _initializeQueryDesign() async {
    var design = this.db.design(designDocName);
    if (!(await design.exists)) {
      var designDoc = new couch.DesignDocument(design.name);
      views.forEach((view) {
        var viewDoc = new couch.DesignViewDocument(view.name)..map = view.map;
        if (view.reduce.isNotEmpty) viewDoc..reduce = view.reduce;
        designDoc.addView(viewDoc);
        logger.warning(
            "db ${db.name} add view ${view.name} in design doc ${design.name}");
      });
      logger.warning("db ${db.name} create design doc ${design.name}");

      await design.create(designDoc.toDocument());
    } else {
      var flag = false;
      couch.DesignDocument designDoc = await design.get();
      views.forEach((view) {
        if (!designDoc.hasView(view.name)) {
          flag = true;
          var viewDoc = new couch.DesignViewDocument(view.name)..map = view.map;
          if (view.reduce.isNotEmpty) viewDoc..reduce = view.reduce;
          designDoc.addView(viewDoc);
          logger.warning(
              "db ${db.name} add view ${view.name} in design doc ${design.name}");
        } else {
          var currentView = designDoc.getView(view.name);
          var reduce = currentView.reduce == null ? '' : currentView.reduce;
          if (currentView.map != view.map || reduce != view.reduce) {
            var viewDoc = new couch.DesignViewDocument(view.name)
              ..map = view.map;
            if (view.reduce.isNotEmpty) viewDoc..reduce = view.reduce;
            designDoc.addView(viewDoc);
            logger.warning(
                "db ${db.name} update view ${view.name} in design doc ${design.name}");
            flag = true;
          }
        }
      });

      if (flag) {
        logger.warning("db ${db.name} update design doc ${design.name}");
        await design.update(designDoc.toDocument());
      }
    }
  }

  void onError(e, StackTrace st) {
    if (logger.isLoggable(Level.FINE)) {
      logger.fine('onError', e, st);
    }

    if (e is couch.CouchClientException) {
      throw new ServiceError(
          e.statusCode, "${this.serviceName}.${e.error}", e.reason);
    } else if (e is ServiceError) {
      throw e;
    } else
      throw new ServiceError(500, "${this.serviceName}.unknown", e.toString());
  }

  @override
  Future<T> create(T object, {AclContext aclContext}) async {
    T answer;
    try {
      var canAccess = await this
          .accessManager
          .canAccessObjects(Privilege.READWRITE, aclContext, [object]);

      if (!canAccess)
        throw new ServiceError(
            401, "${this.serviceName}.create.forbidden", "Forbidden");

      answer = this.fromJson((await db
              .postDocument(new couch.Document.fromMap2(this.toJson(object))))
          .toJson2());
    } on ServiceError {
      rethrow;
    } catch (e, st) {
      onError(e, st);
    }
    return answer;
  }

  @override
  Future<T> get(String id,
      {AclContext aclContext,
      bool useFactory: true,
      String ifNoneMatchRev}) async {
    T object;
    try {
      var document =
          await this.db.getDocument(id, ifNoneMatchRev: ifNoneMatchRev);

      if (ifNoneMatchRev != null && document == null) {
        return null;
      }
      object = this.fromJson(document.toJson2(), useFactory: useFactory);
      var canAccess = await this
          .accessManager
          .canAccessObjects(Privilege.READ, aclContext, [object]);

      if (!canAccess)
        throw new ServiceError(
            401, "${this.serviceName}.get.forbidden", "Forbidden");
    } on ServiceError {
      rethrow;
    } catch (e, st) {
      onError(e, st);
    }
    return object;
  }

  Future<List<T>> list(List<String> ids,
      {bool throwOnNotFound: true,
      AclContext aclContext,
      bool useFactory: true}) async {
    List<T> objects;
    try {
      if (ids.isEmpty) return [];
//      if (ids.length == 1) {
//        var doc = await this.db.getDocument(ids.first);
//        objects = [fromJson(doc.toJson2(), useFactory: useFactory)];
//      } else {
      var answer = await this.db.queryAllDocsPost(ids) as Map;
      var rows = answer["rows"] as List;
      objects = rows.map((m) {
        if (m["doc"] == null) {
          if (throwOnNotFound) {
            throw new ServiceError.notFound(
                '${this.serviceName}.list.not.found.${m["id"]}',
                '${this.serviceName} : not found');
          }
          return null;
        } else {
          return fromJson(new couch.Document.fromMap(m["doc"] as Map).toJson2(),
              useFactory: useFactory);
        }
      }).toList();
//      }

      var canAccess = await this
          .accessManager
          .canAccessObjects(Privilege.READ, aclContext, objects);

      if (!canAccess)
        throw new ServiceError(
            401, "${this.serviceName}.list.forbidden", "Forbidden");
    } on ServiceError {
      rethrow;
    } catch (e, st) {
      onError(e, st);
    }
    return objects;
  }

  @override
  Future<String> patch(PatchCommands commands, {AclContext aclContext}) async {
    throw 'not impl';
  }

  @override
  Future delete(String id, String rev,
      {AclContext aclContext, bool force: false}) async {
    try {
      var object = await this.get(id, aclContext: aclContext);

      var canAccess = await this
          .accessManager
          .canAccessObjects(Privilege.DELETE, aclContext, [object]);

      if (!canAccess)
        throw new ServiceError(
            401, "${this.serviceName}.delete.forbidden", "Forbidden");

      if (force) {
        await db.deleteDocument(id, rev);
      } else {
        object.isDeleted = true;
        var doc = new couch.Document.fromMap2(this.toJson(object));
        var rev = await db.updateDocument(doc);
        return rev.rev;
      }
    } on ServiceError {
      rethrow;
    } catch (e, st) {
      onError(e, st);
    }
  }

  @override
  Future<String> update(T object, {AclContext aclContext}) async {
    try {
      var canAccess = await this
          .accessManager
          .canAccessObjects(Privilege.READWRITE, aclContext, [object]);

      if (!canAccess)
        throw new ServiceError(
            401, "${this.serviceName}.update.forbidden", "Forbidden");

      object.rev = (await db
              .updateDocument(new couch.Document.fromMap2(this.toJson(object))))
          .rev;
    } on ServiceError {
      rethrow;
    } catch (e, st) {
      onError(e, st);
    }
    return object.rev;
  }

  Future<List<T>> findStartKeys(String viewName,
      {startKey,
      endKey,
      int limit: 20,
      int skip: 0,
      bool descending: true,
      bool useFactory: false,
      AclContext aclContext}) async {
    List<T> list;
    try {
      var view = this.db.design(designDocName).view(viewName);
      var query = new couch.ViewQueryParam(
          startkey: startKey,
          endkey: endKey,
          limit: limit,
          skip: skip,
          descending: descending);
      var answer = await view.query(query) as Map;

      List<String> ids = new List<String>.of(
          (answer["rows"] as List).map((m) => (m as Map)["id"] as String));

      list =
          await this.list(ids, aclContext: aclContext, useFactory: useFactory);
    } on ServiceError {
      rethrow;
    } catch (e, st) {
      onError(e, st);
    }

    return list;
  }

  Future<List<T>> findKeys(String viewName,
      {List keys, bool useFactory: false, AclContext aclContext}) async {
    List<T> list;
    try {
      var view = this.db.design(designDocName).view(viewName);
      var query = new couch.ViewQueryParam(keys: keys);
      var answer = await view.query(query) as Map;

      var ids = List<String>.of(
          (answer["rows"] as List).map((m) => m["id"] as String),
          growable: false);

      list =
          await this.list(ids, aclContext: aclContext, useFactory: useFactory);
    } on ServiceError {
      rethrow;
    } catch (e, st) {
      onError(e, st);
    }

    return list;
  }
}
