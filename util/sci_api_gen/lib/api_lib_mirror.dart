library sci.api.builder;

import 'dart:mirrors';

import 'api_lib.dart';

class ApiLibraryMirror {
  ApiLibrary build(String libName) {
    var libM = findLibrary(new Symbol(libName));
    var apiLib = new ApiLibrary(libName);

    libM.declarations.forEach((s, dec) {
      if (dec is ClassMirror) {
        LibraryMirror ol = dec.owner as LibraryMirror;
        var interfaces = dec.superinterfaces.map((each) {
          LibraryMirror l = each.owner as LibraryMirror;
          var libDependency =
              ol.libraryDependencies.firstWhere((ld) => ld.targetLibrary == l);
          if (libDependency.prefix != null) {
            return MirrorSystem.getName(libDependency.prefix) +
                '.' +
                MirrorSystem.getName(each.simpleName);
          } else {
            return MirrorSystem.getName(each.simpleName);
          }
        }).toList();
        var metadata = dec.metadata.map((im) => im.reflectee).toList();
        var clazz = new ApiClass(
            apiLib,
            MirrorSystem.getName(dec.simpleName),
            MirrorSystem.getName(dec.superclass.simpleName),
            interfaces,
            metadata);
        apiLib.addClass(clazz);

        dec.declarations.forEach((s, mdec) {
          if (mdec is VariableMirror) {
            String range;
            bool isFunctional;

            if (mdec.type.simpleName == new Symbol('List')) {
              if (mdec.type.typeArguments.isEmpty)
                throw 'type arguments is empty on class ${clazz.name} property ${mdec.simpleName}';

              range = MirrorSystem
                  .getName(mdec.type.typeArguments.first.simpleName);

              if (range == 'dynamic')
                throw 'type arguments is dynamic on class ${clazz.name} property ${mdec.simpleName}';

              isFunctional = false;
            } else {
              range = MirrorSystem.getName(mdec.type.simpleName);
              isFunctional = true;
            }

            var metadata = mdec.metadata.map((im) => im.reflectee).toList();

            var p = new ApiProperty(
                clazz,
                MirrorSystem.getName(mdec.simpleName),
                range,
                isFunctional,
                metadata);

            clazz.addProperty(p);
          } else if (mdec is MethodMirror) {
            var metadata = mdec.metadata.map((im) => im.reflectee).toList();
            var m = new ApiMethod(clazz, MirrorSystem.getName(mdec.simpleName),
                mdec.isConstructor, metadata);

            mdec.parameters.forEach((pm) {
              m.addArgument(new ApiArgument(
                  MirrorSystem.getName(pm.simpleName),
                  MirrorSystem.getName(pm.type.simpleName),
                  pm.type.typeArguments
                      .map((each) => MirrorSystem.getName(each.simpleName))
                      .toList()));
            });

            m.returnApiType = new ApiType.from(mdec.returnType);

//            m.returnType = MirrorSystem.getName(mdec.returnType.simpleName);
//            m.returnTypes = mdec.returnType.typeArguments
//                .map((each) => MirrorSystem.getName(each.simpleName))
//                .toList();

            clazz.addMethod(m);
          }
        });
      }
    });

    return apiLib;
  }

  LibraryMirror findLibrary(Symbol name) {
    return currentMirrorSystem()
        .libraries
        .values
        .firstWhere((ml) => ml.simpleName == name);
  }
}
