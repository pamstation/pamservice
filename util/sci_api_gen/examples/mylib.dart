library mylib;

import 'test.dart';
part 'male.dart';

@MyAnnotation(name:'test')
class Person {

  int age;
  num size;
  String name;

  List<String> friends;


  Address mainAddress;

  List<Address> addresses;
}



class Address {
  String city;
}