import 'dart:mirrors';

import 'mylib.dart';

LibraryMirror findLibrary(Symbol name) {
  return currentMirrorSystem()
      .libraries
      .values
      .firstWhere((ml) => ml.simpleName == name);
}


class MyAnnotation {
  final String name;
  const MyAnnotation({this.name});
}

main(){
  var libM = findLibrary(new Symbol('mylib'));
  libM.declarations.forEach((s, dec) {

    print(s);
    print(dec);

    if (dec is ClassMirror){
      if (dec.simpleName == new Symbol('Person')){
        dec.metadata.forEach((m){
          print(m);
          print(m.reflectee);
          if (m.reflectee is MyAnnotation){
            print('MyAnnotation $m name ${m.reflectee.name}');
          }
        });
        dec.declarations.values.forEach((v){
          print(v);
        });
      }
    }

  });
}